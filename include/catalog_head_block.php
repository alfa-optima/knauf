<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>


<noindex>
    <section class="bcgBlock  bcgBlock--indexTop" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/images/blockbcg1-2.jpg);">

		<div class="textBlockOverBcgBlock textBlockOverBcgBlock--noMobile" style="opacity:90%">
            <div class="textBlockOverBcgBlock__wrapper">
                <h3 class="textBlockOverBcgBlock__title"><? $APPLICATION->IncludeFile( "/inc/main_top_title.inc.php", Array(), Array("MODE"=>"html") ); ?></h3>
                <p class="textBlockOverBcgBlock__text"><? $APPLICATION->IncludeFile( "/inc/main_top_text.inc.php", Array(), Array("MODE"=>"html") ); ?></p>
                <a href="/search/" class="textBlockOverBcgBlock__link">Поиск продуктов</a>
            </div>
        </div>

        <a href="#popular" class="scrollDownLink mainPopScrollLink" style="display: none">Далее</a>

    </section>
</noindex>


<!--<noindex>
    <section id="banner" class="banner">
        <div class="banner__wrapper">
            <a class="banner__image" target="_blank" href="https://www.rotband.ru/" rel="nofollow">
<img src="<?//=SITE_TEMPLATE_PATH?>/images/cutout_1204312_detail_picture_1.jpg">
            </a>
            <header>

<? //$APPLICATION->IncludeFile( SITE_TEMPLATE_PATH."/inc/main_banner_text.inc.php", Array(), Array("MODE"=>"html") ); ?>

            </header>
        </div>
    </section>
</noindex>-->


<section class="textBlockOverBcgBlock textBlockOverBcgBlock--yesMobile">
    <div class="textBlockOverBcgBlock__wrapper">
        <h3 class="textBlockOverBcgBlock__title"><? $APPLICATION->IncludeFile( "/inc/main_top_title.inc.php", Array(), Array("MODE"=>"html") ); ?></h3>
        <p class="textBlockOverBcgBlock__text"><? $APPLICATION->IncludeFile( "/inc/main_top_text.inc.php", Array(), Array("MODE"=>"html") ); ?></p>
        <a href="/search/" class="textBlockOverBcgBlock__link">Поиск продуктов</a>
    </div>
</section>