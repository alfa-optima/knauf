<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>
<noindex>
    <section class="bcgBlock  bcgBlock--indexTop" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/images/blockbcg1-1.jpg);">
        <div class="bcgBlock__wrapper">
                <h2 class="bcgBlock__title">Весь ассортимент в одном клике</h2>
        </div>
		<div class="textBlockOverBcgBlock textBlockOverBcgBlock--noMobile" style="opacity:0.9;">
            <div class="textBlockOverBcgBlock__wrapper">
                <h3 class="textBlockOverBcgBlock__title"><? $APPLICATION->IncludeFile( "/inc/main_top_title_new.inc.php", Array(), Array("MODE"=>"html") ); ?></h3>
                <p class="textBlockOverBcgBlock__text"><? $APPLICATION->IncludeFile( "/inc/main_top_text_new.inc.php", Array(), Array("MODE"=>"html") ); ?></p>
                <a href="/search/" class="textBlockOverBcgBlock__link">Поиск продуктов и&nbsp;систем</a>
            </div>
        </div>

        <a href="#popular" class="scrollDownLink mainPopScrollLink" style="display: none">Далее</a>

    </section>
</noindex>


<section class="textBlockOverBcgBlock textBlockOverBcgBlock--yesMobile">
    <div class="textBlockOverBcgBlock__wrapper">
        <h3 class="textBlockOverBcgBlock__title"><? $APPLICATION->IncludeFile( "/inc/main_top_title_new.inc.php", Array(), Array("MODE"=>"html") ); ?></h3>
        <p class="textBlockOverBcgBlock__text"><? $APPLICATION->IncludeFile( "/inc/main_top_text_new.inc.php", Array(), Array("MODE"=>"html") ); ?></p>
        <a href="/search/" class="textBlockOverBcgBlock__link">Поиск продуктов и&nbsp;систем</a>
    </div>
</section>