<? //include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
\CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("404 Страница не найдена"); ?>

<article class="block  block--top">
    <div class="block__wrapper">

        <h2 class="block__title  block__title--faq">404 Страница не найдена</h2>

        <div class="not_found_page">
            <!--<h1>Страница не найдена</h1>-->
            <div class="not_found_page_subtext">Ошибка 404</div>
            <div class="not_found_text">Страница удалена, перемещена или вы ошиблись адресом</div>
        </div>

    </div>
</article>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>