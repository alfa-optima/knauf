<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if( $USER->IsAuthorized() ){

    $user = new project\user( $USER->GetID() );

    if( $user->isDealer() ){

        // Назначение цен на товары
        $APPLICATION->IncludeComponent(
            "aoptima:personal_add_prices", ""
        );

    } else {

        LocalRedirect('/personal/');
    }

} else {

    LocalRedirect('/personal/');
}

