<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if( $USER->IsAuthorized() ){
    LocalRedirect("/personal/");
} ?>

<? if( 1==2 ){ ?>

    <section class="bcgBlock  bcgBlock--indexTop" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/images/blockbcg3.jpg');">
        <div class="bcgBlock__wrapper">
            <h2 class="bcgBlock__title">Восстановление пароля</h2>
        </div>
    </section>

<? } ?>

<section class="block">
    <div class="block__wrapper" style="padding-top: 0px;">

        <form onsubmit="return false" method="post" class="form  form--restore password_recovery_form">
            <div class="form__wrapper">

                <div class="form__group">
                    <label for="restore-email">Эл. почта</label>
                    <input type="text" name="email" placeholder="Введите вашу&nbsp;эл. почту">
                </div>

                <p class="error___p"></p>

                <div class="form__buttons">
                    <input type="button" value="Отправить запрос" class="form__button password_recovery_button to___process">
                </div>

            </div>
        </form>

    </div>
</section>