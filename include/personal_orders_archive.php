<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if( $USER->IsAuthorized() ){

    $dealer = new project\dealer( $USER->GetID() );

    if( $dealer->isDealer() ){

        // Архив заказов дилера
        $APPLICATION->IncludeComponent(
            "aoptima:dealer_orders_archive", ""
        );

    } else {

        LocalRedirect(PREFIX.'/personal/');
    }

} else {

    LocalRedirect(PREFIX.'/personal/');
}

