<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

// Категории каталога 1 уровня
$catalog_sections = project\catalog::sections_1_level(); ?>


<nav class="main-footer__menu">


    <div class="main-footer__column">
        <div class="main-footer__menu-item">
            <h2 class="main-footer__menu-subtitle">Каталог</h2>
            <ul class="main-footer__menu-list">
                <? foreach ( $catalog_sections as $sect ){ ?>
                    <li>
                        <a href="<?=$sect['SECTION_PAGE_URL']?>"><?=$sect['NAME']?></a>
                    </li>
                <? } ?>
            </ul>
        </div>
    </div>


    <? // Нижнее меню №1
    $APPLICATION->IncludeComponent(
        "bitrix:menu",  "bottom_menu", // Шаблон меню
        Array(
            "TITLE" => "Информация",
            "ROOT_MENU_TYPE" => "bottom_1",
            "MENU_CACHE_TYPE" => "A",
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "CACHE_SELECTED_ITEMS" => "N",
            "MENU_CACHE_GET_VARS" => array(""),
            "MAX_LEVEL" => "1",
            "CHILD_MENU_TYPE" => "left",
            "USE_EXT" => "N",
            "DELAY" => "N",
            "ALLOW_MULTI_SELECT" => "N"
        )
    ); ?>


    <? // Нижнее меню №2
    $APPLICATION->IncludeComponent(
        "bitrix:menu",  "bottom_menu", // Шаблон меню
        Array(
            "TITLE" => "Сервис",
            "ROOT_MENU_TYPE" => "bottom_2",
            "MENU_CACHE_TYPE" => "A",
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "CACHE_SELECTED_ITEMS" => "N",
            "MENU_CACHE_GET_VARS" => array(""),
            "MAX_LEVEL" => "1",
            "CHILD_MENU_TYPE" => "left",
            "USE_EXT" => "N",
            "DELAY" => "N",
            "ALLOW_MULTI_SELECT" => "N"
        )
    ); ?>






</nav>