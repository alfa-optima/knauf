<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$template = "";

$isDealer = 'N';
$isSubDealer = 'N';
	global $USER;
	/*if ($USER->IsAdmin()){
		$currentUser = new project\user( $USER->GetID() );
		$userProps = $currentUser->arUser;
		if(strlen($userProps["UF_MAIN_USER"]) > 0){
			$user = new project\user( $userProps["UF_MAIN_USER"] );
			if( $user->isDealer() ){
				$template = "dealer";
				$isDealer = 'Y';
				$isSubDealer = 'Y';
			}
		}
	}*/
if( $USER->IsAuthorized() ){
    $user = new project\user( $USER->GetID() );
    if( $user->isDealer() ){
        $template = "dealer";
        $isDealer = 'Y';
    } else{
		$currentUser = new project\user( $USER->GetID() );
		$userProps = $currentUser->arUser;
		if(strlen($userProps["UF_MAIN_USER"]) > 0){
			$user = new project\user( $userProps["UF_MAIN_USER"] );
			if( $user->isDealer() ){
				$template = "dealer";
				$isDealer = 'Y';
				$isSubDealer = 'Y';
			}
		}
	}
}

$APPLICATION->IncludeComponent(
    "aoptima:personal", $template,
    array('isDealer' => $isDealer, 'isSubDealer' => $isSubDealer)
);