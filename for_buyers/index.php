<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Информация для покупателей");
?>
<iframe style="width:100%; height:315px;" src="https://www.youtube.com/embed/YSHS8FvBuYc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<p style="text-align: left; font-family: futuralight, Calibri, Candara, GillSansLightC;">
 <span style="font-size: 16pt;">
	Пользовательское соглашение </span>
</p>
<p style="text-align: left;">
 <b><span style="font-family: Arial, Helvetica; font-size: 9pt;">1. Определения</span></b><span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
</p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
<p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;">1.1. </span><i><span style="font-family: Arial, Helvetica; font-size: 9pt;">Владелец сайта</span></i><span style="font-family: Arial, Helvetica; font-size: 9pt;"> –&nbsp;юридическое лицо, ООО «КНАУФ ГИПС» (ОГРН 1025002863049, ИНН 5024051564), осуществляющее администрирование Сайта, являющееся стороной настоящего Соглашения. Место нахождения: 143405, Московская обл, город Красногорск, улица Центральная, 139. </span>
</p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
<p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;">1.2. </span><i><span style="font-family: Arial, Helvetica; font-size: 9pt;">Заказ</span></i><span style="font-family: Arial, Helvetica; font-size: 9pt;"> –&nbsp;действия Пользователя по приобретению Товара (Товаров) на Сайте у соответствующего Партнера, включающие в себя выбор Товара, его параметров (при наличии) и количества. </span>
</p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
<p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
</p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
<p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;">1.3. </span><i><span style="font-family: Arial, Helvetica; font-size: 9pt;">Корзина</span></i><b><span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span></b><span style="font-family: Arial, Helvetica; font-size: 9pt;">–&nbsp;Сервис Сайта, позволяющий сохранять выбранные Пользователем Товары до момента формирования Заказа. </span>
</p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
<p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;">1.4. </span><i><span style="font-family: Arial, Helvetica; font-size: 9pt;">Личный кабинет Пользователя</span></i><span style="font-family: Arial, Helvetica; font-size: 9pt;"> –&nbsp;персональный раздел Пользователя, представленный в виде защищенных страниц Сайта, доступ к которым возможен только при вводе Учетных данных и после завершения Регистрации Пользователя, в котором Пользователю доступно управление отдельными Сервисами Сайта, на предложенных Владельцем сайта условиях. </span>
</p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
<p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;">1.5. </span><i><span style="font-family: Arial, Helvetica; font-size: 9pt;">Пользователь</span></i><span style="font-family: Arial, Helvetica; font-size: 9pt;">&nbsp;–&nbsp;лицо, не имеющее законодательных ограничений для акцепта настоящего Соглашения, осуществившее доступ к Сайту и/или использующее Сайт, любые его Сервисы, функции и/или возможности, независимо от факта Регистрации на Сайте.</span><br>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
</p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
<p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
</p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
<p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;">1.6. </span><i><span style="font-family: Arial, Helvetica; font-size: 9pt;">Регистрация</span></i><b><span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span></b><span style="font-family: Arial, Helvetica; font-size: 9pt;">–&nbsp;совокупность действий Пользователя в соответствии с указанными на Сайте инструкциями, включая предоставление Регистрационных данных, а также иной информации, совершаемых Пользователем с использованием специальной формы пользовательского интерфейса Сайта в целях формирования Личного кабинета Пользователя и получения доступа к отдельным сервисам Сайта, включая, но не ограничиваясь: история заказов, создание шаблона адреса доставки, подписка на рассылку. </span>
</p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
<p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
</p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
<p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
</p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
<p style="text-align: left;">
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span><b><span style="font-family: Arial, Helvetica; font-size: 9pt;">2. Общие положения</span></b><span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
</p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
<p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;">2.1. Настоящее Пользовательское соглашение (далее по тексту – «Соглашение») регулирует отношения между Владельцем сайта и Пользователем , возникающие при использовании Сайта, на указанных далее условиях. </span>
</p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
<p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
</p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
<p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;">2.2. Пользовательское соглашение является публичной офертой Владельца сайта в отношении Пользователя в соответствии со ст. 437 Гражданского кодекса РФ, т.е. содержит все существенные условия соглашения, заключаемого между Владельцем сайта и Пользователем. </span>
</p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
<p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;">2.3. Полное и безоговорочное принятие данного предложения Владельца сайта со стороны Пользователя осуществляется действиями Пользователя, свидетельствующими об акцепте (конклюдентные действия) Соглашения посредством Регистрации на Сайте или формирования Заказа без Регистрации. В случае акцепта настоящей оферты, между Сторонами заключается договор в электронной форме на основании п.2 ст. 434 и п. 3 ст. 438 Гражданского кодекса РФ. </span>
</p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
<p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;">2.4. Осуществляя доступ к Сайту и заключая таким образом настоящее Пользовательское соглашение, Пользователь гарантирует, что обладает всеми правами и полномочиями, необходимыми для заключения и исполнения Пользовательского соглашения, в том числе является совершеннолетним и полностью дееспособным лицом, либо несовершеннолетним лицом, объявленным по решению уполномоченного органа полностью дееспособным (эмансипация), либо несовершеннолетним лицом, достигшим четырнадцати лет и получившим письменное разрешение в требуемой законом форме от своих родителей или иных законных представителей на заключение Пользовательского соглашения. </span>
</p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span>
<p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;">2.5. Пользовательское соглашение может быть изменено Владельцем сайта в любое время без какого-либо специального уведомления об этом Пользователя. Новая редакция Пользовательского соглашения вступает в силу с момента ее размещения на Сайте, если Владельцем сайта прямо не указано иное. Регулярное ознакомление с действующей редакцией Пользовательского соглашения является обязанностью Пользователя. </span>
</p>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> </span><br>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"> <a href="/user_agreement/" style="color: #1d9ed8;"><span style="font-family: Arial, Helvetica; font-size: 9pt;"><u>Полный текст пользовательского соглашения.</u></span></a></span>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>