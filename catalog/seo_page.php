<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('iblock');

$arURI = tools\funcs::arURI( tools\funcs::pureURL() );

// SEO-страница
$seo_page = project\SeopageTable::getByCode($arURI[1]);

if( intval($seo_page['ID']) > 0 ){
    $section = project\catalog::getSectionByXMLID( $seo_page['SECTION_XML_ID'] );
}

if(
    intval($seo_page['ID']) > 0
    &&
    intval($section['ID']) > 0
){
    
    $smart_filter_path = '';
    if( substr_count( tools\funcs::pureURL(), '/filter/' ) ){
        if(preg_match("/\/filter\/(.+)\/apply\//", tools\funcs::pureURL(),$matches)){
            $smart_filter_path = rawurldecode( $matches[1] );
        }
    }

    $description = $seo_page["DESCRIPTION"]?$seo_page["DESCRIPTION"]:$seo_page["NAME"];
    $APPLICATION->SetPageProperty("description", $description);

    $keywords = $seo_page["KEYWORDS"]?$seo_page["KEYWORDS"]:$seo_page["NAME"];
    $APPLICATION->SetPageProperty("keywords", $keywords);

    $title = $seo_page["TITLE"]?$seo_page["TITLE"]:$seo_page["NAME"];
    $APPLICATION->SetPageProperty("title", $title);

    $APPLICATION->AddChainItem($section["NAME"], $section["SECTION_PAGE_URL"]);
    $APPLICATION->AddChainItem($seo_page["NAME"], '/'.$seo_page["CODE"].'/');

    $seoPageResult = project\SeopageTable::getArFilter( $seo_page['CODE'], $section['ID'] );
    extract( $seoPageResult );  //  $catalogFilter + $arFilter

    $GLOBALS[project\catalog::FILTER_NAME] = $catalogFilter;

    global $APPLICATION;
    $hide_catalog_left_menu = $APPLICATION->get_cookie('hide_catalog_left_menu');

    // Подразделы
    $subSects = tools\section::sub_sects($section['ID']);

    $catalogView = $_SESSION['catalogView']?$_SESSION['catalogView']:'grid'; ?>



    <article class="block  block--top">
        <div class="block__wrapper">
            <h1 class="block__title  block__title--search"><?=$seo_page['H1']?$seo_page['H1']:$seo_page['TITLE']?></h1>
        </div>
    </article>


    <section class="searchBlock <? if( $hide_catalog_left_menu == 'Y' ){ echo 'hideMenu'; } ?> <? if( $catalogView == 'list' && count($subSects)==0 ){ echo 'listView'; } ?>">


        <div class="searchPanel">

            <h3 class="searchPanel__title">Найти продукты
                <span class="searchPanel__toggle catalogLeftMenu">
                    <span class="sr-only">Скрыть/показать панель поиска</span>
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/arrow-left.png" alt="стрелка влево" class="searchPanel__toggle-image">
                </span>
            </h3>

            <div class="searchPanel__inner">


                <? // Фильтр
                $APPLICATION->IncludeComponent(
                    "bitrix:catalog.smart.filter", "catalog_filter",
                    array(
                        "arFilter" => $arFilter,
                        "IBLOCK_TYPE" => 'catalog',
                        "IBLOCK_ID" => project\catalog::catIblockID(),
                        "SECTION_ID" => $section['ID'],
                        "FILTER_NAME" => project\catalog::FILTER_NAME,
                        "PRICE_CODE" => [ 'BASE' ],
                        "CACHE_TYPE" => 'A',
                        "CACHE_TIME" => 3600,
                        "CACHE_GROUPS" => 'Y',
                        "SAVE_IN_SESSION" => "N",
                        "FILTER_VIEW_MODE" => 'VERTICAL',
                        "XML_EXPORT" => "N",
                        "SECTION_TITLE" => "NAME",
                        "SECTION_DESCRIPTION" => "DESCRIPTION",
                        'HIDE_NOT_AVAILABLE' => 'N',
                        "TEMPLATE_THEME" => "",
                        'CONVERT_CURRENCY' => 'N',
                        'CURRENCY_ID' => '',
                        "SEF_MODE" => 'Y',
                        "SEF_RULE" => '/catalog/#SECTION_CODE_PATH#/filter/#SMART_FILTER_PATH#/apply/',
                        "SMART_FILTER_PATH" => $smart_filter_path,
                        "PAGER_PARAMS_NAME" => '',
                        "INSTANT_RELOAD" => '',
                    ),
                    $component, array('HIDE_ICONS' => 'Y')
                ); ?>

            </div>
        </div>


        <div class="searchBlock__wrapper">

            <div class="searchBlock__filter">

                <div class="searchBlock__filter-sort">
                    <!--                        <span>Сортировать по</span>-->
                    <!--                        <select name="sort_by">-->
                    <!--                            <option value="price" selected="selected">цене</option>-->
                    <!--                            <option value="name">названию</option>-->
                    <!--                        </select>-->
                </div>

                <div class="view-switcher">
                    <span class="view-switcher__text">Режим просмотра</span>
                    <div class="view-switcher__buttons">
                        <a href="javascript:;" class="view-switcher__button  view-switcher__button--grid"><span class="sr-only">Сетка</span></a>
                        <a href="javascript:;" class="view-switcher__button  view-switcher__button--list"><span class="sr-only">Список</span></a>
                    </div>
                </div>

            </div>



            <?

            $sort1 = 'PROPERTY_HAS_OFFERS';   $order1 = 'DESC';
            $sort2 = 'SORT';   $order2 = 'ASC';

            $q = strip_tags(trim($_GET['q']));
            if( strlen($q) > 0 ){
                // Поиск по элементам инфоблока
                $search_ids = project\search::iblockElementsByQ($q, true);
                if( count($search_ids) > 0 ){
                    $GLOBALS[project\catalog::FILTER_NAME]['ID'] = $search_ids;
                } else {
                    $search_ids = project\search::iblockElementsByQ($q);
                    if( count($search_ids) > 0 ){
                        $GLOBALS[project\catalog::FILTER_NAME]['ID'] = $search_ids;
                    } else {
                        $GLOBALS[project\catalog::FILTER_NAME]['ID'] = array(0);
                    }
                }
            }

            //////////////////////////////////
            // Все дилеры
            $dealer = new project\dealer();
            $allDealers = $dealer->allList();
            //////////////////////////////////

            $GLOBALS[project\catalog::FILTER_NAME]['SECTION_ID'] = $section['ID'];
            $GLOBALS[project\catalog::FILTER_NAME]['INCLUDE_SUBSECTIONS'] = 'Y';

            // фильтр по СД
            $GLOBALS[project\catalog::FILTER_NAME] = project\catalog::AddStopSDsToFilter($GLOBALS[project\catalog::FILTER_NAME]);

            //echo "<pre>"; print_r( $GLOBALS[project\catalog::FILTER_NAME] ); echo "</pre>";

            $intSectionID = $APPLICATION->IncludeComponent(
                "bitrix:catalog.section", "catalog_goods",
                array(
                    'seo_page' => $seo_page,
                    //////////////////////////////
                    'ALL_DEALERS' => $allDealers,
                    //////////////////////////////
                    "LOC" => $_SESSION['LOC'],
                    'IS_DEALER' => $user->isDealer()?'Y':'N',
                    "IBLOCK_TYPE" => "content",
                    "IBLOCK_ID" => project\catalog::catIblockID(),
                    "INCLUDE_SUBSECTIONS" => 'Y',
                    "SHOW_ALL_WO_SECTION" => "Y",
                    "SECTION_USER_FIELDS" => array(),
                    "ELEMENT_SORT_FIELD" => $sort1,
                    "ELEMENT_SORT_ORDER" => $order1,
                    "ELEMENT_SORT_FIELD2" => $sort2,
                    "ELEMENT_SORT_ORDER2" => $order2,
                    "FILTER_NAME" => project\catalog::FILTER_NAME,
                    "HIDE_NOT_AVAILABLE" => "N",
                    "PAGE_ELEMENT_COUNT" => project\catalog::GOODS_CNT + 1,
                    "LINE_ELEMENT_COUNT" => "3",
                    "PROPERTY_CODE" => array("ARTICLE","MOD","MODS_ARTICLE","ED","VIEW","EDGE_VIEW","VIDEO","RESIST_WATER","DRYING_TIME","FLAMMABILITY_GROUP","BASE_MATERIAL","RESIST_COLD","FLOOR_COATING","TARGET","MIXTURE_BASE","PERFORATION","ROOM","POP","PREIM","PRIM","BUY_WITH","SEE_WITH","PROPERTIES","APPL_METHOD","TECH","TYPE","TYPE_OBJECT","TYPE_OF_APPLICATION","TYPE_PROFILE","TYPE_COATING","LAYER_THICKNESS","FASOVKA","TILE","COLOR","WEIGHT","HEIGHT","LENGTH","DEPTH","WIDTH","CROSS","CALC_WEIGHT","MAX_GABARIT","HAS_OFFERS"),
                    "OFFERS_LIMIT" => "0",
                    "OFFERS_PROPERTY_CODE" => ['LOCATION'],
                    "TEMPLATE_THEME" => "",
                    "PRODUCT_SUBSCRIPTION" => "N",
                    "SHOW_DISCOUNT_PERCENT" => "N",
                    "SHOW_OLD_PRICE" => "N",
                    "MESS_BTN_BUY" => "Купить",
                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                    "MESS_BTN_SUBSCRIBE" => "Подписаться",
                    "MESS_BTN_DETAIL" => "Подробнее",
                    "MESS_NOT_AVAILABLE" => "Нет в наличии",
                    "SECTION_URL" => "",
                    "DETAIL_URL" => "",
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_GROUPS" => "Y",
                    "SET_META_KEYWORDS" => "N",
                    "META_KEYWORDS" => "",
                    "SET_META_DESCRIPTION" => "N",
                    "META_DESCRIPTION" => "",
                    "BROWSER_TITLE" => "-",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "DISPLAY_COMPARE" => "N",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "CACHE_FILTER" => "Y",
                    "PRICE_CODE" => array('BASE'),
                    "USE_PRICE_COUNT" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "PRICE_VAT_INCLUDE" => "Y",
                    "CONVERT_CURRENCY" => "N",
                    "BASKET_URL" => "/personal/basket.php",
                    "ACTION_VARIABLE" => "action",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "USE_PRODUCT_QUANTITY" => "N",
                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                    "PRODUCT_PROPS_VARIABLE" => "prop",
                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                    "PRODUCT_PROPERTIES" => "",
                    "PAGER_TEMPLATE" => "",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Товары",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "Y",
                    "ADD_PICT_PROP" => "-",
                    "LABEL_PROP" => "-",
                    'INCLUDE_SUBSECTIONS' => "Y",
                    'SHOW_ALL_WO_SECTION' => "Y"
                ), $component
            ); ?>

        </div>
    </section>



    <input type="hidden" name="seo_page_code" value="<?=$seo_page['CODE']?>">
    <input type="hidden" name="section_id" value="<?=$section['ID']?>">





<? } else {

    include($_SERVER["DOCUMENT_ROOT"]."/local/templates/main/include/404include.php");
}



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>