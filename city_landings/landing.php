<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$code = trim(strip_tags($_GET['code']));

if( strlen($code) > 0 ){

    $landing = tools\el::info_by_code( $code, project\city_landing::IBLOCK_ID );
    
    if( intval( $landing['ID'] ) > 0 ){
    
        // city_landing
        $APPLICATION->IncludeComponent(
            "aoptima:city_landing", "", [ 'code' => $code ]
        );

    } else {

        include $_SERVER['DOCUMENT_ROOT']."/include/404include.php";
    }

} else {

    include $_SERVER['DOCUMENT_ROOT']."/include/404include.php";
}







require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");


