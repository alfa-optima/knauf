<?

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


global $APPLICATION;
$aMenuLinksExt = $APPLICATION->IncludeComponent(
    "bitrix:menu.sections", "",
    Array(
        "IBLOCK_ID" => project\catalog::catIblockID(),
        "IBLOCK_TYPE" => "catalog",
        "DEPTH_LEVEL" => "2",
        "CACHE_TIME" => "60",
        "CACHE_TYPE" => "A",
    )
);



array_unshift($aMenuLinksExt, [
    'Поиск продуктов',
    '#SITE_DIR#/search/',
    [ '#SITE_DIR#/search/' ],
    [
        'FROM_IBLOCK' => project\catalog::catIblockID(),
        'IS_PARENT' => 1,
        'DEPTH_LEVEL' => 1,
    ]
]);


//echo "<pre>"; print_r( $aMenuLinksExt ); echo "</pre>";


$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);

