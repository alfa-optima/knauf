<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Требования к сотруднику поставщика");
?><p style="text-align: right;">
 <span style="font-family: Arial, Helvetica; font-size: 9pt;">Версия 1.0 от 02.09.19 </span> <br>
</p>
<p style="text-align: center;">
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"><span style="font-weight: 700;">Для работы с сервисом Маркетплейс kupi.knauf.ru необходимы:</span>&nbsp;</span>
</p>
<p style="text-align: left;">
 <span style="font-family: Arial, Helvetica; font-size: 9pt;">
	— Доступ к электронной почте, которая указана в договоре, через нее будет осуществляться коммуникация по заказам;&nbsp;</span>
</p>
<p style="text-align: left;">
 <span style="font-family: Arial, Helvetica; font-size: 9pt;">&nbsp;— Возможность связаться с покупателем в течение двух часов с момента оформления заказа;&nbsp;</span>
</p>
<p style="text-align: left;">
 <span style="font-family: Arial, Helvetica; font-size: 9pt;">&nbsp;— Доступ к личному кабинету поставщика для работы с заказами.&nbsp;</span>
</p>
<p style="text-align: center;">
 <span style="font-family: Arial, Helvetica; font-size: 9pt;"><span style="font-weight: 700;">Обязанности сотрудника:</span></span>
</p>
<ul>
	<li> <span style="font-family: Arial, Helvetica; font-size: 9pt;">— Получение заказов по электронной почте и/или в личном кабинете партнера на сайте.</span> </li>
	<li> <span style="font-family: Arial, Helvetica; font-size: 9pt;">— Обработка и подтверждение заказов (связь с покупателем, изменение статуса заказов в личном кабинете). </span> </li>
	<li> <span style="font-family: Arial, Helvetica; font-size: 9pt;">— Поддержание актуальности размещенного на сайте ассортимента, цен и наличия товаров. </span> </li>
	<li> <span style="font-family: Arial, Helvetica; font-size: 9pt;">— Соблюдение условий доставки заказов и сервиса (подъем, разгрузка) в соответствии с указанными Дилером условиями в Маркетплейсе. </span> </li>
</ul>
 <br>
 <span style="font-family: Arial, Helvetica; font-size: 9pt;">
Вы всегда можете получить информационную поддержку по работе с сервисом по эл. адресу <a href="mailto:support@kupi-knauf.ru">support@kupi-knauf.ru</a> и телефону консультационной службы +7 499 404-20-43.<br>
 </span><span style="font-family: Arial, Helvetica; font-size: 9pt;"><br>
 </span><span style="font-family: Arial, Helvetica; font-size: 9pt;">Желаем приятной работы и хороших продаж! <br>
 <br>
 </span><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>