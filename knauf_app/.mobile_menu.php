<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/mobileapp/public/.mobile_menu.php");

$arMobileMenuItems = array(
	array(
		"type" => "section",
		"text" => "KNAUF",
		"sort" => "100",
		"items" => array(
	array(
				"text" => "КАТАЛОГ",
				"data-url" => SITE_DIR . "knauf_app/catalog/",
				"class" => "menu-item",
				"id" => "catalog",

			),
			array(
				"text" => "ПОКУПАТЕЛЯМ",
				"data-url" => SITE_DIR . "knauf_app/for_buyers/",
				"class" => "menu-item",
				"id" => "mainbuyers",

			),
array(
				"text" => "ВОПРОС-ОТВЕТ",
				"data-url" => SITE_DIR . "knauf_app/faq/",
				"class" => "menu-item",
				"id" => "faq",

			),
array(
				"text" => "ОБРАТНАЯ СВЯЗЬ",
				"data-url" => SITE_DIR . "knauf_app/contacts/",
				"class" => "menu-item",
				"id" => "faq",

			)

		)
	)
);
?>