<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$catalog_iblock_id = project\catalog::catIblockID();

$user = new project\user();

//////////////////////////////////
// Все дилеры
$dealer = new project\dealer();
$allDealers = $dealer->allList();
//////////////////////////////////

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $section_id = strip_tags($_POST['section_id']);

    ob_start();
        $GLOBALS['catalog_slider']['SECTION_ID'] = $section_id;
        $GLOBALS['catalog_slider']['INCLUDE_SUBSECTIONS'] = 'Y';
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section", "catalog_slider_new",
            Array(
                //////////////////////////////
                'ALL_DEALERS' => $allDealers,
                //////////////////////////////
                "LOC" => $_SESSION['LOC'],
                'IS_DEALER' => $user->isDealer()?'Y':'N',
                'SECT_ID' => $section_id,
                "IBLOCK_TYPE" => "content",
                "IBLOCK_ID" => $catalog_iblock_id,
                "SECTION_USER_FIELDS" => array(),
                "ELEMENT_SORT_FIELD" => "NAME",
                "ELEMENT_SORT_ORDER" => "ASC",
                "ELEMENT_SORT_FIELD2" => "NAME",
                "ELEMENT_SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "catalog_slider",
                "HIDE_NOT_AVAILABLE" => "N",
                "PAGE_ELEMENT_COUNT" => "30",
                "LINE_ELEMENT_COUNT" => "3",
                "PROPERTY_CODE" => array('WEIGHT', 'WIDTH', 'HEIGHT', 'LENGTH', 'DEPTH', 'CROSS'),
                "OFFERS_LIMIT" => "0",
                "OFFERS_PROPERTY_CODE" => ['LOCATION'],
                "TEMPLATE_THEME" => "",
                "PRODUCT_SUBSCRIPTION" => "N",
                "SHOW_DISCOUNT_PERCENT" => "N",
                "SHOW_OLD_PRICE" => "N",
                "MESS_BTN_BUY" => "Купить",
                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                "MESS_BTN_DETAIL" => "Подробнее",
                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                "SECTION_URL" => "",
                "DETAIL_URL" => "",
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_GROUPS" => "Y",
                "SET_META_KEYWORDS" => "N",
                "META_KEYWORDS" => "",
                "SET_META_DESCRIPTION" => "N",
                "META_DESCRIPTION" => "",
                "BROWSER_TITLE" => "-",
                "ADD_SECTIONS_CHAIN" => "N",
                "DISPLAY_COMPARE" => "N",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "CACHE_FILTER" => "Y",
                "PRICE_CODE" => array('BASE'),
                "USE_PRICE_COUNT" => "N",
                "SHOW_PRICE_COUNT" => "1",
                "PRICE_VAT_INCLUDE" => "Y",
                "CONVERT_CURRENCY" => "N",
                "BASKET_URL" => "/personal/basket.php",
                "ACTION_VARIABLE" => "action",
                "PRODUCT_ID_VARIABLE" => "id",
                "USE_PRODUCT_QUANTITY" => "N",
                "ADD_PROPERTIES_TO_BASKET" => "Y",
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                "PRODUCT_PROPERTIES" => "",
                "PAGER_TEMPLATE" => "",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Товары",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "ADD_PICT_PROP" => "-",
                "LABEL_PROP" => "-",
                'INCLUDE_SUBSECTIONS' => "Y",
                'SHOW_ALL_WO_SECTION' => "Y"
            )
        );
    	$html = ob_get_contents();
    ob_end_clean();

	// Ответ
	echo json_encode([
	    "status" => "ok",
        "html" => '<div class="slider__wrapper active" id="slide_subSect_'.$section_id.'">'.$html.'</div>'
    ]);
	return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;