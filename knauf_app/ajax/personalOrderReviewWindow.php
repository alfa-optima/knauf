<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('sale');

use Bitrix\Main,
    Bitrix\Main\Localization\Loc as Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Main\Application,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem,
    Bitrix\Sale,
    Bitrix\Sale\Basket,
    Bitrix\Sale\Order,
    Bitrix\Sale\DiscountCouponsManager,
    Bitrix\Main\Context,
    Bitrix\Sale\Internals;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    if( $USER->IsAuthorized() ){

        if( intval($_POST['order_id']) > 0 ){

            $order = new project\order();
            $user_orders = $order->getList($USER->GetID());

            if( $arOrder = $user_orders[$_POST['order_id']] ){

                $obOrder = Sale\Order::load($arOrder['ID']);

                $propValues = project\order::getOrderPropValues($obOrder);
                $os = new project\order_status();
                $cur_status = $os->get( $obOrder, $propValues );

                if( $cur_status == 'Z' ){

                    $propertyCollection = $obOrder->getPropertyCollection();
                    $dealerProp = $propertyCollection->getItemByOrderPropertyId( project\order::getDealerOrderPropID() );
                    $dealer_id = $dealerProp->getValue();

                    if( intval($dealer_id) > 0 ){

                        $dealer = tools\user::info($dealer_id);

                        if( intval($dealer['ID']) > 0 ){

                            // Ответ
                            echo json_encode(
                                Array(
                                    "status" => "ok",
                                    "dealer_id" => $dealer_id,
                                    "dealer" => $dealer,
                                )
                            );
                            return;

                        }

                    }

                } else {

                    // Ответ
                    echo json_encode(
                        Array(
                            "status" => "error",
                            "text" => 'Отзыв можно оставить только к завершённому заказу'
                        )
                    );
                    return;

                }

            } else {

                // Ответ
                echo json_encode(
                    Array(
                        "status" => "error",
                        "text" => 'Ошибка доступа к заказу'
                    )
                );
                return;

            }

        }

    } else {
        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;