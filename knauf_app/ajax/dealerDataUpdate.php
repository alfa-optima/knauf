<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $user = new project\user();
    if( $USER->IsAuthorized() && $user->isDealer() ){

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        $arUser = tools\user::info($USER->GetID());
        $isSocUser = $arUser['EXTERNAL_AUTH_ID']=='socservices'?'Y':'N';

        if( strlen($arFields["EMAIL"]) > 0 && $isSocUser != 'Y' ){

            // проверяем EMAIL на логин
            $filter = Array(
                "LOGIN_EQUAL" => strip_tags($arFields["EMAIL"]),
                "!ID" => $USER->GetID(),
                "!EXTERNAL_AUTH_ID" => "socservices"
            );
            $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter);
            while ($user = $rsUsers->GetNext()){
                // Если Email уже есть
                echo json_encode( Array( "status" => "error", "text" => "Данный Email уже зарегистрирован на&nbsp;сайте!" ) );  return;
            }
            // проверяем EMAIL на наличие у других пользователей
            $filter = Array(
                "=EMAIL" => strip_tags($arFields["EMAIL"]),
                "!ID" => $USER->GetID(),
                "!EXTERNAL_AUTH_ID" => "socservices"
            );
            $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter);
            while ($user = $rsUsers->GetNext()){
                // Если Email уже есть
                echo json_encode( Array( "status" => "error", "text" => "Данный Email уже зарегистрирован на&nbsp;сайте!" ) );  return;
            }

        }

        // поля для сохранения
        $arUserFields = Array(
            "UF_COMPANY_NAME" => strip_tags($arFields["UF_COMPANY_NAME"]),
            "UF_TYPE_OF_ACTIVITY" => strip_tags($arFields["UF_TYPE_OF_ACTIVITY"]),
            "EMAIL" => strip_tags($arFields["EMAIL"]),
            "UF_PHONES" => array_unique(array_diff($arFields["UF_PHONES"], array(''))),
            "UF_SITE" => strip_tags($arFields["UF_SITE"]),
            "UF_INN" => strip_tags($arFields["UF_INN"]),
            "UF_OGRN" => strip_tags($arFields["UF_OGRN"]),
            "UF_FULL_COMPANY_NAME" => strip_tags($arFields["UF_FULL_COMPANY_NAME"]),
            "UF_POST_ADDRESS" => strip_tags($arFields["UF_POST_ADDRESS"]),
            /*"UF_POST_CITY_ID" => strip_tags($arFields["UF_POST_CITY_ID"]),
            "UF_POST_REGION_ID" => strip_tags($arFields["UF_POST_REGION_ID"]),
            "UF_POST_STREET" => strip_tags($arFields["UF_POST_STREET"]),
            "UF_POST_HOUSE" => strip_tags($arFields["UF_POST_HOUSE"]),
            "UF_POST_KV" => strip_tags($arFields["UF_POST_KV"]),*/
            "UF_POST_INDEX" => strip_tags($arFields["UF_POST_INDEX"]),
            "UF_DOP_INFO" => strip_tags($arFields["UF_DOP_INFO"]),
			"UF_REZHIM" => strip_tags($arFields["UF_REZHIM"]),
            "UF_EMAIL_FOR_ORDERS" => strip_tags($arFields["UF_EMAIL_FOR_ORDERS"]),
        );

        $arUserFields["UF_CONTACT_PERSONS"] = '';
        if(
            is_array($arFields["contact_persons"])
            &&
            count($arFields["contact_persons"]) > 0
        ){
            foreach ( $arFields["contact_persons"] as $k1 => $person ){
                foreach ( $person['phones'] as $k2 => $phone ){
                    if( strlen($phone) == 0 ){
                        unset($arFields["contact_persons"][$k1]['phones'][$k2]);
                    }
                }
            }
            if( $arFields["contact_persons"]['new'] ){
                $item_id = microtime(true);
                $arFields["contact_persons"][$item_id] = $arFields["contact_persons"]['new'];
                unset($arFields["contact_persons"]['new']);
            }
            $arUserFields["UF_CONTACT_PERSONS"] = strip_tags(json_encode($arFields["contact_persons"]));
        }

        // логин
        if(
            strlen($arFields["LOGIN"]) > 0
            &&
            $isSocUser != 'Y'
        ){
            $arUserFields['LOGIN'] = strip_tags($arFields["LOGIN"]);
            // проверяем логин
            $filter = Array(
                "LOGIN_EQUAL" => strip_tags($arFields["LOGIN"]),
                "!ID" => $USER->GetID()
            );
            $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter);
            while ($user = $rsUsers->GetNext()){
                // Если Логин уже есть
                echo json_encode( Array( "status" => "error", "text" => "Данный логин уже зарегистрирован на&nbsp;сайте!" ) );  return;
            }
        }

        // Сохраняем
        $obUser = new \CUser;
        $res = $obUser->Update($USER->GetID(), $arUserFields);
        if( $res ){

            ob_start();
                $APPLICATION->IncludeComponent(
                    "aoptima:dealerContactPersons", "",
                    array( 'USER' => tools\user::info($USER->GetID()) )
                );
                $dealerContactPersons = ob_get_contents();
            ob_end_clean();

            // Ответ
            echo json_encode(
                Array(
                    "status" => "ok",
                    'dealerContactPersons' => $dealerContactPersons
                )
            );
            return;
        } else {
            tools\logger::addError('Ошибка сохранения данных [user_id='.$USER->GetID().']:'.$obUser->LAST_ERROR);
        }

    } else {
        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка сохранения данных"));
return;