<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $arFields = [];
    parse_str($_POST["form_data"], $arFields);

    foreach ( $arFields as $key => $value ){
        if( $key == 'product_id' ){
            $product = tools\el::info($value);
            if( intval($product['ID']) > 0 ){
                $product = project\catalog::updateProductName($product);
                $arFields['product'] = '<a href="'.$product['DETAIL_PAGE_URL'].'">'.$product['NAME'].'</a>';
            }
        }
    }


    $server_name = \Bitrix\Main\Config\Option::get('main', 'server_name');

    /////////////////////////////////
    $params['iblock_code'] = 'quick_order';
    $params['iblock_name'] = 'Быстрый заказ';

    $params['email_to'] = \Bitrix\Main\Config\Option::get('aoptima.project', 'COMMON_ORDERS_EMAIL_TO');

    ////////////////////////////////
    $params['settings'] = array(
        'dealer' => 'Дилер',
        'fio' => 'ФИО',
        'phone' => 'Телефон',
        'email' => 'Email',
        'city' => 'Город',
        'product_id' => 'ID товара',
        'product' => 'Товар',
    );
    /////////////////////////////////
    $params['mail_title'] = 'Новый быстрый заказ на товар (сайт "'.$server_name.'")';
    $params['mail_html'] = '<p>Новый быстрый заказ на товар на сайте "'.$server_name.'"</p><hr>';
    /////////////////////////////////

    $res = tools\feedback::send( $params, $arFields );

    if( $res ){

        // Ответ
        echo json_encode([ "status" => "ok" ]);
        return;

    } else {

        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка отправки заявки"));
        return;

    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;
