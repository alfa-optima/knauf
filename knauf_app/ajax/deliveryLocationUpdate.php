<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $user = new project\user();
    if( $USER->IsAuthorized() && $user->isDealer() ){

        $delivLocations = Array();
        parse_str($_POST["form_data"], $delivLocations);

        if(
            is_array($delivLocations)
            &&
            count($delivLocations) > 0
        ){

            foreach ( $delivLocations as $delivLocID => $locInfo ){

                $fields = array();

                if( is_array( $locInfo['pss'] ) && count($locInfo['pss']) > 0 ){
                    $fields['UF_PSS'] = $locInfo['pss'];
                }

                if( count($fields) > 0 ){
                    $delivery_location = new project\delivery_location();
                    $res = $delivery_location->update($delivLocID, $fields);
                }

            }

            // Ответ
            echo json_encode(Array("status" => "ok"));
            return;

        }
    } else {
        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }
}

// Ответ
echo json_encode(Array("status" => "error", "text" => "При сохранении произошла ошибка"));
return;