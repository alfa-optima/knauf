<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$user = new project\user();
if( $USER->IsAuthorized() && $user->isDealer() ){

    $deliv_file = new project\deliv_file($_FILES);
    $deliv_file->upload();

}

