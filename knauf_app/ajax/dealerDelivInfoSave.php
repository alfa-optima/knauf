<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $user = new project\user();
    if( $USER->IsAuthorized() && $user->isDealer() ){

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        $fields = array(
            'UF_DELIV_INFO_TYPE' => strip_tags($arFields['delivery_info_type']),
            'UF_DELIV_INFO_LINK' => strip_tags($arFields['deliv_info_link']),
            'UF_DELIV_FILE_TITLE' => strip_tags($arFields['deliv_info_file_title']),
        );

        $user = new \CUser;
        $res = $user->Update($USER->GetID(), $fields);

        if( $res ){

            ob_start();
                $APPLICATION->IncludeComponent(
                    "aoptima:personalDeliveryLocations", ""
                );
                $html = ob_get_contents();
            ob_end_clean();

            // Ответ
            echo json_encode(Array("status" => "ok", "html" => $html));
            return;

        }

    } else {

        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка сохранения"));
return;