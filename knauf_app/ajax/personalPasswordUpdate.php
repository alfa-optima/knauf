<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    if( $USER->IsAuthorized() ){

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        $curPassword = strip_tags($arFields["CURRENT_PASSWORD"]);
        if( !tools\user::comparePassword($USER->GetID(), $curPassword) ){
            // Ответ
            echo json_encode(Array("status" => "error", "text" => "Введён не верный текущий пароль!"));
            return;
        }

        // поля для сохранения
        $arUserFields = Array(
            "PASSWORD" => strip_tags($arFields["PASSWORD"]),
            "CONFIRM_PASSWORD" => strip_tags($arFields["CONFIRM_PASSWORD"])
        );

        // Сохраняем
        $obUser = new \CUser;
        $res = $obUser->Update($USER->GetID(), $arUserFields);
        if( $res ){
            // Ответ
            echo json_encode(Array("status" => "ok"));
            return;
        } else {
            tools\logger::addError('Ошибка сохранения пароля [user_id='.$USER->GetID().']:'.$obUser->LAST_ERROR);
        }

    } else {
        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка сохранения данных"));
return;