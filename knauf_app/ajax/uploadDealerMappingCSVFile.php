<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$user = new project\user();
if( $USER->IsAuthorized() && $user->isDealer() ){

    $arResult['ERRORS'] = array();

    if( intval($_FILES['csv']['size']) > 0 ){

        $file = new project\dealer_mapping_csv_file($_FILES, $USER->GetID());
        $uploadResult = $file->upload();

        if( $uploadResult['status'] == 'ok' ){

            $diff_columns = array();

            $lines = file($file->getFilePath());
            foreach ( $lines as $key => $value ){
                $value = explode(';', mb_convert_encoding($value, 'UTF-8', 'windows-1251'));
                $value = str_replace(array("\n", "\r"), "", $value);
                $diff_columns[count($value)]++;
                $lines[$key] = $value;
            }
            
            // Если в CSV имеются строки с разным количеством столбцов
            if( count($diff_columns) > 1 ){
                $arResult['ERRORS'][] = 'Ошибка формата: в CSV-файле имеются строки с разным количеством столбцов';
            } else if( count($diff_columns) == 1 ){
                $titlesLine = $lines[0];
                foreach ( $titlesLine as $key => $title ){
                    $titlesLine[$key] = trim(strtoupper($title));
                }
                //unset($lines[0]);
                $total_lines_cnt = count($lines);
                // Отсутствие заголовков
                if(
                    !in_array('ARTICLE', $titlesLine)
                    ||
                    !in_array('KNAUF_ID', $titlesLine)
                ){
                    $arResult['ERRORS'][] = 'Ошибка формата: первая строка не содержит обязательных заголовков (article, knauf_id)';
                }
            }

            if( count($arResult['ERRORS']) > 0 ){

                echo '<script type="text/javascript">window.parent.uploadMappingCSVResponse(\'{"status": "error", "text": "'.implode('<br>', $arResult['ERRORS']).'"}\');</script>';

            } else {

                $user = new \CUser();
                $res = $user->Update( $USER->GetID(), [
                    'UF_ARTICLE_MAPPING' => json_encode( $lines )
                ]);

                if( $res ){

                    ob_start();
                        $APPLICATION->IncludeComponent("aoptima:dealer_load_mapping_file_csv", "");
                        $html = ob_get_contents();
                    ob_end_clean();

                    echo '<script type="text/javascript">window.parent.uploadMappingCSVResponse(\'{"status": "ok", "html":"mybase64'.base64_encode($html).'"}\');';
                    echo '</script>';

                } else {

                    echo '<script type="text/javascript">window.parent.uploadMappingCSVResponse(\'{"status": "error", "text": "Ошибка сохранения"}\');</script>';

                }
            }

            unlink( $file->getFilePath() );

        } else if( $uploadResult['status'] == 'error' ){

            $arResult['ERRORS'][] = $uploadResult['text'];

            echo '<script type="text/javascript">window.parent.uploadMappingCSVResponse(\'{"status": "error", "text": "'.implode('<br>', $arResult['ERRORS']).'"}\');</script>';

        }

    }

}

