<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
    &&
    intval($_POST['id']) > 0
){

    $user = new project\user();
    if( $USER->IsAuthorized() && $user->isDealer() ){

        $obTemplate = new project\dealer_shop();
        $res = $obTemplate->delete($_POST['id']);
        if( $res ){

            $dealer_shop = new project\dealer_shop();
            $shops = $dealer_shop->getList( $USER->GetID() );

            ob_start();
                foreach( $shops as $shop_id => $shop ){
                    $shop = tools\el::info($shop_id);
                    if( intval($shop['PROPERTY_LOC_ID_VALUE']) > 0 ){
                        $shop['LOC'] = project\bx_location::getByID($shop['PROPERTY_LOC_ID_VALUE']);
                    }
                    if( intval($shop['PROPERTY_REGION_ID_VALUE']) > 0 ){
                        $shop['REGION'] = project\bx_location::getByID($shop['PROPERTY_REGION_ID_VALUE']);
                    }
                    $APPLICATION->IncludeComponent(
                        "aoptima:dealerLKShopItem", "",
                        array(
                            'SHOP_ID' => $shop_id,
                            'SHOP' => $shop,
                            'IS_AJAX' => 'Y'
                        )
                    );
                }
                $html = ob_get_contents();
            ob_end_clean();

            $html .= '<script src="'.SITE_TEMPLATE_PATH.'/js/default_scripts.js"></script>';

            // Ответ
            echo json_encode(Array("status" => "ok", "html" => $html));
            return;

        }

    } else {

        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка удаления"));
return;