<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    if( $USER->IsAuthorized() ){

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        $arUser = tools\user::info($USER->GetID());
        $user_groups = \CUser::GetUserGroup($USER->GetID());

        $isSocUser = $arUser['EXTERNAL_AUTH_ID']=='socservices'?'Y':'N';
        $isCallCenterUser =  in_array( project\user::CALL_CENTER_GROUP_ID, $user_groups )?'Y':'N';

        $arUserFields = [
            'UF_DEFAULT_ADDRESS' => json_encode([ 'ADDRESS' => '', 'COORDS' => '' ]),
            'UF_PODYEZD_DEFAULT' => strip_tags($arFields['UF_PODYEZD_DEFAULT']),
            'UF_FLOOR_DEFAULT' => strip_tags($arFields['UF_FLOOR_DEFAULT']),
            'UF_KV_DEFAULT' => strip_tags($arFields['UF_KV_DEFAULT']),
            'UF_LIFT_DEFAULT' => strip_tags($arFields['UF_LIFT_DEFAULT']),
        ];

        if(
            isset($arFields['address']) && strlen($arFields['address']) > 0
            &&
            isset($arFields['address_coords']) && strlen($arFields['address_coords']) > 0
        ){
            $arUserFields['UF_DEFAULT_ADDRESS'] = json_encode([
                'ADDRESS' => $arFields['address'],
                'COORDS' => $arFields['address_coords']
            ]);
        }

        // Сохраняем
        $obUser = new \CUser;
        $res = $obUser->Update($USER->GetID(), $arUserFields);
        if( $res ){
            // Ответ
            echo json_encode(Array("status" => "ok"));
            return;
        } else {
            tools\logger::addError('Ошибка сохранения личных данных [user_id='.$USER->GetID().']:'.$obUser->LAST_ERROR);
        }

    } else {
        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка сохранения данных"));
return;