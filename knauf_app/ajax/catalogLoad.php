<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $user = new project\user();

    $sort1 = 'PROPERTY_HAS_OFFERS';   $order1 = 'DESC';
    $sort2 = 'SORT';   $order2 = 'ASC';

    $section = tools\section::info($_POST['section_id']);
    $smart_filter_path = '';
    $url = strip_tags(trim($_POST['url']));
    if( substr_count( $url, '/filter/' ) ){
        if(preg_match("/\/filter\/(.+)\/apply\//", $url,$matches)){
            $smart_filter_path = rawurldecode( $matches[1] );
        }
    }
    ob_start();
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.smart.filter", "catalog_filter",
            array(
                "IBLOCK_TYPE" => 'catalog',
                "IBLOCK_ID" => project\catalog::catIblockID(),
                "SECTION_ID" => $section['ID'],
                "FILTER_NAME" => project\catalog::FILTER_NAME.'_load',
                "PRICE_CODE" => [ 'BASE' ],
                "CACHE_TYPE" => 'A',
                "CACHE_TIME" => 3600,
                "CACHE_GROUPS" => 'Y',
                "SAVE_IN_SESSION" => "N",
                "FILTER_VIEW_MODE" => 'VERTICAL',
                "XML_EXPORT" => "N",
                "SECTION_TITLE" => "NAME",
                "SECTION_DESCRIPTION" => "DESCRIPTION",
                'HIDE_NOT_AVAILABLE' => 'N',
                "TEMPLATE_THEME" => "",
                'CONVERT_CURRENCY' => 'N',
                'CURRENCY_ID' => '',
                "SEF_MODE" => 'Y',
                "SEF_RULE" => '/catalog/#SECTION_CODE_PATH#/filter/#SMART_FILTER_PATH#/apply/',
                "SMART_FILTER_PATH" => $smart_filter_path,
                "PAGER_PARAMS_NAME" => '',
                "INSTANT_RELOAD" => '',
            ),
            false, array('HIDE_ICONS' => 'Y')
        );
    ob_end_clean();

    if( is_array($_POST['search_results']) ){
        $GLOBALS[project\catalog::FILTER_NAME.'_load']['ID'] = $_POST['search_results'];
    }
    $GLOBALS[project\catalog::FILTER_NAME.'_load']['!ID'] = $_POST['items'];
    if( intval($_POST['section_id']) > 0 ){
        $GLOBALS[project\catalog::FILTER_NAME.'_load']['SECTION_ID'] = $_POST['section_id'];
    }
    $GLOBALS[project\catalog::FILTER_NAME.'_load']['INCLUDE_SUBSECTIONS'] = 'Y';

    //echo "<pre>"; print_r( $GLOBALS[project\catalog::FILTER_NAME.'_load'] ); echo "</pre>";

    $q = strip_tags(trim($_POST['q']));
    if( strlen($q) > 0 ){
        // Поиск по элементам инфоблока
        $search_ids = project\search::iblockElementsByQ($q, true);
        if( count($search_ids) > 0 ){
            $GLOBALS[project\catalog::FILTER_NAME.'_load']['ID'] = $search_ids;
        } else {
            $search_ids = project\search::iblockElementsByQ($q);
            if( count($search_ids) > 0 ){
                $GLOBALS[project\catalog::FILTER_NAME.'_load']['ID'] = $search_ids;
            } else {
                $GLOBALS[project\catalog::FILTER_NAME.'_load']['ID'] = array(0);
            }
        }
    }

    ob_start();
        $APPLICATION->IncludeComponent( "bitrix:catalog.section", "catalog_goods",
            Array(
                "LOC" => $_SESSION['LOC'],
                'IS_DEALER' => $user->isDealer()?'Y':'N',
                "IS_AJAX" => "Y",
                "IBLOCK_TYPE" => "content",
                "IBLOCK_ID" => project\catalog::catIblockID(),
                "INCLUDE_SUBSECTIONS" => 'Y',
                "SHOW_ALL_WO_SECTION" => "Y",
                "SECTION_USER_FIELDS" => array(),
                "ELEMENT_SORT_FIELD" => $sort1,
                "ELEMENT_SORT_ORDER" => $order1,
                "ELEMENT_SORT_FIELD2" => $sort2,
                "ELEMENT_SORT_ORDER2" => $order2,
                "FILTER_NAME" => project\catalog::FILTER_NAME.'_load',
                "HIDE_NOT_AVAILABLE" => "N",
                "PAGE_ELEMENT_COUNT" => project\catalog::GOODS_CNT + 1,
                "LINE_ELEMENT_COUNT" => "3",
                "PROPERTY_CODE" => array("ARTICLE","MOD","MODS_ARTICLE","ED","VIEW","EDGE_VIEW","VIDEO","RESIST_WATER","DRYING_TIME","FLAMMABILITY_GROUP","BASE_MATERIAL","RESIST_COLD","FLOOR_COATING","TARGET","MIXTURE_BASE","PERFORATION","ROOM","POP","PREIM","PRIM","BUY_WITH","SEE_WITH","PROPERTIES","APPL_METHOD","TECH","TYPE","TYPE_OBJECT","TYPE_OF_APPLICATION","TYPE_PROFILE","TYPE_COATING","LAYER_THICKNESS","FASOVKA","TILE","COLOR","WEIGHT","HEIGHT","LENGTH","DEPTH","WIDTH","CROSS","CALC_WEIGHT","MAX_GABARIT","HAS_OFFERS"),
                "OFFERS_LIMIT" => "0",
                "OFFERS_PROPERTY_CODE" => ['LOCATION'],
                "TEMPLATE_THEME" => "",
                "PRODUCT_SUBSCRIPTION" => "N",
                "SHOW_DISCOUNT_PERCENT" => "N",
                "SHOW_OLD_PRICE" => "N",
                "MESS_BTN_BUY" => "Купить",
                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                "MESS_BTN_DETAIL" => "Подробнее",
                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                "SECTION_URL" => "",
                "DETAIL_URL" => "",
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_GROUPS" => "Y",
                "SET_META_KEYWORDS" => "N",
                "META_KEYWORDS" => "",
                "SET_META_DESCRIPTION" => "N",
                "META_DESCRIPTION" => "",
                "BROWSER_TITLE" => "-",
                "ADD_SECTIONS_CHAIN" => "N",
                "DISPLAY_COMPARE" => "N",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "CACHE_FILTER" => "Y",
                "PRICE_CODE" => array('BASE'),
                "USE_PRICE_COUNT" => "N",
                "SHOW_PRICE_COUNT" => "1",
                "PRICE_VAT_INCLUDE" => "Y",
                "CONVERT_CURRENCY" => "N",
                "BASKET_URL" => "/personal/basket.php",
                "ACTION_VARIABLE" => "action",
                "PRODUCT_ID_VARIABLE" => "id",
                "USE_PRODUCT_QUANTITY" => "N",
                "ADD_PROPERTIES_TO_BASKET" => "Y",
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                "PRODUCT_PROPERTIES" => "",
                "PAGER_TEMPLATE" => "",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Товары",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "ADD_PICT_PROP" => "-",
                "LABEL_PROP" => "-",
                'INCLUDE_SUBSECTIONS' => "Y",
                'SHOW_ALL_WO_SECTION' => "Y"
            )
        );

        $html = ob_get_contents();
    ob_end_clean();

    // Ответ
    echo json_encode(Array("status" => "ok", "html" => $html));
    return;

}