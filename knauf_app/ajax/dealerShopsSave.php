<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $user = new project\user();
    if( $USER->IsAuthorized() && $user->isDealer() ){

        $form_data = Array();
        parse_str($_POST["form_data"], $form_data);

        if(
            is_array($form_data['shops'])
            &&
            count($form_data['shops']) > 0
        ){

            $dealer_shop = new project\dealer_shop();
            $dealer_shops = $dealer_shop->getList( $USER->GetID() );

            foreach( $form_data['shops'] as $shop_id => $shop ){
                if( $shop_id == 'new' ){

                    // Новая точка продаж
                    $result = $dealer_shop->add( $shop );
                    if( !$result ){
                        // Ответ
                        echo json_encode(Array("status" => "error", "text" => "Ошибка сохранения"));
                        return;
                    }

                } else if( intval($shop_id) > 0 ){

                    if( in_array( $shop_id, array_keys($dealer_shops) ) ){

                        // Изменения в точку продаж
                        $dealer_shop->update( $shop_id, $shop );

                    } else {
                        // Ответ
                        echo json_encode(Array("status" => "error", "text" => "Ошибка доступа к точке продаж"));
                        return;
                    }

                }
            }

            ob_start();
                $APPLICATION->IncludeComponent(
                    "aoptima:dealerLKShops", ""
                );
                $html = ob_get_contents();
            ob_end_clean();

            $html .= '<script src="'.SITE_TEMPLATE_PATH.'/js/default_scripts.js"></script>';

            // Ответ
            echo json_encode(Array("status" => "ok", "html" => $html));
            return;

        }
    } else {
        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }
}

// Ответ
echo json_encode(Array("status" => "error", "text" => "При сохранении произошла ошибка"));
return;