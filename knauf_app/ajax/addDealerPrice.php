<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('iblock');

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $user = new project\user();

    if( $USER->IsAuthorized() && $user->isDealer() ){

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        $dealer_id = $USER->GetID();
        $product_id = strip_tags($arFields['product_id']);
        $product = tools\el::info($product_id);

        if(
            intval($product_id) > 0
            &&
            is_array($arFields['offers'])
            &&
            count($arFields['offers']) > 0
        ){

            $error_locs = [];
            $errors = [];
            $success_locs = [];

            $locs = array();
            foreach ( $arFields['offers'] as $key => $offer ){
                $locs[$offer['loc_id']]++;
                $offer['price'] = trim(strip_tags($offer['price']));
                $offer['price'] = str_replace(" ", "", $offer['price']);
                $offer['price'] = str_replace(",", ".", $offer['price']);
                $arFields['offers'][$key]['price'] = trim(strip_tags($offer['price']));
            }

            foreach ( $locs as $cnt ){
                if( $cnt > 1 ){
                    // Ответ
                    echo json_encode([
                        "status" => "error",
                        "text" => "Имеются повторы местоположений"
                    ]);
                    return;
                }
            }

            foreach ( $arFields['offers'] as $key => $offer ){
                if( !( $offer['price'] > 0 ) ){
                    // Ответ
                    echo json_encode(["status" => "error", "text" => "Имеются нулевые цены"]);
                    return;
                }
            }

            foreach ( $arFields['offers'] as $key => $offer ){

                if( !isset($offer['quantity']) || is_null($offer['quantity']) ){
                    $offer['quantity'] = 0;
                }

                if(
                    intval($offer['loc_id']) > 0
                    &&
                    strlen($offer['price']) > 0
                    &&
                    intval($offer['quantity']) >= 0
                ){

                    // Инфо о местоположении
                    $loc = project\bx_location::getByID($offer['loc_id']);

                    if(
                        intval($loc['ID']) > 0
                        &&
                        $loc['TYPE_CODE'] == 'REGION'
                    ){

                        // Поиск ценового предложения
                        $filter = [
                        	"IBLOCK_CODE" => project\catalog::TP_IBLOCK_CODE,
                        	"ACTIVE" => "Y",
                        	"PROPERTY_LOCATION" => $loc['ID'],
                            "PROPERTY_DEALER" => $dealer_id,
                            "PROPERTY_CML2_LINK" => $product_id,
                        ];
                        $fields = [ "ID", "PROPERTY_LOCATION", "PROPERTY_DEALER", "PROPERTY_CML2_LINK", "CATALOG_GROUP_".project\catalog::PRICE_ID ];

                        $dbElements = \CIBlockElement::GetList(
                        	["SORT" => "ASC"], $filter, false, ["nTopCount" => 1], $fields
                        );

                        // Если найдено
                        if ($sku = $dbElements->GetNext()){
                            // Обновляем существующее ТП

                            // Сохр. "Под заказ"
                            $set_prop = array(
                                "POD_ZAKAZ" => $offer['pod_zakaz']=='Y'?1:0
                            );
                            \CIBlockElement::SetPropertyValuesEx(
                                $sku['ID'],
                                project\catalog::TP_IBLOCK_CODE,
                                $set_prop
                            );

                            // Сохр. количества на складе
                            $fields = array(
                                'QUANTITY' => intval(strip_tags($offer['quantity']))
                            );
                            $res = \CCatalogProduct::Update($sku['ID'], $fields);
                            if( $res ){

                                // Сохр. цены
                                $fields = Array(
                                    "PRICE" => strip_tags($offer['price']),
                                );
                                $res = \CPrice::Update($sku["CATALOG_PRICE_ID_".project\catalog::PRICE_ID], $fields);
                                if( $res ){

                                    BXClearCache(true, "/el_info/".$sku['ID']."/");

                                    $success_locs[$loc['ID']] = $loc['NAME_RU'];

                                } else {
                                    tools\logger::addError('Ошибка изменения цены для ТП дилера');
                                    $error_locs[$loc['ID']] = $loc['NAME_RU'];
                                }

                            } else {
                                tools\logger::addError('Ошибка изменения количества товара');
                                $error_locs[$loc['ID']] = $loc['NAME_RU'];
                            }

                        // Если такого ТП ещё нет
                        } else {

                            // Создаём новое ТП
                            $PROP[1] = $product['ID'];
                            $PROP[2] = $dealer_id;
                            $PROP[16] = $loc['ID'];
                            $PROP[19] = $offer['pod_zakaz']=='Y'?1:0;

                            $sku_name = $product['NAME'].' (D='.$dealer_id.' L='.$loc['ID'].')';

                            // Создаём элемент инфоблока
                            $element = new \CIBlockElement;
                            $fields = Array(
                                "IBLOCK_ID"				=> project\catalog::getIblockID( project\catalog::TP_IBLOCK_CODE ),
                                "PROPERTY_VALUES"		=> $PROP,
                                "NAME"					=> $sku_name,
                                "ACTIVE"				=> "Y"
                            );
                            if( $sku_id = $element->Add($fields) ){

                                // Создаём товар CCatalogProduct
                                $fields = array(
                                    "ID" => $sku_id,
                                    "VAT_ID" => project\catalog::NDS_ID,
                                    "VAT_INCLUDED" => "Y",
                                    "QUANTITY" => intval(strip_tags($offer['quantity']))
                                );
                                if( $res = \CCatalogProduct::Add($fields) ){

                                    // Создаём цену
                                    $fields = Array(
                                        "PRODUCT_ID" => $sku_id,
                                        "CATALOG_GROUP_ID" => project\catalog::PRICE_ID,
                                        "PRICE" => strip_tags($offer['price']),
                                        "CURRENCY" => "RUB",
                                    );

                                    $price_id = \CPrice::Add($fields);
                                    if( intval($price_id) > 0 ){

                                        BXClearCache(true, "/el_info/".$sku_id."/");

                                        $success_locs[$loc['ID']] = $loc['NAME_RU'];

                                    } else {

                                        \CCatalogProduct::Delete($sku_id);
                                        \CIBlockElement::Delete($sku_id);

                                        tools\logger::addError('Ошибка создания цены для ценового предложения дилера');
                                        $error_locs[$loc['ID']] = $loc['NAME_RU'];
                                    }

                                } else {

                                    \CIBlockElement::Delete($sku_id);

                                    tools\logger::addError('Ошибка создания товара для ценового предложения дилера');
                                    $error_locs[$loc['ID']] = $loc['NAME_RU'];
                                }

                            } else {

                                tools\logger::addError('Ошибка создания элемента инфоблока для ценового предложения дилера- '.$element->LAST_ERROR);
                                $error_locs[$loc['ID']] = $loc['NAME_RU'];
                            }

                        }

                    }
                }
            }
 
        }

        if( count($error_locs) > 0 ){
            // Ответ
            echo json_encode(Array("status" => "error", "text" => "Не удалось добавить/обновить ценовые предложения для следующих местоположений: ".implode(', ', $error_locs)));
            return;
        } else {
            // Ответ
            echo json_encode(["status" => "ok"]);
            return;
        }

    } else {

        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

}


// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка сохранения"));
return;