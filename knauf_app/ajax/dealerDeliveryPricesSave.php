<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $arFields = Array();
    parse_str($_POST["form_data"], $arFields);

    if( intval($arFields['item_id']) > 0 ){

        $item_id = $arFields['item_id'];
        unset($arFields['item_id']);
        
        $distance_ranges = [];

        if(
            is_array($arFields['distance_ranges']['distances'])
            &&
            count($arFields['distance_ranges']['distances']) > 0
        ){

            foreach ( $arFields['distance_ranges']['distances'] as $key => $distance ){
                if(
                    strlen($distance) > 0
                    &&
                    is_array($arFields['distance_ranges']['prices'][$key])
                ){
                    $distance_ranges['distances'][] = strip_tags($distance);
                    $distance_ranges['prices'][] = $arFields['distance_ranges']['prices'][$key];

                    array_multisort($distance_ranges['distances'], SORT_ASC, SORT_NUMERIC, $distance_ranges['prices']);

                    $distanceDoubles = [];
                    foreach ( $distance_ranges['distances'] as $key2 => $distance ){
                        $distanceDoubles[$distance]++;
                        if( $distanceDoubles[$distance] > 1 ){
                            unset($distance_ranges['distances'][$key2]);
                            unset($distance_ranges['prices'][$key2]);
                        }
                    }
                }
            }
        }
        $arFields['distance_ranges'] = $distance_ranges;

        if(
            is_array($arFields['distance_ranges']['prices'])
            &&
            count($arFields['distance_ranges']['prices']) > 0
        ){
            foreach ( $arFields['distance_ranges']['prices'] as $key => $arPrice ){
                $prices_cnt = 0;
                foreach ( $arPrice as $code => $price ){
                    if( strlen($price) > 0 ){  $prices_cnt++;  }
                }
                if( $prices_cnt == 0 ){
                    unset($arFields['distance_ranges']['distances'][$key]);
                    unset($arFields['distance_ranges']['prices'][$key]);
                }
            }
        }

        if( $arFields['dop_services_'.$item_id] ){
            $arFields['dop_services'] = $arFields['dop_services_'.$item_id];
            unset($arFields['dop_services_'.$item_id]);
        }

        $delivery_location = new project\delivery_location();
        $res = $delivery_location->Update( $item_id, [
            'UF_DELIV_SETTINGS' => json_encode( $arFields ),
            'UF_MIN_DELIV_DAYS' => $arFields['min_delivery_time']
        ]);

        if( $res ){

            BXClearCache(true, "/dealersDelivLocs/".$USER->GetID()."/");

            // Ответ
            echo json_encode(Array("status" => "ok", "cnt" => count($deliverySettings)));
            return;

        } else {

            // Ответ
            echo json_encode(Array("status" => "error", "text" => "Ошибка сохранения"));
            return;
        }

    } else {

        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка данных"));
        return;
    }
}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;