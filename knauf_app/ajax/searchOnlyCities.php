<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('sale');
use Bitrix\Sale;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


$results = array();


if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $term = strip_tags($_GET['term']);

    $locs_info = project\bx_location::$main_site_locations;

    $locs = \Bitrix\Sale\Location\LocationTable::getList([
        'filter' => [
            'NAME.NAME' => $term."%",
            '=NAME.LANGUAGE_ID' => 'ru',
            "TYPE.CODE" => [ 'CITY' ],
            '=PARENT.NAME.LANGUAGE_ID' => 'ru',
        ],  
        'select' => [
            '*',
            'NAME_RU' => 'NAME.NAME',
            'TYPE_CODE' => 'TYPE.CODE',
            'PARENT_NAME_RU' => 'PARENT.NAME.NAME',
            'PARENT_TYPE_ID' => 'PARENT.TYPE_ID',
            //'ZIP' => 'EXTERNAL.XML_ID'
        ],
        'limit' => 500
    ]);
    while( $loc = $locs->fetch() ){
        $locName = $loc['NAME_RU'].' ('.$loc['PARENT_NAME_RU'].')';
        // Если родитель - это район
        if( $loc['PARENT_TYPE_ID'] == 4 ){
            // Получим его родителя (область)
            $parentLocs = \Bitrix\Sale\Location\LocationTable::getList(array(
                'filter' => array(
                    'ID' => $loc['PARENT_ID'],
                    '=PARENT.NAME.LANGUAGE_ID' => 'ru',
                ),
                'select' => array(
                    '*',
                    'NAME_RU' => 'NAME.NAME',
                    'TYPE_CODE' => 'TYPE.CODE',
                    'PARENT_NAME_RU' => 'PARENT.NAME.NAME',
                ),
                'limit' => 1
            ));
            while( $parentLoc = $parentLocs->fetch() ){
                $locName = $loc['NAME_RU'].' ('.$loc['PARENT_NAME_RU'].', '.$parentLoc['PARENT_NAME_RU'].')';
            }
        }
        $results[$loc['ID']] = [
            'label' => $locName,
            "short_name" => $loc['NAME_RU'],
            "value" => $loc['ID']
        ];
        if( $locs_info[$loc['ID']] ){
            $results[$loc['ID']]['sub_domain'] = $locs_info[$loc['ID']]['sub_domain'];
        }
    }


}



echo json_encode($results);