<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
    &&
    $USER->IsAuthorized()
){

    ob_start();
        // Пустой шаблон
        $APPLICATION->IncludeComponent("aoptima:delivery_template_item", "");
        $html = ob_get_contents();
    ob_end_clean();

    // Ответ
    echo json_encode(
        Array(
            "status" => "ok",
            "empty_template_html" => $html
        )
    );
    return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса формы"));
return;