<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');  use AOptima\Project as project;

$full_address = strip_tags($_POST['full_address']);

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
    &&
    strlen($full_address) > 0
){

    $arResult = [ 'status' => 'ok' ];
    
    $arResults = project\address::getSuggestions($full_address, 1);
    
    if(
        $arResults['suggestions'][0]['data']['geo_lat']
        &&
        $arResults['suggestions'][0]['data']['geo_lon']
    ){

        $arResult['address'] = [];

        $arResult['lat'] = $arResults['suggestions'][0]['data']['geo_lat'];
        $arResult['lng'] = $arResults['suggestions'][0]['data']['geo_lon'];
        $arResult['region'] = $arResults['suggestions'][0]['data']['region_with_type'];
        $arResult['city'] = $arResults['suggestions'][0]['data']['city_with_type'];
        $arResult['street'] = $arResults['suggestions'][0]['data']['street'];
        $arResult['house'] = $arResults['suggestions'][0]['data']['house'];

        if( strlen($arResult['region']) > 0 ){
            $arResult['address'][] = $arResult['region'];
        }
        if( strlen($arResult['city']) > 0 ){
            $arResult['address'][] = $arResult['city'];
        }
        if( strlen($arResult['street']) > 0 ){
            $arResult['address'][] = 'ул.'.$arResult['street'];
        }
        if( strlen($arResult['house']) > 0 ){
            $arResult['address'][] = 'д.'.$arResult['house'];
        }

        $arResult['address'] = implode(', ', $arResult['address']);

    }

    echo json_encode($arResult); return;

}