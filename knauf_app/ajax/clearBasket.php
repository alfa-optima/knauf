<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

//    \Bitrix\Main\Loader::includeModule('sale');
//    \CSaleBasket::DeleteAll(\CSaleBasket::GetBasketUserID());

    // Ответ
    echo json_encode(["status" => "ok"]);
    return;

}

