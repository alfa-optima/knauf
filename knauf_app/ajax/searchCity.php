<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('sale');
use Bitrix\Sale;


$results = array();


if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $term = strip_tags($_GET['term']);

    $locs = \Bitrix\Sale\Location\LocationTable::getList(array(
        'filter' => array(
            'NAME.NAME' => $term."%",
            '=NAME.LANGUAGE_ID' => 'ru',
            "!TYPE.CODE" => array('COUNTRY', 'COUNTRY_DISTRICT', 'REGION', 'SUBREGION', 'STREET'),
            '=PARENT.NAME.LANGUAGE_ID' => 'ru',
        ),
        'select' => array(
            '*',
            'NAME_RU' => 'NAME.NAME',
            'TYPE_CODE' => 'TYPE.CODE',
            'PARENT_NAME_RU' => 'PARENT.NAME.NAME',
            'PARENT_TYPE_ID' => 'PARENT.TYPE_ID',
            //'ZIP' => 'EXTERNAL.XML_ID'
        ),
        'limit' => 500
    ));
    while( $loc = $locs->fetch() ){
        $locName = $loc['NAME_RU'].' ('.$loc['PARENT_NAME_RU'].')';
        // Если родитель - это район
        if( $loc['PARENT_TYPE_ID'] == 4 ){
            // Получим его родителя (область)
            $parentLocs = \Bitrix\Sale\Location\LocationTable::getList(array(
                'filter' => array(
                    'ID' => $loc['PARENT_ID'],
                    '=PARENT.NAME.LANGUAGE_ID' => 'ru',
                ),
                'select' => array(
                    '*',
                    'NAME_RU' => 'NAME.NAME',
                    'TYPE_CODE' => 'TYPE.CODE',
                    'PARENT_NAME_RU' => 'PARENT.NAME.NAME',
                ),
                'limit' => 1
            ));
            while( $parentLoc = $parentLocs->fetch() ){
                $locName = $loc['NAME_RU'].' ('.$loc['PARENT_NAME_RU'].', '.$parentLoc['PARENT_NAME_RU'].')';
            }
        }
        $results[$loc['ID']] = array(
            'label' => $locName,
            "short_name" => $loc['NAME_RU'],
            "value" => $loc['ID']
        );
    }


}



echo json_encode($results);