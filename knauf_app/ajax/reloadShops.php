<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    ob_start();
        $APPLICATION->IncludeComponent(
            "aoptima:shops", "",
            array('IS_LOAD' => 'Y')
        );
        $html = ob_get_contents();
    ob_end_clean();

    $dealer_shop = new project\dealer_shop();
    $shops = $dealer_shop->getShops();

    // Ответ
    echo json_encode([
        "status" => "ok",
        'html' => $html,
        'shops' => $shops[1]
    ]);
    return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;