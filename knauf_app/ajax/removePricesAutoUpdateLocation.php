<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
    &&
    intval($_POST['item_id']) > 0
){

    $user = new project\user();
    if( $USER->IsAuthorized() && $user->isDealer() ){

        $auto_upd_prices_location = new project\auto_upd_prices_location();
        $res = $auto_upd_prices_location->delete($_POST['item_id']);

        if( $res ){

            ob_start();
                $APPLICATION->IncludeComponent(
                    "aoptima:personalPricesAutoUpdateLocations", ""
                );
                $html = ob_get_contents();
            ob_end_clean();

            // Ответ
            echo json_encode(Array("status" => "ok", "html" => $html));
            return;

        }

    } else {

        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка удаления местоположения доставки"));
return;