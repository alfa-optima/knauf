<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$user = new project\user();
if( $USER->IsAuthorized() && $user->isDealer() ){

    $persPhoto = new project\pers_photo($_FILES);
    $persPhoto->upload();

}

