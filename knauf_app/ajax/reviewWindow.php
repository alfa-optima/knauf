<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
    &&
    intval($_POST['item_id']) > 0
){

    if( $USER->IsAuthorized() ){

        $el = tools\el::info($_POST['item_id']);
        if( intval($el['ID']) > 0 ){

            $vote = new project\vote();
            // сначала подсчитаем оценки пользователя по данному товару
            $user_votes = $vote->getList( $el['ID'], $USER->GetID(), 1 );

            // Ответ
            echo json_encode(Array(
                "status" => "ok",
                "el" => $el,
                "prevVote" => $user_votes[0]['UF_VOTE']?$user_votes[0]['UF_VOTE']:'N'
            ));
            return;
        }

    } else {

        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка открытия формы"));
return;