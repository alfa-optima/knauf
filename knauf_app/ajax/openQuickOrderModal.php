<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $user = new project\user();

    if( $user->isDealer() ){

        // Ответ
        echo json_encode(["status" => "error", 'text' => 'Дилер не может оформлять заказы!']);
        return;

    } else {

        ob_start();
        // quickOrderModalForm
        $APPLICATION->IncludeComponent(
            "aoptima:quickOrderModalForm", "",
            array()
        );
        $html = ob_get_contents();
        ob_end_clean();

    }


	// Ответ
	echo json_encode(["status" => "ok", 'html' => $html]);
	return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;
