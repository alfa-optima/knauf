<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/classes/general/xml.php');

//ini_set('display_errors', 'On');
//error_reporting(E_ALL);

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('iblock');

if( strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ){
    
    $user = new project\user();
    if( $USER->IsAuthorized() && $user->isDealer() ){

        $file_path = $_SESSION['YML_FILE_PATH'];
        if( !$file_path || !file_exists($file_path) ){
            echo json_encode(Array("status" => "error", "text" => "файл не загружен"));  return;
        }

        $errors = array();

        // Шаг загрузки (сек)
        $stepTime = 5;

        $startTime = microtime(true);

        $xml_content = file_get_contents($file_path);

        mb_detect_order("ASCII, UTF-8");
        $cod = mb_detect_encoding($xml_content, array('cp1251', 'utf-8'));

        if( $cod != 'UTF-8' ){
            //$xml_content = mb_convert_encoding($xml_content, 'UTF-8', $cod);
            $xml_content = iconv('cp1251', 'UTF-8', $xml_content);
        }
        $xml_content = html_entity_decode($xml_content);

        $total_offers_cnt = 0;
        
        $xml = new \CDataXML();
        $xml->LoadString($xml_content);
        // Получаем категории
        if ($node = $xml->SelectNodes('/yml_catalog/shop/offers')){

            $offers = $node->children();
            $total_offers_cnt = count($offers);
            
            // Отсеваем некомплектные позиции
            foreach ( $offers as $offer_key => $offer ){

                $offer_id = $offer->getAttribute('id');
                $knauf_id = null;   $client_article = null;
                $price = null;   $quantity = null;
                $custom_price = null;   $custom_quantity = null;

                $fields = $offer->children();
                foreach ( $fields as $field ){
                    if( $field->name == 'knauf_id' && strlen($field->content) > 0 ){
                        $knauf_id = trim(strip_tags($field->content));
                        $knauf_id = project\catalog::treatKnaufID( $knauf_id );
                    }
                    if( $field->name == 'article' && strlen($field->content) > 0 ){
                        $client_article = trim(strip_tags($field->content));
                    } else if( $field->name == 'vendorCode' && strlen($field->content) > 0 ){
                        $client_article = trim(strip_tags($field->content));
                    }
                    if( $field->name == 'price' && strlen($field->content) > 0 ){
                        $price = project\catalog::treatPrice( $field->content );
                    }
                    if( $field->name == 'quantity' && strlen($field->content) > 0 ){
                        $quantity = project\catalog::treatQuantity( $field->content );
                    }
                    if( $field->name == 'custom_price' && strlen($field->content) > 0 ){
                        $custom_price = project\catalog::treatPrice( $field->content );
                    }
                    if( $field->name == 'custom_quantity' && strlen($field->content) > 0 ){
                        $custom_quantity = project\catalog::treatQuantity( $field->content );
                    }
                }

                if(
                    strlen($offer_id) > 0
                    &&
                    (
                        strlen($knauf_id) > 0
                        ||
                        strlen($client_article) > 0
                    )
                    &&
                    (
                        (strlen($price) > 0 && $price > 0)
                        ||
                        (
                            strlen($custom_price) > 0 && $custom_price > 0
                            &&
                            strlen($custom_quantity) > 0 && $custom_quantity > 0
                        )
                    )
                ){} else {
                    unset($offers[$offer_key]);
                }
            }

            if( count($offers) > 0 ){

                if( count($_SESSION['CHECKED_OFFERS']) == 0 ){
                    // Перебираем оставшиеся позиции
                    foreach ( $offers as $offer_key => $offer ){
                        $offer_id = $offer->getAttribute('id');
                        // Сначала по умолчанию добавим в незагруженные
                        $_SESSION['ERROR_OFFERS'][$offer_id] = $offer_id;
                    }
                }

                // Перебираем оставшиеся позиции
                foreach ( $offers as $offer_key => $offer ){

                    if( in_array($offer_key, $_SESSION['CHECKED_OFFERS']) ){   continue;   }

                    $offer_id = $offer->getAttribute('id');
                    $knauf_id = null;   $client_article = null;
                    $price = null;   $quantity = null;
                    $custom_price = null;   $custom_quantity = null;

                    $fields = $offer->children();
                    foreach ( $fields as $field ){
                        if( $field->name == 'knauf_id' && strlen($field->content) > 0 ){
                            $knauf_id = trim(strip_tags($field->content));
                            $knauf_id = project\catalog::treatKnaufID( $knauf_id );
                        }
                        if( $field->name == 'article' && strlen($field->content) > 0 ){
                            $client_article = trim(strip_tags($field->content));
                        } else if( $field->name == 'vendorCode' && strlen($field->content) > 0 ){
                            $client_article = trim(strip_tags($field->content));
                        }
                        if( $field->name == 'price' && strlen($field->content) > 0 ){
                            $price = project\catalog::treatPrice( $field->content );
                        }
                        if( $field->name == 'quantity' && strlen($field->content) > 0 ){
                            $quantity = project\catalog::treatQuantity( $field->content );
                        }
                        if( $field->name == 'custom_price' && strlen($field->content) > 0 ){
                            $custom_price = project\catalog::treatPrice( $field->content );
                        }
                        if( $field->name == 'custom_quantity' && strlen($field->content) > 0 ){
                            $custom_quantity = project\catalog::treatQuantity( $field->content );
                        }
                    }

                    // Найдём товар с таким артикулом
                    $product_id = false;   $product = false;

                    // Если не указан knauf_id, но указан артикул,
                    // то пробуем по файлу соответствия определить knauf_id
                    if( !$knauf_id && $client_article ){
                        $knauf_id = project\dealer_mapping_csv_file::getKnaufID( $USER->GetID(), $client_article );
                        $knauf_id = project\catalog::treatKnaufID( $knauf_id );
                    }

                    if( $knauf_id ){
                        $filter = [
                            "IBLOCK_CODE" => project\catalog::CATALOG_IBLOCK_CODE,
                            "=PROPERTY_ARTICLE" => trim($knauf_id)
                        ];
                        $fields = [ "ID", "NAME", "PROPERTY_ARTICLE", "PROPERTY_PRODUCT_TYPE", 'PROPERTY_LENGTH', 'PROPERTY_WIDTH' ];
                        $products = \CIBlockElement::GetList(
                            ["SORT"=>"ASC"], $filter, false, ["nTopCount"=>1], $fields
                        );
                        if ( $prod = $products->GetNext() ){
                            $product_id = $prod['ID'];
                            $product = $prod;
                        }
                    }

                    // Если найден товар с таким артикулом
                    if( intval($product_id) > 0 ){

                        if( !( strlen($price) > 0 && $price > 0 ) ){
                            $price = project\catalog::getPriceFromCustomPrice(
                                $product,
                                $custom_price
                            );
                        }
                        if( !( strlen($quantity) > 0 ) ){
                            $quantity = project\catalog::getQuantityFromCustomQuantity(
                                $product,
                                $custom_quantity
                            );
                        }

                        if( strlen($price) > 0 && $price > 0 ){

                            // Поиск ценового предложения
                            $filter = Array(
                                "IBLOCK_CODE" => project\catalog::TP_IBLOCK_CODE,
                                "ACTIVE" => "Y",
                                "PROPERTY_LOCATION" => $_SESSION['UPLOAD_FILE_LOC_ID'],
                                "PROPERTY_DEALER" => $USER->GetID(),
                                "PROPERTY_CML2_LINK" => $product_id,
                            );
                            $fields = Array(
                                "ID", "PROPERTY_LOCATION", "PROPERTY_DEALER",
                                "PROPERTY_CML2_LINK", "CATALOG_GROUP_".project\catalog::PRICE_ID
                            );

                            $dbElements = \CIBlockElement::GetList(
                                array("SORT" => "ASC"), $filter, false, array("nTopCount" => 1), $fields
                            );

                            // Если найдено
                            if ($sku = $dbElements->GetNext()){

                                // Обновляем существующее ТП

                                // Сохр. "Под заказ"
                                /*$set_prop = array(
                                    "POD_ZAKAZ" => $offer['pod_zakaz']=='Y'?1:0
                                );
                                \CIBlockElement::SetPropertyValuesEx(
                                    $sku['ID'],
                                    project\catalog::TP_IBLOCK_CODE,
                                    $set_prop
                                );*/

                                // Сохр. количества на складе
                                if( isset($quantity) ){
                                    $fields = [ 'QUANTITY' => $quantity ];
                                    $res = \CCatalogProduct::Update($sku['ID'], $fields);
                                } else {
                                    tools\logger::addError('Загрузка YML: Ошибка изменения количества товара');
                                }

                                // Сохр. цены
                                $fields = [ "PRICE" => $price ];
                                $res = \CPrice::Update($sku["CATALOG_PRICE_ID_".project\catalog::PRICE_ID], $fields);
                                if( $res ){

                                    BXClearCache(true, "/el_info/".$sku['ID']."/");

                                    $_SESSION['SUCCESS_OFFERS_CNT']++;

                                    if( strlen($offer_id) > 0 ){
                                        unset($_SESSION['ERROR_OFFERS'][$offer_id]);
                                    }

                                } else {
                                    tools\logger::addError('Загрузка YML: Ошибка изменения цены для ТП дилера');
                                }

                            // Если такого ТП ещё нет
                            } else {

                                // Создаём новое ТП
                                $PROP[1] = $product_id;
                                $PROP[2] = $USER->GetID();
                                $PROP[16] = $_SESSION['UPLOAD_FILE_LOC_ID'];
                                //$PROP[19] = $offer['pod_zakaz']=='Y'?1:0;

                                $sku_name = $product['NAME'].' (D='.$USER->GetID().' L='.$_SESSION['UPLOAD_FILE_LOC_ID'].')';

                                // Создаём элемент инфоблока
                                $element = new \CIBlockElement;
                                $fields = [
                                    "IBLOCK_ID"				=> project\catalog::getIblockID( project\catalog::TP_IBLOCK_CODE ),
                                    "PROPERTY_VALUES"		=> $PROP,
                                    "NAME"					=> $sku_name,
                                    "ACTIVE"				=> "Y"
                                ];

                                if( $sku_id = $element->Add($fields) ){

                                    // Создаём товар CCatalogProduct
                                    $fields = array(
                                        "ID" => $sku_id,
                                        "VAT_ID" => project\catalog::NDS_ID,
                                        "VAT_INCLUDED" => "Y",
                                        "QUANTITY" => isset($quantity)?$quantity:0
                                    );
                                    if( $res = \CCatalogProduct::Add($fields) ){

                                        // Создаём цену
                                        $fields = [
                                            "PRODUCT_ID" => $sku_id,
                                            "CATALOG_GROUP_ID" => project\catalog::PRICE_ID,
                                            "PRICE" => $price,
                                            "CURRENCY" => "RUB",
                                        ];

                                        $price_id = \CPrice::Add($fields);
                                        if( intval($price_id) > 0 ){

                                            BXClearCache(true, "/el_info/".$sku_id."/");

                                            $_SESSION['SUCCESS_OFFERS_CNT']++;

                                            if( strlen($offer_id) > 0 ){
                                                unset($_SESSION['ERROR_OFFERS'][$offer_id]);
                                            }

                                        } else {

                                            \CCatalogProduct::Delete($sku_id);
                                            \CIBlockElement::Delete($sku_id);

                                            tools\logger::addError('Загрузка YML: Ошибка создания цены для ценового предложения дилера');
                                        }

                                    } else {

                                        \CIBlockElement::Delete($sku_id);

                                        tools\logger::addError('Загрузка YML: Ошибка создания товара для ценового предложения дилера');
                                    }

                                } else {

                                    tools\logger::addError('Загрузка YML: Ошибка создания элемента инфоблока для ценового предложения дилера- '.$element->LAST_ERROR);
                                }
                            }
                        }
                    }

                    $_SESSION['CHECKED_OFFERS'][] = $offer_key;

                    // Превышение времени шага
                    if( microtime(true) - $startTime > $stepTime ){

                        // Ответ
                        $procent = count($offers)==0?100:round(count($_SESSION['CHECKED_OFFERS'])/count($offers)*100, 1);
                        echo json_encode([
                            "status" => count($_SESSION['CHECKED_OFFERS'])==count($offers)?"ok":"progress",
                            'successLinesCnt' => $_SESSION['SUCCESS_OFFERS_CNT'],
                            'checkedLinesCnt' => count($_SESSION['CHECKED_OFFERS']),
                            'totalLinesCnt' => $total_offers_cnt,
                            'procent' => $procent
                        ]); return;

                    }

                }

            }

        }

        if( count($errors) > 0 ){

            // Ответ
            echo json_encode(Array("status" => "error", "text" => implode('<br>', $errors)));
            return;

        } else {



            // Ответ
            $procent = count($offers)==0?100:round(count($_SESSION['CHECKED_OFFERS'])/count($offers)*100, 1);

            echo json_encode(
               [
                    "status" => count($_SESSION['CHECKED_OFFERS'])==count($offers)?"ok":"progress",
                    'successLinesCnt' => $_SESSION['SUCCESS_OFFERS_CNT'],
                    'checkedLinesCnt' => count($_SESSION['CHECKED_OFFERS']),
                    'errorOffers' => $_SESSION['ERROR_OFFERS'],
                    'totalLinesCnt' => $total_offers_cnt,
                    'procent' => $procent
               ]
            );

            if( count($_SESSION['CHECKED_OFFERS']) == count($offers) ){
                unlink($_SESSION['YML_FILE_PATH']);
                $_SESSION['YML_FILE_PATH'] = false;
                $_SESSION['CHECKED_OFFERS'] = array();
                $_SESSION['ERROR_OFFERS'] = array();
                $_SESSION['SUCCESS_OFFERS_CNT'] = 0;
            }

            return;

        }

    } else {
        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;