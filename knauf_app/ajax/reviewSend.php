<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    if( $USER->IsAuthorized() ){

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        if( intval($arFields['tov_id']) > 0 ){

            $el = tools\el::info($arFields['tov_id']);
            if (intval($el['ID']) > 0){

                $vote_stars_cnt = intval(strip_tags($_POST["vote_stars_cnt"]));
                if(
                    $vote_stars_cnt >= 1
                    &&
                    $vote_stars_cnt <= 5
                ){} else {    $vote_stars_cnt = 0;    }

                // Сохранение отзыва
                $review = new project\review();
                $res = $review->add(
                    $el['ID'], $USER->GetID(),
                    $arFields['review'], $vote_stars_cnt
                );
                if( $res ){

                    if( $vote_stars_cnt > 0){
                        $vote = new project\vote();
                        // сначала подсчитаем оценки пользователя по данному товару
                        $user_votes = $vote->getList( $el['ID'], $USER->GetID(), 1 );
                        // Если оценок ещё нет
                        if( count($user_votes) == 0 ){
                            // Добавляем оценку
                            $vote->add($el['ID'], $USER->GetID(), $vote_stars_cnt);
                        } else {
                            // Меняем оценку
                            $vote->reVote($user_votes[0]['ID'], $vote_stars_cnt);
                        }
                    }

                    // Ответ
                    echo json_encode(Array("status" => "ok"));
                    return;
                }

            }

        }

    } else {

        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка отправки отзыва"));
return;