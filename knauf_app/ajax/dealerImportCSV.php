<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('iblock');

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $user = new project\user();
    if( $USER->IsAuthorized() && $user->isDealer() ){

        $file_path = $_SESSION['CSV_FILE_PATH'];
        if( !$file_path || !file_exists($file_path) ){
            echo json_encode(Array("status" => "error", "text" => "файл не загружен"));  return;
        }

        $errors = array();

        // Шаг загрузки (сек)
        $stepTime = 5;

        $startTime = microtime(true);

        $diff_columns = array();

        $lines = file($file_path);
        foreach ( $lines as $key => $value ){
            $value = explode(';', mb_convert_encoding($value, 'UTF-8', 'windows-1251'));
            $value = str_replace(array("\n", "\r"), "", $value);
            $diff_columns[count($value)]++;
            $lines[$key] = $value;
        }

        // Если в CSV имеются строки с разным количеством столбцов
        if( count($diff_columns) > 1 ){

            $errors[] = 'Ошибка формата: в CSV имеются строки с разным количеством столбцов';

        } else if( count($diff_columns) == 1 ){
            
            $titlesLine = $lines[0];
            foreach ( $titlesLine as $key => $title ){
                $titlesLine[$key] = trim(strtoupper($title));
            }
            unset($lines[0]);
            $total_lines_cnt = count($lines);

            // Отсутствие заголовков
            if(
                (
                    !in_array('ARTICLE', $titlesLine)
                    &&
                    !in_array('KNAUF_ID', $titlesLine)
                )
                ||
                !in_array('PRICE', $titlesLine)
                ||
                !in_array('QUANTITY', $titlesLine)
            ){

                $errors[] = 'Ошибка формата: первая строка не содержит обязательных заголовков (ARTICLE или KNAUF_ID, PRICE, QUANTITY)';
                
            } else {

                $knauf_id_key = array_search('KNAUF_ID', $titlesLine);
                $client_article_key = array_search('ARTICLE', $titlesLine);
                $price_key = array_search('PRICE', $titlesLine);
                $quantity_key = array_search('QUANTITY', $titlesLine);
                $custom_price_key = array_search('CUSTOM_PRICE', $titlesLine);
                $custom_quantity_key = array_search('CUSTOM_QUANTITY', $titlesLine);
                
                if( count($lines) > 0 ){

                    // Отсеваем некомплектные строки
                    foreach ( $lines as $line_number => $line ){

                        $_SESSION['ERROR_LINES'][$line_number] = $line_number;

                        $client_article = is_integer($client_article_key)?trim(strip_tags($line[$client_article_key])):null;

                        $knauf_id = is_integer($knauf_id_key)?trim(strip_tags($line[$knauf_id_key])):null;
                        $knauf_id = project\catalog::treatKnaufID( $knauf_id );

                        $quantity = is_integer($quantity_key)?trim(strip_tags($line[$quantity_key])):null;
                        $quantity = project\catalog::treatQuantity( $quantity );

                        $price = is_integer($price_key)?trim(strip_tags($line[$price_key])):null;
                        $price = project\catalog::treatPrice( $price );

                        $custom_price = is_integer($custom_price_key)?trim(strip_tags($line[$custom_price_key])):null;
                        $custom_price = project\catalog::treatPrice( $custom_price );

                        $custom_quantity = is_integer($custom_quantity_key)?trim(strip_tags($line[$custom_quantity_key])):null;
                        $custom_quantity = project\catalog::treatQuantity( $custom_quantity );

                        if(
                            (
                                strlen($knauf_id) > 0
                                ||
                                strlen($client_article) > 0
                            )
                            &&
                            (
                                (strlen($price) > 0 && $price > 0)
                                ||
                                (
                                    strlen($custom_price) > 0 && $custom_price > 0
                                    &&
                                    strlen($custom_quantity) > 0 && $custom_quantity > 0
                                )
                            )
                        ){
                            unset($_SESSION['ERROR_LINES'][$line_number]);
                        } else {
                            unset($lines[$line_number]);
                        }
                    }

                    // Перебираем оставшиеся строки
                    foreach ( $lines as $line_number => $line ){

                        if( in_array($line_number, $_SESSION['CHECKED_LINES']) ){   continue;   }

                        $client_article = is_integer($client_article_key)?trim(strip_tags($line[$client_article_key])):null;

                        $knauf_id = is_integer($knauf_id_key)?trim(strip_tags($line[$knauf_id_key])):null;
                        $knauf_id = project\catalog::treatKnaufID( $knauf_id );

                        $quantity = is_integer($quantity_key)?trim(strip_tags($line[$quantity_key])):null;
                        $quantity = project\catalog::treatQuantity( $quantity, $knauf_id );

                        $price = is_integer($price_key)?trim(strip_tags($line[$price_key])):null;
                        $price = project\catalog::treatPrice( $price );

                        $custom_price = is_integer($custom_price_key)?trim(strip_tags($line[$custom_price_key])):null;
                        $custom_price = project\catalog::treatPrice( $custom_price );

                        $custom_quantity = is_integer($custom_quantity_key)?trim(strip_tags($line[$custom_quantity_key])):null;
                        $custom_quantity = project\catalog::treatQuantity( $custom_quantity );

                        // Найдём товар с таким артикулом
                        $product_id = false;   $product = false;

                        // Если не указан knauf_id, но указан артикул,
                        // то пробуем по файлу соответствия определить knauf_id
                        if( ( !$knauf_id || strlen($knauf_id)==0 ) && strlen($client_article) > 0 ){
                            $knauf_id = project\dealer_mapping_csv_file::getKnaufID( $USER->GetID(), $client_article );
                        }

                        if( strlen($knauf_id) > 0 ){
                            $filter = [
                                "IBLOCK_CODE" => project\catalog::CATALOG_IBLOCK_CODE,
                                "=PROPERTY_ARTICLE" => trim($knauf_id)
                            ];
                            $fields = [ "ID", "NAME", "PROPERTY_ARTICLE", "PROPERTY_PRODUCT_TYPE", 'PROPERTY_LENGTH', 'PROPERTY_WIDTH' ];
                            $products = \CIBlockElement::GetList(
                                array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
                            );
                            if ( $prod = $products->GetNext() ){
                                $product_id = $prod['ID'];
                                $product = $prod;
                            }
                        }

                        // Если найден товар с таким артикулом
                        if( intval($product_id) > 0 ){

                            if( !( strlen($price) > 0 && $price > 0 ) ){
                                $price = project\catalog::getPriceFromCustomPrice(
                                    $product,
                                    $custom_price
                                );
                            }
                            if( !( strlen($quantity) > 0 ) ){
                                $quantity = project\catalog::getQuantityFromCustomQuantity(
                                    $product,
                                    $custom_quantity
                                );
                            }

                            if( strlen($price) > 0 && $price > 0 ){

                                // Поиск ценового предложения
                                $filter = Array(
                                    "IBLOCK_CODE" => project\catalog::TP_IBLOCK_CODE,
                                    "ACTIVE" => "Y",
                                    "PROPERTY_LOCATION" => $_SESSION['UPLOAD_FILE_LOC_ID'],
                                    "PROPERTY_DEALER" => $USER->GetID(),
                                    "PROPERTY_CML2_LINK" => $product_id,
                                );
                                $fields = Array(
                                    "ID", "PROPERTY_LOCATION", "PROPERTY_DEALER",
                                    "PROPERTY_CML2_LINK", "CATALOG_GROUP_".project\catalog::PRICE_ID
                                );

                                $dbElements = \CIBlockElement::GetList(
                                    array("SORT" => "ASC"), $filter, false, array("nTopCount" => 1), $fields
                                );

                                // Если найдено
                                if ($sku = $dbElements->GetNext()){

                                    // Обновляем существующее ТП

                                    // Сохр. "Под заказ"
                                    /*$set_prop = array(
                                        "POD_ZAKAZ" => $offer['pod_zakaz']=='Y'?1:0
                                    );
                                    \CIBlockElement::SetPropertyValuesEx(
                                        $sku['ID'],
                                        project\catalog::TP_IBLOCK_CODE,
                                        $set_prop
                                    );*/

                                    // Сохр. количества на складе
                                    if( isset($quantity) ){
                                        $fields = [ 'QUANTITY' => $quantity ];
                                        $res = \CCatalogProduct::Update($sku['ID'], $fields);
                                        if( !$res ){
                                            tools\logger::addError('Загрузка CSV: Ошибка изменения количества на складе для ТП дилера');
                                        }
                                    }

                                    // Сохр. цены
                                    $fields = [ "PRICE" => $price ];
                                    $res = \CPrice::Update($sku["CATALOG_PRICE_ID_".project\catalog::PRICE_ID], $fields);
                                    if( $res ){

                                        BXClearCache(true, "/el_info/".$sku['ID']."/");

                                        $_SESSION['SUCCESS_LINES_CNT']++;
                                        unset($_SESSION['ERROR_LINES'][$line_number]);

                                    } else {
                                        tools\logger::addError('Загрузка CSV: Ошибка изменения цены для ТП дилера');
                                    }

                                // Если такого ТП ещё нет
                                } else {

                                    // Создаём новое ТП
                                    $PROP[1] = $product_id;
                                    $PROP[2] = $USER->GetID();
                                    $PROP[16] = $_SESSION['UPLOAD_FILE_LOC_ID'];
                                    //$PROP[19] = $offer['pod_zakaz']=='Y'?1:0;

                                    $sku_name = $product['NAME'].' (D='.$USER->GetID().' L='.$_SESSION['UPLOAD_FILE_LOC_ID'].')';

                                    // Создаём элемент инфоблока
                                    $element = new \CIBlockElement;
                                    $fields = Array(
                                        "IBLOCK_ID"				=> project\catalog::getIblockID( project\catalog::TP_IBLOCK_CODE ),
                                        "PROPERTY_VALUES"		=> $PROP,
                                        "NAME"					=> $sku_name,
                                        "ACTIVE"				=> "Y"
                                    );
                                    if( $sku_id = $element->Add($fields) ){

                                        // Создаём товар CCatalogProduct
                                        $fields = array(
                                            "ID" => $sku_id,
                                            "VAT_ID" => project\catalog::NDS_ID,
                                            "VAT_INCLUDED" => "Y",
                                            "QUANTITY" => isset($quantity)?$quantity:0
                                        );

                                        if( $res = \CCatalogProduct::Add($fields) ){

                                            // Создаём цену
                                            $fields = Array(
                                                "PRODUCT_ID" => $sku_id,
                                                "CATALOG_GROUP_ID" => project\catalog::PRICE_ID,
                                                "PRICE" => $price,
                                                "CURRENCY" => "RUB",
                                            );

                                            $price_id = \CPrice::Add($fields);
                                            if( intval($price_id) > 0 ){

                                                BXClearCache(true, "/el_info/".$sku_id."/");

                                                $_SESSION['SUCCESS_LINES_CNT']++;
                                                unset($_SESSION['ERROR_LINES'][$line_number]);

                                            } else {

                                                \CCatalogProduct::Delete($sku_id);
                                                \CIBlockElement::Delete($sku_id);

                                                tools\logger::addError('Загрузка CSV: Ошибка создания цены для ценового предложения дилера');
                                            }

                                        } else {

                                            \CIBlockElement::Delete($sku_id);

                                            tools\logger::addError('Загрузка CSV: Ошибка создания товара для ценового предложения дилера');
                                        }

                                    } else {

                                        tools\logger::addError('Загрузка CSV: Ошибка создания элемента инфоблока для ценового предложения дилера- '.$element->LAST_ERROR);
                                    }
                                }
                            }
                        }

                        $_SESSION['CHECKED_LINES'][] = $line_number;

                        // Превышение времени шага
                        if( microtime(true) - $startTime > $stepTime ){

                            // Ответ
                            $procent = round(count($_SESSION['CHECKED_LINES'])/count($lines)*100, 1);
                            echo json_encode([
                                "status" => count($_SESSION['CHECKED_LINES'])==count($lines)?"ok":"progress",
                                'successLinesCnt' => $_SESSION['SUCCESS_LINES_CNT'],
                                'checkedLinesCnt' => count($_SESSION['CHECKED_LINES']),
                                'totalLinesCnt' => $total_lines_cnt,
                                'procent' => $procent
                            ]); return;

                        }

                    }

                }

            }

        }

        if( count($errors) > 0 ){

            // Ответ
            echo json_encode(["status" => "error", "text" => implode('<br>', $errors)]);
            return;

        } else {

            // Ответ
            $procent = round(count($_SESSION['CHECKED_LINES'])/count($lines)*100, 1);

            echo json_encode([
                "status" => count($_SESSION['CHECKED_LINES'])==count($lines)?"ok":"progress",
                'successLinesCnt' => $_SESSION['SUCCESS_LINES_CNT'],
                'checkedLinesCnt' => count($_SESSION['CHECKED_LINES']),
                'totalLinesCnt' => $total_lines_cnt,
                'errorLines' => $_SESSION['ERROR_LINES'],
                'procent' => $procent
            ]);

            if( count($_SESSION['CHECKED_LINES']) == count($lines) ){
                unlink($_SESSION['CSV_FILE_PATH']);
                $_SESSION['CSV_FILE_PATH'] = false;
                $_SESSION['CHECKED_LINES'] = array();
                $_SESSION['ERROR_LINES'] = array();
                $_SESSION['SUCCESS_LINES_CNT'] = 0;
            }

            return;

        }

    } else {
        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;