<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $user = new project\user();
    if( $USER->IsAuthorized() && $user->isDealer() ){

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        if( !is_array($arFields['pss']) ){
            $arFields['pss'] = array();
        }

        $user = new \CUser;
        $res = $user->Update(
            $USER->GetID(), Array(
                "UF_PSS" => $arFields['pss'],
                "UF_PAY_TYPES" => json_encode($arFields['pay_types']),
            )
        );

        if( $res ){

            // Ответ
            echo json_encode(Array("status" => "ok"));
            return;

        }

    } else {
        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }
}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка сохранения"));
return;