<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('sale');

use Bitrix\Main,
Bitrix\Main\Localization\Loc as Loc,
Bitrix\Main\Loader,
Bitrix\Main\Config\Option,
Bitrix\Sale\Delivery,
Bitrix\Sale\PaySystem,
Bitrix\Sale,
Bitrix\Sale\Basket,
Bitrix\Sale\Order,
Bitrix\Sale\DiscountCouponsManager,
Bitrix\Main\Context;


if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    if( $USER->IsAuthorized() ){

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        if( intval($arFields['order_id']) > 0 ){

            $order = new project\order();
            $userOrders = $order->getList( $USER->GetID(), $arFields['order_id'] );
            if ( $userOrders[$arFields['order_id']] ){

                $obOrder = Sale\Order::load( $arFields['order_id'] );
                $propValues = project\order::getOrderPropValues($obOrder);
                $dealer_id = project\order::getPropValue( $propValues, 'DEALER_ID' );

                if( intval($dealer_id) > 0 ){

                    $vote = intval(strip_tags($_POST["dealer_vote"]));
                    if(
                        $vote >= 1
                        &&
                        $vote <= 5
                    ){} else {    $vote = 0;    }

                    // Создание отзыва
                    $review = new project\review_order();
                    $res = $review->add(
                        $arFields['order_id'],
                        $USER->GetID(),
                        $dealer_id,
                        $arFields['review'],
                        $vote,
                        $arFields['product_quality'],
                        $arFields['shop_quality'],
                        $arFields['delivery_speed']
                    );

                    if( $res ){

                        if( $vote > 0  ){
                            $dealer_vote = new project\vote_dealer();
                            // сначала подсчитаем оценки пользователя по данному товару
                            $user_votes = $dealer_vote->getList( $dealer_id, $USER->GetID(), 1 );
                            // Если оценок ещё нет
                            if( count($user_votes) == 0 ){
                                // Добавляем оценку
                                $dealer_vote->add($dealer_id, $USER->GetID(), $vote);
                            } else {
                                // Меняем оценку
                                $dealer_vote->reVote($user_votes[0]['ID'], $vote);
                            }
                        }

                        // Ответ
                        echo json_encode(Array("status" => "ok"));
                        return;

                    }
                }
            }

        }

    } else {

        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка отправки отзыва"));
return;