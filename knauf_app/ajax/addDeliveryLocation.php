<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('iblock');

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $user = new project\user();
    if( $USER->IsAuthorized() && $user->isDealer() ){

        if( intval($_POST['loc_id']) > 0 ){

            $delivery_location = new project\delivery_location();
            $dealerLocations = $delivery_location->getList( $USER->GetID(), $_POST['loc_id'] );

            if( count($dealerLocations) > 0 ){

                // Ответ
                echo json_encode(Array("status" => "error", "text" => "Вы уже создали данное местоположение ранее"));
                return;

            } else {

                $fields = array(
                    'UF_DEALER' => $USER->GetID(),
                    'UF_LOC_ID' => strip_tags($_POST['loc_id'])
                );
                $loc = project\bx_location::getByID($_POST['loc_id']);
                $fields['UF_LOC_NAME'] = $loc['NAME_RU'];

                $res = $delivery_location->add($fields);

                if( $res ){

                    ob_start();
                    	$APPLICATION->IncludeComponent(
                    	    "aoptima:personalDeliveryLocations", "",
                            array('IS_AJAX' => 'Y')
                        );
                    	$html = ob_get_contents();
                    ob_end_clean();

                    // Ответ
                    echo json_encode(Array("status" => "ok", "html" => $html));
                    return;

                }


            }

        }


    } else {

        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

}


// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка сохранения"));
return;