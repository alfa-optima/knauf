<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $user = new project\user();
    if( $USER->IsAuthorized() && $user->isDealer() ){

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        if( isset($_POST['item_id']) && intval($_POST['item_id']) > 0 ){

            // Проверим ссылку на корректность
            if( isset($arFields['UF_FILE_LINK']) && strlen($arFields['UF_FILE_LINK']) > 0 ){
                $isWriteLink = preg_match("/^ *https?\:\/\/(www\.)?([\da-z\.-]+)(\.([a-z\.]{2,6}))?(\:[\d]+)?.*/", $arFields['UF_FILE_LINK'], $matches, PREG_OFFSET_CAPTURE);
                if( !$isWriteLink ){
                    echo json_encode(Array("status" => "error", "text" => "Некорректный формат ссылки"));
                    return;
                }
            }

            $fields = [
                'UF_FILE_LINK' => isset($arFields['UF_FILE_LINK'])?$arFields['UF_FILE_LINK']:null
            ];

            $auto_upd_prices_location = new project\auto_upd_prices_location();
            $res = $auto_upd_prices_location->update($_POST['item_id'], $fields);

            // Ответ
            echo json_encode(Array("status" => "ok"));
            return;


        }

    } else {
        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }
}

// Ответ
echo json_encode(Array("status" => "error", "text" => "При сохранении произошла ошибка"));
return;