<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$template = "";

$isDealer = 'N';

if( $USER->IsAuthorized() ){
    $user = new project\user( $USER->GetID() );
    if( $user->isDealer() ){
        $template = "dealer";
        $isDealer = 'Y';
    }
}

$APPLICATION->IncludeComponent(
    "aoptima:personal", $template,
    [
        'isDealer' => $isDealer,
        'IS_MOB_APP' => 'Y'
    ]
);