<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');  use AOptima\Tools as tools;
\Bitrix\Main\Loader::includeModule('aoptima.project');  use AOptima\Project as project;
$user = new project\user();

$arURI = tools\funcs::arURI( tools\funcs::pureURL() ); ?>


<? if( $user->isDealer() ){ ?>

    <a class="modalAddGoodsLink  sr-only" href="#modalAddGoods">triggers fancybox</a>
    <div id='modalAddGoods' class="modal-addGoods">
        <div class="modal-addGoods__wrapper">

            <form class="addPriceForm" method="post" onsubmit="return false">

                <div class="modal-addGoods__info">
                    <a class="modal-addGoods__image">
                        <img src="">
                    </a>
                    <div class="modal-addGoods__index">
                        <h5 class="modal-addGoods__title"></h5>
                        <div class="modal-addGoods__table"></div>
                        <div class="modal-addGoods__addPrice dopPriceOfferButton to___process"><span>Ещё цена</span></div>
                    </div>
                </div>

                <p class="error___p"></p>

                <input class="modal-addGoods__button addPriceButton to___process" type="button" value="Добавить">

                <input type="hidden" name="product_id" value="">

            </form>

        </div>
    </div>

<? } ?>



<a class="modal-cities__link  sr-only" href="#modal-cities">triggers fancybox</a>
<div id='modal-cities' class="modal-city  modal-city--cities">
    <a href="javascript:;" onclick="$.fancybox.close();" class="modal-city__close">Закрыть</a>
    <? // selectCityWindow
    $APPLICATION->IncludeComponent(
        "aoptima:selectCityWindow", "",
        array('KNAUF_LOC' => $KNAUF_LOC)
    ); ?>
</div>



<a class="modal-review__link  sr-only" href="#modal-review">triggers fancybox</a>
<div id='modal-review' class="modal-review" style="display:none; z-index: 1000000">
    <div class="modal-review__wrapper">
        <form class="reviewForm" onsubmit="return false">
            <a href="javascript:;" onclick="$.fancybox.close();" class="modal-review__close">Закрыть</a>
            <h2 class="modal-review__title">Отзыв</h2>
            <h3 class="modal-review__subtitle"></h3>
            <div class="modal-review__counter">Оставшиеся символы: <span id="counter">300</span></div>
            <textarea name="review" id="textarea" maxlength="300" class="modal-review__textarea" onkeyup="$(this).val($(this).val().slice(0,299));"></textarea>

            <p class="error___p"></p>

            <div class="modal-review__bottom">

                <div class="modal-review__rating-wrapper">
                    <span>Поставить оценку</span>
                    <div class="modal-review__rating">
                        <ul id='stars'>
                            <li class='star' title='1' data-value='1'>
                                <span></span>
                            </li>
                            <li class='star' title='2' data-value='2'>
                                <span></span>
                            </li>
                            <li class='star' title='3' data-value='3'>
                                <span></span>
                            </li>
                            <li class='star' title='4' data-value='4'>
                                <span></span>
                            </li>
                            <li class='star' title='5' data-value='5'>
                                <span></span>
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="modal-review__rating-wrapper prevVote">
                    <span>Прошлая оценка: нет</span>
                </div>

                <input class="modal-review__button reviewButtonSend to___process" type="button" value="Отправить">

            </div>
            <input type="hidden" name="tov_id">
        </form>
    </div>
</div>





<!--<a class="loadFileWindowLink  sr-only" href="#loadFileWindow">triggers fancybox</a>-->
<!--<div id='loadFileWindow' class="modal-review" style="display:none; z-index: 1000000">-->
<!--    <div class="modal-review__wrapper">-->
<!---->
<!--        <h1 style="text-align: center;">Импорта файла...</h1>-->
<!---->
<!--        <p style="text-align: center; margin-bottom: 20px;">Внимание! Не закрывайте данное окно до окончания процесса!</p>-->
<!---->
<!--        <div class="aoptima_progress_bar_block">-->
<!--            <div class="status___block">-->
<!--                <p class="loading___status">0%</p>-->
<!--            </div>-->
<!--            <div class="aoptima_progress_bar"></div>-->
<!--        </div>-->
<!---->
<!--        <div class="success___block"></div>-->
<!---->
<!--        <p class="error___p" style="text-align: center;"></p>-->
<!---->
<!--    </div>-->
<!--</div>-->





<a class="modal-region__link  sr-only newDealerLocLink" href="#modal-region">triggers fancybox</a>
<div id='modal-region' class="modal-region" style="display:none; z-index: 1000000">
    <div class="modal-region__wrapper">
        <a href="javascript:;" onclick="$.fancybox.close();" class="modal-review__close">Закрыть</a>
        <form class="deliveryLocationAddForm" onsubmit="return false">
            <h2 class="modal-region__title">Добавить местоположение доставки</h2>
            <div class="dealerAccount-form__field  dealerAccount-form__field--w384">
                <span>Местоположение доставки</span>
                <input
                    class="account-form__suggestInput"
                    id="deliveryLocation"
                    type="text"
                    value=""
                    cityName=""
                    onfocus="cityFocus($(this));"
                    onkeyup="cityKeyUp($(this));"
                    onfocusout="cityFocusOut($(this));"
                    autocomplete="off"
                    placeholder="Начните ввод"
                >
                <input class="loc_input" type="hidden" name="loc_id" value="">
                <script>
                $(document).ready(function(){
                    $('#deliveryLocation').autocomplete({
                        source: '/ajax/searchLoc.php',
                        minLength: 2,
                        delay: 700,
                        select: citySelect
                    })
                })
                </script>
            </div>

            <p class="error___p" style="margin-bottom: 30px;"></p>

            <input class="modal-review__button newDeliveryLocationButton to___process" type="button" value="Сохранить">

        </form>
    </div>
</div>



<a class="modal-region__link  sr-only newDealerAutoUpdateLocLink" href="#modal-auto-update-region">triggers fancybox</a>
<div id='modal-auto-update-region' class="modal-region" style="display:none; z-index: 1000000">
    <div class="modal-region__wrapper">
        <a href="javascript:;" onclick="$.fancybox.close();" class="modal-review__close">Закрыть</a>
        <form class="deliveryLocationAddForm" onsubmit="return false">
            <h2 class="modal-region__title">Добавить регион для автообновления цен</h2>
            <div class="dealerAccount-form__field  dealerAccount-form__field--w384">
                <span>Регион для автообновления цен</span>
                <input
                    class="account-form__suggestInput"
                    id="deliveryPricesAutoUpdateLocation"
                    type="text"
                    value=""
                    cityName=""
                    onfocus="cityFocus($(this));"
                    onkeyup="cityKeyUp($(this));"
                    onfocusout="cityFocusOut($(this));"
                    autocomplete="off"
                    placeholder="Начните ввод"
                >
                <input class="loc_input" type="hidden" name="loc_id" value="">
                <script>
                $(document).ready(function(){
                    $('#deliveryPricesAutoUpdateLocation').autocomplete({
                        source: '/ajax/searchRegion.php',
                        minLength: 2,
                        delay: 700,
                        select: regionSelect
                    })
                })
                </script>
            </div>

            <p class="error___p" style="margin-bottom: 30px;"></p>

            <input class="modal-review__button newPricesAutoUpdateLocationButton to___process" type="button" value="Сохранить">

        </form>
    </div>
</div>





<a class="modal-orderReview__link  sr-only" href="#orderReviewWindow">triggers fancybox</a>
<div id='orderReviewWindow' class="modal-orderReview" style="display:none; z-index: 1000000">
    <a href="javascript:;" onclick="$.fancybox.close();" class="modal-review__close">Закрыть</a>
    <form class="orderReviewForm" onsubmit="return false">
        <div class="modal-orderReview__block">

            <h2 class="modal-orderReview__title"></h2>

            <p class="modal-orderReview__text">Пожалуйста, ответьте на&nbsp;три простых вопроса!<br> 1&nbsp;&mdash; очень плохо, 10&nbsp;&mdash; превосходно.</p>

            <div class="dealerAccount-form__field  dealerAccount-form__field--w100">
                <span>Качество товара</span>
                <div class="account-form__checkboxes">
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_1_1" name="product_quality" value="1">
                        <label for="param_1_1">1</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_1_2" name="product_quality" value="2">
                        <label for="param_1_2">2</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_1_3" name="product_quality" value="3">
                        <label for="param_1_3">3</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_1_4" name="product_quality" value="4">
                        <label for="param_1_4">4</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_1_5" name="product_quality" value="5">
                        <label for="param_1_5">5</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_1_6" name="product_quality" value="6">
                        <label for="param_1_6">6</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_1_7" name="product_quality" value="7">
                        <label for="param_1_7">7</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_1_8" name="product_quality" value="8">
                        <label for="param_1_8">8</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_1_9" name="product_quality" value="9">
                        <label for="param_1_9">9</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_1_10" name="product_quality" value="10">
                        <label for="param_1_10">10</label>
                    </div>
                </div>
            </div>

            <div class="dealerAccount-form__field  dealerAccount-form__field--w100">
                <span>Качество работы магазина</span>
                <div class="account-form__checkboxes">
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_2_1" name="shop_quality" value="1">
                        <label for="param_2_1">1</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_2_2" name="shop_quality" value="2">
                        <label for="param_2_2">2</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_2_3" name="shop_quality" value="3">
                        <label for="param_2_3">3</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_2_4" name="shop_quality" value="4">
                        <label for="param_2_4">4</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_2_5" name="shop_quality" value="5">
                        <label for="param_2_5">5</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_2_6" name="shop_quality" value="6">
                        <label for="param_2_6">6</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_2_7" name="shop_quality" value="7">
                        <label for="param_2_7">7</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_2_8" name="shop_quality" value="8">
                        <label for="param_2_8">8</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_2_9" name="shop_quality" value="9">
                        <label for="param_2_9">9</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_2_10" name="shop_quality" value="10">
                        <label for="param_2_10">10</label>
                    </div>
                </div>
            </div>

            <div class="dealerAccount-form__field  dealerAccount-form__field--w100">
                <span>Скорость доставки</span>
                <div class="account-form__checkboxes">
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_3_1" name="delivery_speed" value="1">
                        <label for="param_3_1">1</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_3_2" name="delivery_speed" value="2">
                        <label for="param_3_2">2</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_3_3" name="delivery_speed" value="3">
                        <label for="param_3_3">3</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_3_4" name="delivery_speed" value="4">
                        <label for="param_3_4">4</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_3_5" name="delivery_speed" value="5">
                        <label for="param_3_5">5</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_3_6" name="delivery_speed" value="6">
                        <label for="param_3_6">6</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_3_7" name="delivery_speed" value="7">
                        <label for="param_3_7">7</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_3_8" name="delivery_speed" value="8">
                        <label for="param_3_8">8</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_3_9" name="delivery_speed" value="9">
                        <label for="param_3_9">9</label>
                    </div>
                    <div class="account-form__checkbox">
                        <input type="radio" id="param_3_10" name="delivery_speed" value="10">
                        <label for="param_3_10">10</label>
                    </div>
                </div>
            </div>

        </div>

        <div class="modal-orderReview__block">
            <div class="account-form__field  account-form__field--w100">
                <span>Общая оценка дилера</span>
                <div class="modal-review__rating">
                    <ul id="stars">
                        <li class="star" title="1" data-value="1">
                            <span></span>
                        </li>
                        <li class="star" title="2" data-value="2">
                            <span></span>
                        </li>
                        <li class="star" title="3" data-value="3">
                            <span></span>
                        </li>
                        <li class="star" title="4" data-value="4">
                            <span></span>
                        </li>
                        <li class="star" title="5" data-value="5">
                            <span></span>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="account-form__field  account-form__field--w100">
                <span>Текст отзыва</span>
                <div class="account-form__textarea-counter">Оставшиеся символы: <span>300</span></div>
                <textarea name="review" maxlength="300" placeholder="Текст отзыва"></textarea>
            </div>

            <p class="error___p"></p>

            <input class="modal-review__button orderReviewButtonSend to___process" type="button" value="Отправить">
        </div>

        <input type="hidden" name="dealer_id">
        <input type="hidden" name="order_id">

    </form>
</div>




<div class="cookie_message" style="display: none;">
    <a href="#" class="box_closer" onclick="return false;"></a>
    <div class="cookie_message_content">
        <p>Этот сайт использует файлы cookie, чтобы предоставить Вам лучший пользовательский опыт. Продолжая просматривать сайт, Вы соглашаетесь с нашим использованием cookie.<br><a href="/user_agreement/" class="link">Конфиденциальность и политика cookie</a></p>
    </div>
</div>



<a id="quickOrderFormLink" class="modal-qOrder__link  sr-only" href="#quickOrderForm">triggers fancybox</a>
<div id='quickOrderForm' class="modal-qOrder" style="display:none; z-index: 1000000">
    <div class="modal-qOrder__wrapper">
        <a href="javascript:;" onclick="$.fancybox.close();" class="modal-review__close">Закрыть</a>
        <h2 class="modal-qOrder__title">Быстрый заказ</h2>
        <form class="quickOrderForm" onsubmit="return false"></form>
    </div>
</div>




<a class="underOrderModal__link  sr-only" href="#underOrderModal">triggers fancybox</a>
<div id='underOrderModal' class="modal-qOrder" style="display:none; z-index: 1000000">
    <div class="modal-qOrder__wrapper">
        <a href="javascript:;" onclick="$.fancybox.close();" class="modal-review__close">Закрыть</a>

        <h2 class="modal-qOrder__title">Под заказ</h2>

        <form class="underOrderModalForm" onsubmit="return false;">

            <div class="account-form__row">
                <div class="account-form__field">
                    <span>ФИО *</span><input type="text" name="fio">
                </div>
                <div class="account-form__field">
                    <span>Эл. почта *</span><input type="email" name="email">
                </div>
                <div class="account-form__field">
                    <span>Телефон *</span><input type="tel" name="phone">
                </div>
                <div class="account-form__field">
                    <span>Город *</span><input type="text" name="city" value="<?=$_SESSION['LOC']['CITY']?>">
                </div>
            </div>
            
            <div class="account-form__field  account-form__field--wa">
                <div class="account-form__checkbox">
                    <input type="checkbox" id="agree_77" name="agree" value="1">
                    <label for="agree_77">Я&nbsp;прочитал и&nbsp;принял условия <a href="/user_agreement/" target="_blank">конфиденциальности данных</a> и&nbsp;даю согласие на&nbsp;обработку своих персональных данных *</label>
                </div>
            </div>

            <div>
                <p class="error___p"></p>
            </div>

            <div class="account-form__regBtn">
                <button type="button" class="account-form__button underOrderSend to___process">Отправить</button>
                <p class="account-form__note">* Пожалуйста, заполните все обязательные поля</p>
            </div>

            <input type="hidden" name="product_id">

        </form>
    </div>
</div>




<? if( $user->isDealer() && $arURI[1] == 'personal' ){ ?>

    <a class="modal-region__link  sr-only mapModalLink" href="#map___modal">triggers fancybox</a>
    <div id='map___modal' class="modal-region" style="display:none; z-index: 1000000">
        <div class="modal-region__wrapper">
            <a href="javascript:;" onclick="$.fancybox.close();" class="modal-review__close">Закрыть</a>
            <h2 style="margin: 0 0 10px 0;">Укажите точку на карте</h2>
<!--            <input id="pac-input" class="controls" type="text" placeholder="Строка поиска">-->
            <div id="modal___map" style="width: 100%; height: 400px;"></div>
            <div style="width: 100%; text-align:right!important;">
                <a style="cursor: pointer; width: 110px;margin-top:10px;" class="account-form__button modalMapSelectButton">Сохранить</a>
            </div>
        </div>
    </div>

    <script>
    $(document).ready(function(){
        var $mapDiv = $('#modal___map');
        var mapDim = {
            height: $mapDiv.height(),
            width: $mapDiv.width()
        }
        modal_map = new google.maps.Map($mapDiv[0], {
            center: { lat: 55.75399399999374, lng: 37.62209300000001 },
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoom: 10,
            disableDefaultUI: true,
            zoomControl: true,
            fullscreenControl: true
        });

        // var input = document.getElementById('pac-input');
        // var searchBox = new google.maps.places.SearchBox(input);
        // modal_map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        google.maps.event.addListener(modal_map, 'click', function(event) {
            if( modal_map_marker ){
                modal_map_marker.setMap(null);
                modal_map_marker = false;
            }
            modal_map_marker = new google.maps.Marker({
                position: event.latLng,
                draggable:true,
                map: modal_map,
                animation: google.maps.Animation.DROP,
            });
        });
    })
    </script>

<? } ?>