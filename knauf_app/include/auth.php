<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if(
    tools\funcs::arURI()[1] == 'auth'
    ||
    tools\funcs::arURI()[1] == 'personal'
    ||
    tools\funcs::arURI()[1] == 'order'
){ ?>

    <!--<section class="bcgBlock  bcgBlock--indexTop" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/images/blockbcg3.jpg);">
        <div class="bcgBlock__wrapper">
            <h2 class="bcgBlock__title">Вход в&nbsp;личный кабинет</h2>
        </div>
    </section>-->

<? } ?>

<section class="block">
    <div class="block__wrapper" style="padding-top: 0px;">

<!--        <h2 class="block__title">Вход</h2>-->

        <? if( $USER->IsAuthorized() ){ ?>

            <div class="block__textBlock">
                <p class="block__text">Вы уже авторизованы</p>
                <p class="block__text">
                    Перейти в <a href="<?=SITE_DIR?>/personal/" class="block__link">Личный кабинет</a>
                </p>
            </div>

        <? } else { ?>

            <div class="block__textBlock">
                <p class="block__text">
                    Если у&nbsp;вас нет аккаунта, пожалуйста, <a href="<?=SITE_DIR?>/register/" class="block__link">Зарегистрируйтесь</a>
                </p>
            </div>

            <form method="post" class="form form--loginWithoutReg">
                <div class="form__wrapper">
                    <div class="form__group">
                        <label for="email">Логин (Email)</label>
                        <input type="text" name="login" placeholder="Введите ваш логин">
                    </div>
                    <div class="form__group">
                        <label for="password">Пароль</label>
                        <input type="password" name="password" placeholder="Введите пароль">
                    </div>

                    <? $APPLICATION->IncludeComponent(
                        "bitrix:system.auth.form", "soc_auth",
                        Array(
                            "FORGOT_PASSWORD_URL" => "",
                            "PROFILE_URL" => "",
                            "REGISTER_URL" => "",
                            "SHOW_ERRORS" => "N"
                        )
                    ); ?>

                    <p class="error___p"></p>

                    <div class="form__buttons">

                        <input type="button" value="Войти" class="form__button auth___button to___process">

                        <? if( tools\funcs::arURI()[1] == 'order' ){ ?>
                            <input type="button" value="Покупка без регистрации" class="form__button no_auth_order_button to___process">
                        <? } ?>

                    </div>

                    <a href="<?=SITE_DIR?>/password_recovery/" class="form__link  form__link--restore">Забыли пароль?</a>

                </div>
            </form>

        <? } ?>

    </div>
</section>

