<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if( $USER->IsAuthorized() ){

    $dealer = new project\dealer( $USER->GetID() );

    if( $dealer->isDealer() ){

        // Назначение цен на товары
        $APPLICATION->IncludeComponent(
            "aoptima:personal_goods", "",
            array( 'DEALER' => $dealer )
        );

    } else {

        LocalRedirect('/personal/');
    }

} else {

    LocalRedirect('/personal/');
}

