<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$server_name = \Bitrix\Main\Config\Option::get('main', 'server_name'); ?>



                                                <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                                                    <tbody>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                        <td height="9px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 0px; font-weight: normal; hyphens: auto; line-height: 9px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&amp;nbsp;</td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                            </th>
                                        </tr>
                                    </tbody>
                                </table>
                            </th>
                        </tr>
                    </tbody>
                </table>

<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <th class="small-12 large-12 columns greyBlock" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 13px; padding-right: 13px; text-align: left; width: 564px;">
            <table style="background: #e0dad4; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                <tbody>
                <tr style="padding: 0; text-align: left; vertical-align: top;">
                    <th class="center-all" style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 19px; text-align: left;">

                        <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Посмотреть детали заказа, принять в работу или отменить заказ Вы можете в личном кабинете компании во вкладке "Заказы".</p>

                        <table class="spacer" style="background: #e0dad4; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                            <tbody>
                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                <td height="26px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 0px; font-weight: normal; hyphens: auto; line-height: 26px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&amp;nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="button" style="Margin: 0 0 16px 0; background: #e0dad4; border-collapse: collapse; border-spacing: 0; margin: 0 0 16px 0; margin-bottom: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                            <tbody>
                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                                    <a href="https://<?=$server_name?>/personal/#showtab-tab_111_5" style="Margin: 0; background: #029ee5; border: 0 solid #2199e8; border-radius: 0px; color: #ffffff; display: inline-block; font-family: Arial, Helvetica, sans-serif; font-size: 18px; font-weight: normal; height: 40px; line-height: 1; margin: 0; padding: 0; padding-top: 20px; text-align: center; text-decoration: none; width: 100%;">Посмотреть детали и обработать заказ</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </th>
                </tr>
                </tbody>
            </table>
        </th>
    </tr>
    </tbody>
</table>

<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <td height="22px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 0px; font-weight: normal; hyphens: auto; line-height: 22px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&amp;nbsp;</td>
    </tr>
    </tbody>
</table>

<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 32px; padding-right: 32px; text-align: left; width: 564px;">
            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                <tbody>
                <tr style="padding: 0; text-align: left; vertical-align: top;">
                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">

                        <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Благодарим Вас за использование услуг КНАУФ Маркетплейс!</p>

                        <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                            <tbody>
                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                <td height="14px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 0px; font-weight: normal; hyphens: auto; line-height: 14px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&amp;nbsp;</td>
                            </tr>
                            </tbody>
                        </table>

                    </th>
                </tr>
                </tbody>
            </table>
        </th>
    </tr>
    </tbody>
</table>


            </td>
        </tr>

        <tr style="padding: 0; text-align: left; vertical-align: top;">
            <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                <table class="wrapper secondary" align="center" style="background: #e0dad4; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                    <tbody>


<tr style="padding: 0; text-align: left; vertical-align: top;">
    <td class="wrapper-inner" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
        <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
            <tbody>
            <tr style="padding: 0; text-align: left; vertical-align: top;">
                <td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 0px; font-weight: normal; hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&amp;nbsp;</td>
            </tr>
            </tbody>
        </table>
        <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
            <tbody>
            <tr style="padding: 0; text-align: left; vertical-align: top;">
                <th class="small-12 large-8 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 32px; padding-right: 8px; text-align: left; width: 370.66667px;">
                    <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                        <tbody>
                        <tr style="padding: 0; text-align: left; vertical-align: top;">
                            <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">

                                <p style="Margin: 0; Margin-bottom: 10px; color: #4c4c4c; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">С уважением,</p>
                                <p style="Margin: 0; Margin-bottom: 10px; color: #4c4c4c; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;"><b>КНАУФ Маркетплейс</b></p>

                                <p style="Margin: 0; Margin-bottom: 10px; color: #4c4c4c; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">
                                    <a href="mailto:<?=\Bitrix\Main\Config\Option::get('aoptima.project', 'COMPANY_EMAIL')?>" style="Margin: 0; color: #009fe3; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left; text-decoration: none;"><?=\Bitrix\Main\Config\Option::get('aoptima.project', 'COMPANY_EMAIL')?></a>
                                </p>

                            </th>
                        </tr>
                        </tbody>
                    </table>
                </th>
                <th class="small-12 large-4 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 32px; text-align: left; width: 177.33333px;">
                    <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                        <tbody>
                        <tr style="padding: 0; text-align: left; vertical-align: top;">
                            <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">
                                <p style="Margin: 0; Margin-bottom: 10px; color: #4c4c4c; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;"><?=\Bitrix\Main\Config\Option::get('aoptima.project', 'COMPANY_ADDRESS')?></p>
                                <p style="Margin: 0; Margin-bottom: 10px; color: #4c4c4c; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;"><a href="https://<?=$server_name?>/" style="Margin: 0; color: #009fe3; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left; text-decoration: none;"><?=$server_name?></a></p>
                            </th>
                        </tr>
                        <tr style="padding: 0; text-align: left; vertical-align: top;">
                            <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                                <table class="menu  social" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: auto !important;">
                                    <tbody>
                                    <tr style="padding: 0; text-align: left; vertical-align: top;">
                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                                            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                                                <tbody>
                                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                    <th class="menu-item  float-center" style="Margin: 0 auto; color: #0a0a0a; float: none; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0px; padding-right: 11px; text-align: center;">
                                                        <a href="<?=\Bitrix\Main\Config\Option::get('aoptima.project', 'FACEBOOK_LINK')?>" style="Margin: 0; color: #2199e8; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left; text-decoration: none;"><img src="https://<?=$server_name?>/local/templates/main/images/mail-fb-icon.png" alt="" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: block; max-width: 100%; outline: none; text-decoration: none; width: auto;"></a>
                                                    </th>
                                                    <th class="menu-item  float-center" style="Margin: 0 auto; color: #0a0a0a; float: none; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0px; padding-right: 11px; text-align: center;">
                                                        <a href="<?=\Bitrix\Main\Config\Option::get('aoptima.project', 'TWITTER_LINK')?>" style="Margin: 0; color: #2199e8; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left; text-decoration: none;"><img src="https://<?=$server_name?>/local/templates/main/images/mail-twitter-icon.png" alt="" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: block; max-width: 100%; outline: none; text-decoration: none; width: auto;"></a>
                                                    </th>
                                                    <th class="menu-item  float-center" style="Margin: 0 auto; color: #0a0a0a; float: none; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0px; padding-right: 11px; text-align: center;">
                                                        <a href="<?=\Bitrix\Main\Config\Option::get('aoptima.project', 'YOUTUBE_LINK')?>" style="Margin: 0; color: #2199e8; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left; text-decoration: none;"><img src="https://<?=$server_name?>/local/templates/main/images/mail-youtube-icon.png" alt="" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: block; max-width: 100%; outline: none; text-decoration: none; width: auto;"></a>
                                                    </th>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </th>
            </tr>
            </tbody>
        </table>
    </td>
</tr>


                    </tbody>
                </table>
            </td>

        </tr>

    </tbody>
</table>


</center>

<center data-parsed="" style="min-width: 500px; width: 100%;">
    <table align="center" class="container footer float-center" style="Margin: 0 auto; background: #009fe3; border-collapse: collapse; border-spacing: 0; float: none; margin: 0 auto; padding: 0; text-align: center; vertical-align: top; width: 500px;">
        <tbody>
            <tr style="padding: 0; text-align: left; vertical-align: top;">
                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                    <table align="center" class="" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top;">
                        <tbody>
                        <tr style="padding: 0; text-align: left; vertical-align: top;">
                            <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                                <table class="row collapse" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                                    <tbody>
                                    <tr style="padding: 0; text-align: left; vertical-align: top;">
                                        <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">
                                            <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                                                <tbody>
                                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">
                                                        <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                                                            <tbody>
                                                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                                    <td height="29px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 0px; font-weight: normal; hyphens: auto; line-height: 29px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&amp;nbsp;</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </th>
                                                </tr>
                                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                    <th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-left: 32px; padding-right: 32px; text-align: left; width: 500px;">
                                                        <p class="text-center" style="Margin: 0; Margin-bottom: 10px; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; margin-bottom: 0; padding: 0; text-align: center;">Управлять подпиской Вы можете <a href="https://<?=$server_name?>/" style="Margin: 0; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left; text-decoration: underline;">на сайте</a></p>
                                                    </th>
                                                </tr>
                                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">
                                                        <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                                                            <tbody>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                                <td height="29px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 0px; font-weight: normal; hyphens: auto; line-height: 29px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&amp;nbsp;</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </th>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </th>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>


        </tbody>
    </table>
</center>

                    </td>
                </tr>

            </tbody>
        </table>

    </body>

</html>