<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

$module_id = 'aoptima.project';

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
Loc::loadMessages(__FILE__);

if ( $APPLICATION->GetGroupRight($module_id) < "S" ){
    $APPLICATION->AuthForm('Access denied');
}

\Bitrix\Main\Loader::includeModule($module_id);


$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

#Описание опций

$aTabs = array(
    
	array(
        'DIV' => 'edit1',
        'TAB' => "Настройки модуля",
        'OPTIONS' => array(
            array(
                'COMPANY_ADDRESS',
                'Адрес компании',
                '',
                array('text', 38)
            ),
            array(
                'COMPANY_EMAIL',
                'Эл. почта компании',
                '',
                array('text', 38)
            ),
            array(
                'PRICE_ROUNDING',
                'Округление цен',
                2,
                array('text', 38)
            ),
        )
    ),

    array(
        'DIV' => 'edit2',
        'TAB' => 'Соц.сети',
        'OPTIONS' => array(
            array(
                'YOUTUBE_LINK',
                'Ссылка на канал YouTube',
                '',
                array('text', 38)
            ),
            array(
                'FACEBOOK_LINK',
                'Ссылка на Facebook',
                '',
                array('text', 38)
            ),
            array(
                'TWITTER_LINK',
                'Ссылка на Twitter',
                '',
                array('text', 38)
            ),
        )
    ),

    array(
        'DIV' => 'edit3',
        'TAB' => 'Сортировка предложений дилеров',
        'OPTIONS' => array(
            array(
                'BASKET_SUM_WEIGHT',
                'Коэфф. к параметру "Стоимость товаров"',
                1,
                array('text', 38)
            ),
            array(
                'DELIVERY_PRICE_WEIGHT',
                'Коэфф. к параметру "Стоимость доставки"',
                1,
                array('text', 38)
            ),
            array(
                'DEALER_RATING_WEIGHT',
                'Коэфф. к параметру "Рейтинг дилера"',
                '',
                array('text', 38)
            ),
            array(
                'DEALER_PVZ_DISTANCE_WEIGHT',
                'Коэфф. к параметру "Расстояние до ближайшего ПВЗ дилера (Самовывоз)"',
                '',
                array('text', 38)
            ),
            array(
                'DEALER_RISE_LARGE_GOODS_WEIGHT',
                'Коэфф. к параметру "Подъём крупногабарит. т-ров дилером (при наличии в корзине + Доставка)"',
                '',
                array('text', 38)
            ),
            array(
                'DEALER_DELIVERY_DAYS_WEIGHT',
                'Коэфф. к параметру "Срок доставки (Доставка)"',
                '',
                array('text', 38)
            ),
        )
    ),

    array(
        'DIV' => 'edit4',
        'TAB' => 'Емэйлы',
        'OPTIONS' => array(
            array(
                'COMMON_ORDERS_EMAIL_TO',
                'Общий адрес электронной почты для заказов',
                '',
                array('text', 38)
            ),
            array(
                'POZH_FORM_EMAIL_TO',
                'Эл. почта для формы "Пожелания/предложения"',
                '',
                array('text', 38)
            )
        )
    ),

);

if ($APPLICATION->GetGroupRight($module_id)=="W"){
	
	$aTabs[] = array(
        "DIV" => "edit100",
        "TAB" => Loc::getMessage("MAIN_TAB_RIGHTS"),
        "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_RIGHTS")
    );
	
}


#Сохранение
if ( 
	$request->isPost()
	&&
	$request['Update']
	&&
	check_bitrix_sessid()
){

    foreach ($aTabs as $aTab){
		
        //Или можно использовать __AdmSettingsSaveOptions($MODULE_ID, $arOptions);
        foreach ($aTab['OPTIONS'] as $arOption){
			
            if (!is_array($arOption)) //Строка с подсветкой. Используется для разделения настроек в одной вкладке
                continue;

            if ($arOption['note']) //Уведомление с подсветкой
                continue;

            //Или __AdmSettingsSaveOption($MODULE_ID, $arOption);
            $optionName = $arOption[0];

            $optionValue = $request->getPost($optionName);

            Option::set($module_id, $optionName, is_array($optionValue) ? implode(",", $optionValue):$optionValue);
			
        }
    }
	
}

#Визуальный вывод

$tabControl = new CAdminTabControl('tabControl', $aTabs);

?>

<? $tabControl->Begin(); ?>

<form method='post' action='<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($request['mid'])?>&amp;lang=<?=$request['lang']?>' name='AOPTIMA_PROJECT_settings'>

    <? foreach ($aTabs as $aTab):
		if($aTab['OPTIONS']):?>
			<? $tabControl->BeginNextTab(); ?>
			<? __AdmSettingsDrawList($module_id, $aTab['OPTIONS']); ?>
		<? endif;
     endforeach; ?>

    <? $tabControl->BeginNextTab();
	
	if ($APPLICATION->GetGroupRight($module_id)=="W"){
		require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/admin/group_rights.php');
	}


    $tabControl->Buttons(); ?>

    <input type="submit" name="Update" value="<?echo GetMessage('MAIN_SAVE')?>">
    <input type="reset" name="reset" value="<?echo GetMessage('MAIN_RESET')?>">
	
    <?=bitrix_sessid_post();?>
	
</form>

<? $tabControl->End(); ?>

