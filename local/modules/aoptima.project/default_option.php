<?php

$aoptima_project_default_option = array(
    'PRICE_ROUNDING' => 2,
    'COMPANY_ADDRESS' => '',
    'COMPANY_EMAIL' => '',
    'YOUTUBE_LINK' => '',
    'FACEBOOK_LINK' => '',
    'TWITTER_LINK' => '',
    'DELIVERY_PRICE_WEIGHT' => 1,
    'DEALER_RATING_WEIGHT' => 1,
    'BASKET_SUM_WEIGHT' => 1,
    'DEALER_PVZ_DISTANCE_WEIGHT' => 1,
    'DEALER_RISE_LARGE_GOODS_WEIGHT' => 1,
    'DEALER_DELIVERY_DAYS_WEIGHT' => 1,
    'COMMON_ORDERS_EMAIL_TO' => '',
    'POZH_FORM_EMAIL_TO' => '',
);