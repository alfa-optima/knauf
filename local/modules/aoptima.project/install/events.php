<?php

// Массив событий модуля.

 
$MODULE_EVENTS = array(


    array(
        'iblock',
        'OnAfterIBlockElementAdd',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnAfterIBlockElementAdd',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockElementUpdate',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnAfterIBlockElementUpdate',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockElementDelete',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnAfterIBlockElementDelete',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockElementAdd',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnBeforeIBlockElementAdd',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockElementUpdate',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnBeforeIBlockElementUpdate',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockElementDelete',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnBeforeIBlockElementDelete',
        100,
    ),


    array(
        'iblock',
        'OnAfterIBlockSectionAdd',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnAfterIBlockSectionAdd',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockSectionUpdate',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnAfterIBlockSectionUpdate',
        100,
    ),
    array(
        'iblock',
        'OnAfterIBlockSectionDelete',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnAfterIBlockSectionDelete',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockSectionAdd',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnBeforeIBlockSectionAdd',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockSectionUpdate',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnBeforeIBlockSectionUpdate',
        100,
    ),
    array(
        'iblock',
        'OnBeforeIBlockSectionDelete',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnBeforeIBlockSectionDelete',
        100,
    ),
    
    
    
    
    array(
        'main',
        'OnAfterUserAdd',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnAfterUserAdd',
        100,
    ),
    array(
        'main',
        'OnAfterUserUpdate',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnAfterUserUpdate',
        100,
    ),
    array(
        'main',
        'OnBeforeUserDelete',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnBeforeUserDelete',
        100,
    ),



    array(
        'catalog',
        'OnPriceAdd',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnPriceAdd',
        100,
    ),
    array(
        'catalog',
        'OnPriceUpdate',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnPriceUpdate',
        100,
    ),
    array(
        'catalog',
        'OnBeforePriceDelete',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnBeforePriceDelete',
        100,
    ),
    array(
        'sale',
        'OnSaleBasketSaved',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnSaleBasketSaved',
        100
    ),
    array(
        'sale',
        'OnSaleBasketBeforeSaved',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnSaleBasketBeforeSaved',
        100
    ),
    array(
        'sale',
        'OnSaleBeforeOrderDelete',
        'aoptima.project',
        '\AOptima\Project\handlers',
        'OnSaleBeforeOrderDelete',
        100
    ),

);

