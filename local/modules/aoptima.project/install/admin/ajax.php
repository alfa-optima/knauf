<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;



if( $USER->IsAdmin() ){



    if( $_POST['action'] == 'getPropType' ){

        $propCode = strip_tags($_POST['propCode']);
        $sectXMLID = strip_tags($_POST['sectXMLID']);
        if( strlen($sectXMLID) > 0 ){
            $section = project\catalog::getSectionByXMLID( $sectXMLID );
            $prop = tools\prop::getByCode( $section['IBLOCK_ID'], $propCode );

            if( intval($prop['ID']) > 0 ){

                ob_start();
                    $APPLICATION->IncludeComponent(
                        "aoptima:seo_page_prop_item", "",
                        [ 'prop' => $prop, 'section' => $section ]
                    );
                    $prop_html = ob_get_contents();
                ob_end_clean();

                $ar = [
                    "status" => "ok",
                    "prop_html" => $prop_html
                ];

                // Ответ
                echo json_encode($ar);  return;
            } else {
                // Ответ
                echo json_encode(["status" => "ok"]);
                return;
            }
        } else {
            // Ответ
            echo json_encode(["status" => "error", "text" => "Укажите символьный код категории"]);
            return;
        }
    }



    // Создание страницы
    if( $_POST['action'] == 'add' ){

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        $arFields['CODE'] = trim($arFields['CODE']);
        $arFields['CODE'] = \AOptima\Tools\funcs::translit($arFields['CODE']);

        // Ищём запись по символьному коду
        $res = project\SeopageTable::getList([
            'select' => [ 'ID', 'CODE' ],
            'filter' => [ 'CODE' => $arFields['CODE'] ],
            'limit' => 1,
        ]);
        if( $element = $res->fetch() ){
            // Ответ
            echo json_encode(["status" => "error", "text" => "Страница с таким символьным кодом уже создана ранее"]);
            return;
        }

        $filter = [];
        if( strlen($arFields['PRICE_FROM']) > 0 ){
            $filter['PRICE_FROM'] = $arFields['PRICE_FROM'];
            unset($arFields['PRICE_FROM']);
        }
        if( strlen($arFields['PRICE_TO']) > 0 ){
            $filter['PRICE_TO'] = $arFields['PRICE_TO'];
            unset($arFields['PRICE_TO']);
        }
        // Определяем категорию по XML_ID
        $section = project\catalog::getSectionByXMLID( $arFields['SECTION_XML_ID'] );
        if(
            intval($section['ID']) > 0
            &&
            is_array($arFields['PROP_VALUES'])
            &&
            count($arFields['PROP_VALUES']) > 0
        ){
            $filter['PROP_VALUES'] = $arFields['PROP_VALUES'];
            foreach ( $filter['PROP_VALUES'] as $prop_code => $arValues ){
                foreach ( $arValues as $key => $value ){
                    if( strlen($value) > 0 ){} else {
                        unset($filter['PROP_VALUES'][$prop_code][$key]);
                    }
                }
            }
        }
        $arFields['FILTER_JSON'] = json_encode($filter);
        unset($arFields['PROP_VALUES']);

        $arFields['SHOW_ON_MAIN_PAGE'] = $arFields['SHOW_ON_MAIN_PAGE']=='Y'?'Y':'N';
        
        $id = project\SeopageTable::addItem($arFields);
        if( intval($id) > 0 ){

            BXClearCache(true, "/all_seo_pages/");
            BXClearCache(true, "/SeoPageGetBySectionCode/");

            // Ответ
            echo json_encode(["status" => "ok", "id" => $id]);
            return;
        } else {
            // Ответ
            echo json_encode(["status" => "error", "text" => "Ошибка сохранения"]);
            return;
        }

    }


    // Создание категории
    if( $_POST['action'] == 'add_category' ){

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);
        if( !$arFields['SORT'] ){    $arFields['SORT'] = 500;    }

        // Ищём запись по символьному коду
        $res = project\SeopageTable::getList([
            'select' => [ 'ID', 'NAME' ],
            'filter' => [ 'NAME' => $arFields['NAME'] ],
            'limit' => 1,
        ]);
        if( $element = $res->fetch() ){
            // Ответ
            echo json_encode(["status" => "error", "text" => "Категория с таким названием уже создана ранее"]);
            return;
        }

        $id = project\SeopagecategoryTable::addItem($arFields);
        if( intval($id) > 0 ){
            BXClearCache(true, "/seo_page_categories/");
            // Ответ
            echo json_encode(["status" => "ok", "id" => $id]);
            return;
        } else {
            // Ответ
            echo json_encode(["status" => "error", "text" => "Ошибка сохранения"]);
            return;
        }

    }


    // Редактирование
    if( $_POST['action'] == 'update' ){
        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        $arFields['CODE'] = trim($arFields['CODE']);
        $arFields['CODE'] = \AOptima\Tools\funcs::translit($arFields['CODE']);

        $item_id = $arFields['item_id'];
        unset($arFields['item_id']);

        // Ищём запись по символьному коду
        $res = project\SeopageTable::getList([
            'select' => [ 'ID', 'CODE' ],
            'filter' => [ 'CODE' => $arFields['CODE'], '!ID' => $item_id ],
            'limit' => 1,
        ]);
        if( $element = $res->fetch() ){
            // Ответ
            echo json_encode(["status" => "error", "text" => "Страница с таким символьным кодом уже создана ранее"]);
            return;
        }

        $filter = [];
        if( strlen($arFields['PRICE_FROM']) > 0 ){
            $filter['PRICE_FROM'] = $arFields['PRICE_FROM'];
            unset($arFields['PRICE_FROM']);
        }
        if( strlen($arFields['PRICE_TO']) > 0 ){
            $filter['PRICE_TO'] = $arFields['PRICE_TO'];
            unset($arFields['PRICE_TO']);
        }
        // Определяем категорию по XML_ID
        $section = project\catalog::getSectionByXMLID( $arFields['SECTION_XML_ID'] );
        if(
            intval($section['ID']) > 0
            &&
            is_array($arFields['PROP_VALUES'])
            &&
            count($arFields['PROP_VALUES']) > 0
        ){
            $filter['PROP_VALUES'] = $arFields['PROP_VALUES'];
            foreach ( $filter['PROP_VALUES'] as $prop_code => $arValues ){
                foreach ( $arValues as $key => $value ){
                    if( strlen($value) > 0 ){} else {
                        unset($filter['PROP_VALUES'][$prop_code][$key]);
                    }
                }
            }
        }
        $arFields['FILTER_JSON'] = json_encode($filter);
        unset($arFields['PROP_VALUES']);

        $arFields['SHOW_ON_MAIN_PAGE'] = $arFields['SHOW_ON_MAIN_PAGE']=='Y'?'Y':'N';

        $res = project\SeopageTable::updateItem($item_id, $arFields);
        if( $res ){

            BXClearCache(true, "/all_seo_pages/");
            BXClearCache(true, "/SeoPageGetBySectionCode/");

            // Ответ
            echo json_encode(["status" => "ok"]);
            return;
        } else {
            // Ответ
            echo json_encode(["status" => "error", "text" => "Ошибка сохранения"]);
            return;
        }
    }
    // Редактирование категории
    if( $_POST['action'] == 'update_category' ){
        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        $item_id = $arFields['item_id'];
        unset($arFields['item_id']);

        // Ищём запись по символьному коду
        $res = project\SeopagecategoryTable::getList([
            'select' => [ 'ID', 'NAME' ],
            'filter' => [ 'NAME' => $arFields['NAME'], '!ID' => $item_id ],
            'limit' => 1,
        ]);
        if( $element = $res->fetch() ){
            // Ответ
            echo json_encode(["status" => "error", "text" => "Категория с таким символьным кодом уже создана ранее"]);
            return;
        }

        $res = project\SeopagecategoryTable::updateItem($item_id, $arFields);
        if( $res ){
            BXClearCache(true, "/seo_page_categories/");
            // Ответ
            echo json_encode(["status" => "ok"]);
            return;
        } else {
            // Ответ
            echo json_encode(["status" => "error", "text" => "Ошибка сохранения"]);
            return;
        }
    }


    // Удаление
    if( $_POST['action'] == 'delete' ){

        $item_id = strip_tags($_POST['item_id']);

        if ( intval($item_id) > 0 ){
            $result = project\SeopageTable::delete($item_id);
        }

        if( intval($item_id) > 0 && $result ){

            BXClearCache(true, "/all_seo_pages/");

            // Ответ
            echo json_encode(["status" => "ok"]);
            return;

        } else {

            // Ответ
            echo json_encode(["status" => "error", "text" => "Ошибка удаления"]);
            return;

        }

    }



    // Удаление категории
    if( $_POST['action'] == 'delete_category' ){

        $item_id = strip_tags($_POST['item_id']);

        if ( intval($item_id) > 0 ){
            $result = project\SeopagecategoryTable::delete($item_id);
        }

        if( intval($item_id) > 0 && $result ){

            BXClearCache(true, "/seo_page_categories/");

            // Ответ
            echo json_encode(["status" => "ok"]);
            return;

        } else {

            // Ответ
            echo json_encode(["status" => "error", "text" => "Ошибка удаления"]);
            return;

        }

    }



    // Ответ
    echo json_encode(Array("status" => "error", "text" => "Ошибка данных"));
    return;


}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
return;