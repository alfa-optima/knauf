<?php

namespace AOptima\Project;
use AOptima\Project as project;



class filter_page {

    const HIBLOCK_ID = 10;

    static $user_groups = [ 1 ];


    function __construct(){
        \Bitrix\Main\Loader::includeModule('highloadblock');
        $this->hlblock_id = static::HIBLOCK_ID;
        $this->hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($this->hlblock_id)->fetch();
        $this->entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($this->hlblock);
        $this->entity_data_class = $this->entity->getDataClass();
        $this->entity_table_name = $this->hlblock['TABLE_NAME'];
        $this->sTableID = 'tbl_'.$this->entity_table_name;
    }



    // add
    public function add(
        $url,
        $h1,
        $text = null,
        $meta_title,
        $meta_description = null,
        $meta_keywords = null
    ){
        \Bitrix\Main\Loader::includeModule('highloadblock');
        $arData = [
            'UF_URL' => $url,
            'UF_H1' => $h1,
            'UF_META_TITLE' => $meta_title,
            'UF_META_DESCRIPTION' => $meta_description,
            'UF_META_KEYWORDS' => $meta_keywords,
            'UF_TEXT' => $text,
        ];
        $result = $this->entity_data_class::add($arData);
        if ($result->isSuccess()) {
            return true;
        } else {
            tools\logger::addError('Ошибка добавления SEO-страницы для фильтра - '.implode(', ', $result->getErrors()));
        }
        return false;
    }



    // update
    public function update(
        $id,
        $url,
        $h1,
        $text = null,
        $meta_title,
        $meta_description = null,
        $meta_keywords = null
    ){
        \Bitrix\Main\Loader::includeModule('highloadblock');
        $arData = [
            'UF_URL' => $url,
            'UF_H1' => $h1,
            'UF_META_TITLE' => $meta_title,
            'UF_META_DESCRIPTION' => $meta_description,
            'UF_META_KEYWORDS' => $meta_keywords,
            'UF_TEXT' => $text,
        ];
        $result = $this->entity_data_class::update($id, $arData);
        if ($result->isSuccess()) {
            return true;
        } else {
            tools\logger::addError('Ошибка добавления SEO-страницы для фильтра - '.implode(', ', $result->getErrors()));
        }
        return false;
    }



    // getByUrl
    public function getByUrl( $url ){
        $page = null;
        $arFilter = [ 'UF_URL' => $url ];
        $arSelect = [ '*' ];
        $arOrder = [ "ID" => "DESC" ];
        $arInfo = [
            "select" => $arSelect,
            "filter" => $arFilter,
            "order" => $arOrder,
            "limit" => 1
        ];
        $rsData =  $this->entity_data_class::getList( $arInfo );
        $rsData = new \CDBResult($rsData,  $this->sTableID);
        if( $element = $rsData->Fetch() ){
            $page = $element;
        }
        return $page;
    }



    // удаление
    public function delete( $id ){
        if( intval($id) > 0 ){
            $result = $this->entity_data_class::delete($id);
            if ( $result->isSuccess() ){
                return true;
            } else {
                tools\logger::addError('Ошибка удаления SEO-страницы фильтра - '.implode(', ', $result->getErrors()));
            }
        }
        return false;
    }



    // AddPanelButton
    public function AddPanelButton(){
        global $USER;
        if( $USER->IsAuthorized() ){
            $user_groups = \CUser::GetUserGroup($USER->GetID());
            if( array_intersect( $user_groups, static::$user_groups ) ){
                global $APPLICATION;
                $APPLICATION->AddPanelButton([
                    "ID" => "filterPageButton",
                    "TEXT" => "SEO фильтра",
                    "HREF" => "javascript:openfilterPageWindow();",
                    "ICON" => "bx-panel-seo-icon",
                    "MAIN_SORT" => 10000
                ]);
            }
        }
    }




}