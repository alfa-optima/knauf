<?php
namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

class tp {



    // ТП товара
    static function getList(
        $tovarID = false,
        $dealerID = false,
        $loc_id = false,
        $limit = false,
        $stop_ids = false
    ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $list = array();
        $filter = Array(
            "IBLOCK_CODE" => project\catalog::TP_IBLOCK_CODE,
            "ACTIVE" => "Y",
            '!PROPERTY_LOCATION' => false,
            '!PROPERTY_CML2_LINK' => false
        );
        if( is_array($stop_ids) ){
            //$filter['!ID'] = $stop_ids;
        }
        if( intval($tovarID) > 0 ){
            $filter['PROPERTY_CML2_LINK'] = $tovarID;
        }
        if( intval($loc_id) > 0 ){
            $filter['PROPERTY_LOCATION'] = $loc_id;
        }
        if( intval($dealerID) > 0 ){
            $filter['PROPERTY_DEALER'] = $dealerID;
        }

        $fields = Array(
            "ID", "NAME",
            "PROPERTY_CML2_LINK",
            "PROPERTY_DEALER",
            "PROPERTY_LOCATION",
            "PROPERTY_POD_ZAKAZ",
            "CATALOG_GROUP_".project\catalog::PRICE_ID
        );

        $limitParam = intval($limit)>0?array("nTopCount" => $limit):false;

        $sort = [ "SORT"=>"ASC" ];

        $hash = md5(json_encode($sort).json_encode($filter).json_encode($limitParam).json_encode($fields));

        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 24*60*60;
        $cache_id = 'tp_list_'.$hash;
        $cache_path = '/tp_list/'.$GLOBALS['USER']->GetID().'/'.$hash.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
//            global $APPLICATION;
//            $APPLICATION->RestartBuffer();
//            echo '<pre>';
//            print_r($obCache);
//            die();
        } elseif($obCache->StartDataCache()){
            $tpList = \CIBlockElement::GetList( $sort, $filter, false, $limitParam, $fields );
            while ($tp = $tpList->GetNext()){
                $list[] = $tp;
            }
        $obCache->EndDataCache([ 'list' => $list ]);
        }

        return $list;
    }



    // Автосоздание ТП
    static function createDelaersTP( $userID ){

        \Bitrix\Main\Loader::includeModule('iblock');

        // Перебираем весь каталог товаров
        $tovarFilter = Array(
            "IBLOCK_CODE" => static::CATALOG_IBLOCK_CODE,
            //"ACTIVE" => "Y"
        );
        $tovarFields = Array( "ID", "NAME" );
        $tovary = \CIBlockElement::GetList(
            array("SORT"=>"ASC"), $tovarFilter, false, false, $tovarFields
        );
        while ($tovar = $tovary->GetNext()){

            // Определяем наличие у данного товара ТП для дилера $userID
            $tpFilter = Array(
                "IBLOCK_CODE" => static::TP_IBLOCK_CODE,
                "PROPERTY_CML2_LINK" => $tovar['ID'],
                "PROPERTY_DEALER" => $userID,
            );
            $tpFields = Array( "ID", "PROPERTY_CML2_LINK", "PROPERTY_DEALER" );
            $tps = \CIBlockElement::GetList(
                array("SORT"=>"ASC"), $tpFilter, false, array("nTopCount"=>1), $tpFields
            );
            if ($tp = $tps->GetNext()){

                // Попытка активации ТП
                $element = new \CIBlockElement;
                $fields = Array(
                    "ACTIVE" => "Y"
                );
                $element->Update($tp['ID'], $fields);

            } else {

                $tp_iblock_id = static::getIblockID( static::TP_IBLOCK_CODE );

                if( intval($tp_iblock_id) > 0 ){

                    // Создадим ТП для дилера $userID в данном товаре
                    $PROP[1] = $tovar['ID'];
                    $PROP[2] = $userID;
                    $element = new \CIBlockElement;
                    $fields = Array(
                        "IBLOCK_ID" => $tp_iblock_id,
                        "PROPERTY_VALUES" => $PROP,
                        "NAME" => $tovar['NAME'] . ' (Дилер ID=' . $userID . ')',
                        "ACTIVE" => "Y"
                    );
                    if ($el_id = $element->Add($fields)){

                        $productFields = array(
                            "ID" => $el_id,
                            "VAT_ID" => 1, //выставляем тип ндс (задается в админке)
                            "VAT_INCLUDED" => "Y" // НДС входит в стоимость
                        );
                        if( $product = \CCatalogProduct::Add($productFields) ){

                        } else {
                            tools\logger::addError('Не удалось создать CCatalogProduct для ТП для дилера - ' . $product->LAST_ERROR);
                        }


                    } else {
                        tools\logger::addError('Не удалось создать ТП для дилера - ' . $element->LAST_ERROR);
                    }

                } else {
                    tools\logger::addError('Не удалось создать ТП для дилера - не найден инфоблок торговых предложений');
                }

            }

        }


    }



    // Деактивация дилерских ТП
    static function deactivateDelaersTP( $userID ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $tpFilter = Array(
            "IBLOCK_CODE" => static::TP_IBLOCK_CODE,
            "PROPERTY_DEALER" => $userID,
        );
        $tpFields = Array( "ID", "PROPERTY_DEALER" );
        $tps = \CIBlockElement::GetList(
            array("SORT"=>"ASC"), $tpFilter, false, false, $tpFields
        );
        while ( $tp = $tps->GetNext() ){
            // деактивация ТП
            $element = new \CIBlockElement;
            $fields = Array("ACTIVE" => "N");
            $element->Update($tp['ID'], $fields);
        }
    }



}