<?php
namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('highloadblock');


class review_order extends review {

    const HIBLOCK_ID = 4;
    // const DETAIL_LIST_CNT = 10;


    function __construct(){
        $this->hlblock_id = static::HIBLOCK_ID;
        $this->hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($this->hlblock_id)->fetch();
        $this->entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($this->hlblock);
        $this->entity_data_class = $this->entity->getDataClass();
        $this->entity_table_name = $this->hlblock['TABLE_NAME'];
        $this->sTableID = 'tbl_'.$this->entity_table_name;
    }



    // add
    public function add(
        $order_id,
        $user_id,
        $dealer_id,
        $text = false,
        $dealer_vote = false,
        $product_quality = false,
        $shop_quality = false,
        $delivery_speed = false
    ){
        $arData = Array(
            'UF_ORDER' => $order_id,
            'UF_USER' => $user_id,
            'UF_DEALER' => $dealer_id,
            'UF_TEXT' => $text,
            'UF_DATE' => date('d.m.Y H:i:s'),
        );
        if( $dealer_vote >= 1 && $dealer_vote <= 5 ){
            $arData['UF_DEALER_VOTE'] = intval($dealer_vote);
        }
        if( $product_quality >= 1 && $product_quality <= 10 ){
            $arData['UF_PRODUCT_QUALITY'] = intval($product_quality);
        }
        if( $shop_quality >= 1 && $shop_quality <= 10 ){
            $arData['UF_SHOP_QUALITY'] = intval($shop_quality);
        }
        if( $delivery_speed >= 1 && $delivery_speed <= 10 ){
            $arData['UF_DELIVERY_SPEED'] = intval($delivery_speed);
        }
        global $USER;
        if( $USER->IsAdmin() ){    $arData['UF_ACTIVE'] = 1;    }
        $result = $this->entity_data_class::add($arData);
        if ($result->isSuccess()) {
            return true;
        } else {
            tools\logger::addError('Ошибка добавления отзыва о заказе - '.implode(', ', $result->getErrors()));
        }
        return false;
    }




    // getList
    public function getList(
        $order_id = false,
        $user_id = false,
        $dealer_id = false,
        $limit = false,
        $stop_items = false,
        $all = false
    ){
        $elements = array();
        $arFilter = [];
        if( !$all ){
            $arFilter['UF_ACTIVE'] = 1;
        }
        if( intval($order_id) > 0 ){   $arFilter['UF_ORDER'] = $order_id;    }
        if( intval($user_id) > 0 ){    $arFilter['UF_USER'] = $user_id;      }
        if( intval($dealer_id) > 0 ){    $arFilter['UF_DEALER'] = $dealer_id;      }
        if( is_array($stop_items) ){
            $arFilter['!ID'] = $stop_items;
        }
        $arSelect = array('*');
        $arOrder = array("ID" => "DESC");
        $arInfo = array(
            "select" => $arSelect,
            "filter" => $arFilter,
            "order" => $arOrder
        );
        if( intval($limit) > 0 ){   $arInfo['limit'] = intval($limit);   }
        $rsData =  $this->entity_data_class::getList( $arInfo );
        $rsData = new \CDBResult($rsData,  $this->sTableID);
        while($element = $rsData->Fetch()){
            $element['USER'] = tools\user::info($element['UF_USER']);
            $element['USER']['FIO'] = array();
            if( strlen($element['USER']['NAME']) > 0 ){
                $element['USER']['FIO'][] = $element['USER']['NAME'];
            }
            if(
                strlen($element['USER']['NAME']) > 0
                &&
                strlen($element['USER']['LAST_NAME']) > 0
            ){
                $element['USER']['FIO'][] = $element['USER']['LAST_NAME'];
            }
            $element['USER']['FIO'] = implode(' ', $element['USER']['FIO']);
            $elements[] = $element;
        }
        return $elements;
    }



    // getByID
    public function getByID( $id ){
        $arFilter = array( 'ID' => $id );
        $arSelect = array('*');
        $arOrder = array( "ID" => "ASC" );
        $arInfo = array(
            "select" => $arSelect,
            "filter" => $arFilter,
            "order" => $arOrder
        );
        $rsData =  $this->entity_data_class::getList( $arInfo );
        $rsData = new \CDBResult($rsData,  $this->sTableID);
        if($element = $rsData->Fetch()){
            return $element;
        }
        return false;
    }



    // удаление
    public function delete( $id ){
        if( intval($id) > 0 ){
            $location = static::getByID( $id );
            if( intval($location['ID']) > 0 ){
                $result = $this->entity_data_class::delete($location['ID']);
                if ( $result->isSuccess() ){
                    return true;
                } else {
                    tools\logger::addError('Ошибка удаления местоположения автообновления цен - '.implode(', ', $result->getErrors()));
                }
            }
        }
        return false;
    }




}