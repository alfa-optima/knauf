<?php
namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;



class dealer_shop {

    protected $iblock_id = 8;
    const LIST_MAX_CNT = 10;

    static $work_days = array(
        1 => array(
            'title' => 'Понедельник',
            'short_title' => 'Пн',
        ),
        2 => array(
            'title' => 'Вторник',
            'short_title' => 'Вт',
        ),
        3 => array(
            'title' => 'Среда',
            'short_title' => 'Ср',
        ),
        4 => array(
            'title' => 'Четверг',
            'short_title' => 'Чт',
        ),
        5 => array(
            'title' => 'Пятница',
            'short_title' => 'Пт',
        ),
        6 => array(
            'title' => 'Суббота',
            'short_title' => 'Сб',
        ),
        7 => array(
            'title' => 'Воскресенье',
            'short_title' => 'Вс',
        ),
    );



    public function getIblockID(){
       return $this->iblock_id;
    }


    // getByID
    public function getByID( $id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $filter = Array(
        	"IBLOCK_ID" => $this->iblock_id,
        	//"ACTIVE" => "Y",
        	"ID" => $id
        );
        $fields = Array(
            "ID", "NAME", "ACTIVE"
        );
        $dbElements = \CIBlockElement::GetList(
        	array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
        );
        while ($element = $dbElements->GetNext()){
            return $element;
        }
        return false;
    }

    public function getDataByID( $id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $filter = Array(
            "IBLOCK_ID" => $this->iblock_id,
            //"ACTIVE" => "Y",
            "ID" => $id
        );
        $fields = Array(
            "ID", "NAME", "PROPERTY_GPS", "PROPERTY_STREET", "PROPERTY_HOUSE",
            "PROPERTY_OFFICE", "PROPERTY_PHONES", "PROPERTY_LOC_ID", "PROPERTY_WORK_DAYS",
            "PROPERTY_WORK_TIME_OT", "PROPERTY_WORK_TIME_DO",
            "PROPERTY_OBED_TIME_OT", "PROPERTY_OBED_TIME_DO", "PROPERTY_REZHIM_RABOTY", "PROPERTY_EMAIL", "PROPERTY_SITE",
            "PROPERTY_INDEX",
        );
        $dbElements = \CIBlockElement::GetList(
            array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
        );
        while ($element = $dbElements->GetNext()){
            return $element;
        }
        return false;
    }

    public function getShops( $loc_id = false, $max_cnt = false ){
        $items = [];

        $dealer_shop = new project\dealer_shop();

        $countries = project\bx_location::getCountries();
        $country_id = array_keys($countries)[0];

        if( strlen($_POST['form_data']) > 0 ){
            $form_data = Array();
            parse_str($_POST["form_data"], $form_data);
            if( intval($form_data['filterDealer']) > 0 ){
                $dealer_id = $form_data['filterDealer'];
            }
            if( intval($form_data['filterCity']) > 0 ){
                $loc_id = $form_data['filterCity'];
            }
            if( intval($form_data['filterRegion']) > 0 ){
                $region_id = $form_data['filterRegion'];
            }
            if( intval($form_data['filterCountry']) > 0 ){
                $country_id = $form_data['filterCountry'];
            }
        }

        // Перечень точек продаж
        foreach( $countries as $country_id => $country ){

            $items[$country_id] = $dealer_shop->getList(
                $dealer_id,
                $loc_id,
                $samovyvoz,
                $max_cnt?($max_cnt + 1):false,
                is_array($_POST['stop_ids'])?$_POST['stop_ids']:false,
                $country_id,
                $region_id
            );

            foreach ( $items[$country_id] as $shop_id => $shop){
                $shop = tools\el::info($shop_id);
                $arAddress = array();
                if( intval($shop['PROPERTY_REGION_ID_VALUE']) > 0 ){
                    $loc = project\bx_location::getByID($shop['PROPERTY_REGION_ID_VALUE']);
                    $arAddress['region_name'] = $loc['NAME_RU'];
                }
                if( intval($shop['PROPERTY_LOC_ID_VALUE']) > 0 ){
                    $loc = project\bx_location::getByID($shop['PROPERTY_LOC_ID_VALUE']);
                    $arAddress['loc_name'] = $loc['NAME_RU'];
                }
                if( strlen($shop['PROPERTY_STREET_VALUE']) > 0 ){
                    $arAddress['street'] = 'ул.'.$shop['PROPERTY_STREET_VALUE'];
                }
                if( strlen($shop['PROPERTY_HOUSE_VALUE']) > 0 ){
                    $arAddress['house'] = 'д.'.$shop['PROPERTY_HOUSE_VALUE'];
                }
                if( strlen($shop['PROPERTY_OFFICE_VALUE']) > 0 ){
                    $arAddress['office'] = 'офис '.$shop['PROPERTY_OFFICE_VALUE'];
                }
                $shop['ADDRESS'] = implode(', ', $arAddress);
                $shop['PHONES'] = array();
                if( strlen($shop['PROPERTY_PHONES_VALUE']['TEXT']) > 0 ){
                    $shop['PHONES'] = tools\funcs::json_to_array($shop['PROPERTY_PHONES_VALUE']['TEXT']);
                }

                if(
                    strlen($shop['PROPERTY_SITE_VALUE']) > 0
                    &&
                    !substr_count($shop['PROPERTY_SITE_VALUE'], 'http')
                ){    $shop['PROPERTY_SITE_VALUE'] = 'http://'.$shop['PROPERTY_SITE_VALUE'];    }

                $items[$country_id][$shop_id] = $shop;
            }
        }
        return $items;
    }



    // getList
    public function getList(
        $dealer_id = false,
        $loc_id = false,
        $samovyvoz = false,
        $limit = false,
        $stop_ids = false,
        $country_id = false,
        $region_id = false
    ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $list = array();
        $sort = array("ID" => "ASC");
        $fields = Array(
            "ID", "NAME", "PROPERTY_GPS", "PROPERTY_STREET", "PROPERTY_HOUSE",
            "PROPERTY_OFFICE", "PROPERTY_PHONES", "PROPERTY_LOC_ID", "PROPERTY_WORK_DAYS",
            "PROPERTY_WORK_TIME_OT", "PROPERTY_WORK_TIME_DO",
            "PROPERTY_OBED_TIME_OT", "PROPERTY_OBED_TIME_DO", "PROPERTY_REZHIM_RABOTY"
        );
        $filter = Array(
            "IBLOCK_ID" => $this->iblock_id,
            "ACTIVE" => "Y"
        );
        if( intval($dealer_id) > 0 ){
            $fields[] = 'PROPERTY_DEALER';
            $filter['PROPERTY_DEALER'] = $dealer_id;
        }
        if( intval($loc_id) > 0 ){
            $fields[] = 'PROPERTY_LOC_ID';
            $filter['PROPERTY_LOC_ID'] = $loc_id;
        }
        if( intval($region_id) > 0 ){
            $fields[] = 'PROPERTY_REGION_ID';
            $filter['PROPERTY_REGION_ID'] = $region_id;
        }
        if( intval($country_id) > 0 ){
            $fields[] = 'PROPERTY_COUNTRY_ID';
            $filter['PROPERTY_COUNTRY_ID'] = $country_id;
        }
        if( $samovyvoz ){
            $fields[] = 'PROPERTY_SAMOVYVOZ';
            $filter['PROPERTY_SAMOVYVOZ'] = 'Y';
        }
        if( is_array($stop_ids) ){
            $filter['!ID'] = $stop_ids;
        }

        // Кеширование
        $filterCacheID = md5(json_encode($sort).json_encode($fields).json_encode($filter));
        if( intval($limit) > 0 ){    $filterCacheID .= '_limit_'.$limit;    }
        $obCache = new \CPHPCache();
        $cache_time = 12*60*60;
        $cache_id = 'dealer_shops_'.$filterCacheID;
        $cache_path = '/dealer_shops/'.$filterCacheID.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){

            $dbElements = \CIBlockElement::GetList(
                $sort, $filter, false, (intval($limit)>0?array('nTopCount'=>$limit):false), $fields
            );
            while ($element = $dbElements->GetNext()){

                $address = [];
                if( strlen($element['PROPERTY_STREET_VALUE']) > 0 ){
                    $address[] = strip_tags($element['PROPERTY_STREET_VALUE']);
                }
                if( strlen($element['PROPERTY_HOUSE_VALUE']) > 0 ){
                    $address[] = 'д.'.strip_tags($element['PROPERTY_HOUSE_VALUE']);
                }
                if( strlen($element['PROPERTY_OFFICE_VALUE']) > 0 ){
                    $address[] = 'оф.'.strip_tags($element['PROPERTY_OFFICE_VALUE']);
                }
                $element['ADDRESS'] = implode(', ', $address);

                $list[$element['ID']] = $element;
            }

            $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }



    // add
    public function add( $shop ){

        \Bitrix\Main\Loader::includeModule('iblock');

        $PROP = array();

        // Местоположения
        $PROP['LOC_ID'] = strip_tags($shop['loc_id']);
        $PROP['COUNTRY_ID'] = '';
        $PROP['REGION_ID'] = '';
        $PROP['SUB_REGION_ID'] = '';
        $loc_chain = project\bx_location::getLocChain($shop['loc_id']);
        foreach ( $loc_chain as $key => $loc_chain_item ){
            if( $loc_chain_item['PARENTS_TYPE_CODE'] == 'COUNTRY' ){
                $PROP['COUNTRY_ID'] = strip_tags($loc_chain_item['PARENTS_ID']);
            } else if( $loc_chain_item['PARENTS_TYPE_CODE'] == 'REGION' ){
                $PROP['REGION_ID'] = strip_tags($loc_chain_item['PARENTS_ID']);
            } else if( $loc_chain_item['PARENTS_TYPE_CODE'] == 'SUBREGION' ){
                $PROP['SUB_REGION_ID'] = strip_tags($loc_chain_item['PARENTS_ID']);
            }
        }

        $PROP['STREET'] = strip_tags($shop['street']);
        $PROP['HOUSE'] = strip_tags($shop['house']);
        $PROP['OFFICE'] = strip_tags($shop['office']);
        $PROP['INDEX'] = strip_tags($shop['index']);
        $PROP['GPS'] = preg_replace('/[^0-9.,]/', '', strip_tags($shop['GPS']));
        $PROP['EMAIL'] = strip_tags($shop['email']);
        $PROP['SITE'] = strip_tags($shop['site']);
        if( is_array($shop['phones']) && count($shop['phones']) > 0 ){
            foreach ( $shop['phones'] as $key => $phone ){
                if( strlen(trim($phone)) > 0 ){
                    $PROP['PHONES'][] = strip_tags(trim($phone));
                }
            }
        } else {
            $PROP['PHONES'] = array();
        }
        $PROP['PHONES'] = json_encode($PROP['PHONES']);
        $PROP['SAMOVYVOZ'] = $shop['samovyvoz']=='Y'?'Y':'N';
        if( is_array($shop['work_days']) && count($shop['work_days']) > 0 ){
            $PROP['WORK_DAYS'] = $shop['work_days'];
        } else {
            $PROP['WORK_DAYS'] = array();
        }
        $PROP['WORK_TIME_OT'] = strip_tags($shop['work_time_ot']);
        $PROP['WORK_TIME_DO'] = strip_tags($shop['work_time_do']);
        $PROP['OBED_TIME_OT'] = strip_tags($shop['obed_time_ot']);
        $PROP['OBED_TIME_DO'] = strip_tags($shop['obed_time_do']);

		global $USER;
		$userID = $USER->GetID();
		//Добавление подчиненных пользователей
		$currentUser = new project\user( $USER->GetID() );
		$userProps = $currentUser->arUser;
		if(strlen($userProps["UF_MAIN_USER"]) > 0){
			$user = new project\user( $userProps["UF_MAIN_USER"] );
			if( $user->isDealer() ){
				$userID = $user->arUser["ID"];
			}
		}
        $PROP['DEALER'] = $userID;

        $element = new \CIBlockElement;
        $fields = Array(
            "IBLOCK_ID"				=> $this->iblock_id,
            "PROPERTY_VALUES"		=> $PROP,
            "NAME"					=> strip_tags($shop['name']),
            "ACTIVE"				=> "Y"
        );

        if( $el_id = $element->Add($fields) ){
            return $el_id;

        } else {
            tools\logger::addError('Ошибка создания точки продаж дилера - '.$element->LAST_ERROR);
        }
        return false;
    }



    // update
    public function update( $shop_id, $shop ){

        \Bitrix\Main\Loader::includeModule('iblock');

        $set_prop = array();

        // Местоположения
        $set_prop['LOC_ID'] = strip_tags($shop['loc_id']);
        $set_prop['COUNTRY_ID'] = '';
        $set_prop['REGION_ID'] = '';
        $set_prop['SUB_REGION_ID'] = '';
        $loc_chain = project\bx_location::getLocChain($shop['loc_id']);
        foreach ( $loc_chain as $key => $loc_chain_item ){
            if( $loc_chain_item['PARENTS_TYPE_CODE'] == 'COUNTRY' ){
                $set_prop['COUNTRY_ID'] = strip_tags($loc_chain_item['PARENTS_ID']);
            } else if( $loc_chain_item['PARENTS_TYPE_CODE'] == 'REGION' ){
                $set_prop['REGION_ID'] = strip_tags($loc_chain_item['PARENTS_ID']);
            } else if( $loc_chain_item['PARENTS_TYPE_CODE'] == 'SUBREGION' ){
                $set_prop['SUB_REGION_ID'] = strip_tags($loc_chain_item['PARENTS_ID']);
            }
        }

        $set_prop['STREET'] = strip_tags($shop['street']);
        $set_prop['HOUSE'] = strip_tags($shop['house']);
        $set_prop['OFFICE'] = strip_tags($shop['office']);
        $set_prop['INDEX'] = strip_tags($shop['index']);
        $set_prop['GPS'] = preg_replace('/[^0-9.,]/', '', strip_tags($shop['GPS']));
        $set_prop['EMAIL'] = strip_tags($shop['email']);
        $set_prop['SITE'] = strip_tags($shop['site']);
        $set_prop['REZHIM_RABOTY'] = strip_tags($shop['rezhim_raboty']);
        if( is_array($shop['phones']) && count($shop['phones']) > 0 ){
            foreach ( $shop['phones'] as $key => $phone ){
                if( strlen(trim($phone)) > 0 ){
                    $set_prop['PHONES'][] = strip_tags(trim($phone));
                }
            }
        } else {
            $set_prop['PHONES'] = array();
        }
        $set_prop['PHONES'] = json_encode($set_prop['PHONES']);
        $set_prop['SAMOVYVOZ'] = $shop['samovyvoz']=='Y'?'Y':'N';
        if( is_array($shop['work_days']) && count($shop['work_days']) > 0 ){
            $set_prop['WORK_DAYS'] = $shop['work_days'];
        } else {
            $set_prop['WORK_DAYS'] = array();
        }
        $set_prop['WORK_TIME_OT'] = strip_tags($shop['work_time_ot']);
        $set_prop['WORK_TIME_DO'] = strip_tags($shop['work_time_do']);
        $set_prop['OBED_TIME_OT'] = strip_tags($shop['obed_time_ot']);
        $set_prop['OBED_TIME_DO'] = strip_tags($shop['obed_time_do']);

        \CIBlockElement::SetPropertyValuesEx($shop_id, $this->iblock_id, $set_prop);

        $element = new \CIBlockElement;
        $fields = Array(
            "NAME" => strip_tags($shop['name'])
        );
        if( $element->Update($shop_id, $fields) ){

            BXClearCache(true, "/dealer_shops/");

            return true;
        }
        return false;
    }






    // удаление
    public function delete( $id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $result = \CIBlockElement::Delete($id);
        if ( $result ){
            return true;
        } else {
            tools\logger::addError('Ошибка удаления точки продаж');
        }
        return false;
    }






    static function getPVZDescription( $pvz_id ){
        $descr = '';
        $pvz_el = tools\el::info( $pvz_id );
        if( intval($pvz_el['ID']) > 0 ){
            $pvz = [];
            $pvz[] = $pvz_el['NAME'];
            if( count($pvz_el['PROPERTY_WORK_DAYS_VALUE']) > 0 ){
                $days = [];
                foreach ( $pvz_el['PROPERTY_WORK_DAYS_VALUE'] as $day ){
                    $days[] = project\dealer_shop::$work_days[$day]['short_title'];
                }
                $pvz[] = implode(', ', $days);
            }
            if(
                strlen($pvz_el['PROPERTY_WORK_TIME_OT_VALUE']) > 0
                ||
                strlen($pvz_el['PROPERTY_WORK_TIME_DO_VALUE']) > 0
            ){
                $time = '';
                if( strlen($pvz_el['PROPERTY_WORK_TIME_OT_VALUE']) > 0 ){
                    $time .= 'c '.$pvz_el['PROPERTY_WORK_TIME_OT_VALUE'];
                }
                if( strlen($pvz_el['PROPERTY_WORK_TIME_DO_VALUE']) > 0 ){
                    $time .= ' до '.$pvz_el['PROPERTY_WORK_TIME_DO_VALUE'];
                }
                $pvz[] = $time;
            }
            $descr = implode(";&nbsp; ", $pvz);
        }
        return $descr;
    }





}