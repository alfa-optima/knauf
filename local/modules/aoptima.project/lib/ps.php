<?php

namespace AOptima\Project;
use AOptima\Project as project;


// Платежные системы
class ps {


    static function getByID( $id ){
        \Bitrix\Main\Loader::includeModule('sale');
        $filter = Array( "ACTIVE"=>"Y", "ID" => $id );
        $db_ptype = \CSalePaySystem::GetList(
            Array(
                "SORT"=>"ASC",
                "PSA_NAME"=>"ASC"
            ),
            $filter, false, false,
            array('ID', 'NAME', 'CODE', 'PSA_LOGOTIP', 'DESCRIPTION')
        );
        while ($ps = $db_ptype->GetNext()){
            return $ps;
        }
        return false;
    }


    // Инфо о платёж. системе
    static function getByCode( $code ){
        \Bitrix\Main\Loader::includeModule('sale');
        $arPs = null;
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 60;
        $cache_id = 'psInfoByCode_'.$code;
        $cache_path = '/psInfoByCode/'.$code.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            // запрос
            $filter = [ "ACTIVE"=>"Y", "CODE" => $code ];
            $db_ptype = \CSalePaySystem::GetList(
                [ "SORT"=>"ASC", "PSA_NAME"=>"ASC" ],
                $filter, false, false,
                [ 'ID', 'NAME', 'CODE', 'PSA_LOGOTIP', 'DESCRIPTION' ]
            );
            while ($ps = $db_ptype->GetNext()){
                $arPs = $ps;
            }
            $obCache->EndDataCache([ 'arPs' => $arPs ]);
        }
        return $arPs;
    }


    // Перечень вариантов оплаты
    static function getList( $person_type_id = 1 ){
        \Bitrix\Main\Loader::includeModule('sale');
        $list = array();
        $filter = Array( "ACTIVE"=>"Y", "!ID" => 1 );
        if( intval($person_type_id) > 0 ){
            $filter['PSA_PERSON_TYPE_ID'] = $person_type_id;
        }
        $db_ptype = \CSalePaySystem::GetList(
            Array(
                "SORT"=>"ASC",
                "PSA_NAME"=>"ASC"
            ),
            $filter, false, false,
            array('ID', 'NAME', 'CODE', 'PSA_LOGOTIP', 'DESCRIPTION')
        );
        while ($ps = $db_ptype->GetNext()){
            $list[$ps['ID']] = $ps;
        }
        return  $list;
    }



    static function getFilteredtList( $ds_id, $person_type_id = false ){
        \Bitrix\Main\Loader::includeModule('sale');
        $list = array();
        $all_ps = project\ps::getList( $person_type_id );
        $res = \CSaleDelivery::GetDelivery2PaySystem();
        while ($item = $res->GetNext()){
            if(
                $ds_id == $item['DELIVERY_ID']
                &&
                $all_ps[$item['PAYSYSTEM_ID']]
            ){
                $ps = $all_ps[$item['PAYSYSTEM_ID']];
                $list[$ps['ID']] = $ps;
            }
        }
        return  $list;
    }







}