<?php
namespace AOptima\Project;
use AOptima\Project as project;



class seo {


    const ROBOTS_PATH = '/robots';
    const MAIN_ROBOTS_PATH = '/robots/robots.txt';



    static function generate_sub_domain_robots(){
        $doc_root = $_SERVER['DOCUMENT_ROOT'];
        foreach ( project\bx_location::$main_site_locations as $loc ){
            $robots_path = $doc_root.static::ROBOTS_PATH.'/robots_'.$loc['sub_domain'].'.txt';
            if( !file_exists($robots_path) ){
                $main_robots_path = $doc_root.static::MAIN_ROBOTS_PATH;
                if( file_exists($main_robots_path) ){
                    $main_robots_content = file_get_contents($main_robots_path);
                    $file = fopen($robots_path, "w");
                    $res = fwrite($file,  $main_robots_content);
                    fclose($file);
                }
            }
        }
    }



    static function generate_sub_domain_sitemaps(){
        $doc_root = $_SERVER['DOCUMENT_ROOT'];
        $sitemaps_dir = $doc_root.'/sitemaps';
        // ищем нужные файлы
        $items = new \DirectoryIterator( $sitemaps_dir );
        foreach ( $items as $item ){
            if( $item->isFile() ){

                preg_match("/^((sitemap|sitemap-files|sitemap-iblock-[1-9]+)\.xml)$/", $item->GetFileName(), $matches, PREG_OFFSET_CAPTURE);

                if( isset($matches[1][0]) ){

                    $file_name = $matches[1][0];
                    $file_path = $sitemaps_dir.'/'.$file_name;
                    $file_content = file_get_contents( $file_path );
                    
                    // перебираем локации поддоменов
                    foreach ( project\bx_location::$main_site_locations as $loc ){
                        // папка для файлов поддомена
                        $sub_domain_dir = $sitemaps_dir.'/'.$loc['sub_domain'];
                        if( !file_exists($sub_domain_dir) ){
                            mkdir($sub_domain_dir, 0700);
                        }
                        // замена ссылок в контенте
                        $sub_domain_file_content = preg_replace('/https\:\/\/\.?kupi/', 'https://'.$loc['sub_domain'].'.kupi', $file_content);
                        $sub_domain_file_content = preg_replace('/https\:\/\/\.?kupi/', 'https://kupi', $sub_domain_file_content);
                        $sub_domain_file_content = preg_replace('/knauf\.ru\/sitemaps\//', 'knauf.ru/sitemaps/'.$loc['sub_domain'].'/', $sub_domain_file_content);
                        $sub_domain_file_content = str_replace(['sitemaps//'], 'sitemaps/', $sub_domain_file_content );

                        // сохранение файла
                        $file = fopen($sub_domain_dir.'/'.$file_name, "w");
                        $res = fwrite($file,  $sub_domain_file_content);
                        fclose($file);
                    }
                }
            }
        }
    }





}