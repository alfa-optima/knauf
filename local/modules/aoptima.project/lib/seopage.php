<?php


namespace AOptima\Project;
use AOptima\Project as project;


use Bitrix\Main\Entity;

class SeopageTable extends Entity\DataManager {

    public static function getTableName(){
        return 'aoptima_seo_pages';
    }

    public static function getMap(){
        return array(
            new Entity\IntegerField( 'ID', array(
                'primary' => true,
                'autocomplete' => true,
            )),
            new Entity\StringField( 'NAME', array(
                'required' => true
            )),
            new Entity\StringField( 'CODE', array(
                'required' => true,
            )),
            new Entity\IntegerField( 'CATEGORY' ),
            new Entity\StringField( 'SHOW_ON_MAIN_PAGE' ),
            new Entity\StringField( 'SECTION_XML_ID', array(
                'required' => true,
            )),
            new Entity\StringField( 'H1' ),
            new Entity\StringField( 'TITLE' ),
            new Entity\StringField( 'DESCRIPTION' ),
            new Entity\StringField( 'KEYWORDS' ),
            new Entity\TextField( 'TEXT' ),
            new Entity\TextField( 'FILTER_JSON' ),
        );
    }


    public static function checkTable(){
        $addTableSQL = static::getEntity()->compileDbTableStructureDump();
        if( is_array($addTableSQL) ){
            foreach ( $addTableSQL as $key => $sql ){
                $sql = str_replace('CREATE TABLE', 'CREATE TABLE IF NOT EXISTS', $sql);
                global $DB;
                $res = $DB->Query( $sql );
            }
        }
    }



    public static function addItem( $fields ){
        $result = static::add($fields);
        if ($result->isSuccess()){
            $id = $result->getId();
            return $id;
        }
        return false;
    }


    public static function updateItem( $id, $fields ){
        $result = static::update( $id, $fields );
        return $result;
    }



    public static function getByCode( $code ){
        $item = false;
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 60*60;
        $cache_id = 'SeoPageGetBySectionCode_'.$code;
        $cache_path = '/SeoPageGetBySectionCode/'.$code.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            // Ищём запись по символьному коду
            $res = project\SeopageTable::getList([
                'select' => [ '*' ],
                'filter' => [ 'CODE' => $code ],
                'limit' => 1,
            ]);
            if( $element = $res->fetch() ){
                $item = $element;
            }
        $obCache->EndDataCache(array('item' => $item));
        }
        return $item;
    }




    public static function getBySectionXMLID( $sect_xml_id ){
        $list = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30;
        $cache_id = 'SeoPageGetBySectionXMLID_'.$sect_xml_id;
        $cache_path = '/SeoPageGetBySectionXMLID/'.$sect_xml_id.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $seo_pagesss = project\SeopageTable::getList([
                'select' => [
                    'ID', 'CODE', 'NAME', 'SECTION_XML_ID', 'CATEGORY', 'SHOW_ON_MAIN_PAGE'
                ],
                'filter' => [ 'SECTION_XML_ID' => $sect_xml_id ],
                'order' => array('ID' => 'DESC'),
            ]);
            while( $seo_page = $seo_pagesss->fetch() ){
                $list[] = [
                    'NAME' => $seo_page['NAME'],
                    'SECTION_PAGE_URL' => '/'.$seo_page['CODE'].'/',
                    'CATEGORY' => $seo_page['CATEGORY'],
                    'SHOW_ON_MAIN_PAGE' => $seo_page['SHOW_ON_MAIN_PAGE']=='Y'?'Y':'N'
                ];
            }
        $obCache->EndDataCache([ 'list' => $list ]);
        }
        return $list;
    }



    public static function allList(){
        $list = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'all_seo_pages';
        $cache_path = '/all_seo_pages/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $seo_pagesss = project\SeopageTable::getList([
                'select' => [
                    'ID', 'CODE', 'NAME', 'CATEGORY', 'SHOW_ON_MAIN_PAGE', 'SECTION_XML_ID'
                ],
                'order' => array('ID' => 'DESC'),
            ]);
            while( $seo_page = $seo_pagesss->fetch() ){
                $list[] = [
                    'NAME' => $seo_page['NAME'],
                    'CODE' => $seo_page['CODE'],
                    'SECTION_PAGE_URL' => '/'.$seo_page['CODE'].'/',
                    'CATEGORY' => $seo_page['CATEGORY'],
                    'SECTION_XML_ID' => $seo_page['SECTION_XML_ID'],
                    'SHOW_ON_MAIN_PAGE' => $seo_page['SHOW_ON_MAIN_PAGE']=='Y'?'Y':'N'
                ];
            }
        $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }




    public static function getArFilter( $seoPageCode, $section_id ){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $arFilter = [];    $catalogFilter = [];
        $seo_page = project\SeopageTable::getByCode( $seoPageCode );
        $section = \AOptima\Tools\section::info( $section_id );
        if( intval($seo_page['ID']) > 0 && intval($section['ID']) > 0 ){
            $filter_json = $seo_page['FILTER_JSON'];
            if( strlen($filter_json) > 0 ){
                $arFilter = \AOptima\Tools\funcs::json_to_array($filter_json);
            }
            if( isset($arFilter['PRICE_FROM']) ){
                $catalogFilter['>=CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID] = $arFilter['PRICE_FROM'];
            }
            if( isset($arFilter['PRICE_TO']) ){
                $catalogFilter['<=CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID] = $arFilter['PRICE_TO'];
            }
            if( is_array($arFilter['PROP_VALUES']) ){
                foreach ( $arFilter['PROP_VALUES'] as $prop_code => $values ){
                    $prop = \AOptima\Tools\prop::getByCode( $section['IBLOCK_ID'], $prop_code );
                    if( $prop['PROPERTY_TYPE'] == 'L' ){
                        $catalogFilter['=PROPERTY_'.$prop['ID']] = $values;
                    } else if( $prop['PROPERTY_TYPE'] == 'N' ){
                        if( isset($values['MIN']) ){
                            $catalogFilter['>=PROPERTY_'.$prop['ID']] = $values['MIN'];
                            $catalogFilter['<=PROPERTY_'.$prop['ID']] = $values['MAX'];
                        }
                    } else {
                        if( is_array($values) ){
                            $catalogFilter['PROPERTY_'.$prop['ID']] = $values;
                        } else if( strlen($values) > 0 ){
                            $catalogFilter['PROPERTY_'.$prop['ID']][] = $values;
                        }
                    }
                }
            }
        }
        return [
            'arFilter' => $arFilter,
            'catalogFilter' => $catalogFilter,
        ];
    }




}