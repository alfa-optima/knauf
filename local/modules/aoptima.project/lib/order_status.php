<?php

namespace AOptima\Project;
use AOptima\Project as project;


use Bitrix\Main,
    Bitrix\Main\Localization\Loc as Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Main\Application,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem,
    Bitrix\Sale,
    Bitrix\Sale\Basket,
    Bitrix\Sale\Order,
    Bitrix\Sale\DiscountCouponsManager,
    Bitrix\Main\Context,
    Bitrix\Sale\Internals;



class order_status {


    const LIST_IBLOCK_ID = 9;
    const HIBLOCK_ID = 6;





    // Получение статуса заказа
    public function get( $obOrder, $propValues ){
        $status = project\order::getPropValue( $propValues, 'STATUS' );
        if( strlen($status) > 0 ){} else {
            $status = 'A';
            project\order::setPropValue( $obOrder, 'STATUS', $status );
        }
        return $status;
    }



    // Установка статуса
    public function set( $order, $status ){
        if( is_object($order) ){
            $obOrder = $order;
        } else {
            $obOrder = Sale\Order::load( $order );
        }
        project\order::setPropValue( $obOrder, 'STATUS', $status );
    }









    // getByID
    static function getByID( $id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $item = false;
        $filter = Array(
            "IBLOCK_ID" => static::LIST_IBLOCK_ID,
            "ACTIVE" => "Y",
            "ID" => $id
        );
        $fields = Array( "ID", "NAME", "CODE" );
        $dbElements = \CIBlockElement::GetList(
            array("SORT"=>"ASC"), $filter, false, false, $fields
        );
        while ($element = $dbElements->GetNext()){
            $item = $element;
        }
        return  $item;
    }




    // getList
    static function getList(){
        \Bitrix\Main\Loader::includeModule('iblock');
        $list = array();
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'order_status_list';
        $cache_path = '/order_status_list/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $filter = Array(
                "IBLOCK_ID" => static::LIST_IBLOCK_ID,
                "ACTIVE" => "Y"
            );
            $fields = Array( "ID", "NAME", "CODE" );
            $dbElements = \CIBlockElement::GetList(
                array("SORT"=>"ASC"), $filter, false, false, $fields
            );
            while ($element = $dbElements->GetNext()){
                $list[$element["CODE"]] = $element;
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        return  $list;
    }







}