<?php
namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('sale');
use Bitrix\Sale;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;



class bx_location {
    
    
    static $main_site_locations = [

        // Москва
        84 => [ 'id' => 84, 'sub_domain' => 'msk', 'where' => 'в Москве', 'region' => 2 ],
        // Санкт-Петербург
        85 => [ 'id' => 85, 'sub_domain' => 'spb', 'where' => 'в Санкт-Петербурге', 'region' => 19 ],

        // Ангарск
        2414 => [ 'id' => 2414, 'sub_domain' => 'angarsk', 'where' => 'в Ангарске'],
        // Барнаул
        2719 => [ 'id' => 2719, 'sub_domain' => 'barnaul', 'where' => 'в Барнауле', 'region' => 69 ],
        // Белгород
        210 => [ 'id' => 210, 'sub_domain' => 'belgorod', 'where' => 'в Белгороде', 'region' => 3 ],
        // Брянск
        697 => [ 'id' => 697, 'sub_domain' => 'bryansk', 'where' => 'в Брянске', 'region' => 16 ],
        // Благовещенск
        2916 => [ 'id' => 2916, 'sub_domain' => 'blagoveshensk', 'where' => 'в Благовещенске', 'region' => 75 ],
        // Воронеж
        740 => [ 'id' => 740, 'sub_domain' => 'voronezh', 'where' => 'в Воронеже', 'region' => 17 ],
        // Екатеринбург
        2203 => [ 'id' => 2203, 'sub_domain' => 'ekaterinburg', 'where' => 'в Екатеринбурге', 'region' => 57 ],
        // Казань
        1558 => [ 'id' => 1558, 'sub_domain' => 'kazan', 'where' => 'в Казани', 'region' => 43 ],
        // Калуга
        280 => [ 'id' => 280, 'sub_domain' => 'kaluga', 'where' => 'в Калуге', 'region' => 5 ],
        // Кемерово
        2582 => [ 'id' => 2582, 'sub_domain' => 'kemerovo', 'where' => 'в Кемерово', 'region' => 65 ],
        // Киров
        1641 => [ 'id' => 1641, 'sub_domain' => 'kirov', 'where' => 'в Кирове', 'region' => 45 ],
        // Комсомольск-на-Амуре
        2894 => [ 'id' => 2894, 'sub_domain' => 'komsomolsk', 'where' => 'в Комсомольске-на-Амуре'],
        // Кострома
        330 => [ 'id' => 330, 'sub_domain' => 'kostroma', 'where' => 'в Костроме', 'region' => 6 ],
        // Краснодар
        1132 => [ 'id' => 1132, 'sub_domain' => 'krasnodar', 'where' => 'в Краснодаре', 'region' => 29 ],
        // Красноярск
        2468 => [ 'id' => 2468, 'sub_domain' => 'krasnoyarsk', 'where' => 'в Красноярске', 'region' => 63 ],
        // Курган
        2281 => [ 'id' => 2281, 'sub_domain' => 'kurgan', 'where' => 'в Кургане', 'region' => 58 ],
        // Курск
        365 => [ 'id' => 365, 'sub_domain' => 'kursk', 'where' => 'в Курске', 'region' => 7 ],
        // Иваново
        242 => [ 'id' => 242, 'sub_domain' => 'ivanovo', 'where' => 'в Иваново', 'region' => 4 ],
        // Ижевск
        1770 => [ 'id' => 1770, 'sub_domain' => 'udm', 'where' => 'в Ижевске', 'region' => 47 ],
        // Иркутск
        2415 => [ 'id' => 2415, 'sub_domain' => 'irkutsk', 'where' => 'в Иркутске', 'region' => 62 ],
        // Липецк
        403 => [ 'id' => 403, 'sub_domain' => 'lipetsk', 'where' => 'в Липецке', 'region' => 8 ],
        // Набережные челны
        1559 => [ 'id' => 1559, 'sub_domain' => 'naberezhnye-chelny', 'where' => 'в Набережных челнах'],
        // Нижний Новгород
        1698 => [ 'id' => 1698, 'sub_domain' => 'nn', 'where' => 'в Нижнем Новгороде', 'region' => 46 ],
        // Новокузнецк
        2585 => [ 'id' => 2585, 'sub_domain' => 'novokuznetsk', 'where' => 'в Новокузнецке'],
        // Новосибирск
        2614 => [ 'id' => 2614, 'sub_domain' => 'nsk', 'where' => 'в Новосибирске', 'region' => 66 ],
        // Омск
        2659 => [ 'id' => 2659, 'sub_domain' => 'omsk', 'where' => 'в Омске', 'region' => 67 ],
        // Орёл
        429 => [ 'id' => 429, 'sub_domain' => 'orel', 'where' => 'в Орле', 'region' => 9 ],
        // Пермь
        1869 => [ 'id' => 1869, 'sub_domain' => 'perm', 'where' => 'в Перми', 'region' => 50 ],
        // Петрозаводск
        898 => [ 'id' => 898, 'sub_domain' => 'petrozavodsk', 'where' => 'в Петрозаводске', 'region' => 21 ],
        // Ростов-на-дону
        1248 => [ 'id' => 1248, 'sub_domain' => 'rostov-na-dony', 'where' => 'в Ростове-на-дону', 'region' => 31 ],
        // Рязань
        461 => [ 'id' => 461, 'sub_domain' => 'ryazan', 'where' => 'в Рязани', 'region' => 10 ],
        // Ставрополь
        1442 => [ 'id' => 1442, 'sub_domain' => 'stavropol', 'where' => 'в Ставрополе', 'region' => 38 ],
        // Сыктывкар
        875 => [ 'id' => 875, 'sub_domain' => 'komi', 'where' => 'в Сыктывкаре', 'region' => 20 ],
        // Сургут
        2336 => [ 'id' => 2336, 'sub_domain' => 'surgut', 'where' => 'в Сургуте', 'region' => 60 ],
        // Тамбов
        539 => [ 'id' => 539, 'sub_domain' => 'tambov', 'where' => 'в Тамбове', 'region' => 12 ],
        // Тула
        627 => [ 'id' => 627, 'sub_domain' => 'tula', 'where' => 'в Туле', 'region' => 14 ],
        // Тюмень
        2176 => [ 'id' => 2176, 'sub_domain' => 'tyumen', 'where' => 'в Тюмени', 'region' => 56 ],
        // Улан-Удэ
        2790 => [ 'id' => 2790, 'sub_domain' => 'ulan-ude', 'where' => 'в Улан-Удэ', 'region' => 64 ],
        // Ульяновск
        2090 => [ 'id' => 2090, 'sub_domain' => 'uln', 'where' => 'в Ульяновске', 'region' => 54 ],
        // Уфа
        2015 => [ 'id' => 2015, 'sub_domain' => 'ufa', 'where' => 'в Уфе', 'region' => 53 ],
        // Хабаровск
        2892 => [ 'id' => 2892, 'sub_domain' => 'khabarovsk', 'where' => 'в Хабаровске', 'region' => 74 ],
        // Челябинск
        2354 => [ 'id' => 2354, 'sub_domain' => 'chelyabinsk', 'where' => 'в Челябинске', 'region' => 61 ],
        // Якутск
        2969 => [ 'id' => 2969, 'sub_domain' => 'yakutsk', 'where' => 'в Якутске', 'region' => 78 ],
        // Ярославль
        669 => [ 'id' => 669, 'sub_domain' => 'yaroslavl', 'where' => 'в Ярославле', 'region' => 15 ],
    ];

    static $sd_iblock = 19;

    static function cityPostfix( $onlyForMainLocs = false ){
        $arURI = tools\funcs::arURI( tools\funcs::pureURL() );
        $cityPostfix = '';
        $isMainLoc = false;
        $seo_page = project\SeopageTable::getByCode(tools\funcs::arURI()[1]);
        if(
            $arURI[1] == 'selling_points'
            ||
            $arURI[1] == 'catalog'
            ||
            tools\funcs::isMain()
            ||
            $seo_page
        ){
            $locs = project\bx_location::$main_site_locations;
            $cityPostfix = 'в г.'.$_SESSION['LOC']['CITY'];
            if( $locs[ $_SESSION['LOC']['CITY_LOC_ID'] ] ){   $isMainLoc = true;   }
            if( $locs[ $_SESSION['LOC']['CITY_LOC_ID'] ]['where'] ){
                $cityPostfix = $locs[ $_SESSION['LOC']['CITY_LOC_ID'] ]['where'];
            }
        }
        if( $onlyForMainLocs && !$isMainLoc ){    $cityPostfix = '';    }
        return $cityPostfix;
    }



    static function main_site_locations_info(){
        $list = array();

        $cacheID = md5(json_encode(array_keys(static::$main_site_locations)));

        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'main_site_locations_'.$cacheID;
        $cache_path = '/main_site_locations/'.$cacheID.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $res = \Bitrix\Sale\Location\LocationTable::getList(array(
                'filter' => array(
                    'ID' => array_keys(static::$main_site_locations),
                    '=NAME.LANGUAGE_ID' => 'ru'
                ),
                'select' => array(
                    'ID',
                    'NAME_RU' => 'NAME.NAME'
                ),
            ));
            while($item = $res->fetch()){
                $list[$item['ID']] = $item;
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }




    static function getKnaufLoc(){

        global $APPLICATION;

        $arURI = tools\funcs::arURI();
        $base_domain = \Bitrix\Main\Config\Option::get('main', 'server_name');
        $mainLocs = project\bx_location::$main_site_locations;
        
        // Если мы находимся на основном домене
        if( $_SERVER['SERVER_NAME'] == $base_domain ){

            // Если город установлен в куки
            $cookie_loc_id = $APPLICATION->get_cookie('loc_id');
            if( intval($cookie_loc_id) > 0 ){
                
                // просто получаем инфу по ID местоположения
                $loc = project\bx_location::getByID($cookie_loc_id);
                if( intval($loc['ID']) > 0 ){} else {
                    $loc = project\geolocation::getLoc('Москва');
                }

                // Если местоположение в списке основных
                $locSubDomain = $mainLocs[$loc['ID']]['sub_domain'];
                if(
                    $locSubDomain
                    &&
                    $arURI[1] != 'bitrix'
                ){
                    // РЕДИРЕКТ на поддомен
                    $loc_url = $_SERVER['REQUEST_SCHEME'].'://'.$locSubDomain.'.'.$base_domain.$_SERVER['REQUEST_URI'];
                    //LocalRedirect($loc_url);
                }

            // город НЕ устанлен в куки
            } else {
                
                // Получаем инфу через геолокацию
                $loc = project\geolocation::getLoc();
                if( intval($loc['ID']) > 0 ){} else {
                    $loc = project\geolocation::getLoc('Москва');
                }
            }

        // Если находимся на поддомене
        } else {

            // Определяем местоположение по поддомену
            $arDomain = explode('.', $_SERVER['SERVER_NAME']);
            $subDomain = $arDomain[0];
            foreach ( $mainLocs as $loc_id => $arMainLoc ){
                if( $arMainLoc['sub_domain'] == $subDomain ){
                    $loc = project\bx_location::getByID($loc_id);
                }
            }
            if( intval($loc['ID']) > 0 )
            {
                // Если город установлен в куки
                $cookie_loc_id = $APPLICATION->get_cookie('loc_id');
                if( intval($cookie_loc_id) > 0 ){
                    // если город в куке отличается от города в поддомене
                    if($cookie_loc_id != $loc['ID'])
                    {
                        $locCookie = project\bx_location::getByID($cookie_loc_id);

                        // пропишем регион для Москвы и СПб (т.к. у них нет родителей)
                        if($locCookie["ID"] == 84) $locCookie["REGION_ID"] = 2;
                        if($loc["ID"] == 84) $loc["REGION_ID"] = 2;
                        if($locCookie["ID"] == 85) $locCookie["REGION_ID"] = 19;
                        if($loc["ID"] == 85) $loc["REGION_ID"] = 19;

                        // получить область для $locCookie
                        // если она совпадает с областью для выбранного поддомена
                        if ($locCookie["REGION_ID"] == $loc["REGION_ID"])
                        {
                            // возвращаем $locCookie
                            $loc = $locCookie;
                        }
                        // если не совпадает, возвращаем $loc
                    }
                }
            }
            else
            {
                $loc = project\geolocation::getLoc('Москва');
            }
        }

        $knauf_loc = array(
            'CITY' => $loc['NAME_RU'],
            'CITY_LOC_ID' => $loc['ID']
        );
        $locChain = project\bx_location::getLocChain($loc['ID']);
        foreach ( $locChain as $key => $item ){
            if( $item['PARENTS_TYPE_CODE'] == 'REGION' ){
                $knauf_loc['REGION'] = $item['PARENTS_NAME_RU'];
                $knauf_loc['REGION_ID'] = $item['PARENTS_ID'];
            }
            if( $item['PARENTS_TYPE_CODE'] == 'COUNTRY' ){
                $knauf_loc['COUNTRY'] = $item['PARENTS_NAME_RU'];
            }
        }

        return $knauf_loc;
    }


    static function getSDForLoc($locID, $regID)
    {
        if(!$locID && !$regID) return false;

        \Bitrix\Main\Loader::includeModule('iblock');

        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'getSDForLoc'.md5($locID."_".$regID);
        $cache_path = '/getSDForLoc/'.md5($locID."_".$regID).'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $item = false;

            $arLocs = Array();
            if($locID) $arLocs[] = $locID;
            if($regID) $arLocs[] = $regID;

            $dbSDs = \CIBlockElement::GetList(
                Array(),
                Array(
                    "IBLOCK_ID" => self::$sd_iblock,
                    "PROPERTY_LOCATIONS" => $arLocs,
                    "ACTIVE" => "Y"
                ),
                false,
                false,
                Array(
                    "ID",
                    "NAME"
                )
            );
            if($arSD = $dbSDs->Fetch())
            {
                $item = $arSD;
            }

            $obCache->EndDataCache(array('item' => $item));
        }
        return $item;
    }

    // getByName
    static function getByName( $term ){
        $term = trim(strip_tags($term));
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'locByName'.md5($term);
        $cache_path = '/locByName/'.md5($term).'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $item = false;
            $locs = \Bitrix\Sale\Location\LocationTable::getList(array(
                'filter' => array(
                    '=NAME.NAME' => $term,
                    '=NAME.LANGUAGE_ID' => 'ru',
                    'TYPE.CODE' => array('CITY')
                ),
                'select' => array(
                    'ID',
                    'NAME_RU' => 'NAME.NAME'
                ),
                'limit' => 1
            ));
            while( $loc = $locs->fetch() ){
                $item = $loc;
            }
        $obCache->EndDataCache(array('item' => $item));
        }
        return $item;
    }

    static function getCityOrRegionByName( $term ){
        $term = trim(strip_tags($term));
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'locByNameCityOrRegion'.md5($term);
        $cache_path = '/locByNameCityOrRegion/'.md5($term).'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $item = false;
            $locs = \Bitrix\Sale\Location\LocationTable::getList(array(
                'filter' => array(
                    '=NAME.NAME' => $term,
                    '=NAME.LANGUAGE_ID' => 'ru',
                    'TYPE.CODE' => array('CITY', 'REGION')
                ),
                'select' => array(
                    'ID',
                    'NAME_RU' => 'NAME.NAME',
                    'TYPE_CODE' => 'TYPE.CODE',
                ),
                'limit' => 1
            ));
            while( $loc = $locs->fetch() ){
                $item = $loc;
            }
            $obCache->EndDataCache(array('item' => $item));
        }
        return $item;
    }


    // getByID
    static function getByID( $id ){
        $item = false;
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'locByID_'.$id;
        $cache_path = '/locByID/'.$id.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $locs = \Bitrix\Sale\Location\LocationTable::getList(array(
                'filter' => array(
                    'ID' => $id,
                    '=NAME.LANGUAGE_ID' => 'ru',
                    '=PARENT.NAME.LANGUAGE_ID' => 'ru',
                ),
                'select' => array(
                    '*',
                    'NAME_RU' => 'NAME.NAME',
                    'TYPE_CODE' => 'TYPE.CODE',
                    'PARENT_NAME_RU' => 'PARENT.NAME.NAME',
                    'ZIP' => 'EXTERNAL.XML_ID'
                ),
                'limit' => 1
            ));
            while( $loc = $locs->fetch() ){
                $item = $loc;
            }
        $obCache->EndDataCache(array('item' => $item));
        }
        return $item;
    }



    static function getCountries(){
        $countries = array();
        $locs = \Bitrix\Sale\Location\LocationTable::getList(array(
            'filter' => array(
                '=NAME.LANGUAGE_ID' => 'ru',
                "TYPE.CODE" => array('COUNTRY')
            ),
            'select' => array(
                '*',
                'NAME_RU' => 'NAME.NAME',
                'TYPE_CODE' => 'TYPE.CODE'
            ),
        ));
        while( $loc = $locs->fetch() ){
            $countries[$loc['ID']] = $loc;
        }
        return $countries;
    }




    static function getLocChain( $id ){
        $list = array();
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'locChain_'.$id;
        $cache_path = '/locChain/'.$id.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $res = \Bitrix\Sale\Location\LocationTable::getList(array(
                'filter' => array(
                    '=ID' => $id,
                    '=PARENTS.NAME.LANGUAGE_ID' => 'ru',
                    '=PARENTS.TYPE.NAME.LANGUAGE_ID' => 'ru',
                ),
                'select' => array(
                    'PARENTS_ID' => 'PARENTS.ID',
                    'PARENTS_NAME_RU' => 'PARENTS.NAME.NAME',
                    'PARENTS_TYPE_CODE' => 'PARENTS.TYPE.CODE',
                    'PARENTS_TYPE_NAME_RU' => 'PARENTS.TYPE.NAME.NAME'
                ),
                'order' => array(
                    'PARENTS.DEPTH_LEVEL' => 'asc'
                )
            ));
            while($item = $res->fetch()){
                // Москва
                if( $id == 84 && $item['PARENTS_TYPE_CODE'] == 'CITY' ){
                    $regionLoc = static::getByID(2);
                    // Санкт-Петербург
                } if( $id == 85 && $item['PARENTS_TYPE_CODE'] == 'CITY' ){
                    $regionLoc = static::getByID(19);
                }
                if( intval($regionLoc['ID']) > 0 ){
                    $list[$regionLoc['ID']] = array(
                        'PARENTS_ID' => $regionLoc['ID'],
                        'PARENTS_NAME_RU' => $regionLoc['NAME_RU'],
                        'PARENTS_TYPE_CODE' => $regionLoc['TYPE_CODE'],
                        'PARENTS_TYPE_NAME_RU' => 'Область'
                    );
                }
                $list[$item['PARENTS_ID']] = $item;
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }






}