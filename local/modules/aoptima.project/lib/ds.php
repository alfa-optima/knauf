<?php

namespace AOptima\Project;
use AOptima\Project as project;



class ds {


    static function getByID( $id ){
        \Bitrix\Main\Loader::includeModule('sale');
        $db_dtype = \CSaleDelivery::GetList(
            Array("SORT"=>"ASC", "NAME"=>"ASC"),
            Array(
                "ACTIVE"=>"Y",
                "LID" => SITE_ID,
                "ID" => $id
            ),
            false, false, array()
        );
        while ($ds = $db_dtype->GetNext()){
            return $ds;
        }
        return false;
    }


    // Варианты доставки
    static function getList(){
        \Bitrix\Main\Loader::includeModule('sale');
        $list = array();
        $db_dtype = \CSaleDelivery::GetList(
            Array("SORT"=>"ASC", "NAME"=>"ASC"),
            Array(
                "ACTIVE"=>"Y",
                "LID" => SITE_ID,
            ),
            false, false, array()
        );
        while ($ds = $db_dtype->GetNext()){
            // Тип доставки ///////
            $ds['delivType'] = 'delivery';
            if( $ds['ID'] == 2 ){    $ds['delivType'] = 'samovyvoz';    }
            ///////////////////////
            $list[$ds['ID']] = $ds;
        }
        return  $list;
    }







}