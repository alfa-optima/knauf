<?php

namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


class dealer_yml_file {

    use \files___trait;

    const MAX_FILE_SIZE = 5242880;

    const TEMP_UPLOAD_DIR = "dealerYMLFile";
    const TEMP_UPLOAD_PATH = "/upload/dealerYMLFile/";


    protected $FILES = false;
    protected $doc_root = false;
    protected $fileTypes = array(
        'xml'
    );
    protected $fileTypesInc = array(
        'plain'
    );
    public $file_name = false;
    protected $new_file_name = false;
    public $file_path = false;
    protected $width = false;
    protected $height = false;
    public $file_moved = false;
    protected $file_extension = false;





    // конструктор объекта
    function __construct( $files = false, $user_id = false ){
        if( $files ){
            $this->doc_root = $_SERVER["DOCUMENT_ROOT"];
            // Перемещение файла во временную папку
            if ( $this->moveFile($files, $user_id) ){
                $this->file_moved = true;
            }
        }
    }


    public function getFileTypes(){
        return $this->fileTypes;
    }



    // Загрузка файла
    public function upload(){
        $check_status = $this->checkAsFile();
        
        if ( $check_status == 'ok' ){
            
            if( file_exists( $this->file_path ) ){

                return array(
                    'status' => 'ok',
                    'file_path' => $this->file_path
                );

            } else {

                unlink($_SESSION['YML_FILE_PATH']);

                return array(
                    'status' => 'error',
                    'text' => 'Не удалось загрузить файл'
                );
            }

        } else {

            unlink($_SESSION['YML_FILE_PATH']);

            return array('status' => 'error', 'text' => $check_status);
        }
    }







}