<?php

namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

use Bitrix\Sale;
use Bitrix\Sale\Internals;


class popular_goods {

    const ORDERS_CNT = 50;
    const MAX_PRODUCTS_CNT = 8;



    // ID популярных товаров
    static function getIDS(){
        \Bitrix\Main\Loader::includeModule('iblock');
        $tpIblockCode = project\catalog::TP_IBLOCK_CODE;
        $product_ids = [];   $sku_ids = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*60;
        $cache_id = 'mainPopGoods';
        $cache_path = '/mainPopGoods/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            // Заказы для анализа
            $orders = static::getLastOrders();
            foreach ($orders as $order){
                // Объект заказа
                $obOrder = Sale\Order::load($order['ID']);
                // Корзина заказа
                $basket = $obOrder->getBasket();
                foreach ( $basket as $basketItem ){
                    // Собираем ID ТП
                    $sku_id = $basketItem->getProductId();
                    $sku = tools\el::info($sku_id);
                    if( intval($sku['ID']) > 0 && $sku['ACTIVE'] == 'Y' ){
                        $sku_ids[] = $sku['ID'];
                    }
                }
            }
            $sku_ids = array_unique($sku_ids);
            // Собираем основные товары ТП
            if( count($sku_ids) > 0 ){
                foreach ( $sku_ids as $sku_id ){
                    $filter = Array(
                        "IBLOCK_CODE" => $tpIblockCode,
                        "ACTIVE" => "Y",
                        "=ID" => $sku_id
                    );
                    $fields = Array( "ID", "NAME", "PROPERTY_CML2_LINK" );
                    $skus = \CIBlockElement::GetList(
                        array("SORT"=>"ASC"), $filter, false,
                        array('nTopCount' => 1), $fields
                    );
                    while ($sku = $skus->GetNext()){
                        $p = tools\el::info($sku['ID']);
                        if( intval($p['ID']) > 0 && $p['ACTIVE'] == 'Y' ){
                            $product_ids[$sku['PROPERTY_CML2_LINK_VALUE']]++;
                        }
                    }
                }
                arsort($product_ids);
                $product_ids = array_keys($product_ids);
            }
        $obCache->EndDataCache(array('product_ids' => $product_ids));
        }
        $product_ids = array_slice($product_ids, 0, static::MAX_PRODUCTS_CNT);
        return $product_ids;
    }



    // Заказы для анализа
    static function getLastOrders(){
        $list = [];
        $filter_array = array();
        $orders = \Bitrix\Sale\OrderTable::getList(array(
            'filter' => $filter_array,
            'select' => array( 'ID', 'USER_ID' ),
            'order' => array('ID' => 'DESC'),
            'limit' => static::ORDERS_CNT
        ));
        while ($order = $orders->fetch()){
            $list[] = $order;
        }
        return $list;
    }





}