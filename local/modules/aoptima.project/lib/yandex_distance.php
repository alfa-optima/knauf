<?php

namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;


class yandex_distance {

    const YANDEX_API_KEY = 'df3f37f8-2493-4759-96b8-0e81bfe180da';



    // Получение расстояния по координатам
    static function get(
        $latitude_from, $longitude_from,
        $latitude_to, $longitude_to
    ){

        $coords_hash = md5($latitude_from.$longitude_from.$latitude_to.$longitude_to);
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'coords_distance_'.$coords_hash;
        $cache_path = '/coords_distance/'.$coords_hash.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){

            $distance = static::requestYandexMatrix(
                $latitude_from, $longitude_from,
                $latitude_to, $longitude_to
            );

        $obCache->EndDataCache(array('distance' => $distance));
        }
        if( !$distance ){
            BXClearCache(true, '/coords_distance/'.$coords_hash.'/');
        }

        return $distance;
    }



    // Запрос расстояния у яндекса
    static function requestYandexMatrix(
        $latitude_from, $longitude_from,
        $latitude_to, $longitude_to
    ){
        $distance = false;
        $client = new Client(array( 'timeout' => 10 ));
        try {

            $url = 'https://api.routing.yandex.net/v1.0.0/distancematrix?origins='.$latitude_from.','.$longitude_from.'&destinations='.$latitude_to.','.$longitude_to.'&mode=driving&apikey='.static::YANDEX_API_KEY;
            
            $response = $client->request( 'GET', $url );

            if( $response->getStatusCode() == 200 ){

                $body = $response->getBody();
                $json = $body->getContents();
                $res = tools\funcs::json_to_array($json);
                
                if(
                    isset($res['rows'][0]['elements'][0]['status'])
                    &&
                    $res['rows'][0]['elements'][0]['status'] == 'OK'
                ){
                    if(
                        isset($res['rows'][0]['elements'][0]['distance']['value'])
                        &&
                        $res['rows'][0]['elements'][0]['distance']['value']
                    ){
                        $distance = $res['rows'][0]['elements'][0]['distance']['value'];
                    }
                } else if(
                    isset($res['rows'][0]['elements'][0]['status'])
                    &&
                    $res['rows'][0]['elements'][0]['status'] != 'OK'
                ){
                    tools\logger::addError('Яндекс Матрица расстояний - API отдало статус '.$res['rows'][0]['elements'][0]['status'])." - ".$json;
                } else if( !isset($res['rows'][0]['elements'][0]['status']) ){
                    tools\logger::addError('Яндекс Матрица расстояний - расчёт не удался, статус не определён'." - ".$json);
                }
            } else {
                tools\logger::addError('Яндекс Матрица расстояний - код ответа '.$response->getStatusCode());
            }
        } catch(RequestException $ex){
            tools\logger::addError('Яндекс Матрица расстояний - '.$ex->getMessage());
        }
        return $distance;
    }





}