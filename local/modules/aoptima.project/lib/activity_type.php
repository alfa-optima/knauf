<?php
namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('iblock');



// Тип деятельности
class activity_type {

    const IBLOCK_ID = 6;



    public function getList(){
        $list = array();
        $filter = Array(
            "IBLOCK_ID" => static::IBLOCK_ID,
            "ACTIVE" => "Y"
        );
        $fields = Array( "ID", "NAME", "DATE_CREATE", "DETAIL_PAGE_URL" );
        $dbElements = \CIBlockElement::GetList(
            array("NAME"=>"ASC"), $filter, false, false, $fields
        );
        while ($element = $dbElements->GetNext()){
            $list[$element['ID']] = array(
                'ID' => $element['ID'],
                'NAME' => $element['NAME'],
            );
        }
        return $list;
    }






}