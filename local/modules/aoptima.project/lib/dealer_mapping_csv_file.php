<?php

namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


class dealer_mapping_csv_file {

    use \files___trait;

    const MAX_FILE_SIZE = 5242880;

    const TEMP_UPLOAD_DIR = "dealerMappingCSVFile";
    const TEMP_UPLOAD_PATH = "/upload/dealerMappingCSVFile/";


    protected $FILES = false;
    protected $doc_root = false;
    protected $fileTypes = array(
        'plain'
    );
    protected $fileTypesInc = array(
        'plain'
    );
    protected $file_name = false;
    protected $new_file_name = false;
    protected $file_path = false;
    protected $width = false;
    protected $height = false;
    protected $file_moved = false;
    protected $file_extension = false;





    // конструктор объекта
    function __construct( $files, $user_id ){
        $this->doc_root = $_SERVER["DOCUMENT_ROOT"];
        // Перемещение файла во временную папку
        if ( $this->moveFile($files, $user_id) ){
            $this->file_moved = true;
        }
    }


    public function getFileTypes(){
        return $this->fileTypes;
    }

    public function getFilePath(){
        return $this->file_path;
    }



    protected function onResponse($obj){
        echo '<script type="text/javascript">
		window.parent.onDelerMappingCSVFileResponse("'.$obj.'");
		</script>';
    }



    // Загрузка файла
    public function upload(){
        $check_status = $this->checkAsFile();
        if ( $check_status == 'ok' ){
            
            if( file_exists( $this->file_path ) ){

                return array(
                    'status' => 'ok',
                    'file_path' => $this->file_path
                );

            } else {

                return array(
                    'status' => 'error',
                    'text' => 'Не удалось загрузить файл'
                );
            }

        } else {

            return array('status' => 'error', 'text' => $check_status);
        }
    }





    static function getKnaufID( $dealer_id, $client_article ){
        $knauf_id = false;
        if( strlen($client_article) > 0 ){
            global $USER;
            $dealer = tools\user::info( $dealer_id );
            $mappingNotes = [];
            if( isset($dealer['UF_ARTICLE_MAPPING']) && strlen($dealer['UF_ARTICLE_MAPPING']) > 0 ){
                $mappingNotes = tools\funcs::json_to_array($dealer['UF_ARTICLE_MAPPING']);
            }
            if( count($mappingNotes) > 0 ){
                $titlesLine = $mappingNotes[0];
                $knauf_id_key = null;
                $client_article_key = null;
                foreach ( $titlesLine as $key => $title ){
                    $titlesLine[$key] = trim(strtoupper($title));
                    if( $titlesLine[$key] == 'ARTICLE' ){
                        $client_article_key = $key;
                    } else if( $titlesLine[$key] == 'KNAUF_ID' ){
                        $knauf_id_key = $key;
                    }
                }
                unset($mappingNotes[0]);
                if( isset($client_article_key) && isset($knauf_id_key) ){
                    foreach ( $mappingNotes as $mappingNote ){
                        if( $mappingNote[$client_article_key] == $client_article ){
                            $knauf_id = trim(strip_tags($mappingNote[$knauf_id_key]));
                        }
                    }
                }
            }
        }
        $knauf_id = project\catalog::treatKnaufID( $knauf_id );
        return $knauf_id;
    }






}