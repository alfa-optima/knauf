<?php
namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('highloadblock');


class delivery_price {

    // Методы расчета расстояния
    static $distanceTypes = [
        [
            'CODE' => 'straight_line',
            'NAME' => 'Расстояние по прямой'
        ],
        [
            'CODE' => 'nearest_route',
            'NAME' => 'Расстояние по дорогам'
        ]
    ];

    // Методы расчета стоимости
    static $tariffTypes = [
        [
            'CODE' => 'distance_ranges',
            'NAME' => 'Диапазоны расстояний'
        ],
        [
            'CODE' => 'price_per_kilometer',
            'NAME' => 'Цена за километр'
        ]
    ];

    static $texts = [
        'straight_line_distance_ranges' => 'Цена не зависит от точного расстояния, но ограничивается им относительно любого адреса (Адреса точки отсчета).  Расстояние вычисляется по прямой, т.е. условно окружность на заданном расстоянии от точки определяет радиус доступной доставки по единой стоимости.',
        'nearest_route_distance_ranges' => 'Цена зависит от расстояния, определяется по зонам удаления от склада (либо другого адреса, принимаемого за точку отсчета). Расстояние рассчитывается по дорогам общего пользования с помощью Яндекс Маршрутизации. В этом случае дилер может создать свою матрицу расчета, с любым количеством зон.',
        'straight_line_price_per_kilometer' => 'Цена определяется с точностью до километра по прямой.',
        'nearest_route_price_per_kilometer' => 'Цена зависит от расстояния и определяется с точностью до километра по кратчайшему маршруту. Расстояние рассчитывается по дорогам общего пользования с помощью Яндекс Маршрутизации.',
    ];

    // Методы расчета стоимости
    static $dop_services = [
        [
            'CODE' => 'unloading_of_goods',
            'NAME' => 'Разгрузка товаров'
        ],
        [
            'CODE' => 'rise_to_the_floor',
            'NAME' => 'Подъем на этаж'
        ],
        [
            'CODE' => 'transfer_of_goods',
            'NAME' => 'Перенос товаров'
        ]
    ];

    // Диапазоны тоннажа для настроки стоимости доставки
    static $weight_ranges = [
        '<=0.5' => [
            'NAME' => 'До 0.5 т',
            'CODE' => '<=0.5',
            'RANGE' => [ 0, 0.5 ]
        ],
        '<=1.5' => [
            'NAME' => 'До 1.5 т',
            'CODE' => '<=1.5',
            'RANGE' => [ 0.5, 1.5 ]
        ],
        '<=3.5' => [
            'NAME' => 'До 3.5 т',
            'CODE' => '<=3.5',
            'RANGE' => [ 1.5, 3.5 ]
        ],
        '<=5' => [
            'NAME' => 'До 5 т',
            'CODE' => '<=5',
            'RANGE' => [ 3.5, 5 ]
        ],
        '<=10' => [
            'NAME' => 'До 10 т',
            'CODE' => '<=10',
            'RANGE' => [ 3.5, 10 ]
        ],
        '>10' => [
            'NAME' => 'Свыше 10 т',
            'CODE' => '>10',
            'RANGE' => [ 10, 100000000000 ]
        ],
    ];

    const LARGE_SIZE = 2000; // Признак крупногабаритности, мм



    // Вес корзины в тоннах
    static function getBasketWeight( $basketInfo ){
        $weight = 0;
        foreach( $basketInfo['basketProducts'] as $basketProduct ){
            $weight += $basketProduct['el']['PROPERTY_CALC_WEIGHT_VALUE'] * $basketProduct['quantity'];
        }
        return $weight/1000000;
    }


    static function getLargeBasketGoods( $basketInfo ){
        $list = [];
        foreach( $basketInfo['basketProducts'] as $basketProduct ){
            if( $basketProduct['el']['PROPERTY_MAX_GABARIT_VALUE'] >= static::LARGE_SIZE ){
                $list[] = $basketProduct;
            }
        }
        return $list;
    }


    static function calc(
        $dealerDelivLocation,  $basketInfo,  $dealer,
        $coords,  $form_data = [],  $goodsInfo
    ){

        $delivery_price = 'договорная';

        // Вес товаров, тонн
        $weight = static::getBasketWeight( $basketInfo );
        
        // Если местоположение доставки найдено у дилера
        if( strlen($dealerDelivLocation['UF_DELIV_SETTINGS']) > 0 ){

            $delivSettings = \AOptima\Tools\funcs::json_to_array($dealerDelivLocation['UF_DELIV_SETTINGS']);
            
            foreach ( $delivSettings as $key => $value ){
                unset($delivSettings[$key]);
                $key = preg_replace('/_[0-9]+$/', '', $key);
                $delivSettings[$key] = $value;
            }

            if(
                strlen($delivSettings['distanceType']) > 0
                &&
                strlen($delivSettings['tariffType']) > 0
                &&
                strlen($delivSettings['start_point_coords']) > 0
                &&
                strlen($coords) > 0
            ){

                // Расстояние
                $lat1 = trim(explode(',', $delivSettings['start_point_coords'])[0]);
                $lon1 = trim(explode(',', $delivSettings['start_point_coords'])[1]);
                $lat2 = trim(explode(',', $coords)[0]);
                $lon2 = trim(explode(',', $coords)[1]);

                // По прямой
                if( $delivSettings['distanceType'] == 'straight_line' ){
                    $order_distance = project\order::distanceByCoords($lat1, $lon1, $lat2, $lon2);
                // По дорогам
                } else if( $delivSettings['distanceType'] == 'nearest_route' ){
                    $order_distance = project\yandex_distance::get($lat1, $lon1, $lat2, $lon2);
                }

                if( $order_distance > 0 ){

                    // Диапазоны расстояний + тоннажа
                    if(
                        $delivSettings['tariffType'] == 'distance_ranges'
                        &&
                        is_array($delivSettings['distance_ranges']['distances'])
                        &&
                        count($delivSettings['distance_ranges']['distances']) > 0
                    ){

                        $distance_deliv_price = static::distanceRangesGetPrice(
                            $weight, $delivSettings, $order_distance
                        );

                    // Цена за километр (диапазоны тоннажа)
                    } else if(
                        $delivSettings['tariffType'] == 'price_per_kilometer'
                        &&
                        is_array($delivSettings['prices_per_kilometer'])
                    ){

                        $distance_deliv_price = static::getKMPrice(
                            $weight, $delivSettings, $order_distance
                        );
                    }
                }
            }

            if( isset( $distance_deliv_price ) ){

                $delivery_price = $distance_deliv_price;

                // Добавляем стоимость разгрузки
                if( $form_data['needRazgruzka'] == 'Да' ){
                    $delivery_price += static::razgruzkaPrice( $form_data, $delivSettings, $weight );
                }

                // Добавляем стоимость подъёма на этаж
                if( $form_data['needPodyem'] == 'Да' ){
                    $delivery_price += static::podyemPrice(
                        $form_data,  $delivSettings,
                        $basketInfo,  $goodsInfo
                    );
                }

                // Добавляем стоимость переноса
                if( $form_data['needPerenos'] == 'Да' ){
                    $delivery_price += static::perenosPrice( $form_data, $delivSettings, $weight );
                }

            }
        }

        return $delivery_price;
    }


    // Расчёт стоимости разгрузки
    static function razgruzkaPrice ( $form_data, $deliv_settings, $weight ){
        $sum = 0;
        if(
            $weight > 0
            &&
            // Дилер осуществляет разгрузку в принципе
            (
                is_array($deliv_settings['dop_services'])
                &&
                in_array('unloading_of_goods', $deliv_settings['dop_services'])
            )
            &&
            $deliv_settings['unloading_of_goods']['price'] > 0
        ){
            $weight = $weight * 1000;
            $sum = $weight * $deliv_settings['unloading_of_goods']['price'];
            if(
                $deliv_settings['unloading_of_goods']['min_sum'] > 0
                &&
                $sum < $deliv_settings['unloading_of_goods']['min_sum']
            ){
                $sum = $deliv_settings['unloading_of_goods']['min_sum'];
            }
        }
        return $sum;
    }


    // Расчёт стоимости подъёма на этаж
    static function podyemPrice ( $form_data, $deliv_settings, $basketInfo, $goodsInfo ){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $liftSum = 0;   $noLiftSum = 0;
        $normalGoods = $goodsInfo['normalGoods'];
        $normalGoodsWeight = $goodsInfo['normalGoodsWeight'];
        $largeGoods = $goodsInfo['largeGoods'];
        $largeGoodsWeight = $goodsInfo['largeGoodsWeight'];
        if(
            // указан этаж больше нуля
            $form_data['floor'] > 0
            &&
            // Дилер осуществляет подъём в принципе
            (
                is_array($deliv_settings['dop_services'])
                &&
                in_array('rise_to_the_floor', $deliv_settings['dop_services'])
            )
            &&
            // Указана стоимость подъёма без лифта
            $deliv_settings['rise_to_the_floor']['no_lift']['floor_price'] > 0
        ){

            // ОБЫЧНЫЕ ТОВАРЫ
            if(
                count($normalGoods) > 0
                &&
                $normalGoodsWeight > 0
            ){

                if(
                    // Лифт есть
                    (
                        $form_data['lift'] == 'лифт есть'
                        ||
                        $form_data['lift'] == 'грузовой лифт'
                    )
                    &&
                    // указана стоимость подъема на лифте
                    strlen( $deliv_settings['rise_to_the_floor']['lift']['price'] ) > 0
                ){

                    // считаем по стоимости подъёма на лифте
                    $liftSum += $normalGoodsWeight * $deliv_settings['rise_to_the_floor']['lift']['price'];

                } else {

                    // считаем по стоимости подъёма без лифта
                    $noLiftSum += $normalGoodsWeight * $deliv_settings['rise_to_the_floor']['no_lift']['floor_price'] * $form_data['floor'];
                }
            }
            
            // КРУПНОГАБАРИТНЫЕ ТОВАРЫ
            if(
                count($largeGoods) > 0
                &&
                $largeGoodsWeight > 0
                &&
                $deliv_settings['rise_to_the_floor']['not_rise_large_goods'] != 'Y'
            ){
                // считаем по стоимости подъёма без лифта
                $noLiftSum += $largeGoodsWeight * $deliv_settings['rise_to_the_floor']['no_lift']['floor_price'] * $form_data['floor'];
            }

        }

        if( $noLiftSum > 0 ){
            $no_lift_min_sum = $deliv_settings['rise_to_the_floor']['no_lift']['min_sum'];
            if(  $no_lift_min_sum > 0  &&  $noLiftSum < $no_lift_min_sum  ){
                $noLiftSum = $no_lift_min_sum;
            }
        }

        $sum = $liftSum + $noLiftSum;

        return $sum;
    }


    // Расчёт стоимости переноса
    static function perenosPrice ( $form_data, $deliv_settings, $weight ){
        $sum = 0;
        if(
            $weight > 0
            &&
            // Дилер осуществляет перенос в принципе
            (
                is_array($deliv_settings['dop_services'])
                &&
                in_array('transfer_of_goods', $deliv_settings['dop_services'])
            )
            &&
            $deliv_settings['transfer_of_goods']['price'] > 0
            &&
            $form_data['perenosDistance'] > 0
        ){
            $weight = $weight * 1000;
            $sum = $form_data['perenosDistance'] * $weight * $deliv_settings['transfer_of_goods']['price'];
            if(
                $deliv_settings['transfer_of_goods']['min_sum'] > 0
                &&
                $sum < $deliv_settings['transfer_of_goods']['min_sum']
            ){
                $sum = $deliv_settings['transfer_of_goods']['min_sum'];
            }
        }
        return $sum;
    }


    static function getKMPrice ( $weight, $delivSettings, $order_distance ){
        $km_deliv_price = null;
        $prices_per_kilometer = array_reverse($delivSettings['prices_per_kilometer']);
        foreach ( $prices_per_kilometer as $code => $price ){
            $weight_range = static::$weight_ranges[ $code ];
            if(
                strlen($price) > 0
                &&
                isset($weight_range)
                &&
                $weight <= $weight_range['RANGE'][1]
            ){    $km_deliv_price = $price;    }
        }
        if( isset($km_deliv_price) ){
            return round($order_distance/1000 * $km_deliv_price, project\catalog::ROUND);
        }
        return $km_deliv_price;
    }




    static function distanceRangesGetPrice ( $weight, $delivSettings, $order_distance ){
        $ranges_deliv_price = null;
        if(
            $delivSettings
            &&
            $weight
            &&
            $order_distance
        ){
            if(
                is_array($delivSettings['distance_ranges']['distances'])
                &&
                count($delivSettings['distance_ranges']['distances']) > 0
            ){
                array_multisort($delivSettings['distance_ranges']['distances'], SORT_ASC, SORT_NUMERIC, $delivSettings['distance_ranges']['prices']);
                foreach ( $delivSettings['distance_ranges']['distances'] as $key => $distance ){
                    $arPrice = $delivSettings['distance_ranges']['prices'][$key];
                    if(
                        ($order_distance/1000) <= $distance
                        &&
                        !$arPrices
                    ){   $arPrices = $arPrice;   }
                }
                if( is_array($arPrices) && count($arPrices) > 0 ){
                    $arPrices = array_reverse($arPrices);
                    foreach ( $arPrices as $code => $price ){
                        $weight_range = static::$weight_ranges[$code];
                        if(
                            strlen($price) > 0
                            &&
                            isset($weight_range)
                            &&
                            $weight <= $weight_range['RANGE'][1]
                        ){    $ranges_deliv_price = $price;    }
                    }
                    if( isset($ranges_deliv_price) ){
                        return round($ranges_deliv_price, project\catalog::ROUND);
                    }
                }
            }
        }
        return $ranges_deliv_price;
    }



}