<?php
namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;



class dealer extends user {



    protected $listFilter = Array(
        "ACTIVE" => "Y",
        "GROUPS_ID" => array( user::DEALERS_GROUP_ID )
    );



    // Список всех дилеров
    public function allList( $onlyEnabled = false ){
        $list = array();
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 24*60*60;
        $cache_id = 'allDealers';
        $cache_path = '/allDealers/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $list = $this->getList();
            $obCache->EndDataCache(array('list' => $list));
        }
        if( $onlyEnabled ){
            $new_list = [];
            foreach ( $list as $key => $dealer ){
                if( $dealer['UF_SHOW_IN_CATALOG'] ){
                    $new_list[ $dealer['ID'] ] = $dealer;
                }
            }
            $list = $new_list;
        }
        return $list;
    }

    // получим регионы, в которых у дилера есть ТП
    public static function GetDealerRegions($dealerID)
    {
        if(!$dealerID) return false;

        $arLocationIDs = [];
        \Bitrix\Main\Loader::includeModule('iblock');

        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 60*60;
        $cache_id = 'dealer_regions'.$dealerID;
        $cache_path = '/dealer_regions/'.$dealerID.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){

            $filter = Array(
                "IBLOCK_ID" => project\catalog::tpIblockID(),
                "ACTIVE" => "Y",
                "PROPERTY_DEALER" => $dealerID
            );
            $fields = Array( "ID", "PROPERTY_LOCATION");
            $regions = \CIBlockElement::GetList(
                array("SORT"=>"ASC"), $filter, ["PROPERTY_LOCATION"], false, $fields
            );
            while ($region = $regions->Fetch())
            {
                $arLocationIDs[] = $region['PROPERTY_LOCATION_VALUE'];
            }


            $obCache->EndDataCache(array('arLocationIDs' => $arLocationIDs));
        }

        return $arLocationIDs;
    }

    // получить дилеров, у которых есть ТП в регионе
    public static function GetDealersForLocation($regionID)
    {
        if(!$regionID) return false;

        $arDealers = Array();
        \Bitrix\Main\Loader::includeModule('iblock');

        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 60*60;
        $cache_id = 'region_dealers'.$regionID;
        $cache_path = '/region_dealers/'.$regionID.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){

            $filter = Array(
                "IBLOCK_ID" => project\catalog::tpIblockID(),
                "ACTIVE" => "Y",
                "PROPERTY_LOCATION" => $regionID
            );
            $fields = Array( "ID", "PROPERTY_DEALER");
            $dealers = \CIBlockElement::GetList(
                array("SORT"=>"ASC"), $filter, ["PROPERTY_DEALER"], false, $fields
            );

            while ($dealer = $dealers->Fetch())
            {
                $arDealers[] = $dealer;
            }

            $obCache->EndDataCache(array('arDealers' => $arDealers));
        }

        return $arDealers;
    }
}