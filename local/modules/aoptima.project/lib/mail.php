<?php
namespace AOptima\Project;
use AOptima\Project as project;




class mail {



    static function header(){
        ob_start();
            include $_SERVER['DOCUMENT_ROOT'].'/include/mailHeader.php';
            $mailHeader = ob_get_contents();
        ob_end_clean();
        return $mailHeader;
    }

	static function headerNew(){
		ob_start();
		include $_SERVER['DOCUMENT_ROOT'].'/include/mailHeaderNew.php';
		$mailHeader = ob_get_contents();
		ob_end_clean();
		return $mailHeader;
	}

	static function footerNew(){
		ob_start();
		include $_SERVER['DOCUMENT_ROOT'].'/include/mailFooterNew.php';
		$mailFooter = ob_get_contents();
		ob_end_clean();
		return $mailFooter;
	}

    static function footer(){
        ob_start();
            include $_SERVER['DOCUMENT_ROOT'].'/include/mailFooter.php';
            $mailFooter = ob_get_contents();
        ob_end_clean();
        return $mailFooter;
    }


    static function footerDealer(){
        ob_start();
            include $_SERVER['DOCUMENT_ROOT'].'/include/mailFooterDealer.php';
            $mailFooter = ob_get_contents();
        ob_end_clean();
        return $mailFooter;
    }



}