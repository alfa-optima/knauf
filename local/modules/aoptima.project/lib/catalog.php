<?php
namespace AOptima\Project;
use AOptima\Project as project;

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/classes/general/xml.php');

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

use Bitrix\Main,
Bitrix\Main\Application,
Bitrix\Main\Localization\Loc as Loc,
Bitrix\Main\Loader,
Bitrix\Main\Config\Option,
Bitrix\Sale\Delivery,
Bitrix\Sale\PaySystem,
Bitrix\Sale,
Bitrix\Sale\Order,
Bitrix\Sale\Basket,
\Bitrix\Sale\Discount,
\Bitrix\Sale\Result,
Bitrix\Sale\DiscountCouponsManager,
Bitrix\Main\Context,
Bitrix\Main\Web\Json,
Bitrix\Sale\PersonType,
Bitrix\Sale\Shipment,
Bitrix\Sale\Payment,
Bitrix\Sale\Location\LocationTable,
Bitrix\Sale\Services\Company,
Bitrix\Sale\Location\GeoIp;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;



class catalog {


    const IBLOCK_TYPE = 'catalog';
    const CATALOG_IBLOCK_CODE = 'main_catalog';
    const TP_IBLOCK_CODE = 'catalog_tp';
    const GOODS_CNT = 18;
    const PRICE_ID = 1;
    const BASE_PRICE_CODE = 'BASE';
    const ROUND = 2;
    const NDS_ID = 1;
    const FILTER_NAME = 'arrFilter';
    const DEALER_CATALOG_FILE_PATH = '/upload/productsForImport.csv';
    const DEALER_CATALOG_FILE_YML_PATH = '/upload/productsForImport.xml';

    const CACHE_TYPE = 'A';
    const CACHE_TIME = 36000000;





    static function toMobileAppUrl( $url ){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        if( !tools\funcs::string_begins_with( '/knauf_app', $url ) ){
            $url = '/knauf_app'.$url;
        }
        return $url;
    }

    static function toSiteUrl( $url ){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        if( tools\funcs::string_begins_with( '/knauf_app', $url ) ){
            $url = str_replace( '/knauf_app', '', $url );
        }
        return $url;
    }



    // ID инфоблока по его симв. коду
    static function getIblockID( $iblockCode ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $iblock_id = false;
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 60*60;
        $cache_id = 'iblock_id_by_code_'.$iblockCode;
        $cache_path = '/iblock_id_by_code/'.$iblockCode.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $iblocks = \CIBlock::GetList( Array(), Array( 'CODE' => $iblockCode ), true );
            if($iblock = $iblocks->Fetch()){    $iblock_id = $iblock['ID'];    }
        $obCache->EndDataCache(array('iblock_id' => $iblock_id));
        }
        return $iblock_id;
    }

    // ID инфоблока каталога
    static function catIblockID(){
        return static::getIblockID( static::CATALOG_IBLOCK_CODE );;
    }

    // ID инфоблока ТП
    static function tpIblockID(){
        return static::getIblockID( static::TP_IBLOCK_CODE );;
    }


    static function setProductHasOffers(
        $product_id,
        $product_iblock_id = false,
        $stop_offer_id = false
    ){
        if( !$product_iblock_id ){
            $product_iblock_id = tools\el::getIblock($product_id);
        }
        $has_offers = 0;
        // Ищем ТП к товару
        \Bitrix\Main\Loader::includeModule('iblock');
        $filter = Array(
            "IBLOCK_ID" => project\catalog::tpIblockID(),
            "ACTIVE" => "Y",
            "PROPERTY_CML2_LINK" => $product_id
        );
        if( intval($stop_offer_id) > 0 ){
            $filter['!ID'] = $stop_offer_id;
        }
        $fields = Array( "ID", "PROPERTY_CML2_LINK_VALUE" );
        $dbElements = \CIBlockElement::GetList(
            array("SORT"=>"ASC"), $filter, false, array("nTopCount" => 1), $fields
        );
        if ($element = $dbElements->GetNext()){  $has_offers = 1;  }
        $set_prop = array("HAS_OFFERS" => $has_offers);
        \CIBlockElement::SetPropertyValuesEx($product_id, $product_iblock_id, $set_prop);
    }

    static function GetStopIDsForSD()
    {
        global $KNAUF_SD;

        $arStopSDIDs = array();
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'catalog_stop_ids'.$KNAUF_SD["ID"];
        $cache_path = '/catalog_stop_ids/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            // найдём ID товаров, которые не нужно выводить в текущем СД
            $dbProds = \CIBlockElement::GetList(Array(), Array(
                "PROPERTY_STOP_SD" => $KNAUF_SD["ID"],
                "IBLOCK_ID" => static::catIblockID()
            ), false, false, Array("ID"));
            $arStopSDIDs = Array();
            while($arProd = $dbProds->Fetch())
            {
                $arStopSDIDs[] = $arProd["ID"];
            }

            $obCache->EndDataCache(array('arStopSDIDs' => $arStopSDIDs));
        }
        return $arStopSDIDs;
    }

    static function AddStopSDsToFilter($arFilter = Array())
    {
        // получим ID товаров, которые для текущего СД выводить не надо
        $arStopSDIDs = static::GetStopIDsForSD();
        if($arFilter['!ID'])
        {
            $arFilter['!ID'] = array_merge($arFilter['!ID'], $arStopSDIDs);
            $arFilter['!ID'] = array_unique($arFilter['!ID']);
        }
        else
        {
            $arFilter['!ID'] = $arStopSDIDs;
        }
        return $arFilter;
    }

    static function sections_1_level($is_eshop = false){
        \Bitrix\Main\Loader::includeModule('iblock');
        $sections = array();
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'catalog_sections_1_level'.$is_eshop;
        $cache_path = '/catalog_sections_1_level/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $dbSections = \CIBlockSection::GetList(
                Array("SORT" => "ASC"),
                Array(
                    "ACTIVE" => "Y",
                    "DEPTH_LEVEL" => 1,
                    "IBLOCK_ID" => static::catIblockID()
                ),
                false, Array('UF_TYPE')
            );
            while ($section = $dbSections->GetNext()){   $sections[] = $section;   }
            $obCache->EndDataCache(array('sections' => $sections));
        }
        return $sections;
    }

    static function sections_for_main($is_eshop = false){
        \Bitrix\Main\Loader::includeModule('iblock');
        $sections = array();
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'catalog_sections_for_main'.$is_eshop;
        $cache_path = '/catalog_sections_for_main/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $dbSections = \CIBlockSection::GetList(
                Array("DEPTH_LEVEL" => "ASC", "SORT" => "ASC"),
                Array(
                    "ACTIVE" => "Y",
                    "DEPTH_LEVEL" => Array(1, 2),
                    "IBLOCK_ID" => static::catIblockID(),
                    "UF_INDEX" => true
                ), false, Array("UF_INDEX", "UF_INDEX_PIC")
            );
            while ($section = $dbSections->GetNext())
            {
                $sections[$section["ID"]] = $section;
            }
            $obCache->EndDataCache(array('sections' => $sections));
        }
        return $sections;
    }

    static function elements_for_main($sectID){

        if(!$sectID) return false;

        \Bitrix\Main\Loader::includeModule('iblock');
        $elements = array();
        global $KNAUF_SD;
        $sd = $KNAUF_SD["ID"];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'catalog_elements_for_main'.$sectID."_".$sd;
        $cache_path = '/catalog_elements_for_main/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){

            $arFilter = Array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => static::catIblockID(),
                "PROPERTY_ON_INDEX_VALUE" => "Y",
                "SECTION_ID" => $sectID,
                "INCLUDE_SUBSECTIONS" => "Y"
            );

            // фильтр по СД
            $arFilter = static::AddStopSDsToFilter($arFilter);

            $dbElements = \CIBlockElement::GetList(
                Array("SORT" => "ASC"), $arFilter, false, false, Array("ID")
            );
            while ($element = $dbElements->GetNext())
            {
                $elements[] = $element;
            }
            $obCache->EndDataCache(array('elements' => $elements));
        }
        return $elements;
    }

    // Дерево каталога (для корневого раздела)
    static function tree($is_eshop = false){
        $tree = array();
        $sections_1_level = project\catalog::sections_1_level($is_eshop);
        if( count($sections_1_level) > 0 ){
            foreach($sections_1_level as $key1 => $sect1){
                $subSections = tools\section::sub_sects($sect1['ID']);
                if( count($subSections) > 0 ){
                    foreach ($subSections as $key2 => $sect2) {
                        if ($sect2['DEPTH_LEVEL'] != 2){  unset($subSections[$key2]);  }
                    }
                }
                if( count($subSections) > 0 ){
                    foreach($subSections as $key2 => $sect2){
                        $elementList = static::elementList($sect2['ID'], 10);
                        if( count($elementList) > 0 ){
                            $tree['sects_1_level'][$sect1['ID']] = $sect1;
                            $tree['sects_2_level'][$sect1['ID']][$sect2['ID']] = $sect2;
                            $tree['elements'][$sect2['ID']] = static::elementList($sect2['ID']);
                        }
                    }
                }
            }
        }
        return $tree;
    }



    static function updateProductName($arResult){

        $weight = is_array($arResult['PROPERTIES'])?$arResult['PROPERTIES']['WEIGHT']['VALUE']:$arResult['PROPERTY_WEIGHT_VALUE'];
        $width = is_array($arResult['PROPERTIES'])?$arResult['PROPERTIES']['WIDTH']['VALUE']:$arResult['PROPERTY_WIDTH_VALUE'];
        $height = is_array($arResult['PROPERTIES'])?$arResult['PROPERTIES']['HEIGHT']['VALUE']:$arResult['PROPERTY_HEIGHT_VALUE'];
        $length = is_array($arResult['PROPERTIES'])?$arResult['PROPERTIES']['LENGTH']['VALUE']:$arResult['PROPERTY_LENGTH_VALUE'];
        $depth = is_array($arResult['PROPERTIES'])?$arResult['PROPERTIES']['DEPTH']['VALUE']:$arResult['PROPERTY_DEPTH_VALUE'];
        $cross = is_array($arResult['PROPERTIES'])?$arResult['PROPERTIES']['CROSS']['VALUE']:$arResult['PROPERTY_CROSS_VALUE'];

        if( strlen( trim($weight) ) > 0 ){
            $arResult['NAME'] .= ' '.trim($weight).'&nbsp;кг';
        } else if( strlen( trim($cross) ) > 0 ){
            $arResult['NAME'] .= ' '.trim($cross).'мм';
            if( strlen(trim($length)) > 0 ){
                $arResult['NAME'] .= ' ('.trim($length).'мм)';
            }
        } else if(
            strlen(trim($width)) > 0 || strlen(trim($length)) > 0
            ||
            strlen(trim($height)) > 0 || strlen(trim($depth)) > 0
        ){
            $size = [];
            if( strlen(trim($length)) > 0 ){    $size[] = trim($length);    }
            if( strlen(trim($width)) > 0 ){    $size[] = trim($width);    }
            if( strlen(trim($height)) > 0 ){
                $size[] = trim($height);
            } else if( strlen(trim($depth)) > 0 ){
                $size[] = trim($depth);
            }
            $arResult['NAME'] .= ' '.implode('x', $size).'мм';
        }
        return $arResult;
    }




    // Элементы инфоблока
    static function elementList(
        $sectID = false,
        $cnt = false,
        $results_as_array = false,
        $dopFields = false,
        $active = 'Y'
    ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $elements = array();
        // Фильтр
        $filter = Array(
            "IBLOCK_CODE" => static::CATALOG_IBLOCK_CODE,
            "INCLUDE_SUBSECTIONS" => "Y"
        );
        if( strlen($active) > 0 && $active != 'all' ){
            $filter['ACTIVE'] = $active;
        }
        if( intval($sectID) > 0 ){
            $filter["SECTION_ID"] = $sectID;
        }
        // Поля
        $fields = Array("ID", "XML_ID", "IBLOCK_ID", "NAME", "IBLOCK_SECTION_ID");
        $params = intval($cnt)>0?array("nTopCount" => $cnt):false;
        if( !is_array($dopFields) ){
            // Кеширование
            $obCache = new \CPHPCache();
            $cache_time = 24 * 60 * 60;
            $cache_id = 'elementList_' . ($sectID ? $sectID : 'all');
            $cache_path = '/elementList/' . ($sectID ? $sectID : 'all') . '/';
            if ($obCache->InitCache($cache_time, $cache_id, $cache_path)){
                $vars = $obCache->GetVars();   extract($vars);
            } elseif ($obCache->StartDataCache()) {
                // Запрос
                $dbElements = \CIBlockElement::GetList(array("SORT" => "ASC"), $filter, false, $params, $fields);
                while ($element = $dbElements->GetNext()) {
                    $elements[$element['ID']] = $element;
                }
                $obCache->EndDataCache(array('elements' => $elements));
            }
        } else {
            $fields = array_merge($fields, $dopFields);
            $fields = array_unique($fields);
            // Запрос
            $dbElements = \CIBlockElement::GetList(array("SORT" => "ASC"), $filter, false, $params, $fields);
            while ($element = $dbElements->GetNext()) {
                $elements[$element['ID']] = $element;
            }
        }
        if( !$results_as_array ){
            $results = array_keys($elements);
        } else {
            $results = $elements;
        }
        return $results;
    }




    // Модификации по товару
    static function getProductMods( $el ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $list = array();
        $filter = Array(
            "IBLOCK_CODE" => static::CATALOG_IBLOCK_CODE,
            "ACTIVE" => "Y",
            "=NAME" => $el['NAME']
        );

        $fields = Array(
            "ID", "NAME", "DETAIL_PAGE_URL", "PROPERTY_MOD",
            "PROPERTY_WEIGHT", "PROPERTY_WIDTH", "PROPERTY_HEIGHT",
            "PROPERTY_LENGTH", "PROPERTY_DEPTH", "PROPERTY_CROSS"
        );
        $dbElements = \CIBlockElement::GetList(
            array("SORT"=>"ASC"), $filter, false, false, $fields
        );
        while ($element = $dbElements->GetNext()){

            $weight = trim($element['PROPERTY_WEIGHT_VALUE']);
            $width = trim($element['PROPERTY_WIDTH_VALUE']);
            $height = trim($element['PROPERTY_HEIGHT_VALUE']);
            $length = trim($element['PROPERTY_LENGTH_VALUE']);
            $depth = trim($element['PROPERTY_DEPTH_VALUE']);
            $cross = trim($element['PROPERTY_CROSS_VALUE']);

            $mod = '';   $dop = '';
            if( strlen($weight) > 0 ){
                $mod = $weight;
                $dop = '&nbsp;кг';
            } else if( strlen( trim($cross) ) > 0 ){
                $mod = $cross;
            } else if(
                strlen($width) > 0 || strlen($length) > 0
                ||
                strlen($height) > 0 || strlen($depth) > 0
            ){
                $size = [];
                if( strlen($length) > 0 ){    $size[] = $length;    }
                if( strlen($width) > 0 ){    $size[] = $width;    }
                if( strlen($height) > 0 ){
                    $size[] = $height;
                } else if( strlen($depth) > 0 ){
                    $size[] = trim($depth);
                }
                $mod = implode('x', $size);
                $dop = 'мм';
            }
            if( strlen($mod) > 0 ){
                $list[$mod.$dop] = array(
                    'ID' => $element['ID'],
                    'NAME' => $element['NAME'],
                    'URL' => $element['DETAIL_PAGE_URL'],
                    'MOD' => $mod.$dop
                );
            }
        }
        ksort($list);



        return $list;
    }



    static function generateProductXMLID( $el_id ){
        if( intval($el_id) > 0 ){
            \Bitrix\Main\Loader::includeModule('iblock');
            $hashArray = array();
            $hashArray[] = 'product';
            $hashArray[] = static::catIblockID();
            $hashArray[] = $el_id;
            $xml_id = md5(json_encode($hashArray));
            // Сохранение
            $ob = new \CIBlockElement;
            $fields = Array( "XML_ID" => $xml_id );
            return $ob->Update( $el_id, $fields );
        }
        return false;
    }
    static function generateSectionXMLID( $sect_id ){
        if( intval($sect_id) > 0 ){
            \Bitrix\Main\Loader::includeModule('iblock');
            $hashArray = array();
            $hashArray[] = 'section';
            $hashArray[] = static::catIblockID();
            $hashArray[] = $sect_id;
            $xml_id = md5(json_encode($hashArray));
            // Сохранение
            $ob = new \CIBlockSection;
            $fields = Array( "XML_ID" => $xml_id );
            return $ob->Update( $sect_id, $fields );
        }
        return false;
    }





    static $productCSVTitles = array(
        'PROPERTY_ARTICLE' => 'ARTICLE',
        'NAME' => 'NAME',
        'CODE' => 'CODE',
        //'IBLOCK_SECTION_ID' => 'SECTION',
        'PROPERTY_WIDTH' => 'WIDTH',
        'PROPERTY_HEIGHT' => 'HEIGHT',
        'PROPERTY_LENGTH' => 'LENGTH',
        'PROPERTY_WEIGHT' => 'WEIGHT',
        'PROPERTY_DEPTH' => 'DEPTH',
        'PROPERTY_CROSS' => 'CROSS'
    );

    // Экспорт каталога (в CSV)
    static function exportProductsToCSV(){
        $csvLines = array();
        $titles = static::$productCSVTitles;
        //////////
        $ar = array();
        foreach ( $titles as $iblock_field => $title ){
            $ar[] = $title;
        }
        $csvLines[] = implode(';', $ar);
        // Товары каталога
        $elements = static::elementList(
            false,
            false,
            true,
            array('XML_ID', 'PROPERTY_ARTICLE', 'PROPERTY_WIDTH', 'PROPERTY_HEIGHT', 'PROPERTY_LENGTH', 'PROPERTY_DEPTH', 'PROPERTY_WEIGHT', 'PROPERTY_CROSS'),
            'all'
        );
        foreach ( $elements as $key => $element ){
            $ar = array();
            foreach ( $titles as $iblock_field => $title ){
                if( substr_count($iblock_field, 'PROPERTY_') ){
                    $value = $element[$iblock_field.'_VALUE'];
                } else {
                    $value = $element[$iblock_field];
                }
                if( $title == 'SECTION' && intval($value) > 0 ){
                    $sect = tools\section::info(intval($value));
                    if( intval($sect['ID']) > 0 ){
                        $value = $sect['XML_ID'];
                    }
                }
                $ar[] = str_replace(array("\r","\n"), "", $value);
            }
            $csvLines[] = implode(';', $ar);
        }

        $content = implode("\n", $csvLines);
        $content = mb_convert_encoding($content, 'windows-1251', 'UTF-8');
        
        // Запись в файл
        $file_path = $_SERVER['DOCUMENT_ROOT']."/upload/products.csv";
        $file = fopen($file_path, "w");
        $res = fwrite($file,  $content);
        fclose($file);
    }



    // Импорт каталога (из CSV)
    static function importProductsFromCSV( $file_path = "/upload/products.csv" ){

        \Bitrix\Main\Loader::includeModule('iblock');
        
        $file_path = $_SERVER['DOCUMENT_ROOT'].$file_path;
        if( file_exists( $file_path ) ){
            $lines = array();
            $csvLines = file($file_path);
            foreach ( $csvLines as $key => $csvLine ){
                $csvLine = mb_convert_encoding($csvLine, 'UTF-8', 'windows-1251');
                $ar = explode(';', $csvLine);
                $lines[] = $ar;
            }
            
            $titles = $lines[0];
            unset($lines[0]);

            if( count($lines) > 0 ){

                foreach ( $lines as $line ){

                    // Определяем XML_ID записи
                    $article = false;   $product = false;
                    foreach ( $line as $key => $value ){
                        $title = trim(str_replace(array("\r","\n"), "", $titles[$key]));
                        if(  $title == 'ARTICLE' ){    $article = $value;    }
                    }
                    // Если XML_ID указан
                    if( strlen($article) > 0 ){
                        // Пытаемся найти товар с таким XML_ID
                        $product = static::getProductByArticle($article);
                    }

                    // Если товар найден
                    if( intval($product['ID']) > 0 ){
                    // Вносим изменения

                        // Собираем значения полей
                        $fields = array();
                        foreach ( $line as $key => $value ){
                            $title = trim(str_replace(array("\r","\n"), "", $titles[$key]));
                            $value = str_replace(array("\r","\n"), "", $value);
                            if( $title != 'XML_ID' ){
                                $iblockFieldName = array_search($title, static::$productCSVTitles);
                                if( $iblockFieldName ){
                                    if( substr_count($iblockFieldName, 'PROPERTY_') ){
                                        $props[str_replace("PROPERTY_", "", $iblockFieldName)] = $value;
                                    } else {
                                        $fields[$iblockFieldName] = $value;
                                    }
                                }
                            }
                        }

                        if( count($fields) > 0 ){

                            // Определяем символьный код
                            if( strlen($fields['CODE']) > 0 ){
                                $code_el = tools\el::info_by_code($fields['CODE'], static::catIblockID());
                                if(
                                    intval($code_el['ID']) > 0
                                    &&
                                    $code_el['ID'] != $product['ID']
                                ){
                                    $fields['CODE'] .= '_'.str_replace(".", "", microtime(true));
                                }
                            } else {
                                $fields['CODE'] = $product['CODE'];
                            }

                            // Определяем ID раздела
                            if( strlen($fields['IBLOCK_SECTION_ID']) > 0 ){
                                $sect = static::getSectionByXMLID($fields['IBLOCK_SECTION_ID']);
                                if( intval($sect['ID']) > 0 ){
                                    $fields['IBLOCK_SECTION_ID'] = $sect['ID'];
                                } else {
                                    unset($fields['IBLOCK_SECTION_ID']);
                                }
                            }

                            // Сохранение элемента ИБ
                            $obEl = new \CIBlockElement;
                            $res = $obEl->Update( $product['ID'], $fields );
                            if( $res ){

                                // Сохранение свойств
                                if( count($props) > 0 ){
                                    \CIBlockElement::SetPropertyValuesEx($product['ID'], project\catalog::catIblockID(), $props);
                                }

                                BXClearCache(true, "/elementList/");

                            } else {
                                echo $obEl->LAST_ERROR;
                            }

                        }


                    // Новый товар
                    } else {


                        // Ищем раздел для импорта
                        $sect = false;
                        foreach ( $line as $key => $value ){
                            $title = str_replace(array("\r","\n"), "", $titles[$key]);
                            $value = str_replace(array("\r","\n"), "", $value);
                            if( $title == 'SECTION' && strlen($value) > 0 ){
                                $sect = static::getSectionByXMLID($value);
                            }
                        }

                        // Если раздел найден
                        if( intval($sect['ID']) > 0 ){

                            // Собираем значения полей
                            $fields = Array(
                                "IBLOCK_ID"         => project\catalog::catIblockID(),
                                "ACTIVE"            => "Y",
                                "IBLOCK_SECTION_ID" => $sect['ID']
                            );
                            $props = array();

                            foreach ( $line as $key => $value ){
                                $title = trim(str_replace(array("\r","\n"), "", $titles[$key]));
                                if( $title != 'SECTION' ){
                                    $value = str_replace(array("\r","\n"), "", $value);
                                    $iblockFieldName = array_search($title, static::$productCSVTitles);
                                    if( $iblockFieldName ){
                                        if( substr_count($iblockFieldName, 'PROPERTY_') ){
                                            if( strlen($value) > 0 ){
                                                $props[str_replace("PROPERTY_", "", $iblockFieldName)] = $value;
                                            }
                                        } else {
                                            $fields[$iblockFieldName] = $value;
                                        }
                                    }
                                }
                            }

                            if( count($fields) > 0 ){

                                if( count($props) > 0 ){
                                    $fields['PROPERTY_VALUES'] = $props;
                                }

                                // Определяем символьный код
                                if( strlen($fields['CODE']) > 0 ){} else {
                                    $fields['CODE'] = \CUtil::Translit(trim($fields['NAME']), "ru");
                                }
                                $code_el = tools\el::info_by_code($fields['CODE'], static::catIblockID());
                                if( intval($code_el['ID']) > 0 ){
                                    $fields['CODE'] .= '_'.str_replace(".", "", microtime(true));
                                }

                                // Создание элемента ИБ
                                $obEl = new \CIBlockElement;
                                $id = $obEl->Add( $fields );
                                if( intval($id) > 0 ){

                                    /*if(
                                        !$fields['XML_ID']
                                        ||
                                        ($fields['XML_ID'] && strlen(trim($fields['XML_ID'])) == 0)
                                    ){
                                        // Генерация внешнего кода
                                        static::generateProductXMLID($id);
                                    }*/

                                    // Создание товара CCatalogProduct
                                    $fields = array(
                                        "ID" => $id
                                    );
                                    $res = \CCatalogProduct::Add($fields);

                                    BXClearCache(true, "/elementList/");

                                } else {
                                    echo $obEl->LAST_ERROR;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }


    // getProductIdByH1
    static function getProductIdByH1( $h1 ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $el_id = null;
        // Фильтр
        $filter = Array(
            "IBLOCK_CODE" => static::CATALOG_IBLOCK_CODE,
            "=PROPERTY_TITLE_H1" => $h1
        );
        // Поля
        $fields = [ "ID", 'PROPERTY_TITLE_H1' ];
        // Запрос
        $dbElements = \CIBlockElement::GetList(
            [ "SORT" => "ASC" ], $filter, false,
            ["nTopCount" => 1], $fields
        );
        if( $element = $dbElements->GetNext() ){
            $el_id = $element['ID'];
        }
        return $el_id;
    }

    // getProductIdByH1
    static function getProductIdByName( $name ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $el_id = null;
        // Фильтр
        $filter = Array(
            "IBLOCK_CODE" => static::CATALOG_IBLOCK_CODE,
            "=NAME" => $name
        );
        // Поля
        $fields = [ "ID", 'NAME' ];
        // Запрос
        $dbElements = \CIBlockElement::GetList(
            [ "SORT" => "ASC" ], $filter, false,
            ["nTopCount" => 1], $fields
        );
        if( $element = $dbElements->GetNext() ){
            $el_id = $element['ID'];
        }
        return $el_id;
    }


    // getProductByArticle
    static function getProductByArticle( $article ) {
        \Bitrix\Main\Loader::includeModule('iblock');
        $el = false;
        // Фильтр
        $filter = [
            "IBLOCK_CODE" => static::CATALOG_IBLOCK_CODE,
            "=PROPERTY_ARTICLE" => $article
        ];
        // Поля
        $fields = [ "ID", "CODE", "PROPERTY_ARTICLE", "DETAIL_PAGE_URL"];
        // Запрос
        $dbElements = \CIBlockElement::GetList(
            array("SORT" => "ASC"),
            $filter, false,
            array("nTopCount" => 1), $fields
        );
        while ($element = $dbElements->GetNext()) {
            $el = $element;
        }
        return $el;
    }

    static function getProductsByModID( $mod_id ) {
        \Bitrix\Main\Loader::includeModule('iblock');
        $el = false;
        // Фильтр
        $filter = [
            "IBLOCK_CODE" => static::CATALOG_IBLOCK_CODE,
            "=PROPERTY_MODS_ARTICLE_ID" => $mod_id
        ];
        // Поля
        $fields = [ "ID", "NAME", "PROPERTY_ARTICLE", "PROPERTY_MOD", "PROPERTY_BOT_REPLY"];
        // Запрос
        $dbElements = \CIBlockElement::GetList(
            array("SORT" => "ASC"),
            $filter, false,
            false, $fields
        );
        $arMods = Array();
        while ($element = $dbElements->GetNext()) {
            $arMods[] = $element;
        }
        return $arMods;
    }

    // getProductByXMLID
    static function getProductByXMLID( $xml_id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $el = false;
        // Фильтр
        $filter = Array(
            "IBLOCK_CODE" => static::CATALOG_IBLOCK_CODE,
            "=XML_ID" => $xml_id
        );
        // Поля
        $fields = Array("ID", "CODE", "XML_ID");
        // Запрос
        $dbElements = \CIBlockElement::GetList(
            array("SORT" => "ASC"),
            $filter, false,
            array("nTopCount" => 1), $fields
        );
        while ($element = $dbElements->GetNext()) {
            $el = $element;
        }
        return $el;
    }
    // getSectionByXMLID
    static function getSectionByXMLID( $xml_id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $sect = false;
        // Фильтр
        $filter = Array(
            "IBLOCK_CODE" => static::CATALOG_IBLOCK_CODE,
            "XML_ID" => $xml_id
        );
        $dbSections = \CIBlockSection::GetList(
        	array("SORT"=>"ASC"),
        	$filter, false,
        	array(),
        	array('nTopCount' => 1)
        );
        while ($section = $dbSections->GetNext()){
            $sect = $section;
        }
        return $sect;
    }





    static $dealer_catalog_file_fields = array(
        'PROPERTY_ARTICLE_VALUE' => 'KNAUF_ID',
        'PRICE' => 'PRICE',
        'QUANTITY' => 'QUANTITY',
        'NAME' => 'NAME',
        'PROPERTY_MOD_VALUE' => 'MODIFICATION',
        'SAP1' => 'SAP1',
        'SAP2' => 'SAP2',
        'SAP3' => 'SAP3',
        'SAP4' => 'SAP4',
        'DETAIL_PAGE_URL' => 'LINK',
    );
    static $articleLogic = array(
        'WIDTH' => 'W',
        'LENGTH' => 'L',
        'HEIGHT' => 'H',
        'DEPTH' => 'D',
        'WEIGHT' => 'M',
        'CROSS' => 'C',
        'FASOVKA' => 'P',
    );



    static function createCSVForDealers(){

        \Bitrix\Main\Loader::includeModule('iblock');

        $file_path = $_SERVER['DOCUMENT_ROOT'].static::DEALER_CATALOG_FILE_PATH;
        
        if( !file_exists($file_path) ){

            $lines = array();
            $productFields = static::$dealer_catalog_file_fields;

            $line = array();
            foreach ( $productFields as $field => $title ){    $line[] = $title;    }
            $lines[] = implode(';', $line);

            $filter = Array(
                "IBLOCK_CODE" => static::CATALOG_IBLOCK_CODE,
                "ACTIVE" => "Y",
                "!PROPERTY_ARTICLE" => false
            );
            $fields = Array(
                "ID", "NAME", "DETAIL_PAGE_URL",
                "PROPERTY_ARTICLE", "PROPERTY_MOD", "PROPERTY_SAP_ART" /*, "PROPERTY_ED"*/
            );
            $dbElements = \CIBlockElement::GetList(
                array("NAME"=>"ASC"), $filter, false, false, $fields
            );
            while ($element = $dbElements->GetNext()){
                $element["SAP1"] = $element["PROPERTY_SAP_ART_VALUE"][0];
                $element["SAP2"] = $element["PROPERTY_SAP_ART_VALUE"][1];
                $element["SAP3"] = $element["PROPERTY_SAP_ART_VALUE"][2];
                $element["SAP4"] = $element["PROPERTY_SAP_ART_VALUE"][3];

                $line = array();
                foreach ( $productFields as $field => $title ){
                    $value = $element[$field]?$element[$field]:'';
                    if( $field == 'DETAIL_PAGE_URL' ){
                        $value = 'http://'.\Bitrix\Main\Config\Option::get('main', 'server_name').$value;
                    }
                    $line[] = $value;
                }
                $lines[] = implode(';', $line);
            }

            $content = implode("\n", $lines);
            $content = mb_convert_encoding($content, 'windows-1251', 'UTF-8');

            $file = fopen($file_path, "w");
            $res = fwrite($file,  $content);
            fclose($file);

       }

        return file_exists($file_path);
    }




    static function createYMLForDealers(){

        \Bitrix\Main\Loader::includeModule('iblock');

        $file_path = $_SERVER['DOCUMENT_ROOT'].static::DEALER_CATALOG_FILE_YML_PATH;

        if( !file_exists($file_path) ){



        }

        return file_exists($file_path);
    }




    // Скрипт расстановки артикулов по товарам
    static function setCatalogArticles(){
        \Bitrix\Main\Loader::includeModule('iblock');
        $catalogIblockID = project\catalog::catIblockID();
        $filter = [ "IBLOCK_ID" => $catalogIblockID ];
        $fields = [ "ID" ];
        foreach ( project\catalog::$articleLogic as $prop_code => $short_code ){
            $fields[] = 'PROPERTY_'.$prop_code;
        }
        $dbElements = \CIBlockElement::GetList(
            ["SORT"=>"ASC"], $filter, false, false, $fields
        );
        while ($element = $dbElements->GetNext()){
            $sect_id = tools\el::sections( $element['ID'] )[0]['ID'];
            $chain = tools\section::chain($sect_id);
            $level_parent_sect = tools\section::info( $chain[count($chain)-1]['ID'] );
            $article = array($level_parent_sect['UF_CODE']);
            foreach ( project\catalog::$articleLogic as $prop_code => $short_code ){
                $value = trim($element['PROPERTY_'.$prop_code.'_VALUE']);
                if( $prop_code == 'FASOVKA' ){
                    $value = str_replace([' '], '', $value);
                }
                if( strlen($value) > 0 ){
                    if( $prop_code == 'FASOVKA' ){
                        $article[] = $value;
                    } else {
                        $article[] = $short_code.$value;
                    }
                }
            }
            $article = implode("/", $article);
            $article = str_replace(',', '.', $article);
            $article = str_replace(["\n", "\r"], '', $article);
            $set_prop = array("ARTICLE" => $article);
            \CIBlockElement::SetPropertyValuesEx($element['ID'], $catalogIblockID, $set_prop);
        }
    }




    static function checkArticleDoubles(){
        $check = true;
        global $USER;
        if( $USER->IsAuthorized() && $USER->IsAdmin() ){
            $articles = [];
            $filter = [
                "IBLOCK_ID" => static::catIblockID(),
                "ACTIVE" => "Y"
            ];
            $fields = Array( "ID", "DETAIL_PAGE_URL", "PROPERTY_ARTICLE" );
            $dbElements = \CIBlockElement::GetList(
                array("SORT"=>"ASC"), $filter, false, false, $fields
            );
            while ($element = $dbElements->GetNext()){
                $articles[$element['PROPERTY_ARTICLE_VALUE']][] = $element['DETAIL_PAGE_URL'];
            }
            arsort($articles);
            $articles = array_filter($articles, function( $v ){ return count($v)>1; });
            if( count($articles) > 0 ){
                $ar = Array(
                    "MESSAGE" => 'В каталоге имеются повторяющиеся артикулы',
                    "TAG" => "ARTICLE_DOUBLES",
                    "ENABLE_CLOSE" => "Y"
                );
                $ID = \CAdminNotify::Add($ar);
            }
        }
        return $check;
    }





    static function skuProductID( $sku_id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $filter = Array(
        	"IBLOCK_ID" => static::getIblockID( static::TP_IBLOCK_CODE ),
        	"ID" => $sku_id
        );
        $fields = Array( "ID", "PROPERTY_CML2_LINK" );
        $dbElements = \CIBlockElement::GetList(
        	array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
        );
        if ( $element = $dbElements->GetNext() ){
            return $element['PROPERTY_CML2_LINK_VALUE'];
        }
        return false;
    }




    static function randProductSkuID( $product_id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $filter = Array(
            "IBLOCK_ID" => static::getIblockID( static::TP_IBLOCK_CODE ),
            "PROPERTY_CML2_LINK" => $product_id
        );
        $fields = Array( "ID", "PROPERTY_CML2_LINK" );
        $dbElements = \CIBlockElement::GetList(
            array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
        );
        if ( $element = $dbElements->GetNext() ){
            return $element['ID'];
        }
        return false;
    }





    static function getUnderOrderGoods( $ids = false, $cnts = false){
        $under_order_ids = [];  $under_order_goods = [];
        if( is_array($ids) ){
            $under_order_ids = $ids;
        } else {
            global $APPLICATION;
            $under_order = $APPLICATION->get_cookie('under_order');
            $under_order_ids = explode('|', $under_order);
        }
        $under_order_cnts = [];
        if( is_array($cnts) ){
            $under_order_cnts = $cnts;
        } else {
            global $APPLICATION;
            $under_order_cnts = unserialize($APPLICATION->get_cookie('under_order_cnt'));
        }

        if( count($under_order_ids) > 0 ){
            foreach ( $under_order_ids as $id ){
                $el = tools\el::info($id);
                $el = static::updateProductName($el);
                if( intval($el['ID']) > 0 ){
                    $el["QUANTITY"] = $under_order_cnts[$el['ID']];
                    $under_order_goods[$el['ID']] = $el;
                }
            }
        }
        return $under_order_goods;
    }



    static function checkUnderOrderGoods( $arResult, $arParams ){
        \Bitrix\Main\Loader::includeModule('iblock');
        if(
            is_array($arResult['under_order_goods'])
            &&
            count($arResult['under_order_goods']) > 0
        ){
            $productsToDeleteFromUnderOrder = [];
            // Перебираем товары из куки
            foreach ( $arResult['under_order_goods'] as $id => $el ){
                // Ищем у него ТП
                $filter = Array(
                	"IBLOCK_ID" => project\catalog::tpIblockID(),
                	"ACTIVE" => "Y",
                	"PROPERTY_CML2_LINK" => $id,
                	////////////////////////////////
                	"PROPERTY_LOCATION" => $_SESSION['LOC']['REGION_ID'],
                    ////////////////////////////////
                );
                $fields = Array( "ID", "NAME", "PROPERTY_CML2_LINK", 'PROPERTY_LOCATION' );
                $skus = \CIBlockElement::GetList(
                	array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
                );
                // Если уже есть ТП к данному товару
                if ($sku = $skus->GetNext()){
                    $productsToDeleteFromUnderOrder[] = $id;
                	// Добавим ТП в корзину
                    $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());
                    if ($item = $basket->getExistsItem('catalog', $sku['ID'])){} else {
                        $item = $basket->createItem('catalog', $sku['ID']);
                        $item->setFields(array('QUANTITY' => 1, 'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(), 'LID' => \Bitrix\Main\Context::getCurrent()->getSite(), 'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',));
                    }
                    $basket->save();
                }
            }

            if( count($productsToDeleteFromUnderOrder) > 0 ){
                $arResult['basketInfo'] = project\user_basket::info( false, $arParams['delivID']);
                $arResult['basket_cnt'] = $arResult['basketInfo']['basketProductsCNT'].' '.tools\funcs::pfCnt($arResult['basketInfo']['basketProductsCNT'], "товар", "товара", "товаров");
            }

            foreach ( $arResult['basketInfo']['arBasket'] as $bItem ){
                $parent_id = static::skuProductID( $bItem->getProductId() );
                unset($arResult['under_order_goods'][ $parent_id ]);
            }

            global $APPLICATION;
            if( count($arResult['under_order_goods']) > 0 ){
                $cookie_val = implode('|', array_keys($arResult['under_order_goods']));
            } else {
                $cookie_val = '';
            }
            $APPLICATION->set_cookie('under_order', $cookie_val, time()+2592000, '/', '.'.\Bitrix\Main\Config\Option::get('main', 'server_name'));

            $arUnderOrderCnt = Array();
            foreach($arResult['under_order_goods'] as $product_id => $arProductData)
            {
                $arUnderOrderCnt[$product_id] = $arProductData["QUANTITY"];
            }
            $APPLICATION->set_cookie('under_order_cnt', serialize($arUnderOrderCnt), time()+2592000, '/', '.'.\Bitrix\Main\Config\Option::get('main', 'server_name'));

            unset($arResult['basketInfo']['basket']);
            unset($arResult['basketInfo']['arBasket']);
            $_SESSION['basketInfo'] = $arResult['basketInfo'];
        }
        return $arResult;
    }



    static function treatKnaufID( $knauf_id ){
        if( isset($knauf_id) ){
            $knauf_id = str_replace( ['х'] /* русское */, 'x' /* на англ. */, $knauf_id );
            $knauf_id = str_replace( ['Х'] /* русское */, 'X' /* на англ. */, $knauf_id );
        }
        return $knauf_id;
    }


    static function treatQuantity( $q, $knauf_id = false ){
        if( isset($q) ){
            $q = trim( strip_tags( preg_replace('/[^0-9.,]/', '', $q)));
            $q = str_replace(',', '.', $q);
            preg_match("/^0*([1-9][0-9]*)\.?/", $q, $matches, PREG_OFFSET_CAPTURE);
            if( isset($matches[1][0]) ){
                $quantity = $matches[1][0]*1;
                return $quantity;
            } else {
                preg_match("/^0*(0)$/", $q, $matches, PREG_OFFSET_CAPTURE);
                if( isset($matches[1][0]) ){
                    $quantity = $matches[1][0]*1;
                    return $quantity;
                }
            }

        }
        return null;
    }


    static function treatPrice( $p ){
        if( isset($p) ){
            $p = trim( strip_tags( preg_replace('/[^0-9.,]/', '', $p)));
            $p = str_replace(',', '.', $p);
            preg_match("/^0*([1-9][0-9]*)(\.[0-9]{1,2})?/", $p, $matches, PREG_OFFSET_CAPTURE);
            $price = null;
            if( isset($matches[1][0]) ){
                $price = $matches[1][0];
                if( isset($matches[2][0]) ){    $price .= $matches[2][0];    }
            } else {
                preg_match("/^0*(0)(\.[0-9]{1,2})?/", $p, $matches, PREG_OFFSET_CAPTURE);
                if( isset($matches[1][0]) ){
                    $price = $matches[1][0];
                    if( isset($matches[2][0]) ){    $price .= $matches[2][0];    }
                }
            }
            return isset($price)?$price*1:null;
        }
        return null;
    }


    static function treatCoeff( $c ){
        if( isset($c) ){
            $c = trim( strip_tags( preg_replace('/[^0-9.,]/', '', $c)));
            $c = str_replace(',', '.', $c);
            preg_match("/^0*([0-9]+)(\.[0-9]+)?/", $c, $matches, PREG_OFFSET_CAPTURE);
            $coeff = null;
            if( isset($matches[1][0]) ){
                $coeff = $matches[1][0];
                if( isset($matches[2][0]) ){    $coeff .= $matches[2][0];    }
            } else {
                preg_match("/^0*(0)(\.[0-9]{1,2})?/", $c, $matches, PREG_OFFSET_CAPTURE);
                if( isset($matches[1][0]) ){
                    $coeff = $matches[1][0];
                    if( isset($matches[2][0]) ){    $coeff .= $matches[2][0];    }
                }
            }
            return isset($coeff)?$coeff*1:null;
        }
        return null;
    }


    // Автообновление цен дилеров
    static function updateDealersPrices(){

        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        $startTime = microtime(true);
        $arLog = [
            'TimeStart' => date('d.m.Y H:i:s'),
            'TimeFinish' => null,
            'TotalTime' => 0,
            'ErrorDealers' => [],
            'TotalDealersCNT' => 0,
        ];

        try {

            // Перебираем активных дилеров
            $filter = [
                "ACTIVE" => "Y",
                "GROUPS_ID" => [ project\user::DEALERS_GROUP_ID ],
                /////
                //"ID" => 388,
                /////
            ];

            $fields = [
                'FIELDS' => [ 'ID', 'NAME', 'LOGIN', 'EMAIL' ],
                //'NAV_PARAMS' => ['nTopCount' => 20],
                'SELECT' => [ 'ID', 'NAME', 'LOGIN', 'EMAIL' ]
            ];
            $dealers = \CUser::GetList(($by="id"), ($order="desc"), $filter, $fields);
            while( $arDealer = $dealers->GetNext() ){

                $arLog['TotalDealersCNT']++;

                // Получаем местоположения, созданные дилером для автообновления цен
                $auto_upd_prices_location = new project\auto_upd_prices_location();
                $dealerLocations = $auto_upd_prices_location->getList( $arDealer['ID'] );

                $arResultsForLetter = [
                    "EMAIL" => $arDealer["EMAIL"],
                    "NAME" => $arDealer["NAME"],
                    "IMPORTS" => []
                ];

                // Перебираем местоположения
                foreach ( $dealerLocations as $dealerLocation ){

                    // Ссылка на XML-файл с ценами
                    $file_link = $dealerLocation['UF_FILE_LINK'];

                    $arProductIDs = [];

                    if( strlen($file_link) > 0 ){





            $arResultForLetter = [
                "NAME" => "Автообновление цен для местоположения ".$dealerLocation["UF_LOC_NAME"]." по ссылке YML ".$file_link,
                "ERRORS" => []
            ];

            $errorPrefix = 'Импорт цен дилера (ID='.$arDealer['ID'].', LOGIN='.$arDealer['LOGIN'].') по ссылке YML "'.$file_link.'" - ';

            $arContent = static::getRemoteFileContent( $arDealer, $file_link );

            if( $arContent['status'] == 'ok' ){

                $xml_content = $arContent['content'];

                $cod = mb_detect_encoding($xml_content, array('utf-8', 'cp1251'));
                if( $cod != 'UTF-8' ){
                    $xml_content = mb_convert_encoding($xml_content, 'UTF-8', $cod);
                }
                $xml_content = html_entity_decode($xml_content);

                $total_offers_cnt = 0;

                $xml = new \CDataXML();
                $xml->LoadString($xml_content);
                // Получаем офферы
                if ($node = $xml->SelectNodes('/yml_catalog/shop/offers')){

                    $offers = $node->children();
                    $total_offers_cnt = count($offers);

                    // Отсеваем некомплектные позиции
                    foreach ( $offers as $offer_key => $offer ){

                        $offer_id = $offer->getAttribute('id');
                        $knauf_id = null;   $client_article = null;
                        $price = null;   $quantity = null;
                        $custom_price = null;   $custom_quantity = null;

                        $fields = $offer->children();
                        foreach ( $fields as $field ){
                            if( $field->name == 'knauf_id' && strlen($field->content) > 0 ){
                                $knauf_id = trim(strip_tags($field->content));
                                $knauf_id = static::treatKnaufID( $knauf_id );
                            }
                            if( $field->name == 'price' && strlen($field->content) > 0 ){
                                $price = project\catalog::treatPrice( $field->content );
                            }
                            if( $field->name == 'quantity' && strlen($field->content) > 0 ){
                                $quantity = project\catalog::treatQuantity( $field->content );
                            } else if( $field->name == 'min-quantity' && strlen($field->content) > 0 ){
                                $quantity = project\catalog::treatQuantity( $field->content );
                            }
                            if( $field->name == 'custom_price' && strlen($field->content) > 0 ){
                                $custom_price = project\catalog::treatPrice( $field->content );
                            }
                            if( $field->name == 'custom_quantity' && strlen($field->content) > 0 ){
                                $custom_quantity = project\catalog::treatQuantity( $field->content );
                            }
                        }
                        foreach ( $fields as $field ){
                            if( $field->name == 'article' && strlen($field->content) > 0 ){
                                $client_article = trim(strip_tags($field->content));
                            }
                        }
                        if( !isset($client_article) ){
                            foreach ( $fields as $field ){
                                if( $field->name == 'vendorCode' && strlen($field->content) > 0 ){
                                    $client_article = trim(strip_tags($field->content));
                                }
                            }
                        }

                        if(
                            strlen($offer_id) > 0
                            &&
                            (
                                strlen($knauf_id) > 0
                                ||
                                strlen($client_article) > 0
                            )
                            &&
                            (
                                (strlen($price) > 0 && $price > 0)
                                ||
                                (
                                    strlen($custom_price) > 0 && $custom_price > 0
                                    &&
                                    strlen($custom_quantity) > 0 && $custom_quantity > 0
                                )
                            )
                        ){} else {
                            $error = $errorPrefix.'у оффера не заполнены некоторые из обязательных полей - offer_id или knauf_id (или article), или price/custom_price (больше нуля)';
                            $arLog = static::addToArrLog( $arLog, $arDealer, $error );

                            $strOfferID = $offer_id?'(offer_id = '.$offer_id.') ':'';
                            $arResultForLetter["ERRORS"][] = 'Ошибка. У оффера №'.($offer_key + 1).' '.$strOfferID.'не заполнены некоторые из обязательных полей - offer_id или knauf_id (или article), или price/custom_price (больше нуля)';

                            unset($offers[$offer_key]);
                        }
                    }

                    if( count($offers) > 0 ){

                        // Перебираем оставшиеся позиции
                        foreach ( $offers as $offer_key => $offer ){

                            $offer_id = $offer->getAttribute('id');
                            $knauf_id = null;   $client_article = null;
                            $price = null;   $quantity = null;
                            $custom_price = null;   $custom_quantity = null;

                            $fields = $offer->children();
                            foreach ( $fields as $field ){
                                if( $field->name == 'knauf_id' && strlen($field->content) > 0 ){
                                    $knauf_id = trim(strip_tags($field->content));
                                    $knauf_id = static::treatKnaufID( $knauf_id );
                                }
                                if( $field->name == 'price' && strlen($field->content) > 0 ){
                                    $price = project\catalog::treatPrice( $field->content );
                                }
                                if( $field->name == 'quantity' && strlen($field->content) > 0 ){
                                    $quantity = project\catalog::treatQuantity( $field->content );
                                } else if( $field->name == 'min-quantity' && strlen($field->content) > 0 ){
                                    $quantity = project\catalog::treatQuantity( $field->content );
                                }
                                if( $field->name == 'custom_price' && strlen($field->content) > 0 ){
                                    $custom_price = project\catalog::treatPrice( $field->content );
                                }
                                if( $field->name == 'custom_quantity' && strlen($field->content) > 0 ){
                                    $custom_quantity = project\catalog::treatQuantity( $field->content );
                                }
                            }
                            foreach ( $fields as $field ){
                                if( $field->name == 'article' && strlen($field->content) > 0 ){
                                    $client_article = trim(strip_tags($field->content));
                                }
                            }
                            if( !isset($client_article) ){
                                foreach ( $fields as $field ){
                                    if( $field->name == 'vendorCode' && strlen($field->content) > 0 ){
                                        $client_article = trim(strip_tags($field->content));
                                    }
                                }
                            }

                            // Найдём товар с таким артикулом
                            $product_id = false;   $product = false;

                            // Если не указан knauf_id, но указан артикул,
                            // то пробуем по файлу соответствия определить knauf_id
                            if( !$knauf_id && $client_article ){
                                $knauf_id = project\dealer_mapping_csv_file::getKnaufID( $arDealer['ID'], $client_article );
                                $knauf_id = static::treatKnaufID( $knauf_id );
                            }

                            if( $knauf_id ){
                                $filter = [
                                    "IBLOCK_CODE" => project\catalog::CATALOG_IBLOCK_CODE,
                                    "=PROPERTY_ARTICLE" => $knauf_id
                                ];
                                $fields = [ "ID", "NAME", "PROPERTY_ARTICLE", "PROPERTY_PRODUCT_TYPE", 'PROPERTY_LENGTH', 'PROPERTY_WIDTH' ];
                                $products = \CIBlockElement::GetList(
                                    ["SORT"=>"ASC"], $filter, false, ["nTopCount"=>1], $fields
                                );
                                if ( $prod = $products->GetNext() ){
                                    $product_id = $prod['ID'];
                                    $product = $prod;
                                }

                                // Если найден товар с таким артикулом
                                if( intval($product_id) > 0 ){

                                    $arProductIDs[] = $product_id;
                                    $arProductIDs = array_unique($arProductIDs);

                                    if( !( strlen($price) > 0 && $price > 0 ) ){
                                        $price = project\catalog::getPriceFromCustomPrice(
                                            $product,
                                            $custom_price
                                        );
                                    }

                                    if( !( strlen($quantity) > 0 ) ){
                                        $quantity = project\catalog::getQuantityFromCustomQuantity(
                                            $product,
                                            $custom_quantity
                                        );
                                    }

                                    if( strlen($price) > 0 && $price > 0 ){

                                        // Поиск ценового предложения
                                        $filter = [
                                            "IBLOCK_CODE" => project\catalog::TP_IBLOCK_CODE,
                                            "ACTIVE" => "Y",
                                            "PROPERTY_LOCATION" => $dealerLocation['UF_LOC_ID'],
                                            "PROPERTY_DEALER" => $arDealer['ID'],
                                            "PROPERTY_CML2_LINK" => $product_id,
                                        ];
                                        $fields = [
                                            "ID", "PROPERTY_LOCATION", "PROPERTY_DEALER",
                                            "PROPERTY_CML2_LINK", "CATALOG_GROUP_".project\catalog::PRICE_ID
                                        ];

                                        $dbElements = \CIBlockElement::GetList(
                                            ["SORT" => "ASC"], $filter,
                                            false, ["nTopCount" => 1], $fields
                                        );

                                        // Если найдено
                                        if ($sku = $dbElements->GetNext()){


                                            // Обновляем существующее ТП
                                            // Сохр. количества на складе
                                            if( isset($quantity) ){
                                                $fields = [ 'QUANTITY' => $quantity ];
                                                $res = \CCatalogProduct::Update($sku['ID'], $fields);
                                                if( !$res ){
                                                    tools\logger::addError('Загрузка YML: Ошибка изменения количества товара');
                                                }
                                            }

                                            // Сохр. цены
                                            $res = \CPrice::Update(
                                                $sku[ "CATALOG_PRICE_ID_".project\catalog::PRICE_ID ],
                                                [ "PRICE" => $price ]
                                            );

                                            if( $res ){

                                                BXClearCache(true, "/el_info/".$sku['ID']."/");

                                            } else {

                                                $error = $errorPrefix.'ошибка изменения цены для ТП дилера';
                                                $arLog = static::addToArrLog( $arLog, $arDealer, $error );

                                                $arResultForLetter["ERRORS"][] = 'Ошибка изменения цены для оффера id='.$offer_id;
                                            }


                                        // Если такого ТП ещё нет
                                        } else {


                                            // Создаём новое ТП
                                            $PROP[1] = $product_id;
                                            $PROP[2] = $arDealer['ID'];
                                            $PROP[16] = $dealerLocation['UF_LOC_ID'];

                                            $sku_name = $product['NAME'].' (D='.$arDealer['ID'].' L='.$dealerLocation['UF_LOC_ID'].')';

                                            // Создаём элемент инфоблока
                                            $element = new \CIBlockElement;
                                            $fields = [
                                                "IBLOCK_ID"				=> project\catalog::getIblockID( project\catalog::TP_IBLOCK_CODE ),
                                                "PROPERTY_VALUES"		=> $PROP,
                                                "NAME"					=> $sku_name,
                                                "ACTIVE"				=> "Y"
                                            ];

                                            if( $sku_id = $element->Add($fields) ){

                                                // Создаём товар CCatalogProduct
                                                $fields = [
                                                    "ID" => $sku_id,
                                                    "VAT_ID" => project\catalog::NDS_ID,
                                                    "VAT_INCLUDED" => "Y",
                                                    "QUANTITY" => isset($quantity)?$quantity:0
                                                ];
                                                if( $res = \CCatalogProduct::Add($fields) ){

                                                    // Создаём цену
                                                    $fields = [
                                                        "PRODUCT_ID" => $sku_id,
                                                        "CATALOG_GROUP_ID" => project\catalog::PRICE_ID,
                                                        "PRICE" => $price,
                                                        "CURRENCY" => "RUB",
                                                    ];

                                                    $price_id = \CPrice::Add($fields);
                                                    if( intval($price_id) > 0 ){

                                                        BXClearCache(true, "/el_info/".$sku_id."/");

                                                    } else {

                                                        \CCatalogProduct::Delete($sku_id);
                                                        \CIBlockElement::Delete($sku_id);

                                                        $error = $errorPrefix.'ошибка создания цены для ценового предложения дилера';
                                                        $arLog = static::addToArrLog( $arLog, $arDealer, $error );

                                                        $arResultForLetter["ERRORS"][] = 'Ошибка создания цены для для оффера id='.$offer_id;
                                                    }

                                                } else {

                                                    \CIBlockElement::Delete($sku_id);

                                                    $error = $errorPrefix.'ошибка создания товара для ценового предложения дилера';
                                                    $arLog = static::addToArrLog( $arLog, $arDealer, $error );
                                                    $arResultForLetter["ERRORS"][] = 'Ошибка создания товара для для оффера id='.$offer_id;
                                                }

                                            } else {

                                                $error = $errorPrefix.'ошибка создания элемента инфоблока для ценового предложения дилера - '.$element->LAST_ERROR;
                                                $arLog = static::addToArrLog( $arLog, $arDealer, $error );
                                                $arResultForLetter["ERRORS"][] = 'Ошибка создания элемента инфоблока для для оффера id='.$offer_id.' - '.$element->LAST_ERROR;;
                                            }

                                        }
                                    }

                                } else {

                                    $error = $errorPrefix.'не удалось определить product_id и price для оффера id='.$offer_id;
                                    $arLog = static::addToArrLog( $arLog, $arDealer, $error );
                                    $arResultForLetter["ERRORS"][] = 'Ошибка. Не удалось найти товар для оффера id='.$offer_id.' knauf_id = '.$knauf_id;
                                }

                            } else {
                                $error = $errorPrefix.'не удалось найти knauf_id для оффера id='.$offer_id;
                                $arLog = static::addToArrLog( $arLog, $arDealer, $error );

                                $arResultForLetter["ERRORS"][] = 'Ошибка. Не удалось найти knauf_id для оффера id='.$offer_id;
                            }
                        }
                    }

                    // найдём предложения дилера для выбранного местоположения
                    // которые мы не обработали (не было в файле или были без необходимых полей)
                    $filter = [
                        "IBLOCK_CODE" => project\catalog::TP_IBLOCK_CODE,
                        "ACTIVE" => "Y",
                        "PROPERTY_LOCATION" => $dealerLocation['UF_LOC_ID'],
                        "PROPERTY_DEALER" => $arDealer['ID'],
                        "!PROPERTY_CML2_LINK" => $arProductIDs,
                    ];
                    $dbElements = \CIBlockElement::GetList(
                        array("SORT" => "ASC"), $filter, false, false, Array("ID", "NAME")
                    );
                    while ($arDelSku = $dbElements->Fetch()){
                        // удалим их
                        \CIBlockElement::Delete($arDelSku["ID"]);
                    }

                } else {

                    $error = $errorPrefix.'ошибка: в файле не найден блок <yml_catalog> -> <shop> -> <offers>';
                    $arLog = static::addToArrLog( $arLog, $arDealer, $error );
                    $arResultForLetter["ERRORS"][] = 'Ошибка: в файле не найден блок <yml_catalog> -> <shop> -> <offers>';

                }
            } else if( $arContent['status'] == 'error' ){

                $error = $errorPrefix.'ошибка: '.$arContent['text'];
                $arLog = static::addToArrLog( $arLog, $arDealer, $error );
                $arResultForLetter["ERRORS"][] = 'Ошибка: '.$arContent['text'];
            }



                        // будем отправлять отчёт только, если были ошибки
                        if( $arResultForLetter["ERRORS"] ){
                            $arResultsForLetter["IMPORTS"][] = $arResultForLetter;
                        }

                    }
                }



                // отправим дилеру письмо о результатах импорта
                if( $arResultsForLetter["IMPORTS"] ){

                    $letterHtml = '';
                    foreach( $arResultsForLetter["IMPORTS"] as $arImport ){
                        $letterHtml .= '<b>'.$arImport["NAME"].'</b><br>';
                        if($arImport["ERRORS"]){
                            $letterHtml .= "<p style='color:red;'>При обновлении произошли следующие ошибки:</p>";
                            $letterHtml .= implode("<br>", $arImport["ERRORS"]);
                            $letterHtml .= "<br><br>";
                        } else {
                            $letterHtml .= "<p style='color:green;'>Обновление завершено без ошибок</p><br>";
                        }
                    }

                    $arEventFields = Array(
                        "HTML" => $letterHtml,
                        "EMAIL_TO" => $arResultsForLetter["EMAIL"],
                        "TITLE" => "Отчёт об автообновлении цен для дилера ".$arResultsForLetter["NAME"]
                    );
                    \CEvent::SendImmediate("MAIN", "s1", $arEventFields);
                }
            }

            $arLog['TimeFinish'] = date('d.m.Y H:i:s');
            $endTime = microtime(true);
            $arLog['TotalTime'] = $endTime - $startTime;

            static::saveArrLogToFile( $arLog );

        } catch( Exception $ex ){

            $error = $errorPrefix.'ошибка: '.$ex->getMessage();
            $arLog = static::addToArrLog( $arLog, $arDealer, $error );
            $arResultForLetter["ERRORS"][] = 'Ошибка: '.$ex->getMessage();

            static::saveArrLogToFile( $arLog );
        }
    }


    static function addToArrLog( $arLog, $arDealer, $error ){
        if( !isset($arLog['ErrorDealers'][ $arDealer['ID'] ]) ){
            $arLog['ErrorDealers'][ $arDealer['ID'] ] = [
                'ID' => $arDealer['ID'],
                'LOGIN' => $arDealer['LOGIN'],
            ];
        }
        $arLog['ErrorDealers'][ $arDealer['ID'] ]['ERRORS'][] = $error;
        return $arLog;
    }



    static function saveArrLogToFile( $arLog ){
        $logPath = $_SERVER['DOCUMENT_ROOT']."/local/php_interface/cron/update_dealer_prices/";
        if( !file_exists($logPath) ){    mkdir($logPath, 0700);    }
        $logFileName = 'last_log.txt';
        $content = '{'."\n";
        foreach ( $arLog as $k1 => $v1 ){
            if( is_array($v1) ){
                $content .= '  '.$k1.': ['.(count($v1)>0?"\n":"");
                foreach ( $v1 as $k2 => $v2 ){
                    if( is_array($v2) ){
                        $content .= '    '.$k2.': ['.(count($v2)>0?"\n":"");
                        foreach ( $v2 as $k3 => $v3 ){
                            if( is_array($v3) ){
                                $content .= '      '.$k3.': ['.(count($v3)>0?"\n":"");
                                foreach ( $v3 as $k4 => $v4 ){
                                    if( is_array($v4) ){
                                        $content .= '        '.$k4.': '.json_encode($v4, JSON_UNESCAPED_UNICODE);
                                    } else {
                                        $content .= '        '.$k4.": ".$v4."\n";
                                    }
                                }
                                $content .= (count($v3)>0?"      ":"").']'."\n";
                            } else {
                                $content .= '      '.$k3.": ".$v3."\n";
                            }
                        }
                        $content .= (count($v2)>0?"    ":"").']'."\n";
                    } else {
                        $content .= '    '.$k2.": ".$v2."\n";
                    }
                }
                $content .= (count($v1)>0?"  ":"").']'."\n";
            } else {
                $content .= '  '.$k1.": ".$v1."\n";
            }
        }
        $content .= '}';
        $logFile = fopen($logPath.$logFileName, "w");
        $res = fwrite($logFile, $content);
        fclose($logFile);
    }





    static function getPriceFromCustomPrice( $product, $custom_price ){
        $price = null;
        if(
            intval($product['ID']) > 0
            &&
            strlen($custom_price) > 0 && $custom_price > 0
        ){
            $product_type = trim(strtolower($product['PROPERTY_PRODUCT_TYPE_VALUE']));
            $product_length = trim(strtolower($product['PROPERTY_LENGTH_VALUE']));
            $product_width = trim(strtolower($product['PROPERTY_WIDTH_VALUE']));
            if( isset($product_type) ){
                if( $product_type == 'лист' ){
                    if(
                        strlen($product_length) > 0 && $product_length > 0
                        &&
                        strlen($product_width) > 0 && $product_width > 0
                    ){
                        $item_area = $product_length / 1000 * $product_width / 1000;
                        $price = round($item_area * $custom_price, 2);
                    }
                } else if( $product_type == 'профиль' ){
                    if( strlen($product_length) > 0 && $product_length > 0 ){
                        $item_length = $product_length / 1000;
                        $price = round($item_length * $custom_price, 2);
                    }
                }
            }
        }
        return $price;
    }



    static function getQuantityFromCustomQuantity( $product, $custom_quantity ){
        $quantity = null;
        if(
            intval($product['ID']) > 0
            &&
            strlen($custom_quantity) > 0 && $custom_quantity > 0
        ){
            $product_type = trim(strtolower($product['PROPERTY_PRODUCT_TYPE_VALUE']));
            $product_length = trim(strtolower($product['PROPERTY_LENGTH_VALUE']));
            $product_width = trim(strtolower($product['PROPERTY_WIDTH_VALUE']));
            if( isset($product_type) ){
                if( $product_type == 'лист' ){
                    if(
                        strlen($product_length) > 0 && $product_length > 0
                        &&
                        strlen($product_width) > 0 && $product_width > 0
                    ){
                        $item_area = $product_length / 1000 * $product_width / 1000;
                        $quantity = floor($custom_quantity / $item_area);
                    }
                } else if( $product_type == 'профиль' ){
                    if( strlen($product_length) > 0 && $product_length > 0 ){
                        $item_length = $product_length / 1000;
                        $quantity = floor($custom_quantity / $item_length);
                    }
                }
            }
        }
        return $quantity;
    }





    static function getRemoteFileContent( $arDealer, $file_link ){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $result = [
            'status' => 'ok',
            'text' => null,
            'content' => null,
        ];
        $client = new Client([ 'timeout' => 10, 'verify' => false ]);
        try {
            $response = $client->request( 'GET', $file_link );
            if( $response->getStatusCode() == 200 ){
                $body = $response->getBody();
                $result['content'] = $body->getContents();
            } else {
                $error = 'файл по ссылке не доступен';
                $result['status'] = 'error';
                $result['text'] = $error;
            }
        } catch(ClientException $ex){
            $error = $ex->getMessage();
            $result['status'] = 'error';
            $result['text'] = $error;
        } catch(RequestException $ex){
            $error = $ex->getMessage();
            $result['status'] = 'error';
            $result['text'] = $error;
        }
        return $result;
    }





}


