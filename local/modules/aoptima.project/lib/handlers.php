<? namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;



class handlers {

    static function OnSaleBasketSaved(\Bitrix\Main\Event $event)
    {

    }

    static function OnSaleBasketBeforeSaved(\Bitrix\Main\Event $event)
    {
        global $noGifts;
        if($noGifts) return true;

        // получить данные акций
        $arActions = action_gifts::GetList();

        $basket = $event->getParameter("ENTITY");

        $basketItems = $basket->getBasketItems();
        $arBasket = Array();
        $arGifts = Array();
        foreach($basketItems as $basketItem)
        {
            $offerID = $basketItem->getProductId();
            $arProd = \CCatalogSku::GetProductInfo($offerID);
            $prodID = $arProd["ID"]?$arProd["ID"]:$offerID;

            $price = $basketItem->getPrice();
            // не подарки
            if($basketItem->getField('CUSTOM_PRICE') != "Y")
            {
                if($arBasket[$prodID])
                {
                    $arBasket[$prodID]["QUANTITY"] += $basketItem->getQuantity();
                }
                else
                {
                    $arBasket[$prodID] = [
                        "ID" => $basketItem->getId(),
                        "PRODUCT_ID" => $offerID,
                        "PRODUCT_ID_CATALOG" => $prodID,
                        "QUANTITY" => $basketItem->getQuantity(),
                        "NAME" => $basketItem->getField('NAME'),
                        "PRICE" => $price,
                        "CAN_BUY" => $basketItem->canBuy(),
                        "IS_DELAY" => $basketItem->isDelay(),
                        "OBJ" => $basketItem
                    ];
                }
            }
            // подарки
            else
            {
                if($arGifts[$prodID])
                {
                    $arGifts[$prodID]["QUANTITY"] += $basketItem->getQuantity();
                }
                else
                {
                    $arGifts[$prodID] = [
                        "ID" => $basketItem->getId(),
                        "PRODUCT_ID" => $offerID,
                        "PRODUCT_ID_CATALOG" => $prodID,
                        "QUANTITY" => $basketItem->getQuantity(),
                        "NAME" => $basketItem->getField('NAME'),
                        "PRICE" => $price,
                        "CAN_BUY" => $basketItem->canBuy(),
                        "IS_DELAY" => $basketItem->isDelay(),
                        "OBJ" => $basketItem
                    ];
                }
            }
        }

        // если акций нет, то удаляем из корзины все подарки
        if(!$arActions)
        {
            foreach($arGifts as $arGift)
            {
                $arGift["OBJ"]->delete();
            }
        }
        else
        {
            // определить сколько каких товаров нужно в качестве подарка
            $arNeedGifts = Array();
            foreach($arActions as $arAction)
            {
                if($arBasket[$arAction["PROD"]] && ($arBasket[$arAction["PROD"]]["QUANTITY"] >= $arAction["CART_CNT"]))
                {
                    $needGiftCnt = floor($arBasket[$arAction["PROD"]]["QUANTITY"]/$arAction["CART_CNT"]) * $arAction["GIFT_CNT"];

                    $arNeedGifts[] = [
                        "PRODUCT_ID" => $arBasket[$arAction["PROD"]]["PRODUCT_ID"],
                        "PRICE" => $arBasket[$arAction["PROD"]]["PRICE"],
                        "QUANTITY" => $needGiftCnt,
                        "PRODUCT_ID_CATALOG" => $arBasket[$arAction["PROD"]]["PRODUCT_ID_CATALOG"],
                    ];
                }
            }
            // сравнить $arGifts и $arNeedGifts: добавить недостающие, удалить лишние
            foreach($arNeedGifts as $arNeedGift)
            {
                // если такой подарок уже есть в корзине
                if($arGifts[$arNeedGift["PRODUCT_ID_CATALOG"]])
                {
                    // сравним количество
                    if($arGifts[$arNeedGift["PRODUCT_ID_CATALOG"]]["QUANTITY"] != $arNeedGift["QUANTITY"])
                    {
                        $arGifts[$arNeedGift["PRODUCT_ID_CATALOG"]]["OBJ"]->setField('QUANTITY', $arNeedGift["QUANTITY"]);
                    }
                }
                else
                {
                    // добавим
                    $item = $basket->createItem('catalog', $arNeedGift["PRODUCT_ID"]);
                    $item->setFields(Array(
                        'QUANTITY' => $arNeedGift["QUANTITY"],
                        'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                        'LID' => IS_ESHOP?'mb':'s1',
                        'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
                        'CUSTOM_PRICE' => "Y",
                        "PRICE" => 0,
                        "DISCOUNT_PRICE" => $arNeedGift["PRICE"],
                        'BASE_PRICE' => $arNeedGift["PRICE"],
                    ));
                }
            }
        }
    }


    // При удалении заказа
    static function OnSaleBeforeOrderDelete(\Bitrix\Main\Event $event){
        $order = $event->getParameter("ENTITY");
        // Удаляем отзывы к заказу $order->getId()
        $review_order = new project\review_order();
        $orderReviews = $review_order->getList( $order->getId(), false, false, false, false, true );
        foreach ( $orderReviews as $key => $orderReview ){
            $review_order->delete( $orderReview['ID'] );
        }
    }
    

    static function OnAfterIBlockElementAdd( $arFields ){
        BXClearCache(true, "/seo_actions/");
        // Каталог
        if( $arFields['IBLOCK_ID'] == project\catalog::catIblockID() ){
            BXClearCache(true, "/elementList/");
            BXClearCache(true, "/catalog_elements_for_main/");
            DeleteDirFilesEx("/upload/search_results/");
            unlink($_SERVER['DOCUMENT_ROOT'].project\catalog::DEALER_CATALOG_FILE_PATH);
            project\catalog::setProductHasOffers( $arFields['ID'], $arFields['IBLOCK_ID'] );

            BXClearCache(true, "/catalog_stop_ids/");
        }
        // ТП
        if( $arFields['IBLOCK_ID'] == project\catalog::tpIblockID() ){
            $el = tools\el::info($arFields['ID']);
            if( intval($el['PROPERTY_CML2_LINK_VALUE']) > 0  ){
                project\catalog::setProductHasOffers( $el['PROPERTY_CML2_LINK_VALUE'] );
            }
            BXClearCache(true, "/tp_list/");
            BXClearCache(true, "/dealer_regions/");
            BXClearCache(true, "/region_dealers/");
        }
        // Доп. пункты к заказу
        if( $arFields['IBLOCK_ID'] == project\order::ORDER_DOP_PUNKTY_IBLOCK_ID ){
            BXClearCache(true, "/deliveryTime/");
        }
        // Статусы заказов
        if( $arFields['IBLOCK_ID'] == project\order_status::LIST_IBLOCK_ID ){
            BXClearCache(true, "/order_status_list/");
        }
        // Точки продаж
        $dealer_shop = new project\dealer_shop();
        if( $arFields['IBLOCK_ID'] == $dealer_shop->getIblockID() ){
            BXClearCache(true, "/dealer_shops/");
        }
        // СД
        if( $arFields['IBLOCK_ID'] == project\bx_location::$sd_iblock ){
            BXClearCache(true, "/getSDForLoc/");
        }
        // Акции
        if( $arFields['IBLOCK_ID'] == project\action_gifts::IBLOCK_ID ){
            BXClearCache(true, "/action_gifts/");
        }
        // системы
        if( $arFields['IBLOCK_ID'] == project\systems::SYSTEMS_IBLOCK_ID ){
            BXClearCache(true, "/system_elements/");
            BXClearCache(true, "/system_data/");
        }
        // состав систем
        if( $arFields['IBLOCK_ID'] == project\systems::PARTS_IBLOCK_ID ){
            BXClearCache(true, "/system_data/");
        }
        // инструкции
        if( $arFields['IBLOCK_ID'] == project\instructions::INSTRUCTIONS_IBLOCK_ID ){
            BXClearCache(true, "/instruction_by_id/");
        }
        // шаги
        if( $arFields['IBLOCK_ID'] == project\instructions::STEPS_IBLOCK_ID ){
            BXClearCache(true, "/instruction_steps/");
        }
        // баннеры
        if( $arFields['IBLOCK_ID'] == project\mainbanners::BANNERS_IBLOCK_ID ){
            BXClearCache(true, "/main_banners/");
        }
    }

    static function OnAfterIBlockElementUpdate( $arFields ){
        BXClearCache(true, "/seo_actions/");
        // Каталог
        if( $arFields['IBLOCK_ID'] == project\catalog::catIblockID() ){
            BXClearCache(true, "/elementList/");
            BXClearCache(true, "/catalog_elements_for_main/");
            DeleteDirFilesEx("/upload/search_results/");
            unlink($_SERVER['DOCUMENT_ROOT'].project\catalog::DEALER_CATALOG_FILE_PATH);
            project\catalog::setProductHasOffers( $arFields['ID'], $arFields['IBLOCK_ID'] );
            BXClearCache(true, "/catalog_stop_ids/");
        }
        // ТП
        if( $arFields['IBLOCK_ID'] == project\catalog::tpIblockID() ){
            $el = tools\el::info($arFields['ID']);
            if( intval($el['PROPERTY_CML2_LINK_VALUE']) > 0  ){
                project\catalog::setProductHasOffers( $el['PROPERTY_CML2_LINK_VALUE'] );
            } else {
                $el = tools\el::info($arFields['ID'], true);
                if( intval($el['PROPERTY_CML2_LINK_VALUE']) > 0  ){
                    project\catalog::setProductHasOffers( $el['PROPERTY_CML2_LINK_VALUE'] );
                }
            }
            BXClearCache(true, "/tp_list/");
        }
        // Доп. пункты к заказу
        if( $arFields['IBLOCK_ID'] == project\order::ORDER_DOP_PUNKTY_IBLOCK_ID ){
            BXClearCache(true, "/deliveryTime/");
        }
        // Статусы заказов
        if( $arFields['IBLOCK_ID'] == project\order_status::LIST_IBLOCK_ID ){
            BXClearCache(true, "/order_status_list/");
        }
        // Точки продаж
        $dealer_shop = new project\dealer_shop();
        if( $arFields['IBLOCK_ID'] == $dealer_shop->getIblockID() ){
            BXClearCache(true, "/dealer_shops/");
        }
        // СД
        if( $arFields['IBLOCK_ID'] == project\bx_location::$sd_iblock ){
            BXClearCache(true, "/getSDForLoc/");
        }
        // Акции
        if( $arFields['IBLOCK_ID'] == project\action_gifts::IBLOCK_ID ){
            BXClearCache(true, "/action_gifts/");
        }
        // системы
        if( $arFields['IBLOCK_ID'] == project\systems::SYSTEMS_IBLOCK_ID ){
            BXClearCache(true, "/system_elements/");
            BXClearCache(true, "/system_data/");
        }
        // состав систем
        if( $arFields['IBLOCK_ID'] == project\systems::PARTS_IBLOCK_ID ){
            BXClearCache(true, "/system_data/");
        }
        // инструкции
        if( $arFields['IBLOCK_ID'] == project\instructions::INSTRUCTIONS_IBLOCK_ID ){
            BXClearCache(true, "/instruction_by_id/");
        }
        // шаги
        if( $arFields['IBLOCK_ID'] == project\instructions::STEPS_IBLOCK_ID ){
            BXClearCache(true, "/instruction_steps/");
        }
        // баннеры
        if( $arFields['IBLOCK_ID'] == project\mainbanners::BANNERS_IBLOCK_ID ){
            BXClearCache(true, "/main_banners/");
        }
    }

    static function OnAfterIBlockElementDelete(){}



    static function OnBeforeIBlockElementAdd( &$arFields ){}

    static function OnBeforeIBlockElementUpdate( &$arFields ){}

    static function OnBeforeIBlockElementDelete( $ID ){
        BXClearCache(true, "/seo_actions/");
        $iblock_id = tools\el::getIblock($ID);
        // Каталог
        if( $iblock_id == project\catalog::catIblockID() ){
            BXClearCache(true, "/elementList/");
            BXClearCache(true, "/catalog_elements_for_main/");
            DeleteDirFilesEx("/upload/search_results/");
            unlink($_SERVER['DOCUMENT_ROOT'].project\catalog::DEALER_CATALOG_FILE_PATH);
            BXClearCache(true, "/catalog_stop_ids/");
        }
        // ТП
        if( $iblock_id == project\catalog::tpIblockID() ){
            $el = tools\el::info($ID);
            if( intval($el['PROPERTY_CML2_LINK_VALUE']) > 0  ){
                project\catalog::setProductHasOffers( $el['PROPERTY_CML2_LINK_VALUE'], false, $ID );
            }
            BXClearCache(true, "/tp_list/");
            BXClearCache(true, "/dealer_regions/");
            BXClearCache(true, "/region_dealers/");
        }
        // Доп. пункты к заказу
        if( $iblock_id == project\order::ORDER_DOP_PUNKTY_IBLOCK_ID ){
            BXClearCache(true, "/deliveryTime/");
        }
        // Статусы заказов
        if( $iblock_id == project\order_status::LIST_IBLOCK_ID ){
            BXClearCache(true, "/order_status_list/");
        }
        // Точки продаж
        $dealer_shop = new project\dealer_shop();
        if( $iblock_id == $dealer_shop->getIblockID() ){
            BXClearCache(true, "/dealer_shops/");
        }
    }



    static function OnAfterIBlockSectionAdd( $arFields ){
        // Каталог
        if( $arFields['IBLOCK_ID'] == project\catalog::catIblockID() ){
            BXClearCache(true, "/catalog_sections_1_level/");
            BXClearCache(true, "/catalog_sections_for_main/");
            BXClearCache(true, "/catalog_elements_for_main/");
            BXClearCache(true, "/elementList/");
            unlink($_SERVER['DOCUMENT_ROOT'].project\catalog::DEALER_CATALOG_FILE_PATH);
        }
        // системы
        if( $arFields['IBLOCK_ID'] == project\systems::SYSTEMS_IBLOCK_ID ){
            BXClearCache(true, "/system_sections/");
        }
    }

    static function OnAfterIBlockSectionUpdate( $arFields ){
        // Каталог
        if( $arFields['IBLOCK_ID'] == project\catalog::catIblockID() ){
            BXClearCache(true, "/catalog_sections_1_level/");
            BXClearCache(true, "/catalog_sections_for_main/");
            BXClearCache(true, "/catalog_elements_for_main/");
            BXClearCache(true, "/elementList/");
            unlink($_SERVER['DOCUMENT_ROOT'].project\catalog::DEALER_CATALOG_FILE_PATH);
        }
        // системы
        if( $arFields['IBLOCK_ID'] == project\systems::SYSTEMS_IBLOCK_ID ){
            BXClearCache(true, "/system_sections/");
        }
    }

    static function OnAfterIBlockSectionDelete(){}



    static function OnBeforeIBlockSectionAdd( &$arFields ){}

    static function OnBeforeIBlockSectionUpdate( &$arFields ){}

    static function OnBeforeIBlockSectionDelete( $ID ){
        $iblock_id = tools\el::getIblock($ID);
        // Каталог
        if( $iblock_id == project\catalog::catIblockID() ){
            BXClearCache(true, "/catalog_sections_1_level/");
            BXClearCache(true, "/catalog_sections_for_main/");
            BXClearCache(true, "/catalog_elements_for_main/");
            BXClearCache(true, "/elementList/");
            unlink($_SERVER['DOCUMENT_ROOT'].project\catalog::DEALER_CATALOG_FILE_PATH);
        }
    }










    static function OnAfterUserAdd($arFields){
        // Очищаем кеш
        BXClearCache(true, "/allDealers/");
        BXClearCache(true, "/basketDealers/");
    }

    static function OnAfterUserUpdate($arFields){
        // Очищаем кеш
        BXClearCache(true, "/allDealers/");
        BXClearCache(true, "/basketDealers/");
    }

    static function OnBeforeUserDelete($user_id){
        \Bitrix\Main\Loader::includeModule('iblock');
        global $APPLICATION;
        if( $user_id == project\order::SERVICE_USER_ID ){
            $APPLICATION->throwException("Служебный пользователь не может быть удалён");
            return false;
        }
        if( $user_id == 1 ){
            $APPLICATION->throwException("Админ не может быть удалён");
            return false;
        }
        $user = new project\user( $user_id );
        if( $user->isDealer() ){
            // Очищаем кеш
            BXClearCache(true, "/allDealers/");
            BXClearCache(true, "/basketDealers/");
            // Удаляем все ТП данного дилера
            $filter = [ "IBLOCK_CODE" => project\catalog::TP_IBLOCK_CODE, "PROPERTY_DEALER" => $user_id ];
            $fields = [ "ID", "PROPERTY_DEALER" ];
            $dbElements = \CIBlockElement::GetList( ["SORT"=>"ASC"], $filter, false, false, $fields );
            while ($element = $dbElements->GetNext()){
                \CIBlockElement::Delete($element['ID']);
            }
            // Удаляем все местоположения доставки дилера
            $delivery_location = new project\delivery_location();
            $locs = $delivery_location->getList( $user_id );
            foreach ( $locs as $loc ){
                $delivery_location->delete( $loc['ID'] );
            }
            BXClearCache(true, '/dealersDelivLocs/'.$user_id.'/');
            // Удаляем все местоположения автозагрузки дилера
            $auto_upd_prices_location = new project\auto_upd_prices_location();
            $locs = $auto_upd_prices_location->getList( $user_id );
            foreach ( $locs as $loc ){
                $auto_upd_prices_location->delete( $loc['ID'] );
            }
            // Удаляем все отзывы о заказах дилеров
            $order_review = new project\review_order();
            $reviews = $order_review->getList( false, false, $user_id );
            foreach ( $reviews as $review ){
                $order_review->delete( $review['ID'] );
            }
            // Удаляем все оценки дилеров
            $dealer_vote = new project\vote_dealer();
            $votes = $dealer_vote->getList( $user_id );
            foreach ( $votes as $vote ){
                $dealer_vote->delete( $vote['ID'] );
            }
            // Удаляем все точки продаж дилеров
            $dealer_shop = new project\dealer_shop();
            $list = $dealer_shop->getList( $user_id );
            foreach ( $list as $listItem ){
                $dealer_shop->delete( $listItem['ID'] );
            }
            BXClearCache(true, '/dealer_shops/');
        }
    }





    static function OnPriceAdd( $ID, $arFields ){
        BXClearCache(true, "/tp_list/");
    }

    static function OnPriceUpdate( $ID, $arFields ){
        BXClearCache(true, "/tp_list/");
    }

    static function OnBeforePriceDelete( $ID ){
        BXClearCache(true, "/tp_list/");
    }




}