<?php

namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


class pers_photo {

    use \files___trait;

    const MIN_WIDTH = 65;
    const MIN_HEIGHT = 65;
    const MAX_WIDTH = 270;
    const MAX_HEIGHT = 270;
    const MAX_FILE_SIZE = 5242880;

    const TEMP_UPLOAD_DIR = "persPhotoTMP";
    const TEMP_UPLOAD_PATH = "/upload/persPhotoTMP/";


    protected $FILES = false;
    protected $doc_root = false;
    protected $fileTypes = array('jpeg', 'png');
    protected $file_name = false;
    protected $new_file_name = false;
    protected $file_path = false;
    protected $width = false;
    protected $height = false;
    protected $file_moved = false;
    protected $file_extension = false;





    // конструктор объекта
    function __construct($files = false){
        $this->doc_root = $_SERVER["DOCUMENT_ROOT"];
        // Перемещение файла во временную папку
        if ( $this->moveFile($files) ){
            $this->file_moved = true;
        }
    }


    public function getFileTypes(){
        return $this->fileTypes;
    }



    protected function jsOnResponse($obj){
        echo '<script type="text/javascript">
		window.parent.onResponse("'.$obj.'");
		</script>';
    }



    // Загрузка фото
    public function upload(){
        $check_status = $this->checkAsPhoto();
        if ( $check_status == 'ok' ){
            // МАСШТАБИРУЕМ ФОТО
            $big_file_path = (static::TEMP_UPLOAD_PATH).($this->new_file_name);
            // Инфо о файле $big_file_path
            $arFile = \CFile::MakeFileArray(($this->doc_root).($big_file_path));
			global $USER;
			$user_id = $USER->GetID();
			$currentUser = new project\user( $USER->GetID() );
			$userProps = $currentUser->arUser;
			if(strlen($userProps["UF_MAIN_USER"]) > 0){
				$user = new project\user( $userProps["UF_MAIN_USER"] );
				if( $user->isDealer() ){
					$user_id = $user->arUser["ID"];
				}
			}
            $user = new \CUser;
            $fields = array('PERSONAL_PHOTO' => $arFile);
            $res = $user->Update($user_id, $fields);
            if( $res ){
                // Ответ
                $this->jsOnResponse("{'status':'ok', 'fname':'" . $this->file_name . "'}");
            } else {
                // Ответ
                //$this->jsOnResponse("{'status':'".$user->LAST_ERROR."'}");
                $this->jsOnResponse("{'status':'Ошибка сохранения профиля'}");
            }

            // Удаляем временные файлы
            unlink( $this->file_path );

        } else {
            // Ответ
            $this->jsOnResponse("{'status':'".$check_status."'}");
        }
    }







}