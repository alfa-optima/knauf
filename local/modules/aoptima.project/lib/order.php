<?php

namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('sale');




class order {


    const ORDER_DOP_PUNKTY_IBLOCK_ID = 7;
    const SERVICE_USER_ID = 16;





    static function getDealerOrderPropID(){
        $propID = 19;
        return $propID;
    }
    static function getStatusOrderPropID(){
        $propID = 20;
        return $propID;
    }


    // Получение ID заказов в адрес дилера
    static function getDealerOrders( $dealerID, $limit = false, $status = false ){
        $list = array();
        // Получим ID заказов дилера
        $params = array(
            'filter' => array(
                'PROPS.VALUE' => $dealerID,
                'PROPS.ORDER_PROPS_ID' => static::getDealerOrderPropID(),
            ),
            'select' => array( 'ID', 'PROPS.VALUE' ),
            'runtime' => array(
                new \Bitrix\Main\Entity\ReferenceField(
                    'PROPS',
                    '\Bitrix\Sale\Internals\OrderPropsValueTable',
                    array( '=this.ID' => 'ref.ORDER_ID' ),
                    array( "join_type" => 'inner' )
                )
            ),
            'order' => array()
        );
        if(
            is_array($status) && count($status) > 0
            ||
            is_string($status) && strlen($status) > 0
        ){
            $params['filter']['PROPS2.VALUE'] = $status;
            $params['filter']['PROPS2.ORDER_PROPS_ID'] = static::getStatusOrderPropID();

            $params['select'][] = 'PROPS2.VALUE';
            $params['runtime'][] = new \Bitrix\Main\Entity\ReferenceField(
                'PROPS2',
                '\Bitrix\Sale\Internals\OrderPropsValueTable',
                array( '=this.ID' => 'ref.ORDER_ID' ),
                array( "join_type" => 'inner' )
            );
        }
        if( $limit ){
            $params['limit'] = $limit;
        }
        $orders = \Bitrix\Sale\Order::getList( $params );
        while ($order = $orders->fetch()){
            $list[] = $order['ID'];
        }
        $list = array_unique($list);
        return $list;
    }




    // Адрес доставки
    static function getDeliveryAddress( $propValues ){
        $arAddress = array(
//            'loc_name' => false,
//            'street' => false,
//            'house' => false,
            'address' => false,
            'podyezd' => false,
            'floor' => false,
            'kv' => false,
        );
        foreach ( $propValues as $key => $prop ){
            $value = $prop['VALUE'][0];
            if( strlen($value) > 0 ){
//                if( $prop['CODE'] == 'CITY' ){   $arAddress['loc_name'] = $value;   }
//                if( $prop['CODE'] == 'STREET' ){   $arAddress['street'] = 'ул.'.$value;   }
//                if( $prop['CODE'] == 'HOUSE' ){   $arAddress['house'] = 'д.'.$value;   }
                if( $prop['CODE'] == 'ADDRESS' ){   $arAddress['address'] = $value;   }
                if( $prop['CODE'] == 'PODYEZD' ){   $arAddress['podyezd'] = 'подъезд '.$value;   }
                if( $prop['CODE'] == 'FLOOR' ){   $arAddress['floor'] = 'этаж '.$value;   }
                if( $prop['CODE'] == 'KVARTIRA' ){   $arAddress['kv'] = 'кв/офис '.$value;   }
            }
        }
        $address = implode(', ', $arAddress);
        return $address;
    }



    // Инфо по свойству
    static function getPropValue( $propValues, $propCode ){
        $propValue = false;
        foreach ( $propValues as $key => $prop ){
            $value = $prop['VALUE'][0];
            if( strlen($value) > 0 ){
                if( $prop['CODE'] == $propCode ){   $propValue = $value;   }
            }
        }
        return $propValue;
    }



    // Значения свойств заказа
    static function getOrderPropValues( $obOrder, $clearCache = false ){
        if( $clearCache ){
            BXClearCache(true, '/orderPropValues/'.$obOrder->getID().'/');
        }
        $obCache = new \CPHPCache();
        $cache_time = 60*60;
        $cache_id = 'orderPropValues_'.$obOrder->getID();
        $cache_path = '/orderPropValues/'.$obOrder->getID().'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $propertyCollection = $obOrder->getPropertyCollection();
            $propValues = $propertyCollection->getArray()['properties'];
            $obCache->EndDataCache(array('propValues' => $propValues));
        }
        return $propValues;
    }



    // setPropValue
    static function setPropValue( $obOrder, $propCode, $propValue ){
        $propertyCollection = $obOrder->getPropertyCollection();
        $propValues = $propertyCollection->getArray()['properties'];
        foreach ( $propValues as $key => $prop ){
            if( $prop['CODE'] == $propCode ){
                $obProp = $propertyCollection->getItemByOrderPropertyId($prop['ID']);
                $obProp->setValue( $propValue );
                $obOrder->save();
                BXClearCache(true, "/orderPropValues/".$obOrder->getID."/");
            }
        }
    }




    // Список заказов
    public function getList( $user_id, $order_id = false ){
        $list = array();
        $filter_array = array(
            'USER_ID' => $user_id
        );
        if( intval($order_id) > 0 ){
            $filter_array['ID'] = $order_id;
        }
        $orders = \Bitrix\Sale\OrderTable::getList(array(
            'filter' => $filter_array,
            'select' => array( '*' ),
            'order' => array('ID' => 'DESC'),
        ));
        while ($order = $orders->fetch()){
            $list[$order['ID']] = $order;
        }
        return $list;
    }











    // Доп. пункты к адресу доставки
    public function addressDopPunkty(){
        $list = array();
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'orderAddressDopPunkty';
        $cache_path = '/orderAddressDopPunkty/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $filter = Array(
                "IBLOCK_ID" => static::ORDER_DOP_PUNKTY_IBLOCK_ID,
                "ACTIVE" => "Y"
            );
            $fields = Array( "ID", "NAME", "SORT");
            $dbElements = \CIBlockElement::GetList(
                array("SORT"=>"ASC"), $filter, false, false, $fields
            );
            while ($element = $dbElements->GetNext()){
                $list[$element['ID']] = $element;
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }



    static $deliveryTimeList = [
        'с 00:00 до 03:00' => [ 'VALUES' => [0, 3] ],
        'с 03:00 до 06:00' => [ 'VALUES' => [3, 6] ],
        'с 06:00 до 09:00' => [ 'VALUES' => [6, 9] ],
        'с 09:00 до 12:00' => [ 'VALUES' => [9, 12] ],
        'с 12:00 до 15:00' => [ 'VALUES' => [12, 15] ],
        'с 15:00 до 18:00' => [ 'VALUES' => [15, 18] ],
        'с 18:00 до 21:00' => [ 'VALUES' => [18, 21] ],
        'с 21:00 до 00:00' => [ 'VALUES' => [21, 24] ]
    ];



    static function distanceByCoords ( $lat1, $lon1, $lat2, $lon2 ){
        $lat1 *= M_PI / 180;
        $lat2 *= M_PI / 180;
        $lon1 *= M_PI / 180;
        $lon2 *= M_PI / 180;

        $d_lon = $lon1 - $lon2;

        $slat1 = sin($lat1);
        $slat2 = sin($lat2);
        $clat1 = cos($lat1);
        $clat2 = cos($lat2);
        $sdelt = sin($d_lon);
        $cdelt = cos($d_lon);

        $y = pow($clat2 * $sdelt, 2) + pow($clat1 * $slat2 - $slat1 * $clat2 * $cdelt, 2);
        $x = $slat1 * $slat2 + $clat1 * $clat2 * $cdelt;

        return round(atan2(sqrt($y), $x) * 6372795, 0);
    }



}