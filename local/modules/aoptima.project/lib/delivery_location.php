<?php
namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('highloadblock');

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


class delivery_location {

    const HIBLOCK_ID = 7;


    function __construct(){
        $this->hlblock_id = static::HIBLOCK_ID;
        $this->hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($this->hlblock_id)->fetch();
        $this->entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($this->hlblock);
        $this->entity_data_class = $this->entity->getDataClass();
        $this->entity_table_name = $this->hlblock['TABLE_NAME'];
        $this->sTableID = 'tbl_'.$this->entity_table_name;
    }




    // add
    public function add( $arData ){
        $result = $this->entity_data_class::add($arData);
        if ($result->isSuccess()) {
            BXClearCache(true, "/dealersDelivLocs/".$arData['UF_DEALER']."/");
            return true;
        } else {
            tools\logger::addError('Ошибка создания местоположения доставки - '.implode(', ', $result->getErrors()));
        }
        return false;
    }



    // update
    public function update( $id, $arData ){
        $result = $this->entity_data_class::update($id, $arData);
        if ( $result->isSuccess() ){
            $location = static::getByID( $id );
            if( intval($location['ID']) > 0 ){
                BXClearCache(true, "/dealersDelivLocs/".$location['UF_DEALER']."/");
                return true;
            }
        } else {
            tools\logger::addError('Ошибка изменения местоположения доставки - '.implode(', ', $result->getErrors()));
        }
        return false;
    }



    // getList
    public function getList( $dealer_id, $loc_id = false, $deliv_days = false ){
        $elements = array();
        $arFilter = array(
            'UF_DEALER' => $dealer_id
        );
        if( intval($loc_id) > 0 ){
            $arFilter['UF_LOC_ID'] = $loc_id;
        }
        if( $deliv_days ){
            $arFilter['<UF_MIN_DELIV_DAYS'] = $deliv_days;
        }
        $arSelect = array('*');
        $arOrder = array( "UF_LOC_NAME" => "ASC" );
        $arInfo = array(
            "select" => $arSelect,
            "filter" => $arFilter,
            "order" => $arOrder
        );
        $hash = md5( json_encode($arInfo) );
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 24*60*60;
        $cache_id = 'dealersDelivLocs_'.$dealer_id.'_'.$hash;
        $cache_path = '/dealersDelivLocs/'.$dealer_id.'/'.$hash.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $rsData =  $this->entity_data_class::getList( $arInfo );
            $rsData = new \CDBResult($rsData,  $this->sTableID);
            while($element = $rsData->Fetch()){
                $elements[$element['ID']] = $element;
            }
        $obCache->EndDataCache(array('elements' => $elements));
        }
        return $elements;
    }



    // getByID
    public function getByID( $id ){
        $arFilter = array( 'ID' => $id );
        $arSelect = array('*');
        $arOrder = array( "ID" => "ASC" );
        $arInfo = array(
            "select" => $arSelect,
            "filter" => $arFilter,
            "order" => $arOrder
        );
        $rsData =  $this->entity_data_class::getList( $arInfo );
        $rsData = new \CDBResult($rsData,  $this->sTableID);
        if($element = $rsData->Fetch()){
            return $element;
        }
        return false;
    }



    // удаление
    public function delete( $id ){
        if( intval($id) > 0 ){
            $location = static::getByID( $id );
            if( intval($location['ID']) > 0 ){
                $result = $this->entity_data_class::delete($location['ID']);
                if ( $result->isSuccess() ){
                    BXClearCache(true, "/dealersDelivLocs/".$location['UF_DEALER']."/");
                    return true;
                } else {
                    tools\logger::addError('Ошибка удаления местоположения доставки - '.implode(', ', $result->getErrors()));
                }
            }
        }
        return false;
    }










}