<?php
namespace AOptima\Project;
use AOptima\Project as project;


class dealer_sort {

    const MIN_SCORE = 1;
    const MAX_SCORE = 5;



    function __construct() {

        $this->fields = [
            'basket_sum' => [
                'CODE' => 'basket_sum',
                'NAME' => 'По стоимости доставки',
                'ARRESULT_FIELD' => 'BASKET_SUM',
                'REVERSE_LOGIC' => 'Y',
                'TYPE' => 'NUMBER'
            ],
            'delivery_price' => [
                'CODE' => 'delivery_price',
                'NAME' => 'По стоимости доставки',
                'ARRESULT_FIELD' => 'DELIVERY_PRICE',
                'REVERSE_LOGIC' => 'Y',
                'TYPE' => 'NUMBER'
            ],
            'dealer_rating' => [
                'CODE' => 'dealer_rating',
                'NAME' => 'Рейтинг дилера',
                'ARRESULT_FIELD' => 'DEALER_RATING',
                'TYPE' => 'NUMBER'
            ],
            'dealer_pvz_distance' => [
                'CODE' => 'dealer_pvz_distance',
                'NAME' => 'Расстояние до ближайшего ПВЗ дилера'
            ],
            'dealer_rise_large_goods' => [
                'CODE' => 'dealer_rise_large_goods',
                'NAME' => 'Подъём крупногабаритных товаров дилером'
            ],
            'dealer_delivery_days' => [
                'CODE' => 'dealer_delivery_days',
                'NAME' => 'Срок доставки'
            ],
        ];

        foreach ( $this->fields as $key => $field ){
            $weight = \Bitrix\Main\Config\Option::get('aoptima.project', strtoupper($field['CODE']).'_WEIGHT');
            if( !isset($weight) ){    $weight = 1;    }
            $this->fields[$key]['WEIGHT'] = $weight;
        }
    }




    // сортировка комплектных предложений
    public function go( $arResult, $arParams ){

        $offers_list = $arResult['OFFERS_LIST'];

        // соберём параметры для сортировки
        foreach($offers_list as $k => $arOffer)
        {
            // предупреждения
            $offers_list[$k]["SORT"]["WARNINGS"] = count($arOffer["WARNINGS"]);

            // процент доступного товара
            $fullCnt = 0;
            $haveCnt = 0;
            foreach($arOffer["basketProducts"] as $arProd)
            {
                $fullCnt += $arProd["quantity"]; // нужное количество
                $haveCnt += $arProd["DEALER_QUANTITY"]; // имеющееся количество
            }
            $offers_list[$k]["SORT"]["QUANTITY_PERCENT"] = $haveCnt/$fullCnt;

                // стоимость TOTAL_SUM
            $offers_list[$k]["SORT"]["PRICE"] = $arOffer["TOTAL_SUM"];
        }

        usort($offers_list, function($a, $b){
            if ($a["SORT"]['WARNINGS'] == $b["SORT"]['WARNINGS'])
            {
                if ($a["SORT"]['QUANTITY_PERCENT'] == $b["SORT"]['QUANTITY_PERCENT'])
                {
                    if ($a["SORT"]['PRICE'] == $b["SORT"]['PRICE'])
                    {
                        return 0;
                    }
                    else
                    {
                        return ($a["SORT"]['PRICE'] > $b["SORT"]['PRICE']) ? 1 : -1; // по возрастанию
                    }
                }
                else
                {
                    return ($a["SORT"]['QUANTITY_PERCENT'] > $b["SORT"]['QUANTITY_PERCENT']) ? -1 : 1; // по убыванию
                }
            }
            else
            {
                return ($a["SORT"]['WARNINGS'] > $b["SORT"]['WARNINGS']) ? 1 : -1; // по возрастанию
            }
        });

        /*if( count($offers_list) > 1 ){

            $sortArray = [];

            foreach ( $this->fields as $field_code => $field ) {
                // Числовые
                if( $field['TYPE'] == 'NUMBER' ){
                    $values = [];
                    foreach ( $offers_list as $dealer_id => $dealer ){
                        if( $field['ARRESULT_FIELD'] ){
                            $value = $dealer[$field['ARRESULT_FIELD']];
                            if( !$value && $field_code == 'dealer_rating' ){   $value = 1;   }
                            if( $value ){     $values[$dealer_id] = $value;     }
                        }
                    }
                    $values = array_unique($values);
                    $minValue = min($values);
                    $maxValue = max($values);
                    foreach ( $offers_list as $dealer_id => $dealer ){
                        if( $field['ARRESULT_FIELD'] ){
                            $value = $dealer[$field['ARRESULT_FIELD']];
                            if( !$value && $field_code == 'dealer_rating' ){   $value = 1;   }
                            if( $field['REVERSE_LOGIC'] == 'Y' ){
                                if( $value == $minValue ){
                                    $score = static::MAX_SCORE;
                                } else if( $value == $maxValue ){
                                    $score = static::MIN_SCORE;
                                } else {
                                    $score =  ($maxValue - $value)*(static::MAX_SCORE - static::MIN_SCORE)/($maxValue - $minValue) + static::MIN_SCORE;
                                }
                            } else {
                                if( $value == $minValue ){
                                    $score = static::MIN_SCORE;
                                } else if( $value == $maxValue ){
                                    $score = static::MAX_SCORE;
                                } else {
                                    $score =  $value*(static::MAX_SCORE + static::MIN_SCORE)/($maxValue + $minValue) + static::MIN_SCORE;
                                }
                            }
                            $score = $score * $field['WEIGHT'];
                            $sortArray[$dealer_id][$field_code] = $score;
                        }
                    }
                }
            }


            // Расстояния до ближайших точек ПВЗ дилера (Самовывоз)
            if( 
                $arResult['delivType'] == 'samovyvoz' 
                &&
                $this->fields['dealer_pvz_distance']
                &&
                strlen($arResult['ADDRESS_COORDS']) > 0
            ){
                $field = $this->fields['dealer_pvz_distance'];
                $min_distances = [];
                $dealer_shop = new project\dealer_shop();
                // Координаты адреса покупателя
                $ar = explode(',', trim($arResult['ADDRESS_COORDS']));
                $lat = trim($ar[0]);
                $lng = trim($ar[1]);
                // Перебираем дилеров
                foreach ( $offers_list as $dealer_id => $dealer ){
                    // Получаем перечень ПВЗ дилера
                    $dealerPVZlist = $dealer_shop->getList(
                        $dealer['ID'],
                        $arResult['LOC']['ID'],
                        true
                    );
                    // Хотим определить минимальное расстояние до ПВЗ данного дилера
                    $min_distance = false;
                    // Перебираем ПВЗ
                    foreach ( $dealerPVZlist as $pvz ){
                        // Если заполнены координаты
                        if( strlen($pvz['PROPERTY_GPS_VALUE']) > 0 ){
                            $ar = explode(',', trim($pvz['PROPERTY_GPS_VALUE']));
                            $pvz_lat = trim($ar[0]);
                            $pvz_lng = trim($ar[1]);
                            // расстояние до ПВЗ
                            $distance = project\order::distanceByCoords($lat, $lng, $pvz_lat, $pvz_lng);
                            if( !$min_distance || $distance < $min_distance ){
                                $min_distance = $distance;
                            }
                        }
                    }
                    $min_distances[$dealer_id] = $min_distance;
                }
                // Определим баллы
                if( count($min_distances) > 0 ){
                    $values = [];
                    foreach ( $min_distances as $dealer_id => $min_distance ){
                        if( $min_distance === false ){
                            $sortArray[$dealer_id][$field['CODE']] = 0;
                        } else {
                            $values[] = $min_distance;
                        }
                    }
                    if( count($values) > 0 ){
                        $values = array_unique($values);
                        $minValue = min($values);
                        $maxValue = max($values);
                        foreach ( $min_distances as $dealer_id => $min_distance ){
                            if( $min_distance == $minValue ){
                                $score = static::MAX_SCORE;
                            } else if( $min_distance == $maxValue ){
                                $score = static::MIN_SCORE;
                            } else {
                                $score =  ($maxValue - $min_distance)*(static::MAX_SCORE - static::MIN_SCORE)/($maxValue - $minValue) + static::MIN_SCORE;
                            }
                            $score = $score * $field['WEIGHT'];
                            $sortArray[$dealer_id][$field['CODE']] = $score;
                        }
                    }
                }
            }


            // Наличие подъёма крупногабаритных товаров (при наличии в корзине + Доставка)
            if(
                $this->fields['dealer_rise_large_goods']
                &&
                $arResult['delivType'] == 'delivery'
                &&
                $arParams['form_data']['needPodyem'] == 'Да'
                &&
                count($arResult['largeGoods']) > 0
            ){
                $field = $this->fields['dealer_rise_large_goods'];
                foreach ( $offers_list as $dealer_id => $dealer ){

                    $delivLocation = false;
                    $dl = new project\delivery_location();
                    foreach( $arResult['LOC_CHAIN_IDS'] as $loc_id ) {
                        if( !$delivLocation ){
                            $deliveryLocations = $dl->getList( $dealer['ID'], $loc_id );
                            if( count($deliveryLocations) > 0 ){
                                $delivLocation = $deliveryLocations[array_keys($deliveryLocations)[0]];
                            }
                        }
                    }

                    // Если местоположение доставки у дилера найдено
                    if(
                        $delivLocation
                        &&
                        strlen($delivLocation['UF_DELIV_SETTINGS']) > 0
                    ){
                        
                        $delivSettings = \AOptima\Tools\funcs::json_to_array($delivLocation['UF_DELIV_SETTINGS']);

                        if( $delivSettings['rise_to_the_floor']['not_rise_large_goods'] == 'Y' ){
                            $sortArray[$dealer_id][$field['CODE']] = 1;
                        } else {
                            $sortArray[$dealer_id][$field['CODE']] = 5;
                        }
                    }

                }

            }


            // Срок доставки (Доставка)
            if(
                $this->fields['dealer_delivery_days']
                &&
                $arResult['delivType'] == 'delivery'
            ){
                $field = $this->fields['dealer_delivery_days'];
                $min_deliv_days = [];
                foreach ( $offers_list as $dealer_id => $dealer ){
                    $sortArray[$dealer_id][$field['CODE']] = 0;
                    $delivLocation = false;
                    $dl = new project\delivery_location();
                    foreach( $arResult['LOC_CHAIN_IDS'] as $loc_id ) {
                        if( !$delivLocation ){
                            $deliveryLocations = $dl->getList( $dealer['ID'], $loc_id );
                            if( count($deliveryLocations) > 0 ){
                                $delivLocation = $deliveryLocations[array_keys($deliveryLocations)[0]];
                            }
                        }
                    }
                    // Если местоположение доставки у дилера найдено
                    if(
                        $delivLocation
                        &&
                        strlen($delivLocation['UF_DELIV_SETTINGS']) > 0
                    ){
                        $delivSettings = \AOptima\Tools\funcs::json_to_array($delivLocation['UF_DELIV_SETTINGS']);
                        if( strlen($delivSettings['min_delivery_time']) > 0 ){
                            $min_deliv_days[$dealer_id] = $delivSettings['min_delivery_time'];
                        }
                    }
                }
                if( count($min_deliv_days) > 0 ){
                    $minValue = min($min_deliv_days);
                    $maxValue = max($min_deliv_days);
                    foreach ( $min_deliv_days as $dealer_id => $days ){
                        if( $days == $minValue ){
                            $score = static::MAX_SCORE;
                        } else if( $days == $maxValue ){
                            $score = static::MIN_SCORE;
                        } else {
                            $score =  ($maxValue - $days)*(static::MAX_SCORE - static::MIN_SCORE)/($maxValue - $minValue) + static::MIN_SCORE;
                        }
                        $score = $score * $field['WEIGHT'];
                        $sortArray[$dealer_id][$field['CODE']] = $score;
                    }
                }
            }
        }
        
        if( count($sortArray) > 0 ){
            foreach ( $sortArray as $dealer_id => $scores ){
                $sortArray[$dealer_id] = array_sum($scores);
            }
            arsort($sortArray);
            $newList = [];
            foreach ( $sortArray as $dealer_id => $score ){
                foreach ( $offers_list as $dealerID => $dealer ){
                    if( $dealer_id == $dealerID ){
                        $newList[$dealerID] = $dealer;
                    }
                }
            }
            $offers_list = $newList;
            unset($newList);
        }*/
        
        return $offers_list;
    }





}