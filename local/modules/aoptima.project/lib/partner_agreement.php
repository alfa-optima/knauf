<?php

namespace AOptima\Project;
use AOptima\Project as project;


class partner_agreement {

    const IBLOCK_ID = 13;



    static function get(){
        $el = false;
        \Bitrix\Main\Loader::includeModule('iblock');
        $filter = Array(
        	"IBLOCK_ID" => static::IBLOCK_ID,
        	"ACTIVE" => "Y"
        );
        $fields = Array( "ID", "DETAIL_TEXT", "PROPERTY_DATE" );
        $dbElements = \CIBlockElement::GetList(
        	array("PROPERTY_DATE"=>"DESC"), $filter, false, array("nTopCount"=>1), $fields
        );
        while ($element = $dbElements->GetNext()){
            $el = $element;
        }
        return $el;
    }



    static function list( $id = false ){
        $list = [];
        \Bitrix\Main\Loader::includeModule('iblock');
        $filter = Array(
            "IBLOCK_ID" => static::IBLOCK_ID,
            "ACTIVE" => "Y"
        );
        if( $id ){   $filter['!ID'] = $id;   }
        $fields = Array( "ID", "DETAIL_TEXT", "PROPERTY_DATE" );
        $dbElements = \CIBlockElement::GetList(
            array( "PROPERTY_DATE" => "DESC" ), $filter, false, false, $fields
        );
        while ($element = $dbElements->GetNext()){
            $list[$element['ID']] = $element;
        }
        return $list;
    }



}