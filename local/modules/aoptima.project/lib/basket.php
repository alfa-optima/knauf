<?php

namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


use Bitrix\Sale, Bitrix\Main;


class basket {


    const LIST_OFFERS_CNT = 100;



    static $dealerOffersSort = array(
        'DEALER_NAME' => array(
            'DEFAULT_ORDER' => 'ASC',
        ),
        'BASKET_SUM' => array(
            'DEFAULT_ORDER' => 'ASC',
        )
    );





    // Сбор необходимых данных
    static function collectNecessaryData( $arResult, $arParams ){

        $user = new project\user();

        // Инфо по сортировке
        if( $arParams['IS_AJAX'] != 'Y' ){
            unset($_SESSION['OFFERS_SORT']);
        }
        if( !$_SESSION['OFFERS_SORT'] ){
            /*$_SESSION['OFFERS_SORT'] = array(
                'CODE' => 'BASKET_SUM',
                'ORDER' => project\basket::$dealerOffersSort['BASKET_SUM']['DEFAULT_ORDER']
            );*/
        }
        $arResult['offers_sort'] = $_SESSION['OFFERS_SORT'];

        // Адрес доставки по умолчанию
        $arResult['USER_DEFAULT_DELIV_ADDRESS'] = [];
        if( isset($user->arUser) ){
            if( strlen($user->arUser['UF_DEFAULT_ADDRESS']) > 0 ){
                $arResult['USER_DEFAULT_DELIV_ADDRESS'] = tools\funcs::json_to_array($user->arUser['UF_DEFAULT_ADDRESS']);
            }
            if( strlen($user->arUser['UF_PODYEZD_DEFAULT']) > 0 ){
                $arResult['PODYEZD_DEFAULT'] = $user->arUser['UF_PODYEZD_DEFAULT'];
            }
            if( strlen($user->arUser['UF_FLOOR_DEFAULT']) > 0 ){
                $arResult['FLOOR_DEFAULT'] = $user->arUser['UF_FLOOR_DEFAULT'];
            }
            if( strlen($user->arUser['UF_KV_DEFAULT']) > 0 ){
                $arResult['KV_DEFAULT'] = $user->arUser['UF_KV_DEFAULT'];
            }
            if( strlen($user->arUser['UF_LIFT_DEFAULT']) > 0 ){
                $arResult['LIFT_DEFAULT'] = $user->arUser['UF_LIFT_DEFAULT'];
            }
        }

        // Получим товары "под заказ"
        $arResult['under_order_goods'] = project\catalog::getUnderOrderGoods();
        
        // Инфо о корзине
        $arResult = project\basket::getGeneralInfo( $arResult, $arParams );

        if( count($_POST) > 0 && !$_POST['action'] ){
            $arParams['form_data'] = $_POST;
        } else {
            $arResult = project\catalog::checkUnderOrderGoods( $arResult, $arParams );
        }

        // Варианты списка по лифту
        $arResult['LIFT_ENUMS'] = project\basket::getLiftEnums();

        // Разделим товары на обычные и крупногабаритные
        $arResult = project\basket::getRegularAndLargeProducts( $arResult );

        // Список вариантов доставки
        $arResult['DS_LIST'] = project\ds::getList();

        // Тип активного варианта доставки
        $arResult = project\basket::getActiveDeliveryType( $arResult, $arParams );

        // Время доставки
        $arResult['DELIVERY_TIME_LIST'] = project\order::$deliveryTimeList;

        // Адрес доставки
        if( strlen($arParams['form_data']['address']) > 0 ){
            $arResult['ADDRESS'] = strip_tags($arParams['form_data']['address']);
        } else if( isset($arResult['USER_DEFAULT_DELIV_ADDRESS']['ADDRESS']) ){
            $arResult['ADDRESS'] = $arResult['USER_DEFAULT_DELIV_ADDRESS']['ADDRESS'];
        }

        // Координаты адреса доставки
        if( strlen($arParams['form_data']['address_coords']) > 0 ){
            $arResult['ADDRESS_COORDS'] = strip_tags($arParams['form_data']['address_coords']);
        } else if( isset($arResult['USER_DEFAULT_DELIV_ADDRESS']['COORDS']) ){
            $arResult['ADDRESS_COORDS'] = $arResult['USER_DEFAULT_DELIV_ADDRESS']['COORDS'];
        }

        // местоположение пользователя
        $arResult['LOC'] = false;
        $loc_id = intval($arParams['form_data']['loc_id'])>0?$arParams['form_data']['loc_id']:$arParams['KNAUF_LOC']['CITY_LOC_ID'];
        if( intval($loc_id) > 0 ){
            // Инфо по местоположению
            $arResult['LOC'] = project\bx_location::getByID( $loc_id );
            if( intval($arResult['LOC']['ID']) > 0 ){
                // Родители местоположения
                $arResult['LOC_CHAIN'] = project\bx_location::getLocChain($arResult['LOC']['ID']);
                // Исключим из цепочки страну (пока страна не нужна)
                foreach ( $arResult['LOC_CHAIN'] as $key => $item ){
                    if( $item['PARENTS_TYPE_NAME_RU'] == 'Страна' ){
                        unset($arResult['LOC_CHAIN'][$key]);
                    }
                }
                $arResult['LOC_CHAIN_IDS'] = array_reverse(array_keys($arResult['LOC_CHAIN']));
            }
        }

        // Варианты оплаты
        $arResult['PS_LIST'] = project\ps::getList();

        // Проверка заполненности даты/времени (вариант Доставка)
        $arResult = project\basket::dateTimeIsSelected( $arResult, $arParams );

        return $arResult;
    }



    // УСЛОВИЯ НАЧАЛА ПОДБОРА ДИЛЕРОВ
    static function checkBeforeOffersCollecting( $arResult, $arParams ){
        return (
            $arResult['basketInfo']['basketProductsCNT'] > 0
            &&
            intval($arParams['form_data']['ds']) > 0
            &&
            $arResult['LOC']
            &&
            $arResult['dateTimeIsSelected']
            &&
            strlen($arParams['form_data']['ps']) > 0
            &&
            strlen($arParams['form_data']['pay_type']) > 0
        );
    }


    // Инфо по товарам предложения конкретного дилера
    static function getDealerBasketInfo( $dealer, $arResult, $arParams ){

        $dealer['basketProducts'] = $arResult['basketInfo']['basketProducts'];
        $dealer['BASKET_SUM'] = 0;

        $arDealerQuantities = Array();
        foreach ($dealer['basketProducts'] as $productKey => $bProduct){

            $bProduct['NAL'] = 'N';
            $bProduct['FULL_QUANTITY'] = 'Y';

            // Найдём ТП к данному товару для данного дилера
            $tpList = array();
            // Переберём цепочку местоположения
            foreach( $arResult['LOC_CHAIN_IDS'] as $loc_id ) {
                if( count($tpList) == 0 ){
                    $tpList = project\tp::getList(
                        $bProduct['el']['ID'], $dealer['ID'], $loc_id, 1
                    );
                }
            }

            if ( count($tpList) > 0 ){

                // ТП
                $tp = $tpList[0];

                // Цена ТП дилера
                if($bProduct["custom_price"])
                {
                    $price = $bProduct["price"];
                }
                else
                {
                    // Цена ТП дилера
                    $price = $tp['CATALOG_PRICE_' . project\catalog::PRICE_ID];
                }
                if ($tp && ($price || $bProduct["custom_price"])){

                    $bProduct['DEALER_PRICE'] = $price;
                    $bProduct['DEALER_PRICE_FORMAT'] = number_format($price, project\catalog::ROUND, ",", " ");

                    // Количество на складе дилера
                    if(!isset($arDealerQuantities[$tp['ID']]))
                    {
                        $arDealerQuantities[$tp['ID']] = $tp['CATALOG_QUANTITY'];
                        $bProduct['DEALER_QUANTITY'] = $tp['CATALOG_QUANTITY'];
                    }
                    else
                    {
                        $bProduct['DEALER_QUANTITY'] = $arDealerQuantities[$tp['ID']];
                    }
                    if( $bProduct['DEALER_QUANTITY'] > 0 ){
                        $bProduct['NAL'] = 'Y';
                        $dealer['hasGoods'] = true;
                    }
                    if ($bProduct['DEALER_QUANTITY'] < $bProduct['quantity']){
                        $dealer['isFullOffer'] = false;
                        $bProduct['FULL_QUANTITY'] = 'N';
                        $dealer['BASKET_SUM'] += $bProduct['DEALER_PRICE'] * $bProduct['DEALER_QUANTITY'];

                        $arDealerQuantities[$tp['ID']] = 0;
                    } else {
                        $dealer['BASKET_SUM'] += $bProduct['DEALER_PRICE'] * $bProduct['quantity'];

                        $arDealerQuantities[$tp['ID']] -= $bProduct['quantity'];
                    }

                } else {
                    $dealer['isFullOffer'] = false;
                }
            } else {
                $dealer['isFullOffer'] = false;
            }
            $dealer['basketProducts'][$productKey] = $bProduct;
        }

        $dealer['BASKET_SUM_FORMAT'] = number_format($dealer['BASKET_SUM'], project\catalog::ROUND, ",", "&nbsp;");

        $dealer['TOTAL_SUM'] = $dealer['BASKET_SUM'];

        return $dealer;
    }


    // Сбор данных по предложению дилера
    static function collectDealerOfferInfo( $dealer, $arResult, $arParams ){

        $dealer['isFullOffer'] = true;
        $dealer['hasGoods'] = false;
        $dealer['WARNINGS'] = [];

        $dealer['DEALER_RATING'] = $dealer['UF_RATING']?$dealer['UF_RATING']:0;

        // Инфо по корзине дилера
        $dealer = static::getDealerBasketInfo( $dealer, $arResult, $arParams );

        // Проверка платёжных систем дилера - для фильтрации по ПС
        $dealer = project\basket::dealer_psOK( $dealer, $arResult, $arParams );

        // Проверка наличия типа оплаты (оплата/предоплата) для данного варианта оплаты
        $dealer = project\basket::dealer_payTypeOK( $dealer, $arResult, $arParams );

        // Если выбран вариант Самовывоз
        if( $arResult['delivType'] == 'samovyvoz' ){

            // Перечень ПВЗ дилера (точно в данном местоположении)
            $dealer_shop = new project\dealer_shop();
            $dealer['PVZ_LIST'] = $dealer_shop->getList(
                $dealer['ID'],
                $arResult['LOC']['ID'],
                true
            );

        // Если выбран вариант Доставка
        } else if(
            $arResult['delivType'] == 'delivery'
            &&
            $arParams['form_data']['deliveryDate']
        ){

            // Местоположения доставки для данного дилера + данного местоположения
            $dealer['deliveryLocation'] = false;
            $dl = new project\delivery_location();
            foreach( $arResult['LOC_CHAIN_IDS'] as $loc_id ) {
                if( !$dealer['deliveryLocation'] ){
                    $deliveryLocations = $dl->getList( $dealer['ID'], $loc_id );
                    if( count($deliveryLocations) > 0 ){
                        $dealer['deliveryLocation'] = $deliveryLocations[array_keys($deliveryLocations)[0]];
                    }
                }
            }
        }

        // Наличие ПВЗ (при варианте Самовывоз)
        $dealer = project\basket::dealer_pvzOK( $dealer, $arResult, $arParams );

        // Наличие доставки в данный город (при варианте Доставка)
        $dealer = project\basket::dealer_hasDelivInLoc( $dealer, $arResult, $arParams );

        // Проверка возможности разгрузки
        $dealer = project\basket::dealer_razgruzkaOK( $dealer, $arResult, $arParams );
        // Проверка наличия стоимости разгрузки
        $dealer = project\basket::dealer_razgruzkaPriceOK( $dealer, $arResult, $arParams );

        // Проверка возможности переноса
        $dealer = project\basket::dealer_perenosOK( $dealer, $arResult, $arParams );
        // Проверка наличия стоимости переноса
        $dealer = project\basket::dealer_perenosPriceOK( $dealer, $arResult, $arParams );

        // Проверка возможности подъема
        $dealer = project\basket::dealer_podyemOK( $dealer, $arResult, $arParams );
        // Проверка наличия стоимости подъема
        $dealer = project\basket::dealer_podyemPriceOK( $dealer, $arResult, $arParams );

        // Проверка по дате
        $dealer = project\basket::dealer_dateOK( $dealer, $arResult, $arParams );

        // Проверка по времени
        $dealer = project\basket::dealer_timeOK( $dealer, $arResult, $arParams );

        return $dealer;
    }


    // УСЛОВИЯ ПОДБОРА ПРЕДЛОЖЕНИЯ ДИЛЕРА
    static function checkDealerOffer( $dealer, $arResult, $arParams ){
        return (
            // Если у дилера есть хотя бы 1 товар из корзины (цена + наличие)
            $dealer['hasGoods']
            &&
            // Наличие доставки в данный город (при варианте Доставка)
            $dealer['hasDelivInLoc']
            &&
            // Наличие ПВЗ (при варианте Самовывоз)
            $dealer['pvzOK']
        );
    }


    // СБОР ПРЕДЛОЖЕНИЙ ДИЛЕРОВ
    static function collectDealerOffers( $arResult, $arParams ){

        // Комплектные предложения
        $arResult['FULL_OFFERS'] = array();

        // Некомплектные предложения
        $arResult['NOT_FULL_OFFERS'] = array();

        // УСЛОВИЯ НАЧАЛА ПОДБОРА
        if( static::checkBeforeOffersCollecting( $arResult, $arParams ) ){

            // Получим перечень дилеров
            $arResult['DEALERS'] = project\basket::getDealers();

            // Перебирём этих дилеров
            foreach ($arResult['DEALERS'] as $dealerKey => $dealer) {

                // Сбор данных по предложению дилера
                $dealer = static::collectDealerOfferInfo( $dealer, $arResult, $arParams );

                // Проверка дилера по условиям
                if( static::checkDealerOffer( $dealer, $arResult, $arParams ) ){

                    // Если выбран вариант доставка
                    if( $arResult['delivType'] == 'delivery' ){

                        // Определяем стоимость доставки для дилера
                        $dealer = project\basket::getDealerDelivPrice(
                            $arResult,
                            $dealer,
                            $arResult['delivType'],
                            $arParams['form_data'],
                            [
                                'normalGoods' => $arResult['normalGoods'],
                                'largeGoods' => $arResult['largeGoods'],
                                'normalGoodsWeight' => $arResult['normalGoodsWeight'],
                                'largeGoodsWeight' => $arResult['largeGoodsWeight']
                            ]
                        );
                    }

                    $dealer['TOTAL_SUM_FORMAT'] = number_format($dealer['TOTAL_SUM'], project\catalog::ROUND, ",", "&nbsp;");
                    
                    // Формируем уведомляющие сообщения по дилеру
                    $dealer = static::setDealerWarnings( $dealer, $arResult, $arParams );

                    // Добавляем предложение в список
                    if ( $dealer['isFullOffer'] ){

                        // Комплектные предложения
                        $arResult['FULL_OFFERS'][$dealer['ID']] = $dealer;

                    } else {

                        // "Недокомплектные" предложения
                        $arResult['NOT_FULL_OFFERS'][$dealer['ID']] = $dealer;
                    }
                }
            }

            // Если есть "недокомплектные" предложения
            if( count($arResult['NOT_FULL_OFFERS']) > 0 ){
                // применяем к ним фильтрацию
                $arResult['NOT_FULL_OFFERS'] = project\basket::filterNotFullOffers($arResult['NOT_FULL_OFFERS']);
            }
        }

        // Итоговый список предложений
        $arResult['OFFERS_LIST'] = count($arResult['FULL_OFFERS'])>0?$arResult['FULL_OFFERS']:$arResult['NOT_FULL_OFFERS'];

        return $arResult;
    }



    // Формируем уведомляющие сообщения по дилеру
    static function setDealerWarnings( $dealer, $arResult, $arParams ){
        // Дата доставки
        if( !$dealer['dateOK'] ){
            if( $dealer['nearDeliveryDate'] ){
                $dealer['WARNINGS'][] = 'Ближайшая дата доставки: '.$dealer['nearDeliveryDate'];
            } else {
                $dealer['WARNINGS'][] = 'Дата доставки: по договорённости';
            }
        }
        // Время доставки
        if( !$dealer['timeOK'] ){
            if( $dealer['deliveryTime'] ){
                $dealer['WARNINGS'][] = 'Время доставки: '.$dealer['deliveryTime'];
            } else {
                $dealer['WARNINGS'][] = 'Время доставки: по договорённости';
            }
        }
        // Вариант оплаты
        if( !$dealer['psOK'] ){

            //$dealer['WARNINGS'][] = 'Вариант оплаты отличается от выбранного';

			$psNames = [];
			foreach ( $dealer['UF_PSS'] as $ps_code ){
			    $arPS = project\ps::getByCode( $ps_code );
			    if( intval($arPS['ID']) > 0 ){
                    $psNames[] = strtolower($arPS['NAME']);
			    }
			}
			if( count($psNames) > 0 ){
                $dealer['WARNINGS'][] = 'Доступны только следующие варианты оплаты: '.implode(', ', $psNames);
			}
			
        }
        // Тип оплаты
        if( !$dealer['payTypeOK'] ){
            //$dealer['WARNINGS'][] = 'Тип оплаты отличается от выбранного';
			 $dealer['WARNINGS'][] = $dealer['paytype_mess'];
        }
        // Разгрузка
        if( !$dealer['razgruzkaOK'] ){
            $dealer['WARNINGS'][] = 'Данный дилер не осуществляет разгрузку';
        } else if( !$dealer['razgruzkaPriceOK'] ){
            $dealer['WARNINGS'][] = 'Стоимость разгрузки договорная';
        }
        // Подъём
        if( !$dealer['podyemOK'] ){
            $dealer['WARNINGS'][] = 'Данный дилер не осуществляет подъём на этаж';
        } else if( !$dealer['podyemPriceOK'] ){
            $dealer['WARNINGS'][] = 'Стоимость подъёма договорная';
        }
        // Перенос
        if( !$dealer['perenosOK'] ){
            $dealer['WARNINGS'][] = 'Данный дилер не осуществляет перенос';
        } else if( !$dealer['perenosPriceOK'] ){
            $dealer['WARNINGS'][] = 'Стоимость переноса договорная';
        }
        $_SESSION['DEALER_WARNINGS'][ $dealer['ID'] ] = $dealer['WARNINGS'];
        return $dealer;
    }



    // Первоначальная основная инфа по корзине
    static function getGeneralInfo( $arResult, $arParams ){
        \Bitrix\Main\Loader::includeModule('iblock');
        if(
            $arParams['IS_AJAX'] != 'Y'
            ||
            $_POST['newBasketQuantity']
            ||
            $_POST['remove_product_id']
        ){    unset($_SESSION['basketInfo']);    }
        if ( $_SESSION['basketInfo'] ){

            $arResult['basketInfo'] = $_SESSION['basketInfo'];

        } else {

            // Все дилеры
            $dealer = new project\dealer();
            $allDealers = $dealer->allList();
            $stop_dealers = [];
            foreach ( $allDealers as $dealer ){
                if( !$dealer['UF_SHOW_IN_CATALOG'] ){    $stop_dealers[] = $dealer['ID'];    }
            }
            $stop_dealers = array_unique( $stop_dealers );

            $arResult['basketInfo'] = project\user_basket::info(
                false, $arParams['delivID'], null, null,
                $arParams['IS_MOB_APP']=='Y'?'mb':'s1'
            );

            // Получим мин. цены в товарах $basketInfo['basketProducts']
            $arResult['basketInfo']['minPricesSum'] = 0;
            foreach( $arResult['basketInfo']['basketProducts'] as $key => $arEl ){
                if( !$arEl['custom_price'] ){
                    $filter = [
                        "IBLOCK_ID" => project\catalog::tpIblockID(),
                        "ACTIVE" => "Y",
                        "PROPERTY_CML2_LINK" => $arEl['tov_id'],
                        "PROPERTY_LOCATION" => $_SESSION['LOC']['REGION_ID'],
                    ];
                    if (count($stop_dealers) > 0){
                        $filter['!PROPERTY_DEALER'] = $stop_dealers;
                    }
                    $fields = [
                        "ID",
                        "PROPERTY_CML2_LINK",
                        "PROPERTY_DEALER",
                        "PROPERTY_LOCATION", "CATALOG_GROUP_" . project\catalog::PRICE_ID
                    ];
                    $offers = \CIBlockElement::GetList(
                        ["CATALOG_PRICE_" . project\catalog::PRICE_ID => "ASC"],
                        $filter, false, ["nTopCount" => 1], $fields
                    );
                    if ($offer = $offers->GetNext()){
                        $min_price = $offer["CATALOG_PRICE_" . project\catalog::PRICE_ID];
                        $min_price_format = number_format($min_price, project\catalog::ROUND, ",", " ");
                        $arResult['basketInfo']['basketProducts'][$key]['min_price'] = $min_price;
                        $arResult['basketInfo']['basketProducts'][$key]['min_price_format'] = $min_price_format;
                        $arResult['basketInfo']['minPricesSum'] += $min_price * $arEl['quantity'];
                    }
                } else {
                    $arResult['basketInfo']['basketProducts'][$key]['min_price'] = $arEl['price'];
                    $arResult['basketInfo']['basketProducts'][$key]['min_price_format'] = number_format($arEl['price'], project\catalog::ROUND, ",", " ");
                    $arResult['basketInfo']['minPricesSum'] += $arEl['price'] * $arEl['quantity'];
                }
            }
            $arResult['basketInfo']['minPricesSumFormat'] = number_format($arResult['basketInfo']['minPricesSum'], project\catalog::ROUND, ",", " ");

            // Проверка корзины (на местоположения)
            if( $arParams['IS_AJAX'] != 'Y' ){
                $arResult = project\basket::check( $arResult, $arParams );
            }

            unset($arResult['basketInfo']['basket']);
            unset($arResult['basketInfo']['arBasket']);
            
            $_SESSION['basketInfo'] = $arResult['basketInfo'];
        }

        $arResult['basket_cnt'] = $arResult['basketInfo']['basketProductsCNT'].' '.tools\funcs::pfCnt($arResult['basketInfo']['basketProductsCNT'], "товар", "товара", "товаров");

        return $arResult;
    }


    // Проверка корзины на местоположения
    static function check( $arResult, $arParams ){
        $productsToOrder = [];
        // Перебираем корзину
        foreach ( $arResult['basketInfo']['basket'] as $basketItem ){
            $product_id = $basketItem->product['ID'];
            // Проверим - есть ли у данного товара+местоположения ТП дилеров
            $filter = Array(
                "IBLOCK_ID" => project\catalog::tpIblockID(),
                "ACTIVE" => "Y",
                "PROPERTY_CML2_LINK" => $product_id,
                "PROPERTY_LOCATION" => $_SESSION['LOC']['REGION_ID'],
            );
            $fields = Array( "ID", "NAME", "PROPERTY_CML2_LINK", 'PROPERTY_LOCATION' );
            $skus = \CIBlockElement::GetList(
                array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
            );
            // Если для данного товара+местоположения ТП дилеров нет
            if( $skus->SelectedRowsCount() == 0 ){
                // Удалим товар из корзины
                $basketUserID = Sale\Fuser::getId();
                $siteID = \Bitrix\Main\Context::getCurrent()->getSite();
                $userBasket = Sale\Basket::loadItemsForFUser( $basketUserID, $siteID );
                $quantity = $userBasket->getItemById($basketItem->id)->getQuantity();
                $userBasket->getItemById($basketItem->id)->delete();
                $userBasket->save();
                // Добавим товар в "под заказ"
                $productsToOrder[$product_id] += $quantity;
            }
        }

        // Если есть новые товары "под заказ"
        if( count( $productsToOrder ) > 0 ){
            $arResult['basketInfo'] = project\user_basket::info( false, $arParams['delivID']);
            $under_order_ids = array_keys( $arResult['under_order_goods'] );
            $under_order_ids = array_merge( $under_order_ids, array_keys($productsToOrder) );
            $under_order_ids = array_unique($under_order_ids);
            // Добавим их в куку
            global $APPLICATION;
            if( count($under_order_ids) > 0 ){
                $cookie_val = implode('|', $under_order_ids);
            } else {
                $cookie_val = '';
            }
            $APPLICATION->set_cookie('under_order', $cookie_val, time()+2592000, '/', '.'.\Bitrix\Main\Config\Option::get('main', 'server_name'));

            $arUnderOrderCnt = $productsToOrder;
            foreach($under_order_ids as $under_order_id)
            {
                if(!$arUnderOrderCnt[$under_order_id])
                {
                    $arUnderOrderCnt[$under_order_id] = $arResult['under_order_goods'][$under_order_id]["QUANTITY"];
                }
            }

            $APPLICATION->set_cookie('under_order_cnt', serialize($arUnderOrderCnt), time()+2592000, '/', '.'.\Bitrix\Main\Config\Option::get('main', 'server_name'));

            $arResult['under_order_goods'] = project\catalog::getUnderOrderGoods($under_order_ids, $arUnderOrderCnt);
        }
        return $arResult;
    }


    // Получим тип активного варианта доставки
    static function getActiveDeliveryType( $arResult, $arParams ){
        $arResult['delivType'] = false;
        if( intval($arParams['form_data']['ds']) > 0 ){
            foreach( $arResult['DS_LIST'] as $ds ){
                if( $ds['ID'] == $arParams['form_data']['ds'] ){    $arResult['delivType'] = $ds['delivType'];    }
            }
        }
        return $arResult;
    }




    // Фильтрация некомплектных предложений
    static function filterNotFullOffers( $notFullOffers ){
        if( count($notFullOffers) > 0 ){
            // Оставляем только некомплектные предложения с макс. количеством
            $arQuantities = [];
            foreach ( $notFullOffers as $dealer_id => $dealer ){
                $qnt = 0;
                foreach ( $dealer['basketProducts'] as $bProduct ){
                    $qnt += $bProduct['quantity']?$bProduct['quantity']:$bProduct['DEALER_QUANTITY'];
                }
                $arQuantities[$qnt][$dealer_id] = $dealer;
            }
            $max_qnt_key = max(array_keys($arQuantities));
            $notFullOffers = $arQuantities[$max_qnt_key];
        }
        return $notFullOffers;
    }



    // Сортировка предложений дилеров
    static function sortDealerOffers( $arResult ){
        if( count($arResult['OFFERS_LIST']) > 1 ){
            // Если пользователь выбрал конкретную сортировку
            if( strlen($arResult['offers_sort']['CODE']) > 0 ){
                // По стоимости корзины
                if ($arResult['offers_sort']['CODE'] == 'BASKET_SUM'){
                    $sotArray = array();
                    foreach ($arResult['OFFERS_LIST'] as $dealer_id => $dealer) {
                        $sotArray[$dealer_id] = $dealer['BASKET_SUM'];
                    }
                // По рейтингу дилера
                } else if ($arResult['offers_sort']['CODE'] == 'DEALER_RATING'){
                    $sotArray = array();
                    foreach ($arResult['OFFERS_LIST'] as $dealer_id => $dealer) {
                        $sotArray[$dealer_id] = $dealer['DEALER_RATING'];
                    }
                }
                if ($arResult['offers_sort']['ORDER'] == 'ASC'){
                    array_multisort($sotArray, SORT_ASC, SORT_NUMERIC, $arResult['OFFERS_LIST']);
                } else if ($arResult['offers_sort']['ORDER'] == 'DESC'){
                    array_multisort($sotArray, SORT_DESC, SORT_NUMERIC, $arResult['OFFERS_LIST']);
                }
            } else {
                // Сортируем комплектные предложения по дефолтной логике
                $dealerSort = new project\dealer_sort();
                $arResult['OFFERS_LIST'] = $dealerSort->go($arResult, $arParams);
            }
        }
        return $arResult;
    }



    static function getDealers(){
        $dealers = [];
        $obCache = new \CPHPCache();
        $cache_time = 24*60*60;
        $cache_id = 'basketDealers';
        $cache_path = '/basketDealers/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $dealer = new project\dealer();
            $dealers = $dealer->getList( [], 'N', 'ID', 'DESC', true );
            $obCache->EndDataCache(array('dealers' => $dealers));
        }
        return $dealers;
    }





    static function dealer_psOK( $dealer, $arResult, $arParams ){
        $dealer['psOK'] = true;
        // Проверка платёжных систем дилера - для фильтрации по ПС
        // Если у дилера не указаны ПС - отсекаем
        if(
            !is_array($dealer['UF_PSS'])
            ||
            (
                is_array($dealer['UF_PSS'])
                &&
                count($dealer['UF_PSS']) == 0
            )
        ){
            $dealer['psOK'] = false;
        // Если в фильтре указаны ПС
        } else if( is_array($dealer['UF_PSS']) ){
            // Если указанные ПС не указаны у дилера - отсекаем
            if( !array_intersect(
                [ $arParams['form_data']['ps'] ],
                $dealer['UF_PSS'] )
            ){     $dealer['psOK'] = false;     }
        }
        return $dealer;
    }

    static function dealer_payTypeOK( $dealer, $arResult, $arParams ){
        $dealer['payTypeOK'] = false;
        if(
            $dealer['psOK']
            &&
            strlen( $arParams['form_data']['pay_type'] ) > 0
            &&
            strlen( $dealer['UF_PAY_TYPES'] ) > 0
        ){

            $dealer_pay_types = tools\funcs::json_to_array($dealer['UF_PAY_TYPES']);

            if(
                in_array(
                    $arParams['form_data']['pay_type'],
                    $dealer_pay_types[ $arParams['form_data']['ps'] ]
                )
            ){

                $dealer['payTypeOK'] = true;
				$dealer['paytypes']= $dealer_pay_types;

			} else {

				if ($arParams['form_data']['ps'] == 'cash'){

					if ($arParams['form_data']['pay_type']=='pre'){
						$mes = "Оплата наличными доступна только при получении";
                    } else {
						$mes = "Оплата наличными доступна только при оплате сразу";
                    }
				
                } elseif ($arParams['form_data']['ps'] == 'bank_card') {

					if ($arParams['form_data']['pay_type'] == 'pre'){
						$mes = "Оплата банковской картой доступна только при получении";
                    } else {
						$mes = "Оплата банковской картой доступна только при оплате сразу";
                    }

                } elseif ($arParams['form_data']['ps'] == 'electronic_money') {

					if ($arParams['form_data']['pay_type'] == 'pre'){
						$mes = "Оплата электронными деньгами доступна только при оплате при получении";
                    } else {
						$mes = "Оплата электронными деньгами доступна только при оплате сразу";
                    }

                } elseif ($arParams['form_data']['ps'] == 'invoice') {

                    if ($arParams['form_data']['pay_type'] == 'pre'){
                        $mes = "Оплата по счёту доступна только при получении";
                    } else {
                        $mes = "Оплата по счёту доступна только при оплате сразу";
                    }
                }
				$dealer['paytype_mess'] = $mes;
			}
        }
        return $dealer;
    }

    static function dealer_pvzOK( $dealer, $arResult, $arParams ){
        $dealer['pvzOK'] = false;
        if(
            $arResult['delivType'] != 'samovyvoz'
            ||
            (
                $arResult['delivType'] == 'samovyvoz'
                &&
                is_array($dealer['PVZ_LIST']) && count($dealer['PVZ_LIST']) > 0
            )
        ){    $dealer['pvzOK'] = true;    }
        return $dealer;
    }

    static function dealer_hasDelivInLoc( $dealer, $arResult, $arParams ){
        $dealer['hasDelivInLoc'] = false;
        if(
            (
                $arResult['delivType'] == 'delivery'
                &&
                intval($dealer['deliveryLocation']['ID']) > 0
            )
            ||
            $arResult['delivType'] != 'delivery'
        ){    $dealer['hasDelivInLoc'] = true;    }
        return $dealer;
    }

    static function dealer_razgruzkaOK( $dealer, $arResult, $arParams ){
        $dealer['razgruzkaOK'] = true;
        if(
            $arResult['delivType'] == 'delivery'
            &&
            $arParams['form_data']['needRazgruzka'] == 'Да'
        ){
            $delivSettings = [];
            if( strlen($dealer['deliveryLocation']['UF_DELIV_SETTINGS']) > 0 ){
                $delivSettings = tools\funcs::json_to_array($dealer['deliveryLocation']['UF_DELIV_SETTINGS']);
            }
            $dealer['razgruzkaOK'] = (
                is_array($delivSettings['dop_services'])
                &&
                in_array('unloading_of_goods', $delivSettings['dop_services'])
            );
        }
        return $dealer;
    }
    static function dealer_razgruzkaPriceOK( $dealer, $arResult, $arParams ){
        $dealer['razgruzkaPriceOK'] = true;
        if(
            $arResult['delivType'] == 'delivery'
            &&
            $arParams['form_data']['needRazgruzka'] == 'Да'
        ){
            $delivSettings = [];
            if( strlen($dealer['deliveryLocation']['UF_DELIV_SETTINGS']) > 0 ){
                $delivSettings = tools\funcs::json_to_array($dealer['deliveryLocation']['UF_DELIV_SETTINGS']);
            }
            $dealer['razgruzkaPriceOK'] = (
                is_array($delivSettings['dop_services'])
                &&
                in_array('unloading_of_goods', $delivSettings['dop_services'])
                &&
                strlen($delivSettings['unloading_of_goods']['price']) > 0
            );
        }
        return $dealer;
    }

    static function dealer_perenosOK( $dealer, $arResult, $arParams ){
        $dealer['perenosOK'] = true;
        if(
            $arResult['delivType'] == 'delivery'
            &&
            $arParams['form_data']['needPerenos'] == 'Да'
        ){
            $delivSettings = [];
            if( strlen($dealer['deliveryLocation']['UF_DELIV_SETTINGS']) > 0 ){
                $delivSettings = tools\funcs::json_to_array($dealer['deliveryLocation']['UF_DELIV_SETTINGS']);
            }
            $dealer['perenosOK'] = (
                is_array($delivSettings['dop_services'])
                &&
                in_array('transfer_of_goods', $delivSettings['dop_services'])
            );
        }
        return $dealer;
    }
    static function dealer_perenosPriceOK( $dealer, $arResult, $arParams ){
        $dealer['perenosPriceOK'] = true;
        if(
            $arResult['delivType'] == 'delivery'
            &&
            $arParams['form_data']['needPerenos'] == 'Да'
        ){
            $delivSettings = [];
            if( strlen($dealer['deliveryLocation']['UF_DELIV_SETTINGS']) > 0 ){
                $delivSettings = tools\funcs::json_to_array($dealer['deliveryLocation']['UF_DELIV_SETTINGS']);
            }
            $dealer['perenosPriceOK'] = (
                is_array($delivSettings['dop_services'])
                &&
                in_array('transfer_of_goods', $delivSettings['dop_services'])
                &&
                strlen($delivSettings['transfer_of_goods']['price']) > 0
            );
        }
        return $dealer;
    }

    static function dealer_podyemOK( $dealer, $arResult, $arParams ){
        $dealer['podyemOK'] = true;
        if(
            $arResult['delivType'] == 'delivery'
            &&
            $arParams['form_data']['needPodyem'] == 'Да'
        ){
            $delivSettings = [];
            if( strlen($dealer['deliveryLocation']['UF_DELIV_SETTINGS']) > 0 ){
                $delivSettings = tools\funcs::json_to_array($dealer['deliveryLocation']['UF_DELIV_SETTINGS']);
            }
            $dealer['podyemOK'] = (
                is_array($delivSettings['dop_services'])
                &&
                in_array('rise_to_the_floor', $delivSettings['dop_services'])
            );
        }
        return $dealer;
    }
    static function dealer_podyemPriceOK( $dealer, $arResult, $arParams ){
        $dealer['podyemPriceOK'] = true;
        if(
            $arResult['delivType'] == 'delivery'
            &&
            $arParams['form_data']['needPodyem'] == 'Да'
        ){
            $delivSettings = [];
            if( strlen($dealer['deliveryLocation']['UF_DELIV_SETTINGS']) > 0 ){
                $delivSettings = tools\funcs::json_to_array($dealer['deliveryLocation']['UF_DELIV_SETTINGS']);
            }
            $dealer['podyemPriceOK'] = (
                is_array($delivSettings['dop_services'])
                &&
                in_array('rise_to_the_floor', $delivSettings['dop_services'])
                &&
                strlen($delivSettings['rise_to_the_floor']['no_lift']['floor_price']) > 0
            );
        }
        return $dealer;
    }

    static function dealer_dateOK( $dealer, $arResult, $arParams ){
        $dealer['dateOK'] = false;
        if( $arResult['delivType'] == 'samovyvoz' ){
            $dealer['dateOK'] = true;
        } else if( $arResult['delivType'] == 'delivery' ){
            $dealer['nearDeliveryDate'] = false;
            $delivSettings = [];
            if( strlen($dealer['deliveryLocation']['UF_DELIV_SETTINGS']) > 0 ){
                $delivSettings = tools\funcs::json_to_array($dealer['deliveryLocation']['UF_DELIV_SETTINGS']);
            }
            if( isset($delivSettings['min_delivery_time']) ){
                $datetime1 = date_create(ConvertDateTime(date('d.m.Y').' 00:00:00', "YYYY-MM-DD HH:MI:SS", "ru"));
                $datetime2 = date_create(ConvertDateTime($arParams['form_data']['deliveryDate'].' 00:00:00', "YYYY-MM-DD HH:MI:SS", "ru"));
                $interval = date_diff($datetime1, $datetime2);
                // количество дней доставки (по выбранной в корзине дате)
                $deliv_days = $interval->format('%d%');
                // ближайшая дата доставки исходя из настроек дилера
                $dealer['nearDeliveryDate'] = date("d.m.Y", strtotime("+".intval($delivSettings['min_delivery_time'])." day"));
                if( $deliv_days >= intval($delivSettings['min_delivery_time']) ){
                    $dealer['dateOK'] = true;
                }
            }
        }
        return $dealer;
    }

    static function dealer_timeOK( $dealer, $arResult, $arParams ){
        $dealer['timeOK'] = false;
        if( $arResult['delivType'] == 'samovyvoz' ){
            $dealer['timeOK'] = true;
        } else if( $arResult['delivType'] == 'delivery' ){
            $dealer['deliveryTime'] = false;
            $delivSettings = [];
            if( strlen($dealer['deliveryLocation']['UF_DELIV_SETTINGS']) > 0 ){
                $delivSettings = tools\funcs::json_to_array($dealer['deliveryLocation']['UF_DELIV_SETTINGS']);
            }
            $delivery_time_ot = $delivSettings['delivery_time_ot'];
            $delivery_time_do = $delivSettings['delivery_time_do'];
            if(
                strlen($delivery_time_ot) > 0 && $delivery_time_ot != 'empty'
                &&
                strlen($delivery_time_do) > 0 && $delivery_time_do != 'empty'
            ){
                $dealer['deliveryTime'] = 'с '.$delivery_time_ot.' по '.$delivery_time_do;
                // Время дилера
                $dealer_h_ot = preg_replace('/:00/', '', $delivery_time_ot);
                $dealer_h_do = preg_replace('/:00/', '', $delivery_time_do);
                $dealer_h_ot = preg_replace('/^0([0-9])$/', '$1', $dealer_h_ot);
                $dealer_h_do = preg_replace('/^0([0-9])$/', '$1', $dealer_h_do);
                $dealer_h_range = [];
                for ( $h = $dealer_h_ot; $h <= $dealer_h_do; $h++ ){  $dealer_h_range[] = $h;  }
                // Время в корзине
                $deliveryTime = $arParams['form_data']['deliveryTime'];
                $timeRange = project\order::$deliveryTimeList[$deliveryTime]['VALUES'];
                $h_ot = $timeRange[0];   $h_do = $timeRange[1];
                $h_range = [];
                for ( $h = $h_ot; $h <= $h_do; $h++ ){  $h_range[] = $h;  }
                // Проверка наличия пересечений по часам
                $dealer['timeOK'] = count(array_intersect($dealer_h_range, $h_range)) > 0;
            }
        }
        return $dealer;
    }


    static function dateTimeIsSelected( $arResult, $arParams ){
        $arResult['dateTimeIsSelected'] = false;
        if(
            (
                $arResult['delivType'] == 'delivery'
                &&
                strlen( $arParams['form_data']['deliveryDate'] ) > 0
                &&
                $arParams['form_data']['deliveryTime'] != 'empty'
            )
            ||
            $arResult['delivType'] != 'delivery'
        ){     $arResult['dateTimeIsSelected'] = true;     }
        return $arResult;
    }
    
    

    // Определяем стоимость доставки для дилера
    static function getDealerDelivPrice(
        $arResult,
        $dealer,
        $delivType,
        $form_data,
        $goodsInfo
    ){

        $dealer['DELIVERY_PRICE'] = 'договорная';
        $largeGoods = $goodsInfo['largeGoods'];

        // Найдём у данного дилера подходящее местоположение доставки
        if( !isset($dealer['deliveryLocation']) ){
            $dl = new project\delivery_location();
            foreach( $arResult['LOC_CHAIN_IDS'] as $loc_id ) {
                if( !$dealer['deliveryLocation'] ){
                    $deliveryLocations = $dl->getList( $dealer['ID'], $loc_id );
                    if( count( $deliveryLocations ) > 0 ){
                        $dealer['deliveryLocation'] = $deliveryLocations[array_keys($deliveryLocations)[0]];
                    }
                }
            }
        }
        
        // Если местоположение доставки у дилера найдено
        if(  strlen($dealer['deliveryLocation']['UF_DELIV_SETTINGS']) > 0  ){

            $delivSettings = \AOptima\Tools\funcs::json_to_array($dealer['deliveryLocation']['UF_DELIV_SETTINGS']);

            if(
                $form_data['floor'] > 0
                &&
                $form_data['needPodyem'] == 'Да'
                &&
                count($largeGoods) > 0
                &&
                $delivSettings['rise_to_the_floor']['not_rise_large_goods'] == 'Y'
            ){
                $dealer['WARNINGS'][] = 'Внимание! В корзине есть крупногабаритные товары.<br>Данный дилер не осуществляет подъём крупногабаритных товаров.';
            }

            // Установим бесплатную доставку, если дилер настроил пороговую сумму
            // и стоимость корзины её превышает

            if(
                strlen($delivSettings['free_delivery_basket_sum']) > 0
                &&
                $dealer['BASKET_SUM'] >= $delivSettings['free_delivery_basket_sum']
            ){

                $dealer['DELIVERY_PRICE'] = 0;

            } else {

                // Расчёт стоимости доставки
                $dealer['DELIVERY_PRICE'] = project\delivery_price::calc(
                    $dealer['deliveryLocation'],
                    $arResult['basketInfo'],
                    $dealer,
                    $arResult['ADDRESS_COORDS'],
                    $form_data,
                    $goodsInfo
                );

            }

        }

        $r = preg_match("/[а-яёА-ЯЁ]+/i", $dealer['DELIVERY_PRICE']);
        if( $r ){

            $dealer['DELIVERY_PRICE_FORMAT'] = false;
            $dealer['WARNINGS'][] = 'Стоимость доставки: договорная';

        } else {

            $dealer['DELIVERY_PRICE_FORMAT'] = number_format($dealer['DELIVERY_PRICE'], project\catalog::ROUND, ",", "&nbsp;");
            $dealer['TOTAL_SUM'] = $dealer['BASKET_SUM'] + $dealer['DELIVERY_PRICE'];
        }

        return $dealer;
    }




    // Варианты списка по лифту
    static function getLiftEnums(){
        $lift_enums = [];
        $obCache = new \CPHPCache();
        $cache_time = 5*60;
        $cache_id = 'lift_enums';
        $cache_path = '/lift_enums/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $props = \CUserTypeEntity::GetList(array($by => $order), array('ENTITY_ID' => 'HLBLOCK_' . project\delivery_template::HIBLOCK_ID, 'FIELD_NAME' => 'UF_LIFT'));
            while ($prop = $props->Fetch()) {
                $enums = \CUserFieldEnum::GetList(array(), array("USER_FIELD_ID" => $prop['ID'],));
                while ($enum = $enums->GetNext()) {    $lift_enums[$enum['XML_ID']] = $enum;    }
            }
            $obCache->EndDataCache(array('lift_enums' => $lift_enums));
        }
        return $lift_enums;
    }




    // Разделение товаров на обычные и крупногабаритные
    static function getRegularAndLargeProducts( $arResult ){
        $arResult['normalGoods'] = [];   $arResult['largeGoods'] = [];
        $arResult['normalGoodsWeight'] = 0;   $arResult['largeGoodsWeight'] = 0;
        foreach( $arResult['basketInfo']['basketProducts'] as $basketProduct ){
            $el = \AOptima\Tools\el::info($basketProduct['el']['ID']);
            if( $el['PROPERTY_MAX_GABARIT_VALUE'] >= project\delivery_price::LARGE_SIZE ){
                $arResult['largeGoodsWeight'] += $el['PROPERTY_CALC_WEIGHT_VALUE'] * $basketProduct['quantity'];
                $arResult['largeGoods'][] = $basketProduct;
            } else {
                $arResult['normalGoodsWeight'] += $el['PROPERTY_CALC_WEIGHT_VALUE'] * $basketProduct['quantity'];
                $arResult['normalGoods'][] = $basketProduct;
            }
        }
        $arResult['largeGoodsWeight'] = $arResult['largeGoodsWeight'] / 1000;
        $arResult['normalGoodsWeight'] = $arResult['normalGoodsWeight'] / 1000;
        return $arResult;
    }





    static function getMapFilePath(){
        $file_path = '/local/php_interface/basket/articles_map_file.csv';
        return $_SERVER['DOCUMENT_ROOT'].$file_path;
    }


    static function getMapTable(){
        $table = [];
        $mapFilePath = static::getMapFilePath();
        if( file_exists( $mapFilePath ) ){
            $lines = file($mapFilePath);
            foreach ( $lines as $key => $value ){
                $value = explode(';', mb_convert_encoding($value, 'UTF-8', 'windows-1251'));
                $value = str_replace(array("\n", "\r"), "", $value);
                $lines[$key] = $value;
            }
            $titlesLine = $lines[0];
            foreach ( $titlesLine as $key => $title ){
                $titlesLine[$key] = trim(strtoupper($title));
            }
            unset($lines[0]);
            if(
                in_array('EXTERNAL_ID', $titlesLine)
                &&
                in_array('KNAUF_ID', $titlesLine)
            ){
                $knauf_id_key = array_search('KNAUF_ID', $titlesLine);
                $external_id_key = array_search('EXTERNAL_ID', $titlesLine);
                if( count($lines) > 0 ){
                    foreach ( $lines as $line_number => $line ){
                        $knauf_id = is_integer($knauf_id_key)?trim(strip_tags($line[$knauf_id_key])):null;
                        $knauf_id = project\catalog::treatKnaufID( $knauf_id );
                        $external_id = is_integer($external_id_key)?trim(strip_tags($line[$external_id_key])):null;
                        if(
                            !(
                                strlen($knauf_id) > 0
                                &&
                                strlen($external_id) > 0
                            )
                        ){    unset($lines[$line_number]);    }
                    }
                }
                if( count($lines) > 0 ){
                    foreach ( $lines as $line_number => $line ){
                        $knauf_id = is_integer($knauf_id_key)?trim(strip_tags($line[$knauf_id_key])):null;
                        $knauf_id = project\catalog::treatKnaufID( $knauf_id );
                        $external_id = is_integer($external_id_key)?trim(strip_tags($line[$external_id_key])):null;
                        $table[ $external_id ] = $knauf_id;
                    }
                }
            }
        }
        return $table;
    }


    static function getKnaufIdByMapTable( $ext_id ){
        $table = static::getMapTable();
        if( isset( $table[ $ext_id ] ) ){
            return $table[ $ext_id ];
        }
        return null;
    }











}