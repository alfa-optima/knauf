<?php
namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('highloadblock');



class delivery_template {

    const HIBLOCK_ID = 3;

    function __construct(){
        $this->hlblock_id = static::HIBLOCK_ID;
        $this->hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($this->hlblock_id)->fetch();
        $this->entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($this->hlblock);
        $this->entity_data_class = $this->entity->getDataClass();
        $this->entity_table_name = $this->hlblock['TABLE_NAME'];
        $this->sTableID = 'tbl_'.$this->entity_table_name;
    }



    // add
    public function add( $arData ){
        $result = $this->entity_data_class::add($arData);
        if ($result->isSuccess()) {
            return true;
        } else {
            tools\logger::addError('Ошибка создания шаблона доставки - '.implode(', ', $result->getErrors()));
        }
        return false;
    }



    // update
    public function update( $id, $arData ){
        $result = $this->entity_data_class::update($id, $arData);
        if ( $result->isSuccess() ){
            return true;
        } else {
            tools\logger::addError('Ошибка изменения шаблона доставки - '.implode(', ', $result->getErrors()));
        }
        return false;
    }



    // getList
    public function getList( $user_id, $limit = false, $loc_id = false ){
        $elements = array();
        $arFilter = array();
        if( intval($user_id) > 0 ){     $arFilter['UF_USER'] = $user_id;        }
        if( intval($loc_id) > 0 ){     $arFilter['UF_LOC_ID'] = $loc_id;        }
        $arSelect = array('*');
        $arOrder = array("ID" => "ASC");
        $arInfo = array(
            "select" => $arSelect,
            "filter" => $arFilter,
            "order" => $arOrder
        );
        if( intval($limit) > 0 ){   $arInfo['limit'] = intval($limit);   }
        $rsData =  $this->entity_data_class::getList( $arInfo );
        $rsData = new \CDBResult($rsData,  $this->sTableID);
        while($element = $rsData->Fetch()){
            $elements[$element['ID']] = $element;
        }
        return $elements;
    }





    // удаление
    public function delete( $id ){
        $result = $this->entity_data_class::delete($id);
        if ( $result->isSuccess() ){
            return true;
        } else {
            tools\logger::addError('Ошибка удаления шаблона доставки - '.implode(', ', $result->getErrors()));
        }
        return false;
    }





}