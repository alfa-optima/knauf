<?php
namespace AOptima\Project;
use AOptima\Project as project;



class search {



    // Поиск элементов инфоблока по поисковому индексу
    static function iblockElementsByQ( $q, $stemming = false ){

        \Bitrix\Main\Loader::includeModule('iblock');
        $filter = Array(
        	"IBLOCK_ID" => $catalogIblockID,
        	"ACTIVE" => "Y",
        	"NAME" => $q."%"
        );
        $fields = Array( "ID", "NAME" );
        $dbElements = \CIBlockElement::GetList(
        	array("SORT"=>"ASC"), $filter, false, false, $fields
        );
        while ($element = $dbElements->GetNext()){
            $search_results[] = $element['ID'];
        }

        $filter = Array(
            "IBLOCK_ID" => $catalogIblockID,
            "ACTIVE" => "Y",
            "NAME" => "%".$q."%",
            "!ID" => $search_results
        );
        $fields = Array( "ID", "NAME" );
        $dbElements = \CIBlockElement::GetList(
            array("SORT"=>"ASC"), $filter, false, false, $fields
        );
        while ($element = $dbElements->GetNext()){
            $search_results[] = $element['ID'];
        }

        /*\Bitrix\Main\Loader::includeModule('search');

        $q = strip_tags(trim($q));

        $catalogIblockID = project\catalog::catIblockID();

        $search_results = array();
        $startFilter = array("S%");

        // Файл кеша
        $chache_id = md5(strtolower($q)).$stemming;
        $cache_path = $_SERVER['DOCUMENT_ROOT'].'/upload/search_results/';
        $file_path = $cache_path.$chache_id.".txt";
        if( !file_exists($cache_path) ){    mkdir($cache_path, 0700); }

        // Проверка наличия кеш-файла
        if( file_exists( $file_path ) ){
            $content = file_get_contents( $file_path );
            if( strlen($content) > 0 ){
                $search_results = explode('|', $content);
            }
        } else {

            // Цикл поиска
            do {
                $results = array();
                $ids_filter = array_merge($startFilter, $search_results);
                $ids_filter = array_unique($ids_filter);

                // Запрос
                $obSearch = new \CSearch;
                $obSearch->Search(
                    array(
                        "QUERY" => $q,
                        "SITE_ID" => LANG,
                        array(
                            array(
                                "=MODULE_ID" => "iblock",
                                "!ITEM_ID" => $ids_filter,
                                "PARAM2" => $catalogIblockID
                            )
                        )
                    ),
                    array("TITLE_RANK" => "DESC"),
                    array('STEMMING' => $stemming)
                );

                if ($obSearch->errorno != 0){} else {
                    while($item = $obSearch->GetNext()){
                        $results[] = $item['ITEM_ID'];
                        $search_results[] = $item['ITEM_ID'];
                    }
                }
            } while ( count($results) > 0 );

            $search_results = array_unique($search_results);

            // Запись в кеш-файл
            $file = fopen($file_path, "w");
            $res = fwrite( $file,  implode('|', $search_results) );
            fclose($file);
        }*/

        return $search_results;
    }






}