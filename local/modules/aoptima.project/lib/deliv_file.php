<?php

namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


class deliv_file {

    use \files___trait;

    const MAX_FILE_SIZE = 5242880;

    const TEMP_UPLOAD_DIR = "deliveryFilesTMP";
    const TEMP_UPLOAD_PATH = "/upload/deliveryFilesTMP/";


    protected $FILES = false;
    protected $doc_root = false;
    protected $fileTypes = array(
        'pdf', 'doc', 'docx', 'xls', 'xlsx',
        'txt', 'plain'
    );
    protected $fileTypesInc = array(
        'ms-excel', 'ms-word', 'officedocument'
    );
    protected $file_name = false;
    protected $new_file_name = false;
    protected $file_path = false;
    protected $width = false;
    protected $height = false;
    protected $file_moved = false;
    protected $file_extension = false;





    // конструктор объекта
    function __construct($files = false){
        $this->doc_root = $_SERVER["DOCUMENT_ROOT"];
        // Перемещение файла во временную папку
        if ( $this->moveFile($files) ){
            $this->file_moved = true;
        }
    }


    public function getFileTypes(){
        return $this->fileTypes;
    }



    protected function onResponse($obj){
        echo '<script type="text/javascript">
		window.parent.onDelivFileResponse("'.$obj.'");
		</script>';
    }



    // Загрузка файла
    public function upload(){
        $check_status = $this->checkAsFile();
        if ( $check_status == 'ok' ){

            $file_path = (static::TEMP_UPLOAD_PATH).($this->new_file_name);
            $arFile = \CFile::MakeFileArray(($this->doc_root).($file_path));
            global $USER;
            $user = new \CUser;
            $fields = array(
                'UF_DELIV_FILE_TITLE' => strip_tags($_POST['deliveryFileTitle']),
                'UF_DELIV_INFO_FILE' => $arFile
            );
            $res = $user->Update($USER->GetID(), $fields);
            if( $res ){

                ob_start();
                    global $APPLICATION;
                    $APPLICATION->IncludeComponent(
                        "aoptima:personalDeliveryLocations", ""
                    );
                    $html = ob_get_contents();
                ob_end_clean();

                $html = base64_encode($html);

                // Ответ
                $this->onResponse("{'status':'ok', 'html':'" . $html . "'}");
            } else {
                // Ответ
                $this->onResponse("{'status':'Ошибка сохранения профиля'}");
            }

            // Удаляем временные файлы
            unlink( $this->file_path );

        } else {
            // Ответ
            $this->onResponse("{'status':'".$check_status."'}");
        }
    }







}