<?php

namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;


class address {

    const DADATA_API_URL = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address';
    const DADATA_API_KEY = '275927bfc60c502f5867aa03fe949656f0fa207c';

    

    // Получение подсказок адресов
    static function getSuggestions( $term, $count = 10 ){
        $results = [];
        $client = new Client(array( 'timeout' => 5 ));
        try {
            $response = $client->request(
                'POST',
                static::DADATA_API_URL,
                array(
                    // при необходимости
                    'headers' => array(
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        'Authorization' => 'Token '.static::DADATA_API_KEY
                    ),
                    'body' => json_encode(['query' => $term, 'count' => $count])
                )
            );
            if( $response->getStatusCode() == 200 ){
                $body = $response->getBody();
                $content = $body->getContents();
                $results = tools\funcs::json_to_array($content);
            } else {
                tools\logger::addError('dadata.ru - код ответа '.$response->getStatusCode());
            }
        } catch(RequestException $ex){
            tools\logger::addError('dadata.ru - '.$ex->getMessage());
        }
        return $results;
    }
    
    
    
    
    
    
    
    

}