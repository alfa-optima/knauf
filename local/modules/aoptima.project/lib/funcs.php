<? namespace AOptima\Project;
use AOptima\Project as project; 

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;  



class funcs {


    static function isDealer(){
        global $USER;
        if( $USER->IsAuthorized() ){
            $user = new project\user();
            return $user->isDealer();
        }
        return false;
    }


    static function isCityLandingPage(){
        $pureURL = tools\funcs::pureURL();
        return preg_match("/\/lampa_[a-z-_]+\//", $pureURL, $matches, PREG_OFFSET_CAPTURE);
    }


    static function isContentPage(){
        $stopArray = array(
            'catalog', 'faq', 'search', 'personal',
            'password_update',
            'basket', 'dealers', 'selling_points', 'order', 'promo-sapfir', 'promo-list'
        );

        $seo_page = project\SeopageTable::getByCode(tools\funcs::arURI()[1]);
        return (
            !tools\funcs::isMain()
            &&
            !in_array( tools\funcs::arURI()[1], $stopArray )
            &&
            !in_array( tools\funcs::arURI()[2], $stopArray )
            &&
            !project\funcs::isCityLandingPage()
            &&
            !$seo_page
            &&
            ERROR_404 != 'Y'
        );
    }
}