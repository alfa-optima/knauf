<?php
namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('highloadblock');



class vote {

    const HIBLOCK_ID = 2;

    function __construct(){
        $this->hlblock_id = static::HIBLOCK_ID;
        $this->hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($this->hlblock_id)->fetch();
        $this->entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($this->hlblock);
        $this->entity_data_class = $this->entity->getDataClass();
        $this->entity_table_name = $this->hlblock['TABLE_NAME'];
        $this->sTableID = 'tbl_'.$this->entity_table_name;
    }



    // add
    public function add( $tov_id = false, $user_id = false, $vote = false ){
        $arData = Array(
            'UF_USER' => $user_id,
            'UF_TOVAR' => $tov_id,
            'UF_VOTE' => $vote,
            'UF_DATE' => date('d.m.Y H:i:s'),
        );
        $result = $this->entity_data_class::add($arData);
        if ($result->isSuccess()) {
            return true;
        } else {
            tools\logger::addError('Ошибка добавления оценки товара - '.implode(', ', $result->getErrors()));
        }
        return false;
    }



    // изменение оценки
    public function reVote( $vote_id, $vote ){
        $arNewData = array('UF_VOTE' => $vote);
        $result = $this->entity_data_class::update($vote_id, $arNewData);
        if ( $result->isSuccess() ){
            return true;
        } else {
            tools\logger::addError('Ошибка изменения оценки товара - '.implode(', ', $result->getErrors()));
        }
        return false;
    }



    // getList
    public function getList( $tovar_id = false, $user_id = false, $limit = false ){
        $elements = array();
        $arFilter = array();
        if( intval($tovar_id) > 0 ){    $arFilter['UF_TOVAR'] = $tovar_id;      }
        if( intval($user_id) > 0 ){     $arFilter['UF_USER'] = $user_id;        }
        $arSelect = array('*');
        $arOrder = array("ID" => "DESC");
        $arInfo = array(
            "select" => $arSelect,
            "filter" => $arFilter,
            "order" => $arOrder
        );
        if( intval($limit) > 0 ){   $arInfo['limit'] = intval($limit);   }
        $rsData =  $this->entity_data_class::getList( $arInfo );
        $rsData = new \CDBResult($rsData,  $this->sTableID);
        while($element = $rsData->Fetch()){
            $elements[] = $element;
        }
        return $elements;
    }





    // подсчёт рейтинга
    static function getRating( $tov_id ){
        $rating = 0;
        $votes_sum = 0;
        $vote = new static();
        $user_votes = $vote->getList( $tov_id, false );
        $votes_cnt = count($user_votes);
        foreach( $user_votes as $user_vote ){
            $votes_sum += $user_vote['UF_VOTE'];
        }
        if( $votes_cnt > 0 ){
            $rating = round($votes_sum/$votes_cnt, 0);
        }
        return $rating;
    }





}