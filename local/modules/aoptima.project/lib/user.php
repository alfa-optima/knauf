<?php
namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;



class user {

    const DEALERS_GROUP_ID = 6;
    const CALL_CENTER_GROUP_ID = 9;


    protected $listFilter = Array(
        "ACTIVE" => "Y"
    );
    protected $fields = Array(
        'FIELDS' => array(
            'ID',
            'EMAIL'
            //'LOGIN',
        ),
        'SELECT' => array(
            'UF_RATING',
            'UF_COMPANY_NAME',
            'UF_FULL_COMPANY_NAME',
            'UF_PSS',
            'UF_PAY_TYPES',
            'UF_SHOW_IN_CATALOG',
            'UF_EMAIL_FOR_ORDERS',
            'UF_POST_ADDRESS',
            'UF_OGRN',
            'UF_INN',
            'UF_SITE',
            'UF_PHONES'
        )
    );




    function __construct( $user_id = false ){
        global $USER;
        if( !$user_id && $USER->IsAuthorized() ){
            $user_id = $USER->GetID();
        }
        if( intval($user_id) > 0 ){
            $arUser = tools\user::info($user_id);
            if( intval($arUser['ID']) > 0 ){
                $this->arUser = $arUser;
            }
        }
    }


    // Это дилер ДА/НЕТ
    public function isDealer(){
        if( is_array($this->arUser) ){
            $user_groups = \CUser::GetUserGroup($this->arUser['ID']);
            return in_array(static::DEALERS_GROUP_ID,  $user_groups);
        }
        return false;
    }




    // Список пользователей
    public function getList(
        $stop_ids = [],
        $limit = 'N',
        $sortField = 'ID',
        $sortOrder = 'DESC',
        $onlyAccepted = false
    ){
        $list = array();

        if( $limit != 'N' ){
            $this->fields['NAV_PARAMS']['nTopCount'] = $limit;
        }

        if( is_array($stop_ids) && count($stop_ids) > 0 ){
            $this->listFilter['ID'] = '~'.implode('&~', $stop_ids);
        }
        if( $onlyAccepted ){
            $this->listFilter['UF_SHOW_IN_CATALOG'] = 1;
        }

        $rsUsers = \CUser::GetList(
            $sortField, $sortOrder,
            $this->listFilter,
            $this->fields
        );
        while ($arUser = $rsUsers->GetNext()){
            $list[$arUser['ID']] = $arUser;
        }
        return $list;
    }





    static function getByLogin ( $email ){
        $fields = Array( 'FIELDS' => array( 'ID', 'EMAIL', 'LOGIN', 'EXTERNAL_AUTH_ID' )  );
        $filter = Array( "LOGIN_EQUAL" => $email, "!EXTERNAL_AUTH_ID" => "socservices" );
        $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter, $fields);
        while ($user = $rsUsers->GetNext()){     return $user;     }
        return false;
    }


    static function getByEmail ( $email ){
        $fields = Array( 'FIELDS' => array( 'ID', 'EMAIL', 'LOGIN', 'EXTERNAL_AUTH_ID' )  );
        $filter = Array( "EMAIL" => $email, "!EXTERNAL_AUTH_ID" => "socservices" );
        $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter, $fields);
        while ($user = $rsUsers->GetNext()){     return $user;     }
        return false;
    }





}