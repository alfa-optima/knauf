<?php
namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

use \Bitrix\Main\Application;
use \Bitrix\Main\Service\GeoIp;

require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;



class geolocation {

    const IPGEOBASE_CACHE_TIME = 86400;  // 1 сутки
    const SYPEXGEO_CACHE_TIME = 86400;  // 1 сутки
    const CLEAR_OLD_CACHE_TIME = 1800; // полчаса



    static function getCity(){
        $locInfo = static::getInfo();
        $city = $locInfo['city']?$locInfo['city']:'Москва';
        return $city;
    }
    static function getLoc( $city = false ){
        if( !$city ){
            $city = static::getCity();
        }
        $loc = project\bx_location::getByName( $city );
        if( intval($loc['ID']) > 0 ){
            return $loc;
        }
        return false;
    }
    static function getRegion(){
        $locInfo = static::getInfo();
        $region = $locInfo['region']?$locInfo['region']:'N';
        return $region;
    }
    static function getCountry(){
        $locInfo = static::getInfo();
        $country = $locInfo['country']?$locInfo['country']:'N';
        return $country;
    }


    static function getInfo($ip = false){
        if( !$ip ){  $ip = GeoIp\Manager::getRealIp();  }
        //$ip = '62.105.129.44';
        //$info = static::ipGeoBaseInfo($ip);
        $info = static::sypexGeoInfo($ip);
        return $info;
    }



    // ipGeoBase
    function ipGeoBaseInfo($ip){
        if( !$ip ){  $ip = GeoIp\Manager::getRealIp();  }
        $info = array(
            "shirota" => false, "dolgota" => false,
            'country' => false, 'region' => false, 'city' => false,
        );
        // Кеш
        $obCache = new \CPHPCache();
        $cache_time = static::IPGEOBASE_CACHE_TIME;
        $cache_id = 'ipGeoBaseInfo_'.$ip;
        $cache_path = '/ipGeoBaseInfo/'.$ip.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()) {
            try {
                $client = new \GuzzleHttp\Client([ 'timeout'  => 5 ]);
                $res = $client->request('GET', 'http://ipgeobase.ru:7020/geo?ip='.$ip);
                $body = $res->getBody();
                $xml_content = mb_convert_encoding($body, "UTF-8", "windows-1251");
                $xml = new \CDataXML();
                $xml->LoadString($xml_content);
                if ($node = $xml->SelectNodes('/ip-answer/ip')){
                    $ip_answer = $node->children();
                    foreach ($ip_answer as $f){
                        if ( $f->name == 'lat' ){       $info['shirota'] = $f->content;     }
                        if ( $f->name == 'lng' ){       $info['dolgota'] = $f->content;     }
                        if ( $f->name == 'country' ){   $info['country'] = $f->content;     }
                        if ( $f->name == 'region' ){    $info['region'] = $f->content;      }
                        if ( $f->name == 'city' ){      $info['city'] = $f->content;        }
                    }
                }
            } catch(RequestException $ex){
                tools\logger::addError($ex->getMessage());
            }
        $obCache->EndDataCache(array('info' => $info));
        }
        return $info;
    }



    function sypexGeoInfo($ip){
        if( !$ip ){  $ip = GeoIp\Manager::getRealIp();  }
        $info = [
            'shirota' => false, 'dolgota' => false,
            'country' => false, 'region' => false,
            'city' => false
        ];
        // Кеш
        $obCache = new \CPHPCache();
        $cache_time = static::SYPEXGEO_CACHE_TIME;
        $cache_id = 'sypexGeoInfo_'.$ip;
        $cache_path = '/sypexGeoInfo/'.$ip.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
             $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()) {
            $result = GeoIp\Manager::getDataResult($ip, "ru");
            if( isset($result) ){
                $data = $result->getGeoData();
                if ($data->latitude){        $info['shirota'] = $data->latitude;      }
                if ($data->longitude){       $info['dolgota'] = $data->longitude;     }
                if ($data->cityName){        $info['city'] = $data->cityName;         }
                if ($data->regionName){      $info['region'] = $data->regionName;     }
            }
        $obCache->EndDataCache(array('info' => $info));
        }
        return $info;
    }



    static function clearOldCaches(){
        $obCache = new \CPHPCache();
        $cache_time = static::CLEAR_OLD_CACHE_TIME;
        $cache_id = 'clearOldCaches';
        $cache_path = '/clearOldCaches/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()) {
            // BXClearCache(false, "/ipGeoBaseInfo/");
            // BXClearCache(false, "/sypexGeoInfo/");
            BXClearCache(false, "/");
         $obCache->EndDataCache(array('clearOldCaches' => true));
        }
        return "\AOptima\Project\geolocation::clearOldCaches();";
    }



}