<?php
namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('highloadblock');


class review {

    const HIBLOCK_ID = 1;
    const DETAIL_LIST_CNT = 10;



    function __construct(){
        $this->hlblock_id = static::HIBLOCK_ID;
        $this->hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($this->hlblock_id)->fetch();
        $this->entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($this->hlblock);
        $this->entity_data_class = $this->entity->getDataClass();
        $this->entity_table_name = $this->hlblock['TABLE_NAME'];
        $this->sTableID = 'tbl_'.$this->entity_table_name;
    }



    // add
    public function add(
        $hash_id,
        $tov_id,
        $name,
        $text,
        $user_vote = false,
        $date = null
    ){
        global $USER;
        //$user_id = intval($USER->GetID())>0?intval($USER->GetID()):null;
        if( !isset($date) ){    $date = date('d.m.Y H:i:s');    }
        $arData = [
            'UF_HASH_ID' => $hash_id,
            'UF_NAME' => strip_tags($name),
            //'UF_USER' => $user_id,
            'UF_TOVAR' => $tov_id,
            'UF_TEXT' => strip_tags($text),
            'UF_DATE' => $date,
        ];
        if( $user_vote >= 1 && $user_vote <= 5 ){
            $arData['UF_USER_VOTE'] = intval($user_vote);
        }
        if( $USER->IsAdmin() ){    $arData['UF_ACTIVE'] = 1;    }
        $result = $this->entity_data_class::add($arData);
        if ($result->isSuccess()) {
            return true;
        } else {
            tools\logger::addError('Ошибка добавления отзыва о товаре - '.implode(', ', $result->getErrors()));
        }
        return false;
    }


    // update
    public function update( $hash_id, $id, $tov_id, $name, $text, $user_vote = false, $date = null ){
        global $USER;
        //$user_id = intval($USER->GetID())>0?intval($USER->GetID()):null;
        if( !isset($date) ){    $date = date('d.m.Y H:i:s');    }
        $arData = [
            'UF_HASH_ID' => $hash_id,
            'UF_NAME' => strip_tags($name),
            //'UF_USER' => $user_id,
            'UF_TOVAR' => $tov_id,
            'UF_TEXT' => strip_tags($text),
            'UF_DATE' => $date,
        ];
        if( $user_vote >= 1 && $user_vote <= 5 ){
            $arData['UF_USER_VOTE'] = intval($user_vote);
        }
        if( $USER->IsAdmin() ){    $arData['UF_ACTIVE'] = 1;    }
        $result = $this->entity_data_class::update($id, $arData);
        if ($result->isSuccess()) {
            return true;
        } else {
            tools\logger::addError('Ошибка изменения отзыва ['.$id.'] о товаре - '.implode(', ', $result->getErrors()));
        }
        return false;
    }




    // getList
    public function getList(
        $tovar_id = false,
        $user_id = false,
        $limit = false,
        $stop_items = false
    ){
        $elements = array();
        $arFilter = array('UF_ACTIVE' => 1);
        if( intval($tovar_id) > 0 ){    $arFilter['UF_TOVAR'] = $tovar_id;      }
        //if( intval($user_id) > 0 ){     $arFilter['UF_USER'] = $user_id;        }
        if( is_array($stop_items) ){
            $arFilter['!ID'] = $stop_items;
        }
        $arSelect = array('*');
        $arOrder = array("UF_DATE" => "DESC");
        $arInfo = array(
            "select" => $arSelect,
            "filter" => $arFilter,
            "order" => $arOrder
        );
        if( intval($limit) > 0 ){   $arInfo['limit'] = intval($limit);   }
        $rsData =  $this->entity_data_class::getList( $arInfo );
        $rsData = new \CDBResult($rsData,  $this->sTableID);
        while($element = $rsData->Fetch()){
            $elements[] = $element;
        }
        return $elements;
    }



    // getByHashID
    public function getByHashID( $hashID ){
        $arFilter = [
            'UF_ACTIVE' => 1,
            'UF_HASH_ID' => $hashID,
        ];
        $arSelect = ['*'];
        $arOrder = ["UF_DATE" => "DESC"];
        $arInfo = [
            "select" => $arSelect,
            "filter" => $arFilter,
            "order" => $arOrder,
            "limit" => 1,
        ];
        $rsData =  $this->entity_data_class::getList( $arInfo );
        $rsData = new \CDBResult($rsData,  $this->sTableID);
        if( $element = $rsData->Fetch() ){
            return $element;
        }
        return null;
    }



    static function importFromCSV(){

        $file_path = $_SERVER['DOCUMENT_ROOT'].'/reviews.csv';

        if( file_exists( $file_path ) ){

            $errors = [];

            $diff_columns = array();

            $lines = file($file_path);
            foreach ( $lines as $key => $value ){
                $value = explode(';', mb_convert_encoding($value, 'UTF-8', 'windows-1251'));
                $value = str_replace(array("\n", "\r"), "", $value);
                $diff_columns[count($value)]++;
                $lines[$key] = $value;
            }

            // Если в CSV имеются строки с разным количеством столбцов
            if( count($diff_columns) > 1 ){

                $errors[] = 'Ошибка формата: в CSV имеются строки с разным количеством столбцов';

            } else if( count($diff_columns) == 1 ){

                $titlesLine = $lines[0];
                foreach ( $titlesLine as $key => $title ){
                    $titlesLine[$key] = trim(strtoupper($title));
                }
                unset($lines[0]);
                $total_lines_cnt = count($lines);

                // Отсутствие заголовков
                if(
                    !in_array('ARTICLE', $titlesLine)
                    ||
                    !in_array('NAME', $titlesLine)
                    ||
                    !in_array('TEXT', $titlesLine)
                    ||
                    !in_array('DATE', $titlesLine)
                ){

                    $errors[] = 'Ошибка формата: первая строка не содержит обязательных заголовков (ARTICLE, NAME, TEXT, DATE)';

                } else {

                    $article_key = array_search('ARTICLE', $titlesLine);
                    $name_key = array_search('NAME', $titlesLine);
                    $text_key = array_search('TEXT', $titlesLine);
                    $date_key = array_search('DATE', $titlesLine);
                    $vote_key = array_search('VOTE', $titlesLine);

                    if( count($lines) > 0 ){

                        // Отсеваем некомплектные строки
                        foreach ( $lines as $line_number => $line ){

                            $article = is_integer($article_key)?trim(strip_tags($line[$article_key])):null;
                            $name = is_integer($name_key)?trim(strip_tags($line[$name_key])):null;
                            $text = is_integer($text_key)?trim(strip_tags($line[$text_key])):null;
                            $date = is_integer($date_key)?trim(strip_tags($line[$date_key])):null;
                            $vote = is_integer($vote_key)?trim(strip_tags($line[$vote_key])):null;

                            if(
                                strlen($article) > 0
                                &&
                                strlen( $name ) > 0
                                &&
                                strlen( $text ) > 0
                            ){} else {
                                unset($lines[$line_number]);
                            }
                        }

                        // Перебираем оставшиеся строки
                        foreach ( $lines as $line_number => $line ){

                            $article = is_integer($article_key)?trim(strip_tags($line[$article_key])):null;
                            $name = is_integer($name_key)?trim(strip_tags($line[$name_key])):null;
                            $text = is_integer($text_key)?trim(strip_tags($line[$text_key])):null;
                            $date = is_integer($date_key)?trim(strip_tags($line[$date_key])):null;
                            $vote = is_integer($vote_key)?trim(strip_tags($line[$vote_key])):null;

                            // Найдём товар с таким артикулом
                            $product = project\catalog::getProductByArticle( $article );

                            // Если найден товар с таким артикулом
                            if( intval( $product['ID'] ) > 0 ){

                                // Сформируем уникальный хэш отзыва
                                $review_hash_id = md5($article.$name.$text.$date.$vote);

                                $review = new static();

                                // Если отзыв с таким хэшем УЖЕ ЕСТЬ
                                $arReview = $review->getByHashID( $review_hash_id );
                                if( isset( $arReview ) ){

                                    // Изменим его
                                    $res = $review->update(
                                        $review_hash_id,
                                        $arReview['ID'],
                                        $product['ID'],
                                        $name,
                                        $text,
                                        $vote,
                                        $date
                                    );
                                    if( !$res ){
                                        $errors[] = 'Не удалось изменить отзыв к товару с артикулом "'.$article.'" - строка '.($line_number+1);
                                    }

                                // Если отзыва с таким хэшем ЕЩЁ НЕТ
                                } else {

                                    // Создадим его
                                    $res = $review->add(
                                        $review_hash_id,
                                        $product['ID'],
                                        $name,
                                        $text,
                                        $vote,
                                        $date
                                    );
                                    if( !$res ){
                                        $errors[] = 'Не удалось создать отзыв к товару с артикулом "'.$article.'" - строка '.($line_number+1);
                                    }
                                }

                            } else {
                                $errors[] = 'Товар с артикулом "'.$article.'" не найден - строка '.($line_number+1);
                            }
                        }
                    }
                }
            }

            if( count($errors) > 0 ){
                echo implode("<br>", $errors);
            }
        }
    }



}