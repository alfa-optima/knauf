<? use Bitrix\Main,  Bitrix\Main\Loader;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

$CURRENCY_RIGHT = $APPLICATION->GetGroupRight("aoptima.tools");

if ($CURRENCY_RIGHT=="D") $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$APPLICATION->SetTitle('Aoptima.tools - поля для инфоблоков');


// Получим все инфоблоки
$all_iblocks = array();
\Bitrix\Main\Loader::includeModule('iblock');
$res = \CIBlock::GetList( Array('ID' => 'ASC'), Array(), true );
while($iblock = $res->Fetch()){
	$all_iblocks[] = $iblock;
}

if( count($all_iblocks) > 0 ){

	$module_path = tools\module::getPath('aoptima.tools');

	if( $module_path ){
		
		$file_path = $module_path.'install/module_settings/iblock_fields.json';
		
		// Если файла нет
		if( !file_exists($file_path) ){
			tools\el::createSettingsFile($file_path);
		}
		
		if( file_exists($file_path) ){
		
			$json = file_get_contents($file_path);
			$fields = tools\funcs::json_to_array($json);

			$to_save = false;
			foreach ( $all_iblocks as $iblock ){
				if( !$fields[$iblock['ID']] ){
					$to_save = true;
					$fields[$iblock['ID']] = array(
						'EL_FIELDS' => array(
							'ID', 'ACTIVE', 'NAME', 'CODE', 'XML_ID',
							'DETAIL_PAGE_URL', 'IBLOCK_EXTERNAL_ID'
						),
						'EL_PROPS' => array(),
						'SECT_FIELDS' => array()
					);
				}
			}
			if( $to_save ){
				// сохраним файл настроек
				$f = fopen($file_path, "w"); 
				$result = fwrite( $f,  json_encode($fields) );
				fclose($f);
			}

			
			if( count($_POST) > 0 ){
				foreach($_POST as $type => $iblocks){
					if( count($iblocks) > 0 ){
						foreach($iblocks as $iblock_id => $items){
							if( count($items) > 0 ){
								foreach($items as $key => $value){
									if( $value == null ){
										unset($_POST[$type][$iblock_id][$key]);
									}
								}
							}
						}
					}
				}
				
				$fields = $_POST;
				
				// сохраним файл настроек
				$f = fopen($file_path, "w"); 
				$result = fwrite( $f,  json_encode($fields) );
				fclose($f);
				
				BXClearCache(true, "/".tools\el::EL_CACHE_NAME."/");
				BXClearCache(true, "/".tools\el::EL_CACHE_NAME."_by_code/");
				BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."/");
				BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."_by_code/");
				
			} ?>

			
			<style>
				.admin_iblock_fields_table {
					border-collapse: collapse;
					margin: 0 0 15px 0;
				}
				.admin_iblock_fields_table th, .admin_iblock_fields_table td{
					padding: 12px 12px;
					border: 1px solid #bdb8b8;
					vertical-align:top;
				}
				.first_td {
					text-align:left;
				}
				.buttons_td {
					text-align:right;
				}
				.admin_iblock_fields_plus {
					margin-top:15px
				}
			</style>


			<form method="post">

				<table class="admin_iblock_fields_table">
				
					<tr>
						<th class="first_td">Инфоблок</th>
						<th>Поля (элементы)</th>
						<th>Свойства (элементы)</th>
						<th>Поля (разделы)</th>
					</tr>

					<? foreach ( $all_iblocks as $iblock ){ ?>
					
						<tr class="iblock_tr" iblock_id="<?=$iblock['ID']?>">
							<td class="first_td"><b><?=$iblock['NAME']?> [<?=$iblock['ID']?>]</b></td>
							<td>
								<div class="inputs_block">
									<? if( 
										$fields[$iblock['ID']]['EL_FIELDS']
										&&
										is_array($fields[$iblock['ID']]['EL_FIELDS'])
										&&
										count($fields[$iblock['ID']]['EL_FIELDS']) > 0
									){ 
										foreach ( $fields[$iblock['ID']]['EL_FIELDS'] as $item ){ ?>
											<input type="text" name="<?=$iblock['ID']?>[EL_FIELDS][]" value="<?=$item?>">
											<br>
										<? }
									} ?>
									<input  type="text" name="<?=$iblock['ID']?>[EL_FIELDS][]">
								</div>
								<div class="clear___both" style="clear:both"></div>
								<input class="admin_iblock_fields_plus" item_name="<?=$iblock['ID']?>[EL_FIELDS][]" type="button" value="+">
							</td>
							<td>
								<div class="inputs_block">
									<? if( 
										$fields[$iblock['ID']]['EL_PROPS']
										&&
										is_array($fields[$iblock['ID']]['EL_PROPS'])
										&&
										count($fields[$iblock['ID']]['EL_PROPS']) > 0
									){ 
										foreach ( $fields[$iblock['ID']]['EL_PROPS'] as $item ){ ?>
											<input type="text" name="<?=$iblock['ID']?>[EL_PROPS][]" value="<?=$item?>">
											<br>
										<? }
									} ?>
									<input  type="text" name="<?=$iblock['ID']?>[EL_PROPS][]">
								</div>
								<div class="clear___both" style="clear:both"></div>
								<input class="admin_iblock_fields_plus" item_name="<?=$iblock['ID']?>[EL_PROPS][]" type="button" value="+">
							</td>
							<td>
								<div class="inputs_block">
									<? if( 
										$fields[$iblock['ID']]['SECT_FIELDS']
										&&
										is_array($fields[$iblock['ID']]['SECT_FIELDS'])
										&&
										count($fields[$iblock['ID']]['SECT_FIELDS']) > 0
									){ 
										foreach ( $fields[$iblock['ID']]['SECT_FIELDS'] as $item ){ ?>
											<input type="text" name="<?=$iblock['ID']?>[SECT_FIELDS][]" value="<?=$item?>">
											<br>
										<? }
									} ?>
									<input  type="text" name="<?=$iblock['ID']?>[SECT_FIELDS][]">
								</div>
								<div class="clear___both" style="clear:both"></div>
								<input class="admin_iblock_fields_plus" item_name="<?=$iblock['ID']?>[SECT_FIELDS][]" type="button" value="+">
							</td>
						</tr>
					
					<? } ?>
					
					<tr>
						<td colspan="4" class="buttons_td">
							<input class="admin_iblock_fields_save adm-btn-save" type="submit" value="Сохранить">
						</td>
					</tr>
				
				</table>
				
			</form>
			
			
			
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
			
			<script type="text/javascript">
			$(document).ready(function(){
				
				$(document).on('click', '.admin_iblock_fields_plus', function(){

					var item_name = $(this).attr('item_name');
					$(this).parent('td').find('.inputs_block').append('<br><input type="text" name="'+item_name+'">');
					
				})
				
			});
			</script>

			
			
		<? }
	
	}
	
	
} else {
	
	echo BeginNote();
	echo "<b>Инфоблоков нет!!!</b>";
	echo EndNote();
	
}





require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");