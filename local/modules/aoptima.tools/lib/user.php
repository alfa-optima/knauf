<? namespace AOptima\Tools;
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;



class user {
	

	
	const USER_CACHE_NAME = 'user';
	
	
	
	
	// Инфо о пользователе
	static function info($user_id){
		$arUser = false;
		if( intval($user_id) > 0 ){
			// Кешируем 
			$obCache = new \CPHPCache();
			$cache_time = 30*24*60*60;
			$cache_id = static::USER_CACHE_NAME.'_'.$user_id;
			$cache_path = '/'.static::USER_CACHE_NAME.'/'.$user_id.'/';
			if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
				$vars = $obCache->GetVars();   extract($vars);
			} elseif($obCache->StartDataCache()){
				// Инфо о пользователе
				$dbUser = \CUser::GetByID($user_id);   $arUser = $dbUser->GetNext();
				if ( intval($arUser['ID']) > 0 ){    
					$arUser = tools\funcs::clean_array($arUser);
				}
			$obCache->EndDataCache(array('arUser' => $arUser));
			}
		}
		return $arUser;
	}
	

	
	

	
	// Авторизация
	static function auth(){
        global $USER;
        if( !$USER->IsAuthorized() ){

            $login = tools\funcs::param_post("login");
            $password = strip_tags(tools\funcs::param_post("password"));

            $result = $USER->Login($login, $password, "Y");

            if (!is_array($result)){
                $USER->Authorize($USER->GetID());

                // Инфо о пользователе
                $user = static::info($USER->GetID());
                if(
                    !$user['UF_REG_CONFIRMED']
                    &&
                    !$USER->IsAdmin()
                ){
                    $USER->Logout();
                    // Ответ
                    echo json_encode( Array(
                        "status" => "error",
                        "text" => "Профиль ещё не подтверждён"
                    ) );
                    return;
                }

                // Ответ
                echo json_encode( Array( "status" => "ok" ) ); return;
            } else {
                // Ответ
                echo json_encode( Array( "status" => "error", "text" => "Неверный логин или пароль" ) ); return;
            }
        } else {
            // Ответ
            echo json_encode( Array( "status" => "auth", "text" => "Вы уже авторизованы" ) );
        }
	}
	
	
	
	
	// Регистрация
	/*static function register(){
        global $USER;
        if( !$USER->IsAuthorized() ){
            // данные в массив
            $arFields = Array();
            parse_str(param_post("form_data"), $arFields);
            // проверяем EMAIL на логин
            $filter = Array( "LOGIN" => strip_tags($arFields["EMAIL"]) );
            $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter);
            while ($user = $rsUsers->GetNext()){
                // Если Email уже есть
                echo json_encode( Array( "status" => "error", "text" => "Данный логин уже зарегистрирован на&nbsp;сайте!" ) );  return;
            }
            // проверяем EMAIL на наличие у других пользователей
            $filter = Array( "EMAIL" => strip_tags($arFields["EMAIL"]) );
            $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter);
            while ($user = $rsUsers->GetNext()){
                // Если Email уже есть
                echo json_encode( Array( "status" => "error", "text" => "Данный Email уже зарегистрирован на&nbsp;сайте!" ) );  return;
            }
            // Регистрируем пользователя
            $user = new \CUser;
            $arUserFields = Array(
                "ACTIVE" => "Y",
                "LOGIN" => strip_tags($arFields["EMAIL"]),
                "EMAIL" => strip_tags($arFields["EMAIL"]),
                "NAME" => strip_tags($arFields["NAME"]),
                "PASSWORD" => strip_tags($arFields["PASSWORD"]),
                "CONFIRM_PASSWORD" => strip_tags($arFields["CONFIRM_PASSWORD"]),
                "GROUP_ID" => Array(6)
            );
            $userID = $user->Add($arUserFields);
            if ($userID){
                static::sendRegInfo($userID);
                // Ответ
                echo json_encode( array("status" => 'ok') );
            } else {
                // Ответ
                echo json_encode( array("status" => 'error', "text" => $user->LAST_ERROR) );
            }
        } else {
            // Ответ
            echo json_encode( Array( "status" => "auth", "text" => "Вы уже авторизованы" ) );
        }
	}*/
	
	
	
	
	// Регистрационная информация на почту
	static function sendRegInfo($user_id, $password = false, $title = false){
		if( intval($user_id) > 0 ){
			$user = static::info($user_id);
			if( intval($user['ID']) > 0 ){

			    $server_name = \Bitrix\Main\Config\Option::get('main', 'server_name');

$content_html = '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Ваша регистрационная информация для сайта "'.$server_name.'":</p>';
$content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">ID пользователя: '.$user["ID"].'</p>';
$content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Профиль подтверждён: '.($user["UF_REG_CONFIRMED"]?"Да":"<u>Нет</u>").'</p>';
$content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Фамилия: '.$user["LAST_NAME"].'</p>';
$content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Имя: '.$user["NAME"].'</p>';
$content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">E-Mail: '.$user["EMAIL"].'</p>';
if( $password ){
    $content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Пароль: '.$password.'</p>';
    $content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Сменить пароль вы можете в вашем <a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$server_name.'/personal/">личном кабинете</a>.</p>';
}

$mail_fields = Array(
    "TITLE" => $title?$title:'Регистрационная информация (сайт "'.$server_name.'")',
    "HTML" => project\mail::header().$content_html.project\mail::footer(),
    "EMAIL_TO" => $user["EMAIL"]
);
\CEvent::SendImmediate("MAIN", "s1", $mail_fields);


			}
		}
	}



    // Отправка ссылки для подтверждения регистрации
    static function sendRegConfirmLink( $user_id ){
        if( intval($user_id) > 0 ){
            $user = static::info($user_id);
            if(
                intval($user['ID']) > 0
                &&
                strlen($user['UF_REG_CODE']) > 0
            ){

                $link = $_SERVER['REQUEST_SCHEME'].'://'.\Bitrix\Main\Config\Option::get('main', 'server_name').'/register_confirm/?code='.$user['UF_REG_CODE'];

$content_html = '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Ссылка для подтверждения регистрации на сайте "'.\Bitrix\Main\Config\Option::get('main', 'server_name').'":</p>';
$content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;"><a href="'.$link.'" style="Margin: 0; color: #009fe3; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left; text-decoration: none;">'.$link.'</a></p>';

$mail_fields = Array(
    "TITLE" => 'Ссылка для подтверждения регистрации (сайт "'.\Bitrix\Main\Config\Option::get('main', 'server_name').'")',
    "HTML" => project\mail::header().$content_html.project\mail::footer(),
    "EMAIL_TO" => $user["EMAIL"]
);
\CEvent::SendImmediate("MAIN", "s1", $mail_fields);



            }
        }
    }


	
	// Сверка пароля с пользовательским
	static function comparePassword($userId, $password){
		if( intval($userId) > 0 && strlen($password) > 0 ){
			$userData = static::info($userId);
			$salt = substr($userData['PASSWORD'], 0, (strlen($userData['PASSWORD']) - 32));
			$realPassword = substr($userData['PASSWORD'], -32);
			$password = md5($salt.$password);
			return ($password == $realPassword);
		}
		return false;
	}
	
	
	
	
}



