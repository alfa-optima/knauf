<?

$MESS["AOPTIMA_TOOLS_MODULE_NAME"] = "Aoptima tools";
$MESS["AOPTIMA_TOOLS_MODULE_DESC"] = "Aoptima tools";
$MESS["AOPTIMA_TOOLS_PARTNER_NAME"] = "Aoptima";
$MESS["AOPTIMA_TOOLS_PARTNER_URI"] = "http://alfa-optima.ru/";

$MESS["AOPTIMA_TOOLS_DENIED"] = "Доступ закрыт";
$MESS["AOPTIMA_TOOLS_READ_COMPONENT"] = "Доступ к компонентам";
$MESS["AOPTIMA_TOOLS_WRITE_SETTINGS"] = "Изменение настроек модуля";
$MESS["AOPTIMA_TOOLS_FULL"] = "Полный доступ";

$MESS["AOPTIMA_TOOLS_INSTALL_TITLE"] = "Установка модуля";
$MESS["AOPTIMA_TOOLS_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";

#работа с .settings.php
$MESS["AOPTIMA_TOOLS_INSTALL_COUNT"] = "Количество установок модуля: ";
$MESS["AOPTIMA_TOOLS_UNINSTALL_COUNT"] = "Количество удалений модуля: ";

$MESS["AOPTIMA_TOOLS_NO_CACHE"] = 'Внимание, на сайте выключено кеширование!<br>Возможно замедление в работе модуля.';
#работа с .settings.php
?>