<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('sale');

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arResult = $arParams['arResult'];

$dealer_pay_types = [];
if( strlen($arResult['DEALER']['UF_PAY_TYPES']) > 0 ){
    $dealer_pay_types = tools\funcs::json_to_array($arResult['DEALER']['UF_PAY_TYPES']);
}

// Варианты оплаты для дилера
$arResult['PAYMENTS'] = array();
if( count($arResult['DEALER']['UF_PSS']) > 0 ){
    $db_ptype = \CSalePaySystem::GetList(
        Array(
            "SORT"=>"ASC",
            "PSA_NAME"=>"ASC"
        ),
        Array(
            "ACTIVE"=>"Y",
            "CODE" => $arResult['DEALER']['UF_PSS']
        ),
        false,
        false,
        array('ID', 'NAME', 'CODE', 'PSA_LOGOTIP', 'DESCRIPTION')
    );
    while ($ps = $db_ptype->GetNext()){

        $ps['PAY_TYPES'] = [];

        if( is_array($dealer_pay_types[$ps['CODE']]) && count($dealer_pay_types[$ps['CODE']]) > 0 ){
            foreach ( $dealer_pay_types[$ps['CODE']] as $pay_type ){
                if( $pay_type == 'pre' ){   $ps['PAY_TYPES'][] = 'Предоплата';   }
                if( $pay_type == 'post' ){   $ps['PAY_TYPES'][] = 'Постоплата';   }
            }
        }

        $arResult['PAYMENTS'][$ps['ID']] = $ps;
    }
}







$this->IncludeComponentTemplate();