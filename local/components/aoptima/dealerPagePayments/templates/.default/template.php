<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>

<section>
    <div>
        <h4 class="tab__title">Оплата</h4>
        <div class="tab__inner">
            <div class="dealer__pay">

                <? foreach( $arResult['PAYMENTS'] as $key => $ps ){ ?>

                    <div class="dealer__pay-item">
                        <div>
                            <span><?=$ps['NAME']?> <? if( count($ps['PAY_TYPES']) > 0 ){ echo ' ('.implode(' / ', $ps['PAY_TYPES']).')'; } ?></span>
                        </div>
                        <? if( intval($ps['PSA_LOGOTIP']) > 0 ){ ?>
                            <div>
                                <img src="<?=\CFile::GetPath( $ps['PSA_LOGOTIP'] )?>">
                            </div>
                        <? } ?>
                    </div>

                <? } ?>

            </div>
        </div>
    </div>
</section>