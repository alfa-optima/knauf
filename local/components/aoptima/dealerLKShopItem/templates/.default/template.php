<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$shop_id = $arResult['SHOP_ID'];
$shop = $arResult['SHOP']; ?>


<div class="dealerAccount-form__block dealerLKShopItem <? if( $shop_id == 'new' ){ ?>emptyShopBlock<? } ?>" item_id="<?=$shop_id?>">

    <div class="dealerAccount-form__row">

        <div class="dealerAccount-form__row">

            <div class="dealerAccount-form__field  dealerAccount-form__field--w271">
                <span>Название точки продаж</span>
                <input class="name_input" type="text" name="shops[<?=$shop_id?>][name]" value="<?=$shop['NAME']?>">
            </div>

            <span class="account-form__deleteAddress removeDealerShop to___process">Удалить точку продаж</span>


        </div>

        <!--
        <div class="dealerAccount-form__field  dealerAccount-form__field--w271">
            <span>Область</span>
            <input
                class="account-form__suggestInput"
                id="dealerShopRegion_<?=$shop_id?>"
                type="text"
                placeholder="Начните ввод"
                loc_name=""
                autocomplete="off"
                onfocus="regionFocus($(this));"
                onkeyup="regionKeyUp($(this));"
                onfocusout="regionFocusOut($(this));"
                value="<?=$shop['REGION']['NAME_RU']?>"
            >
            <input class="region_input" type="hidden" name="shops[<?=$shop_id?>][region_id]" value="<?=$shop['PROPERTY_REGION_ID_VALUE']?>">
            <script>
                $(document).ready(function(){
                    $('#dealerShopRegion_<?=$shop_id?>').autocomplete({
                        source: '/ajax/searchRegion.php',
                        minLength: 2,
                        delay: 700,
                        select: regionSelect
                    })
                })
            </script>
        </div>
        -->
        
        <div class="dealerAccount-form__field  dealerAccount-form__field--w271">
            <span>Населённый пункт</span>
            <input
                class="account-form__suggestInput"
                id="dealerShopCity_<?=$shop_id?>"
                type="text"
                placeholder="Начните ввод"
                cityName=""
                autocomplete="off"
                onfocus="cityFocus($(this));"
                onkeyup="cityKeyUp($(this));"
                onfocusout="cityFocusOut($(this));"
                value="<? if( $shop_id != 'new' ){ ?><?=$shop['LOC']['NAME_RU']?> (<?=$shop['LOC']['PARENT_NAME_RU']?>)<? } ?>"
            >
            <input class="loc_input" type="hidden" name="shops[<?=$shop_id?>][loc_id]" value="<?=$shop['PROPERTY_LOC_ID_VALUE']?>">
            <script>
                $(document).ready(function(){
                    $('#dealerShopCity_<?=$shop_id?>').autocomplete({
                        source: '/ajax/searchCity.php',
                        minLength: 2,
                        delay: 700,
                        select: citySelect
                    })
                })
            </script>
        </div>

    </div>

    <div class="dealerAccount-form__row">

        <div class="dealerAccount-form__field  dealerAccount-form__field--w271">
            <span>Улица<br><small>Можно указать вместе с посёлком, деревней и т.п. (при необходимости)</small></span>
            <input class="street_input" type="text" name="shops[<?=$shop_id?>][street]" value="<?=$shop['PROPERTY_STREET_VALUE']?>">
        </div>

        <div class="dealerAccount-form__field  dealerAccount-form__field--w83">
            <span>Дом</span>
            <input class="house_input" type="text" name="shops[<?=$shop_id?>][house]" value="<?=$shop['PROPERTY_HOUSE_VALUE']?>">
        </div>

        <div class="dealerAccount-form__field  dealerAccount-form__field--w83">
            <span>Офис</span>
            <input class="office_input" type="text" name="shops[<?=$shop_id?>][office]" value="<?=$shop['PROPERTY_OFFICE_VALUE']?>">
        </div>

        <div class="dealerAccount-form__field  dealerAccount-form__field--w83">
            <span>Индекс</span>
            <input class="index_input" type="text" name="shops[<?=$shop_id?>][index]" value="<?=$shop['PROPERTY_INDEX_VALUE']?>">
        </div>

        <div class="dealerAccount-form__field  dealerAccount-form__field--w155">
            <span>GPS координаты<br><small>Пример: 55.75399399999,37.62209300000</small></span>
<!--            <a class="openDealerModalMapLink" onclick="openDealerModalMap( $(this).parents('div.dealerAccount-form__field').find('input') );">Указать на карте</a>-->
            <input class="gps_input" type="text" name="shops[<?=$shop_id?>][GPS]" value="<?=$shop['PROPERTY_GPS_VALUE']?>">
        </div>

    </div>

    <div class="dealerAccount-form__row">

        <div class="dealerAccount-form__field  dealerAccount-form__field--w271">
            <span>Эл. почта</span>
            <input class="email_input" type="text" name="shops[<?=$shop_id?>][email]" value="<?=$shop['PROPERTY_EMAIL_VALUE']?>">
        </div>

        <div class="dealerAccount-form__field  dealerAccount-form__field--w271">
            <span>Сайт</span>
            <input class="site_input" type="text" name="shops[<?=$shop_id?>][site]" value="<?=$shop['PROPERTY_SITE_VALUE']?>">
        </div>

    </div>

    <div class="dealerAccount-form__row">
        <? if( is_array($shop['PROPERTY_PHONES_VALUE']) ){
            foreach ( $shop['PROPERTY_PHONES_VALUE'] as $key => $phone ){
                if( strlen($phone) > 0 ){ ?>
                    <div class="dealerAccount-form__field">
                        <span>Телефон</span>
                        <input class="phone_input" type="tel" name="shops[<?=$shop_id?>][phones][]" value="<?=$phone?>">
                    </div>
                <? }
            }
        } ?>
        <div class="dealerAccount-form__field">
            <span>Телефон</span>
            <input class="phone_input" type="tel" name="shops[<?=$shop_id?>][phones][]">
        </div>
        <div class="dealerAccount-form__addTel"><span>Добавить телефон</span></div>
    </div>

    <div class="dealerAccount-form__row">
        <div class="account-form__field  account-form__field--wa">
            <div class="account-form__checkbox">
                <input type="checkbox" id="Pickup_<?=$shop_id?>" name="shops[<?=$shop_id?>][samovyvoz]" <? if( $shop['PROPERTY_SAMOVYVOZ_VALUE'] == 'Y' ){ echo 'checked'; } ?> value="Y">
                <label for="Pickup_<?=$shop_id?>">Самовывоз</label>
            </div>
        </div>
    </div>

    <div class="dealerAccount-form__block">
        <h4 class="tab__title">Режим работы</h4>
        <div class="dealerAccount-form__row">
            <div class="dealerAccount-form__field  dealerAccount-form__field--w583">
                <div class="account-form__textarea-counter">Оставшиеся символы: <span>300</span></div>
                <textarea name="shops[<?=$shop_id?>][rezhim_raboty]" maxlength="300" placeholder="Режим работы точки продаж..."><?=$shop['PROPERTY_REZHIM_RABOTY_VALUE']['TEXT']?></textarea>
            </div>
        </div>
    </div>

</div>

