<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


if( intval($arParams['SHOP_ID']) > 0 ){
    $arResult['SHOP_ID'] = $arParams['SHOP_ID'];
    $arResult['SHOP'] = $arParams['SHOP'];
} else {
    $arResult['SHOP_ID'] = 'new';
    $arResult['SHOP'] = array();
}


if( strlen($arResult['SHOP']['PROPERTY_PHONES_VALUE']['TEXT']) > 0 ){
    $arResult['SHOP']['PROPERTY_PHONES_VALUE'] = tools\funcs::json_to_array($arResult['SHOP']['PROPERTY_PHONES_VALUE']['TEXT']);
}



$arResult['TIME_LIST'] = array();
for ( $n = 0; $n <= 23; $n++ ){
    $arResult['TIME_LIST'][] = ($n>=10?'':'0').$n.':00';
}






$this->IncludeComponentTemplate();