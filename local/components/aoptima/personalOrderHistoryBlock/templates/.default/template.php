<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$maxCNT = $arResult['MAX_CNT'];

if( $arParams['IS_AJAX'] != 'Y' ){ ?>

    <section class="personalOrderHistoryBlock">
        <div>
            <h4 class="tab__title">История заказов</h4>

            <? if( count($arResult['ORDERS']) > 0 ){ ?>

                <table class="account-form__table">
                    <tbody class="personalOrderHistoryArea">

                    <tr>
                        <th>№ заказа</th>
                        <th>Стоимость</th>
                        <th>Дата</th>
                        <th>Дилер</th>
                        <th>Тип доставки</th>
                        <th>Статус</th>
                    </tr>


                    <? $cnt = 0;
                    foreach( $arResult['ORDERS'] as $order_id => $order ){ $cnt++;
                        if( $cnt <= $maxCNT ){

                            // orderListItem
                            $APPLICATION->IncludeComponent(
                                 "aoptima:orderListItem", "",
                                [
                                    'ORDER' => $order,
                                    'IS_MOB_APP' => $arParams['IS_MOB_APP']
                                ]
                            );

                        }
                    } ?>

                    </tbody>
                </table>

                <div class="more-button ordersListMoreButton to___process" <? if( $cnt <= $maxCNT ){ ?>style="display:none"<? } ?>>
                    <span>Показать еще</span>
                </div>

            <? } ?>

        </div>
    </section>


<? } else if( $arParams['IS_AJAX'] == 'Y' ){

    $cnt = 0;
    foreach( $arResult['ORDERS'] as $order_id => $order ){ $cnt++;
        if( $cnt <= $maxCNT ){

            // orderListItem
            $APPLICATION->IncludeComponent("aoptima:orderListItem", "",
                array('ORDER' => $order,
                      'IS_MOB_APP' => $arParams['IS_MOB_APP'])
            );

        } else {
            echo '<tr class="ost"></tr>';
        }
    }

} ?>