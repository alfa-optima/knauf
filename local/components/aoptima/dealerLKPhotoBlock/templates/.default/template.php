<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<?php

global $USER;
if ($USER->IsAdmin()){
	//echo '<pre>';
	//print_r($arResult["USER"]);
	//echo '</pre>';
}

?>
<div class="dealerAccount-form__rightCol dealerLKPhotoBlock">

    <p style="text-align: center; max-width: 190px;margin-bottom:40px;"><a href="<?=PREFIX?>/dealers/<?=$arResult['USER']['ID']?>/" target="_blank">Посмотреть свою страницу&nbsp;дилера на&nbsp;сайте</a></p>

    <? if( !$arResult['PHOTO_SRC'] ){ ?>
        <div class="dealerAccount-form__logoUpload persPhotoSelect to___process">
            <label>
                <span>Загрузить логотип</span>
            </label>
        </div>

    <? } else { ?>

        <div class="dealerAccount-form__logoUpload persPhotoSelect to___process" style="background: url('<?=$arResult['PHOTO_SRC']?>'); background-repeat: no-repeat;">

            <a style="cursor: pointer; z-index: 999999;" class="cart__goodsTable-delBtn removeDealerPhotoButton to___process" title="Удалить логотип">
                <span class="sr-only">Удалить лого</span>
            </a>

        </div>

    <? } ?>

    <div class="dealerAccount-form__logoUpload-info">
        <span>Мин. размеры: <?=$arResult['MIN_WIDTH']?> x <?=$arResult['MIN_HEIGHT']?>&nbsp;px</span>
        <span>Макс. вес файла: <?=$arResult['MAX_FILE_SIZE']?> МБ</span>
        <span>Типы файлов: <?=$arResult['FILE_TYPES']?></span>
    </div>

    <p class="error___p"></p>



</div>