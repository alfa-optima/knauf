<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


$arResult['USER'] = $arParams['USER'];

if( intval($arResult['USER']['PERSONAL_PHOTO']) > 0 ){
    $arResult['PHOTO_SRC'] = tools\funcs::rIMGG($arResult['USER']['PERSONAL_PHOTO'], 4, 190, 190);
}

$arResult['MIN_WIDTH'] = project\pers_photo::MIN_WIDTH;
$arResult['MIN_HEIGHT'] = project\pers_photo::MIN_HEIGHT;
$arResult['MAX_FILE_SIZE'] = round(project\pers_photo::MAX_FILE_SIZE/1024/1024, 1);

$pers_photo = new project\pers_photo();
$arResult['FILE_TYPES'] = implode(', ', $pers_photo->getFileTypes());




$this->IncludeComponentTemplate();