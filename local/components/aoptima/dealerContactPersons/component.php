<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


$arResult['USER'] = $arParams['USER'];

$arResult['ITEMS'] = array();

$arResult['JSON'] = htmlspecialchars_decode(trim($arResult['USER']['UF_CONTACT_PERSONS']));

if( strlen($arResult['JSON']) > 0 ){

    $arResult['ITEMS'] = tools\funcs::json_to_array($arResult['JSON']);

    if( count($arResult['ITEMS']) > 0 ){

        foreach ( $arResult['ITEMS'] as $item_id => $item ){

            if( intval($item['loc_id']) > 0 ){
                $arResult['ITEMS'][$item_id]['loc_name'] = project\bx_location::getByID($item['loc_id'])['NAME_RU'];
            }

        }

    }

}





$this->IncludeComponentTemplate();