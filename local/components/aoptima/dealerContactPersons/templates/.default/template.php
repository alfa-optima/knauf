<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="dealerAccount-form__block contactPersonsBlock">

    <h4 class="tab__title">Контактные лица</h4>

    <? if( count($arResult['ITEMS']) > 0 ){

        foreach ( $arResult['ITEMS'] as $item_id => $item ){ ?>

            <div class="contactPersonItem" item_id="<?=$item_id?>">

                <div class="dealerAccount-form__row">
                    <div class="dealerAccount-form__field  dealerAccount-form__field--w271">
                        <span>Населённый пункт</span>
                        <input
                            class="account-form__suggestInput"
                            id="dealerContactPerson_<?=$item_id?>"
                            type="text"
                            placeholder="Начните ввод города"
                            cityName=""
                            autocomplete="off"
                            onfocus="cityFocus($(this));"
                            onkeyup="cityKeyUp($(this));"
                            onfocusout="cityFocusOut($(this));"
                            value="<?=$item['loc_name']?>"
                        >
                        <input class="loc_input" type="hidden" name="contact_persons[<?=$item_id?>][loc_id]" value="<?=$item['loc_id']?>">
                        <script>
                            $(document).ready(function(){
                                $('#dealerContactPerson_<?=$item_id?>').autocomplete({
                                    source: '/ajax/searchCity.php',
                                    minLength: 2,
                                    delay: 700,
                                    select: citySelectShort
                                })
                            })
                        </script>
                    </div>

                    <span class="account-form__deleteAddress removeContactPerson to___process">Удалить запись</span>

                </div>

                <div class="dealerAccount-form__row">
                    <div class="dealerAccount-form__field">
                        <span>Имя</span>
                        <input class="name_input" type="text" name="contact_persons[<?=$item_id?>][name]" value="<?=$item['name']?>">
                    </div>
                    <div class="dealerAccount-form__field">
                        <span>Фамилия</span>
                        <input class="last_name_input" type="text" name="contact_persons[<?=$item_id?>][last_name]" value="<?=$item['last_name']?>">
                    </div>
                    <div class="dealerAccount-form__field">
                        <span>Отчество</span>
                        <input class="second_name_input" type="text" name="contact_persons[<?=$item_id?>][second_name]" value="<?=$item[second_name]?>">
                    </div>
                </div>

                <div class="dealerAccount-form__row">

                    <? if(
                        is_array($item['phones'])
                        &&
                        count($item['phones']) > 0
                    ){
                        foreach($item['phones'] as $phone){ ?>
                            <div class="dealerAccount-form__field">
                                <span>Телефон</span>
                                <input class="phoneItem" type="tel" name="contact_persons[<?=$item_id?>][phones][]" value="<?=$phone?>">
                            </div>
                        <? }
                    } else { ?>
                        <div class="dealerAccount-form__field">
                            <span>Телефон</span>
                            <input class="phoneItem" type="tel" name=contact_persons[<?=$item_id?>][phones][]">
                        </div>
                    <? } ?>

                    <div class="dealerAccount-form__addTel">
                        <span>Добавить телефон</span>
                    </div>

                </div>

            </div>

        <? }
    } ?>

    <div class="dealerAccount-form__addPerson addContactPerson to___process"><span>Добавить контактное лицо</span></div>

</div>
