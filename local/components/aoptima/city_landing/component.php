<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$code = trim(strip_tags($_GET['code']));

$arResult['LANDING'] = tools\el::info_by_code( $code, project\city_landing::IBLOCK_ID );

if( intval( $arResult['LANDING']['ID'] ) > 0 ){

    $APPLICATION->SetPageProperty("title", $arResult['LANDING']["PROPERTY_SEO_TITLE_VALUE"]?$arResult['LANDING']["PROPERTY_SEO_TITLE_VALUE"]:$arResult['LANDING']["NAME"]);

    $arResult['LANDING']['BANNER_IMAGES'] = [];
    foreach ( $arResult['LANDING']['PROPERTY_BANNER_IMAGES_VALUE'] as $pic_id ){
        $arResult['LANDING']['BANNER_IMAGES'][] = \CFile::GetPath( $pic_id );
    }



    $this->IncludeComponentTemplate();
}




