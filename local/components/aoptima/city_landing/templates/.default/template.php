<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>

<section class="bcgBlock  bcgBlock--indexTop" style="background-image: url(<?=$arResult['LANDING']['BANNER_IMAGES'][0]?>);">
    <div class="bcgBlock__wrapper">
        <h2 class="bcgBlock__title" style="background-color: #e6332c; color: white;"><?=$arResult['LANDING']["NAME"]?></h2>
        <a class="bcgBlock__link"><span>АКЦИЯ</span></a>
    </div>
</section>

<section class="bcgBlock " style="background-color: white;">

    <div class="city_landing_text">
        <?=html_entity_decode($arResult['LANDING']["~PREVIEW_TEXT"])?>
    </div>

</section>

<?php if(
    is_array( $arResult['LANDING']['PROPERTY_PRODUCTS_VALUE'] )
    &&
    count( $arResult['LANDING']['PROPERTY_PRODUCTS_VALUE'] ) > 0
){

    $user = new project\user();

    $sort1 = 'PROPERTY_HAS_OFFERS';   $order1 = 'DESC';
    $sort2 = 'SORT';   $order2 = 'ASC';

    //////////////////////////////////
    // Все дилеры
    $dealer = new project\dealer();
    $allDealers = $dealer->allList();
    //////////////////////////////////

    $GLOBALS['landing_goods']['ID'] = $arResult['LANDING']['PROPERTY_PRODUCTS_VALUE'];

    $APPLICATION->IncludeComponent( "bitrix:catalog.section", "city_landing_products",
        Array(
            "LOC" => $_SESSION['LOC'],
            'IS_DEALER' => $user->isDealer()?'Y':'N',
            "IBLOCK_TYPE" => "content",
            "IBLOCK_ID" => project\catalog::catIblockID(),
            "INCLUDE_SUBSECTIONS" => 'Y',
            "SHOW_ALL_WO_SECTION" => "Y",
            "SECTION_USER_FIELDS" => array(),
            "ELEMENT_SORT_FIELD" => $sort1,
            "ELEMENT_SORT_ORDER" => $order1,
            "ELEMENT_SORT_FIELD2" => $sort2,
            "ELEMENT_SORT_ORDER2" => $order1,
            "FILTER_NAME" => 'landing_goods',
            "HIDE_NOT_AVAILABLE" => "N",
            "PAGE_ELEMENT_COUNT" => project\catalog::GOODS_CNT + 1,
            "LINE_ELEMENT_COUNT" => "3",
            "PROPERTY_CODE" => array("ARTICLE","MOD","MODS_ARTICLE","ED","VIEW","EDGE_VIEW","VIDEO","RESIST_WATER","DRYING_TIME","FLAMMABILITY_GROUP","BASE_MATERIAL","RESIST_COLD","FLOOR_COATING","TARGET","MIXTURE_BASE","PERFORATION","ROOM","POP","PREIM","PRIM","BUY_WITH","SEE_WITH","PROPERTIES","APPL_METHOD","TECH","TYPE","TYPE_OBJECT","TYPE_OF_APPLICATION","TYPE_PROFILE","TYPE_COATING","LAYER_THICKNESS","FASOVKA","TILE","COLOR","WEIGHT","HEIGHT","LENGTH","DEPTH","WIDTH","CROSS","CALC_WEIGHT","MAX_GABARIT","HAS_OFFERS"),
            "OFFERS_LIMIT" => "0",
            "OFFERS_PROPERTY_CODE" => ['LOCATION'],
            "TEMPLATE_THEME" => "",
            "PRODUCT_SUBSCRIPTION" => "N",
            "SHOW_DISCOUNT_PERCENT" => "N",
            "SHOW_OLD_PRICE" => "N",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
            "MESS_BTN_SUBSCRIBE" => "Подписаться",
            "MESS_BTN_DETAIL" => "Подробнее",
            "MESS_NOT_AVAILABLE" => "Нет в наличии",
            "SECTION_URL" => "",
            "DETAIL_URL" => "",
            "SECTION_ID_VARIABLE" => "SECTION_ID",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_GROUPS" => "Y",
            "SET_META_KEYWORDS" => "N",
            "META_KEYWORDS" => "",
            "SET_META_DESCRIPTION" => "N",
            "META_DESCRIPTION" => "",
            "BROWSER_TITLE" => "-",
            "ADD_SECTIONS_CHAIN" => "N",
            "DISPLAY_COMPARE" => "N",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "CACHE_FILTER" => "Y",
            "PRICE_CODE" => array('BASE'),
            "USE_PRICE_COUNT" => "N",
            "SHOW_PRICE_COUNT" => "1",
            "PRICE_VAT_INCLUDE" => "Y",
            "CONVERT_CURRENCY" => "N",
            "BASKET_URL" => "/personal/basket.php",
            "ACTION_VARIABLE" => "action",
            "PRODUCT_ID_VARIABLE" => "id",
            "USE_PRODUCT_QUANTITY" => "N",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "PRODUCT_PROPERTIES" => "",
            "PAGER_TEMPLATE" => "",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Товары",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "Y",
            "ADD_PICT_PROP" => "-",
            "LABEL_PROP" => "-",
            'INCLUDE_SUBSECTIONS' => "Y",
            'SHOW_ALL_WO_SECTION' => "Y",
            'IS_MOB_APP' => IS_ESHOP?"Y":"N"
        )
    );

} ?>