<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


$arResult['articleMappingNotes'] = [];
global $USER;
$userID = $USER->GetID();
$currentUser = new project\user( $USER->GetID() );
$userProps = $currentUser->arUser;
if(strlen($userProps["UF_MAIN_USER"]) > 0){
	$user = new project\user( $userProps["UF_MAIN_USER"] );
	if( $user->isDealer() ){
		$userID = $user->arUser["ID"];
	}
}
$dealer = tools\user::info( $userID );
if(
    isset($dealer['UF_ARTICLE_MAPPING'])
    &&
    strlen($dealer['UF_ARTICLE_MAPPING']) > 0
){
    $arResult['articleMappingNotes'] = tools\funcs::json_to_array($dealer['UF_ARTICLE_MAPPING']);
}






$this->IncludeComponentTemplate();