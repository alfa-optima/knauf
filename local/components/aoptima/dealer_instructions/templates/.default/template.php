<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>


<style>

.instruct_info a
{
    font-family: Arial, Helvetica, sans-serif;
    font-size: 14px;
    font-weight: 500;
    line-height: 19px;
    color: #009fe3;
    border-bottom: 1px dashed #009fe3;
}

.instruct_info a:hover
{
    color: #016b99;
}
.dealer_instruction
{
    width:100%;
    height:500px;
}
@media screen and (max-width: 1000px)
{
    .dealer_instruction
    {
        width:100%;
        height:315px;
    }
}
</style>

<div class="dealerAccount-form__wrapper dealerLoadMappingCSVFormBlock dealer-instructions-link-wrapper">
	<div class="instruct_info"><a href="<?=PREFIX?>/how_to_work_in_the_dealer_account/" target="_blank">Инструкция по работе в личном кабинете партнера</a></div>
</div>

<div class="dealerAccount-form__wrapper dealerLoadMappingCSVFormBlock dealer-instructions-video-wrapper">

    <iframe class="dealer_instruction" src="https://www.youtube.com/embed/DiVxofWLHI8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</div>

