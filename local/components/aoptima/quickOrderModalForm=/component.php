<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


if( intval($_POST['item_id']) > 0 && strlen($_POST['dealer_ids']) > 0 ){
    $arResult['PRODUCT'] = tools\el::info(intval($_POST['item_id']));
    if( intval($arResult['PRODUCT']['ID']) > 0 ){

        $dealer_ids = explode('|', $_POST['dealer_ids']);
        $arResult['DEALERS'] = [];
        foreach ( $dealer_ids as $dealer_id ){
            $dealer = tools\user::info($dealer_id);
            if( intval($dealer['ID']) > 0 ){
                $arResult['DEALERS'][ $dealer['ID'] ] = $dealer;
            }
        }
        

        $this->IncludeComponentTemplate();
    }
}





