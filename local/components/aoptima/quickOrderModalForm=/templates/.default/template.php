<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

<form class="quickOrderForm" onsubmit="return false">

    <div class="account-form__row">
        <div class="account-form__field  account-form__field--w420">
            <span>Дилер *</span>
            <div class="styledSelect">
                <select class="modalSelect" name="dealer">
                    <option value="empty">--- Выберите дилера ---</option>
                    <? foreach( $arResult['DEALERS'] as $dealer ){ ?>
                        <option value="<?=$dealer['UF_COMPANY_NAME']?> [<?=$dealer['ID']?>]"><?=$dealer['UF_COMPANY_NAME']?></option>
                    <? } ?>
                </select>
            </div>
        </div>
    </div>

    <div class="account-form__row">
        <div class="account-form__field  account-form__field--w420">
            <span>Город *</span><input type="text" name="city" value="<?=$_SESSION['LOC']['CITY']?>">
        </div>
    </div>

    <div class="account-form__row">
        <div class="account-form__field">
            <span>ФИО *</span><input type="text" name="fio">
        </div>
        <div class="account-form__field">
            <span>Телефон *</span><input type="tel" name="phone">
        </div>
        <div class="account-form__field">
            <span>Эл. почта *</span><input type="email" name="email">
        </div>
    </div>

    <div class="account-form__field  account-form__field--wa">
        <div class="account-form__checkbox">
            <input type="checkbox" id="agree_78" name="agree" value="1">
            <label for="agree_78">Я&nbsp;прочитал и&nbsp;принял условия <a href="/user_agreement/" target="_blank">конфиденциальности данных</a> и&nbsp;даю согласие на&nbsp;обработку своих персональных данных *</label>
        </div>
    </div>

    <div>
        <p class="error___p"></p>
    </div>

    <div class="account-form__regBtn">
        <button type="button" class="account-form__button quickOrderButton to___process">Отправить</button>
        <p class="account-form__note">* Пожалуйста, заполните все обязательные поля</p>
    </div>

    <input type="hidden" name="product_id" value="<?=$arResult['PRODUCT']['ID']?>">
    <input type="hidden" name="product_name" value="<?=$arResult['PRODUCT']['NAME']?>">

</form>
