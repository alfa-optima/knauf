<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('iblock');

use Bitrix\Main\Application,
Bitrix\Main\Context,
Bitrix\Main\Request;

use Bitrix\Main,
Bitrix\Main\Localization\Loc as Loc,
Bitrix\Main\Loader,
Bitrix\Main\Config\Option,
Bitrix\Sale\Delivery,
Bitrix\Sale\PaySystem,
Bitrix\Sale,
Bitrix\Sale\Basket,
Bitrix\Sale\Order,
Bitrix\Sale\DiscountCouponsManager;

$context = Context::getCurrent();
$request = $context->getRequest();
$method = $request->getRequestMethod();

$arHeaders = [];
$headers = $request->getHeaders();
foreach ( $headers as $key => $value ){
    $arHeaders[ $key ] = $value;
}

$values = $request->getValues();

if( in_array($method, [ 'GET' ]) ){

    if(
        is_array($values['products'])
        &&
        count($values['products']) > 0
    ){

        $under_order_ids = [];
        $under_order_cnts = [];

        // Удаляем старую корзину
        \CSaleBasket::DeleteAll(\CSaleBasket::GetBasketUserID());

        $APPLICATION->set_cookie('under_order', $cookie_val, time()-2592000, '/', '.'.\Bitrix\Main\Config\Option::get('main', 'server_name'));

        $APPLICATION->set_cookie('under_order_cnt', $cookie_val, time()-2592000, '/', '.'.\Bitrix\Main\Config\Option::get('main', 'server_name'));

        $mapTable = project\basket::getMapTable();

        foreach ( $values['products'] as $ext_id => $qnt ){

            $knauf_id = $mapTable[ $ext_id ];

            if( isset( $knauf_id ) ){

                $product = project\catalog::getProductByArticle( $knauf_id );

                if( intval( $product['ID'] ) > 0 ){

                    $filter = [
                        "IBLOCK_ID" => project\catalog::tpIblockID(),
                        "ACTIVE" => "Y",
                        "PROPERTY_CML2_LINK" => $product['ID']
                    ];
                    $fields = [ "ID", "PROPERTY_CML2_LINK" ];
                    $offers = \CIBlockElement::GetList(
                        [ "SORT" => "ASC" ], $filter, false, [ "nTopCount" => 1 ], $fields
                    );
                    if( $offer = $offers->GetNext() ){

                        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
                        if ($item = $basket->getExistsItem('catalog', $offer['ID'])){
                            $item->setField('QUANTITY', $item->getQuantity() + $qnt);
                        } else {
                            $item = $basket->createItem('catalog', $offer['ID']);
                            $item->setFields(array('QUANTITY' => $qnt, 'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(), 'LID' => \Bitrix\Main\Context::getCurrent()->getSite(), 'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',));
                        }
                        $basket->save();

                    } else {

                        $under_order_ids[] = $product['ID'];
                        $under_order_cnts[ $product['ID'] ] = $qnt;

                    }
                }
            }
        }

        // Добавим их в куку
        global $APPLICATION;
        if( count($under_order_ids) > 0 ){
            $cookie_val = implode('|', $under_order_ids);
        } else {
            $cookie_val = '';
        }
        $APPLICATION->set_cookie('under_order', $cookie_val, time()+2592000, '/', '.'.\Bitrix\Main\Config\Option::get('main', 'server_name'));

        $APPLICATION->set_cookie('under_order_cnt', serialize($under_order_cnts), time()+2592000, '/', '.'.\Bitrix\Main\Config\Option::get('main', 'server_name'));


        LocalRedirect( '/basket/' );



    } else {
        LocalRedirect( '/' );
    }
} else {
    LocalRedirect( '/' );
}