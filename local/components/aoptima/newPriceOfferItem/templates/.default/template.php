<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>


<div class="modal-addGoods__item newPriceOfferItem" number="<?=$arResult['NUMBER']?>">
    <div class="account-form__checkbox">
        <input type="checkbox" id="pod_zakaz_<?=$arResult['NUMBER']?>" name="offers[<?=$arResult['NUMBER']?>][pod_zakaz]" value="Y">
        <label for="pod_zakaz_<?=$arResult['NUMBER']?>">Под заказ</label>
    </div>
    <div class="modal-addGoods__stockAndPrice">
        <div class="account-form__field">
            <span>Наличие</span>
            <div>
                <input class="quantity_input number___input" name="offers[<?=$arResult['NUMBER']?>][quantity]" type="text">
                <span class="edSpan"><?=$arResult['el']['PROPERTY_ED_VALUE']?$arResult['el']['PROPERTY_ED_VALUE']:'шт'?></span>
            </div>
        </div>
        <div class="account-form__field">
            <span>Цена</span>
            <div>
                <input class="price_input price___input" name="offers[<?=$arResult['NUMBER']?>][price]" type="text">
                <span><span class="rub">₽</span></span>
            </div>
        </div>
    </div>
    <div class="account-form__field">
        <span>Регион</span>
        <div class="styledSelect">
            <input
                class="account-form__suggestInput regionName"
                id="addPriceRegion_<?=$arResult['NUMBER']?>"
                type="text"
                process="N"
                value=""
                loc_name=""
                onfocus="regionFocus($(this));"
                onkeyup="regionKeyUp($(this));"
                onfocusout="regionFocusOut($(this));"
                autocomplete="off"
            >
            <input class="loc_input" type="hidden" name="offers[<?=$arResult['NUMBER']?>][loc_id]" value="">

            <script>
                $(document).ready(function(){
                    $('#addPriceRegion_<?=$arResult["NUMBER"]?>').autocomplete({
                        source: '/ajax/searchRegion.php',
                        minLength: 2,
                        delay: 700,
                        select: regionSelect
                    })
                })
            </script>

        </div>
    </div>
</div>