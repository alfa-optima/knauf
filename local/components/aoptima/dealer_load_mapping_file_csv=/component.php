<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


$arResult['articleMappingNotes'] = [];

$dealer = tools\user::info( $USER->GetID() );
if(
    isset($dealer['UF_ARTICLE_MAPPING'])
    &&
    strlen($dealer['UF_ARTICLE_MAPPING']) > 0
){
    $arResult['articleMappingNotes'] = tools\funcs::json_to_array($dealer['UF_ARTICLE_MAPPING']);
}






$this->IncludeComponentTemplate();