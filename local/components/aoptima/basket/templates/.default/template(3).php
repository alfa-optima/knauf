<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$basketInfo = $arResult['basketInfo']; ?>

<div class="basketArea">

    <script type="text/javascript">
    var basketInfo = {
        'DS_LIST': <?=json_encode( $arResult['DS_LIST'] )?>,
        'PS_LIST': <?=json_encode( $arResult['PS_LIST'] )?>
    };

    $(document).ready(function(){
        $('.basketSmallBlock .main-nav__cart-quantity').html(<?=$basketInfo['basketProductsCNT']?>);
    })
    </script>

    <? if( $basketInfo['basketProductsCNT'] > 0 ){ ?>

        <script type="text/javascript">
            dropDownHandler();
        </script>

        <section class="block  block--account">
            <div class="block__wrapper">
                <h1 class="block__title">Корзина</h1>
                <div class="block__textBlock">
                    <p class="block__text"><?=$arResult['basket_cnt']?></p>
                </div>
            </div>
        </section>

        <section class="block  block--cartForm">
            <div class="block__wrapper">

                <form onsubmit="return false" method="post" class="form basketForm" style="margin-bottom: 0" action="/order/" method="POST" onsubmit="return false">


<div class="cart-form__block">
    <div class="cart-form__wrapper  cart-form__wrapper--goods">
        <table class="cart__goodsTable">
            <tbody>

            <? // Товары корзины
            foreach( $basketInfo['basketProducts'] as $basketProduct ){ ?>

                <tr class="basketProductTR" data-product-id="<?=$basketProduct['el']['ID']?>">
                    <td>
                        <div class="cart__goodsTable-leftCol">
                            <a style="cursor:pointer" class="cart__goodsTable-delBtn basketRemoveButton to___process">
                                <span class="sr-only">Удалить товар</span>
                            </a>
                            <a href="<?=$basketProduct['el']['DETAIL_PAGE_URL']?>" class="cart__goodsTable-image">
                                <? if(
                                    intval($basketProduct['el']['DETAIL_PICTURE']) > 0
                                ){ ?>
                                    <img src="<?=tools\funcs::rIMGG($basketProduct['el']['DETAIL_PICTURE'], 4, 89, 82)?>">
                                <? } else { ?>
                                    <div style="display: block; width:89px; height:82px; display: block; width: 89px; height: 82px; background-color: white; padding-top: 18px;">
                                        <p style="text-align: center; color: #929292; text-transform: uppercase; font-size: 13px;">Нет фото</p>

                                    </div>
                                <? } ?>
                            </a>
                            <a href="<?=$basketProduct['el']['DETAIL_PAGE_URL']?>" class="cart__goodsTable-link"><?=$basketProduct['el']['NAME']?></a>
                        </div>
                    </td>
                    <td>
                        <div class="cart__goodsTable-rightCol">
                            <div class="cart__goodsTable-size" style="height:auto">

                                <a class="cart__goodsTable-link" style="color: #807366;"><?=$basketProduct['quantity']?> шт</a>

                            </div>
                            <div class="cart__goodsTable-counter">
                                <button type="button" class="cart__goodsTable-counterBut dec basketMinus">-</button>
								<!--<input type="text" name="quantity" class="field fieldCount" value="<?=$basketProduct['quantity']?>" data-min="1" data-max="9999" onkeyup="minus_zero($(this)); $('.basketRecalcButton').show();" onblur="minus_zero($(this), true);">-->
								<input type="text" name="quantity" class="field fieldCount" value="<?=$basketProduct['quantity']?>" data-min="1" data-max="9999" onblur="minus_zero($(this), true);">
                                <button type="button" class="cart__goodsTable-counterBut inc basketPlus">+</button>
                            </div>
                        </div>
                    </td>
                </tr>

            <? } ?>

            <tr>
                <td></td>
                <td style="height:64px">
					<a style="cursor:pointer; display:none; margin-right:20px;" class="cart__goodsTable-button basketRecalcButton to___process">Пересчитать</a>
                    <a style="cursor:pointer;" class="cart__goodsTable-button basketCleanButton to___process">Очистить</a>
                </td>
            </tr>

            <tr>
                <td colspan="2"></td>
            </tr>

            <? if( count($arResult['under_order_goods']) > 0 ){ ?>
                <tr>
                    <td colspan="2" style="padding:15px;">
                        <p class="block__text" style="text-align: left;"><b>Товары под заказ:</b></p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                </tr>

                <? $cnt = 0;
                foreach( $arResult['under_order_goods'] as $id => $el ){ $cnt++; ?>

                    <tr class="basketUnderOrderProductTR" data-product-id="<?=$el['ID']?>">
                        <td>
                            <div class="cart__goodsTable-leftCol">
                                <a style="cursor:pointer" class="cart__goodsTable-delBtn basketRemoveUnderOrderButton to___process">
                                    <span class="sr-only">Удалить товар</span>
                                </a>
                                <a href="<?=$el['DETAIL_PAGE_URL']?>" class="cart__goodsTable-image">
                                    <? if(
                                        intval($el['DETAIL_PICTURE']) > 0
                                    ){ ?>
                                        <img src="<?=tools\funcs::rIMGG($el['DETAIL_PICTURE'], 4, 89, 82)?>">
                                    <? } else { ?>
                                        <div style="display: block; width:89px; height:82px; display: block; width: 89px; height: 82px; background-color: white; padding-top: 18px;">
                                            <p style="text-align: center; color: #929292; text-transform: uppercase; font-size: 13px;">Нет фото</p>

                                        </div>
                                    <? } ?>
                                </a>
                                <a href="<?=$el['DETAIL_PAGE_URL']?>" class="cart__goodsTable-link"><?=$el['NAME']?></a>
                            </div>
                        </td>
                        <td></td>
                    </tr>

                <? } ?>

                <tr>
                    <td colspan="2" style="padding: 0 30px;">
                        <p style="text-align: left;"><span class="cart-form__deliveryColumn-text">Этих товаров на данный момент нет в наличии у официальных дилеров, но мы передадим информацию о вашем желании приобрести данные продукты и дилер уточнит информацию о возможности и сроках поставки при его подтверждении</span></p>
                    </td>
                </tr>

            <? } ?>
			
			<!--<tr>
                <td colspan="2" style="padding: 0 30px;">
                        <p style="text-align: left;"><span class="cart-form__deliveryColumn-text">Заполните все параметры, и мы подберем для вас лучшее предложение</span></p>
                 </td>
            </tr>-->

            </tbody>
        </table>
    </div>
</div>




<!-- Варианты доставки -->
<div class="cart-form__block">
    <div class="cart-form__wrapper">
        <h4 class="cart-form__title">Тип доставки</h4>
        <div class="account-form__row">
            <div class="account-form__field  account-form__field--w100   account-form__field--noMB">
                <div class="account-form__checkboxes">

                    <? foreach( $arResult['DS_LIST'] as $ds ){ ?>

                        <div class="account-form__checkbox  account-form__checkbox--w228">
                            <input type="radio" id="ds_<?=$ds['ID']?>" name="ds" <? if( $arParams['form_data']['ds'] == $ds['ID'] ){ echo 'checked'; } ?> value="<?=$ds['ID']?>" delivType="<?=$ds['delivType']?>">
                            <label for="ds_<?=$ds['ID']?>"><?=$ds['NAME']?></label>
                        </div>

                    <? } ?>

                </div>
            </div>
        </div>
    </div>
</div>


<!--- Выбор населённого пункта --->
<div class="cart-form__block  cart-form__block--pickup" style="display: <? if( intval($arParams['form_data']['ds']) > 0 ){ echo 'block'; } else { echo 'none'; } ?>;">
    <div class="cart-form__wrapper">

        <? if( 1==2 ){ ?>

            <div class="account-form__row">

                <div class="account-form__field  account-form__field--w318">
                    <h4 class="cart-form__title">Ваш населённый пункт</h4>
                    <input
                            class="cityName account-form__suggestInput"
                            id="basketCity"
                            type="text"
                            process="N"
                            name="basketCity"
                            value="<?=($arResult['LOC']?($arResult['LOC']['NAME_RU'].' ('.$arResult['LOC']['PARENT_NAME_RU'].')'):'')?>"
                            cityName=""
                            onfocus="cityFocus($(this));"
                            onkeyup="cityKeyUp($(this));"
                            onfocusout="cityFocusOut($(this), '<?=$arResult['LOC']['ID']?>');"
                            autocomplete="off"
                    >
                    <input type="hidden" name="loc_id" value="<?=$arResult['LOC']['ID']?>">
                </div>

                <script>
                    $(document).ready(function(){
                        $('#basketCity').autocomplete({
                            source: '/ajax/searchCity.php',
                            minLength: 2,
                            delay: 700,
                            select: citySelect,
                            response: function(event, ui){
                                if( ui.content.length == 0 ){
                                    reloadBasket();
                                }
                            }
                        })
                    })
                </script>

            </div>

        <? } else { ?>

            <div class="account-form__row">
                 <h4 class="cart-form__title">Населённый пункт: &nbsp; <?=($arResult['LOC']?($arResult['LOC']['NAME_RU'].' ('.$arResult['LOC']['PARENT_NAME_RU'].')'):'')?></h4>
            </div>

            <input type="hidden" name="loc_id" value="<?=$arResult['LOC']['ID']?>">
            <input type="hidden" name="basketCity" value="<?=($arResult['LOC']?($arResult['LOC']['NAME_RU'].' ('.$arResult['LOC']['PARENT_NAME_RU'].')'):'')?>">

        <? } ?>



        <div class="account-form__row">

            <div class="account-form__field  account-form__field--w318" style="width:100%; display: <? if( intval($arParams['form_data']['loc_id']) > 0 ){ echo 'block'; } else { echo 'none'; } ?>;">

                <h4 class="cart-form__title">Ваш адрес</h4>
<!--
				<? //if( $arResult['delivType'] == 'samovyvoz' ):?>
                    <p style="font-size: 22px; line-height: 25px;"><small>Укажите адрес и мы подберем ближайшую к нему точку самовывоза</small></p>

				<?//else:?>
					<p style="font-size: 22px; line-height: 25px;"><small>Укажите адрес для подбора лучшей цены доставки</small></p>
				
				<?//endif;?>
-->
                <input
                    class="cityName account-form__suggestInput"
                    id="address"
                    type="text"
                    process="N"
                    name="address"
                    addressName=""
                    value="<?=$arResult['ADDRESS']?$arResult['ADDRESS']:''?>"
                    onfocus="addressFocus($(this));"
                    onkeyup="addressKeyUp($(this), false, true);"
                    onfocusout="addressFocusOut($(this), '<?=$arResult['ADDRESS_COORDS']?>');"
                    autocomplete="off"
                >
                <input type="hidden" name="address_coords" value="<?=$arResult['ADDRESS_COORDS']?>">
            </div>

            <script>
            $(document).ready(function(){
                $('#address').autocomplete({
                    source: '/ajax/searchAddress.php',
                    minLength: 2,
                    delay: 700,
                    select: addressSelect,
                    response: function(event, ui){
                        if( ui.content.length == 0 ){
                            reloadBasket();
                        }
                    }
                })
            })
            </script>

        </div>

        <div class="account-form__row account-form__row--dblock" style="width:100%; display:none <? //if( intval($arParams['form_data']['loc_id']) > 0 && $arResult['delivType'] == 'delivery' ){ echo 'block'; } else { echo 'none'; } ?>;">
		
            <div class="account-form__fields">
                <div class="account-form__field  account-form__field--w85">
                    <span>Подъезд</span>
                    <input type="text" onkeyup="reloadDeliveryTemplates();" name="podyezd" value="<?=$arParams['form_data']['podyezd']?$arParams['form_data']['podyezd']:$arResult['PODYEZD_DEFAULT']?>">
                </div>
                <div class="account-form__field  account-form__field--w85">
                    <span>Этаж</span>
                    <input type="text" onkeyup="reloadDeliveryTemplates();" name="floor" value="<?=$arParams['form_data']['floor']?$arParams['form_data']['floor']:$arResult['FLOOR_DEFAULT']?>">
                </div>
                <div class="account-form__field  account-form__field--w85">
                    <span>кв/офис</span>
                    <input type="text" onkeyup="reloadDeliveryTemplates();" name="kvartira" value="<?=$arParams['form_data']['kvartira']?$arParams['form_data']['kvartira']:$arResult['KV_DEFAULT']?>">
                </div>
            </div>
            <div class="account-form__field  account-form__field--w416  account-form__field--mr0">
                <div class="account-form__checkboxes">

                    <? foreach( $arResult['LIFT_ENUMS'] as $enum ){ ?>

                        <div class="account-form__checkbox  account-form__checkbox--mr35">
                            <input class="liftCheckbox" <? if( isset($arParams['form_data']['lift']) ){ if( $arParams['form_data']['lift'] == $enum['VALUE']){ echo 'checked'; } } else { if( $arResult['LIFT_DEFAULT'] == $enum['VALUE']){ echo 'checked'; } } ?> type="radio" id="lift_<?=$enum['XML_ID']?>" name="lift" value="<?=$enum['VALUE']?>" lift_enum_id="<?=$enum['ID']?>" onchange="reloadDeliveryTemplates();">
                            <label for="lift_<?=$enum['XML_ID']?>"><?=$enum['VALUE']?></label>
                        </div>

                    <? } ?>

                </div>
            </div>

            <br>

            <div class="account-form__field  account-form__field--w416  account-form__field--mr0">
                <div class="account-form__checkboxes">
                    <div class="account-form__checkbox  account-form__checkbox--mr35">
                        <input class="razgruzkaCheckbox" <? if( $arParams['form_data']['needRazgruzka'] == 'Да'){ echo 'checked'; } ?> type="checkbox" id="razgruzkaCheckbox" name="needRazgruzka" value="Да">
                        <label for="razgruzkaCheckbox">Требуется разгрузка</label>
                    </div>
                </div>
            </div>

            <? if( strlen($arParams['form_data']['floor']) > 0 ){ ?>

                <br>

                <div class="account-form__field  account-form__field--w416  account-form__field--mr0">
                    <div class="account-form__checkboxes">
                        <div class="account-form__checkbox  account-form__checkbox--mr35">
                            <input class="needPodyemCheckbox" <? if( $arParams['form_data']['needPodyem'] == 'Да'){ echo 'checked'; } ?> type="checkbox" id="needPodyemCheckbox" name="needPodyem" value="Да">
                            <label for="needPodyemCheckbox">Требуется подъём на этаж</label>
                        </div>
                    </div>
                </div>

            <? } ?>

            <br>

            <div class="account-form__field  account-form__field--w416  account-form__field--mr0">
                <div class="account-form__checkboxes">
                    <div class="account-form__checkbox  account-form__checkbox--mr35">
                        <input class="needPerenosCheckbox" <? if( $arParams['form_data']['needPerenos'] == 'Да'){ echo 'checked'; } ?> type="checkbox" id="needPerenosCheckbox" name="needPerenos" value="Да">
                        <label for="needPerenosCheckbox">Требуется перенос</label>
                    </div>
                </div>
                <div class="account-form__field" <? if( $arParams['form_data']['needPerenos'] != 'Да'){ ?>style="display:none;"<? } ?>>
                    <span>Расстояние переноса, м.</span>
                    <input type="text" name="perenosDistance" value="<?=$arParams['form_data']['perenosDistance']?>">
                </div>
            </div>

        </div>
		
		<? if($arResult['delivType'] == 'delivery' ):?>
		<div class="account-form__toggleBtn"><span>Дополнительные параметры - полный адрес, разгрузка, подъем, перенос</span></div>
		<?endif;?>

    </div>
</div>


<? if(
    $arResult['delivType'] == 'delivery'
    &&
    strlen($arParams['form_data']['basketCity']) > 0
    &&
    strlen($arParams['form_data']['address']) > 0
    &&
    strlen($arParams['form_data']['address_coords']) > 0
){ ?>

    <div class="cart-form__block">
        <div class="cart-form__wrapper">
            <div class="account-form__row">

                <div class="account-form__field ">
                    <span class="cart-form__subtitle">Дата доставки</span>
                    <input autocomplete="off" class="basketDatepicker" type="text" name="deliveryDate" value="<?=$arParams['form_data']['deliveryDate']?>">
                </div>

                <div class="account-form__field">
                    <span class="cart-form__subtitle">Время доставки</span>
                    <div class="styledSelect">
                        <select id="deliveryTime" name="deliveryTime">
                            <option value="empty">--- Выбор времени ---</option>
                            <? foreach( $arResult['DELIVERY_TIME_LIST'] as $title => $time ){ ?>
                                <option <? if( $arParams['form_data']['deliveryTime'] == $title ){ echo 'selected'; } ?> value="<?=$title?>"><?=$title?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
    $(".basketDatepicker").datepicker({
        inline: true,
        showOtherMonths: true,
        minDate: 0
    });
    </script>

<? } ?>


<? // Варианты оплаты
if(
    (
        (
            $arResult['delivType'] == 'delivery'
            &&
            strlen($arParams['form_data']['deliveryDate']) > 0
            &&
            $arParams['form_data']['deliveryTime'] != 'empty'
        )
        ||
        $arResult['delivType'] != 'delivery'
    )
    &&
    count($arResult['PS_LIST']) > 0
    &&
    strlen($arParams['form_data']['address_coords']) > 0
){ ?>

    <div class="cart-form__block" <? if( !$arResult['LOC'] || !$arParams['form_data']['ds'] ){ echo 'style="display:none"'; } ?>>
        <div class="cart-form__wrapper">

            <h4 class="cart-form__title">Вариант оплаты</h4>

            <div class="account-form__row  account-form__row--mb36">
                <div class="account-form__field  account-form__field--w760">
                    <div class="account-form__checkboxes">

                        <? $ps_cnt = 0;
                        foreach( $arResult['PS_LIST'] as $ps ){ $ps_cnt++; ?>

                            <div class="account-form__checkbox  account-form__checkbox--w228">
                                <input class="psInput" type="radio" id="ps_<?=$ps['ID']?>" name="ps" <? if( $ps['CODE'] == $arParams['form_data']['ps'] ){ echo 'checked'; } ?> value="<?=$ps['CODE']?>">
                                <label for="ps_<?=$ps['ID']?>"><?=$ps['NAME']?>
                                    <? if( intval($ps['PSA_LOGOTIP']) > 0 ){ ?>
                                        <div class="account-form__checkbox-paymentMethods">
                                            <img src="<?=\CFile::GetPath($ps['PSA_LOGOTIP'])?>">
                                        </div>
                                    <? } ?>
                                </label>
                            </div>

                            <? if(
                                $ps_cnt%3 == 0
                                &&
                                $ps_cnt < count($arResult['PS_LIST'])
                            ){ ?>

                                </div></div></div>
                                <div class="account-form__row"><div class="account-form__field  account-form__field--w760"><div class="account-form__checkboxes">

                            <? } ?>

                        <? } ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<? } ?>

<? // Типы оплаты
if(
    (
        $arResult['delivType'] == 'delivery'
        &&
        strlen($arParams['form_data']['deliveryDate']) > 0
        &&
        $arParams['form_data']['deliveryTime'] != 'empty'
    )
    ||
    $arResult['delivType'] != 'delivery'
){ ?>

    <div class="cart-form__block" <? if( !$arParams['form_data']['ps'] ){ echo 'style="display:none"'; } ?>>
        <div class="cart-form__wrapper">

            <h4 class="cart-form__title">Тип оплаты</h4>

            <div class="account-form__row ">
                <div class="account-form__field  account-form__field--w760">
                    <div class="account-form__checkboxes">

                        <div class="account-form__checkbox  account-form__checkbox--w228">

                            <input class="prepsInput" type="radio" id="pay_type_pre" name="pay_type" <? if( $arParams['form_data']['pay_type'] == 'pre' ){ echo 'checked'; } ?> value="pre">

                            <label for="pay_type_pre">Оплатить сразу</label>

                        </div>

                        <div class="account-form__checkbox  account-form__checkbox--w228">

                            <input class="prepsInput" type="radio" id="pay_type_post" name="pay_type" <? if( $arParams['form_data']['pay_type'] == 'post' ){ echo 'checked'; } ?> value="post">

                            <label for="pay_type_post">Оплата при получении</label>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

<? } ?>


                    <input type="hidden" name="dealer_id">
                    <input type="hidden" name="pvz">
                    <input type="hidden" name="delivType" value="<?=$arResult['delivType']?>">


                </form>
            </div>
        </section>


        <? if( !$arParams['form_data']['ds'] ){ ?>
            <section class="sliderBlock">
                <div class="sliderBlock__wrapper  sliderBlock__wrapper--cart">
                    <h2 class="sliderBlock__title" style="text-align:center; color:#bf0000;">Заполните все параметры, <br>и мы подберем для вас лучшее предложение</h2>
                </div>
            </section>
        <? } else if( !$arResult['LOC'] ){ ?>
            <section class="sliderBlock">
                <div class="sliderBlock__wrapper  sliderBlock__wrapper--cart">
                    <h2 class="sliderBlock__title" style="text-align:center; color:#bf0000;">Укажите Ваш населённый пункт</h2>
                </div>
            </section>
        <? } else if( !$arParams['form_data']['address'] ){ ?>
            <section class="sliderBlock">
                <div class="sliderBlock__wrapper  sliderBlock__wrapper--cart">
					<?if ($arParams['form_data']['ds']==2):?>
						<h2 class="sliderBlock__title" style="text-align:center; color:#bf0000;">Укажите адрес, и мы подберем ближайшую к нему точку самовывоза.</h2>
					<?else:?>
						<h2 class="sliderBlock__title" style="text-align:center; color:#bf0000;">Укажите адрес для подбора лучшей цены доставки</h2>
					<?endif;?>	

                </div>
            </section>
        <? } else if( !$arParams['form_data']['address_coords'] ){ ?>
            <section class="sliderBlock">
                <div class="sliderBlock__wrapper  sliderBlock__wrapper--cart">
                    <h2 class="sliderBlock__title" style="text-align:center; color:#bf0000;">Адрес не корректный (нужно выбрать из списка)</h2>
                </div>
            </section>
        <? } else if(
            $arResult['delivType'] == 'delivery'
            &&
            !$arParams['form_data']['deliveryDate']
        ){ ?>
            <section class="sliderBlock">
                <div class="sliderBlock__wrapper  sliderBlock__wrapper--cart">
                    <h2 class="sliderBlock__title" style="text-align:center; color:#bf0000;">Укажите дату доставки</h2>
                </div>
            </section>
    <? } else if(
        $arResult['delivType'] == 'delivery'
        &&
        (
            !$arParams['form_data']['deliveryTime']
            ||
            $arParams['form_data']['deliveryTime'] == 'empty'
        )
    ){ ?>
        <section class="sliderBlock">
            <div class="sliderBlock__wrapper  sliderBlock__wrapper--cart">
                <h2 class="sliderBlock__title" style="text-align:center; color:#bf0000;">Укажите время доставки</h2>
            </div>
        </section>
        <? } else if( !$arParams['form_data']['ps'] ){ ?>
            <section class="sliderBlock">
                <div class="sliderBlock__wrapper  sliderBlock__wrapper--cart">
                    <h2 class="sliderBlock__title" style="text-align:center; color:#bf0000;">Выберите вариант оплаты</h2>
                </div>
            </section>
        <? } else if( !$arParams['form_data']['pay_type'] ){ ?>
            <section class="sliderBlock">
                <div class="sliderBlock__wrapper  sliderBlock__wrapper--cart">
                    <h2 class="sliderBlock__title" style="text-align:center; color:#bf0000;">Выберите тип оплаты</h2>
                </div>
            </section>
        <? } else { ?>

            <? // ПРЕДЛОЖЕНИЯ ДИЛЕРОВ
            if( count($arResult['OFFERS_LIST']) > 0 ){ ?>

                <section class="cart-offer">
                    <div class="cart-offer__wrapper  cart-offer__wrapper--price">

                        <span class="cart-offer__quantity">Найдено предложений: <?=count($arResult['OFFERS_LIST'])?></span>

                        <? if( count($arResult['OFFERS_LIST']) > 1 ){ ?>

                            <!-- Сортировка -->
                            <div class="cart-offer__sortLine">
                                <span class="cart-offer__sortLine-dealer <? if( $arResult['offers_sort']['CODE']=='DEALER_RATING' ){ echo 'sort'.$arResult['offers_sort']['ORDER']; } else { echo 'no___sort'; } ?> offersSortButton to___process" sort="DEALER_RATING" order="<?=$arResult['offers_sort']['ORDER']?>">Дилер</span>
                                <span class="cart-offer__sortLine-quantity" style="background: none">Количество</span>
                                <span class="cart-offer__sortLine-price <? if( $arResult['offers_sort']['CODE']=='BASKET_SUM' ){ echo 'sort'.$arResult['offers_sort']['ORDER']; } else { echo 'no___sort'; } ?> offersSortButton to___process" sort="BASKET_SUM" order="<?=$arResult['offers_sort']['ORDER']?>">Цена</span>
                            </div>

                        <? } ?>

                        <div class="cart-offer__best-wrapper">

                            <h2 class="cart-offer__title">Предложения дилеров (<?=$arResult['LOC']['NAME_RU']?>)</h2>

                            <div class="cart-offer__best-items">

                                <? $offers_cnt = 0;
                                foreach( $arResult['OFFERS_LIST'] as $dealer ){

                                    $offers_cnt++;
                                    // basketOfferItem
                                    $APPLICATION->IncludeComponent(
                                        "aoptima:basketOfferItem", "",
                                        array(
                                            'dealer' => $dealer,
                                            'componentArResult' => $arResult,
                                            'visible' => $offers_cnt<=$arResult['MAX_CNT']?'Y':'N'
                                        )
                                    );

                                } ?>

                            </div>

                            <? if( count($arResult['OFFERS_LIST']) > $arResult['MAX_CNT'] ){ ?>
                                <div class="more-button basketMoreButton to___process" maxCNT="<?=$arResult['MAX_CNT']?>">
                                    <span>Показать еще (<?=count($arResult['OFFERS_LIST'])-$arResult['MAX_CNT']?>)</span>
                                </div>
                            <? } ?>

                        </div>
                    </div>
                </section>

            <? } else { ?>

                <section class="sliderBlock">
                    <div class="sliderBlock__wrapper  sliderBlock__wrapper--cart">
                     <h2 class="sliderBlock__title" style="text-align:center; color:#bf0000;">Предложений дилеров не найдено</h2>
                    </div>
                </section>

            <? } ?>

        <? } ?>






        <? // С этими товарами покупают
        echo $arResult['BUY_WITH_HTML']; ?>

        <? // С этими товарами смотрят
        echo $arResult['SEE_WITH_HTML']; ?>



    <? } else { ?>

        <section class="block  block--account">
            <div class="block__wrapper">
                <h1 class="block__title">Корзина</h1>
                <div class="block__textBlock">
                    <p class="block__text">Ваша корзина пуста</p>

                    <? if( count($arResult['under_order_goods']) > 0 ){ ?>
                        <hr style="border:1px dashed #e6e6e6; border-right:0px; border-left:0px; border-top:0px; height:1px; margin:10px 0 15px 0;">
                        <p class="block__text">Товары под заказ:</p>
                        <? foreach( $arResult['under_order_goods'] as $id => $el ){
                            $el = project\catalog::updateProductName($el); ?>
                            <p class="block__text" style="color:#04a2e0">- <a href="<?=$el['DETAIL_PAGE_URL']?>" style="color:#04a2e0"><?=$el['NAME']?></a></p>
                        <? } ?>
                    <? } ?>

                </div>
            </div>
        </section>

    <? } ?>

</div>



<? if( $basketInfo['basketProductsCNT'] > 0 ){ ?>

    <? if( 1==2 ){ ?>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/sliders.js"></script>
    <? } ?>

    <? if( $arParams['IS_AJAX'] == 'Y' ){ ?>

        <script src="<?=SITE_TEMPLATE_PATH?>/js/aoptima/datepicker.js"></script>

        <? if( 1==2 ){ ?>
            <script src="<?=SITE_TEMPLATE_PATH?>/js/default_scripts.js"></script>
        <? } ?>

    <? } ?>

<? } ?>