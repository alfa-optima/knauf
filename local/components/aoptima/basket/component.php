<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$user = new project\user();
if( $user->isDealer() ){
    if( $arParams['IS_MOB_APP']=='Y' ){
        LocalRedirect('/knauf_app/');
    } else {
        LocalRedirect('/');
    }
}


unset( $_SESSION['DEALER_WARNINGS'] );

project\action_gifts::Update();

// Получение необходимых данных
$arResult = project\basket::collectNecessaryData( $arResult, $arParams );

// Сбор предложений дилеров
$arResult = project\basket::collectDealerOffers( $arResult, $arParams );

// Сортировка предложений дилеров
$arResult = project\basket::sortDealerOffers( $arResult );

// Обрезаем массив (ограничиваем количество элементов)
$arResult['MAX_CNT'] = project\basket::LIST_OFFERS_CNT;

$this->IncludeComponentTemplate();