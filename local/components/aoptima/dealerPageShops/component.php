<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arResult['DEALER'] = $arParams['arResult']['DEALER'];

$arResult['MAP_POINTS'] = [];

$dealer_shop = new project\dealer_shop();
$arResult['DEALER_SHOPS'] = $dealer_shop->getList( $arResult['DEALER']['ID'] );
foreach( $arResult['DEALER_SHOPS'] as $shop_id => $shop ){
    $shop = tools\el::info($shop_id);

    if( intval($shop['ID']) > 0 ){

        if(
            strlen($shop['PROPERTY_GPS_VALUE']) > 0
            &&
            substr_count($shop['PROPERTY_GPS_VALUE'], ',')
        ){    $arResult['MAP_POINTS'][$shop['ID']] = $shop;    }

        $arAddress = array();
        if( intval($shop['PROPERTY_REGION_ID_VALUE']) > 0 ){
            $loc = project\bx_location::getByID($shop['PROPERTY_REGION_ID_VALUE']);
            $arAddress['region_name'] = $loc['NAME_RU'];
        }
        if( intval($shop['PROPERTY_LOC_ID_VALUE']) > 0 ){
            $loc = project\bx_location::getByID($shop['PROPERTY_LOC_ID_VALUE']);
            $arAddress['loc_name'] = $loc['NAME_RU'];
        }
        if( strlen($shop['PROPERTY_STREET_VALUE']) > 0 ){
            $arAddress['street'] = 'ул.'.$shop['PROPERTY_STREET_VALUE'];
        }
        if( strlen($shop['PROPERTY_HOUSE_VALUE']) > 0 ){
            $arAddress['house'] = 'д.'.$shop['PROPERTY_HOUSE_VALUE'];
        }
        if( strlen($shop['PROPERTY_OFFICE_VALUE']) > 0 ){
            $arAddress['office'] = 'офис '.$shop['PROPERTY_OFFICE_VALUE'];
        }
        $shop['ADDRESS'] = implode(', ', $arAddress);

        $shop['PHONES'] = array();
        if( strlen($shop['PROPERTY_PHONES_VALUE']['TEXT']) > 0 ){
            $shop['PHONES'] = tools\funcs::json_to_array($shop['PROPERTY_PHONES_VALUE']['TEXT']);
        }

        sort($shop['PROPERTY_WORK_DAYS_VALUE']);
        $shop['WORK_DAYS'] = array_map(
            function ( $day_num ){
                return project\dealer_shop::$work_days[$day_num]['title'];
            },
            $shop['PROPERTY_WORK_DAYS_VALUE']
        );

        $arResult['DEALER_SHOPS'][$shop_id] = $shop;

    }

}


//echo "<pre>"; print_r( $arResult['DEALER_SHOPS'] ); echo "</pre>";






$this->IncludeComponentTemplate();