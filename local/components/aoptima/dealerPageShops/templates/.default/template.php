<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project; ?>

<section>
    <div>
        <div class="tab__inner">

            <? if( count($arResult['MAP_POINTS']) > 0 ){ ?>
                <div class="dealer__map" id="map_canvas"></div>
            <? } ?>

            <div class="dealer__addresses">
                <h3 class="dealer__addresses-title">Адреса точек продаж</h3>
                <div class="dealer__addresses-items">

                    <? if( count($arResult['DEALER_SHOPS']) > 0 ){ ?>

                        <? foreach( $arResult['DEALER_SHOPS'] as $shop_id => $shop ){ ?>

                            <div class="dealer__addresses-item">


                                <h4 class="dealer__addresses-itemTitle"><?=$shop['NAME']?></h4>

                                <div class="dealer__addresses-row">
                                    <div class="dealer__addresses-column">
                                        <!--
                                        <div class="dealer__addresses-stars">
                                            <span class="dealer__addresses-star  dealer__addresses-star--full"></span>
                                            <span class="dealer__addresses-star  dealer__addresses-star--full"></span>
                                            <span class="dealer__addresses-star  dealer__addresses-star--full"></span>
                                            <span class="dealer__addresses-star  dealer__addresses-star--full"></span>
                                            <span class="dealer__addresses-star"></span>
                                        </div>
                                        -->
                                        <span><?=$shop['ADDRESS']?></span>
                                    </div>
                                    <div class="dealer__addresses-column">
                                        <? if( is_array( $shop['PHONES'] ) ){
                                            foreach ( $shop['PHONES'] as $key => $phone ){ ?>
                                                <span><?=$phone?></span>
                                            <? }
                                        } ?>
                                        <? if( strlen($shop['PROPERTY_EMAIL_VALUE']) > 0 ){ ?>
                                            <a href="mailTo:<?=$shop['PROPERTY_EMAIL_VALUE']?>"><?=$shop['PROPERTY_EMAIL_VALUE']?></a>
                                        <? } ?>
                                        <? if( strlen($shop['PROPERTY_SITE_VALUE']) > 0 ){
                                            $site_url = $shop['PROPERTY_SITE_VALUE'];
                                            if(
                                                substr($site_url, 0, 1) != '/'
                                                &&
                                                !substr_count($site_url, 'http')
                                            ){
                                                $site_url = 'https://'.$shop['PROPERTY_SITE_VALUE'];
                                            }?>
                                            <a href="<?=$site_url?>" target="_blank"><?=$shop['PROPERTY_SITE_VALUE']?></a>
                                        <? } ?>
                                    </div>
                                    <div class="dealer__addresses-column">
										<span>Режим работы:<br>
											<?echo $shop['PROPERTY_REZHIM_RABOTY_VALUE']['TEXT'];?>
										</span>
                                        <!--<span>Рабочие дни:<br><?//=implode(', ', $shop['WORK_DAYS'])?></span>
                                        <?/* if(
                                            $shop['PROPERTY_WORK_TIME_OT_VALUE'] != 'empty'
                                            &&
                                            $shop['PROPERTY_WORK_TIME_DO_VALUE'] != 'empty'
                                        ){ */?>
                                            <span>Время работы:<br><?//=$shop['PROPERTY_WORK_TIME_OT_VALUE']?> - <?//=$shop['PROPERTY_WORK_TIME_DO_VALUE']?></span>
                                        <? //} ?>
                                        <? /*if(
                                            $shop['PROPERTY_OBED_TIME_OT_VALUE'] != 'empty'
                                            &&
                                            $shop['PROPERTY_OBED_TIME_DO_VALUE'] != 'empty'
                                        ){ */?>
                                            <span>Время обеда:<br><?//=$shop['PROPERTY_OBED_TIME_OT_VALUE']?> - <?//=$shop['PROPERTY_OBED_TIME_DO_VALUE']?></span>
                                        <? //} ?>-->
                                    </div>
                                </div>
                            </div>

                        <? } ?>

                    <? } else { ?>

                        <div style="padding: 25px 15px;">
                            <p class="no___count">В данный момент активных точек продаж нет</p>
                        </div>

                    <? } ?>

                </div>
            </div>

        </div>
    </div>
</section>


<script>
$(document).ready(function(){
    if ( $('#map_canvas').length ) {

        function getBoundsZoomLevel(bounds, mapDim) {
            var WORLD_DIM = { height: 256, width: 256 };
            var ZOOM_MAX = 21;
            function latRad(lat) {
                var sin = Math.sin(lat * Math.PI / 180);
                var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
                return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
            }
            function zoom(mapPx, worldPx, fraction) {
                return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
            }
            var ne = bounds.getNorthEast();
            var sw = bounds.getSouthWest();
            var latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI;
            var lngDiff = ne.lng() - sw.lng();
            var lngFraction = ((lngDiff < 0) ? (lngDiff + 1000) : lngDiff) / 1000;
            var latZoom = zoom(mapDim.height, WORLD_DIM.height, latFraction);
            var lngZoom = zoom(mapDim.width, WORLD_DIM.width, lngFraction);
            return Math.min(latZoom, lngZoom, ZOOM_MAX);
        }

        function createBoundsForMarkers(markers) {
            var bounds = new google.maps.LatLngBounds();
            $.each(markers, function(){    bounds.extend(this.getPosition());    });
            return bounds;
        }

        function initMap() {
            var $mapDiv = $('#map_canvas');
            var mapDim = {
                height: $mapDiv.height(),
                width: $mapDiv.width()
            }
            var markers = [];
            <? foreach( $arResult['MAP_POINTS'] as $shop_id => $shop ){
                $ar = explode(',', $shop['PROPERTY_GPS_VALUE']); ?>
                markers.push(
                    new google.maps.Marker({
                        position: new google.maps.LatLng(<?=$ar[0]?>, <?=$ar[1]?>),
                        title: '<?=$shop['ADDRESS']?>',
                        icon: '<?=SITE_TEMPLATE_PATH?>/images/marker.png'
                    })
                );
            <? } ?>
            var bounds = (markers.length > 0) ? createBoundsForMarkers(markers) : null;
            var zoom = (bounds) ? getBoundsZoomLevel(bounds, mapDim) : 0;
            if( markers.length == 1 ){
                zoom = 16;
            }
            var map = new google.maps.Map($mapDiv[0], {
                center: (bounds) ? bounds.getCenter() : new google.maps.LatLng(0, 0),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                zoom: zoom
            });
            $.each(markers, function() { this.setMap(map); });
        }


        // initMap /////////////////////////////
        initMap();
    }
})
</script>