<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('iblock');

$APPLICATION->SetPageProperty("title", 'Создание цен вручную');

$APPLICATION->AddChainItem('Товары', '/personal/goods/');
$APPLICATION->AddChainItem('Создание вручную', '/personal/add_prices/');


if( $arParams['IS_AJAX'] != 'Y' && $arParams['IS_LOAD'] != 'Y' ){
    unset($_SESSION['ADD_PRICES_PRODUCTS']);
}

$arResult['catalogView'] = $_SESSION['catalogView']?$_SESSION['catalogView']:'grid';


$arResult['PRODUCTS'] = array();
$arResult['MAX_CNT'] = 24;
$maxCNT = $arResult['MAX_CNT'];


// Разделы каталога 1 уровня
$arResult['CATALOG_SECTIONS'] = project\catalog::sections_1_level();

//////////////
$arResult['CURRENT_SECTION_ID'] = intval($arParams['form_data']['section_id'])>0?$arParams['form_data']['section_id']:false;


// Фильтр
$filter = Array(
    "IBLOCK_CODE" => project\catalog::CATALOG_IBLOCK_CODE,
    "ACTIVE" => "Y",
    "INCLUDE_SUBSECTIONS" => "Y"
);

// Фильтр по категории
if( intval($arResult['CURRENT_SECTION_ID']) > 0 ){
    $filter['SECTION_ID'] = $arResult['CURRENT_SECTION_ID'];
}



// Поиск
if( strlen($arParams['form_data']['q']) > 0 ){
    $q = strip_tags(trim($arParams['form_data']['q']));
    // Поиск по элементам инфоблока
    $search_ids = project\search::iblockElementsByQ($q);
    if( count($search_ids) > 0 ){
        $filter['ID'] = $search_ids;
    } else {
        $filter['ID'] = array(0);
    }
}


// Поля
$fields = Array( "ID" );


$hash_array = array(
    'filter_hash' => json_encode($filter),
    'fields_hash' => json_encode($fields),
    'stop_ids_hash' => json_encode(is_array($_POST['stop_ids'])?$_POST['stop_ids']:array())
);
$final_hash = md5(json_encode($hash_array));


if( $_SESSION['ADD_PRICES_PRODUCTS'][$final_hash] ){

    $arResult['PRODUCTS'] = $_SESSION['ADD_PRICES_PRODUCTS'][$final_hash];

} else {

    // Запрос товаров каталога
    $dbElements = \CIBlockElement::GetList(array("NAME" => "ASC", "NAME" => "ASC"), $filter, false, false, $fields);

    $cnt = 0;
    while ($element = $dbElements->GetNext()) {
        if (
            !is_array($_POST['stop_ids'])
            ||
            (
                is_array($_POST['stop_ids'])
                &&
                !in_array($element['ID'], $_POST['stop_ids'])
            )
        ){
            $cnt++;
            $el = array();
            if ($cnt <= $maxCNT){
                $el = tools\el::info($element['ID']);
            }

            $arResult['PRODUCTS'][$element['ID']] = $el;
        }
    }


    $_SESSION['ADD_PRICES_PRODUCTS'][$final_hash] = $arResult['PRODUCTS'];
}




$arResult['totalCNT'] = count($arResult['PRODUCTS']);

$arResult['PRODUCTS'] = array_slice($arResult['PRODUCTS'], 0, $arResult['MAX_CNT'] + 1);




$this->IncludeComponentTemplate();