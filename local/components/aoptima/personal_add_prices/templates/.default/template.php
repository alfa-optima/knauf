<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$maxCNT = $arResult['MAX_CNT'] - 1;

if( $arParams['IS_LOAD'] != 'Y' ){ ?>

    <form class="dealerAddPricesForm" onsubmit="return false" style="margin-bottom: 0;">

        <article class="block  block--top">
            <div class="block__wrapper">
                <h2 class="block__title  block__title--search">Создание товарных предложений</h2>
                <span class="block__text">Всего товаров: <span class="block__text--numberFound"><?=$arResult['totalCNT']?></span></span>
            </div>
        </article>

        <section class="searchBlock <? if( $arResult['catalogView'] == 'list' ){ echo 'listView'; } ?>">
            <div class="searchPanel">

                <h3 class="searchPanel__title">Найти продукты
                    <span class="searchPanel__toggle"><span class="sr-only">Скрыть/показать панель поиска</span>
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/arrow-left.png" alt="стрелка влево" class="searchPanel__toggle-image">
                    </span>
                </h3>

                <div class="searchPanel__inner">

                    <div class="searchPanel__keywords">

                        <input id="keywords" name="q" placeholder="Ключевые слова" type="text" value="<?=$arParams['form_data']['q']?>" autocomplete="off">

                        <!--
                        <input id="article" name="article" value="Артикул" type="text" onfocus="this.value=''">
                        -->

                    </div>

                    <ul class="searchPanel-nav">

                        <? foreach ( $arResult['CATALOG_SECTIONS'] as $section ){ ?>

                            <li class="searchPanel-nav__item <? if( $arResult['CURRENT_SECTION_ID'] == $section['ID'] ){ ?>searchPanel-nav__item--active<? } ?>">
                                <a style="cursor: pointer;" class="searchPanel-nav__item-link addPricesLKSection to___process" section_id="<?=$section['ID']?>"><?=$section['NAME']?></span></a>
                            </li>

                        <? } ?>

                    </ul>

                    <input type="hidden" name="section_id" value="<?=$arResult['CURRENT_SECTION_ID']?>">

                </div>
            </div>

            <div class="searchBlock__wrapper">

                <div class="view-switcher">
                    <span class="view-switcher__text">Режим просмотра</span>
                    <div class="view-switcher__buttons">
                        <a href="javascript:;" class="view-switcher__button  view-switcher__button--grid"><span class="sr-only">Сетка</span></a>
                        <a href="javascript:;" class="view-switcher__button  view-switcher__button--list"><span class="sr-only">Список</span></a>
                    </div>
                </div>

                <div class="searchBlock__content">

                    <h3 class="goods__title">Результаты поиска:</h3>

                    <? if( count($arResult['PRODUCTS']) > 0 ){ ?>

                        <ul class="searchBlock__items">

                            <? $cnt = 0;
                            foreach ( $arResult['PRODUCTS'] as $arItem ){ $cnt++;
                                if(
                                    intval($arItem['ID']) > 0
                                    &&
                                    $cnt <= $maxCNT
                                ){
                                    // catalog_item
                                    $APPLICATION->IncludeComponent(
                                        "aoptima:catalog_item", "catalog",
                                        array(
                                                'arItem' => $arItem,
                                                'isAddPrices' => 'Y',
                                                "IS_MOB_APP" => $arParams['IS_MOB_APP'],
                                        )
                                    );
                                }
                            } ?>

                        </ul>

                        <div class="more-button addPricesMoreButton to___process" <?  if( $cnt <= $maxCNT ){?>style="display:none"<? } ?>>
                            <span>Показать еще</span>
                        </div>

                    <? } else { ?>



                        <hr style="border: none; color: #dedede; background-color: #dedede; height: 1px; margin-bottom: 27px;">

                        <p class="lk___no_count">Товаров не найдено</p>

                    <? } ?>

                </div>

            </div>
        </section>


    </form>


<? // Подгрузка
} else if( $arParams['IS_LOAD'] == 'Y' ){ ?>


    <? $cnt = 0;
    foreach ( $arResult['PRODUCTS'] as $arItem ){ $cnt++;
        if(
            intval($arItem['ID']) > 0
            &&
            $cnt <= $maxCNT
        ){
            // catalog_item
            $APPLICATION->IncludeComponent(
                "aoptima:catalog_item", "catalog",
                array(
                        'arItem' => $arItem,
                        'isAddPrices' => 'Y',
                        "IS_MOB_APP" => $arParams['IS_MOB_APP'],
                )
            );
        } else {
            echo '<ost></ost>';
        }
    } ?>


<? } ?>
