<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

global $USER;
$userID = $USER->GetID();
$currentUser = new project\user( $USER->GetID() );
$userProps = $currentUser->arUser;
if(strlen($userProps["UF_MAIN_USER"]) > 0){
	$user = new project\user( $userProps["UF_MAIN_USER"] );
	if( $user->isDealer() ){
		$userID = $user->arUser["ID"];
	}
}
$dealer_shop = new project\dealer_shop();
$arResult['SHOPS'] = $dealer_shop->getList( $userID );
//echo '<pre>';
//print_r($arResult["SHOPS"]);
//echo '</pre>';
foreach ( $arResult['SHOPS'] as $shop_id => $shop ){
    $el = tools\el::info($shop_id);
    if( intval($el['PROPERTY_LOC_ID_VALUE']) > 0 ){
        $el['LOC'] = project\bx_location::getByID($el['PROPERTY_LOC_ID_VALUE']);
    }
    if( intval($el['PROPERTY_REGION_ID_VALUE']) > 0 ){
        $el['REGION'] = project\bx_location::getByID($el['PROPERTY_REGION_ID_VALUE']);
    }
    $arResult['SHOPS'][$shop_id] = $el;
}
//echo '<pre>';
//print_r($arResult["SHOPS"]);
//echo '</pre>';









$this->IncludeComponentTemplate();