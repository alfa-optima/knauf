<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>


<form class="dealerLKShopsForm" onsubmit="return true" method="post">

    <div class="dealerAccount-form__wrapper  dealerAccount-form__wrapper--address">

        <h4 class="tab__title">Адреса точек продаж</h4>

        <div class="dealerShopsArea">

            <? if( count($arResult['SHOPS']) > 0 ){

                foreach( $arResult['SHOPS'] as $shop_id => $shop ){

                    $APPLICATION->IncludeComponent(
                        "aoptima:dealerLKShopItem", "",
                        array(
                            'SHOP_ID' => $shop_id,
                            'SHOP' => $shop
                        )
                    );

                }

            } ?>

        </div>

        <span class="account-form__addAddress addDealerShopButton to___process">
            <span>Добавить адрес</span>
        </span>

        <p class="error___p"></p>

        <a style="cursor: pointer; <? if( count($arResult['SHOPS']) == 0 ){ ?>display:none;<? } ?>" class="account-form__button dealerShopsSave to___process">Сохранить данные</a>

    </div>
</form>



