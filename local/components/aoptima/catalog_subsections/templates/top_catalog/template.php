<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(
    is_array($arResult['subSects'])
    &&
    count($arResult['subSects']) > 0
){ ?>



    <div class="searchBlock__types">
        <div class="sliderBlock__tabs" <? if( count($arResult['subSects'])==1 ){ echo 'style="display:none;"'; } ?>>

            <dl class="dropdowns dropdown">
                <dt><a><span>Выберите раздел</span></a></dt>
                <dd>

                <ul class="tabs">

                    <? foreach( $arResult['subSects'] as $subSect ){ ?>



                        <li onclick="window.location.href = '<?=$subSect['SECTION_PAGE_URL']?>';" <?if(CSite::InDir($subSect['SECTION_PAGE_URL'])): ?>class="active"<?endif;?>>
                            <a style="cursor: pointer"><?=$subSect['NAME']?></a>
                        </li>


                    <? } ?>

                </ul>

                </dd>
            </dl>
            <script type="text/javascript">
                dropDownHandler();
            </script>
        </div>
    </div>

<? } ?>


