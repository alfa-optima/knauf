<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="searchBlock__content">
    <ul class="searchBlock__items">

        <? foreach( $arResult['subSects'] as $subSect ){ ?>

            <li class="searchBlock__item product-item">
                <div class="product-item__image">
                    <a href="<?=$subSect['SECTION_PAGE_URL']?>">
                        <img src="<?=$subSect['PICTURE_SRC']?>" alt="<?=$subSect['NAME']?>">
                    </a>
                </div>
                <div class="product-item__inner">
                    <h3 class="product-item__title"><a href="<?=$subSect['SECTION_PAGE_URL']?>" style="color:#484846"><?=$subSect['NAME']?></a></h3>
                    <a href="<?=$subSect['SECTION_PAGE_URL']?>" class="product-item__link">Подробнее</a>
                </div>
            </li>

        <? } ?>

    </ul>
</div>
