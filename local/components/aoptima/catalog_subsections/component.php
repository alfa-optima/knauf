<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult['subSects'] = $arParams['subSects'];

foreach($arResult['subSects'] as $key => $subSect){

    if( $arParams['IS_MOB_APP'] == 'Y' ){
        $arResult['subSects'][$key]['SECTION_PAGE_URL'] = project\catalog::toMobileAppUrl($subSect['SECTION_PAGE_URL']);
    } else {
        $arResult['subSects'][$key]['SECTION_PAGE_URL'] = project\catalog::toSiteUrl($subSect['SECTION_PAGE_URL']);
    }

    if( intval($subSect['PICTURE']) > 0 ){
        $arResult['subSects'][$key]['PICTURE_SRC'] = tools\funcs::rIMGG($subSect['PICTURE'], 5, 202, 151);
    } else {
        $arResult['subSects'][$key]['PICTURE_SRC'] = SITE_TEMPLATE_PATH."/images/no_image.png";
    }

}




$this->IncludeComponentTemplate();