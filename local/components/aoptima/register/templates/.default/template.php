<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>


<? if( $arResult['IS_AUTH'] == 'N' ){ ?>


    <!--<section class="bcgBlock  bcgBlock--indexTop" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/images/blockbcg3.jpg);">
        <div class="bcgBlock__wrapper">
            <h2 class="bcgBlock__title">Вход в&nbsp;личный кабинет</h2>
        </div>
    </section>-->

    <section class="block">
        <div class="block__wrapper" style="padding-top: 0px;">

            <div class="block__textBlock registerTitleBlock">
                <p class="block__text">Уже зарегистрированы? <a href="<?=PREFIX?>/auth/" class="block__link">Авторизуйтесь</a></p>
            </div>

            <form onsubmit="return false" method="post" class="form registerForm">

                <div class="account-form__block">

                    <h4 class="account-form__title">Информация о&nbsp;покупателе</h4>

                    <div class="account-form__wrapperMinusMargin">

                        <div class="account-form__field">
                            <span>Имя *</span>
                            <input type="text" name="NAME">
                        </div>

                        <div class="account-form__field">
                            <span>Фамилия *</span>
                            <input type="text" name="LAST_NAME">
                        </div>

                        <div class="account-form__field">
                            <span>Эл. почта *</span>
                            <input type="EMAIL" name="EMAIL">
                        </div>

<!--                        <div class="account-form__hideBlock">-->
<!---->
<!--                            <div class="account-form__field">-->
<!--                                <span>Дата рождения</span>-->
<!--                                <input class="datepicker" type="text" name="PERSONAL_BIRTHDAY" autocomplete="off">-->
<!--                            </div>-->
<!---->
<!--                            <div class="account-form__field">-->
<!--                                <span>Пол</span>-->
<!--                                <div class="account-form__checkboxes">-->
<!--                                    <div class="account-form__checkbox">-->
<!--                                        <input type="radio" id="reg_male" name="PERSONAL_GENDER" value="M">-->
<!--                                        <label for="reg_male">Муж</label>-->
<!--                                    </div>-->
<!--                                    <div class="account-form__checkbox">-->
<!--                                        <input type="radio" id="reg_female" name="PERSONAL_GENDER" value="F">-->
<!--                                        <label for="reg_female">Жен</label>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!---->
<!--                        </div>-->

<!--                        <div>-->
<!--                            <a style="cursor: pointer" class="account-form__moreBtn  tooltip--lime" title="Введите данные для регистрации.">Дополнительные данные</a>-->
<!--                        </div>-->

                    </div>

                </div>

                <div class="account-form__block">

                    <h4 class="account-form__title">Пароль</h4>

                    <div class="account-form__wrapperMinusMargin">

                        <div class="account-form__row">
                            <div class="account-form__field">
                                <span>Пароль *</span>
                                <input type="password" name="PASSWORD">
                            </div>
                            <div class="account-form__field">
                                <span>Подтвердить пароль *</span>
                                <input type="password" name="CONFIRM_PASSWORD">
                            </div>
                        </div>

                        <div class="account-form__field  account-form__field--w100">
                            <div class="account-form__checkbox">
                                <input type="checkbox" id="reg_agree" name="agree" value="Y">
                                <label for="reg_agree">Я принимаю условия <a href="<?=PREFIX?>/user_agreement/" target="_blank">Пользовательского соглашения</a> и даю своё согласие на обработку моей персональной информации на условиях, определенных <a href="https://www.knauf.ru/about/confidentiality/" target="_blank">Политикой конфиденциальности</a></label>
                            </div>
                        </div>

                        <div class="account-form__field  account-form__field--w100">
                            <div class="account-form__checkbox">
                                <input type="checkbox" id="reg_subscribe" name="subscribe" value="Y">
                                <label for="reg_subscribe">Да, я&nbsp;хотел&nbsp;бы получать информационные материалы, в&nbsp;личном кабинете вы&nbsp;сможете настроить подписку</label>
                            </div>
                        </div>

                        <p class="error___p"></p>

                        <div class="account-form__regBtn">

                            <a style="cursor: pointer;" class="account-form__button registerButton to___process">Зарегистрироваться</a>

                            <p class="account-form__note">Все поля отмеченные * обязательны для заполнения<br>Дополнительные данные вы&nbsp;можете внести в&nbsp;личном кабинете после регистрации</p>

                        </div>

                    </div>
                </div>

            </form>
        </div>
    </section>


<? } ?>