<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if( count($arResult['REVIEWS']) > 0 ){ ?>

    <? if( $arParams['IS_AJAX'] != 'Y' ){ ?>

        <table class="revTable">
            <tbody class="reviewsLoadTbody">

            <tr>
                <th>Покупатель</th>
                <th>Отзыв</th>
            </tr>

    <? } ?>

        <? $cnt = 0;
        foreach( $arResult['REVIEWS'] as $review ){ $cnt++; ?>

            <tr class="reviewItem" item_id="<?=$review['ID']?>">
                <td>
                    <div class="tab__review--mob">
                        <span>Покупатель</span>
                        <span class="tab__date"><?=ConvertDateTime($review['UF_DATE'], "DD.MM.YYYY HH:MI:SS", "ru")?></span>
                    </div>
                    <div><? if( $review['UF_USER_VOTE'] ){ ?><span><?=$review['UF_USER_VOTE']?></span><? } ?><?= strlen($review['UF_NAME'])>0?$review['UF_NAME']:'-'?></div>
                </td>
                <td>
                    <p><?=$review['UF_TEXT']?></p>
                    <span class="tab__date"><?=ConvertDateTime($review['UF_DATE'], "DD.MM.YYYY", "ru")?></span>
                </td>
            </tr>

        <? } ?>

    <? if( $arParams['IS_AJAX'] != 'Y' ){ ?>

        </tbody>
        </table>

        <div class="more-button reviewsMoreButton to___process" style="display: none;">
            <span></span>
        </div>

        <input type="hidden" name="total_reviews_cnt" value="<?=$arResult['TOTAL_REVIEWS_CNT']?>">
        <input type="hidden" name="reviews_tov_id" value="<?=$arParams['el_id']?>">

    <? } ?>

<? } else { ?>

    <? if( $arParams['IS_AJAX'] != 'Y' ){ ?>

        <div style="padding: 20px">
            <p class="no___count">Отзывов пока нет</p>
        </div>

    <? } ?>

<? } ?>