<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


// Перечень отзывов по товару
$review = new project\review();
$arResult['REVIEWS'] = $review->getList(
    $arParams['el_id'], false,
    false, $arParams['stop_items']
);
$arResult['TOTAL_REVIEWS_CNT'] = count($arResult['REVIEWS']);

$arResult['REVIEWS'] = array_slice($arResult['REVIEWS'], 0, project\review::DETAIL_LIST_CNT);















$this->IncludeComponentTemplate();