<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult = $arParams['~arResult'];

// Варианты списка по лифту
$arResult['LIFT_ENUMS'] = project\basket::getLiftEnums();




$this->IncludeComponentTemplate();