<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>


<section>
    <div>

        <form class="personal_delivery_form" onsubmit="return false">
            <h4 class="tab__title">Данные для доставки</h4>
            <div class="account-form__wrapperMinusMargin">

                <div class="account-form__field account-delivery-field">

                    <span>Адрес доставки по умолчанию</span>

                    <input
                        class="cityName account-form__suggestInput"
                        id="persDefaultAddress"
                        type="text"
                        process="N"
                        name="address"
                        addressName=""
                        value="<?=$arResult['USER_DEFAULT_DELIV_ADDRESS']['ADDRESS']?$arResult['USER_DEFAULT_DELIV_ADDRESS']['ADDRESS']:''?>"
                        onfocus="addressFocus($(this));"
                        onkeyup="addressKeyUp($(this), false, true);"
                        onfocusout="addressFocusOut($(this), '<?=$arResult['USER_DEFAULT_DELIV_ADDRESS']['COORDS']?>');"
                        autocomplete="off"
                    >

                    <input type="hidden" name="address_coords" value="<?=$arResult['USER_DEFAULT_DELIV_ADDRESS']['COORDS']?>">

                    <script>
                        $(document).ready(function(){
                            $('#persDefaultAddress').autocomplete({
                                source: '/ajax/searchAddress.php',
                                minLength: 2,
                                delay: 700,
                                select: addressSelect,
                                response: function(event, ui){}
                            })
                        })
                    </script>

                </div>

                <div style="clear:both"></div>

                <div class="account-form__field">
                    <span>№ подъезда по умолчанию</span>
                    <input
                        type="text"
                        name="UF_PODYEZD_DEFAULT"
                        value="<?=$arResult['USER']['UF_PODYEZD_DEFAULT']?$arResult['USER']['UF_PODYEZD_DEFAULT']:''?>"
                    >
                </div>

                <div class="account-form__field">
                    <span>№ этажа по умолчанию</span>
                    <input
                        type="text"
                        name="UF_FLOOR_DEFAULT"
                        value="<?=$arResult['USER']['UF_FLOOR_DEFAULT']?$arResult['USER']['UF_FLOOR_DEFAULT']:''?>"
                    >
                </div>

                <div class="account-form__field">
                    <span>№ кв./офиса по умолчанию</span>
                    <input
                        type="text"
                        name="UF_KV_DEFAULT"
                        value="<?=$arResult['USER']['UF_KV_DEFAULT']?$arResult['USER']['UF_KV_DEFAULT']:''?>"
                    >
                </div>

                <div style="clear:both"></div>

                <div class="account-form__field  account-form__field--w416  account-form__field--mr0">
                    <span>Наличие лифта по умолчанию</span>
                    <div class="account-form__checkboxes">

                        <? foreach( $arResult['LIFT_ENUMS'] as $enum ){ ?>

                            <div class="account-form__checkbox  account-form__checkbox--mr35">
                                <input class="liftCheckbox" <? if( $arResult['USER']['UF_LIFT_DEFAULT'] == $enum['VALUE']){ ?>checked<? } ?> type="radio" id="lift_<?=$enum['XML_ID']?>" name="UF_LIFT_DEFAULT" value="<?=$enum['VALUE']?>" lift_enum_id="<?=$enum['ID']?>">
                                <label for="lift_<?=$enum['XML_ID']?>"><?=$enum['VALUE']?></label>
                            </div>

                        <? } ?>

                    </div>
                </div>

                <div style="clear:both"></div>

                <p class="error___p"></p>

                <a style="cursor: pointer;" class="account-form__button personalDeliverySaveButton to___process">Сохранить</a>

            </div>

        </form>

    </div>
</section>