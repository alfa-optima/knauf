<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>


<div class="dealerAccount-form__wrapper dealerLoadMappingCSVFormBlock">

    <form class="dealerLoadMappingCSVForm" method="post" enctype="multipart/form-data" target="upload___Frame" action="/ajax/uploadDealerMappingCSVFile.php">

        <? if( count($arResult['articleMappingNotes']) > 0 ){ ?>

            <div class="dealerAccount-form__block">
                <h4 class="tab__title">В данный момент загружен файл соответствия (<?=count($arResult['articleMappingNotes'])?> <?=tools\funcs::pfCnt(count($arResult['articleMappingNotes']), 'строка', 'строки', 'строк')?>)</h4>

                <br>
                <a style="cursor: pointer;" class="account-form__button clearDealerMappingCSVButton to___process">Удалить загруженные соответствия</a>
                <br><br>
            </div>
            <hr>
            <br>

        <? } ?>

        <div class="dealerAccount-form__block">

            <h4 class="tab__title">Загрузка <? if( count($arResult['articleMappingNotes']) > 0 ){ ?>нового <? } ?>файла соответствия артикулов <!--<a href="/include/files/example.csv" class="account-form__table-goodsQuantity" target="_blank" download >(Скачать шаблон)</a>--></h4>
			
			 

			<div class="dealerAccount-form__field  dealerAccount-form__field--w271">

                <span>Файл CSV:</span>
                <div class="goods__top-upload">
                    <label>
                        <input type="file" name="csv" accept=".csv">
                        <span>Выбрать файл CSV</span>
                        <span>Выбрать</span>
                    </label>
                </div>
				<p>
					<a href="/include/files/example.csv" style="color: #009fe3; font-size: 14px;" class="account-form__table-goodsQuantity" target="_blank" download >(Скачать шаблон)</a>
				</p>


				
            </div>
			


            <p class="error___p"><?=count($arResult['ERRORS'])>0?implode('<br>', $arResult['ERRORS']):''?></p>

            <a style="cursor: pointer;" class="account-form__button loadDealerMappingCSVButton to___process">Загрузить</a>

        </div>

    </form>

</div>

