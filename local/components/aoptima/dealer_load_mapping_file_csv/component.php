<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


$arResult['articleMappingNotes'] = [];
$currentUser = new project\user();
$userProps = $currentUser->arUser;
if(strlen($userProps["UF_MAIN_USER"]) > 0){
	$user = new project\user( $userProps["UF_MAIN_USER"] );
}
else{
	$user = new project\user();
}
$dealer = tools\user::info( $user->arUser["ID"] );
if(
    isset($dealer['UF_ARTICLE_MAPPING'])
    &&
    strlen($dealer['UF_ARTICLE_MAPPING']) > 0
){
    $arResult['articleMappingNotes'] = tools\funcs::json_to_array($dealer['UF_ARTICLE_MAPPING']);
}






$this->IncludeComponentTemplate();