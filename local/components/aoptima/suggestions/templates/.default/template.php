<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<style>

.contact_info a
{
    font-family: Arial, Helvetica, sans-serif;
    font-size: 14px;
    font-weight: 500;
    line-height: 19px;

	 display: inline-block;

    color: #009fe3;
}

.contact_info a:hover
{
    color: #016b99;
}
</style>

<!--<section class="sliderBlock">

<h3 class="textBlockOverBcgBlock__title"><?// $APPLICATION->IncludeFile( "/inc/main_welcome_title.inc.php", Array(), Array("MODE"=>"html") ); ?></h3>
<p class="textBlockOverBcgBlock__text"><?// $APPLICATION->IncludeFile( "/inc/main_welcome_text.inc.php", Array(), Array("MODE"=>"html") ); ?></p>


</section>-->



<section class="block  block--accountForm">
		<!--<div class="contact_info">	
			<p>Поддержка Маркетплейс "Купи КНАУФ"</p>
			<p>Телефон: <a href="tel:+74994042043">+7 (499) 404-20-43</a></p>
			<p>Эл. почта:  <a href="mailto:support@kupi-knauf.ru">support@kupi-knauf.ru</a></p>


		</div>-->
        <section class="suggestions">
			 <form onsubmit="return false;">

                <div class="account-form__wrapperMinusMargin">

                    <h4 class="tab__title">Пожалуйста, укажите Ваши данные</h4>

                    <div class="account-form__row">

                        <div class="account-form__field">
                            <span>Ваш населённый пункт</span>
                            <div class="styledSelect">
                                <input
                                    autocomplete="off"
                                    type="text"
                                    name="city"
                                 >
                            </div>
                        </div>

                    </div>


                    <div class="account-form__row">
                        <div class="account-form__field">
                            <span>Тема обращения</span>
                            <div class="styledSelect">
                                <select id="suggestion_theme_select" class="suggestion_theme_select" name="theme">
                                    <option selected value=""></option>
                                    <?
                                    foreach($arResult["THEMES"] as $arTheme)
                                    {
                                        ?>
                                        <option value="<?=$arTheme["ID"]?>"><?=$arTheme["NAME"]?></option>
                                        <?
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="account-form__wrapperMinusMargin">
                    <div class="account-form__row">

                        <div class="account-form__field">
                            <span>Имя</span>
                            <input type="text" name="name" value="<?=$arResult['USER']['NAME']?>">
                        </div>

                        <div class="account-form__field">
                            <span>Фамилия</span>
                            <input type="text" name="last_name" value="<?=$arResult['USER']['LAST_NAME']?>">
                        </div>

                        <div class="account-form__field ">
                            <span>Эл. почта</span>
                            <input type="text" name="email" value="<?=$arResult['USER']['EMAIL']?>">
                        </div>

                        <div class="account-form__field ">
                            <span>Телефон</span>
                            <input type="text" name="phone" value="<?=$arResult['USER']['PERSONAL_PHONE']?>">
                        </div>

                    </div>
                </div>

                <div class="account-form__wrapperMinusMargin">

                    <h4 class="tab__title">Вопрос, замечание, предложение</h4>

                    <div class="account-form__row">
                        <div class="account-form__field  account-form__field--w760">
                            <div class="account-form__textarea-counter">Оставшиеся символы: <span>300</span></div>
                            <textarea maxlength="300" name="message"></textarea>
                        </div>
                    </div>

                    <div class="account-form__row">
                        <div class="account-form__field">
                            <div class="account-form__checkbox">
                                <input type="checkbox" id="agree" name="agree" value="1">
                                <label for="agree">Я&nbsp;прочитал и&nbsp;принял условия <span title='Политика общества с ограниченной ответственностью "КНАУФ ГИПС" в отношении обработки персональных данных и сведения о реализуемых требованиях к защите персональных данных.' class="dealerAccount-form__tooltip tooltipstered"><a href="https://www.knauf.ru/about/confidentiality/" target="_blank">конфиденциальности данных</a></span> и&nbsp;даю согласие на&nbsp;обработку своих персональных данных</label>
                            </div>
                        </div>
                    </div>
                    <div class="account-form__row">
                        <div class="account-form__field">
                            <div class="account-form__checkbox">
                                <input type="checkbox" id="agree1" name="agree1" value="1">
                                <label for="agree1">Я&nbsp;принимаю условия <a href="<?=PREFIX?>/user_agreement/" target="_blank">Пользовательского соглашения</a></label>
                            </div>
                        </div>
                    </div>
                    <div class="account-form__row">
                        <div class="account-form__field">
                            <div class="account-form__checkbox">
                                <input type="checkbox" id="agree2" name="agree2" value="1">
                                <label for="agree2">Подписаться на рассылку информационных материалов</label>
                            </div>
                        </div>
                    </div>


                    <p class="error___p"></p>

                    <div class="account-form__buttonWrapper">
                        <a style="cursor: pointer;" class="account-form__button addSuggestionButton to___process">Отправить</a>
                        <span>Все поля обязательны для заполнения</span>
                    </div>

                </div>

            </form>
        </section>
</section>