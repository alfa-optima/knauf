<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


if( $USER->IsAuthorized() ){

    $arResult['USER'] = tools\user::info( $USER->GetID() );



}

// получим типы обращения
$arResult["THEMES"] = Array();
$dbThemes = CIBlockElement::GetList(
    Array(
        "SORT" => "ASC",
        "ID" => "ASC"
    ),
    Array(
        "IBLOCK_ID" => 18,
        "ACTIVE" => "Y"
    ),
    false,
    false,
    Array(
        "ID",
        "NAME"
    )
);
while($arTheme = $dbThemes->Fetch())
{
    $arResult["THEMES"][] = $arTheme;
}










$this->IncludeComponentTemplate();