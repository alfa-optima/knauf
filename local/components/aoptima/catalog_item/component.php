<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult = $arParams['arItem'];

$arResult = project\catalog::updateProductName($arResult);

$arResult['PRICE_MIN'] = false;   $arResult['PRICE_MAX'] = false;

$arResult['SHOW_BASKET_BUTTON'] = false;

/*echo '<pre>111111';
print_r ($arParams['LOC']);
echo '</pre>';*/

foreach ($arResult['OFFERS'] as $key => $offer){
    if(
        intval($arParams['LOC']['REGION_ID']) > 0
        &&
        $arParams['LOC']['REGION_ID'] != $offer['PROPERTIES']['LOCATION']['VALUE']
    ){   unset($arResult['OFFERS'][$key]);   }
}

/////
foreach ($arResult['OFFERS'] as $key => $offer){
    if(
        $arResult['OFFERS'][$key]['PRODUCT']['QUANTITY'] == 0
    ){   unset($arResult['OFFERS'][$key]);   }
}
//////


if(
    is_array($arResult['OFFERS'])
    &&
    count($arResult['OFFERS']) > 0
    &&
    $arParams['IS_DEALER'] != 'Y'
){

    foreach ($arResult['OFFERS'] as $offer){
        if( $offer['PRICES']['BASE']['ROUND_VALUE_VAT'] ){
            if(
                !$arResult['PRICE_MIN']
                ||
                (
                    $arResult['PRICE_MIN']
                    &&
                    $offer['PRICES']['BASE']['ROUND_VALUE_VAT'] <  $arResult['PRICE_MIN']
                )
            ){    $arResult['PRICE_MIN'] = $offer['PRICES']['BASE']['ROUND_VALUE_VAT'];    }
            if(
                !$arResult['PRICE_MAX']
                ||
                (
                    $arResult['PRICE_MAX']
                    &&
                    $offer['PRICES']['BASE']['ROUND_VALUE_VAT'] > $arResult['PRICE_MAX']
                )
            ){    $arResult['PRICE_MAX'] = $offer['PRICES']['BASE']['ROUND_VALUE_VAT'];    }
        }
    }

    $arResult['SHOW_BASKET_BUTTON'] = true;
}

$this->IncludeComponentTemplate();