<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if( $arParams['IS_MOB_APP'] == 'Y' ){
    $arResult['DETAIL_PAGE_URL'] = project\catalog::toMobileAppUrl($arResult['DETAIL_PAGE_URL']);
} ?>

<div data-id="<?=$arResult['ID']?>" class="productItem slider__item <? if( $arResult['PROPERTIES']['ACTION']['VALUE'] ){ echo 'slider__item--sale'; } ?>">

    <div class="slider__item-image">
        <a href="<?=$arResult['DETAIL_PAGE_URL']?>">
            <? if( intval($arResult['DETAIL_PICTURE']['ID']) > 0 ){ ?>
                 <img alt="<?=$arResult['NAME']?>" src="<?=tools\funcs::rIMGG($arResult['DETAIL_PICTURE']['ID'], 5, 202, 151)?>">
            <? } else { ?>
                <img alt="<?=$arResult['NAME']?>" src="<?=SITE_TEMPLATE_PATH?>/images/no_image.png">
            <? } ?>
        </a>
    </div>

    <div class="slider__item-inner">

        <a class="slider__item-titleProduct" href="<?=$arResult['DETAIL_PAGE_URL']?>">
            <h3><?=$arResult['NAME']?></h3>
        </a>

        <? if( $arResult['PRICE_MIN'] ){ ?>
            <span class="slider__item-fromPrice">от <?=number_format($arResult['PRICE_MIN'], project\catalog::ROUND, ",", " ")?>
                <span class="rub">₽</span>
            </span>
        <? } ?>

        <? if(
                $arResult['PRICE_MAX']
                &&
                $arResult['PRICE_MAX'] != $arResult['PRICE_MIN']
        ){ ?>
            <span class="slider__item-toPrice">до <?=number_format($arResult['PRICE_MAX'], project\catalog::ROUND, ",", " ")?>
                <span class="rub">₽</span>
            </span>
        <? } ?>

        <? if( $arResult['SHOW_BASKET_BUTTON'] ){ ?>
            <a class="slider__item-cartLink basketButton to___process" style="cursor:pointer" item_id="<?=$arResult['OFFERS'][ array_keys($arResult['OFFERS'])[0] ]['ID']?>">В корзину</a>

            <div class="slider__item-counter catalog-list-inbask" style="display: none;">
                <div class="slider__item-counterTop">
                    <div class="cart__goodsTable-counter  cart__goodsTable-counter--slider">
                        <button type="button" class="cart__goodsTable-counterBut dec listbasketMinus">-</button>
                        <input
                                type="text"
                                name="quantity"
                                class="field listfieldCount"
                                value="1"
                                data-min="1"
                                data-max="9999"
                                item_id="<?=$arResult['OFFERS'][ array_keys($arResult['OFFERS'])[0] ]['ID']?>"
                        >
                        <button type="button" class="cart__goodsTable-counterBut inc listbasketPlus">+</button>
                    </div>
                    <?
                    if($arResult["PROPERTIES"]["PRODUCT_TYPE"]["VALUE_XML_ID"] == "list")
                    {
                        $sizeOne = ($arResult["PROPERTIES"]["LENGTH"]["VALUE"]/1000) * ($arResult["PROPERTIES"]["WIDTH"]["VALUE"]/1000);
                        ?>
                        <span class="slider__item-counterDimension" data-size="<?=$sizeOne?>"><span><?=$sizeOne?></span>&nbsp;м<sup>2</sup></span>
                        <?
                    }
                    elseif($arResult["PROPERTIES"]["PRODUCT_TYPE"]["VALUE_XML_ID"] == "profile")
                    {
                        $sizeOne = $arResult["PROPERTIES"]["LENGTH"]["VALUE"]/1000;
                        ?>
                        <span class="slider__item-counterDimension" data-size="<?=$sizeOne?>"><span><?=$sizeOne?></span>&nbsp;м.п.</span>
                        <?
                    }?>
                </div>
                <!--<span class="slider__item-counterText">В корзине, выберите<br> количество товара</span>-->
            </div>
        <? } ?>

        <? if(
            $arParams['isAddPrices'] == 'Y'
            ||
            $arParams['IS_DEALER'] == 'Y'
        ){ ?>
            <a class="slider__item-cartLink addPriceOpenWindow to___process" style="cursor: pointer;" item_id="<?=$arResult['ID']?>">Добавить</a>
        <? } ?>

        <? if(
            count($arResult['OFFERS']) == 0
            &&
            $arParams['IS_DEALER'] != 'Y'
            &&
            $arParams['isAddPrices'] != 'Y'
        ){ ?>
		
			 <?if($arResult['PROPERTIES']['DELIVERY_DATE']['VALUE'])
                {
                    ?>
                    <p class="product-item__note  product-item__note--warning">
                        <?=$arResult['PROPERTIES']['DELIVERY_DATE']['VALUE']?>
                    </p>
                    <?
                }?>
		
		
            <?
            $sizeOne = "";
            $sizeName = "";
            if($arResult["PROPERTIES"]["PRODUCT_TYPE"]["VALUE_XML_ID"] == "list")
            {
                $sizeOne = ($arResult["PROPERTIES"]["LENGTH"]["VALUE"]/1000) * ($arResult["PROPERTIES"]["WIDTH"]["VALUE"]/1000);
                $sizeName = "<span>Кол-во, м<sup>2</sup></span>";
            }
            elseif($arResult["PROPERTIES"]["PRODUCT_TYPE"]["VALUE_XML_ID"] == "profile")
            {
                $sizeOne = $arResult["PROPERTIES"]["LENGTH"]["VALUE"]/1000;
                $sizeName = "<span>Кол-во, м.п.</span>";
            }
            ?>
            <a class="product-item__cartLink underOrderButton to___process"
               style="cursor:pointer"
               item_id="<?=$arResult['ID']?>"
               data-name="<?=$arResult['NAME']?>"
               data-pic="<?=tools\funcs::rIMGG($arResult['DETAIL_PICTURE']['ID'], 4, 204, 153)?>"
               data-size_one="<?=$sizeOne?>"
               data-size_name="<?=$sizeName?>"
               data-url="<?=$arResult["DETAIL_PAGE_URL"]?>"
            >Под заказ</a>
        <? } ?>

    </div>
</div>
