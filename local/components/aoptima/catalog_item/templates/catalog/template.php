<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if( $arParams['IS_MOB_APP'] == 'Y' ){
    $arResult['DETAIL_PAGE_URL'] = project\catalog::toMobileAppUrl($arResult['DETAIL_PAGE_URL']);
} ?>

<li data-id="<?=$arResult['ID']?>" class="productItem searchBlock__item product-item catalog___item <? if( $arResult['PROPERTIES']['ACTION']['VALUE'] ){ echo 'product-item--sale'; } ?>" item_id="<?=$arResult['ID']?>">

    <div class="product-item__grid" rel="z-0001">

        <div class="product-item__image">
            <a href="<?=$arResult['DETAIL_PAGE_URL']?>">
                <? if( is_array($arResult['DETAIL_PICTURE']) > 0 ){
                    $pic_id = $arResult['DETAIL_PICTURE']['ID'];
                } else if( intval($arResult['DETAIL_PICTURE']) > 0 ){
                    $pic_id = $arResult['DETAIL_PICTURE'];
                }
                if( intval($pic_id) > 0 ){ ?>
                    <img pic_id="<?=$pic_id?>" alt="<?=$arResult['NAME']?>" src="<?=tools\funcs::rIMGG($pic_id, 5, 202, 151)?>">
                <? } ?>
            </a>
        </div>

        <div class="product-item__inner">

            <a class="product-item__titleProduct" href="<?=$arResult['DETAIL_PAGE_URL']?>">
                <h3><?=$arResult['NAME']?></h3>
            </a>

            <? if( $arResult['PRICE_MIN'] ){ ?>
                <span class="product-item__fromPrice">от <?=number_format($arResult['PRICE_MIN'], project\catalog::ROUND, ",", " ")?>
                    <span class="rub">₽</span>
                </span>
            <? }

            if(
                $arResult['PRICE_MAX']
                &&
                $arResult['PRICE_MAX'] != $arResult['PRICE_MIN']
            ){ ?>
                <span class="product-item__toPrice">до <?=number_format($arResult['PRICE_MAX'], project\catalog::ROUND, ",", " ")?>
                    <span class="rub">₽</span>
                </span>
            <? }?>
			
			<? /*if( count($arResult['OFFERS']) == 0 ):
				$arTPs = project\tp::getList($arResult["ID"]);
				$av_price = 0;
				if($arTPs)
				{
					foreach ($arTPs as $arTP)
					{
						$av_price += $arTP["CATALOG_PRICE_" . project\catalog::PRICE_ID];
					}
					$av_price = ceil($av_price / count($arTPs));
				}
				?>
				
				<?if ($av_price>0):?>
					 <span class="product-item__fromPrice">
						~<?echo  number_format($av_price, 0, '', ' ');?>
						<span class="rub">₽</span>
					</span>
					<span class="product-item__toPrice">
						*Средняя цена по России
					</span>
				<?endif;?>
			<?endif;*/?>
			

            <?if( $arResult['SHOW_BASKET_BUTTON'] ){ ?>

                <a class="product-item__cartLink basketButton to___process" style="cursor:pointer" item_id="<?=$arResult['OFFERS'][ array_keys($arResult['OFFERS'])[0] ]['ID']?>">В корзину</a>

                <div class="slider__item-counter catalog-list-inbask" style="display: none;">
                    <div class="slider__item-counterTop">
                        <div class="cart__goodsTable-counter  cart__goodsTable-counter--slider">
                            <button type="button" class="cart__goodsTable-counterBut dec listbasketMinus">-</button>
                            <input
                                    type="text"
                                    name="quantity"
                                    class="field listfieldCount"
                                    value="1"
                                    data-min="1"
                                    data-max="9999"
                                    item_id="<?=$arResult['OFFERS'][ array_keys($arResult['OFFERS'])[0] ]['ID']?>"
                            >
                            <button type="button" class="cart__goodsTable-counterBut inc listbasketPlus">+</button>
                        </div>
                        <? if($arResult["PROPERTIES"]["PRODUCT_TYPE"]["VALUE_XML_ID"] == "list"){
                            $sizeOne = ($arResult["PROPERTIES"]["LENGTH"]["VALUE"]/1000) * ($arResult["PROPERTIES"]["WIDTH"]["VALUE"]/1000);
                            ?>
                            <span class="slider__item-counterDimension" data-size="<?=$sizeOne?>"><span><?=$sizeOne?></span>&nbsp;м<sup>2</sup></span>
                            <?
                        } elseif($arResult["PROPERTIES"]["PRODUCT_TYPE"]["VALUE_XML_ID"] == "profile"){
                            $sizeOne = $arResult["PROPERTIES"]["LENGTH"]["VALUE"]/1000;
                            ?>
                            <span class="slider__item-counterDimension" data-size="<?=$sizeOne?>"><span><?=$sizeOne?></span>&nbsp;м.п.</span>
                            <?
                        } ?>
                    </div>
                    <!--<span class="slider__item-counterText">В корзине, выберите<br> количество товара</span>-->
                </div>
            <? }

            if(
                $arParams['isAddPrices'] == 'Y'
                ||
                $arParams['IS_DEALER'] == 'Y'
            ){ ?>
                <a class="product-item__cartLink addPriceOpenWindow to___process" style="cursor: pointer;" item_id="<?=$arResult['ID']?>">Добавить</a>
            <? }

            if(
                count($arResult['OFFERS']) == 0
                &&
                $arParams['IS_DEALER'] != 'Y'
                &&
                $arParams['isAddPrices'] != 'Y'
            ){
                if($arResult['PROPERTIES']['DELIVERY_DATE']['VALUE'])
                {
                    ?>
                    <p class="product-item__note  product-item__note--warning">
                        <?=$arResult['PROPERTIES']['DELIVERY_DATE']['VALUE']?>
                    </p>
                    <?
                }?>

                <?
                $sizeOne = "";
                $sizeName = "";
                if($arResult["PROPERTIES"]["PRODUCT_TYPE"]["VALUE_XML_ID"] == "list")
                {
                    $sizeOne = ($arResult["PROPERTIES"]["LENGTH"]["VALUE"]/1000) * ($arResult["PROPERTIES"]["WIDTH"]["VALUE"]/1000);
                    $sizeName = "<span>Кол-во, м<sup>2</sup></span>";
                }
                elseif($arResult["PROPERTIES"]["PRODUCT_TYPE"]["VALUE_XML_ID"] == "profile")
                {
                    $sizeOne = $arResult["PROPERTIES"]["LENGTH"]["VALUE"]/1000;
                    $sizeName = "<span>Кол-во, м.п.</span>";
                }
                ?>
                <a class="product-item__cartLink underOrderButton to___process"
                   style="cursor:pointer"
                   item_id="<?=$arResult['ID']?>"
                   data-name="<?=$arResult['NAME']?>"
                   data-pic="<?=tools\funcs::rIMGG($pic_id, 4, 204, 153)?>"
                   data-size_one="<?=$sizeOne?>"
                   data-size_name="<?=$sizeName?>"
                   data-url="<?=$arResult["DETAIL_PAGE_URL"]?>"
                   data-kratnost="<?=$arResult['PROPERTIES']['KRATNOST']['VALUE'];?>"
                   data-dl="<?
                   if(strlen($arResult['PROPERTIES']['LENGTH']['VALUE'])>0) {
                       echo str_replace(",",".",$arResult['PROPERTIES']['LENGTH']['VALUE']);
                   }else {
                       echo "0";
                   }
                   ?>"
                    data-sh="<?
                   if(strlen($arResult['PROPERTIES']['LENGTH']['VALUE'])>0) {
                       echo str_replace(",",".",$arResult['PROPERTIES']['WIDTH']['VALUE']);
                    }else{
                       echo "0";
                    }
                   ?>"
                >Под заказ</a>
            <? }?>

        </div>

    </div>

    <div class="product-item__list">

        <h3 class="product-item__largeTitle">
            <a href="<?=$arResult['DETAIL_PAGE_URL']?>" style="color:#484846"><?=$arResult['NAME']?></a>
        </h3>
        <p class="product-item__desc"><?=strip_tags(html_entity_decode($arResult['DETAIL_TEXT']))?></p>

        <a href="<?=$arResult['DETAIL_PAGE_URL']?>" class="product-item__linkTitle"><?=$arResult['NAME']?></a>
    </div>

</li>