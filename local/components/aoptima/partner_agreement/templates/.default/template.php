<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if( count($arResult['LIST']) > 0 ){

    if( intval($_GET['id']) > 0 && !$arResult['LIST'][intval($_GET['id'])] ){

        include($_SERVER["DOCUMENT_ROOT"]."/include/404include.php");

    } else {

        $cnt = 0;
        foreach ( $arResult['LIST'] as $key => $el ){ $cnt++;
            if(
                ( $cnt == 1 && !$_GET['id'] )
                ||
                ( intval($_GET['id']) > 0 && $el['ID'] == intval($_GET['id']) )
            ){

                echo $el['DETAIL_TEXT'];

                echo '<p style="text-align: right;"><b>Дата: '.ConvertDateTime($el['PROPERTY_DATE_VALUE'], "DD.MM.YYYY", "ru").'</b></p>';

                unset($arResult['LIST'][$key]);

            }
        }

        if( count($arResult['LIST']) > 0 ){
            if( intval($_GET['id']) > 0 ){
                echo '<p>Другие варианты соглашений:</p>';
            } else {
                echo '<p>Предыдущие варианты соглашений:</p>';
            }
            echo '<p>';
                $cnt = 0;
                foreach ( $arResult['LIST'] as $key => $el ){ $cnt++;
                    if( $cnt != 1 ){
                        echo ', ';
                    }
                    echo '<a href="?id='.$el['ID'].'">'.ConvertDateTime($el['PROPERTY_DATE_VALUE'], "DD.MM.YYYY", "ru").'</a>';
                }
            echo '</p>';
        }

    }
}
