<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('iblock');

$arResult['IS_AUTH'] = $USER->IsAuthorized()?'Y':'N';
$arResult['IS_ADMIN'] = $USER->IsAdmin()?'Y':'N';

if( $USER->IsAuthorized() ){

    // Инфо о пользователе
	if( $arParams['isSubDealer'] == 'Y' ){
		$arResult['SUB_USER'] = tools\user::info($USER->GetID());
		$rsDirectories = \CUserFieldEnum::GetList(array(), array(
			"ID" => $arResult["SUB_USER"]["UF_SUBUSER_DIRECTORY"],
		));
		while($arDirectories = $rsDirectories->GetNext()){
			$arResult["DIRECTORIES"][$arDirectories['XML_ID']] = $arDirectories;
		}
		$dillerUser = $arResult['SUB_USER']["UF_MAIN_USER"]; //ID основного диллера
		$arResult['USER'] = tools\user::info($dillerUser);
		$user_groups = CUser::GetUserGroup($dillerUser);
		$arResult['IS_SOC_USER'] =  $arResult['SUB_USER']['EXTERNAL_AUTH_ID']=='socservices'?'Y':'N';

	}
	else{
		$arResult['USER'] = tools\user::info($USER->GetID());
		$user_groups = CUser::GetUserGroup($USER->GetID());
		$arResult['IS_SOC_USER'] =  $arResult['USER']['EXTERNAL_AUTH_ID']=='socservices'?'Y':'N';
	}

    $arResult['IS_CALL_CENTER_USER'] =  in_array( project\user::CALL_CENTER_GROUP_ID, $user_groups )?'Y':'N';

    // Обычный пользователь
    if( $arParams['isDealer'] == 'N' ) {

		// Адрес доставки по умолчанию
		$arResult['USER_DEFAULT_DELIV_ADDRESS'] = [];
		if (strlen($arResult['USER']['UF_DEFAULT_ADDRESS']) > 0) {
			$arResult['USER_DEFAULT_DELIV_ADDRESS'] = tools\funcs::json_to_array($arResult['USER']['UF_DEFAULT_ADDRESS']);
		}


    // ДИЛЕР
    } else if( $arParams['isDealer'] == 'Y' ){



        // Виды деятельности
        $activity_type = new project\activity_type();
        $arResult['TYPES_OF_ACTIVITY'] = $activity_type->getList();


        $arResult['POST_CITY_LOC'] = false;
        if( intval($arResult['USER']['UF_POST_CITY_ID']) > 0 ){
            $loc = project\bx_location::getByID($arResult['USER']['UF_POST_CITY_ID']);
            if( intval($loc['ID']) > 0 ){
                $arResult['POST_CITY_LOC'] = $loc;
            }
        }


        $arResult['POST_REGION_LOC'] = false;
        if( intval($arResult['USER']['UF_POST_REGION_ID']) > 0 ){
            $loc = project\bx_location::getByID($arResult['USER']['UF_POST_REGION_ID']);
            if( intval($loc['ID']) > 0 ){   $arResult['POST_REGION_LOC'] = $loc;   }
        }

	}



}


/*global $USER;
if ($USER->IsAdmin()):
	echo '<pre>'; print_r($arResult['USER']); echo '</pre>';
endif;*/


$this->IncludeComponentTemplate();