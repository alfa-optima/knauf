<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<section class="block  block--account">
    <div class="block__wrapper">
        <h1 class="block__title"><?$APPLICATION->ShowTitle()?></h1>
    </div>
</section>

<? if( $arResult['IS_AUTH'] == 'Y' ){ ?>

    <section class="block  block--accountForm">
        <div class="block__wrapper">
            <section class="account-form">
                <div class="account-form__tabs">



<dl class="dropdowns dropdown">
    <dt><a><span>Личные данные</span></a></dt>
    <dd>
        <ul>
            <li class='account-form__tab  active'>
                <a href="#tab_111_1" id="tabLink_111_1">Личные данные</a>
            </li>
            <li class="account-form__tab">
                <a href="#tab_111_2" id="tabLink_111_2">Доставка</a>
            </li>
            <li class="account-form__tab">
                <a href="#tab_111_4" id="tabLink_111_4">История заказов</a>
            </li>
            <li class="account-form__tab">
                <a href="#tab_111_5" id="tabLink_111_5">Подписка</a>
            </li>
        </ul>
    </dd>
</dl>



<script type="text/javascript">
    dropDownHandler();
</script>



<article class="tab first" id="tab_111_1">

    <? // personalDataForm
    $APPLICATION->IncludeComponent(
        "aoptima:personalDataForm", "",
        array('arResult' => $arResult)
    ); ?>

</article>



<!--- Настройки доставки --->
<article class="tab" id="tab_111_2">

    <? // personalDeliverySettings
    $APPLICATION->IncludeComponent(
        "aoptima:personalDeliverySettings", "",
        array('arResult' => $arResult)
    ); ?>

</article>


        <?php if( 0 ){ ?>

            <!--- Адреса доставки (шаблоны) --->
            <article class="tab" id="tab_111_2">

                <? // personalDeliveryTemplates
                $APPLICATION->IncludeComponent(
                    "aoptima:personalDeliveryTemplates", ""
                ); ?>

            </article>

        <?php } ?>



<article class="tab" id="tab_111_4">

    <? // История заказов
    $APPLICATION->IncludeComponent(
        "aoptima:personalOrderHistoryBlock", "",
        [
            'USER_ID' => $arResult['USER']['ID'],
            'IS_MOB_APP' => $arParams['IS_MOB_APP']
        ]
    ); ?>

</article>



<article class="tab" id="tab_111_5">

    <? // subscribe_block
    $APPLICATION->IncludeComponent(
        "aoptima:subscribe_block", "lk"
    ); ?>

</article>



                </div>
            </section>
        </div>
    </section>



<? } else {

    include $_SERVER['DOCUMENT_ROOT'].'/include/auth.php';

} ?>
