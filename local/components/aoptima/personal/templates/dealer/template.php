<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<section class="block  block--account">
    <div class="block__wrapper">
        <h1 class="block__title"><?$APPLICATION->ShowTitle()?></h1>
    </div>
</section>


<? if( $arResult['IS_AUTH'] == 'Y' ){ ?>


    <section class="block  block--accountForm">
        <div class="block__wrapper">
            <section class="dealerAccount-form">
                <div class="account-form__tabs">

                    <dl class="dropdowns dropdown">
                        <dt><a><span>О компании</span></a></dt>
                        <dd>
                            <ul>
								<?//если у аккаунта доступ только к обработке заказов
								//if (!CSite::InGroup (array(12))):?>
								<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["about"]) && !empty($arResult["DIRECTORIES"]["about"])) || $arParams['isSubDealer'] != 'Y'):?>
									<li class='account-form__tab  <?if (($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["about"])) || $arParams['isSubDealer'] != 'Y'):?>active<?endif;?>'>
										<a href="#tab_111_1" id="tabLink_111_1">О компании</a>
									</li>
								<?endif;?>
								<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["delivery"]) && !empty($arResult["DIRECTORIES"]["delivery"])) || $arParams['isSubDealer'] != 'Y'):?>
									<li class="account-form__tab">
										<a href="#tab_111_2" id="tabLink_111_2">Доставка</a>
									</li>
								<?endif;?>
								<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["payitem"]) && !empty($arResult["DIRECTORIES"]["payitem"])) || $arParams['isSubDealer'] != 'Y'):?>
									<li class="account-form__tab">
										<a href="#tab_111_3" id="tabLink_111_3">Оплата</a>
									</li>
								<?endif;?>
								<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["salepoint"]) && !empty($arResult["DIRECTORIES"]["salepoint"])) || $arParams['isSubDealer'] != 'Y'):?>
									<li class="account-form__tab">
										<a href="#tab_111_4" id="tabLink_111_4" class="points_tab">Точки продаж</a>
									</li>
								<?endif;?>
								<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["orders"]) && !empty($arResult["DIRECTORIES"]["orders"])) || $arParams['isSubDealer'] != 'Y'):?>
	                                <li class="account-form__tab <?if ($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["orders"]) && !isset($arResult["DIRECTORIES"]["about"])):?>active<?endif;?> <?if (CSite::InGroup (array(12))):?>active<?endif;?>">
	                                    <a href="#tab_111_5" id="tabLink_111_5">Заказы</a>
	                                </li>
								<?endif;?>
								
								
								<?//если у аккаунта доступ только к обработке заказов
								//if (!CSite::InGroup (array(12))):?>
                                <?
                                global $USER;
                                // fixme
                                //if($USER->GetID() == 17)
                                //{
                                    ?>
									<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["rialto"]) && !empty($arResult["DIRECTORIES"]["rialto"])) || $arParams['isSubDealer'] != 'Y'):?>
	                                    <li class="account-form__tab">
	                                        <a href="#tab_111_10" id="tabLink_111_10">Биржа</a>
	                                    </li>
									<?endif;?>
                                    <?
                                //}?>
								<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["reviews"]) && !empty($arResult["DIRECTORIES"]["reviews"])) || $arParams['isSubDealer'] != 'Y'):?>
									<li class="account-form__tab">
										<a href="#tab_111_6" id="tabLink_111_6">Отзывы</a>
									</li>
								<?endif;?>
								<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["autoupdate"]) && !empty($arResult["DIRECTORIES"]["autoupdate"])) || $arParams['isSubDealer'] != 'Y'):?>
									<li class="account-form__tab">
										<a href="#tab_111_7" id="tabLink_111_7">Автообновление</a>
									</li>
								<?endif;?>
								<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["articles"]) && !empty($arResult["DIRECTORIES"]["articles"])) || $arParams['isSubDealer'] != 'Y'):?>
									<li class="account-form__tab">
										<a href="#tab_111_8" id="tabLink_111_8">Артикулы</a>
									</li>
								<?endif;?>
								<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["instuctions"]) && !empty($arResult["DIRECTORIES"]["instuctions"])) || $arParams['isSubDealer'] != 'Y'):?>
									<li class="account-form__tab">
										<a href="#tab_111_9" id="tabLink_111_9">Инструкции</a>
									</li>
		                            <?global $USER;
									if ($USER->IsAdmin() && $arParams['isSubDealer'] != 'Y'){?>
			                            <li class="account-form__tab">
				                            <a href="#tab_111_11" id="tabLink_111_11">Менеджеры</a>
			                            </li>
	                                <?}?>
								<?endif;?>
								<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["goods"]) && !empty($arResult["DIRECTORIES"]["goods"])) || $arParams['isSubDealer'] != 'Y'):?>
									<li class="account-form__tab--link" style="font-size:12px" class="">
										<a href="<?=PREFIX?>/personal/goods/">Товары</a>
									</li>
								<?endif;?>
                            </ul>
                        </dd>
                    </dl>

                    <script type="text/javascript">
                        dropDownHandler();
                    </script>

<?//если у аккаунта доступ только к обработке заказов
//if (!CSite::InGroup (array(12))):?>
	<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["about"]) && !empty($arResult["DIRECTORIES"]["about"])) || $arParams['isSubDealer'] != 'Y'):?>
	<article class="tab <?if (($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["about"])) || $arParams['isSubDealer'] != 'Y'):?>first<?endif;?>" id="tab_111_1">

		<? // personalDataForm
		$APPLICATION->IncludeComponent(
			"aoptima:personalDataForm", "dealer",
			array('arResult' => $arResult)
		); ?>

	</article>
	<?endif;?>


	<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["delivery"]) && !empty($arResult["DIRECTORIES"]["delivery"])) || $arParams['isSubDealer'] != 'Y'):?>
		<article class="tab" id="tab_111_2">

			<? // Местоположения доставки
			$APPLICATION->IncludeComponent(
				"aoptima:personalDeliveryLocations", ""
			); ?>

		</article>
	<?endif;?>

	<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["payitem"]) && !empty($arResult["DIRECTORIES"]["payitem"])) || $arParams['isSubDealer'] != 'Y'):?>
		<article class="tab" id="tab_111_3">

			<? // Варианты оплаты
			$APPLICATION->IncludeComponent(
				"aoptima:personalPaymentVariants", ""
			); ?>

		</article>
	<?endif;?>

	<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["salepoint"]) && !empty($arResult["DIRECTORIES"]["salepoint"])) || $arParams['isSubDealer'] != 'Y'):?>
		<article class="tab  tab--address" id="tab_111_4">

			<? // Точки продаж дилера
			$APPLICATION->IncludeComponent(
				"aoptima:dealerLKShops", ""
			); ?>

		</article>
	<?endif;?>

<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["orders"]) && !empty($arResult["DIRECTORIES"]["orders"])) || $arParams['isSubDealer'] != 'Y'):?>
	<article class="tab <?if ($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["orders"]) && !isset($arResult["DIRECTORIES"]["about"])):?>first<?endif;?> tab--orders" id="tab_111_5" <?if (CSite::InGroup (array(12))):?>style="display:block;"<?endif;?>>

	    <?
	    global $USER;
	    // fixme
	    //if($USER->GetID() == 17)
	    {
	        ?>
	        <? // заявки под заказ для дилера
	        $APPLICATION->IncludeComponent(
	            "aoptima:under_order", "",
	            [
	                'IS_MOB_APP' => $arParams['IS_MOB_APP'],
					'IS_SUB_DILER' => $arParams['isSubDealer']
	            ]
	        ); ?>
	        <?
	    }?>

	    <? // Заказы к дилеру
	    $APPLICATION->IncludeComponent(
	        "aoptima:personalDealerOrders", "",
	        [
	            'IS_MOB_APP' => $arParams['IS_MOB_APP'],
				'DEALER_ID' => $arParams['isSubDealer'] == 'Y' ? $arResult['USER']["ID"]: '',
				'IS_SUB_DILER' => $arParams['isSubDealer']
	        ]
	    ); ?>
	</article>
<?endif;?>
<?//если у аккаунта доступ только к обработке заказов
	//if (!CSite::InGroup (array(12))):?>
	<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["rialto"]) && !empty($arResult["DIRECTORIES"]["rialto"])) || $arParams['isSubDealer'] != 'Y'):?>
	<article class="tab  tab--orders" id="tab_111_10">
		<? // биржа
		$APPLICATION->IncludeComponent(
			"aoptima:stock_market", "",
			[
			]
		); ?>
	</article>
	<?endif;?>
	<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["reviews"]) && !empty($arResult["DIRECTORIES"]["reviews"])) || $arParams['isSubDealer'] != 'Y'):?>
	<article class="tab  tab--review" id="tab_111_6">

		<? // Отзывы о заказах
		$APPLICATION->IncludeComponent(
			"aoptima:personalOrderReviews", "",
			[ 'DEALER_ID' => $arResult['USER']['ID'] ]
		); ?>

	</article>
	<?endif;?>

	<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["autoupdate"]) && !empty($arResult["DIRECTORIES"]["autoupdate"])) || $arParams['isSubDealer'] != 'Y'):?>
		<article class="tab  tab--review" id="tab_111_7">

			<? // Местоположения автообновления цен
			$APPLICATION->IncludeComponent(
				"aoptima:personalPricesAutoUpdateLocations", ""
			); ?>

		</article>
	<?endif;?>

	<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["articles"]) && !empty($arResult["DIRECTORIES"]["articles"])) || $arParams['isSubDealer'] != 'Y'):?>
	<article class="tab  tab--review" id="tab_111_8">

		<? // dealer_load_mapping_file_csv
		$APPLICATION->IncludeComponent(
			"aoptima:dealer_load_mapping_file_csv", ""
		); ?>

	</article>
	<?endif;?>
	<?if(($arParams['isSubDealer'] == 'Y' && isset($arResult["DIRECTORIES"]["instuctions"]) && !empty($arResult["DIRECTORIES"]["instuctions"])) || $arParams['isSubDealer'] != 'Y'):?>
		<article class="tab  tab--review" id="tab_111_9">

			<? // dealer_load_mapping_file_csv
			$APPLICATION->IncludeComponent(
				"aoptima:dealer_instructions", ""
			); ?>

		</article>
	<?endif;?>
	<?global $USER;
	if ($USER->IsAdmin() && $arParams['isSubDealer'] != 'Y'){?>
        <article class="tab  tab--review" id="tab_111_11">

			<?
			$APPLICATION->IncludeComponent(
				"aoptima:dealer_new_user", "",
				array('arResult' => $arResult)
			); ?>

        </article>
    <?}?>
					<?//endif;?>


                </div>
            </section>
        </div>
    </section>

    <iframe id="upload___Frame" name="upload___Frame" style="display: none"></iframe>
    <form style="visibility:hidden; height:0" class="persUpload___form" action="/ajax/uploadPersPhoto.php" target="upload___Frame" method="post" enctype="multipart/form-data">
        <input type="file" name="persPhotoInput" style="visibility:hidden; height:0">
    </form>
    <form style="visibility:hidden; height:0" class="deliveryFile___form" action="/ajax/uploadDeliveryFile.php" target="upload___Frame" method="post" enctype="multipart/form-data">
        <input type="file" name="deliveryFileInput" style="visibility:hidden; height:0">
        <input type="hidden" name="deliveryFileTitle">
    </form>


<? } else {

    include $_SERVER['DOCUMENT_ROOT'].'/include/auth.php';

} ?>
