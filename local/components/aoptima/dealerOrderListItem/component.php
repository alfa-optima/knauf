<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
\Bitrix\Main\Loader::includeModule('aoptima.tools');    use AOptima\Tools as tools;

$arResult = $arParams['ORDER'];

$arResult['DEALER_URL'] = ($arParams['IS_MOB_APP']=='Y'?'/knauf_app':'').'/dealers/'.$arResult['ORDER']['DEALER']['ID'].'/';

/*global $USER;
if($USER->IsAdmin())
{
    echo '<pre>';
    print_r ($arResult);
    echo '</pre>';
}*/


	$pvz = explode("[", $arResult['PVZ']);
	$pvz2 = (int)$pvz[1];
	$pvz_el = tools\el::info($pvz2);
	$arResult["PVZ_ADDR"] = trim($pvz[0]).', '.$pvz_el["PROPERTY_STREET_VALUE"].', '.$pvz_el["PROPERTY_HOUSE_VALUE"].' - '.$pvz_el["PROPERTY_OFFICE_VALUE"]; 




$this->IncludeComponentTemplate();