<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<tr class="archOrderItem" item_id="<?=$arResult['ID']?>">
    <td>
        <div class="account-form__table-flexColumn">
            <span class="account-form__table-goodsQuantity"><?=$arResult['ID']?></span>
        </div>
    </td>
    <td>
        <div class="account-form__table-flexColumn">
            <span>
                <?
                if($arResult["FAST_ORDER"])
                {
                    ?>
                    Быстрый заказ <br>
                    <?= $arResult['DELIVERY_ADDRESS'] ?>
                    <?
                }
                else
                {
                    ?>
                    Доставка "<?= $arResult['ds']['NAME'] ?>"
                    <? if ($arResult['delivType'] == 'delivery') { ?>
                    <br>
                    <?= $arResult['DELIVERY_ADDRESS'] ?>
                    <? } ?>
                    <?
                }?>
            </span>
        </div>
    </td>
    <td>
        <div class="account-form__table-flexColumn">
            <span class="account-form__table-mobTh">Оплата</span>
            <span><?=$arResult['ps']['NAME']?></span>
        </div>
    </td>
    <td>
        <div class="account-form__table-flexColumn">
            <span class="account-form__table-mobTh">Дата</span>
            <span><?=$arResult['DATE_INSERT']?></span>
        </div>
    </td>
    <td>
        <div class="account-form__table-flexColumn">
            <div>
                <span class="account-form__table-mobTh">Статус</span>
                <span><?=$arResult['ORDER_DEALER_STATUSES'][$arResult['ORDER_DEALER_STATUS']]['NAME']?></span>
            </div>
        </div>
    </td>
</tr>
<tr class="account-form__table-orderInner">
    <td colspan="6">

        <table class="account-form__table-order">
            <tbody>

            <? foreach( $arResult['arBasket'] as $key => $basketItem ){ ?>

                <tr>
                    <td>
                        <div class="account-form__table-orderWrapper">
                            <div class="account-form__table-orderName">

                                <?php if( intval($basketItem['product']['ID']) > 0 ){ ?>

                                    <? if( $basketItem['product_img'] ){ ?>
                                        <a href="<?=$basketItem['product']['DETAIL_PAGE_URL']?>">
                                            <img src="<?=$basketItem['product_img']?>">
                                        </a>
                                    <? } ?>
                                    <a href="<?=$basketItem['product']['DETAIL_PAGE_URL']?>"><?=$basketItem['product']['NAME']?></a>

                                <? } else { ?>

                                    <a><?=$basketItem['basketItemName']?></a>

                                <? } ?>

                            </div>
                        </div>
                    </td>
                    <td>
                        <div>
                                <span>
                                    <?=$basketItem['QUANTITY']?> шт
                                </span>
                        </div>
                    </td>
                    <td>
                        <div>
                                <span>
                                    <?
                                    if($basketItem['sum'])
                                    {
                                        ?>
                                        <?= $basketItem['priceFormat'] ?> <span class="rub">₽</span> за шт.
                                        <?
                                    }
                                    else
                                    {
                                        echo 'ПОДАРОК';
                                    }?>
                                </span>
                        </div>
                    </td>
                </tr>

            <? } ?>

            </tbody>
        </table>

    </td>
</tr>