<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<tr class="account-form__table-mainRow">
    <td>
        <div class="account-form__table-flexColumn">
            <span class="account-form__table-mobTh">№</span>
            <span class="account-form__table-goodsQuantity"><?=$arResult['ID']?></span>
        </div>
    </td>
    <td>
        <div class="account-form__table-flexColumn">
            <span class="account-form__table-mobTh">Дата</span>
            <span><?=$arResult['DATE_INSERT']?></span>
        </div>
    </td>
    <td>
        <div class="account-form__table-flexColumn">
            <span class="account-form__table-mobTh">Доставка</span>
            <span>
                <?
                if($arResult["FAST_ORDER"])
                {
                    ?>
                    <span>Быстрый заказ</span>
                    <?
                }
                else
                {
                    ?>
                    <span><?= $arResult['ds']['NAME'] ?></span>
                    <?
                }?>
            </span>
        </div>
    </td>
    <td>
        <div class="account-form__table-flexColumn">
            <span class="account-form__table-mobTh">Оплата</span>
            <span><?=$arResult['ps']['NAME']?></span>
        </div>
    </td>
    <td>
        <div class="account-form__table-flexColumn">
            <span class="account-form__table-mobTh">Дата доставки</span>
            <span><?=$arResult['DELIV_DATE']?></span>
        </div>
    </td>
    <td>
        <div class="account-form__table-flexRow">
            <div>
                <div class="styledSelect">
                    <select id="status_<?=$arResult['ID']?>" name="statuses[<?=$arResult['ID']?>]">
                        <? foreach( $arResult['ORDER_DEALER_STATUSES'] as $key => $status ){ ?>
                            <option <? if(  $arResult['ORDER_DEALER_STATUS'] == $status['CODE'] ){ echo 'selected'; } ?> value="<?=$status['CODE']?>"><?=$status['NAME']?></option>
                        <? } ?>
                    </select>
                </div>
            </div>
            <div><span class="account-form__table-goodsQuantity  account-form__table-goodsQuantity--change">Подробнее</span>
            </div>
            <div>
                <a href="javascript:;" class="dealerAccount-delivery__button dealerOrderStatusesSave" data-id="<?=$arResult['ID']?>">Сохранить</a>
            </div>
        </div>
    </td>
</tr>
<tr class="account-form__table-orderInner">
    <td colspan="100">
        <div class="account-form__table-order-wrapper">
            <table class="account-form__table-order">
                <tbody>
                <tr>
                    <th>Товар</th>
                    <th>Артикул SAP</th>
                    <th>Количество</th>
                    <th>Цена</th>
                    <th>Цена общая</th>
                </tr>
                <?
                foreach( $arResult['arBasket'] as $key => $basketItem )
                {?>
                    <tr>
                        <td>
                            <?php if( intval($basketItem['product']['ID']) > 0 ){ ?>

                                <? if( $basketItem['product_img'] ){ ?>
                                    <a href="<?=$basketItem['product']['DETAIL_PAGE_URL']?>">
                                        <img src="<?=$basketItem['product_img']?>">
                                    </a>
                                <? } ?>
                                <a href="<?=$basketItem['product']['DETAIL_PAGE_URL']?>"><?=$basketItem['product']['NAME']?></a>

                            <? } else { ?>

                                <a><?=$basketItem['basketItemName']?></a>

                            <? } ?>
                        </td>
                        <td>
                                                    <span>
                                                        <?=implode(", ", $basketItem['product']["PROPERTY_SAP_ART_VALUE"])?>
                                                    </span>
                        </td>
                        <td>
                            <span>
                                <?=$basketItem['QUANTITY']?> шт
                            </span>
                        </td>
                        <td>
                            <span>
                                <?
                                if($basketItem['sum'])
                                {
                                    ?>
                                    <?= $basketItem['priceFormat'] ?> <span class="rub">₽</span> за шт.
                                    <?
                                }
                                else
                                {
                                    echo 'ПОДАРОК';
                                }?>
                            </span>
                        </td>
                        <td>
                            <span>
                                <?
                                if($basketItem['sum'])
                                {
                                    ?>
                                    <?= $basketItem['priceFormat'] * $basketItem['QUANTITY']?> <span class="rub">₽</span>
                                    <?
                                }
                                else
                                {
                                    echo 'ПОДАРОК';
                                }?>
                            </span>
                        </td>
                    </tr>
                    <?
                }?>
                <tr>
                    <td colspan="100">
                        <div class="cart-form__infoColumn-sum">
                            <span>Итого:</span>
                            <span><?=$arResult['TOTAL_SUM_FORMAT']?><span class="rub">₽</span></span>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="account-form__table-order-total">
            <? if( strlen($arResult['POD_ZAKAZ_GOODS']) > 0 ){ ?>

                <div class="account-form__row">
                    <div class="cart-form__infoColumn">
                        <span class="cart-form__infoColumn-title"><b>Товары под заказ:</b> <?=$arResult['POD_ZAKAZ_GOODS']?></span>
                    </div>
                </div>

            <? } ?>

            <div class="account-form__row">
                <div class="cart-form__infoColumn">
                    <span class="cart-form__infoColumn-title">ФИО: <?=$arResult['BUYER_NAME']?> <?=$arResult['BUYER_LAST_NAME']?></span>
                </div>
            </div>

            <div class="account-form__row">
                <div class="cart-form__infoColumn">
                    <span class="cart-form__infoColumn-title">Телефон: <?=$arResult['BUYER_PHONE']?></span>
                </div>
            </div>

            <div class="account-form__row">
                <div class="cart-form__infoColumn">
                    <span class="cart-form__infoColumn-title">Эл. почта: <?=$arResult['BUYER_EMAIL']?></span>
                </div>
            </div>

            <?
            if($arResult["FAST_ORDER"])
            {
                ?>
                <div class="account-form__row">
                    <div class="cart-form__infoColumn">
                        <span class="cart-form__infoColumn-title">Адрес: <?= $arResult['DELIVERY_ADDRESS'] ?></span>
                    </div>
                </div>
                <?
            }
            else
            {
                ?>
                <? if ($arResult['delivType'] == 'delivery')
            {?>
                <div class="account-form__row">
                    <div class="cart-form__infoColumn">
                        <span class="cart-form__infoColumn-title">Адрес: <?= $arResult['DELIVERY_ADDRESS'] ?></span>
                    </div>
                </div>
                <?
            }
            else
            { ?>
                <div class="account-form__row">
                    <div class="cart-form__infoColumn">
                        <span class="cart-form__infoColumn-title">Пункт выдачи: <?= $arResult['PVZ_ADDR'] ?></span>
                    </div>
                </div>
                <?
            }
            }?>

            <? if( $arResult['BUYER_LIFT'] && 1==2 ){ ?>

                <div class="account-form__row">
                    <div class="cart-form__infoColumn">
                        <span class="cart-form__infoColumn-title">Наличие лифта: <?=$arResult['BUYER_LIFT']?></span>
                    </div>
                </div>

            <? } ?>

            <div class="account-form__row">
                <div class="cart-form__infoColumn">
                    <span class="cart-form__infoColumn-title">Комментарий: <?=$arResult['BUYER_COMMENT']?></span>
                </div>
            </div>

            <div class="account-form__row">
                <div class="cart-form__infoColumn">

                    <span class="cart-form__infoColumn-title">Стоимость доставки: <? if( $arResult['DELIVERY_SUM']==0 ){ echo 'по договоренности'; } else { ?><b><?=$arResult['DELIVERY_SUM_FORMAT']?></b> <span class="rub">₽</span><? } ?></span>

                    <? if( $arResult['LIFT'] ){ ?>
                        <span class="cart-form__infoColumn-text"><?=$arResult['LIFT']?></span>
                    <? } ?>

                </div>

                <? if( $arResult['DELIV_DATE'] ){ ?>
                    <div class="cart-form__infoColumn">
                        <span class="cart-form__infoColumn-title">Дата доставки</span>
                        <span class="cart-form__infoColumn-text"><?=$arResult['DELIV_DATE']?></span>
                    </div>
                <? } ?>

                <? if( $arResult['DELIV_TIME'] ){ ?>
                    <div class="cart-form__infoColumn">
                        <span class="cart-form__infoColumn-title">Время доставки</span>
                        <span class="cart-form__infoColumn-text"><?=$arResult['DELIV_TIME']?></span>
                    </div>
                <? } ?>
            </div>
            <div class="dealerAccount-form__row">
                <div class="dealerAccount-form__block">
                    <span class="dealerAccount-form__block-title">Комментарий к заказу № <?=$arResult['ID']?></span>
                    <div class="dealerAccount-form__field  dealerAccount-form__field--w100">
                        <div class="account-form__textarea-counter">Оставшиеся символы: <span><?=(300 - strlen($arResult['DEALER_COMMENT']))?></span>
                        </div>
                        <textarea maxlength="300"
                                  placeholder="Дополнительный комментарий о заказе"
                                  class="dealerLKComment" name="dealer_order_comments[<?=$arResult['ID']?>]"><?=$arResult['DEALER_COMMENT']?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </td>
</tr>
<?
				/*\Bitrix\Main\Loader::includeModule('aoptima.tools');    use AOptima\Tools as tools;
global $USER;
if ($USER->IsAdmin()):

//echo '<pre>'; print_r($arResult['PVZ']); echo '</pre>';

$pvz = explode("[", $arResult['PVZ']);
$pvz2 = (int)$pvz[1];
 $pvz_el = tools\el::info($pvz2);
$addr = trim($pvz[0]).', '.$pvz_el["PROPERTY_STREET_VALUE"].', '.$pvz_el["PROPERTY_HOUSE_VALUE"].' - '.$pvz_el["PROPERTY_OFFICE_VALUE"]; 
//echo '<pre>'; print_r($pvz_el); echo '</pre>';
echo $addr;
endif;*/
?>