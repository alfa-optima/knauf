<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult = $arParams['template'];

if( $arResult ){

    $arResult['LIFT_ENUMS'] = array();
    $props = \CUserTypeEntity::GetList(array($by => $order), array('ENTITY_ID' => 'HLBLOCK_' . project\delivery_template::HIBLOCK_ID, 'FIELD_NAME' => 'UF_LIFT'));
    while ($prop = $props->Fetch()) {
        $enums = \CUserFieldEnum::GetList(array(), array("USER_FIELD_ID" => $prop['ID'],));
        while ($enum = $enums->GetNext()) {
            $arResult['LIFT_ENUMS'][$enum['XML_ID']] = $enum;
        }
    }

    $arResult['LOC'] = false;
    if( intval($arResult['UF_LOC_ID']) > 0 ){
        $loc = project\bx_location::getByID($arResult['UF_LOC_ID']);
        if( intval($loc['ID']) > 0 ){   $arResult['LOC'] = $loc;   }
    }

}




$this->IncludeComponentTemplate();