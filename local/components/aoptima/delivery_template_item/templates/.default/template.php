<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$template_id = $arResult?$arResult['ID']:'new';

//echo "<pre>"; print_r($arResult); echo "</pre>"; ?>

<div class="account-form__wrapperMinusMargin deliveryTemplateItem <? if( !$arResult ){ ?>emptyDelivBlock<? } ?>">

    <input class="id_item" type="hidden" name="ID[<?=$template_id?>]" value="<?=$template_id?>">

    <div class="account-form__row">
        <div class="account-form__field  account-form__field--w318">
            <span>Имя шаблона адреса доставки</span>
            <input class="required" type="text" name="UF_TEMPLATE[<?=$template_id?>]" value="<?=$arResult['UF_TEMPLATE']?>">
        </div>
        <? //if( $arResult ){ ?>
            <span class="account-form__deleteAddress removeDeliveryTemplate to___process">Удалить адрес доставки</span>
        <? //} ?>
    </div>
    <div class="account-form__row">

        <? if( 1==2 ){ ?>

            <div class="account-form__field  account-form__field--w318">
                <span>Город</span>
                <input
                    class="cityInput required account-form__suggestInput"
                    id="dt_city_<?=$template_id?>"
                    type="text"
                    name="CITY_NAME[<?=$template_id?>]"
                    value="<?=($arResult['LOC']?($arResult['LOC']['NAME_RU'].' ('.$arResult['LOC']['PARENT_NAME_RU'].')'):'')?>"
                    cityName=""
                    autocomplete="off"
                    onfocus="cityFocus($(this));"
                    onkeyup="cityKeyUp($(this));"
                    onfocusout="cityFocusOut($(this));"
                >
                <input type="hidden" name="UF_LOC_ID[<?=$template_id?>]" value="<?=($arResult['LOC']?$arResult['LOC']['ID']:'')?>">
            </div>
            <script>
            $(document).ready(function(){
                $('#dt_city_<?=$template_id?>').autocomplete({
                    source: '/ajax/searchCity.php',
                    minLength: 2,
                    delay: 700,
                    select: citySelect
                })
            })
            </script>

        <? } ?>

        <div class="account-form__field  account-form__field--w760">
            <span>Адрес (Город, улица, номер дома)</span>
            <input
                class="account-form__suggestInput required"
                id="delivTemplateAddress_<?=$template_id?>"
                type="text"
                process="N"
                name="UF_ADDRESS[<?=$template_id?>]"
                addressName=""
                value="<?=$arResult['UF_ADDRESS']?>"
                onfocus="addressFocus($(this));"
                onkeyup="addressKeyUp($(this), 'delivTemplateAddress_<?=$template_id?>');"
                onfocusout="addressFocusOut($(this), '<?=$arResult['UF_COORDS']?>');"
                autocomplete="off"
            >
            <input type="hidden" name="UF_COORDS[<?=$template_id?>]" value="<?=$arResult['UF_COORDS']?>">
            <script>
            $(document).ready(function(){
                $('#delivTemplateAddress_<?=$template_id?>').autocomplete({
                    source: '/ajax/searchAddress.php',
                    minLength: 2,
                    delay: 700,
                    select: addressSelect,
                    response: function(event, ui){
                        if( ui.content.length == 0 ){
                            reloadOrder();
                        }
                    }
                })
            })
            </script>

        </div>
    </div>
    <div class="account-form__row">
        <div class="account-form__fields">
            <div class="account-form__field  account-form__field--w85">
                <span>Подъезд</span>
                <input type="text" name="UF_PODYEZD[<?=$template_id?>]" value="<?=$arResult['UF_PODYEZD']?>">
            </div>
            <div class="account-form__field  account-form__field--w85">
                <span>Этаж</span>
                <input type="text" name="UF_FLOOR[<?=$template_id?>]" value="<?=$arResult['UF_FLOOR']?>">
            </div>
            <div class="account-form__field  account-form__field--w85">
                <span>Квартира</span>
                <input type="text" name="UF_KVARTIRA[<?=$template_id?>]" value="<?=$arResult['UF_KVARTIRA']?>">
            </div>
        </div>
        <div class="account-form__field  account-form__field--w416">
            <div class="account-form__checkboxes">
                <div class="account-form__checkbox  account-form__checkbox--mr35">
                    <input class="liftCheckbox" <? if( $arResult && $arResult['LIFT_ENUMS']['Y']['ID'] == $arResult['UF_LIFT'] ){ echo 'checked'; } ?> type="radio" id="lift_Y_<?=$template_id?>" name="UF_LIFT[<?=$template_id?>]" value="Y">
                    <label for="lift_Y_<?=$template_id?>">с лифтом</label>
                </div>
                <div class="account-form__checkbox  account-form__checkbox--mr35">
                    <input class="liftCheckbox" <? if( $arResult && $arResult['LIFT_ENUMS']['N']['ID'] == $arResult['UF_LIFT'] ){ echo 'checked'; } ?> type="radio" id="lift_N_<?=$template_id?>" name="UF_LIFT[<?=$template_id?>]" value="N">
                    <label for="lift_N_<?=$template_id?>">без лифта</label>
                </div>
                <div class="account-form__checkbox">
                    <input class="liftCheckbox" <? if( $arResult && $arResult['LIFT_ENUMS']['G']['ID'] == $arResult['UF_LIFT'] ){ echo 'checked'; } ?> type="radio" id="lift_G_<?=$template_id?>" name="UF_LIFT[<?=$template_id?>]" value="G">
                    <label for="lift_G_<?=$template_id?>">грузовой лифт</label>
                </div>
            </div>
        </div>
    </div>
    <div class="account-form__field  account-form__field--w760">
        <span>Комментарий</span>
        <div class="account-form__textarea-counter">Оставшиеся символы: <span>300</span></div>
        <textarea name="UF_COMMENT[<?=$template_id?>]" maxlength="300"><?=$arResult['UF_COMMENT']?></textarea>
    </div>
    <? if( $arResult ){ ?>
        <div class="account-form__addAddress addEmptyDeliveryTemplate to___process">
            <span>Добавить ещё один адрес</span>
        </div>
    <? } ?>
</div>
