<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$visible = $arParams['visible']; ?>


<div class="cart-offer__best-item basketOfferItem <? if( $visible == 'N' ){ ?>basketOfferItemHidden<? } ?>" dealer_id="<?=$arResult['DEALER']['ID']?>" dealer_name="<?=$arResult['DEALER']['UF_COMPANY_NAME']?>">
    <table class="cart-offer__table">
        <tbody>

        <? $cnt = 0;
        foreach ( $arResult['DEALER']['basketProducts'] as $bProduct ){ $cnt++; ?>

            <tr class="cart-offer__table-stripped <? if( $bProduct['NAL'] == 'N' || $bProduct['FULL_QUANTITY'] == 'N'){ ?>cart-offer__table-noItem<? } ?>">

                <? if( $cnt == 1 ){ ?>
                    <td class="cart-offer__table-rowspan" rowspan="1000<?//=count($arResult['DEALER']['basketProducts'])?>">
                        <div class="cart-offer__table-dealer">
                            <a <?/*target="_blank" href="<?=$arResult['DEALER_URL']?>"*/?> class="cart-offer__table-dealerName"><?=htmlspecialchars_decode($arResult['DEALER']['UF_COMPANY_NAME'])?></a>
                            <div class="cart-offer__table-dealerRating">
                                <? for ( $r = 1; $r <=5; $r++ ){ ?>
                                    <span class="cart-offer__table-dealerRating-star  <? if( $arResult['DEALER']['UF_RATING'] && $arResult['DEALER']['UF_RATING'] >= $r ){ ?>cart-offer__table-dealerRating-star--full<? } ?>"></span>
                                <? } ?>
                            </div>
                        </div>
                    </td>
                <? } ?>

                <td><?=htmlspecialchars_decode($bProduct['el']['NAME'])?></td>

                <td>
                    <? if( $bProduct['NAL'] == 'Y' ){ ?>
                        <span class="yesMobile">Количество</span>
                        <?=$bProduct['DEALER_QUANTITY']>=$bProduct['quantity']?$bProduct['quantity']:$bProduct['DEALER_QUANTITY']?> шт
                    <? } else { ?>
                        нет
                    <? } ?>
                </td>

                <td>
                    <?
                    if(!$bProduct["custom_price"] || $bProduct["price"])
                    {
                        ?>
                        <? if( $bProduct['DEALER_PRICE'] ){ ?>
                        <span class="yesMobile">Цена</span>
                        <?=$bProduct['DEALER_PRICE_FORMAT']?>&nbsp;<span class="rub">₽</span>
                        <? } ?>
                        <?
                    }
                    else
                    {
                        ?>
                        <span class="yesMobile">Цена</span>
                        ПОДАРОК
                        <?
                    }?>
                </td>
				
				<!--итоговая цена-->
				<td>
                    <?
                    if(!$bProduct["custom_price"] || $bProduct["price"])
                    {
                        ?>
                        <? if( $bProduct['DEALER_PRICE'] ){ ?>
                        <span class="yesMobile">Итого</span>
						
						<?$price = str_replace(" ", "", $bProduct['DEALER_PRICE_FORMAT']);?>
						<?$price = str_replace(",", ".", $price);?>
						
						<?$number = $bProduct['DEALER_QUANTITY']>=$bProduct['quantity']?$bProduct['quantity']:$bProduct['DEALER_QUANTITY'];?>
						
						<?$total = number_format($price * $number, 2, ',', ' '); ?>
						
                        <?echo $total;?>&nbsp;<span class="rub">₽</span>
                        <? } ?>
                        <?
                    }
                    else
                    {
                        ?>
                        <span class="yesMobile">Цена</span>
                        ПОДАРОК
                        <?
                    }?>
                </td>
				
				
				

            </tr>

        <? }
        ?>
        <tr class="cart-offer__table-delivery">
            <td>
                <div class="cart-offer__table-deliveryWrapper">

                    <? // вариант с доставкой
                    if( $arParams['componentArResult']['delivType'] == 'delivery' ){
                        $r = preg_match("/[а-яёА-ЯЁ]+/i", $arResult['DEALER']['DELIVERY_PRICE']);
                        //if( 1==2 ){ ?>

                            <span class="cart-offer__table-deliveryText">Стоимость доставки: </span>

                            <div class="cart-offer__table-deliveryList">
                                <div class="cart-offer__table-deliveryListItem  cart-offer__table-deliveryListItem--active ">
                                    <?
                                    if(!$arResult['DEALER']['DELIVERY_PRICE'])
                                    {
                                        ?>
                                        <span class="free_deliv">бесплатно</span>
                                        <?
                                    }
                                    else
                                    {
                                        ?>
                                        <? if (!$arResult['DEALER']['DELIVERY_PRICE_FORMAT']) { ?>
                                        <span>
                                            <?= $arResult['DEALER']['DELIVERY_PRICE'] ?>
                                        </span>
                                    <? } else { ?>
                                        <span><?= $arResult['DEALER']['DELIVERY_PRICE_FORMAT'] ?>&nbsp;<span
                                                    class="rub">₽</span></span>
                                    <? } ?>
                                        <?
                                    }?>
                                </div>
                            </div>

                        <? //}
                    // вариант с самовывозом
                    } else if( $arParams['componentArResult']['delivType'] == 'samovyvoz' ){ ?>

                        <div class="account-form__field  account-form__field--w318 offerPVZBlock">
                            <span>Выберите пункт выдачи</span>
                            <div class="styledSelect">
                                <select id="dealerPVZ_<?=$arResult['DEALER']['ID']?>" name="pvz_<?=$arResult['DEALER']['ID']?>" onchange="$(this).parents('div.basketOfferItem').find('.error___td').html('');">
								<?if (count($arResult['DEALER']['PVZ_LIST'])>1):?>
                                    <option value="empty">--- Выбор пункта выдачи ---</option>
								<?endif;?>	
                                    <? foreach( $arResult['DEALER']['PVZ_LIST'] as $key => $pvz ){ ?>
                                        <option value="<?=$pvz['ID']?>"><?=strlen($pvz['ADDRESS'])>0?$pvz['ADDRESS']:$pvz['NAME']?></option>
                                    <? } ?>
                                </select>
                            </div>
                        </div>

                    <? } ?>

                </div>
            </td>
            <td></td>
            <td></td>
        </tr>

        <? if( count($arResult['DEALER']['WARNINGS']) > 0 ){
            foreach( $arResult['DEALER']['WARNINGS'] as $warning ){ ?>
                <tr class="cart-offer__table-totalRow" >
                    <td colspan="4">
                        <div class="cart-offer__table-deliveryWrapper" style="float: right;">
                            <span class="cart-offer__table-deliveryText basket___warning"><?=html_entity_decode($warning)?></span>
                        </div>
                    </td>
                </tr>
            <? } ?>
            <tr class="cart-offer__table-totalRow" >
                <td colspan="4"></td>
            </tr>
        <? } ?>

        <tr class="cart-offer__table-totalRow" >
            <td></td>
            <td>
                <div class="cart-offer__table-total"><span class="cart-offer__table-totalText">Итого</span><span class="cart-offer__table-totalPrice"><?=$arResult['DEALER']['TOTAL_SUM_FORMAT']?>&nbsp;<span class="rub">₽</span></span></div>
            </td>
            <td colspan="1000">
                <a style="cursor: pointer;" class="cart-offer__table-button toOrderButton to___process" >Оформить заказ</a>
            </td>
        </tr>

        <tr class="cart-offer__table-totalRow">
            <td colspan="4" class="error___td"></td>
        </tr>

        </tbody>
    </table>
</div>
<div style="display:none">
<? //echo ''; print_r($arResult); echo ''; ?>
</div>




