<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<input type="hidden" name="loc_city" value="<?=$arResult['KNAUF_LOC']['CITY']?>">
<input type="hidden" name="loc_city_id" value="<?=$arResult['KNAUF_LOC']['CITY_LOC_ID']?>">
<input type="hidden" name="loc_region" value="<?=$arResult['KNAUF_LOC']['REGION']?>">
<input type="hidden" name="loc_country" value="<?=$arResult['KNAUF_LOC']['COUNTRY']?>">

<a class="modal-city__link  sr-only" href="#modal-city">triggers fancybox</a>
<div id='modal-city'  class="modal-city">
    <a href="javascript:;" onclick="$.fancybox.close();" class="modal-city__close">Закрыть</a>
    <h2 class="modal-city__title">Ваш город</h2>
    <div class="modal-city__city">
        <span><?=$arResult['KNAUF_LOC']['CITY']?><? if($arResult['KNAUF_LOC']['REGION']!='N'){ echo ' ('.$arResult['KNAUF_LOC']['REGION'].')'; } ?></span>
    </div>
    <div class="modal-city__buttons">
        <span class="modal-city__button  modal-city__button--yes city_accept_button to___process" loc_id="<?=$arResult['KNAUF_LOC']['CITY_LOC_ID']?>" data-city="<?=$arResult['KNAUF_LOC']['CITY']?>">Да</span>
        <span onclick="$.fancybox.close(); setTimeout(function(){ $('.modal-cities__link').fancybox({ padding: 0 }) .trigger('click'); }, 800);" class="modal-city__button  modal-city__button--no">Нет</span>
    </div>
</div>





