<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(
    $arResult['arURI'][1] != 'personal'
    ||
    (
        $arResult['arURI'][1] == 'personal'
        &&
        strlen($arResult['arURI'][2]) > 0
    )
){ ?>

    <div class="subscribe  subscribe__hidden">
        <header class="subscribe__header">
            <div id="subscribeToggle">
                <h4 class="subscribe__title">Подписка на&nbsp;рассылку</h4>
                <span class="subscribe__arrow"></span>
            </div>
        </header>
        <form onsubmit="return false" class="subscribeForm subscribe__form">
            <div class="subscribe__inner">

                <p>Подпишитесь на&nbsp;нашу рассылку и&nbsp;будьте в&nbsp;курсе последних новостей.</p>

                <div class="subscribe__input">
                    <input type="text" placeholder="Ваш адрес эл. почты *" name="email" value="<?=$arResult['USER_SUBSCRIBE_EMAIL']?>">
                </div>

                <div class="subscribe__interests">

                    <?php if( count($arResult['ALL_SUBSCRIBE_RUBRICS']) > 0 ){ ?>

                        <p>Рубрики подписки:</p>

                        <? foreach( $arResult['ALL_SUBSCRIBE_RUBRICS'] as $rub ){ ?>

                            <div class="subscribe__input">
                                <input class="rubric_checkbox" <? if( in_array($rub['ID'], $arResult['USER_SUBSCRIBE_RUBRICS']) ){ echo 'checked'; } ?> type="checkbox" id="modal_rubric_<?=$rub['ID']?>" name="rubrics[]" value="<?=$rub['ID']?>">
                                <label for="modal_rubric_<?=$rub['ID']?>"><?=$rub['NAME']?></label>
                            </div>

                        <? } ?>

                    <?php } ?>

                    <div class="subscribe__input">
                        <input type="checkbox" <? if( $arResult['USER']['UF_SUB_RULES_ACCEPT'] ){ echo 'checked'; } ?> id="subscribeYes" name="subscribeYes" value="Y">
                        <label for="subscribeYes">Да, я хотел бы подписаться на рассылку, прочитав и приняв <a  class="subscribe__input-link" href="https://www.knauf.ru/about/confidentiality/" target="_blank">условия</a>.</label>
                    </div>

                    <p class="error___p modal_error___p"></p>

                    <div class="subscribe__button">
                        <a style="cursor:pointer" class="subscribeButton isModalButton to___process">Сохранить подписку</a>
                    </div>

                </div>
            </div>
        </form>
    </div>

<? } ?>