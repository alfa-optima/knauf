<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<section>
    <div>
        <form onsubmit="return false" class="subscribeForm">
            <h4 class="tab__title">Подписка</h4>
            <div class="account-form__wrapperMinusMargin">
                <div class="account-form__flexColumn">

                    <div class="account-form__field">
                        <span>Ваш адрес эл. почты *</span>
                        <input type="email" placeholder="Адрес эл. почты для подписки" name="email" value="<?=$arResult['USER_SUBSCRIBE_EMAIL']?>">
                    </div>

                    <?php if( count($arResult['ALL_SUBSCRIBE_RUBRICS']) > 0 ){ ?>

                        <span class="account-form__listTitle">Рубрики подписки:</span>

                        <? foreach( $arResult['ALL_SUBSCRIBE_RUBRICS'] as $rub ){ ?>

                            <div class="account-form__checkbox  account-form__checkbox--vertical">
                                <input <? if( in_array($rub['ID'], $arResult['USER_SUBSCRIBE_RUBRICS']) ){ echo 'checked'; } ?> class="rubric_checkbox" type="checkbox" id="rubric_<?=$rub['ID']?>" name="rubrics[]" value="<?=$rub['ID']?>">
                                <label for="rubric_<?=$rub['ID']?>"><?=$rub['NAME']?></label>
                            </div>

                        <? } ?>

                        <div>
                            <span class="account-form__selectAll">выделить все</span>
                            <span class="account-form__cancelAll">отменить все</span>
                        </div>

                    <? } ?>

                    <p class="error___p"></p>

                    <a style="cursor:pointer" class="account-form__button subscribeButton to___process">Сохранить</a>

                </div>
            </div>
        </form>
    </div>
</section>