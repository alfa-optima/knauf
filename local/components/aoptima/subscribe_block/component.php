<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$obSubscribe = new project\subscribe();

// все рубрики
$arResult['ALL_SUBSCRIBE_RUBRICS'] = $obSubscribe->rubricsList();

if( $USER->IsAuthorized() ){

    $arResult['arURI'] = tools\funcs::arURI();

    // Инфо о пользователе
    $arResult['USER'] = tools\user::info($USER->GetID());

    $isSocUser = $arResult['USER']['EXTERNAL_AUTH_ID']=='socservices'?'Y':'N';

    //if( $isSocUser != 'Y' ){

        // Email подписки для пользователя
        $arResult['USER_SUBSCRIBE_EMAIL'] = $arResult['USER']['UF_SUBSCRIBE_EMAIL']?$arResult['USER']['UF_SUBSCRIBE_EMAIL']:$arResult['USER']['EMAIL'];

        // Рубрики, на которые подписан пользователь
        $arResult['USER_SUBSCRIBE_RUBRICS'] = array();
        if( strlen($arResult['USER_SUBSCRIBE_EMAIL']) > 0 ){
            $arResult['USER_SUBSCRIBE_RUBRICS'] = $obSubscribe->getEmailRubrics( $arResult['USER_SUBSCRIBE_EMAIL'] );
        }

        $this->IncludeComponentTemplate();

    //}

}





