<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

use Bitrix\Main,
    Bitrix\Main\Localization\Loc as Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem,
    Bitrix\Sale,
    Bitrix\Sale\Order,
    Bitrix\Sale\Basket,
    Bitrix\Sale\DiscountCouponsManager,
    Bitrix\Main\Context;

\Bitrix\Main\Loader::includeModule('sale');

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arResult['ORDER_ID'] = $arParams['ORDER_ID'];
$arResult['SERVER_NAME'] = \Bitrix\Main\Config\Option::get('main', 'server_name');
$arResult['AUTH'] = $USER->IsAuthorized();

if( intval($arResult['ORDER_ID']) > 0 ){

    $order = Sale\Order::load(intval($arResult['ORDER_ID']));
    $basket = Sale\Order::load(intval($arResult['ORDER_ID']))->getBasket();
    $arResult['arBasket'] = [];
    foreach ( $basket as $key => $bItem ){
        $bItem->el = tools\el::info($bItem->getProductId());
        if( intval($bItem->el['PROPERTY_CML2_LINK_VALUE']) > 0 ){
            $bItem->product = tools\el::info($bItem->el['PROPERTY_CML2_LINK_VALUE']);
        } else {
            $bItem->product = $bItem->el;
        }
        $bItem->product = project\catalog::updateProductName($bItem->product);
        $arResult['arBasket'][$key] = $bItem;
    }

    $arResult['ORDER_DATE'] = ConvertDateTime($order->getDateInsert(), "DD", "ru").' '.tools\funcs::getMonthName(ConvertDateTime($order->getDateInsert(), "MM", "ru")).' '.ConvertDateTime($order->getDateInsert(), "YYYY", "ru").ConvertDateTime($order->getDateInsert(), " HH:MI", "ru");

    $arResult['DELIVERY_PRICE'] = $order->getDeliveryPrice();
    $arResult['PAY_SUM'] = $order->getPrice();
    $arResult['BASKET_SUM'] = $arResult['PAY_SUM'] - $arResult['DELIVERY_PRICE'];

    $paymentCollection = $order->getPaymentCollection();
    $shipmentCollection = $order->getShipmentCollection();
    if($paymentCollection[0])
    {
        $arResult['PS_ID'] = $paymentCollection[0]->getPaymentSystemId();
        $arResult['PS'] = project\ps::getByID($arResult['PS_ID']);
    }
    if($shipmentCollection[0])
    {
        $arResult['DS_ID'] = $shipmentCollection[0]->getDeliveryId();
        $arResult['DS'] = project\ds::getByID($arResult['DS_ID']);
    }

    // Список вариантов доставки
    $arResult['DS_LIST'] = project\ds::getList();

    // Тип активного варианта доставки
    $arResult['delivType'] = false;
    if( intval($arResult['DS_ID']) > 0 ){
        foreach( $arResult['DS_LIST'] as $ds ){
            if( $ds['ID'] == $arResult['DS_ID'] ){
                $arResult['delivType'] = $ds['delivType'];
            }
        }
    }

    $propValues = project\order::getOrderPropValues($order, true);
    $arResult['NAME'] = project\order::getPropValue($propValues, 'NAME');
    $arResult['LAST_NAME'] = project\order::getPropValue($propValues, 'LAST_NAME');

    $arResult['PHONE'] = project\order::getPropValue($propValues, 'PHONE');
    $arResult['EMAIL'] = project\order::getPropValue($propValues, 'EMAIL');
    $arResult['GENDER'] = project\order::getPropValue($propValues, 'GENDER');
    $arResult['BIRTHDAY'] = project\order::getPropValue($propValues, 'BIRTHDAY');

    $arResult['ADDRESS'] = project\order::getPropValue($propValues, 'ADDRESS');
    $arResult['KVARTIRA'] = project\order::getPropValue($propValues, 'KVARTIRA');
    $arResult['PODYEZD'] = project\order::getPropValue($propValues, 'PODYEZD');
    $arResult['FLOOR'] = project\order::getPropValue($propValues, 'FLOOR');

    $arResult['DELIVERY_NO_CALC'] = project\order::getPropValue($propValues, 'DELIVERY_NO_CALC');

    $arResult['FINAL_ADDRESS'] = [];
    if( strlen($arResult['ADDRESS']) > 0 ){
        $arResult['FINAL_ADDRESS'][] = $arResult['ADDRESS'];
    }
    if( strlen($arResult['KVARTIRA']) > 0 && $arResult['KVARTIRA'] != '-' ){
        $arResult['FINAL_ADDRESS'][] = 'кв.'.$arResult['KVARTIRA'];
    }
    if( strlen($arResult['PODYEZD']) > 0 && $arResult['PODYEZD'] != '-' ){
        $arResult['FINAL_ADDRESS'][] = 'подъезд '.$arResult['PODYEZD'];
    }
    if( strlen($arResult['FLOOR']) > 0 && $arResult['FLOOR'] != '-' ){
        $arResult['FINAL_ADDRESS'][] = 'этаж '.$arResult['FLOOR'];
    }
    $arResult['FINAL_ADDRESS'] = implode(', ', $arResult['FINAL_ADDRESS']);
    //$arResult['FINAL_ADDRESS'] = preg_replace('/\s*\[[^\[\]]+\]/', '', $arResult['FINAL_ADDRESS']);

    $arResult['PVZ'] = project\order::getPropValue($propValues, 'PVZ');
    if(
        strlen($arResult['PVZ']) > 0
        &&
        preg_match("/[^\[\]]+\[([1-9][0-9]*)\]/", $arResult['PVZ'], $matches, PREG_OFFSET_CAPTURE)
    ){
        if( intval($matches[1][0]) > 0 ){
            $arResult['PVZ'] = project\dealer_shop::getPVZDescription( $matches[1][0] );
			
			//адрес пункта выдачи
			$shop = tools\el::info($matches[1][0]);
			$arAddress = array();
			if( intval($shop['PROPERTY_REGION_ID_VALUE']) > 0 ){
				$loc = project\bx_location::getByID($shop['PROPERTY_REGION_ID_VALUE']);
				$arAddress['region_name'] = $loc['NAME_RU'];
			}
			if( intval($shop['PROPERTY_LOC_ID_VALUE']) > 0 ){
				$loc = project\bx_location::getByID($shop['PROPERTY_LOC_ID_VALUE']);
				$arAddress['loc_name'] = $loc['NAME_RU'];
			}
			if( strlen($shop['PROPERTY_STREET_VALUE']) > 0 ){
				$arAddress['street'] = 'ул.'.$shop['PROPERTY_STREET_VALUE'];
			}
			if( strlen($shop['PROPERTY_HOUSE_VALUE']) > 0 ){
				$arAddress['house'] = 'д.'.$shop['PROPERTY_HOUSE_VALUE'];
			}
			if( strlen($shop['PROPERTY_OFFICE_VALUE']) > 0 ){
				$arAddress['office'] = 'офис '.$shop['PROPERTY_OFFICE_VALUE'];
			}
			$arResult['PVZ_ADDRESS']  = implode(', ', $arAddress);

        }
    }

    $arResult['DELIV_DATE'] = project\order::getPropValue($propValues, 'DELIV_DATE');
    $arResult['DELIV_TIME'] = project\order::getPropValue($propValues, 'DELIV_TIME');
    $arResult['PAY_TYPE'] = project\order::getPropValue($propValues, 'PAY_TYPE');
    $arResult['LIFT'] = project\order::getPropValue($propValues, 'LIFT');

    $arResult['NEED_RAZGRUZKA'] = project\order::getPropValue($propValues, 'NEED_RAZGRUZKA');
    $arResult['NEED_PODYEM'] = project\order::getPropValue($propValues, 'NEED_PODYEM');
    $arResult['NEED_PERENOS'] = project\order::getPropValue($propValues, 'NEED_PERENOS');
    $arResult['PERENOS_DISTANCE'] = project\order::getPropValue($propValues, 'PERENOS_DISTANCE');

    $arResult['COMMENT'] = project\order::getPropValue($propValues, 'COMMENT');

    $arResult['DEALER_ID'] = project\order::getPropValue($propValues, 'DEALER_ID');
    $arResult['DEALER'] = tools\user::info($arResult['DEALER_ID']);

    $arResult["BASKET_CITY"] = project\order::getPropValue($propValues, 'CITY');
    $arResult["LOC_ID"] = project\order::getPropValue($propValues, 'LOC_ID');

    // Доп. инфо о дилере
    $arResult['DEALER_PHONES'] = $arResult['DEALER']['UF_PHONES'];
    if(
        strlen($arResult['DEALER']['UF_CONTACT_PERSONS']) > 2
        &&
        intval($arResult["LOC_ID"]) > 0
    ){
        $arResult['DEALER']['CONTACT_PERSONS'] = tools\funcs::json_to_array($arResult['DEALER']['UF_CONTACT_PERSONS']);
        if( is_array($arResult['DEALER']['CONTACT_PERSONS']) ){
            $arResult['DEALER_PHONES'] = [];
            foreach ( $arResult['DEALER']['CONTACT_PERSONS'] as $person ){
                if( $person['loc_id'] == $arResult["LOC_ID"] ){
                    $name = [];
                    if( strlen($person['name']) > 0 ){
                        $name[] = $person['name'];
                        if( strlen($person['second_name']) > 0 ){
                            $name[] = ''.$person['second_name'];
                        }
                        if( strlen($person['last_name']) > 0 ){
                            $name[] = ''.$person['last_name'];
                        }
                    }
                    $name = implode(' ', $name);

                    $arResult['DEALER_PHONES'][] = $name.(strlen($name)>0?'<br>':'').implode('<br>', $person['phones']);
                }
            }
        }
    }
    $arResult['DEALER_LOGO_SRC'] = false;
    if( intval($arResult['DEALER']['PERSONAL_PHOTO']) > 0 ){
        $arResult['DEALER_LOGO_SRC'] = 'http://'.$arResult['SERVER_NAME'].tools\funcs::rIMGG($arResult['DEALER']['PERSONAL_PHOTO'], 4, 100, 80);
    }


    ob_start();
    $APPLICATION->IncludeComponent("aoptima:mailHeader", "");
    $arResult['MAIL_HEADER'] = ob_get_contents();
    ob_end_clean();
    ob_start();
    $APPLICATION->IncludeComponent("aoptima:mailFooter", "");
    $arResult['MAIL_FOOTER'] = ob_get_contents();
    ob_end_clean();

}



$arResult['under_order_goods_names'] = [];
$arResult['under_order_goods'] = project\catalog::getUnderOrderGoods();
foreach ( $arResult['under_order_goods'] as $id => $el ){
    $el = project\catalog::updateProductName($el);
    $arResult['under_order_goods_names'][] = $el['NAME'];
}





$this->IncludeComponentTemplate();