<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>



<?=$arResult['MAIL_HEADER']?>



<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 32px; padding-right: 32px; text-align: left; width: 564px;">
            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                <tbody>
                <tr style="padding: 0; text-align: left; vertical-align: top;">
                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">

                        <h3 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: Arial, Helvetica, sans-serif; font-size: 22px; font-weight: normal; line-height: 26px; margin: 0; margin-bottom: 26px; padding: 0; text-align: left; word-wrap: normal;">Уважаемый(ая) <?=$arResult['NAME']?>,</h3>

                        <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Заказ №<?=$arResult['ORDER_ID']?> оформлен <?=$arResult['ORDER_DATE']?></p>

                        <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Дилер свяжется с Вами в течение 2-х часов в рабочее время для подтверждения заказа</p>

                    </th>
                </tr>
                </tbody>
            </table>
        </th>
    </tr>
    </tbody>
</table>

<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <th class="small-12 large-12 columns colored" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 13px; padding-right: 13px; text-align: left; width: 564px;">
            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                <tbody>
                <tr style="padding: 0; text-align: left; vertical-align: top;">
                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">

                        <table class="info-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                            <tbody>
<tr style="padding: 0; text-align: left; vertical-align: top;">
    <td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">

        <table>
            <tr>
                <td>Ваш заказ будет исполнен официальным дилером КНАУФ:<br><a class="cart-offer__table-dealerName" href="/dealers/<?=$arResult['DEALER']['ID']?>/" target="_blank"><?=strlen($arResult['DEALER']['UF_FULL_COMPANY_NAME'])>0?$arResult['DEALER']['UF_FULL_COMPANY_NAME']:$arResult['DEALER']['UF_COMPANY_NAME']?></a></td>
                <td>
                    <?php if( $arResult['DEALER_LOGO_SRC'] ){ ?>
                        <img src="<?=$arResult['DEALER_LOGO_SRC']?>">
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php if( count($arResult['DEALER_PHONES']) > 0 ){ ?>
                        Контактные телефоны:<br><?=implode('<br>', $arResult['DEALER_PHONES'])?>
                    <?php } ?>
                </td>
            </tr>
        </table>

    </td>
</tr>
                            </tbody>
                        </table>

                    </th>
                </tr>
                </tbody>
            </table>
        </th>
    </tr>
    </tbody>
</table>

<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <th class="small-12 large-12 columns colored" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 13px; padding-right: 13px; text-align: left; width: 564px;">
            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                <tbody>
                <tr style="padding: 0; text-align: left; vertical-align: top;">
                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">

                        <table class="info-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                            <tbody>
                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                <th colspan="2" style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 14px; padding-top: 8px; text-align: left;">Контактные данные</th>
                            </tr>
                            <?
                            if($arResult['LAST_NAME'])
                            {
                                ?>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">
                                        Фамилия:
                                    </td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?= $arResult['LAST_NAME'] ?></td>
                                </tr>
                                <?
                            }?>
                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Имя:</td>
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['NAME']?></td>
                            </tr>

                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Эл. почта:</td>
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['EMAIL']?></td>
                            </tr>

                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Телефон:</td>
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['PHONE']?></td>
                            </tr>

                            <? if( strlen($arResult['GENDER']) > 0 ){ ?>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Пол:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['GENDER']?></td>
                                </tr>
                            <? } ?>

                            </tbody>
                        </table>

                    </th>
                </tr>
                </tbody>
            </table>
        </th>
    </tr>
    </tbody>
</table>


<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <th class="small-12 large-12 columns colored" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 13px; padding-right: 13px; text-align: left; width: 564px;">
            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                <tbody>
                <tr style="padding: 0; text-align: left; vertical-align: top;">
                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">

                        <table class="info-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                            <tbody>
                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                <th colspan="2" style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 14px; padding-top: 8px; text-align: left;">Информация о заказе</th>
                            </tr>
                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Дилер:</td>
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><a class="cart-offer__table-dealerName" href="/dealers/<?=$arResult['DEALER']['ID']?>/" target="_blank"><?=strlen($arResult['DEALER']['UF_FULL_COMPANY_NAME'])>0?$arResult['DEALER']['UF_FULL_COMPANY_NAME']:$arResult['DEALER']['UF_COMPANY_NAME']?></a></td>
                            </tr>
                            <?
                            if($arResult['DS']['NAME'])
                            {
                                ?>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">
                                        Вариант доставки:
                                    </td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?= $arResult['DS']['NAME'] ?></td>
                                </tr>
                                <?
                            }?>
                            <?
                            if($arResult['PS']['NAME'])
                            {
                                ?>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">
                                        Вариант оплаты:
                                    </td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?= $arResult['PS']['NAME'] ?></td>
                                </tr>
                                <?
                            }
                            if($arResult['PAY_TYPE'])
                            {
                                ?>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">
                                        Тип оплаты:
                                    </td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?= $arResult['PAY_TYPE'] ?></td>
                                </tr>
                                <?
                            }?>
                            </tbody>
                        </table>

                    </th>
                </tr>
                </tbody>
            </table>
        </th>
    </tr>
    </tbody>
</table>


<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <th class="small-12 large-12 columns colored" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 13px; padding-right: 13px; text-align: left; width: 564px;">
            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                <tbody>
                <tr style="padding: 0; text-align: left; vertical-align: top;">
                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">
                        <h4 class="table-title" style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 19px; margin: 0; margin-bottom: 0; padding: 0; padding-bottom: 18px; padding-left: 18px; text-align: left; word-wrap: normal;">Состав заказа</h4></th>
                </tr>
                <tr style="padding: 0; text-align: left; vertical-align: top;">
                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                        <table class="colored-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                            <tbody>

<tr style="background-color: #f1efeb; padding: 0; text-align: left; vertical-align: top;">
    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-top: 8px; text-align: left;">
        №
    </th>
    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-top: 8px; text-align: left;">
        Наименование
    </th>
    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-top: 8px; text-align: left;">
        Кол-во, шт
    </th>
    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-top: 8px; text-align: left;">
        Цена, р
    </th>
    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left;">
        Сумма, р
    </th>
</tr>

<? $cnt = 0;
foreach( $arResult['arBasket'] as $bItem ){ $cnt++; ?>

    <tr style="background-color: #ffffff; padding: 0; text-align: left; vertical-align: top;">
        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$cnt?></td>
        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$bItem->product['NAME']?> (<?=$bItem->product['PROPERTY_ARTICLE_VALUE']?>)</td>
        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=number_format($bItem->getQuantity(), 0, ",", " ")?></td>
        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-top: 8px; text-align: left; vertical-align: top; white-space: nowrap; word-wrap: break-word;">
            <?
            $price = $bItem->getPrice();
            if(!$price)
            {
                echo "ПОДАРОК";
            }
            else
            {
                ?>
                <?= number_format($bItem->getPrice(), 2, ",", " ") ?>
                <?
            }?>
        </td>
        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; white-space: nowrap; word-wrap: break-word;">
            <?
            if($price)
            {
                ?>
                <?= number_format($bItem->getPrice() * $bItem->getQuantity(), 2, ",", " ") ?>
                <?
            }?>
        </td>
    </tr>

<? } ?>

<? if( $arResult['delivType'] == 'delivery' ){ ?>

    <tr style="background-color: #f1efeb; padding: 0; text-align: left; vertical-align: top;">
        <td colspan="5" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;">Стоимость товаров: <?=number_format($arResult['BASKET_SUM'], 2, ",", " ")?> руб.</td>
    </tr>

    <tr style="background-color: #f1efeb; padding: 0; text-align: left; vertical-align: top;">
        <td colspan="5" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;">
            Стоимость доставки:
            <?
            if($arResult['DELIVERY_NO_CALC'] == "Y")
            {
                echo 'по договоренности';
            }
            else
            {
                ?>
                <?= number_format($arResult['DELIVERY_PRICE'], 2, ",", " ") ?> руб.
                <?
            }?>
        </td>
    </tr>

<? } ?>

<tr style="background-color: #f1efeb; padding: 0; text-align: left; vertical-align: top;">
    <td colspan="5" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;">Итого: <?=number_format($arResult['PAY_SUM'], 2, ",", " ")?> руб.</td>
</tr>

                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </th>
    </tr>
    </tbody>
</table>


<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 0px; font-weight: normal; hyphens: auto; line-height: 22px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&amp;nbsp;</td>
    </tr>
    </tbody>
</table>


<?php if( count($arResult['under_order_goods_names']) > 0 ){ ?>

    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
        <tbody>
        <tr style="padding: 0; text-align: left; vertical-align: top;">
            <td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 0px; font-weight: normal; hyphens: auto; line-height: 22px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&amp;nbsp;</td>
        </tr>
        </tbody>
    </table>


    <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
        <tbody>
        <tr style="padding: 0; text-align: left; vertical-align: top;">
            <th class="small-12 large-12 columns colored" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 13px; padding-right: 13px; text-align: left; width: 564px;">
                <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                    <tbody>
                    <tr style="padding: 0; text-align: left; vertical-align: top;">
                        <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">
                            <h4 class="table-title" style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 19px; margin: 0; margin-bottom: 0; padding: 0; padding-bottom: 18px; padding-left: 18px; text-align: left; word-wrap: normal;">Товары под заказ</h4></th>
                    </tr>
                    <tr style="padding: 0; text-align: left; vertical-align: top;">
                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                            <table class="colored-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                                <tbody>

                                <tr style="background-color: #f1efeb; padding: 0; text-align: left; vertical-align: top;">
                                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-top: 8px; text-align: left;">№</th>
                                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-top: 8px; text-align: left;">Наименование</th>
                                </tr>

                                <? $cnt = 0;
                                foreach( $arResult['under_order_goods_names'] as $product_name ){ $cnt++; ?>

                                    <tr style="background-color: #ffffff; padding: 0; text-align: left; vertical-align: top;">
                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$cnt?></td>
                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$product_name?></td>
                                    </tr>

                                <? } ?>

                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </th>
        </tr>
        </tbody>
    </table>

<?php } ?>


<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <th class="small-12 large-12 columns colored" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 13px; padding-right: 13px; text-align: left; width: 564px;">
            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                <tbody>
                <tr style="padding: 0; text-align: left; vertical-align: top;">
                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">

                        <table class="info-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                            <tbody>
                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                <th colspan="2" style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 14px; padding-top: 8px; text-align: left;">Информация по доставке</th>
                            </tr>
                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;"><?
                                    if( strlen($arResult['PVZ']) > 0 ){
                                        echo 'Точка самовывоза';
                                    } else if( strlen($arResult['FINAL_ADDRESS']) > 0 ){
                                        echo 'Адрес доставки';
                                    } ?>:</td>
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?
                                    if( strlen($arResult['PVZ_ADDRESS']) > 0 ){
                                        echo $arResult['PVZ_ADDRESS'];
                                    } else if( strlen($arResult['FINAL_ADDRESS']) > 0 ){
                                        echo $arResult['FINAL_ADDRESS'];
                                    } ?></td>
                            </tr>

                            <? if( $arResult['delivType'] == 'delivery' ){ ?>

                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Дата доставки:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['DELIV_DATE']?></td>
                                </tr>

                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Время доставки:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['DELIV_TIME']?></td>
                                </tr>

                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Наличие лифта:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['LIFT']?></td>
                                </tr>

                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Нужна разгрузка:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['NEED_RAZGRUZKA']?></td>
                                </tr>

                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Нужен подъём:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['NEED_PODYEM']?></td>
                                </tr>

                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Нужен перенос:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['NEED_PERENOS']?></td>
                                </tr>

                                <? if( strlen($arResult['PERENOS_DISTANCE']) > 0 ){ ?>
                                    <tr style="padding: 0; text-align: left; vertical-align: top;">
                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Расстояие переноса, м.:</td>
                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['PERENOS_DISTANCE']?></td>
                                    </tr>
                                <? } ?>

                            <? } ?>

                            <? if( strlen($arResult['COMMENT']) > 0 ){ ?>

                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Комментарий:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['COMMENT']?></td>
                                </tr>

                            <? } ?>

                            </tbody>
                        </table>

                    </th>
                </tr>
                </tbody>
            </table>
        </th>
    </tr>
    </tbody>
</table>


<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 32px; padding-right: 32px; text-align: left; width: 564px;">
            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                <tbody>
                <tr style="padding: 0; text-align: left; vertical-align: top;">
                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">
                        <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Благодарим Вас за покупку на сайте <a href="http://<?=$arResult['SERVER_NAME']?>/" style="Margin: 0; color: #009fe3; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left; text-decoration: underline;">Маркетплейс «Купи КНАУФ»!</a></p>

                        <? if( $arResult['AUTH'] ){ ?>

                            <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Вы можете отслеживать статус заказа в <a href="http://<?=$arResult['SERVER_NAME']?>/personal/" style="Margin: 0; color: #009fe3; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left; text-decoration: underline;">личном кабинете</a> на&nbsp;вкладке «История заказов».</p>

                        <? } ?>

                    </th>
                </tr>
                </tbody>
            </table>
        </th>
    </tr>
    </tbody>
</table>



<?=$arResult['MAIL_FOOTER']?>
