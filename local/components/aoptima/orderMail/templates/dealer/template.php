<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>



<?=$arResult['MAIL_HEADER']?>



<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 32px; padding-right: 32px; text-align: left; width: 564px;">
            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                <tbody>
                <tr style="padding: 0; text-align: left; vertical-align: top;">
                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">
                        <h3 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: Arial, Helvetica, sans-serif; font-size: 22px; font-weight: normal; line-height: 26px; margin: 0; margin-bottom: 26px; padding: 0; text-align: left; word-wrap: normal;">Ваш новый заказ</h3>
                        <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">от маркетплейса «Купи КНАУФ»</p>

                    </th>
                </tr>
                </tbody>
            </table>
        </th>
    </tr>
    </tbody>
</table>


<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <th class="small-12 large-12 columns colored" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 13px; padding-right: 13px; text-align: left; width: 564px;">
            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                <tbody>
                <tr style="padding: 0; text-align: left; vertical-align: top;">
                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">

                        <table class="info-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                            <tbody>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <th colspan="2" style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 14px; padding-top: 8px; text-align: left;">Подробности по заказу</th>
                                </tr>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Номер заказа:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['ORDER_ID']?></td>
                                </tr>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Дата/время заказа:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['ORDER_DATE']?></td>
                                </tr>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Заказ будет исполнен дилером:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;">
                                        <a class="cart-offer__table-dealerName" href="/dealers/<?=$arResult['DEALER']['ID']?>/" target="_blank"><?=strlen($arResult['DEALER']['UF_FULL_COMPANY_NAME'])>0?$arResult['DEALER']['UF_FULL_COMPANY_NAME']:$arResult['DEALER']['UF_COMPANY_NAME']?></a>
                                        <?php if( $arResult['DEALER_LOGO_SRC'] ){ ?>
                                            <br><br>
                                            <img src="<?=$arResult['DEALER_LOGO_SRC']?>">
                                        <?php } ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </th>
                </tr>
                </tbody>
            </table>
        </th>
    </tr>
    </tbody>
</table>


<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <th class="small-12 large-12 columns colored" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 13px; padding-right: 13px; text-align: left; width: 564px;">
            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                <tbody>
                <tr style="padding: 0; text-align: left; vertical-align: top;">
                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">
                        <h4 class="table-title" style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 19px; margin: 0; margin-bottom: 0; padding: 0; padding-bottom: 18px; padding-left: 18px; text-align: left; word-wrap: normal;">Состав заказа</h4></th>
                </tr>
                <tr style="padding: 0; text-align: left; vertical-align: top;">
                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                        <table class="colored-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                            <tbody>

<tr style="background-color: #f1efeb; padding: 0; text-align: left; vertical-align: top;">
    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-top: 8px; text-align: left;">№</th>
    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-top: 8px; text-align: left;">Наименование</th>
    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-top: 8px; text-align: left;">Кол-во, шт</th>
    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-top: 8px; text-align: left;">Цена, р</th>
    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left;">Сумма, р</th>
</tr>

<? $cnt = 0;
foreach( $arResult['arBasket'] as $bItem ){ $cnt++; ?>

    <tr style="background-color: #ffffff; padding: 0; text-align: left; vertical-align: top;">
        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$cnt?></td>
        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$bItem->product['NAME']?> (<?=$bItem->product['PROPERTY_ARTICLE_VALUE']?>)</td>
        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=number_format($bItem->getQuantity(), 0, ",", " ")?></td>
        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-top: 8px; text-align: left; vertical-align: top; white-space: nowrap; word-wrap: break-word;">
            <?
            $price = $bItem->getPrice();
            if(!$price)
            {
                echo "ПОДАРОК";
            }
            else
            {
                ?>
                <?= number_format($bItem->getPrice(), 2, ",", " ") ?>
                <?
            }?>
        </td>
        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; white-space: nowrap; word-wrap: break-word;">
            <?
            if($price)
            {
                ?>
                <?= number_format($bItem->getPrice() * $bItem->getQuantity(), 2, ",", " ") ?>
                <?
            }?>
        </td>
    </tr>

<? } ?>

<? if( $arResult['delivType'] == 'delivery' ){ ?>

    <tr style="background-color: #f1efeb; padding: 0; text-align: left; vertical-align: top;">
        <td colspan="5" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;">Стоимость товаров: <?=number_format($arResult['BASKET_SUM'], 2, ",", " ")?> р.</td>
    </tr>

    <tr style="background-color: #f1efeb; padding: 0; text-align: left; vertical-align: top;">
        <td colspan="5" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;">
            Стоимость доставки:
            <?
            if($arResult['DELIVERY_NO_CALC'] == "Y")
            {
                echo 'по договоренности';
            }
            else
            {
                ?>
                <?= number_format($arResult['DELIVERY_PRICE'], 2, ",", " ") ?> руб.
                <?
            }?>
        </td>
    </tr>

<? } ?>

<tr style="background-color: #f1efeb; padding: 0; text-align: left; vertical-align: top;">
    <td colspan="5" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;">Итого: <?=number_format($arResult['PAY_SUM'], 2, ",", " ")?> р.</td>
</tr>

                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </th>
    </tr>
    </tbody>
</table>


<?php if( count($arResult['under_order_goods_names']) > 0 ){ ?>


    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
        <tbody>
        <tr style="padding: 0; text-align: left; vertical-align: top;">
            <td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 0px; font-weight: normal; hyphens: auto; line-height: 22px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&amp;nbsp;</td>
        </tr>
        </tbody>
    </table>


    <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
        <tbody>
        <tr style="padding: 0; text-align: left; vertical-align: top;">
            <th class="small-12 large-12 columns colored" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 13px; padding-right: 13px; text-align: left; width: 564px;">
                <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                    <tbody>
                    <tr style="padding: 0; text-align: left; vertical-align: top;">
                        <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">
                            <h4 class="table-title" style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 19px; margin: 0; margin-bottom: 0; padding: 0; padding-bottom: 18px; padding-left: 18px; text-align: left; word-wrap: normal;">Товары под заказ</h4></th>
                    </tr>
                    <tr style="padding: 0; text-align: left; vertical-align: top;">
                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                            <table class="colored-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                                <tbody>

                                <tr style="background-color: #f1efeb; padding: 0; text-align: left; vertical-align: top;">
                                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-top: 8px; text-align: left;">№</th>
                                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-top: 8px; text-align: left;">Наименование</th>
                                </tr>

                                <? $cnt = 0;
                                foreach( $arResult['under_order_goods_names'] as $product_name ){ $cnt++; ?>

                                    <tr style="background-color: #ffffff; padding: 0; text-align: left; vertical-align: top;">
                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$cnt?></td>
                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$product_name?></td>
                                    </tr>

                                <? } ?>

                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </th>
        </tr>
        </tbody>
    </table>

<?php } ?>


<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 0px; font-weight: normal; hyphens: auto; line-height: 22px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&amp;nbsp;</td>
    </tr>
    </tbody>
</table>


<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <th class="small-12 large-12 columns colored" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 13px; padding-right: 13px; text-align: left; width: 564px;">
            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                <tbody>
                <tr style="padding: 0; text-align: left; vertical-align: top;">
                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">

                        <table class="info-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                            <tbody>
                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                <th colspan="2" style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 14px; padding-top: 8px; text-align: left;">Информация о покупателе</th>
                            </tr>
                            <?
                            if($arResult['LAST_NAME'])
                            {
                                ?>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">
                                        Фамилия:
                                    </td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?= $arResult['LAST_NAME'] ?></td>
                                </tr>
                                <?
                            }?>

                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Имя:</td>
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['NAME']?></td>
                            </tr>

                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Эл. почта:</td>
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['EMAIL']?></td>
                            </tr>

                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Телефон:</td>
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['PHONE']?></td>
                            </tr>

                            <? if( strlen($arResult['GENDER']) > 0 ){ ?>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Пол:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['GENDER']?></td>
                                </tr>
                            <? } ?>

                            <? if( strlen($arResult['BIRTHDAY']) > 0 ){ ?>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Дата рождения:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['BIRTHDAY']?></td>
                                </tr>
                            <? } ?>

                            </tbody>
                        </table>

                    </th>
                </tr>
                </tbody>
            </table>
        </th>
    </tr>
    </tbody>
</table>


<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <th class="small-12 large-12 columns colored" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 13px; padding-right: 13px; text-align: left; width: 564px;">
            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                <tbody>
                <tr style="padding: 0; text-align: left; vertical-align: top;">
                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">

                        <table class="info-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                            <tbody>
                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                <th colspan="2" style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 14px; padding-top: 8px; text-align: left;">Информация по доставке</th>
                            </tr>
                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;"><?
                                    if( strlen($arResult['PVZ']) > 0 ){
                                        echo 'Точка самовывоза';
                                    } else if( strlen($arResult['FINAL_ADDRESS']) > 0 ){
                                        echo 'Адрес доставки';
                                    } ?>:</td>
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?
                                    if( strlen($arResult['PVZ_ADDRESS']) > 0 ){
                                        echo $arResult['PVZ_ADDRESS'];
                                    } else if( strlen($arResult['FINAL_ADDRESS']) > 0 ){
                                        echo $arResult['FINAL_ADDRESS'];
                                    } ?></td>
                            </tr>

                            <? if( $arResult['delivType'] == 'delivery' ){ ?>

                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Дата доставки:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['DELIV_DATE']?></td>
                                </tr>

                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Время доставки:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['DELIV_TIME']?></td>
                                </tr>

                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Наличие лифта:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['LIFT']?></td>
                                </tr>

                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Нужна разгрузка:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['NEED_RAZGRUZKA']?></td>
                                </tr>

                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Нужен подъём:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['NEED_PODYEM']?></td>
                                </tr>

                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Нужен перенос:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['NEED_PERENOS']?></td>
                                </tr>

                                <? if( strlen($arResult['PERENOS_DISTANCE']) > 0 ){ ?>
                                    <tr style="padding: 0; text-align: left; vertical-align: top;">
                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Расстояие переноса, м.:</td>
                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['PERENOS_DISTANCE']?></td>
                                    </tr>
                                <? } ?>

                            <? } ?>

                            <? if( strlen($arResult['COMMENT']) > 0 ){ ?>

                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #807366; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; min-width: 165px; padding: 0; padding-bottom: 8px; padding-left: 18px; padding-right: 22px; padding-top: 8px; text-align: left; vertical-align: top; width: 165px; word-wrap: break-word;">Комментарий:</td>
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; padding-bottom: 8px; padding-left: 10px; padding-right: 14px; padding-top: 8px; text-align: left; vertical-align: top; word-wrap: break-word;"><?=$arResult['COMMENT']?></td>
                                </tr>

                            <? } ?>

                            </tbody>
                        </table>

                    </th>
                </tr>
                </tbody>
            </table>
        </th>
    </tr>
    </tbody>
</table>


<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 32px; padding-right: 32px; text-align: left; width: 564px;">
            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                <tbody>
                <tr style="padding: 0; text-align: left; vertical-align: top;">
                    <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">
                        <p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Напоминаем Вам, что срок первой реакции на заказ не более 2 часов в рабочее время.<br> За это время Вам необходимо связаться с покупателем и поменять статус заказа в <a href="http://<?=$arResult['SERVER_NAME']?>/personal/#showtab-tab_111_5" style="Margin: 0; color: #009fe3; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left; text-decoration: underline;">Личном кабинете</a> (вкладка "Заказы").</p>
                    </th>
                </tr>
                </tbody>
            </table>
        </th>
    </tr>
    </tbody>
</table>



<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <th class="small-12 large-12 columns greyBlock" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 8px; text-align: left; width: 564px;">
            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                <tbody>
                <tr style="padding: 0; text-align: left; vertical-align: top;">
                    <th class="center-all" style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 19px; text-align: left;">

                        <table class="button" style="Margin: 0 0 16px 0; border-collapse: collapse; border-spacing: 0; margin: 0 0 16px 0; margin-bottom: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                            <tbody>
                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                                    <a href="http://<?=$arResult['SERVER_NAME']?>/personal/#showtab-tab_111_5" style="Margin: 0; background: #029ee5; border: 0 solid #2199e8; border-radius: 0px; color: #ffffff; display: inline-block; font-family: Arial, Helvetica, sans-serif; font-size: 18px; font-weight: normal; height: 40px; line-height: 1; margin: 0; padding: 0; padding-top: 20px; text-align: center; text-decoration: none; width: 100%;">Посмотреть детали и обработать заказ</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </th>
                </tr>
                </tbody>
            </table>
        </th>
    </tr>
    </tbody>
</table>




<?=$arResult['MAIL_FOOTER']?>
