<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<? if( $arResult['ERROR'] ){ ?>

    <p class="error___p"><?=$arResult['ERROR']?></p>

<? } else if( $arResult['SUCCESS'] ){ ?>

    <p class="success___p"><?=$arResult['SUCCESS']?></p>

    <p class="success___p">
        <a href="/auth/" style="color: green; text-decoration: underline;">Авторизация</a>
    </p>

<? } ?>
