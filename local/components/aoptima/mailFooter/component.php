<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

$arResult['SERVER_NAME'] = \Bitrix\Main\Config\Option::get('main', 'server_name');

global $USER;
if($USER)
{
    $arResult['AUTH'] = $USER->IsAuthorized();
}




$this->IncludeComponentTemplate();