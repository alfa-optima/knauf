<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="dealer__addresses-item shopItem" item_id="<?=$arResult['ID']?>">

    <h4 class="dealer__addresses-itemTitle <? if( strlen($arResult['PROPERTY_GPS_VALUE']) > 0 ){} else { echo 'no___label'; } ?>" <? if( strlen($arResult['PROPERTY_GPS_VALUE']) > 0 ){ echo 'style="cursor: pointer"'; } ?>><?=$arResult['NAME']?></h4>

    <div class="dealer__addresses-row">

        <div class="dealer__addresses-column">
            <!--
            <div class="dealer__addresses-stars">
                <span class="dealer__addresses-star  dealer__addresses-star--full"></span>
                <span class="dealer__addresses-star  dealer__addresses-star--full"></span>
                <span class="dealer__addresses-star  dealer__addresses-star--full"></span>
                <span class="dealer__addresses-star  dealer__addresses-star--full"></span>
                <span class="dealer__addresses-star"></span>
            </div>
            -->
            <span><?=$arResult['ADDRESS']?></span>
        </div>

        <div class="dealer__addresses-column">
            <? if( strlen($arResult['DEALER']['UF_COMPANY_NAME']) > 0 ){ ?>
                <span><?=$arResult['DEALER']['UF_COMPANY_NAME']?></span>
            <? } ?>
            <? if( is_array( $arResult['PHONES'] ) ){
                foreach ( $arResult['PHONES'] as $key => $phone ){ ?>
                    <span><?=$phone?></span>
                <? }
            } ?>
            <? if( strlen($arResult['PROPERTY_EMAIL_VALUE']) > 0 ){ ?>
                <a href="mailTo:<?=$arResult['PROPERTY_EMAIL_VALUE']?>" onclick="ym(55379437, 'reachGoal', 'clickDealerEmail', '<?=strlen($arResult['DEALER']['UF_COMPANY_NAME'])>0?$arResult['DEALER']['UF_COMPANY_NAME']:$arResult['LOGIN']?>'); return true;"
                ><?=$arResult['PROPERTY_EMAIL_VALUE']?></a>
            <? } ?>
            <? if( strlen($arResult['PROPERTY_SITE_VALUE']) > 0 ){ ?>
                <a href="<?=$arResult['PROPERTY_SITE_VALUE']?>" target="_blank" onclick="ym(55379437, 'reachGoal', 'goToDealer', '<?=strlen($arResult['DEALER']['UF_COMPANY_NAME'])>0?$arResult['DEALER']['UF_COMPANY_NAME']:$arResult['LOGIN']?>'); return true;"
                ><?=$arResult['PROPERTY_SITE_VALUE']?></a>
            <? } ?>
        </div>

        <div class="dealer__addresses-column">

            <span><?=str_replace("\n", "<br>", $arResult['PROPERTY_REZHIM_RABOTY_VALUE']['TEXT'])?></span>

        </div>

    </div>
</div>