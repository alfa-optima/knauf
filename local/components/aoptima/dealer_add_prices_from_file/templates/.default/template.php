<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<form class="loadCSVForm" method="post" enctype="multipart/form-data" target="upload___Frame" action="<?=PREFIX?>/ajax/uploadDealerCSVFile.php">

    <article class="block  block--top">
        <div class="block__wrapper">
            <h2 class="block__title  block__title--search">Загрузка цен из файла (CSV)</h2>
            <? if( $arResult['FILE_EXISTS'] ){ ?>
                <div style="text-align:center; margin-top:30px;">
                    <a href="<?=$arResult['DEALER_CATALOG_FILE_PATH']?>" class="account-form__button" download target="_blank" >Скачать текущий каталог (CSV)</a>
                </div>
            <? } ?>
        </div>
    </article>

    <section class="block  block--accountForm">

        <div class="block__wrapper">

            <section class="account-form">

                <div class="account-form__tabs">

                    <article class="tab first" id="tab_111_1">

                        <div class="dealerAccount-form__wrapper csvFormBlock">

                            <div class="dealerAccount-form__block">

                                <div class="dealerAccount-form__field  dealerAccount-form__field--w271">
                                    <span>Регион для цен:</span>
                                    <input
                                        class="account-form__suggestInput"
                                        id="fileRegion"
                                        type="text"
                                        process="N"
                                        name="fileRegion"
                                        loc_name=""
                                        onfocus="regionFocus($(this));"
                                        onkeyup="regionKeyUp($(this));"
                                        onfocusout="regionFocusOut($(this));"
                                        autocomplete="off"
                                        placeholder="Укажите регион"
                                    >
                                    <input type="hidden" name="loc_id">
                                    <script>
                                        $(document).ready(function(){
                                            $('#fileRegion').autocomplete({
                                                source: '/ajax/searchRegion.php',
                                                minLength: 2,
                                                delay: 700,
                                                select: regionSelect
                                            })
                                        })
                                    </script>
                                </div>

                                <div class="dealerAccount-form__field  dealerAccount-form__field--w271">
                                    <span>Файл CSV:</span>
                                    <div class="goods__top-upload">
                                        <label>
                                            <input type="file" name="csv" accept=".csv">
                                            <span>Выбрать файл CSV</span>
                                            <span>Выбрать</span>
                                        </label>
                                    </div>
                                </div>

                                <p class="error___p"><?=count($arResult['ERRORS'])>0?implode('<br>', $arResult['ERRORS']):''?></p>

                                <a style="cursor: pointer;" class="account-form__button loadCSVButton to___process">Загрузить</a>

                            </div>

                        </div>

                        <div id='loadFileWindow' style="display:none;" go="Y">

                            <h1 style="text-align: center;">Импорта файла...</h1>

                            <p class="warning___p">Внимание! Не закрывайте страницу до окончания процесса!</p>

                            <div class="aoptima_progress_bar_block">
                                <div class="status___block">
                                    <p class="loading___status">0%</p>
                                </div>
                                <div class="aoptima_progress_bar"></div>
                            </div>

                            <div class="success___block"></div>

                            <p class="error___p" style="text-align: center;"></p>

                        </div>

                    </article>

                </div>

            </section>

        </div>

    </section>

</form>

<iframe id="upload___Frame" name="upload___Frame" style="display: none"></iframe>