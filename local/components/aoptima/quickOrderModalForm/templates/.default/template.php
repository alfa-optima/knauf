<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;
?>

<form class="quickOrderForm" onsubmit="return false">

    <div class="modal-qOrder__product">
        <a href="<?=PREFIX.$arResult['PRODUCT']["DETAIL_PAGE_URL"]?>" class="modal-qOrder__product-img">
            <img src="<?=$arResult["PICTURE"]?>" alt="">
        </a>
        <h3 class="modal-qOrder__product-title"><?=$arResult['PRODUCT']["NAME"]?></h3>
    </div>

    <div class="account-form__row  account-form__row--flexend  account-form__row--mtMinus62">
        <div class="account-form__field">
            <span>Кол-во, шт.*</span>
            <input type="text" name="number" value="<?=$arResult['QUANTITY']?>">
        </div>
        <div class="account-form__field">
            <?
            if($arResult["SIZE_ONE"])
            {
            ?>
                <?=$arResult["SIZE_NAME"]?><input type="text" name="meters" data-size_one="<?= $arResult["SIZE_ONE"] ?>"
                                value="<?= number_format($arResult["SIZE_ONE"] * $arResult['QUANTITY'], 2, ".", "") ?>">
                <?
            }?>
        </div>
    </div>

    <div class="account-form__row">
        <div class="account-form__field">
            <span>Город *</span><input type="text" name="city" value="<?=$_SESSION['LOC']['CITY']?>">
        </div>

        <div class="account-form__field  account-form__field--fg1">
            <span>Адрес</span><input type="text" value="" name="address">
        </div>
    </div>

    <div class="account-form__row">
        <div class="account-form__field">
            <span>ФИО *</span><input type="text" name="fio" value="<?=$arResult['USER']['NAME'].' '.$arResult['USER']['LAST_NAME']?>">
        </div>
        <div class="account-form__field">
            <span>Телефон *</span><input type="tel" name="phone" value="<?=$arResult['USER']['PERSONAL_PHONE']?>">
        </div>
        <div class="account-form__field  account-form__field--wa  account-form__field--fg1">
            <span>Эл. почта *</span><input type="email" name="email" value="<?=$arResult['USER']['EMAIL']?>">
        </div>
    </div>

    <div class="account-form__field  account-form__field--wa">
        <div class="account-form__checkbox">
            <input type="checkbox" id="agree_78" name="agree" value="1">
            <label for="agree_78">Я&nbsp;прочитал и&nbsp;принял условия <a href="https://www.knauf.ru/about/confidentiality/" target="_blank">конфиденциальности данных</a> и&nbsp;даю согласие на&nbsp;обработку своих персональных данных *</label>
        </div>
    </div>
    <div class="account-form__field  account-form__field--wa">
        <div class="account-form__checkbox">
            <input type="checkbox" id="agree_781" name="agree1" value="1">
            <label for="agree_781">Я&nbsp;принимаю условия <a href="<?=PREFIX?>/user_agreement/" target="_blank">Пользовательского соглашения</a> *</label>
        </div>
    </div>
    <div class="account-form__field  account-form__field--wa">
        <div class="account-form__checkbox">
            <input type="checkbox" id="agree_782" name="agree2" value="1">
            <label for="agree_782">Подписаться на рассылку информационных материалов</label>
        </div>
    </div>

    <div>
        <p class="error___p"></p>
    </div>

    <div class="modal-qOrder__dealers">
        <h2 class="modal-qOrder__title">Предложения дилеров</h2>
        <?
        foreach( $arResult['DEALERS'] as $arDealer )
        {
            ?>
            <div class="modal-qOrder__dealer" data-quantity="<?=$arDealer["QUANTITY"]?>">
                <div class="cart-offer__table-dealer">
                    <a class="cart-offer__table-dealerName"><?=$arDealer['UF_COMPANY_NAME']?></a>
                    <div class="dealer__review-stars">
                        <? for ( $r = 1; $r <=5; $r++ ){ ?>
                            <span class="dealer__review-star  <? if( $arDealer['UF_RATING'] && $arDealer['UF_RATING'] >= $r ){ ?>dealer__review-star--full<? } ?>"></span>
                        <? } ?>
                    </div>
                </div>
                <div class="modal-qOrder__options" style="width:min-content;">
                    <?
                    if($arDealer["PAYMENTS"])
                    {
                        ?>
                        <div class="modal-qOrder__option">
                            <span class="modal-qOrder__option-leftCol">Оплата</span>
                            <div class="modal-qOrder__option-rightCol">
                                <div class="modal-qOrder__paymentIcons">
                                    <?
                                    foreach($arDealer["PAYMENTS"] as $arPay)
                                    {
                                        ?>
                                        <img style="max-width:100px;max-height:50px;"
                                             title="<?=$arPay["NAME"]?>"
                                             src="<?=CFile::GetPath($arPay["PSA_LOGOTIP"]);?>" alt="">
                                        <?
                                    }?>
                                </div>
                            </div>
                        </div>
                        <?
                    }
                    if($arDealer['hasDelivery'])
                    {
                        ?>
                        <div class="modal-qOrder__option">
                            <span class="modal-qOrder__option-leftCol">Доставка</span>
                            <div class="modal-qOrder__option-rightCol">
                                <div class="account-form__checkbox">
                                    <?
                                    $arDelivData = [];
                                    if($arDealer['deliveryLocation']['UF_MIN_DELIV_DAYS'])
                                    {
                                        $arDelivData[] = 'от '.$arDealer['deliveryLocation']['UF_MIN_DELIV_DAYS']
                                            .' '.tools\funcs::pluralForm(
                                                    $arDealer['deliveryLocation']['UF_MIN_DELIV_DAYS'],
                                                    "дня",
                                                    "дней",
                                                    "дней"
                                            );
                                    }
                                    if($arDealer['free_delivery_basket_sum'])
                                    {
                                        $arDelivData[] = 'бесплатная доставка от '.$arDealer['free_delivery_basket_sum'].' ₽';
                                    }
                                    if($arDelivData)
                                    {
                                        ?>
                                        <label for="delivery">
                                            <?=implode(", ", $arDelivData);?>
                                        </label>
                                        <?
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?
                    }
                    if($arDealer['hasPickup'])
                    {
                        ?>
                        <div class="modal-qOrder__option">
                            <span class="modal-qOrder__option-leftCol">Самовывоз</span>

                            <div class="check-fast-order-features"></div>

                            <div class="modal-qOrder__option-rightCol">
                            </div>
                        </div>
                        <?
                    }?>
                    <?
                    if($arDealer['delivery_dop_services'])
                    {
                        ?>
                        <div class="modal-qOrder__option">
                            <span class="modal-qOrder__option-leftCol"><?=tools\funcs::mb_ucfirst(implode(", ", $arDealer['delivery_dop_services']))?></span>

                            <div class="check-fast-order-features"></div>

                            <div class="modal-qOrder__option-rightCol">
                            </div>
                        </div>
                        <?
                    }?>
                    <div class="modal-qOrder__option quantity_warning" style="color:red;">
                        <span class="modal-qOrder__option-leftCol"></span>
                        <div class="modal-qOrder__option-rightCol">
                        </div>
                    </div>
                    <div class="modal-qOrder__option">
                        <?
                        /*if($arDealer['delivery_dop_services'])
                        {
                            */?><!--
                            <span class="modal-qOrder__option-leftCol"><?/*=implode(", ", $arDealer['delivery_dop_services'])*/?></span>
                            --><?/*
                        }*/?>
                        <div class="modal-qOrder__option-rightCol">
                            <div class="modal-qOrder__total">
                                <?
                                if($arDealer["PRICE"])
                                {
                                    ?>
                                    <div class="cart-offer__table-total" style="width:130px;">
                                        <span class="cart-offer__table-totalText">Итого</span><span
                                                class="cart-offer__table-totalPrice"
                                        ><span data-price="<?=$arDealer["PRICE"]?>"
                                               class="fastOrderPrice"
                                            ><?=$arDealer["PRICE"] * $arResult['QUANTITY']?></span>&nbsp;₽</span>
                                    </div>
                                    <?
                                }?>
                                <a class="cart-offer__table-button quickOrderButton" data-id="<?=$arDealer['ID']?>">Оформить заказ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?
        }?>
    </div>

    <input type="hidden" name="dealer" value="">
    <input type="hidden" name="product_id" value="<?=$arResult['PRODUCT']['ID']?>">
    <input type="hidden" name="product_name" value="<?=$arResult['PRODUCT']['NAME']?>">

    <input type="hidden" name="where_from" value="<?=IS_ESHOP?'APP':'SITE'?>">

    <p class="account-form__note">* Пожалуйста, заполните все обязательные поля</p>

</form>

<script>
    $(document).ready(function() {
        if ($('.modal-qOrder__paymentIcons  img').length) {
            $('.modal-qOrder__paymentIcons  img').tooltipster({
                animation: 'fade',
                delay: 100,
                maxWidth: 200,
                side: 'top',
                distance: 2,
                theme: 'tooltipster-white'
            });
        }
    });
</script>