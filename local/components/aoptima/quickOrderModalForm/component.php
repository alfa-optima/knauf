<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if( $USER->IsAuthorized() ){

    $arResult['USER'] = tools\user::info( $USER->GetID() );

}

if( intval($_POST['item_id']) > 0 && strlen($_POST['dealer_ids']) > 0 ){
    $arResult['PRODUCT'] = tools\el::info(intval($_POST['item_id']));
    if( intval($arResult['PRODUCT']['ID']) > 0 ){

        // получим местоположение пользователя
        global $KNAUF_LOC;
        if( intval($KNAUF_LOC['CITY_LOC_ID']) > 0 ){
            // Инфо по местоположению
            $arLoc = project\bx_location::getByID( $KNAUF_LOC['CITY_LOC_ID'] );
            if( intval($arLoc['ID']) > 0 ){
                // Родители местоположения
                $arLocChain = project\bx_location::getLocChain($arLoc['ID']);
                // Исключим из цепочки страну (пока страна не нужна)
                foreach ( $arLocChain as $key => $item ){
                    if( $item['PARENTS_TYPE_NAME_RU'] == 'Страна' ){
                        unset($arLocChain[$key]);
                    }
                }
                $arLocChainIDs = array_reverse(array_keys($arLocChain));
            }
        }

        $dealer_ids = explode('|', $_POST['dealer_ids']);
        $arResult['DEALERS'] = [];
        foreach ( $dealer_ids as $dealer_id ){

            $arDealerIDandPrice = explode("_", $dealer_id);
            $dealer_id = $arDealerIDandPrice[0];
            $dealer_price = $arDealerIDandPrice[1];
            $dealer_quantity = $arDealerIDandPrice[2];

            $dealer = tools\user::info($dealer_id);
            if( intval($dealer['ID']) > 0 ){

                // Варианты оплаты для дилера
                $dealer['PAYMENTS'] = array();
                if( count($dealer['UF_PSS']) > 0 ){
                    $db_ptype = \CSalePaySystem::GetList(
                        Array(
                            "SORT"=>"ASC",
                            "PSA_NAME"=>"ASC"
                        ),
                        Array(
                            "ACTIVE"=>"Y",
                            "CODE" => $dealer['UF_PSS']
                        ),
                        false,
                        false,
                        array('ID', 'NAME', 'CODE', 'PSA_LOGOTIP', 'DESCRIPTION')
                    );
                    while ($ps = $db_ptype->GetNext()){

                        $ps['PAY_TYPES'] = [];

                        if( is_array($dealer_pay_types[$ps['CODE']]) && count($dealer_pay_types[$ps['CODE']]) > 0 ){
                            foreach ( $dealer_pay_types[$ps['CODE']] as $pay_type ){
                                if( $pay_type == 'pre' ){   $ps['PAY_TYPES'][] = 'Предоплата';   }
                                if( $pay_type == 'post' ){   $ps['PAY_TYPES'][] = 'Постоплата';   }
                            }
                        }

                        $dealer['PAYMENTS'][$ps['ID']] = $ps;
                    }
                }

                $dealer["PRICE"] = $dealer_price;
                $dealer["QUANTITY"] = $dealer_quantity;

                // получим для дилера данные о доставке
                $dealer['deliveryLocation'] = false;
                $dl = new project\delivery_location();
                foreach( $arLocChainIDs as $loc_id ) {
                    if( !$dealer['deliveryLocation'] ){
                        $deliveryLocations = $dl->getList( $dealer['ID'], $loc_id );
                        if( count($deliveryLocations) > 0 ){
                            $dealer['deliveryLocation'] = $deliveryLocations[array_keys($deliveryLocations)[0]];
                        }
                    }
                }

                // получим список ПВЗ
                $dealer_shop = new project\dealer_shop();
                $dealer['PVZ_LIST'] = $dealer_shop->getList(
                    $dealer['ID'],
                    $arLoc['ID'],
                    true
                );

                // самовывоз
                $dealer['hasPickup'] = ($dealer['PVZ_LIST'] && count($dealer['PVZ_LIST']) > 0);

                // доставка
                $dealer['hasDelivery'] = !!$dealer['deliveryLocation'];

                // все настройки доставки
                $arDelivSettings = [];
                if( strlen($dealer['deliveryLocation']['UF_DELIV_SETTINGS']) > 0 ){
                    $arDelivSettings = tools\funcs::json_to_array($dealer['deliveryLocation']['UF_DELIV_SETTINGS']);
                }

                // бесплатная доставка от
                $dealer['free_delivery_basket_sum'] = $arDelivSettings['free_delivery_basket_sum'];

                // есть ли у дилера разгрузка, перенос, подъем
                $dealer['delivery_dop_services'] = [];
                if(
                    is_array($arDelivSettings['dop_services'])
                    &&
                    in_array('unloading_of_goods', $arDelivSettings['dop_services'])
                )
                {
                    $dealer['delivery_dop_services'][] = "разгрузка";
                }
                if(
                    is_array($arDelivSettings['dop_services'])
                    &&
                    in_array('transfer_of_goods', $arDelivSettings['dop_services'])
                )
                {
                    $dealer['delivery_dop_services'][] = "перенос";
                }
                if(
                    is_array($arDelivSettings['dop_services'])
                    &&
                    in_array('rise_to_the_floor', $arDelivSettings['dop_services'])
                )
                {
                    $dealer['delivery_dop_services'][] = "подъем";
                }


                $arResult['DEALERS'][ $dealer['ID'] ] = $dealer;
            }
        }

        $arResult['QUANTITY'] = $_POST["quantity"]?$_POST["quantity"]:1;
        $arResult["PICTURE"] = tools\funcs::rIMGG($arResult['PRODUCT']['DETAIL_PICTURE'], 4, 204, 153);

        $arResult["SIZE_ONE"] = "";
        $arResult["SIZE_NAME"] = "";
        if($arResult['PRODUCT']["PROPERTY_PRODUCT_TYPE_VALUE"] == "лист")
        {
            $arResult["SIZE_ONE"] = ($arResult['PRODUCT']["PROPERTY_LENGTH_VALUE"]/1000) * ($arResult['PRODUCT']["PROPERTY_WIDTH_VALUE"]/1000);
            $arResult["SIZE_NAME"] = "<span>Кол-во, м<sup>2</sup></span>";
        }
        elseif($arResult['PRODUCT']["PROPERTY_PRODUCT_TYPE_VALUE"] == "профиль")
        {
            $arResult["SIZE_ONE"] = $arResult['PRODUCT']["PROPERTY_LENGTH_VALUE"]/1000;
            $arResult["SIZE_NAME"] = "<span>Кол-во, м.п.</span>";
        }

        $this->IncludeComponentTemplate();
    }
}





