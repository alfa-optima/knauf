<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if(
    isset($_POST['url'])
    &&
    intval($_POST['section_id']) > 0
){

    $section = tools\section::info(intval($_POST['section_id']));

    preg_match("/(\/catalog\/.*)/", trim(strip_tags($_POST['url'])), $matches, PREG_OFFSET_CAPTURE);
    $url = $matches[1][0];

    if( preg_match("/(\/filter\/.*\/apply\/)/", $url, $matches, PREG_OFFSET_CAPTURE) ){

        $obFilterPage = new project\filter_page();
        $arResult['filterPage'] = $obFilterPage->getByUrl( tools\funcs::pureURL( $url ) );

        $this->IncludeComponentTemplate();

    } else {

        echo "<script>$.noty.closeAll(); var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Данная страница не является страницей фильтра'});</script>";

    }
}






