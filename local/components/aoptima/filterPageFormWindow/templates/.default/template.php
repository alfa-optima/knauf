<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

<a href="javascript:;" onclick="$.fancybox.close();" class="modal-review__close">Закрыть</a>
<form class="filterPageForm" onsubmit="return false">

    <div class="modal-orderReview__block">

        <div class="account-form__field  account-form__field--w100">
            <span>H1</span>
            <input name="UF_H1" placeholder="H1" value="<?=$arResult['filterPage']['UF_H1']?>">
        </div>

        <div class="account-form__field  account-form__field--w100">
            <span>SEO title</span>
            <input name="UF_META_TITLE" placeholder="Seo title" value="<?=$arResult['filterPage']['UF_META_TITLE']?>">
        </div>

        <div class="account-form__field  account-form__field--w100">
            <span>SEO description</span>
            <input name="UF_META_DESCRIPTION" placeholder="SEO description" value="<?=$arResult['filterPage']['UF_META_DESCRIPTION']?>">
        </div>

        <?php if( 0 ){ ?>

            <div class="account-form__field  account-form__field--w100">
                <span>SEO keywords</span>
                <input name="UF_META_KEYWORDS" placeholder="SEO keywords" value="<?=$arResult['filterPage']['UF_META_KEYWORDS']?>">
            </div>

        <?php } ?>

        <div class="account-form__field  account-form__field--w100">
            <span>Текст страницы</span>
            <textarea name="UF_TEXT" placeholder="Текст отзыва"><?=$arResult['filterPage']['UF_TEXT']?></textarea>
        </div>

        <p class="error___p"></p>

        <input type="hidden" name="ID" value="<?=$arResult['filterPage']['ID']?>">

        <div>

            <?php if( intval($arResult['filterPage']['ID']) > 0 ){ ?>
                <input class="modal-review__button deleteFilterPage to___process" type="button" value="Удалить">
                <input class="modal-review__button saveFilterPage to___process" type="button" value="Сохранить">
            <?php } else { ?>
                <input class="modal-review__button saveFilterPage to___process" type="button" value="Создать">
            <? } ?>

        </div>


    </div>

</form>
