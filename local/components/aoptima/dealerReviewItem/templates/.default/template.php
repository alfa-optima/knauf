<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$review = $arResult; ?>

<tr class="dealarReviewItem" item_id="<?=$review['ID']?>">
    <td>
        <div><span><?=$review['UF_DEALER_VOTE']?></span><?=strlen($review['USER']['FIO'])>0?$review['USER']['FIO']:'Пользователь'?> [<?=$review['USER']['ID']?>]</div>
    </td>
    <td>
        <span class="account-form__table-goodsQuantity"><?=$review['ORDER']['BASKET_CNT_STR']?></span>
        <span class="tab__date  account-form__table-mobTh"><?=ConvertDateTime($review['UF_DATE'], "DD.MM.YYYY HH:MI", "ru")?></span>
    </td>
    <td>
        <p><?=$review['UF_TEXT']?></p>
        <span class="tab__date"><?=ConvertDateTime($review['UF_DATE'], "DD.MM.YYYY HH:MI", "ru")?></span>
    </td>
</tr>

<tr class="account-form__table-orderInner">
    <td colspan="3">
        <table class="account-form__table-order">
            <tbody>

                <? foreach( $review['ORDER']['arBasket'] as $key => $basketItem ){ ?>

                    <tr>
                        <td>
                            <div class="account-form__table-orderWrapper">
                                <div class="account-form__table-orderName">
                                    <? if( $basketItem['product_img'] ){ ?>
                                        <a href="<?=$basketItem['product']['DETAIL_PAGE_URL']?>">
                                            <img src="<?=$basketItem['product_img']?>">
                                        </a>
                                    <? } ?>
                                    <a href="<?=$basketItem['product']['DETAIL_PAGE_URL']?>"><?=$basketItem['product']['NAME']?></a>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div>
                                <?=$basketItem['sumFormat']?> <span class="rub">₽</span>
                            </div>
                        </td>
                    </tr>

                <? } ?>

            </tbody>
        </table>
        <div class="account-form__table-order-total">
            <div class="account-form__row">
                <div class="cart-form__infoColumn">
                    <span class="cart-form__infoColumn-title">Стоимость доставки: <? if( $review['ORDER']['DELIVERY_SUM']==0 ){ echo 'бесплатно'; } else { ?><?=$review['ORDER']['DELIVERY_SUM_FORMAT']?><span class="rub">₽</span><? } ?></span>
                    <? if( $review['ORDER']['LIFT'] ){ ?>
                        <span class="cart-form__infoColumn-text"><?=$review['ORDER']['LIFT']?></span>
                    <? } ?>
                </div>
                <? if( $review['ORDER']['DELIV_DATE'] ){ ?>
                    <div class="cart-form__infoColumn">
                        <span class="cart-form__infoColumn-title">Дата доставки</span>
                        <span class="cart-form__infoColumn-text"><?=$review['ORDER']['DELIV_DATE']?></span>
                    </div>
                <? } ?>
                <? if( $review['ORDER']['DELIV_TIME'] ){ ?>
                    <div class="cart-form__infoColumn">
                        <span class="cart-form__infoColumn-title">Время доставки</span>
                        <span class="cart-form__infoColumn-text"><?=$review['ORDER']['DELIV_TIME']?></span>
                    </div>
                <? } ?>
                <div class="cart-form__infoColumn-sum">
                    <span>Итого:</span>
                    <span><?=$review['ORDER']['TOTAL_SUM_FORMAT']?><span class="rub">₽</span></span>
                </div>
            </div>
        </div>
    </td>
</tr>

<? if( 1==2 ){ ?>
    <tr class="account-form__table-reviewsBtn">
        <td colspan="3">
            <div class="account-form__button-wrapper">
                <a style="cursor: pointer;" class="account-form__button">Ответить</a>
            </div>
        </td>
    </tr>
<? } ?>



