<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

global $USER;
$userID = $USER->GetID();
//Добавление подчиненных пользователей
$currentUser = new project\user( $USER->GetID() );
$userProps = $currentUser->arUser;
if(strlen($userProps["UF_MAIN_USER"]) > 0){
	$user = new project\user( $userProps["UF_MAIN_USER"] );
	if( $user->isDealer() ){
		$userID = $user->arUser["ID"];
	}
}
$arResult['USER'] = tools\user::info($userID);


// Местоположения дилера
$delivery_location = new project\auto_upd_prices_location();
$arResult['ITEMS'] = $delivery_location->getList( $userID );
foreach ( $arResult['ITEMS'] as $key => $item ){
    $item['LOC'] = project\bx_location::getByID($item['UF_LOC_ID']);
    $item['SETTINGS'] = [];
    if( strlen($item['UF_SETTINGS']) > 0 ){
        $item['SETTINGS'] = tools\funcs::json_to_array( $item['UF_SETTINGS'] );
    }
    $arResult['ITEMS'][$key] =  $item;
}





$this->IncludeComponentTemplate();