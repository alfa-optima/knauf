<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project; ?>

<div class="autoUpdateLocationsBlock">

    <div class="dealerAccount-form__wrapper  dealerAccount-form__wrapper--delivery">
        <h4 class="tab__title">Регионы для автообновления цен</h4>
        <a href="javascript:;" onclick="$('.newDealerAutoUpdateLocLink').fancybox({
            padding: 0,
            wrapCSS: 'fancybox-review'
        }) .trigger('click'); $('#modal-auto-update-region .error___p').html('');" class="dealerAccount-delivery__button">Добавить регион</a>
    </div>

    <section class="dealerAccount-delivery">

        <? foreach( $arResult['ITEMS'] as $key => $item ){ ?>

            <form class="pricesAutoUpdateSettingsForm" onsubmit="return false;">

                <div class="dealerAccount-delivery__inner">

                    <div class="dealerAccount-delivery__header">
                        <div class="dealerAccount-delivery__header-wrapper" style="margin-left: 30px!important; padding-left: 0!important;">

                            <div class="dealerAccount-delivery__regionWrapper" style="width: 300px;">
                                <span class="dealerAccount-delivery__regionName"><?=$item['LOC']['NAME_RU'].' ('.$item['LOC']['PARENT_NAME_RU'].')'?></span>
                            </div>

                            <span class="dealerAccount-prices_auto_update_save___link pricesAutoUpdateLocationSaveButton to___process" item_id="<?=$item['ID']?>">Сохранить</span>

                            <span class="dealerAccount-delivery__link--del removePricesAutoUpdateLocationButton to___process" item_id="<?=$item['ID']?>">Удалить</span>

                            <div style="clear:both"></div>

                        </div>

                        <div class="dealerAccount-delivery__header-wrapper" style="margin-top: 0!important; margin-left: 0!important; padding-left: 0!important; padding-top: 0!important;">

                            <input class="dealerAutoUpdatePricesInput" placeholder="Ссылка на файл YML" type="text" name="UF_FILE_LINK" style="width: 90%!important; margin-left: 30px!important;" value="<?=$item['UF_FILE_LINK']?>">

                        </div>
						<!--<div style="text-align:center">
							<a href="/include/files/knauf_example.xml" style="cursor: pointer;" class="account-form__button" target="_blank" download="">Скачать образец файла YML 3</a>
						</div>-->
                    </div>

                </div>

            </form>

        <? } ?>

    </section>

				<div style="text-align:center">
					<a href="/include/files/knauf_example.xml" style="cursor: pointer; margin-top:10px;" class="account-form__button" target="_blank" download="">Скачать образец файла YML</a>
			</div>

</div>

