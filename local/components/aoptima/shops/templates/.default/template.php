<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if( $arParams['IS_LOAD'] != 'Y' ){ ?>

    <? if( count($arResult['MAP_POINTS']) > 0 ){ ?>

        <article class="block  block--map">
            <div class="block__wrapper">
                <section class="shops">
                    <div class="shops__map" id="map_dealers"></div>
                </section>
            </div>
        </article>

    <? } else { ?>

        <article class="block block--top">

    <? } ?>

    <section class="block <? if( count($arResult['MAP_POINTS']) > 0 ){ ?>block--tabs<? } ?>">
        <div class="block__wrapper">
            <h2 class="block__title">Точки самовывоза</h2>
            <div class="product-tabs">

                <dl class="dropdowns dropdown">
                    <dt><a><span><?=$arResult['COUNTRIES'][0]['NAME_RU']?></span></a></dt>
                    <dd>
                        <ul>
                            <? $cnt = 0;
                            foreach( $arResult['COUNTRIES'] as $country_id => $country ){ $cnt++; ?>
                                <li class='product-tab  <? if( $cnt == 1 ){ echo 'active'; } ?>'>
                                    <a href="#tab_23111_<?=$country_id?>" id="tabLink_23111_<?=$country_id?>"><?=$country['NAME_RU']?></a>
                                </li>
                            <? } ?>
                        </ul>
                    </dd>
                </dl>

                <script type="text/javascript">
                    dropDownHandler();
                </script>

                <? foreach( $arResult['COUNTRIES'] as $country_id => $country ){ ?>

                    <article class="tab  tab--shops <? if( $cnt == 1 ){ echo 'first'; } ?>" id="tab_23111_<?=$country_id?>">
                        <section>
                            <div>
                                <div class="tab__inner">
                                    <div class="dealer__addresses">

<? if( count($arResult['SHOPS'][$country_id]) > 0 ){ ?>

    <form class="shopsFilterForm">

        <input type="hidden" name="filterCountry" value="<?=$country_id?>">

        <div class="shops__header">

            <h3 class="shops__title"><?=$country['NAME_RU']?></h3>

            <div class="shops__filters">

                <div class="account-form__field">
                    <input
                        id="shopsFilterDealer_<?=$country_id?>"
                        type="text"
                        placeholder="Дилер"
                        dealer_name=""
                        autocomplete="off"
                        onfocus="dealerFocus($(this));"
                        onkeyup="dealerKeyUp($(this));"
                        onfocusout="dealerFocusOut($(this));"
                    >
                    <input class="dealer_input" type="hidden" name="filterDealer">
                    <script>
                    $(document).ready(function(){
                        $('#shopsFilterDealer_<?=$country_id?>').autocomplete({
                            source: '/ajax/searchDealers.php',
                            minLength: 2,
                            delay: 700,
                            select: dealerSelect
                        })
                    })
                    </script>
                </div>

                <div class="account-form__field">
                    <div class="styledSelect">
                        <input
                            id="shopsFilterRegion_<?=$country_id?>"
                            type="text"
                            placeholder="Регион"
                            loc_name=""
                            autocomplete="off"
                            onfocus="regionFocus($(this));"
                            onkeyup="regionKeyUp($(this));"
                            onfocusout="regionFocusOut($(this));"
                        >
                        <input class="region_input" type="hidden" name="filterRegion">
                        <script>
                        $(document).ready(function(){
                            $('#shopsFilterRegion_<?=$country_id?>').autocomplete({
                                source: '/ajax/searchRegion.php',
                                minLength: 2,
                                delay: 700,
                                select: regionSelect,
                                //search: reloadShops
                            })
                        })
                        </script>
                    </div>
                </div>

                <div class="account-form__field">
                    <div class="styledSelect">
                        <input
                            id="shopsFilterCity_<?=$country_id?>"
                            type="text"
                            placeholder="Населённый пункт"
                            cityName=""
                            autocomplete="off"
                            onfocus="cityFocus($(this));"
                            onkeyup="cityKeyUp($(this));"
                            onfocusout="cityFocusOut($(this));"
                            value="<?=$arResult['LOC']['NAME_RU']?>"
                        >
                        <input class="loc_input" type="hidden" name="filterCity" value="<?=$arResult['LOC']['ID']?>">
                        <script>
                        $(document).ready(function(){
                            $('#shopsFilterCity_<?=$country_id?>').autocomplete({
                                source: '/ajax/searchCity.php',
                                minLength: 2,
                                delay: 700,
                                select: citySelect,
                                //search: reloadShops
                            })
                        })
                        </script>
                    </div>
                </div>

            </div>
        </div>

    </form>

<? } ?>

<div class="dealer__addresses-items shopsLoadArea">

    <? $cnt = 0;
    foreach( $arResult['SHOPS'][$country_id] as $shop ){ $cnt++;
        if( $cnt <= $arResult['MAX_CNT'] ){
            // shopItem
            $APPLICATION->IncludeComponent(
                "aoptima:shopItem", "",
                array( 'shop' => $shop )
            );
        }
    } ?>

</div>

<div class="shops__footer">

    <a class="moreShopsButton to___process" style="cursor: pointer; <? if( $cnt <= $arResult['MAX_CNT'] ){ ?>display:none;<? } ?>">Показать ещё</a>

    <p class="no___count no_count_shops" style="padding-top: 30px; font-size: 16px; color: gray; <? if( count($arResult['SHOPS'][$country_id]) > 0 ){ ?>display:none;<? } ?>">В стране "<?=$country['NAME_RU']?>" точек не найдено</p>

</div>

                                    </div>
                                </div>
                            </div>
                        </section>
                    </article>

                <? } ?>

            </div>
        </div>
    </section>

    <script>
    $(document).ready(function(){

        var $mapDiv = $('#map_dealers');
        var mapDim = {
            height: $mapDiv.height(),
            width: $mapDiv.width()
        }

        <? foreach( $arResult['MAP_POINTS'] as $shop ){
            $ar = explode(',', $shop['PROPERTY_GPS_VALUE']); ?>

            shop_markers.push(
                new google.maps.Marker({
                    position: { lat: <?=$ar[0]?>, lng: <?=$ar[1]?> },
                    arShop: <?=json_encode($shop)?>,
                    icon: '<?=SITE_TEMPLATE_PATH?>/images/marker.png'
                })
            );

        <? } ?>

        var bounds = (shop_markers.length > 0) ? createBoundsForMarkers(shop_markers) : null;
        var zoom = (bounds) ? getBoundsZoomLevel(bounds, mapDim) : 0;
        if( shop_markers.length == 1 ){   zoom = 15;   }
        points_map = new google.maps.Map($mapDiv[0], {
            center: (bounds) ? bounds.getCenter() : new google.maps.LatLng(0, 0),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoom: zoom,
            disableDefaultUI: true,
            zoomControl: true,
            fullscreenControl: true
        });
        $.each(shop_markers, function() { this.setMap(points_map); });

        var clusterStyles = {
            styles: [{
                height: 43,
                url: '<?=SITE_TEMPLATE_PATH?>/images/marker_cluster.png',
                textColor: 'white',
                width: 43,
                fontFamily: 'futuralight,Arial,sans-serif',
                fontWeight: 'normal',
                textSize: 16
            }]
        };
        markerCluster = new MarkerClusterer(points_map, shop_markers, clusterStyles);
    })
    </script>


    <? if( count($arResult['MAP_POINTS']) == 0 ){ ?>

        </article>

    <? } ?>


<? } else if( $arParams['IS_LOAD'] == 'Y' ){


    $form_data = Array();
    parse_str($_POST["form_data"], $form_data);


    $cnt = 0;
    foreach( $arResult['SHOPS'][$form_data['filterCountry']] as $shop ){ $cnt++;
        if( $cnt <= $arResult['MAX_CNT'] ){
            // shopItem
            $APPLICATION->IncludeComponent(
                "aoptima:shopItem", "",
                array( 'shop' => $shop )
            );
        } else {
            echo '<ost></ost>';
        }
    }


} ?>