<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('iblock');

$arResult['MAX_CNT'] = project\dealer_shop::LIST_MAX_CNT;

$dealer_shop = new project\dealer_shop();

$arResult['COUNTRIES'] = project\bx_location::getCountries();

if( $arParams['IS_LOAD'] != 'Y' ){
    $loc_id = $_SESSION['LOC']['CITY_LOC_ID'];
    $arResult['ALL_SHOPS'] = $dealer_shop->getShops( $loc_id );
}

$arResult['SHOPS'] = $dealer_shop->getShops( $loc_id, $arResult['MAX_CNT'] );
$has_shops = false;
foreach( $arResult['SHOPS'] as $country_id => $shops ){
    if( count($shops) > 0 ){  $has_shops = true;  }
}
if( !$has_shops ){
    $arResult['ALL_SHOPS'] = $dealer_shop->getShops( false );
    $arResult['SHOPS'] = $dealer_shop->getShops( false, $arResult['MAX_CNT'] );
}


if( strlen($_POST['form_data']) > 0 ){
    $form_data = Array();
    parse_str($_POST["form_data"], $form_data);
    if( intval($form_data['filterCity']) > 0 ){
        $loc_id = $form_data['filterCity'];
    }
}
$arResult['LOC'] = project\bx_location::getByID($loc_id);


$arResult['MAP_POINTS'] = [];
foreach( $arResult['COUNTRIES'] as $country_id => $country ){
    foreach( $arResult['ALL_SHOPS'][$country_id] as $shop ){
        if( strlen($shop['PROPERTY_GPS_VALUE']) > 0 ){
            $arResult['MAP_POINTS'][$shop['ID']] = $shop;
        }
    }
}



$this->IncludeComponentTemplate();