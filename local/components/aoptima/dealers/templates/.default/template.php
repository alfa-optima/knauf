<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project'); use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools'); use AOptima\Tools as tools;

//////////////////////////////////
// Все дилеры
$dealer = new project\dealer();
$allDealers = $dealer->allList();
//////////////////////////////////

$user = new project\user();

if( $arResult['DEALER'] ){ ?>

    <article class="block  block--top">
        <div class="block__wrapper">
            <section class="dealer">
                <h1 class="dealer__title"><?=$arResult['DEALER']['UF_COMPANY_NAME']?></h1>
                <div class="dealer__top">
                    <? if( $arResult['PIC_SRC'] ){ ?>
                        <div class="dealer__image">
                            <img src="<?=$arResult['PIC_SRC']?>" alt="<?=$arResult['DEALER']['UF_COMPANY_NAME']?>">
                        </div>
                    <? } ?>
                    <div class="dealer__top-column">
                        <? if(
                            is_array( $arResult['DEALER']['UF_PHONES'] )
                            &&
                            count( $arResult['DEALER']['UF_PHONES'] ) > 0
                        ){ ?>
                            <div class="dealer__top-telephones">
                                <? foreach( $arResult['DEALER']['UF_PHONES'] as $phone ){ ?>
                                    <span><?=$phone?></span>
                                <? } ?>
                            </div>
                        <? } ?>
                        <? if( strlen($arResult['DEALER']['EMAIL']) > 0 ){ ?>
                            <div class="dealer__top-web">
                                <a target="_blank" href="mailto:<?=$arResult['DEALER']['EMAIL']?>"><?=$arResult['DEALER']['EMAIL']?></a>
                            </div>
                        <? } ?>
                        <? if( strlen($arResult['DEALER']['UF_SITE']) > 0 ){ ?>
                            <div class="dealer__top-web">
                                <a target="_blank" href="<?=$arResult['DEALER']['UF_SITE']?>"><?=$arResult['DEALER']['UF_SITE']?></a>
                            </div>
                        <? } ?>
                    </div>
                    <div class="dealer__top-column">
                        <div class="dealer__review-stars">
                            <? for( $n = 1; $n <= 5; $n++ ){ ?>
                                <span class="dealer__review-star  <? if( $n <= $arResult['DEALER_RATING'] ){ ?>dealer__review-star--full<? } ?>"></span>
                            <? } ?>
                        </div>
                        <div class="dealer__top-info">
                            <span>Лет работы: <?=$arResult['DEALER']['REG_YEARS']?></span>
                            <span>Заказов выполнено: <?=count($arResult['DEALER_COMPLETED_ORDERS'])?></span>
                            <span><?=$arResult['POSITIVE_REVIEWS_PROCENTS']?>% положительных отзывов</span>
                        </div>
                    </div>
                    <div class="reveiws-diagram">
                        <? for( $v = 5; $v >= 1; $v-- ){ ?>
                            <div class="reveiws-diagram__row">
                                <span><?=$v?></span>
                                <div class="reveiws-diagram__maxRow">
                                    <span class="reveiws-diagram__colorRow"></span>
                                </div>
                                <span class="reveiws-diagram__reviewsQuantity">
                                    <?=$arResult['arVotes'][$v]?$arResult['arVotes'][$v]:0?> <?=tools\funcs::pfCnt($arResult['arVotes'][$v]?$arResult['arVotes'][$v]:0, "отзыв", "отзыва", "отзывов")?>
                                </span>
                            </div>
                        <? } ?>
                    </div>
                </div>
            </section>
        </div>
    </article>

    <!--<section class="dealer__label">-->
    <!--    <div class="dealer__label-wrapper">-->
    <!--        <span>лучший дилер по сухим смесям </span>-->
    <!--    </div>-->
    <!--</section>-->

    <section class="block  block--tabs">
        <div class="block__wrapper">
            <h2 class="block__title">ПОДРОБНЕЕ</h2>
            <div class="product-tabs">

                <dl class="dropdowns dropdown">
                    <dt><a><span>О поставщике</span></a></dt>
                    <dd>
                        <ul>
                            <li class='product-tab  active'><a href="#tab_23111_1" id="tabLink_23111_1">О поставщике</a></li>
                            <li class="product-tab"><a href="#tab_23111_2" id="tabLink_23111_2">Юридическая информация</a></li>
                            <li class="product-tab  product-tab--addresses"><a href="#tab_23111_3" id="tabLink_23111_3">Адреса точек продаж</a></li>
                            <li class="product-tab"><a href="#tab_23111_4" id="tabLink_23111_4">Доставка</a></li>
                            <li class="product-tab"><a href="#tab_23111_5" id="tabLink_23111_5">Оплата</a></li>
                            <li class="product-tab  product-tab--review"><a href="#tab_23111_6" id="tabLink_23111_6">Отзывы</a></li>
                        </ul>
                    </dd>
                </dl>

                <script type="text/javascript">
                    dropDownHandler();
                </script>

                <article class="tab bildtext first" id="tab_23111_1">
                    <section>
                        <div>

                            <? if( strlen($arResult['DEALER']['UF_COMPANY_NAME']) > 0 ){ ?>
                                <h4 class="tab__title"><?=$arResult['DEALER']['UF_COMPANY_NAME']?></h4>
                            <? } ?>

                            <? if( strlen($arResult['DEALER']['UF_POST_ADDRESS']) > 0 ){ ?>

                                <h4 class="tab__title">Почтовый адрес:</h4>
                                <div class="tab__inner">

                                    <? if( strlen($arResult['DEALER']['UF_POST_ADDRESS']) > 0 ){ ?>
                                        <p><?=$arResult['DEALER']['UF_POST_ADDRESS']?></p>
                                    <? } ?>

                                    <? if( 1==2 ){ ?>
                                        <? if( intval($arResult['DEALER']['UF_POST_CITY_ID']) > 0 ){
                                            $city = project\bx_location::getByID($arResult['DEALER']['UF_POST_CITY_ID']); ?>
                                            <p>Населённый пункт: <?=$city['NAME_RU']?></p>
                                        <? } ?>
                                        <? if( strlen($arResult['DEALER']['UF_POST_INDEX']) > 0 ){ ?>
                                            <p>Почтовый индекс: <?=$arResult['DEALER']['UF_POST_INDEX']?></p>
                                        <? } ?>
                                        <? if( strlen($arResult['DEALER']['UF_POST_STREET']) > 0 ){ ?>
                                            <p>Улица: <?=$arResult['DEALER']['UF_POST_STREET']?></p>
                                        <? } ?>
                                        <? if( strlen($arResult['DEALER']['UF_POST_HOUSE']) > 0 ){ ?>
                                            <p>Дом: <?=$arResult['DEALER']['UF_POST_HOUSE']?></p>
                                        <? } ?>
                                        <? if( strlen($arResult['DEALER']['UF_POST_KV']) > 0 ){ ?>
                                            <p>Кв./офис: <?=$arResult['DEALER']['UF_POST_KV']?></p>
                                        <? } ?>
                                        <? if( strlen($arResult['DEALER']['UF_DOP_INFO']) > 0 ){ ?>
                                            <p><?=$arResult['DEALER']['UF_DOP_INFO']?></p>
                                        <? } ?>
                                    <? } ?>

                                </div>

                            <? } ?>

                            <? if( strlen($arResult['DEALER']['UF_DOP_INFO']) > 0 ){ ?>
                                <div class="tab__inner" style="margin-top:20px;">
                                    <p><?=$arResult['DEALER']['UF_DOP_INFO']?></p>
                                </div>
                            <? } ?>

                        </div>
                    </section>
                </article>

                <article class="tab bildtext " id="tab_23111_2">
                    <section>
                        <div>
                            <h4 class="tab__title">Юридическая информация</h4>
                            <div class="tab__inner">
                                <? if( strlen($arResult['DEALER']['UF_FULL_COMPANY_NAME']) > 0 ){ ?>
                                    <p>Полное наименование: <?=$arResult['DEALER']['UF_FULL_COMPANY_NAME']?></p>
                                <? } ?>
                                <? if( strlen($arResult['DEALER']['UF_INN']) > 0 ){ ?>
                                    <p>ИНН: <?=$arResult['DEALER']['UF_INN']?></p>
                                <? } ?>
                                <? if( strlen($arResult['DEALER']['UF_OGRN']) > 0 ){ ?>
                                    <p>ОГРН: <?=$arResult['DEALER']['UF_OGRN']?></p>
                                <? } ?>
                            </div>
                        </div>
                    </section>
                </article>

                <article class="tab bildtext  tab--map" id="tab_23111_3">

                    <? // Точки продаж дилеров
                    $APPLICATION->IncludeComponent(
                        "aoptima:dealerPageShops", "",
                        array( 'arResult' => $arResult )
                    ); ?>

                </article>

                <article class="tab bildtext" id="tab_23111_4">
                    <section>
                        <div>
                            <h4 class="tab__title">Доставка</h4>
                            <div class="tab__inner">

                                <? if(
                                    $arResult['DEALER']['UF_DELIV_INFO_TYPE'] == 'link'
                                    &&
                                    strlen( $arResult['DEALER']['UF_DELIV_INFO_LINK']) > 0
                                ){ ?>

                                    <p>Информация об условиях и стоимости доставки <a href="<?=$arResult['DEALER']['UF_DELIV_INFO_LINK']?>" target="_blank" style="color: #0895d4; text-decoration: underline;">здесь</a></p>
                                    <br>

                                <? } else if(
                                    $arResult['DEALER']['UF_DELIV_INFO_TYPE'] == 'file'
                                    &&
                                    $arResult['DELIV_INFO_FILE']
                                ){ ?>

                                    <table class="delivFileTable">
                                        <tr>
                                            <td>
                                                <a href="<?=$arResult['DELIV_INFO_FILE']['SRC']?>" target="_blank">
                                                    <img style="max-height: 100px" src="/local/templates/main/images/file_ok.png">
                                                </a>
                                            </td>
                                            <td style="vertical-align: top;">
                                                <table>
                                                    <tr>
                                                        <td colspan="2"><?=$arResult['DEALER']['UF_DELIV_FILE_TITLE']?></td>
                                                    </tr>
                                                    <? if( $arResult['DELIV_INFO_FILE'] ){ ?>
                                                        <tr>
                                                            <td>Тип: </td>
                                                            <td><?=$arResult['DELIV_INFO_FILE']['EX']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Размер: </td>
                                                            <td><?=round($arResult['DELIV_INFO_FILE']['FILE_SIZE'], 1)?> <?=$arResult['DELIV_INFO_FILE']['FILE_SIZE_ED']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><a href="<?=$arResult['DELIV_INFO_FILE']['SRC']?>" target="_blank" download style="color: #0895d4; text-decoration: underline;">Скачать</a></td>
                                                            <td></td>
                                                        </tr>
                                                    <? } ?>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

                                <? } else { ?>

                                    <p class="no___count">Информация по доставке не указана</p>
                                    <br>

                                <? } ?>

                            </div>
                        </div>
                    </section>
                </article>

                <article class="tab  tab--pay bildtext" id="tab_23111_5">

                    <? // Варианты оплаты дилера
                    $APPLICATION->IncludeComponent(
                        "aoptima:dealerPagePayments", "",
                        array( 'arResult' => $arResult )
                    ); ?>

                </article>

                <article class="tab  tab--review bildtext" id="tab_23111_6">

                    <? // Отзывы о заказах
                    $APPLICATION->IncludeComponent(
                        "aoptima:personalOrderReviews", "dealer_page",
                        array(
                            'DEALER_ID' => $arResult['DEALER']['ID'],
                            'IS_DEALER_PAGE' => 'Y'
                        )
                    ); ?>

                </article>

            </div>
        </div>
    </section>



    <? // Самые покупаемые товары
    if( count($arResult['TOP_PRODUCTS']) > 0 ){
        $GLOBALS['top_products']['ID'] = array_keys($arResult['TOP_PRODUCTS']);

        // фильтр по СД
        $GLOBALS['top_products'] = project\catalog::AddStopSDsToFilter($GLOBALS['top_products']);

        $APPLICATION->IncludeComponent("bitrix:catalog.section", "dealer_page_top_products",
            Array(
                //////////////////////////////
                'ALL_DEALERS' => $allDealers,
                //////////////////////////////
                "LOC" => $_SESSION['LOC'],
                'IS_DEALER' => $user->isDealer()?'Y':'N',
                "IBLOCK_TYPE" => "content",
                "IBLOCK_ID" => project\catalog::catIblockID(),
                "SECTION_USER_FIELDS" => array(),
                "ELEMENT_SORT_FIELD" => 'RAND',
                "ELEMENT_SORT_ORDER" => 'ASC',
                "ELEMENT_SORT_FIELD2" => 'RAND',
                "ELEMENT_SORT_ORDER2" => 'ASC',
                "FILTER_NAME" => "top_products", "HIDE_NOT_AVAILABLE" => "N", "PAGE_ELEMENT_COUNT" => count($arResult['TOP_PRODUCTS']), "LINE_ELEMENT_COUNT" => "3", "PROPERTY_CODE" => array(), "OFFERS_LIMIT" => "0", "OFFERS_PROPERTY_CODE" => ['LOCATION'], "TEMPLATE_THEME" => "", "PRODUCT_SUBSCRIPTION" => "N", "SHOW_DISCOUNT_PERCENT" => "N", "SHOW_OLD_PRICE" => "N", "MESS_BTN_BUY" => "Купить", "MESS_BTN_ADD_TO_BASKET" => "В корзину", "MESS_BTN_SUBSCRIBE" => "Подписаться", "MESS_BTN_DETAIL" => "Подробнее", "MESS_NOT_AVAILABLE" => "Нет в наличии", "SECTION_URL" => "", "DETAIL_URL" => "", "SECTION_ID_VARIABLE" => "SECTION_ID", "AJAX_MODE" => "N", "AJAX_OPTION_JUMP" => "N", "AJAX_OPTION_STYLE" => "Y", "AJAX_OPTION_HISTORY" => "N", "CACHE_TYPE" => "A", "CACHE_TIME" => "600", "CACHE_GROUPS" => "Y", "SET_META_KEYWORDS" => "N", "META_KEYWORDS" => "", "SET_META_DESCRIPTION" => "N", "META_DESCRIPTION" => "", "BROWSER_TITLE" => "-", "ADD_SECTIONS_CHAIN" => "N", "DISPLAY_COMPARE" => "N", "SET_TITLE" => "N", "SET_STATUS_404" => "N", "CACHE_FILTER" => "Y", "PRICE_CODE" => array('BASE'), "USE_PRICE_COUNT" => "N", "SHOW_PRICE_COUNT" => "1", "PRICE_VAT_INCLUDE" => "Y", "CONVERT_CURRENCY" => "N", "BASKET_URL" => "/personal/basket.php", "ACTION_VARIABLE" => "action", "PRODUCT_ID_VARIABLE" => "id", "USE_PRODUCT_QUANTITY" => "N", "ADD_PROPERTIES_TO_BASKET" => "Y", "PRODUCT_PROPS_VARIABLE" => "prop", "PARTIAL_PRODUCT_PROPERTIES" => "N", "PRODUCT_PROPERTIES" => "", "PAGER_TEMPLATE" => "", "DISPLAY_TOP_PAGER" => "N", "DISPLAY_BOTTOM_PAGER" => "Y", "PAGER_TITLE" => "Товары", "PAGER_SHOW_ALWAYS" => "N", "PAGER_DESC_NUMBERING" => "N", "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", "PAGER_SHOW_ALL" => "Y", "ADD_PICT_PROP" => "-", "LABEL_PROP" => "-", 'INCLUDE_SUBSECTIONS' => "Y", 'SHOW_ALL_WO_SECTION' => "Y"
            )
        );
    } ?>


    <script src="<?=SITE_TEMPLATE_PATH?>/js/sliders.js?v=<?=time()?>"></script>


<? } else {

    include $_SERVER['DOCUMENT_ROOT']."/include/404include.php";

} ?>