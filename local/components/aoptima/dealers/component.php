<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('sale');

use Bitrix\Main,
    Bitrix\Main\Localization\Loc as Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Main\Application,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem,
    Bitrix\Sale,
    Bitrix\Sale\Basket,
    Bitrix\Sale\Order,
    Bitrix\Sale\DiscountCouponsManager,
    Bitrix\Main\Context,
    Bitrix\Sale\Internals;

\Bitrix\Main\Loader::includeModule('iblock');

$arURI = tools\funcs::arURI( tools\funcs::pureURL() );

if(
    (
        $arURI[1] == 'dealers'
        &&
        intval($arURI[2]) > 0
        &&
        !$arURI[3]
    )
    ||
    (
        $arURI[1] == 'knauf_app'
        &&
        $arURI[2] == 'dealers'
        &&
        intval($arURI[3]) > 0
        &&
        !$arURI[4]
    )
){
    
    $user_id = $arURI[1]=='knauf_app'?$arURI[2]:$arURI[3];
    
    $user = new project\user($arURI[1]=='knauf_app'?$arURI[3]:$arURI[2]);
    
    if( $user->isDealer() ){

        $arResult['DEALER'] = $user->arUser;
        
        if( strlen($arResult['DEALER']['UF_COMPANY_NAME']) > 0 ){
            $title = 'Дилер '.$arResult['DEALER']['UF_COMPANY_NAME'];
        } else {
            $title = 'Дилер №'.$arResult['DEALER']['ID'];
        }

        $APPLICATION->SetPageProperty("title", $title);

        $arResult['PIC_SRC'] = false;
        if( intval($arResult['DEALER']['PERSONAL_PHOTO']) > 0 ){
            $arResult['PIC_SRC'] = tools\funcs::rIMGG( $arResult['DEALER']['PERSONAL_PHOTO'], 4, 145, 145 );
        }

        // Считаем годы с момента регистрации
        $regStamp = MakeTimeStamp($arResult['DEALER']['DATE_REGISTER'], "DD.MM.YYYY HH:MI:SS");
        $curStamp = time();
        $arResult['DEALER']['REG_YEARS'] = round(($curStamp-$regStamp)/60/60/24/365, 1);

        // Выполненные заказы дилера
        $arResult['DEALER_COMPLETED_ORDERS'] = project\order::getDealerOrders(
            $arResult['DEALER']['ID'], false, array('Z')
        );

        // Отзывы к заказам дилера
        $review_order = new project\review_order();
        $arResult['DEALER_ORDERS_REVIEWS'] = $review_order->getList( false, false, $arResult['DEALER']['ID'] );
        // Определяем процент положительных отзывов
        $arResult['POSITIVE_REVIEWS'] = 0;
        $arResult['arVotes'] = array();
        $arResult['DEALER_VOTES_SUM'] = 0;
        $arResult['DEALER_VOTES_CNT'] = 0;
        $arResult['DEALER_RATING'] = 0;
        $arResult['POSITIVE_REVIEWS_PROCENTS'] = 0;
        if( count($arResult['DEALER_ORDERS_REVIEWS']) > 0 ){
            foreach ( $arResult['DEALER_ORDERS_REVIEWS'] as $key => $review ){
                if( $review['UF_DEALER_VOTE'] >= 4 ){
                    $arResult['POSITIVE_REVIEWS']++;
                }
                $arResult['arVotes'][$review['UF_DEALER_VOTE']]++;
                $arResult['DEALER_VOTES_SUM'] += $review['UF_DEALER_VOTE'];
                $arResult['DEALER_VOTES_CNT']++;
            }
            $arResult['POSITIVE_REVIEWS_PROCENTS'] = round($arResult['POSITIVE_REVIEWS']/count($arResult['DEALER_ORDERS_REVIEWS'])*100, 0);
            $arResult['DEALER_RATING'] = round($arResult['DEALER_VOTES_SUM']/$arResult['DEALER_VOTES_CNT'], 0);
        }


        // Инфо по доставке
        $arResult['DELIV_INFO_FILE'] = false;
        if( intval($arResult['DEALER']['UF_DELIV_INFO_FILE']) > 0 ){
            $arResult['DELIV_INFO_FILE']['SRC'] = \CFile::GetPath($arResult['DEALER']['UF_DELIV_INFO_FILE']);
            $ar = explode('.', $arResult['DELIV_INFO_FILE']['SRC']);
            $arResult['DELIV_INFO_FILE']['EX'] = $ar[count($ar)-1];
            $arResult['DELIV_INFO_FILE']['INFO'] = \CFile::GetFileArray($arResult['DEALER']['UF_DELIV_INFO_FILE']);
            $arResult['DELIV_INFO_FILE']['FILE_SIZE'] = $arResult['DELIV_INFO_FILE']['INFO']['FILE_SIZE'];
            $arResult['DELIV_INFO_FILE']['FILE_SIZE_ED'] = 'Б';
            if( $arResult['DELIV_INFO_FILE']['FILE_SIZE'] > 1024 ){
                $arResult['DELIV_INFO_FILE']['FILE_SIZE'] = $arResult['DELIV_INFO_FILE']['FILE_SIZE']/1024;
                $arResult['DELIV_INFO_FILE']['FILE_SIZE_ED'] = 'КБ';
                if( $arResult['DELIV_INFO_FILE']['FILE_SIZE'] > 1024 ){
                    $arResult['DELIV_INFO_FILE']['FILE_SIZE'] = $arResult['DELIV_INFO_FILE']['FILE_SIZE']/1024;
                    $arResult['DELIV_INFO_FILE']['FILE_SIZE_ED'] = 'МБ';
                }
            }
        }



        // Определение самых покупаемых товаров дилера
        // Соберём завершённые заказы дилера
        $arResult['TOP_PRODUCTS'] = array();
        $tps = array();
        // Получим товары из заказов
        foreach ( $arResult['DEALER_COMPLETED_ORDERS'] as $key => $order_id ){
            $obOrder = Sale\Order::load($order_id);
            $basket = $obOrder->getBasket();
            foreach ( $basket as $basketItem ){
                $tps[] = $basketItem->getProductID();
            }
        }
        if( count($tps) > 0 ){
            $filter = Array(
                "IBLOCK_ID" => project\catalog::tpIblockID(),
                "ACTIVE" => "Y",
                "ID" => $tps
            );
            $fields = Array( "ID", "PROPERTY_CML2_LINK" );
            $dbElements = \CIBlockElement::GetList(
                array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>10), $fields
            );
            if ($element = $dbElements->GetNext()){
                $arResult['TOP_PRODUCTS'][$element['PROPERTY_CML2_LINK_VALUE']]++;
            }
        }
        arsort($ar);



    }
}


$this->IncludeComponentTemplate();
