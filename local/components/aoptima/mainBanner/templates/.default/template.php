<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
if($arResult)
{
    ?>
    <noindex>

        <section class="bcgBlock  bcgBlock--indexTop"
                 style="background-image: url(<?=CFile::GetPath($arResult["PREVIEW_PICTURE"]);?>);">
            <?
            if($arResult["PROPERTY_TITLE1_VALUE"])
            {
                ?>
                <div class="bcgBlock__wrapper">
                    <h2 class="bcgBlock__title"><?= $arResult["PROPERTY_TITLE1_VALUE"] ?></h2>
                </div>
                <?
            }?>

            <? if( 1 ){ ?>

                <div class="textBlockOverBcgBlock textBlockOverBcgBlock--noMobile">
                    <div class="textBlockOverBcgBlock__wrapper">
                        <?
                        if($arResult["PROPERTY_TITLE2_VALUE"])
                        {
                            ?>
                            <h3 class="textBlockOverBcgBlock__title"><?= $arResult["PROPERTY_TITLE2_VALUE"] ?></h3>
                            <?
                        }
                        if($arResult["PREVIEW_TEXT"])
                        {
                            ?>
                            <p class="textBlockOverBcgBlock__text"><?= $arResult["PREVIEW_TEXT"] ?></p>
                            <?
                        }
                        if($arResult["PROPERTY_LINK_VALUE"] && $arResult["PROPERTY_LINK_TEXT_VALUE"])
                        {
                            ?>
                            <a href="<?=PREFIX?><?= $arResult["PROPERTY_LINK_VALUE"] ?>"
                               class="textBlockOverBcgBlock__link"><?= $arResult["PROPERTY_LINK_TEXT_VALUE"] ?></a>
                            <?
                        }?>
                    </div>
                </div>

            <? } ?>



            <a href="#popular" class="scrollDownLink mainPopScrollLink" style="display: none">Далее</a>

        </section>
    </noindex>


    <section class="textBlockOverBcgBlock textBlockOverBcgBlock--yesMobile">
        <div class="textBlockOverBcgBlock__wrapper">
            <?
            if($arResult["PROPERTY_TITLE2_VALUE"])
            {
                ?>
                <h3 class="textBlockOverBcgBlock__title"><?= $arResult["PROPERTY_TITLE2_VALUE"] ?></h3>
                <?
            }
            if($arResult["PREVIEW_TEXT"])
            {
                ?>
                <p class="textBlockOverBcgBlock__text"><?= $arResult["PREVIEW_TEXT"] ?></p>
                <?
            }
            if($arResult["PROPERTY_LINK_VALUE"] && $arResult["PROPERTY_LINK_TEXT_VALUE"])
            {
                ?>
                <a href="<?=PREFIX?><?= $arResult["PROPERTY_LINK_VALUE"] ?>"
                   class="textBlockOverBcgBlock__link"><?= $arResult["PROPERTY_LINK_TEXT_VALUE"] ?></a>
                <?
            }?>
        </div>
    </section>
    <?
}?>