<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$order = $arResult['ORDER']; ?>


<tr class="<? if( $order['ORDER_DEALER_STATUS'] == 'Z' && intval($order['DEALER_ID']) > 0 ){ echo 'account-form__table-delivered'; } ?> orderListItem" item_id="<?=$order['ID']?>">
    <td>
        <div class="account-form__table-flexColumn">
            <span><?=$order['ID']?></span>
            <span class="account-form__table-goodsQuantity"><?/*<?=$order['BASKET_CNT_STR']?> по&nbsp;цене*/?>подробнее</span>
        </div>
    </td>
    <td>
        <div class="account-form__table-flexColumn">
            <span class="account-form__table-mobTh">Стоимость</span>
            <span><?=$order['TOTAL_SUM_FORMAT']?> <span class="rub">₽</span></span>
        </div>
    </td>
    <td>
        <div class="account-form__table-flexColumn">
            <span class="account-form__table-mobTh">Дата</span>
            <span><?=$order['DATE_INSERT']?></span>
        </div>
    </td>
    <td>
        <div class="account-form__table-flexColumn">
            <span class="account-form__table-mobTh">Дилер</span>
            <a href="<?=$arResult['DEALER_URL']?>" class="account-form__table-link"><?=$order['DEALER']['UF_COMPANY_NAME']?></a>
        </div>
    </td>
    <td>
        <div class="account-form__table-flexColumn">
            <span class="account-form__table-mobTh">Вариант доставки</span>
            <span><?=$order['ds']['NAME']?></span>
        </div>
    </td>
    <td>
        <div class="account-form__table-flexColumn">
            <span class="account-form__table-mobTh">Статус</span>
            <div class="account-form__table-statusField">
                <span><?=$order['ORDER_DEALER_STATUSES'][$order['ORDER_DEALER_STATUS']]['NAME']?></span>
                <? if( $order['ORDER_DEALER_STATUS'] == 'Z' && intval($order['DEALER_ID']) > 0 ){ ?>
                    <!--
                    <span class="account-form__table-dateOfDelivery"><?=$order['STATUS_DATE']?></span>
                    -->

                    <a style="cursor: pointer;" class="account-form__table-order-btn  account-form__table-order-btn--review dealerReviewWindowButton to___process">Написать отзыв</a>

                <? } ?>
            </div>
        </div>
    </td>
</tr>

<tr class="account-form__table-orderInner">
    <td colspan="6">

        <table class="account-form__table-order">
            <tbody>
            <?
            foreach( $order['arBasket'] as $key => $basketItem ){ ?>

                <tr>
                    <td>
                        <div class="account-form__table-orderWrapper">
                            <div class="account-form__table-orderName">

                                <?php if( intval($basketItem['product']['ID']) > 0 ){ ?>

                                    <? if( $basketItem['product_img'] ){ ?>
                                        <a href="<?=$basketItem['product']['DETAIL_PAGE_URL']?>">
                                            <img src="<?=$basketItem['product_img']?>">
                                        </a>
                                    <? } ?>
                                    <a href="<?=$basketItem['product']['DETAIL_PAGE_URL']?>"><?=$basketItem['product']['NAME']?></a>

                                <? } else { ?>

                                    <a><?=$basketItem['basketItemName']?></a>

                                <? } ?>

                            </div>
                            <span>
                                <?
                                if($basketItem['sum'])
                                {
                                    ?>
                                    <?= $basketItem['sumFormat'] ?> <span
                                        class="rub">₽</span> (<?= $basketItem['QUANTITY'] ?> шт)
                                    <?
                                }
                                else
                                {
                                    ?>
                                    ПОДАРОК (<?= $basketItem['QUANTITY'] ?> шт)
                                    <?
                                }
                                ?>
                            </span>
                        </div>
                    </td>
                    <td>
                        <a style="cursor: pointer" class="account-form__table-order-btn basketButton to___process" item_id="<?=$basketItem['el']['ID']?>">В корзину</a>
                    </td>
                </tr>

            <? } ?>

            </tbody>
        </table>

        <div class="account-form__table-order-total">

            <? if( strlen($order['POD_ZAKAZ_GOODS']) > 0 ){ ?>

                <div class="account-form__row">
                    <div class="cart-form__infoColumn">
                        <span class="cart-form__infoColumn-title"><b>Товары под заказ:</b> <?=$order['POD_ZAKAZ_GOODS']?></span>
                    </div>
                </div>

            <? } ?>

            <div class="account-form__row">
                <div class="cart-form__infoColumn">
                    <span class="cart-form__infoColumn-title">ФИО: <?=$order['BUYER_NAME']?> <?=$order['BUYER_LAST_NAME']?></span>
                </div>
            </div>

            <div class="account-form__row">
                <div class="cart-form__infoColumn">
                    <span class="cart-form__infoColumn-title">Телефон: <?=$order['BUYER_PHONE']?></span>
                </div>
            </div>

            <div class="account-form__row">
                <div class="cart-form__infoColumn">
                    <span class="cart-form__infoColumn-title">Эл. почта: <?=$order['BUYER_EMAIL']?></span>
                </div>
            </div>

            <?
            if($order['FAST_ORDER'])
            {
                ?>

                <div class="account-form__row">
                    <div class="cart-form__infoColumn">
                        <span class="cart-form__infoColumn-title">Адрес: <?= $order['DELIVERY_ADDRESS'] ?></span>
                    </div>
                </div>

                <?
            }
            else
            {
                if ($order['delivType'] == 'delivery')
                { ?>

                    <div class="account-form__row">
                        <div class="cart-form__infoColumn">
                            <span class="cart-form__infoColumn-title">Адрес: <?= $order['DELIVERY_ADDRESS'] ?></span>
                        </div>
                    </div>

                <? } else { ?>

                    <div class="account-form__row">
                        <div class="cart-form__infoColumn">
                            <span class="cart-form__infoColumn-title">Пункт выдачи: <?= $order['PVZ'] ?></span>
                        </div>
                    </div>

                <? }
            }?>

            <div class="account-form__row">
                <div class="cart-form__infoColumn">
                    <span class="cart-form__infoColumn-title">Комментарий: <?=$order['BUYER_COMMENT']?></span>
                </div>
            </div>

            <div class="account-form__row">
                <div class="cart-form__infoColumn">

                    <span class="cart-form__infoColumn-title">Стоимость доставки: <? if( $order['DELIVERY_SUM']==0 ){ echo 'по договоренности'; } else { ?><b><?=$order['DELIVERY_SUM_FORMAT']?></b> <span class="rub">₽</span><? } ?></span>

                    <? if( $order['LIFT'] ){ ?>
                        <span class="cart-form__infoColumn-text"><?=$order['LIFT']?></span>
                    <? } ?>

                </div>
                <? if( $order['DELIV_DATE'] ){ ?>
                    <div class="cart-form__infoColumn">
                        <span class="cart-form__infoColumn-title">Дата доставки</span>
                        <span class="cart-form__infoColumn-text"><?=$order['DELIV_DATE']?></span>
                    </div>
                <? } ?>
                <? if( $order['DELIV_TIME'] ){ ?>
                    <div class="cart-form__infoColumn">
                        <span class="cart-form__infoColumn-title">Время доставки</span>
                        <span class="cart-form__infoColumn-text"><?=$order['DELIV_TIME']?></span>
                    </div>
                <? } ?>
                <div class="cart-form__infoColumn-sum">
                    <span>Итого:</span>
                    <span><?=$order['TOTAL_SUM_FORMAT']?><span class="rub">₽</span></span>
                </div>
            </div>

        </div>

        <div class="account-form__table-order-btns">
            <a href="javascript:;" class="account-form__table-order-btn  account-form__table-order-btn--hide">Свернуть</a>
            <a style="cursor:pointer" class="account-form__table-order-btn repeatOrderButton" order_id="<?=$order['ID']?>">Повторить заказ</a>
        </div>

    </td>
</tr>

