<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if( $arParams['IS_LOAD'] != 'Y' ){ ?>

    <section class="block  block--account">
        <div class="block__wrapper">
            <h1 class="block__title"><?$APPLICATION->ShowTitle()?></h1>
        </div>
    </section>


    <section class="block  block--accountForm">
        <div class="block__wrapper">
            <section class="dealerAccount-form">
                <div class="account-form__tabs">





    <article class="tab  tab--orders" style="display:block">

        <form class="dealerActiveOrdersForm" onsubmit="return false;">

            <h4 class="tab__title">Завершённые заказы</h4>

            <? if( count($arResult['ORDERS']) > 0 ){ ?>

                <div class="dealerAccount-form__completedOrders">

                    <table class="account-form__table  account-form__table--dealerOrders  account-form__table--dealerOrdersComplete archOrdersLoadArea">

                        <? $cnt = 0;
                        foreach( $arResult['ORDERS'] as $order_id => $order ){ $cnt++;
                            if( $cnt <= $arResult['MAX_CNT'] ){
                                // orderListItem
                                $APPLICATION->IncludeComponent(
                                    "aoptima:dealerOrderListItem", "complete",
                                    array( 'ORDER' => $order )
                                );
                            }
                        } ?>

                    </table>

                    <div class="more-button archOrdersMoreButton to___process" <? if( $cnt <= $arResult['MAX_CNT'] ){ ?>style="display:none"<? } ?>>
                        <span>Показать еще</span>
                    </div>

                </div>

            <? } else { ?>

                <div class="dealerAccount-form__completedOrders">

                    <p class="no___count" style="padding:30px; font-size: 16px; text-align:center;">Завершённых заказов нет</p>

                </div>

            <? } ?>

        </form>

    </article>



                </div>
            </section>
        </div>
    </section>


<? } else if( $arParams['IS_LOAD'] == 'Y' ){

    if( count($arResult['ORDERS']) > 0 ){

        $cnt = 0;
        foreach( $arResult['ORDERS'] as $order_id => $order ){ $cnt++;
            if( $cnt <= $arResult['MAX_CNT'] ){
                // orderListItem
                $APPLICATION->IncludeComponent(
                    "aoptima:dealerOrderListItem", "complete",
                    array( 'ORDER' => $order )
                );
            } else {
                echo '<tr class="ost"></tr>';
            }
        }

    }

}