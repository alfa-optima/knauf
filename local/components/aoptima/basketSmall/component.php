<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$siteID = $arParams['IS_MOB_APP']=='Y'?'mb':\Bitrix\Main\Context::getCurrent()->getSite();

$user = new project\user();

if( !$user->isDealer() ){

    $basketInfo = project\user_basket::info(
        null, null, null, null,
        $siteID
    );

    $arResult['IS_ACTIVE'] = tools\funcs::arURI()[1]=='basket'?'Y':'N';

//    $arResult['basket_cnt'] = 0;
//    foreach ( $basketInfo['basketProducts'] as $key => $basketProduct ){
//        $arResult['basket_cnt'] += $basketProduct['quantity'];
//    }

    $arResult['basket_cnt'] = $basketInfo['basketProductsCNT'];

    // сохраним ID товаров и количество в сессию, чтобы отмечать товары в корзине
    $_SESSION["BASKET"] = Array();
    foreach($basketInfo["basketProducts"] as $arProd)
    {
        if($arProd["custom_price"]) continue;

        $_SESSION["BASKET"][] = Array(
            "ID" => $arProd["el"]["ID"],
            "QUANTITY" => $arProd["quantity"]
        );
    }

    $this->IncludeComponentTemplate();

}