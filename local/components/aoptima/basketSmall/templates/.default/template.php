<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<a href="<?=$arParams['IS_MOB_APP']=='Y'?'/knauf_app':''?>/basket/" class="main-nav__cart  main-nav__cart--notEmpty basketSmallBlock <? if($arResult['IS_ACTIVE']=='Y'){ echo 'main-nav__cart--active'; } ?>">
    <span class="main-nav__cart-quantity"><?=$arResult['basket_cnt']?></span>
    <span class="sr-only">Корзина</span>
</a>