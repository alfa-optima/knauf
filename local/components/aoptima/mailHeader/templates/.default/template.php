<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>
<title>Письмо покупателю</title>
<title>Письмо покупателю</title>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Письмо покупателю</title>

    <style></style>
</head>

<body style="-moz-box-sizing: border-box; -ms-text-size-adjust: 100%; -webkit-box-sizing: border-box; -webkit-text-size-adjust: 100%; Margin: 0; box-sizing: border-box; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; min-width: 100%; padding: 0; text-align: left; width: 100% !important;">
<style>

    a {
        color: #009fe3;
        text-decoration: underline;
    }

    a:hover {

        text-decoration: none!important;
    }

    a:active {
        color: #016b99;
    }

    a:visited {
        color: #009fe3;
    }
    @media only screen {
        html {
            min-height: 100%;
            background: #f1efeb;
        }
    }

    @media only screen and (max-width: 500px) {
        .small-float-center {
            margin: 0 auto !important;
            float: none !important;
            text-align: center !important;
        }
        .colored-table th {
            font-size: 12px;
        }
        .colored-table td {
            font-size: 12px;
        }
        .info-table td {
            font-size: 12px;
        }
        .info-table>tbody>tr>td:first-child {
            width: 115px;
            min-width: 115px;
            padding-right: 15px;
            padding-left: 0;
        }
        .info-table>tbody>tr>th:first-child {
            padding-left: 0;
        }
        .info-table>tbody>tr>th:last-child {
            padding-right: 0;
        }
        .info-table>tbody>tr>td:last-child {
            padding-right: 0;
        }
        .banner {
            padding-left: 0 !important;
            padding-right: 0 !important;
        }
        td.large-12.colored,
        th.large-12.colored {
            padding-left: 0 !important;
            padding-right: 0 !important;
        }
        .small-text-center {
            text-align: center !important;
        }
        .small-text-left {
            text-align: left !important;
        }
        .small-text-right {
            text-align: right !important;
        }
    }

    @media only screen and (max-width: 500px) {
        .hide-for-large {
            display: block !important;
            width: auto !important;
            overflow: visible !important;
            max-height: none !important;
            font-size: inherit !important;
            line-height: inherit !important;
        }
    }

    @media only screen and (max-width: 500px) {
        table.body table.container .hide-for-large,
        table.body table.container .row.hide-for-large {
            display: table !important;
            width: 100% !important;
        }
        .greyBlock .button a {
            font-size: 14px !important;
            height: 33px !important;
        }
    }

    @media only screen and (max-width: 500px) {
        table.body table.container .callout-inner.hide-for-large {
            display: table-cell !important;
            width: 100% !important;
        }
    }

    @media only screen and (max-width: 500px) {
        table.body table.container .show-for-large {
            display: none !important;
            width: 0;
            mso-hide: all;
            overflow: hidden;
        }
    }

    @media only screen and (max-width: 500px) {
        table.body img {
            width: auto;
            height: auto;
        }
        table.body center {
            min-width: 0 !important;
        }
        table.body .container {
            width: 100% !important;
        }
        table.body .columns,
        table.body .column {
            height: auto !important;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            padding-left: 8px !important;
            padding-right: 8px !important;
        }
        table.body .columns .column,
        table.body .columns .columns,
        table.body .column .column,
        table.body .column .columns {
            padding-left: 0 !important;
            padding-right: 0 !important;
        }
        table.body .collapse .columns,
        table.body .collapse .column {
            padding-left: 0 !important;
            padding-right: 0 !important;
        }
        td.small-1,
        th.small-1 {
            display: inline-block !important;
            width: 8.33333% !important;
        }
        td.small-2,
        th.small-2 {
            display: inline-block !important;
            width: 16.66667% !important;
        }
        td.small-3,
        th.small-3 {
            display: inline-block !important;
            width: 25% !important;
        }
        td.small-4,
        th.small-4 {
            display: inline-block !important;
            width: 33.33333% !important;
        }
        td.small-5,
        th.small-5 {
            display: inline-block !important;
            width: 41.66667% !important;
        }
        td.small-6,
        th.small-6 {
            display: inline-block !important;
            width: 50% !important;
        }
        td.small-7,
        th.small-7 {
            display: inline-block !important;
            width: 58.33333% !important;
        }
        td.small-8,
        th.small-8 {
            display: inline-block !important;
            width: 66.66667% !important;
        }
        td.small-9,
        th.small-9 {
            display: inline-block !important;
            width: 75% !important;
        }
        td.small-10,
        th.small-10 {
            display: inline-block !important;
            width: 83.33333% !important;
        }
        td.small-11,
        th.small-11 {
            display: inline-block !important;
            width: 91.66667% !important;
        }
        td.small-12,
        th.small-12 {
            display: inline-block !important;
            width: 100% !important;
        }
        td.small-12 a,
        th.small-12 a,
        td.small-12 img,
        th.small-12 img {
            max-width: 100%;
        }
        .columns td.small-12,
        .column td.small-12,
        .columns th.small-12,
        .column th.small-12 {
            display: block !important;
            width: 100% !important;
        }
        table.body td.small-offset-1,
        table.body th.small-offset-1 {
            margin-left: 8.33333% !important;
            Margin-left: 8.33333% !important;
        }
        table.body td.small-offset-2,
        table.body th.small-offset-2 {
            margin-left: 16.66667% !important;
            Margin-left: 16.66667% !important;
        }
        table.body td.small-offset-3,
        table.body th.small-offset-3 {
            margin-left: 25% !important;
            Margin-left: 25% !important;
        }
        table.body td.small-offset-4,
        table.body th.small-offset-4 {
            margin-left: 33.33333% !important;
            Margin-left: 33.33333% !important;
        }
        table.body td.small-offset-5,
        table.body th.small-offset-5 {
            margin-left: 41.66667% !important;
            Margin-left: 41.66667% !important;
        }
        table.body td.small-offset-6,
        table.body th.small-offset-6 {
            margin-left: 50% !important;
            Margin-left: 50% !important;
        }
        table.body td.small-offset-7,
        table.body th.small-offset-7 {
            margin-left: 58.33333% !important;
            Margin-left: 58.33333% !important;
        }
        table.body td.small-offset-8,
        table.body th.small-offset-8 {
            margin-left: 66.66667% !important;
            Margin-left: 66.66667% !important;
        }
        table.body td.small-offset-9,
        table.body th.small-offset-9 {
            margin-left: 75% !important;
            Margin-left: 75% !important;
        }
        table.body td.small-offset-10,
        table.body th.small-offset-10 {
            margin-left: 83.33333% !important;
            Margin-left: 83.33333% !important;
        }
        table.body td.small-offset-11,
        table.body th.small-offset-11 {
            margin-left: 91.66667% !important;
            Margin-left: 91.66667% !important;
        }
        table.body table.columns td.expander,
        table.body table.columns th.expander {
            display: none !important;
        }
        table.body .right-text-pad,
        table.body .text-pad-right {
            padding-left: 10px !important;
        }
        table.body .left-text-pad,
        table.body .text-pad-left {
            padding-right: 10px !important;
        }
        table.menu {
            width: 100% !important;
        }
        table.menu td,
        table.menu th {
            width: auto !important;
            display: inline-block !important;
        }
        table.menu.vertical td,
        table.menu.vertical th,
        table.menu.small-vertical td,
        table.menu.small-vertical th {
            display: block !important;
        }
        table.menu[align="center"] {
            width: auto !important;
        }
        table.button.small-expand,
        table.button.small-expanded {
            width: 100% !important;
        }
        table.button.small-expand table,
        table.button.small-expanded table {
            width: 100%;
        }
        table.button.small-expand table a,
        table.button.small-expanded table a {
            text-align: center !important;
            width: 100% !important;
            padding-left: 0 !important;
            padding-right: 0 !important;
        }
        table.button.small-expand center,
        table.button.small-expanded center {
            min-width: 0;
        }
    }
</style>
<!-- <style> -->

<table class="body" data-made-with-foundation="" style="Margin: 0; background: #f1efeb; border-collapse: collapse; border-spacing: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; height: 100%; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
    <tr style="padding: 0; text-align: left; vertical-align: top;">
        <td class="float-center" align="center" valign="top" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0 auto; border-collapse: collapse !important; color: #0a0a0a; float: none; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0 auto; padding: 0; text-align: center; vertical-align: top; word-wrap: break-word;">
            <center data-parsed="" style="min-width: 500px; width: 100%;">
                <table align="center" class="container header float-center" style="Margin: 0 auto; background: #009fe3; border-collapse: collapse; border-spacing: 0; float: none; margin: 0 auto; padding: 0; text-align: center; vertical-align: top; width: 500px;">
                    <tbody>
                    <tr style="padding: 0; text-align: left; vertical-align: top;">
                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                            <table align="center" class="" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top;">
                                <tbody>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                                        <table class="row collapse" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                                            <tbody>
                                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">
                                                    <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                                                        <tbody>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                            <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">
                                                                <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                                                                    <tbody>
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                                        <td height="5px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 0px; font-weight: normal; hyphens: auto; line-height: 5px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;"></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                        <?php if( 0 ){ ?>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                                <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">
                                                                    <p class="text-center" style="Margin: 0; Margin-bottom: 10px; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; margin-bottom: 0; padding: 0; text-align: center;">
                                                                        <a style="Margin: 0; color: #ffffff; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">Открыть в интернет-обозревателе</a>
                                                                    </p>
                                                                </th>
                                                            </tr>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                                <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">
                                                                    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                                                                        <tbody>
                                                                        <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                                            <td height="5px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 0px; font-weight: normal; hyphens: auto; line-height: 5px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;"></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </th>
                                                            </tr>
                                                        <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </th>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr style="padding: 0; text-align: left; vertical-align: top;">
                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                            <table align="center" class="row logo" style="background: #ffffff; border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                                <tbody>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                                        <table class="row collapse" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                                            <tbody>
                                            <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">
                                                    <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                                                        <tbody>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                            <th style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">
                                                                <table class="spacer" style="background: #ffffff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                                                                    <tbody>
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                                        <td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 0px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;"></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                            <th class="first" style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; padding-left: 32px;  padding-right: 13px;  padding-bottom: 0px; text-align: right;">
                                                                <a href="http://<?=$arResult['SERVER_NAME']?>/" style="Margin: 0; color: #009fe3; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left; text-decoration: underline;">
                                                                    <img style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: inline; max-width: 90px; outline: none; text-decoration: none; width: 60px;" src="https://<?=$arResult['SERVER_NAME']?>/local/templates/main/images/logo2.png">
                                                                </a>
                                                            </th>
                                                        </tr>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                            <th class="border-bottom" style="Margin: 0; border-bottom: 1px solid #ccc2b8; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left;">
                                                                <table class="spacer" style="background: #ffffff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                                                                    <tbody>
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                                        <td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 0px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;"></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </th>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr style="padding: 0; text-align: left; vertical-align: top;">
                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                            <table class="row collapse" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                                <tbody>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <th class="small-12  large-12 banner" style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; padding-left: 0px; padding-right: 0px; text-align: left; width: 564px;">
                                        <a href="http://<?=$arResult['SERVER_NAME']?>/catalog/" style="Margin: 0; color: #009fe3; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left; text-decoration: underline;">
                                            <img src="https://<?=$arResult['SERVER_NAME']?>/local/templates/main/images/banner-mail2.jpg" alt="" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: block; max-width: 100%; outline: none; text-decoration: none; width: auto;">
                                        </a>
                                    </th>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <table align="center" class="container float-center  main" style="Margin: 0 auto; background: #ffffff; border-collapse: collapse; border-spacing: 0; float: none; margin: 0 auto; padding: 0; text-align: center; vertical-align: top; width: 500px;">
                    <tbody>
                    <tr style="padding: 0; text-align: left; vertical-align: top;">
                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">
                            <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                                <tbody>
                                <tr style="padding: 0; text-align: left; vertical-align: top;">
                                    <td height="22px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 0px; font-weight: normal; hyphens: auto; line-height: 22px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;"></td>
                                </tr>
                                </tbody>
                            </table>