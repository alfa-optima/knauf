<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;
\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if( count($arResult) > 0 )
{
    // Все дилеры
    $dealer = new project\dealer();
    $allDealers = $dealer->allList();

    $user = new project\user();

    foreach ( $arResult as $arGroup )
    {
    ?>
        <div class="bcgBlock bcgBlock--indexMaterials" style="background-image: url(<?=CFile::GetPath($arGroup["UF_INDEX_PIC"])?>);">
            <div class="bcgBlock__wrapper">
                <h2 class="bcgBlock__title"><?=$arGroup["NAME"]?></h2>
                <a href="<?=$arGroup["SECTION_PAGE_URL"]?>" class="bcgBlock__link"><span>Подробнее</span></a>
            </div>
        </div>
        <?
        foreach($arGroup["SUB"] as $arSection)
        {
            ?>
            <section class="sliderBlock  sliderBlock--popular">
                <div class="sliderBlock__wrapper">
                    <h2 class="sliderBlock__title"><?=$arSection["NAME"]?></h2>

                    <?
                    if($arSection["ITEMS"])
                    {
                        $GLOBALS['main_prods']['ID'] = $arSection["ITEMS"];
                        ?>
                        <?
                        $APPLICATION->IncludeComponent(
                            "bitrix:catalog.section", "catalog_main",
                            Array(
                                //////////////////////////////
                                'ALL_DEALERS' => $allDealers,
                                //////////////////////////////
                                "LOC" => $_SESSION['LOC'],
                                "IDS" => $arSection["ITEMS"],
                                'IS_DEALER' => $user->isDealer()?'Y':'N',
                                "IBLOCK_TYPE" => "content",
                                "IBLOCK_ID" => project\catalog::catIblockID(),
                                "SECTION_USER_FIELDS" => array(),
                                "ELEMENT_SORT_FIELD" => 'RAND',
                                "ELEMENT_SORT_ORDER" => 'ASC',
                                "ELEMENT_SORT_FIELD2" => 'RAND',
                                "ELEMENT_SORT_ORDER2" => 'ASC',
                                "FILTER_NAME" => "main_prods",
                                "HIDE_NOT_AVAILABLE" => "N",
                                "PAGE_ELEMENT_COUNT" => count($arSection["ITEMS"]),
                                "LINE_ELEMENT_COUNT" => "3",
                                "PROPERTY_CODE" => array("ARTICLE","MOD","MODS_ARTICLE","ED","VIEW","EDGE_VIEW","VIDEO","RESIST_WATER","DRYING_TIME","FLAMMABILITY_GROUP","BASE_MATERIAL","RESIST_COLD","FLOOR_COATING","TARGET","MIXTURE_BASE","PERFORATION","ROOM","POP","PREIM","PRIM","BUY_WITH","SEE_WITH","PROPERTIES","APPL_METHOD","TECH","TYPE","TYPE_OBJECT","TYPE_OF_APPLICATION","TYPE_PROFILE","TYPE_COATING","LAYER_THICKNESS","FASOVKA","TILE","COLOR","WEIGHT","HEIGHT","LENGTH","DEPTH","WIDTH","CROSS","CALC_WEIGHT","MAX_GABARIT","HAS_OFFERS",""),
                                "OFFERS_LIMIT" => "0", "OFFERS_PROPERTY_CODE" => ['LOCATION'], "TEMPLATE_THEME" => "", "PRODUCT_SUBSCRIPTION" => "N", "SHOW_DISCOUNT_PERCENT" => "N", "SHOW_OLD_PRICE" => "N", "MESS_BTN_BUY" => "Купить", "MESS_BTN_ADD_TO_BASKET" => "В корзину", "MESS_BTN_SUBSCRIBE" => "Подписаться", "MESS_BTN_DETAIL" => "Подробнее", "MESS_NOT_AVAILABLE" => "Нет в наличии", "SECTION_URL" => "", "DETAIL_URL" => "", "SECTION_ID_VARIABLE" => "SECTION_ID", "AJAX_MODE" => "N", "AJAX_OPTION_JUMP" => "N", "AJAX_OPTION_STYLE" => "Y", "AJAX_OPTION_HISTORY" => "N", "CACHE_TYPE" => "A", "CACHE_TIME" => "36000000", "CACHE_GROUPS" => "Y", "SET_META_KEYWORDS" => "N", "META_KEYWORDS" => "", "SET_META_DESCRIPTION" => "N", "META_DESCRIPTION" => "", "BROWSER_TITLE" => "-", "ADD_SECTIONS_CHAIN" => "N", "DISPLAY_COMPARE" => "N", "SET_TITLE" => "N", "SET_STATUS_404" => "N", "CACHE_FILTER" => "Y", "PRICE_CODE" => array('BASE'), "USE_PRICE_COUNT" => "N", "SHOW_PRICE_COUNT" => "1", "PRICE_VAT_INCLUDE" => "Y", "CONVERT_CURRENCY" => "N", "BASKET_URL" => "/personal/basket.php", "ACTION_VARIABLE" => "action", "PRODUCT_ID_VARIABLE" => "id", "USE_PRODUCT_QUANTITY" => "N", "ADD_PROPERTIES_TO_BASKET" => "Y", "PRODUCT_PROPS_VARIABLE" => "prop", "PARTIAL_PRODUCT_PROPERTIES" => "N", "PRODUCT_PROPERTIES" => "", "PAGER_TEMPLATE" => "", "DISPLAY_TOP_PAGER" => "N", "DISPLAY_BOTTOM_PAGER" => "Y", "PAGER_TITLE" => "Товары", "PAGER_SHOW_ALWAYS" => "N", "PAGER_DESC_NUMBERING" => "N", "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", "PAGER_SHOW_ALL" => "Y", "ADD_PICT_PROP" => "-", "LABEL_PROP" => "-", 'INCLUDE_SUBSECTIONS' => "Y", 'SHOW_ALL_WO_SECTION' => "Y"
                            )
                        );
                        ?>
                        <?
                    }?>
                </div>
            </section>
            <?
        }
        ?>
    <?
    }
}
?>