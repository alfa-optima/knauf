<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arSections = project\catalog::sections_for_main(IS_ESHOP);

// получим товары для разделов второго уровня
foreach($arSections as $k => $arSection)
{
    if($arSection["DEPTH_LEVEL"] == 2)
    {
        $arSections[$k]["ITEMS"] = array_column(project\catalog::elements_for_main($arSection["ID"]), "ID");
    }
}

// сгруппируем разделы
$arGroupSections = Array();
foreach($arSections as $arSection)
{
    if($arSection["IBLOCK_SECTION_ID"])
    {
        $arGroupSections[$arSection["IBLOCK_SECTION_ID"]]["SUB"][] = $arSection;
    }
    else
    {
        $arGroupSections[$arSection["ID"]] = $arSection;
    }
}

// отбросим разделы без товаров
foreach($arGroupSections as $k => $arGroup)
{
    foreach($arGroup["SUB"] as $j => $arSection)
    {
        if(!$arSection["ITEMS"])
        {
            unset($arGroupSections[$k]["SUB"][$j]);
        }
    }

    if(!$arGroupSections[$k]["SUB"])
    {
        unset($arGroupSections[$k]);
    }
}

$arResult = $arGroupSections;

$this->IncludeComponentTemplate();