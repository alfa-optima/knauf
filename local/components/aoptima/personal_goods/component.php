<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('iblock');

$APPLICATION->AddChainItem('Товары', '/personal/goods/');


$arResult['MAX_CNT'] = 20;

//$arResult['DEALER_ID'] = $USER->GetID();

global $USER;
$currentUser = new project\user();
$userProps = $currentUser->arUser;
if(strlen($userProps["UF_MAIN_USER"]) > 0){
	$user = new project\user( $userProps["UF_MAIN_USER"] );
}
else{
	$user = new project\user();
}
//$arResult['DEALER_ID'] = $USER->GetID();
$arResult['DEALER_ID'] = $user->arUser["ID"];

$arResult['DEALER'] = tools\user::info($arResult['DEALER_ID']);
$arResult['CATALOG_SECTIONS'] = project\catalog::sections_1_level();


$arResult['OFFERS'] = array();
$arResult['ALL_OFFERS'] = array();

$arResult['REGIONS'] = array();
$arResult['DEFAULT_REGION'] = false;


if( $arParams['form_data']['loc_id'] != 'empty' ){
    if ( intval($arParams['form_data']['loc_id']) > 0 ){
        $regionLocID = $arParams['form_data']['loc_id'];
    }
    if (intval($regionLocID) > 0){
        $arResult['DEFAULT_REGION'] = project\bx_location::getByID($regionLocID);
    }
}



if( intval($arResult['DEFAULT_REGION']['ID']) > 0 ){

    // Ценовые предложения дилера
    $stop_ids_cnt = is_array($stop_ids)?count($stop_ids):0;

    $dealerPriceOffers = project\tp::getList(
        false,
        $arResult['DEALER']['ID'],
        $arResult['DEFAULT_REGION']['ID'],
        false,
        false
    );

    foreach ( $dealerPriceOffers as $offer ){

        $tovar = tools\el::info($offer['PROPERTY_CML2_LINK_VALUE']);
        $tovar = project\catalog::updateProductName($tovar);

        $loc_id = $offer['PROPERTY_LOCATION_VALUE'];

        $regionID = false;

        // Инфо о местоположении
        /*$loc = project\bx_location::getByID($loc_id);
        if ($loc['TYPE_CODE'] == 'REGION'){
            $arResult['REGIONS'][$loc['ID']] = $loc;
            $regionID = $loc['ID'];
        } else if ($loc['TYPE_CODE'] == 'CITY'){
            $locChain = project\bx_location::getLocChain($loc['ID']);
            foreach ($locChain as $id => $chainLoc) {
                if ($chainLoc['PARENTS_TYPE_CODE'] == 'REGION'){
                    $regionLoc = project\bx_location::getByID($chainLoc['PARENTS_ID']);
                    $arResult['REGIONS'][$regionLoc['ID']] = $regionLoc;
                    $regionID = $chainLoc['PARENTS_ID'];
                }
            }
        }*/

        $sectOk = true;
        if( intval($arParams['form_data']['section_id']) > 0 ){
            $sectOk = false;
            $sect_id = tools\el::sections($tovar['ID'])[0]['ID'];
            $chain = tools\section::chain($sect_id);
            if( $chain[0]['ID'] == $arParams['form_data']['section_id'] ){
                $sectOk = true;
            }
        }

        if( $sectOk ){
            $arOffer = array(
                'ID' => $offer['ID'],
                'NAME' => $offer['NAME'],
                'REGION_ID' => $arResult['DEFAULT_REGION']['ID'],
                'LOC_ID' => $loc_id,
                'TOVAR_ID' => $tovar['ID'],
                'TOVAR' => $tovar,
                'PRICE' => round($offer['CATALOG_PRICE_1'], project\catalog::ROUND),
                'QUANTUTY' => $offer['CATALOG_QUANTITY'],
                'POD_ZAKAZ' => $offer['PROPERTY_POD_ZAKAZ_VALUE']
            );
            $arResult['OFFERS'][$arResult['DEFAULT_REGION']['ID']][$arOffer['ID']] = $arOffer;
            $arResult['ALL_OFFERS'][$arOffer['ID']] = $arOffer;
        }

    }

}

$arResult["ALL_DEALER_TP_CNT"] = 0;
if( !$arResult['DEFAULT_REGION'] ){
    $arResult['DEALER_REGIONS'] = [];
    \Bitrix\Main\Loader::includeModule('iblock');
    $filter = Array(
    	"IBLOCK_ID" => project\catalog::tpIblockID(),
    	"ACTIVE" => "Y",
    	"PROPERTY_DEALER" => $arResult['DEALER_ID']
    );
    $fields = Array( "ID", "PROPERTY_DEALER", "PROPERTY_LOCATION" );
    $regions = \CIBlockElement::GetList(
    	array("SORT"=>"ASC"), $filter, false, false, $fields
    );
    while ($region = $regions->GetNext()){
        $arResult["ALL_DEALER_TP_CNT"]++;
        $arResult['DEALER_REGIONS'][$region['PROPERTY_LOCATION_VALUE']] = $region['PROPERTY_LOCATION_VALUE'];
    }
    if( count($arResult['DEALER_REGIONS']) > 0 ){
        foreach ( $arResult['DEALER_REGIONS'] as $key => $region_id ){
            $loc = project\bx_location::getByID($region_id);
            if( intval($loc['ID']) > 0 ){
                $arResult['DEALER_REGIONS'][$key] = $loc;
            } else {
                unset($arResult['DEALER_REGIONS'][$key]);
            }
        }
    }
}





$this->IncludeComponentTemplate();