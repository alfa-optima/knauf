<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if( $arResult['DEFAULT_REGION'] ){
    $offers = $arResult['OFFERS'][$arResult['DEFAULT_REGION']['ID']];
} else {
    $offers = $arResult['ALL_OFFERS'];
}

$cnt = count($offers);
if(!$arResult['DEFAULT_REGION'])
{
    $cnt = $arResult["ALL_DEALER_TP_CNT"];
}
?>

<form class="dealerGoodsForm" onsubmit="return false" style="margin: 0;">

    <article class="block  block--top">
        <div class="block__wrapper">
            <h2 class="block__title  block__title--search">Ваши товарные предложения</h2>
            <span class="block__text">Найдено предложений: <span class="block__text--numberFound"><?=$cnt?></span></span>
        </div>
    </article>

    <section class="searchBlock  searchBlock--goods">

        <div class="searchPanel">

            <h3 class="searchPanel__title">Найти продукты
                <span class="searchPanel__toggle">
                    <span class="sr-only">Скрыть/показать панель поиска</span>
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/arrow-left.png" class="searchPanel__toggle-image">
                </span>
            </h3>

            <div class="searchPanel__inner">

                <div class="searchPanel__keywords">
                    <div class="styledSelect">

                        <input
                            class="account-form__suggestInput"
                            id="goodsLKRegion"
                            type="text"
                            process="N"
                            name="goodsLKRegion"
                            value="<?=$arResult['DEFAULT_REGION']['NAME_RU']?>"
                            loc_name=""
                            onfocus="regionFocus($(this));"
                            onkeyup="regionKeyUp($(this));"
                            onfocusout="regionFocusOut($(this));"
                            autocomplete="off"
                            placeholder="Укажите регион"
                        >
                        <input type="hidden" name="loc_id" value="<?=$arResult['DEFAULT_REGION']['ID']?>">

                        <script>
                        $(document).ready(function(){
                            $('#goodsLKRegion').autocomplete({
                                source: '/ajax/searchRegion.php',
                                minLength: 2,
                                delay: 700,
                                select: regionSelect
                            })
                        })
                        </script>

                    </div>
                </div>

                <ul class="searchPanel-nav">

                    <? foreach ( $arResult['CATALOG_SECTIONS'] as $section ){ ?>

                        <li class="searchPanel-nav__item <? if( $arParams['form_data']['section_id'] == $section['ID'] ){ ?>searchPanel-nav__item--active<? } ?>">
                            <a style="cursor: pointer;" class="searchPanel-nav__item-link goodsLKSection to___process" section_id="<?=$section['ID']?>"><?=$section['NAME']?></span></a>
                        </li>

                    <? } ?>

                </ul>

                <input type="hidden" name="section_id" value="<?=$arParams['form_data']['section_id']?>">

            </div>
        </div>

        <div class="searchBlock__wrapper">
            <div class="searchBlock__content">
                <div class="goods">

                    <div class="goods__top">

                        <a href="<?=PREFIX?>/personal/add_prices/" class="goods__top-button">Добавить вручную</a>

                        <div class="goods__top-upload" style="text-align: right;">

                            <a href="<?=PREFIX?>/personal/add_prices_from_file/" class="goods__top-button">Загрузка из файла CSV</a>

                            <a href="<?=PREFIX?>/personal/add_prices_from_file_yml/" class="goods__top-button" style="margin-top:15px">Загрузка из файла YML</a>

                        </div>

                    </div>

                    <section class="goods__main">

                        <? if($arResult['DEFAULT_REGION']){ ?>

                            <h3 class="goods__title"><?=$arResult['DEFAULT_REGION']['NAME_RU']?></h3>

                            <? // Предложения для дефолтного региона
                            if(
                                is_array($offers)
                                &&
                                count($offers) > 0
                            ){ ?>

                                <table class="goods__table">
                                <tbody>

                                    <tr>
                                        <th>Наименование товара</th>
                                        <th>В наличии</th>
                                        <th>Цена</th>
                                    </tr>

                                    <? $cnt = 0;  $items_cnt = 0;
                                    foreach ( $offers as $offer_id => $offer ){
                                        $cnt++;
                                        if(
                                            is_array($_POST['stop_ids'])
                                            &&
                                            in_array($offer_id, $_POST['stop_ids'])
                                        ){   $cnt--;   }
                                        if( $cnt <= $arResult['MAX_CNT'] ){
                                            $items_cnt++; ?>

                                            <tr class="dealerOfferItem" item_id="<?=$offer['ID']?>">
                                                <td>
                                                    <div class="goods__table-orderWrapper">

                                                        <a style="cursor:pointer;" class="goods__table-deleteItemBtn deleteDealerOfferButton to___process" offer_id="<?=$offer['ID']?>"></a>

                                                        <div class="goods__table-orderName">

                                                            <? if( intval($offer['TOVAR']['DETAIL_PICTURE']) > 0 ){ ?>
                                                                  <a href="<?=PREFIX?><?=$offer['TOVAR']['DETAIL_PAGE_URL']?>">
                                                                    <img src="<?=tools\funcs::rIMGG($offer['TOVAR']['DETAIL_PICTURE'], 4, 50,
                                                                        50)?>">
                                                                </a>
                                                            <? } ?>

                                                            <a href="<?=PREFIX?><?=$offer['TOVAR']['DETAIL_PAGE_URL']?>"><?=$offer['TOVAR']['NAME']?></a>

                                                        </div>

                                                        <div class="account-form__checkbox">
                                                            <input <? if( $offer['POD_ZAKAZ'] == 1 ){ echo 'checked'; } ?> type="checkbox" id="podZakaz_<?=$offer['ID']?>" name="pod_zakaz">
                                                            <label for="podZakaz_<?=$offer['ID']?>">Под заказ</label>
                                                        </div>

                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="account-form__field">
                                                        <div>
                                                            <input class="number___input" type="text" name="quantity" value="<?=$offer['QUANTUTY']?>">
                                                            <span><?=$offer['TOVAR']['PROPERTY_ED_VALUE']?$offer['TOVAR']['PROPERTY_ED_VALUE']:'шт'?></span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="goods__table-row">
                                                        <div class="account-form__field">
                                                            <div>
                                                                <input type="text" name="price" value="<?=$offer['PRICE']?>" class="price___input">
                                                                <span class="rub">₽</span>
                                                            </div>
                                                        </div>
                                                        <a style="cursor:pointer; background: none; display:none" class="goods__table-saveBtn dealerPriceSave to___process" offer_id="<?=$offer['ID']?>"><img src="<?=SITE_TEMPLATE_PATH?>/images/pin-3-2.png"></a>
                                                    </div>
                                                </td>
                                            </tr>

                                        <? }
                                    } ?>

                                </tbody>
                                </table>

                            <? } else { ?>

                                <hr style="border: none; color: #dedede; background-color: #dedede; height: 1px; margin-bottom: 27px;">
                                <p class="lk___no_count">В данном регионе ценовых предложений нет</p>

                            <? } ?>

                        <? } else { ?>

                            <h3 class="goods__title" style="color: #bf0000;">Укажите регион</h3>

                            <? if( count($arResult['DEALER_REGIONS']) > 0 ){ ?>
                                <p>У меня есть товары в регионах:</p>
                                <? foreach( $arResult['DEALER_REGIONS'] as $region ){ ?>
                                    <p><a style="cursor: pointer; color:#009fe3;" class="my___region_button to___process" loc_id="<?=$region['ID']?>"><?=$region['NAME_RU']?></a></p>
                                <? } ?>
                            <? } ?>

                        <? } ?>

                    </section>

                </div>

                <div class="more-button moreDealerOffersButton to___process" <? if( count($offers) <= $items_cnt ){ ?>style="display:none"<? } ?>>
                    <span>Показать еще (<?=count($offers)-$items_cnt?>)</span>
                </div>

            </div>
        </div>

    </section>

</form>


<? if( $arParams['IS_AJAX'] == 'Y' ){ ?>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/default_scripts.js"></script>
<? } ?>
