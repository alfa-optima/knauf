<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>


<section class="personalDelivTemplatesBlock">

    <form class="account-form__address lkAddressesForm"  onsubmit="false">

        <h4 class="tab__title">Адреса доставки</h4>

        <div class="formsArea">

            <? foreach( $arResult['DELIVERY_TEMPLATES'] as $template ){
                $APPLICATION->IncludeComponent(
                    "aoptima:delivery_template_item", "",
                    array('template' => $template)
                );
            }

            if( count($arResult['DELIVERY_TEMPLATES']) == 0 ){
                // Пустой шаблон
                $APPLICATION->IncludeComponent(
                    "aoptima:delivery_template_item", ""
                );
            } ?>

        </div>

        <div class="account-form__wrapperMinusMargin">

            <p class="error___p"></p>

            <a style="cursor: pointer;" class="account-form__button deliveryTemplatesSave to___process">Сохранить</a>
        </div>

    </form>

</section>

