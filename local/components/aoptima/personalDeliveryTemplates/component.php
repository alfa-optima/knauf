<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


// Шаблоны доставки
$delivery_template = new project\delivery_template();
$arResult['DELIVERY_TEMPLATES'] = $delivery_template->getList($USER->GetID());





$this->IncludeComponentTemplate();