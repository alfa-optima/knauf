<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


global $USER;
$userID = $USER->GetID();
$currentUser = new project\user( $USER->GetID() );
$userProps = $currentUser->arUser;
if(strlen($userProps["UF_MAIN_USER"]) > 0){
	$user = new project\user( $userProps["UF_MAIN_USER"] );
	if( $user->isDealer() ){
		$userID = $user->arUser["ID"];
	}
}

// Инфо о пользователе
$arResult['USER'] = tools\user::info($userID);

$arResult['PAY_TYPES'] = tools\funcs::json_to_array( $arResult['USER']['UF_PAY_TYPES'] );

//echo "<pre>"; print_r( $arResult['PAY_TYPES'] ); echo "</pre>";

// Все варианты оплаты
$arResult['PS_LIST'] = project\ps::getList();






$this->IncludeComponentTemplate();