<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>

<form onsubmit="return false;">

    <section class="dealerAccount-delivery">

        <div class="dealerAccount-delivery__inner  dealerAccount-delivery__inner--noSettings" >

            <div class="dealerAccount-delivery__index" style="display:block; background: none">

                <div class="dealerAccount-delivery__indexBlock" style="border-bottom: none">

                    <div class="dealerAccount-delivery__indexBlock-wrapper">

                        <h5 class="dealerAccount-delivery__indexBlock-title">Используемые варианты оплаты:</h5>


                        <div class="account-form__row">
                            <div class="account-form__field  account-form__field--rows">
                                <div class="account-form__checkboxes  account-form__checkboxes--ha">

                                    <? foreach( $arResult['PS_LIST'] as $key => $ps ){ ?>

                                        <div class="account-form__checkbox">

                                            <input class="psInput" type="checkbox" id="lk_ps_<?=$ps['ID']?>" name="pss[]" <? if( in_array($ps['CODE'], $arResult['USER']['UF_PSS']) ){ echo 'checked'; } ?> value="<?=$ps['CODE']?>">

                                            <label for="lk_ps_<?=$ps['ID']?>"><?=$ps['NAME']?></label>

                                            <div class="account-form__checkbox-paymentMethods">
                                                <img src="<?=\CFile::GetPath($ps['PSA_LOGOTIP'])?>">
                                            </div>

                                            <div class="prepayment___block">

                                                <div class="account-form__checkbox">

                                                    <input <? if( in_array("pre",$arResult['PAY_TYPES'][$ps['CODE']]) ){  echo 'checked';  } ?> class="prepsInput" type="checkbox" id="lk_pre_ps_<?=$ps['ID']?>_prepayment" name="pay_types[<?=$ps['CODE']?>][]" value="pre">

                                                    <label for="lk_pre_ps_<?=$ps['ID']?>_prepayment">Предоплата</label>

                                                </div>

                                                <div class="account-form__checkbox">

                                                    <input <? if( in_array("post",$arResult['PAY_TYPES'][$ps['CODE']]) ){  echo 'checked';  } ?> class="prepsInput" type="checkbox" id="lk_post_ps_<?=$ps['ID']?>_prepayment" name="pay_types[<?=$ps['CODE']?>][]" value="post">

                                                    <label for="lk_post_ps_<?=$ps['ID']?>_prepayment">Постоплата</label>
                                                    
                                                </div>
                                            </div>
                                        </div>

                                    <? } ?>

                                </div>
                            </div>
                        </div>

                        <? if( 1==2 ){ ?>

                            <div class="account-form__row  account-form__row--mb36">

                                <div class="account-form__field  account-form__field--w760">

                                    <div class="account-form__checkboxes">

                                        <? foreach( $arResult['PS_LIST'] as $key => $ps ){ ?>

                                            <div class="account-form__checkbox  account-form__checkbox--w228">

                                                <input class="psInput" type="checkbox" id="lk_ps_<?=$ps['ID']?>" name="pss[]" <? if( in_array($ps['CODE'], $arResult['USER']['UF_PSS']) ){ echo 'checked'; } ?> value="<?=$ps['CODE']?>">

                                                <label for="lk_ps_<?=$ps['ID']?>">

                                                    <?=$ps['NAME']?>

                                                    <? if( intval($ps['PSA_LOGOTIP']) > 0 ){ ?>

                                                        <div class="account-form__checkbox-paymentMethods">
                                                            <img src="<?=\CFile::GetPath($ps['PSA_LOGOTIP'])?>">
                                                        </div>

                                                    <? } ?>

                                                </label>

                                                <div style="clear:both"></div>

                                                <div class="prepayment___block" style="margin-top: 50px;">

                                                    <input <? if( in_array("pre",$arResult['PAY_TYPES'][$ps['CODE']]) ){  echo 'checked';  } ?> class="prepsInput" type="checkbox" id="lk_pre_ps_<?=$ps['ID']?>_prepayment" name="pay_types[<?=$ps['CODE']?>][]" value="pre">

                                                    <label for="lk_pre_ps_<?=$ps['ID']?>_prepayment"><small>Предоплата</small></label>

                                                    <input <? if( in_array("post",$arResult['PAY_TYPES'][$ps['CODE']]) ){  echo 'checked';  } ?> class="prepsInput" type="checkbox" id="lk_post_ps_<?=$ps['ID']?>_prepayment" name="pay_types[<?=$ps['CODE']?>][]" value="post">

                                                    <label for="lk_post_ps_<?=$ps['ID']?>_prepayment"><small>Постоплата</small></label>

                                                </div>

                                            </div>

                                        <? } ?>

                                    </div>

                                </div>

                            </div>

                        <? } ?>

                    </div>

                </div>

                <div style="clear:both"></div>

                <p class="error___p"></p>

                <a class="dealerAccount-delivery__index-button dealerPaymentsSaveButton to___process" style="cursor: pointer; margin: 25px 25px 25px 0; float: right; ">Сохранить</a>

                <div style="clear:both"></div>

            </div>

        </div>

    </section>

</form>