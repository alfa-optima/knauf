<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('iblock');

\Bitrix\Main\Loader::includeModule('sale');

use Bitrix\Main\Application,
Bitrix\Main\Context,
Bitrix\Main\Request;

use Bitrix\Main,
Bitrix\Main\Localization\Loc as Loc,
Bitrix\Main\Loader,
Bitrix\Main\Config\Option,
Bitrix\Sale\Delivery,
Bitrix\Sale\PaySystem,
Bitrix\Sale,
Bitrix\Sale\Basket,
Bitrix\Sale\Order,
Bitrix\Sale\DiscountCouponsManager;

$context = Context::getCurrent();
$request = $context->getRequest();
$method = $request->getRequestMethod();

$arHeaders = [];
$headers = $request->getHeaders();
foreach ( $headers as $key => $value ){
    $arHeaders[ $key ] = $value;
}

$values = $request->getValues();

if( in_array($method, [ 'GET' ]) ){

    if(
        isset($values['id'])
        &&
        strlen($values['id']) > 0
    ){

        $mapTable = project\basket::getMapTable();
        
        $knauf_id = $mapTable[ $values['id'] ] ;
        
        if( isset( $knauf_id ) ){

            // Ищём товар по артикулу
            $product = project\catalog::getProductByArticle( $knauf_id );

            if( intval($product['ID']) > 0 ){

                $result = ['regions' => []];

                // переберём регионы
                $regions = \Bitrix\Sale\Location\LocationTable::getList(array(
                    'filter' => array(
                        'TYPE_CODE' => 'REGION',
                        '=NAME.LANGUAGE_ID' => 'ru',
                    ),
                    'select' => array(
                        'ID' => 'ID',
                        'NAME_RU' => 'NAME.NAME',
                        'TYPE_CODE' => 'TYPE.CODE',
                    )
                ));
                while( $region = $regions->fetch() ){

                    $price = null;

                    // Ищем ТП к товару
                    $filter = Array(
                        "IBLOCK_ID" => project\catalog::tpIblockID(),
                        "ACTIVE" => "Y",
                        "PROPERTY_CML2_LINK" => $product['ID'],
                        "PROPERTY_LOCATION" => $region['ID'],
                    );
                    
                    $fields = [
                        "ID", "PROPERTY_CML2_LINK_VALUE", "PROPERTY_LOCATION",
                        "CATALOG_GROUP_".project\catalog::PRICE_ID
                    ];
                    $offers = \CIBlockElement::GetList(
                        [ "CATALOG_PRICE_".project\catalog::PRICE_ID => "ASC" ],
                        $filter, false, ["nTopCount" => 1], $fields
                    );
                    if ( $offer = $offers->GetNext() ){

                        $result['regions'][ $region['ID'] ] = [
                            'id' => $region['ID'],
                            'name' => $region['NAME_RU'],
                            'price' => $offer[ "CATALOG_PRICE_".project\catalog::PRICE_ID ],
                        ];
                    }
                }

                header('Content-Type: application/json');
                echo \Bitrix\Main\Web\Json::encode( $result );

            } else {

                \CHTTP::SetStatus("400 Bad request");
                header('Content-Type: application/json');
                echo \Bitrix\Main\Web\Json::encode([
                    'status' => 'error',
                    'text' => 'товар не найден',
                ]);

            }

        } else {

            \CHTTP::SetStatus("400 Bad request");
            header('Content-Type: application/json');
            echo \Bitrix\Main\Web\Json::encode([
                'status' => 'error',
                'text' => 'товар не найден',
            ]);

        }

    } else {

        \CHTTP::SetStatus("400 Bad request");
        header('Content-Type: application/json');
        echo \Bitrix\Main\Web\Json::encode([
            'status' => 'error',
            'text' => 'не передан id товара',
        ]);

    }

} else {

    \CHTTP::SetStatus("405 Method Not Allowed");

}