<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult['ALL_CATEGORIES'] = project\catalog::sections_1_level();

$arResult['ALL_TYPES'] = [];
$enums = \CUserFieldEnum::GetList([], [ "USER_FIELD_ID" => 105 ]);
while($enum = $enums->GetNext()){
    $arResult['ALL_TYPES'][$enum['ID']] = $enum;
}

foreach ( $arResult['ALL_CATEGORIES'] as $sect ){
    if( $arParams['IS_MOB_APP'] == 'Y' ){
        $sect['SECTION_PAGE_URL'] = project\catalog::toMobileAppUrl( $sect['SECTION_PAGE_URL'] );
    }
    if( is_array($sect['UF_TYPE']) && count($sect['UF_TYPE']) > 0 ){
        foreach ( $sect['UF_TYPE'] as $enum_id ){
            $arResult['ALL_TYPES'][$enum_id]['ITEMS'][$sect['ID']] = [
                'ID' => $sect['ID'],
                'NAME' => $sect['NAME'],
                'URL' => $sect['SECTION_PAGE_URL'],
                'PICTURE' => $sect['PICTURE'],
            ];
        }
    }
}

foreach ( $arResult['ALL_TYPES'] as $key => $type ){
    if( !$type['ITEMS'] ){
        unset($arResult['ALL_TYPES'][$key]);
    }
}

//echo "<pre>"; print_r( $arResult['ALL_TYPES'] ); echo "</pre>";
$this->IncludeComponentTemplate();