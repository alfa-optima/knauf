<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if( count($arResult['ALL_TYPES']) > 0 ){

    foreach ( $arResult['ALL_TYPES'] as $type ){

        if( $type['XML_ID'] == 'type_1' ){ ?>

            <section class="sliderBlock  sliderBlock--spotlight">
                <div class="sliderBlock__wrapper">

                    <h2 class="sliderBlock__title"><?=$type['VALUE']?></h2>

                    <div class="slider  slider--spotlight">

                        <? foreach( $type['ITEMS'] as $sect ){ ?>

                            <div class="slider__item">

                                <div class="slider__item-image">

                                    <a href="<?=$sect['URL']?>">
                                        <? if( intval($sect['PICTURE']) > 0 ){ ?>
                                            <img src="<?=tools\funcs::rIMGG($sect['PICTURE'], 5, 434, 326)?>" alt="<?=$sect['NAME']?>">
                                        <? } else { ?>
                                            <img src="<?=SITE_TEMPLATE_PATH?>/images/no_image.png" alt="<?=$sect['NAME']?>">
                                        <? } ?>
                                    </a>

                                </div>

                                <div class="slider__item-inner">

                                    <h3 class="slider__item-title"><?=$sect['NAME']?></h3>

                                    <a href="<?=$sect['URL']?>" class="slider__item-link">Подробнее</a>

                                </div>

                            </div>

                        <? } ?>

                    </div>

                </div>
            </section>

        <? } else { ?>

            <section class="sliderBlock  sliderBlock--materials">
                <div class="sliderBlock__wrapper">

                    <h2 class="sliderBlock__title"><?=$type['VALUE']?></h2>

                    <div class="slider  slider--materials">

                        <? foreach( $type['ITEMS'] as $sect ){ ?>

                            <div class="slider__item">

                                <div class="slider__item-image">

                                    <a href="<?=$sect['URL']?>">
                                        <img src="<?=tools\funcs::rIMGG($sect['PICTURE'], 5, 204, 151)?>" alt="<?=$sect['NAME']?>">
                                    </a>

                                </div>

                                <div class="slider__item-inner">

                                    <h3 class="slider__item-title"><?=$sect['NAME']?></h3>

                                    <a href="<?=$sect['URL']?>" class="slider__item-link">Подробнее</a>

                                </div>
                            </div>

                        <? } ?>

                    </div>
                </div>
            </section>

        <? }

    }

} ?>