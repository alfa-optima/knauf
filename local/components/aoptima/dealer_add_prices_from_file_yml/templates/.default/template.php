<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<form class="loadYMLForm" method="post" enctype="multipart/form-data" target="upload___Frame" action="<?=PREFIX?>/ajax/uploadDealerYMLFile.php">

    <article class="block  block--top">
        <div class="block__wrapper">
            <h2 class="block__title  block__title--search">Загрузка цен из файла (YML)</h2>
        </div>
    </article>

    <section class="block  block--accountForm">

        <div class="block__wrapper">

            <section class="account-form">

                <div class="account-form__tabs">

                    <article class="tab first" id="tab_111_1">

                        <div class="dealerAccount-form__wrapper ymlFormBlock">

                            <div class="dealerAccount-form__block">

								<div class="dealerAccount-download_yml_template" style="text-align:center">
				<a href="/include/files/knauf_example.xml" style="cursor: pointer;" class="account-form__button" target="_blank" download>Скачать образец файла YML</a>
			</div>

                                <div class="dealerAccount-form__field  dealerAccount-form__field--w271 dealerAccount-region-field">
                                    <span>Регион для цен:</span>
                                    <input
                                        class="account-form__suggestInput"
                                        id="fileRegion"
                                        type="text"
                                        process="N"
                                        name="fileRegion"
                                        loc_name=""
                                        onfocus="regionFocus($(this));"
                                        onkeyup="regionKeyUp($(this));"
                                        onfocusout="regionFocusOut($(this));"
                                        autocomplete="off"
                                        placeholder="Укажите регион"
                                    >
                                    <input type="hidden" name="loc_id">
                                    <script>
                                        $(document).ready(function(){
                                            $('#fileRegion').autocomplete({
                                                source: '/ajax/searchRegion.php',
                                                minLength: 2,
                                                delay: 700,
                                                select: regionSelect
                                            })
                                        })
                                    </script>
                                </div>

                                <div class="dealerAccount-form__field  dealerAccount-form__field--w271">
                                    <span>Тип загрузки:</span>

                                    <div class="account-form__checkboxes dealerAccount-yml-checkboxes">

                                        <div class="account-form__checkbox  account-form__checkbox--w228">
                                            <input checked type="radio" id="yml_type_link" name="yml_load_type" value="link">
                                            <label for="yml_type_link">Ссылка на файл YML</label>
                                        </div>

                                        <div class="account-form__checkbox  account-form__checkbox--w228">
                                            <input type="radio" id="yml_type_file" name="yml_load_type" value="file">
                                            <label for="yml_type_file">Выбрать файл YML</label>
                                        </div>

                                    </div>
                                </div>

                                <div id="yml_type_link_block" class="dealerAccount-form__field  dealerAccount-form__field--w271">
                                    <input type="text" name="yml_link" placeholder="Ссылка на файл YML">
                                </div>

                                <div id="yml_type_file_block" class="dealerAccount-form__field  dealerAccount-form__field--w271" style="display:none">
                                    <span>Файл YML:</span>
                                    <div class="goods__top-upload">
                                        <label>
                                            <input type="file" name="yml" accept=".xml">
                                            <span>Выбрать файл YML</span>
                                            <span>Выбрать</span>
                                        </label>
                                    </div>
                                </div>

                                <p class="error___p"><?=count($arResult['ERRORS'])>0?implode('<br>', $arResult['ERRORS']):''?></p>

                                <a style="cursor: pointer;" class="account-form__button loadYMLButton to___process">Загрузить</a>

                            </div>

                        </div>

                        <div id='loadFileWindow' style="display:none;" go="Y">

                            <h1 style="text-align: center;">Импорт файла...</h1>

                            <p class="warning___p">Внимание! Не закрывайте страницу до окончания процесса!</p>

                            <div class="aoptima_progress_bar_block">
                                <div class="status___block">
                                    <p class="loading___status">0%</p>
                                </div>
                                <div class="aoptima_progress_bar"></div>
                            </div>

                            <div class="success___block"></div>

                            <p class="error___p" style="text-align: center;"></p>

                        </div>

                    </article>

                </div>

            </section>

        </div>

    </section>

</form>

<iframe id="upload___Frame" name="upload___Frame" style="display: none"></iframe>