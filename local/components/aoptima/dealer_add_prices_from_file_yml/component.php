<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('iblock');

$APPLICATION->AddChainItem('Товары', '/personal/goods/');
$APPLICATION->AddChainItem('Загрузка из файла YML', '/personal/add_prices_from_file_yml/');
$APPLICATION->SetPageProperty("title", 'Загрузка из файла YML');

if( !$USER->IsAuthorized() ){  LocalRedirect('/');  }

//$user = new project\user();

$currentUser = new project\user();
$userProps = $currentUser->arUser;
if(strlen($userProps["UF_MAIN_USER"]) > 0){
	$user = new project\user( $userProps["UF_MAIN_USER"] );
}
else{
	$user = new project\user();
}
if( !$user->isDealer() ){  LocalRedirect('/personal/');  }


// Генерируем файл каталога для дилера (для скачивания)
//$arResult['FILE_EXISTS'] = project\catalog::createYMLForDealers();






$this->IncludeComponentTemplate();