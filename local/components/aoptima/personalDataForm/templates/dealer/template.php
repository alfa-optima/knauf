<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<div class="dealerAccount-form__wrapper dealer_personal_data_block">

    <div class="dealerAccount-form__leftCol">

        <form class="dealer_personal_data_form" onsubmit="return false">

            <div class="dealerAccount-form__block">

                <h4 class="tab__title">Информация о&nbsp;компании</h4>

                <div class="dealerAccount-form__row">

                    <div class="dealerAccount-form__field  dealerAccount-form__field--w271">
                        <span class="dealerAccount-form__tooltip" title="Маркетинговое название">Название компании</span>
                        <input type="text" name="UF_COMPANY_NAME" value="<?=$arResult['USER']['UF_COMPANY_NAME']?>">
                    </div>

                    <? if( count($arResult['TYPES_OF_ACTIVITY']) > 0 && 1==2 ){ ?>

                        <div class="dealerAccount-form__field  dealerAccount-form__field--w304">
                            <span>Тип деятельности</span>
                            <div class="styledSelect">
                                <select id="businessType" name="UF_TYPE_OF_ACTIVITY">
                                    <option <? if( $arResult['USER']['UF_TYPE_OF_ACTIVITY'] == "empty" ){ echo 'selected'; } ?> value="empty">--- Выбор типа деятельности ---</option>
                                    <? foreach( $arResult['TYPES_OF_ACTIVITY'] as $type ){ ?>
                                        <option <? if( $arResult['USER']['UF_TYPE_OF_ACTIVITY'] == $type['NAME'] ){ ?>selected<? } ?> value="<?=$type['NAME']?>"><?=$type['NAME']?></option>
                                    <? } ?>
                                </select>
                            </div>
                        </div>

                    <? } ?>

                </div>

                <div class="dealerAccount-form__row">
                    <? if(
                        is_array($arResult['USER']['UF_PHONES'])
                        &&
                        count($arResult['USER']['UF_PHONES']) > 0
                    ){
                        foreach($arResult['USER']['UF_PHONES'] as $phone){ ?>
                            <div class="dealerAccount-form__field">
                                <span>Телефон</span>
                                <input class="phoneItem" type="tel" name="UF_PHONES[]" value="<?=$phone?>">
                            </div>
                        <? }
                    } else { ?>
                        <div class="dealerAccount-form__field">
                            <span>Телефон</span>
                            <input class="phoneItem" type="tel" name="UF_PHONES[]">
                        </div>
                    <? } ?>

                    <div class="dealerAccount-form__addTel">
                        <span>Добавить телефон</span>
                    </div>

                </div>

                <div class="dealerAccount-form__row">

                    <? if(
                        $arResult['IS_ADMIN'] == 'Y'
                        &&
                        $arResult['IS_SOC_USER'] != 'Y'
                    ){ ?>
                        <div class="dealerAccount-form__field">
                            <span>Логин</span>
                            <input type="text" name="LOGIN" value="<?=$arResult['USER']['LOGIN']?>">
                        </div>
                    <? } ?>

                    <div class="dealerAccount-form__field">
                        <span>Эл. почта</span>
                        <input type="email" name="EMAIL" value="<?=$arResult['USER']['EMAIL']?>">
                    </div>

                    <div class="dealerAccount-form__field">
                        <span>Сайт</span>
                        <input type="text" name="UF_SITE" value="<?=$arResult['USER']['UF_SITE']?>">
                    </div>

                </div>

            </div>

            <div class="dealerAccount-form__block">
                <h4 class="tab__title">Информация, имеющая юридическое значение</h4>
                <div class="dealerAccount-form__row">
                    <div class="dealerAccount-form__field">
                        <span>ИНН</span>
                        <input type="text" name="UF_INN" value="<?=$arResult['USER']['UF_INN']?>">
                    </div>
                    <div class="dealerAccount-form__field">
                        <span>ОГРН/ОГРНИП</span>
                        <input type="text" name="UF_OGRN" value="<?=$arResult['USER']['UF_OGRN']?>">
                    </div>
                </div>
                <div class="dealerAccount-form__row">
                    <div class="dealerAccount-form__field  dealerAccount-form__field--w600">
                        <span>Полное название компании</span>
                        <input type="text" name="UF_FULL_COMPANY_NAME" value="<?=$arResult['USER']['UF_FULL_COMPANY_NAME']?>">
                    </div>
                </div>
            </div>


            <? // Контактные лица
            $APPLICATION->IncludeComponent(
                "aoptima:dealerContactPersons", "",
                array( 'USER' => $arResult['USER'] )
            ); ?>


            <div class="dealerAccount-form__block">

                <h4 class="tab__title">Почтовый адрес</h4>

                <div class="dealerAccount-form__row">
                    <div class="dealerAccount-form__field  dealerAccount-form__field--w600">
                        <input class="" id="suggest4" type="text" placeholder="Почтовый адрес" name="UF_POST_ADDRESS" value="<?=$arResult['USER']['UF_POST_ADDRESS']?>">
                    </div>
                </div>

                <? if( 1==2 ){ ?>

                    <div class="dealerAccount-form__row">

                        <div class="dealerAccount-form__field  dealerAccount-form__field--w271">
                            <span>Город</span>
                            <input
                                class="account-form__suggestInput"
                                id="dealerPostalCity"
                                type="text"
                                placeholder="Начните ввод города"
                                cityName=""
                                autocomplete="off"
                                onfocus="cityFocus($(this));"
                                onkeyup="cityKeyUp($(this));"
                                onfocusout="cityFocusOut($(this));"
                                value="<?=$arResult['POST_CITY_LOC']['NAME_RU']?>"
                            >
                            <input type="hidden" name="UF_POST_CITY_ID" value="<?=$arResult['POST_CITY_LOC']['ID']?>">
                        </div>
                        <script>
                            $(document).ready(function(){
                                $('#dealerPostalCity').autocomplete({
                                    source: '/ajax/searchCity.php',
                                    minLength: 2,
                                    delay: 700,
                                    select: citySelectShort
                                })
                            })
                        </script>

                        <div class="dealerAccount-form__field  dealerAccount-form__field--w271">
                            <span>Область</span>
                            <input
                                class="account-form__suggestInput"
                                id="dealerPostalRegion"
                                type="text"
                                placeholder="Начните ввод региона"
                                cityName=""
                                autocomplete="off"
                                onfocus="cityFocus($(this));"
                                onkeyup="cityKeyUp($(this));"
                                onfocusout="cityFocusOut($(this));"
                                value="<?=$arResult['POST_REGION_LOC']['NAME_RU']?>"
                            >
                            <input type="hidden" name="UF_POST_REGION_ID" value="<?=$arResult['POST_REGION_LOC']['ID']?>">
                        </div>
                        <script>
                            $(document).ready(function(){
                                $('#dealerPostalRegion').autocomplete({
                                    source: '/ajax/searchRegion.php',
                                    minLength: 2,
                                    delay: 700,
                                    select: citySelect
                                })
                            })
                        </script>

                    </div>

                    <div class="dealerAccount-form__row">
                        <div class="dealerAccount-form__field  dealerAccount-form__field--w271">
                            <span>Улица</span>
                            <input class="" id="suggest4" type="text" placeholder="Ваша улица" name="UF_POST_STREET" value="<?=$arResult['USER']['UF_POST_STREET']?>">
                        </div>
                        <div class="dealerAccount-form__field  dealerAccount-form__field--w83">
                            <span>Дом</span>
                            <input type="text" name="UF_POST_HOUSE" value="<?=$arResult['USER']['UF_POST_HOUSE']?>">
                        </div>
                        <div class="dealerAccount-form__field  dealerAccount-form__field--w83">
                            <span>Квартира/Офис</span>
                            <input type="text" name="UF_POST_KV" value="<?=$arResult['USER']['UF_POST_KV']?>">
                        </div>
                        <div class="dealerAccount-form__field  dealerAccount-form__field--w83">
                            <span>Индекс</span>
                            <input type="text" name="UF_POST_INDEX" value="<?=$arResult['USER']['UF_POST_INDEX']?>">
                        </div>
                    </div>

                <? } ?>

                <div class="dealerAccount-form__note">
                    <p>Адреса складов&nbsp;Вы можете указать в&nbsp;разделе <a class="dealerAccount-form__note--addressLink" onclick="$('.points_tab').trigger('click'); var $container = $('html,body'); var $scrollTo = $('.account-form__tab--address'); $container.animate({ scrollTop: $scrollTo.scrollTop() }, 300);" style="cursor: pointer;">Адреса точек продаж</a></p>
                </div>
            </div>

            <div class="dealerAccount-form__block">
                <h4 class="tab__title">Режим работы</h4>
                <div class="dealerAccount-form__row">
                    <div class="dealerAccount-form__field  dealerAccount-form__field--w583">
                        <div class="account-form__textarea-counter">Оставшиеся символы: <span>300</span></div>
                        <textarea name="UF_REZHIM" maxlength="300" placeholder="Информация будет отображаться на вашей странице на сайте"><?=$arResult['USER']['UF_REZHIM']?></textarea>
                    </div>
                </div>
            </div>

            <div class="dealerAccount-form__block">
                <h4 class="tab__title">Дополнительная информация</h4>
                <div class="dealerAccount-form__row">
                    <div class="dealerAccount-form__field  dealerAccount-form__field--w583">
                        <div class="account-form__textarea-counter">Оставшиеся символы: <span>300</span></div>
                        <textarea name="UF_DOP_INFO" maxlength="300" placeholder="Информация будет отображаться на вашей странице на сайте"><?=$arResult['USER']['UF_DOP_INFO']?></textarea>
                    </div>
                </div>
            </div>


            <div class="dealerAccount-form__block">

                <h4 class="tab__title">Прочие настройки</h4>

                <div class="dealerAccount-form__row">
                    <div class="dealerAccount-form__field">
                        <span>Эл. почта для заказов</span>
                        <input type="text" name="UF_EMAIL_FOR_ORDERS" value="<?=$arResult['USER']['UF_EMAIL_FOR_ORDERS']?>">
                    </div>
                </div>

            </div>


            <p class="error___p"></p>

            <a style="cursor: pointer;" class="account-form__button dealerDataSaveButton to___process">Сохранить данные</a>

        </form>


    <!--    <form class="personal_settings_form" onsubmit="return false" style="border-top: 1px solid #ccc2b8; padding-top:30px">



        </form> -->

		<?if(!isset($arResult["SUB_USER"]) || empty($arResult["SUB_USER"])):?>
	        <form class="personal_password_form" onsubmit="return false" style="border-top: 1px solid #ccc2b8; padding-top:30px; margin-top: 35px;">

	            <h4 class="tab__title">Изменение пароля</h4>

	            <div class="account-form__wrapperMinusMargin" style="padding-bottom:0;">

	                <div class="account-form__field">
	                    <span>Текущий пароль</span>
	                    <input type="password" name="CURRENT_PASSWORD">
	                </div>

	            </div>

	            <div class="account-form__wrapperMinusMargin" style="padding-top:0;">

	                <div class="account-form__field">
	                    <span>Новый пароль</span>
	                    <input type="password" name="PASSWORD">
	                </div>

	                <div class="account-form__field">
	                    <span>Повтор пароля</span>
	                    <input type="password" name="CONFIRM_PASSWORD">
	                </div>

	                <div style="clear:both"></div>

	                <p class="error___p"></p>

	                <a style="cursor: pointer;" class="account-form__button personalPasswordSaveButton to___process">Сохранить</a>

	            </div>

	        </form>
	    <?endif;?>
    </div>

    <? // Блок с фото пользователя
    $APPLICATION->IncludeComponent(
        "aoptima:dealerLKPhotoBlock", "", array('USER' => $arResult['USER'])
    ); ?>



</div>