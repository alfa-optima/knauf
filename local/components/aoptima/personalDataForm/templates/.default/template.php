<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>


<section>
    <div>


        <form class="personal_data_form" onsubmit="return false">
            <h4 class="tab__title">Информация о&nbsp;покупателе</h4>
            <div class="account-form__wrapperMinusMargin">
                <div class="account-form__field">
                    <span>Фамилия</span>
                    <input type="text" name="LAST_NAME" value="<?=$arResult['USER']['LAST_NAME']?>">
                </div>
                <div class="account-form__field">
                    <span>Имя</span>
                    <input type="text" name="NAME" value="<?=$arResult['USER']['NAME']?>">
                </div>
                <div class="account-form__field">
                    <span>Дата рождения</span>
                    <input class="datepicker" type="text" name="PERSONAL_BIRTHDAY" value="<?=ConvertDateTime($arResult['USER']['PERSONAL_BIRTHDAY'], "DD.MM.YYYY", "ru")?>">
                </div>
                <div class="account-form__field">
                    <span>Пол</span>
                    <div class="account-form__checkboxes">
                        <div class="account-form__checkbox">
                            <input type="radio" id="male" name="PERSONAL_GENDER" <? if( $arResult['USER']['PERSONAL_GENDER'] == 'M' ){ echo 'checked'; } ?> value="M">
                            <label for="male">Муж</label>
                        </div>
                        <div class="account-form__checkbox">
                            <input type="radio" id="female" name="PERSONAL_GENDER" <? if( $arResult['USER']['PERSONAL_GENDER'] == 'F' ){ echo 'checked'; } ?> value="F">
                            <label for="female">Жен</label>
                        </div>
                    </div>
                </div>
                <div class="account-form__field">
                    <span>Телефон</span>
                    <input type="tel" name="PERSONAL_PHONE" value="<?=$arResult['USER']['PERSONAL_PHONE']?>" >
                </div>

                <? if(
                    $arResult['IS_ADMIN'] == 'Y'
                    &&
                    $arResult['IS_SOC_USER'] != 'Y'
                    &&
                    $arResult['IS_CALL_CENTER_USER'] != 'Y'
                ){ ?>
                    <div class="account-form__field">
                        <span>Логин</span>
                        <input type="text" name="LOGIN" value="<?=$arResult['USER']['LOGIN']?>">
                    </div>
                <? } ?>

                <? if(
                    $arResult['IS_SOC_USER'] != 'Y'
                    &&
                    $arResult['IS_CALL_CENTER_USER'] != 'Y'
                ){ ?>
                    <div class="account-form__field">
                        <span>Эл. почта</span>
                        <input type="email" name="EMAIL" value="<?=$arResult['USER']['EMAIL']?>">
                    </div>
                <? } ?>

                <div style="clear:both"></div>

                <p class="error___p"></p>

                <a style="cursor: pointer;" class="account-form__button personalDataSaveButton to___process">Сохранить</a>

            </div>

        </form>


        <? if(
            $arResult['IS_SOC_USER'] != 'Y'
            &&
            $arResult['IS_CALL_CENTER_USER'] != 'Y'
        ){ ?>

            <form class="personal_password_form" onsubmit="return false" style="border-top: 1px solid #ccc2b8; padding-top:30px">

                <h4 class="tab__title">Изменение пароля</h4>

                <div class="account-form__wrapperMinusMargin" style="padding-bottom:0;">

                    <div class="account-form__field">
                        <span>Текущий пароль</span>
                        <input type="password" name="CURRENT_PASSWORD">
                    </div>

                </div>

                <div class="account-form__wrapperMinusMargin" style="padding-top:0;">

                    <div class="account-form__field">
                        <span>Новый пароль</span>
                        <input type="password" name="PASSWORD">
                    </div>

                    <div class="account-form__field">
                        <span>Повтор пароля</span>
                        <input type="password" name="CONFIRM_PASSWORD">
                    </div>

                    <div style="clear:both"></div>

                    <p class="error___p"></p>

                    <a style="cursor: pointer;" class="account-form__button personalPasswordSaveButton to___process">Сохранить</a>

                </div>

            </form>

        <? } ?>


    </div>
</section>

