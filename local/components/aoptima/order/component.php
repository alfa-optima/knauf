<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');   use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools');   use AOptima\Tools as tools;

use Bitrix\Main,
    Bitrix\Main\Application,
    Bitrix\Main\Localization\Loc as Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem,
    Bitrix\Sale,
    Bitrix\Sale\Order,
    Bitrix\Sale\Basket,
    \Bitrix\Sale\Discount,
    \Bitrix\Sale\Result,
    Bitrix\Sale\DiscountCouponsManager,
    Bitrix\Main\Context,
    Bitrix\Main\Web\Json,
    Bitrix\Sale\PersonType,
    Bitrix\Sale\Shipment,
    Bitrix\Sale\Payment,
    Bitrix\Sale\Location\LocationTable,
    Bitrix\Sale\Services\Company,
    Bitrix\Sale\Location\GeoIp;

$arResult['SERVER_NAME'] = \Bitrix\Main\Config\Option::get('main', 'server_name');

$siteID = $arParams['IS_MOB_APP']=='Y'?'mb':\Bitrix\Main\Context::getCurrent()->getSite();

$user = new project\user();
if( $user->isDealer() ){
    if( $arParams['IS_MOB_APP']=='Y' ){
        LocalRedirect('/knauf_app/');
    } else {
        LocalRedirect('/');
    }
}

if( count($_POST) > 0 && $arParams['IS_AJAX'] != 'Y' ){    $_SESSION['ORDER_POST'] = $_POST;    }

$arResult['IS_AUTH'] = $USER->IsAuthorized()?'Y':'N';

if( count($_SESSION['ORDER_POST']) > 0 ){} else {
    if( $arParams['IS_MOB_APP']=='Y' ){
        LocalRedirect('/knauf_app/basket/');
    } else {
        LocalRedirect('/basket/');
    }
}

$arResult['POST'] = $_SESSION['ORDER_POST'];
unset($arResult['POST']['quantity']);

// Варианты оплаты
$arResult['PS_LIST'] = project\ps::getList();

$arResult['PS'] = false;
foreach ( $arResult['PS_LIST'] as $ps ){
    if( $ps['CODE'] == $arResult['POST']['ps'] ){   $arResult['PS'] = $ps;   }
}

$arResult['USER'] = tools\user::info($USER->GetID());

$arResult['last_name'] = $arParams['form_data']['last_name']?$arParams['form_data']['last_name']:$arResult['USER']['LAST_NAME'];
$arResult['name'] = $arParams['form_data']['name']?$arParams['form_data']['name']:$arResult['USER']['NAME'];
$arResult['birthday'] = $arParams['form_data']['birthday']?$arParams['form_data']['birthday']:ConvertDateTime($arResult['USER']['PERSONAL_BIRTHDAY'], "DD.MM.YYYY", "ru");
$arResult['gender'] = $arParams['form_data']['gender']?$arParams['form_data']['gender']:$arResult['USER']['PERSONAL_GENDER'];
$arResult['phone'] = $arParams['form_data']['phone']?$arParams['form_data']['phone']:$arResult['USER']['PERSONAL_PHONE'];
$arResult['email'] = $arParams['form_data']['email']?$arParams['form_data']['email']:$arResult['USER']['EMAIL'];

if( strlen($arParams['form_data']['coords']) > 0 ){} else {
    unset($arParams['form_data']['address']);
}

$arResult['under_order_goods'] = project\catalog::getUnderOrderGoods();

$arResult['BASKET_OFFERS'] = [];

$arResult['basketInfo'] = project\user_basket::info(
    false, $arResult['POST']['ds'], null, null,
    $siteID
);

if( is_array($_POST['BASKET_OFFERS']) ){

    $basketUserID = Sale\Fuser::getId();
    $userBasket = Sale\Basket::loadItemsForFUser( $basketUserID, $siteID );

    foreach ( $userBasket as $bItem ){
        if( !$_POST['BASKET_OFFERS'][$bItem->getProductId().(($bItem->getField('CUSTOM_PRICE') == "Y")?"_custom":"")] ){
            \CSaleBasket::Delete($bItem->getId());
        } else {
            \CSaleBasket::Update(
                $bItem->getId(),
                array(
                    "QUANTITY" => $_POST['BASKET_OFFERS'][$bItem->getProductId().(($bItem->getField('CUSTOM_PRICE') == "Y")?"_custom":"")]['QUANTITY']
                ));
        }
    }
    $arResult['basketInfo'] = project\user_basket::info(
        false, $arResult['POST']['ds'], null, null,
        $siteID
    );
}
unset($arResult['basketInfo']['basket']);
unset($arResult['basketInfo']['arBasket']);



if( $arResult['basketInfo']['basketProductsCNT'] == 0 ){
    if( $arParams['IS_MOB_APP']=='Y' ){
        LocalRedirect('/knauf_app/basket/');
    } else {
        LocalRedirect('/basket/');
    }
}

$knaufOrder = new project\order();

if( intval($arResult['POST']['dealer_id']) > 0 ){} else {
    if( $arParams['IS_MOB_APP']=='Y' ){
        LocalRedirect('/knauf_app/basket/');
    } else {
        LocalRedirect('/basket/');
    }
}

// Инфо о дилере
$arResult['DEALER'] = tools\user::info($arResult['POST']['dealer_id']);

$arResult['DEALER_URL'] = ($arParams['IS_MOB_APP']=='Y'?'/knauf_app':'').'/dealers/'.$arResult['DEALER']['ID'].'/';

if(
    intval($arResult['DEALER']['ID']) > 0
    &&
    is_array($arResult['DEALER']['UF_PSS'])
    &&
    count($arResult['DEALER']['UF_PSS']) > 0
){} else {
    if( $arParams['IS_MOB_APP']=='Y' ){
        LocalRedirect('/knauf_app/basket/');
    } else {
        LocalRedirect('/basket/');
    }
}

// Доп. инфо о дилере
$arResult['DEALER_PHONES'] = $arResult['DEALER']['UF_PHONES'];
if(
    strlen($arResult['DEALER']['UF_CONTACT_PERSONS']) > 2
    &&
    intval($arResult["LOC_ID"]) > 0
){
    $arResult['DEALER']['CONTACT_PERSONS'] = tools\funcs::json_to_array($arResult['DEALER']['UF_CONTACT_PERSONS']);
    if( is_array($arResult['DEALER']['CONTACT_PERSONS']) ){
        $arResult['DEALER_PHONES'] = [];
        foreach ( $arResult['DEALER']['CONTACT_PERSONS'] as $person ){
            if( $person['loc_id'] == $arResult["LOC_ID"] ){
                $name = [];
                if( strlen($person['name']) > 0 ){
                    $name[] = $person['name'];
                    if( strlen($person['second_name']) > 0 ){
                        $name[] = ''.$person['second_name'];
                    }
                    if( strlen($person['last_name']) > 0 ){
                        $name[] = ''.$person['last_name'];
                    }
                }
                $name = implode(' ', $name);

                $arResult['DEALER_PHONES'][] = $name.(strlen($name)>0?'<br>':'').implode('<br>', $person['phones']);
            }
        }
    }
}
$arResult['DEALER_LOGO_SRC'] = false;
if( intval($arResult['DEALER']['PERSONAL_PHOTO']) > 0 ){
    $arResult['DEALER_LOGO_SRC'] = 'http://'.$arResult['SERVER_NAME'].tools\funcs::rIMGG($arResult['DEALER']['PERSONAL_PHOTO'], 4, 100, 60);
}

$APPLICATION->SetPageProperty("title", 'Оформление заказа - дилер '.$arResult['DEALER']['UF_COMPANY_NAME']);

$arResult['DEALER']['BASKET_SUM'] = 0;

// местоположение пользователя
$arResult['LOC'] = false;
if( intval($arResult['POST']['loc_id']) > 0 ){
    // Инфо по местоположению
    $loc = project\bx_location::getByID($arResult['POST']['loc_id']);
    if( intval($loc['ID']) > 0 ){   $arResult['LOC'] = $loc;   }
    // Родители местоположения
    $arResult['LOC_CHAIN'] = project\bx_location::getLocChain($arResult['LOC']['ID']);
    $arResult['LOC_CHAIN_IDS'] = array_reverse(array_keys($arResult['LOC_CHAIN']));
}

// Шаблоны адресов доставки
$arResult['deliveryTemplates'] = array();
if ($USER->IsAuthorized()){
    // Шаблоны доставки
    $deliveryTemplate = new project\delivery_template();
    $arResult['deliveryTemplates'] = $deliveryTemplate->getList($USER->GetID(), false, /*$arResult['POST']['loc_id']*/ false );
}
// Варианты списка по лифту
$arResult['LIFT_ENUMS'] = array();
$props = \CUserTypeEntity::GetList([$by => $order], ['ENTITY_ID' => 'HLBLOCK_' . project\delivery_template::HIBLOCK_ID, 'FIELD_NAME' => 'UF_LIFT']);
while ($prop = $props->Fetch()){
    $enums = \CUserFieldEnum::GetList([], ["USER_FIELD_ID" => $prop['ID']]);
    while ($enum = $enums->GetNext()) {
        $arResult['LIFT_ENUMS'][$enum['XML_ID']] = $enum;
    }
}
// Доп. пункты к адресу доставки
$arResult['ADDRESS_DOP'] = $knaufOrder->addressDopPunkty();

$arResult['isFullOffer'] = true;

$arDealerQuantities = Array();
foreach ($arResult['basketInfo']['basketProducts'] as $productKey => $bProduct) {
    $bProduct['NAL'] = 'N';

    // Найдём ТП к данному товару для данного дилера
    $tpList = array();
    foreach( $arResult['LOC_CHAIN_IDS'] as $loc_id ) {
        if( count($tpList) == 0 ){
            $tpList = project\tp::getList($bProduct['el']['ID'], $arResult['DEALER']['ID'], $loc_id);
        }
    }

    if (count($tpList) > 0){

        // ТП
        $tp = $tpList[0];
        if($bProduct["custom_price"])
        {
            $price = $bProduct["price"];
        }
        else
        {
            // Цена ТП дилера
            $price = $tp['CATALOG_PRICE_' . project\catalog::PRICE_ID];
        }

        if ($tp && ($price || $bProduct["custom_price"])){

            $bProduct['DEALER_PRICE'] = $price;
            $bProduct['DEALER_PRICE_FORMAT'] = number_format($price, project\catalog::ROUND, ",", " ");

            // Количество на складе дилера
            if(!isset($arDealerQuantities[$tp['ID']]))
            {
                $arDealerQuantities[$tp['ID']] = $tp['CATALOG_QUANTITY'];
                $bProduct['DEALER_QUANTITY'] = $tp['CATALOG_QUANTITY'];
            }
            else
            {
                $bProduct['DEALER_QUANTITY'] = $arDealerQuantities[$tp['ID']];
            }

            if( $bProduct['DEALER_QUANTITY'] > 0 ){
                $bProduct['NAL'] = 'Y';
                $hasGoods = true;
            }
            if ($bProduct['DEALER_QUANTITY'] < $bProduct['quantity']){
                $arResult['isFullOffer'] = false;
                $bProduct['FULL_QUANTITY'] = 'N';
                $arResult['DEALER']['BASKET_SUM'] += $bProduct['DEALER_PRICE'] * $bProduct['DEALER_QUANTITY'];

                $arDealerQuantities[$tp['ID']] = 0;

            } else {
                $arResult['DEALER']['BASKET_SUM'] += $bProduct['DEALER_PRICE'] * $bProduct['quantity'];

                $arDealerQuantities[$tp['ID']] -= $bProduct['quantity'];
            }

            $arResult['BASKET_OFFERS'][$tp['ID'].($bProduct["custom_price"]?"_custom":"")] = array(
                'ID' => $tp['ID'],
                'REQUIRED_QUANTITY' => $bProduct['quantity'],
                'QUANTITY' => ($bProduct['DEALER_QUANTITY']>=$bProduct['quantity']?$bProduct['quantity']:$bProduct['DEALER_QUANTITY'])
            );

        } else {    $arResult['isFullOffer'] = false;    }

    } else {    $arResult['isFullOffer'] = false;    }

    $arResult['DEALER']['basketProducts'][$productKey] = $bProduct;
}

$arResult['DEALER']['BASKET_SUM_FORMAT'] = number_format($arResult['DEALER']['BASKET_SUM'], project\catalog::ROUND, ",", "&nbsp;");

// Разделим товары на обычные и крупногабаритные
$arResult['normalGoods'] = [];   $arResult['largeGoods'] = [];
$arResult['normalGoodsWeight'] = 0;   $arResult['largeGoodsWeight'] = 0;
foreach( $arResult['basketInfo']['basketProducts'] as $basketProduct ){
    $el = \AOptima\Tools\el::info($basketProduct['el']['ID']);
    if( $el['PROPERTY_MAX_GABARIT_VALUE'] >= project\delivery_price::LARGE_SIZE ){
        $arResult['largeGoodsWeight'] += $el['PROPERTY_CALC_WEIGHT_VALUE'] * $basketProduct['quantity'];
        $arResult['largeGoods'][] = $basketProduct;
    } else {
        $arResult['normalGoodsWeight'] += $el['PROPERTY_CALC_WEIGHT_VALUE'] * $basketProduct['quantity'];
        $arResult['normalGoods'][] = $basketProduct;
    }
}
$arResult['largeGoodsWeight'] = $arResult['largeGoodsWeight'] / 1000;
$arResult['normalGoodsWeight'] = $arResult['normalGoodsWeight'] / 1000;

// Адрес доставки по умолчанию
$arResult['USER_DEFAULT_DELIV_ADDRESS'] = [];
if(
    is_array($arResult['DEALER'])
    &&
    strlen($arResult['DEALER']['UF_DEFAULT_ADDRESS']) > 0
){
    $arResult['USER_DEFAULT_DELIV_ADDRESS'] = tools\funcs::json_to_array($arResult['DEALER']['UF_DEFAULT_ADDRESS']);
}

// Координаты адреса доставки
if( strlen($arResult['POST']['address_coords']) > 0 ){
    $arResult['ADDRESS_COORDS'] = strip_tags($arResult['POST']['address_coords']);
} else if( isset($arResult['USER_DEFAULT_DELIV_ADDRESS']['COORDS']) ){
    $arResult['ADDRESS_COORDS'] = $arResult['USER_DEFAULT_DELIV_ADDRESS']['COORDS'];
}

// Тип доставки "Доставка"
if( $arResult['POST']['delivType'] == 'delivery' ){
    
    // Определяем стоимость доставки для дилера
    $arResult['DEALER'] = project\basket::getDealerDelivPrice(
        $arResult,
        $arResult['DEALER'],
        $arResult['POST']['delivType'],
        $arParams['form_data'],
        [
            'normalGoods' => $arResult['normalGoods'],
            'largeGoods' => $arResult['largeGoods'],
            'normalGoodsWeight' => $arResult['normalGoodsWeight'],
            'largeGoodsWeight' => $arResult['largeGoodsWeight']
        ]
    );

    $r = preg_match("/[а-яёА-ЯЁ]+/i", $arResult['DEALER']['DELIVERY_PRICE']);
    if( $r ){
        $arResult['DEALER']['DELIVERY_PRICE_FORMAT'] = false;
    } else {
        $arResult['DEALER']['DELIVERY_PRICE_FORMAT'] = number_format($arResult['DEALER']['DELIVERY_PRICE'], project\catalog::ROUND, ",", "&nbsp;");
    }

// Тип доставки "Самовывоз"
} else {

    // Стоимость доставки
    $arResult['DEALER']['DELIVERY_PRICE'] = 0;

}



// Сумма заказа
$arResult['DEALER']['TOTAL_SUM'] = $arResult['DEALER']['BASKET_SUM'] + $arResult['DEALER']['DELIVERY_PRICE'];
$arResult['DEALER']['TOTAL_SUM_FORMAT'] = number_format( $arResult['DEALER']['TOTAL_SUM'], project\catalog::ROUND, ",", "&nbsp;" );

// Перечень ПВЗ дилера (точно в данном местоположении)
$dealer_shop = new project\dealer_shop();
$arResult['DEALER']['PVZ_LIST'] = $dealer_shop->getList(
    $arResult['DEALER']['ID'],
    $arResult['LOC']['ID'],
    true
);
$arResult['POST_PVZ'] = false;
if (intval($arResult['POST']['pvz']) > 0){

    $arResult['POST_PVZ'] = $arResult['DEALER']['PVZ_LIST'][$arResult['POST']['pvz']];
    $address = [];
    $address[] = $arResult['LOC']['NAME_RU'];
    if( strlen($arResult['POST_PVZ']['PROPERTY_STREET_VALUE']) > 0 ){
        $address[] = 'ул.'.strip_tags($arResult['POST_PVZ']['PROPERTY_STREET_VALUE']);
    }
    if( strlen($arResult['POST_PVZ']['PROPERTY_HOUSE_VALUE']) > 0 ){
        $address[] = 'д.'.strip_tags($arResult['POST_PVZ']['PROPERTY_HOUSE_VALUE']);
    }
    if( strlen($arResult['POST_PVZ']['PROPERTY_OFFICE_VALUE']) > 0 ){
        $address[] = 'оф.'.strip_tags($arResult['POST_PVZ']['PROPERTY_OFFICE_VALUE']);
    }
    $arResult['POST_PVZ']['ADDRESS'] = implode(', ', $address);

    if( strlen($arResult['POST_PVZ']['PROPERTY_PHONES_VALUE']['TEXT']) > 0 ){
        $arPhones = tools\funcs::json_to_array($arResult['POST_PVZ']['PROPERTY_PHONES_VALUE']['TEXT']);
        $arResult['POST_PVZ']['PHONES'] = implode(', ', $arPhones);
    }

    if( is_array($arResult['POST_PVZ']['PROPERTY_WORK_DAYS_VALUE']) > 0 ){
        $days = [];
        foreach ( $arResult['POST_PVZ']['PROPERTY_WORK_DAYS_VALUE'] as $day_num ){
            $days[] = project\dealer_shop::$work_days[$day_num]['short_title'];
        }
        $arResult['POST_PVZ']['WORK_DAYS'] = implode(', ', $days);
    }
}


$arResult['SHOW_ORDER_INFO'] = 'N';
if(
    $arResult['IS_AUTH'] == 'Y'
    ||
    (
        $arResult['IS_AUTH'] == 'N'
        &&
        $arParams['form_data']['new_user'] == 'Y'
        &&
        $arParams['form_data']['order_register']
    )
){     $arResult['SHOW_ORDER_INFO'] = 'Y';     }



$arResult['DEALER_WARNINGS'] = $_SESSION['DEALER_WARNINGS'][ $arResult['DEALER']['ID'] ];





$this->IncludeComponentTemplate();