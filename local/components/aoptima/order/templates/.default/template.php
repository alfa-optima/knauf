<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$basketInfo = $arResult['basketInfo'];

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>


<article class="block  block--top orderArea">

    <form class="orderForm" action="<?=$arParams['IS_MOB_APP']=='Y'?'/knauf_app':''?>/order_success/" method="post" onsubmit="return false">

        <input type="hidden" name="where_from" value="<?=$arParams['IS_MOB_APP']=='Y'?'APP':'SITE'?>">

        <div class="block__wrapper">

            <h2 class="block__title  block__title--faq">Оформление заказа</h2>

            <script type="text/javascript">
                dropDownHandler();
            </script>

            <? if( $arResult['IS_AUTH'] == 'N' ){ ?>

                <section class="cart-form__block  cart-form__block--buttons">
                    <div class="account-form__wrapperMinusMargin">
                        <div class="account-form__field">
                            <div class="account-form__checkbox">
                                <input type="radio" id="registered" name="new_user" <? if( $arParams['form_data']['new_user'] == 'N' ){ echo 'checked'; } ?> value="N">
                                <label for="registered">Я уже зарегистрирован</label>
                            </div>
                        </div>
                        <div class="account-form__field">
                            <div class="account-form__checkbox">
                                <input type="radio" id="new_user" name="new_user" <? if( $arParams['form_data']['new_user'] == 'Y' ){ echo 'checked'; } ?> value="Y">
                                <label for="new_user">Я новый пользователь</label>
                            </div>
                        </div>
                    </div>
                </section>

                <? if( $arParams['form_data']['new_user'] == 'Y' ){ ?>

                    <section class="cart-form__block  cart-form__block--buttons">
                        <div class="account-form__wrapperMinusMargin">
                            <div class="account-form__field">
                                <div class="account-form__checkbox">
                                    <input type="radio" id="register" name="order_register" <? if( $arParams['form_data']['order_register'] == 'Y' ){ echo 'checked'; } ?> value="Y">
                                    <label for="register">Зарегистрируйте меня</label>
                                </div>
                            </div>
                            <div class="account-form__field">
                                <div class="account-form__checkbox">
                                    <input type="radio" id="withoutreg" name="order_register" <? if( $arParams['form_data']['order_register'] == 'N' ){ echo 'checked'; } ?> value="N">
                                    <label for="withoutreg">Я не хочу регистрироваться</label>
                                </div>
                            </div>
                        </div>
                    </section>

                <? } else if( $arParams['form_data']['new_user'] == 'N' ){ ?>

                    <section class="cart-form__block">
                        <h4 class="tab__title">Авторизуйтесь</h4>
                        <div class="account-form__wrapperMinusMargin">

                            <div class="account-form__field order_email___block">
                                <span>Эл. почта / логин *</span>
                                <input class="order_login" type="text" name="login">
                            </div>

                            <div class="account-form__field order_email___block">
                                <span>Пароль *</span>
                                <input class="order_password" type="password" name="password">
                            </div>

                        </div>

                        <div class="account-form__wrapperMinusMargin basket-auth-form-btns">

                            <p class="error___p"></p>

                            <a style="cursor: pointer;" class="cart-form__order-button orderAuthButton to___process">Авторизоваться</a>

                            <a href="<?=$arParams['IS_MOB_APP']=='Y'?'/knauf_app':''?>/password_recovery/" target="_blank" class="form__link  form__link--restore">Забыли пароль?</a>

                        </div>

                    </section>

                <? } ?>

            <? } ?>


            <? if( $arResult['SHOW_ORDER_INFO'] == 'Y' ){ ?>

                <section class="cart-form__block">
                    <h4 class="tab__title">Информация о&nbsp;покупателе</h4>
                    <div class="account-form__wrapperMinusMargin">

                        <div class="account-form__field">
                            <span>Фамилия *</span>
                            <input type="text" name="last_name" value="<?=$arResult['last_name']?>">
                        </div>

                        <div class="account-form__field">
                            <span>Имя *</span>
                            <input type="text" name="name" value="<?=$arResult['name']?>">
                        </div>

                        <div class="account-form__field" <? if( $arResult['IS_AUTH'] == 'Y' || ( $arResult['IS_AUTH'] == 'N' && $arParams['form_data']['order_register'] == 'Y' || !$arParams['form_data']['order_register'] ) ){} else { echo 'style="display:none;"'; } ?>>
                            <span>Дата рождения</span>
                            <input class="orderDatepicker" type="text" name="birthday" value="<?=$arResult['birthday']?>">
                        </div>

                        <? if( $arParams['IS_AJAX'] == 'Y' ){ ?>
                            <script>
                            $(document).ready(function(){
                                $(".orderDatepicker").datepicker({
                                    inline: true,
                                    showOtherMonths: true,
                                    minDate: 0
                                });
                            })
                            </script>
                        <? } ?>

                        <div class="account-form__field" <? if( $arResult['IS_AUTH'] == 'Y' || ( $arResult['IS_AUTH'] == 'N' && $arParams['form_data']['order_register'] == 'Y' || !$arParams['form_data']['order_register'] ) ){} else { echo 'style="display:none;"'; } ?>>
                            <span>Пол</span>
                            <div class="account-form__checkboxes">
                                <div class="account-form__checkbox">
                                    <input type="radio" id="male" name="gender" <? if( $arResult['gender'] == 'M' ){ echo 'checked'; } ?> value="M">
                                    <label for="male">Муж</label>
                                </div>
                                <div class="account-form__checkbox">
                                    <input type="radio" id="female" name="gender" <? if( $arResult['gender'] == 'F' ){ echo 'checked'; } ?> value="F">
                                    <label for="female">Жен</label>
                                </div>
                            </div>
                        </div>

                        <div class="account-form__field">
                            <span>Телефон *</span>
                            <input type="tel" name="phone" value="<?=$arResult['phone']?>">
                        </div>

                        <div class="account-form__field order_email___block">
                            <span>Эл. почта *</span>
                            <input type="email" name="email" value="<?=$arResult['email']?>">
                        </div>

                    </div>
                </section>

                <section class="cart-form__block deliveryBlock">
                    <div class="account-form__wrapperMinusMargin">

                        <h4 class="tab__title">Ваш заказ будет исполнен официальным дилером КНАУФ:</h4>
                        <div class="account-form__wrapperMinusMargin">
                            <div class="account-form__row">
                                <div class="cart-form__deliveryColumns">

                                    <div class="cart-form__deliveryColumn">

                                        <span class="cart-form__deliveryColumn-title"><a class="cart-offer__table-dealerName" href="<?=$arResult['DEALER_URL']?>" target="_blank"><?=strlen($arResult['DEALER']['UF_FULL_COMPANY_NAME'])>0?$arResult['DEALER']['UF_FULL_COMPANY_NAME']:$arResult['DEALER']['UF_COMPANY_NAME']?></a></span>

                                        <?php if( count($arResult['DEALER_PHONES']) > 0 ){ ?>
                                            <span class="cart-form__deliveryColumn-title">Контактные телефоны:<br><?=implode('<br>', $arResult['DEALER_PHONES'])?></span>
                                        <?php } ?>
										
										<?php if($arResult['DEALER']['UF_REZHIM']){ ?>
                                            <span class="cart-form__deliveryColumn-title">Режим работы:<br>
											<? echo $arResult['DEALER']['UF_REZHIM'];?></span>
                                        <?php } ?>

                                    </div>

                                    <?php if( $arResult['DEALER_LOGO_SRC'] ){ ?>

                                        <div class="cart-form__deliveryColumn">
                                            <img src="<?=$arResult['DEALER_LOGO_SRC']?>">
                                        </div>

                                    <?php } ?>

                                </div>
                            </div>
                        </div>

                    </div>
                </section>

                <? // Инфо по доставке
                if( $arResult['POST']['delivType'] == 'delivery' ){ ?>

                    <section class="cart-form__block deliveryBlock">

                        <h4 class="tab__title">Информация по доставке:</h4>

                        <div class="account-form__wrapperMinusMargin">

                            <div class="account-form__row">
                                <div class="cart-form__deliveryColumns">
                                    <div class="cart-form__deliveryColumn">
                                        <span class="cart-form__deliveryColumn-title">Тип доставки:</span>
                                        <span class="cart-form__deliveryColumn-text">Доставка</span>
                                    </div>
                                </div>
                            </div>

                            <div class="account-form__row">
                                <div class="cart-form__deliveryColumns">
                                    <div class="cart-form__deliveryColumn">
                                        <span class="cart-form__deliveryColumn-title">Дата доставки:</span>
                                        <span class="cart-form__deliveryColumn-text"><?=$arResult['POST']['deliveryDate']?></span>
                                    </div>
                                </div>
                            </div>

                            <div class="account-form__row">
                                <div class="cart-form__deliveryColumns">
                                    <div class="cart-form__deliveryColumn">
                                        <span class="cart-form__deliveryColumn-title">Время доставки:</span>
                                        <span class="cart-form__deliveryColumn-text"><?=$arResult['POST']['deliveryTime']?></span>
                                    </div>
                                </div>
                            </div>

                            <div class="account-form__row">
                                <div class="cart-form__deliveryColumns">
                                    <div class="cart-form__deliveryColumn">
                                        <span class="cart-form__deliveryColumn-title">Населённый пункт:</span>
                                        <span class="cart-form__deliveryColumn-text"><?=$arResult['LOC']['NAME_RU']?></span>
                                    </div>
                                </div>
                            </div>

                            <div class="account-form__row">
                                <div class="cart-form__deliveryColumns">
                                    <div class="cart-form__deliveryColumn">
                                        <span class="cart-form__deliveryColumn-title">Адрес:</span>
                                        <span class="cart-form__deliveryColumn-text"><?=$arResult['POST']['address']?></span>
                                    </div>
                                </div>
                            </div>

                            <? if( strlen($arResult['POST']['podyezd']) > 0 ){ ?>
                                <div class="account-form__row">
                                    <div class="cart-form__deliveryColumns">
                                        <div class="cart-form__deliveryColumn">
                                            <span class="cart-form__deliveryColumn-title">Подъезд:</span>
                                            <span class="cart-form__deliveryColumn-text"><?=$arResult['POST']['podyezd']?></span>
                                        </div>
                                    </div>
                                </div>
                            <? } ?>

                            <? if( strlen($arResult['POST']['floor']) > 0 ){ ?>
                                <div class="account-form__row">
                                    <div class="cart-form__deliveryColumns">
                                        <div class="cart-form__deliveryColumn">
                                            <span class="cart-form__deliveryColumn-title">Этаж:</span>
                                            <span class="cart-form__deliveryColumn-text"><?=$arResult['POST']['floor']?></span>
                                        </div>
                                    </div>
                                </div>
                            <? } ?>

                            <? if( strlen($arResult['POST']['kvartira']) > 0 ){ ?>
                                <div class="account-form__row">
                                    <div class="cart-form__deliveryColumns">
                                        <div class="cart-form__deliveryColumn">
                                            <span class="cart-form__deliveryColumn-title">кв/офис:</span>
                                            <span class="cart-form__deliveryColumn-text"><?=$arResult['POST']['kvartira']?></span>
                                        </div>
                                    </div>
                                </div>
                            <? } ?>

                            <div>

                                <? if( 1==2 ){ ?>

                                    <? if( count($arResult['deliveryTemplates']) > 0 ){ ?>

                                        <div class="account-form__row">
                                            <div class="account-form__field  account-form__field--w318">
                                                <span>Выберите шаблон адреса доставки</span>
                                                <div class="styledSelect deliveryTemplatesSelect">
                                                    <select id="deliveryTemplates" name="deliveryTemplate" onchange="deliveryTemplateChange( $(this) );">

                                                        <option <? if( $arParams['form_data']['deliveryTemplate'] == 'empty' ){ ?>selected<? } ?> value="empty">--- Шаблон не выбран ---</option>

                                                        <? foreach( $arResult['deliveryTemplates'] as $template ){ ?>

                                                            <option <? if( $arParams['form_data']['deliveryTemplate'] == $template['ID'] ){ ?>selected<? } ?> value="<?=$template['ID']?>"><?=$template['UF_TEMPLATE']?></option>

                                                        <? } ?>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    <? } ?>

                                    <div class="account-form__row">
                                        <div class="account-form__field  account-form__field--w760">
                                            <span>Адрес (улица и номер дома) *</span>
                                            <input
                                                class="account-form__suggestInput"
                                                id="orderAddress"
                                                type="text"
                                                process="N"
                                                name="address"
                                                addressName=""
                                                value="<?=$arParams['form_data']['address']?>"
                                                onfocus="addressFocus($(this));"
                                                <? if( 1==2 ){ ?>onkeyup="addressKeyUp($(this));"<? } ?>
                                                onfocusout="addressFocusOut($(this), '<?=$arParams['form_data']['coords']?>');"
                                                autocomplete="off"
                                            >
                                            <input type="hidden" name="coords" value="<?=$arParams['form_data']['coords']?>">
                                            <script>
                                            $(document).ready(function(){
                                                $('#orderAddress').autocomplete({
                                                    source: '/ajax/searchAddress.php',
                                                    minLength: 2,
                                                    delay: 700,
                                                    select: addressSelect,
                                                    response: function(event, ui){
                                                        if( ui.content.length == 0 ){
                                                            reloadOrder();
                                                        }
                                                    }
                                                });
                                            })
                                            </script>
                                        </div>
                                    </div>

                                    <div class="account-form__row">
                                        <div class="account-form__fields">
                                            <div class="account-form__field  account-form__field--w85">
                                                <span>Подъезд</span>
                                                <input type="text" onkeyup="reloadDeliveryTemplates();" name="podyezd" value="<?=$arParams['form_data']['podyezd']?>">
                                            </div>
                                            <div class="account-form__field  account-form__field--w85">
                                                <span>Этаж</span>
                                                <input type="text" onkeyup="reloadDeliveryTemplates();" name="floor" value="<?=$arParams['form_data']['floor']?>">
                                            </div>
                                            <div class="account-form__field  account-form__field--w85">
                                                <span>кв/офис</span>
                                                <input type="text" onkeyup="reloadDeliveryTemplates();" name="kvartira" value="<?=$arParams['form_data']['kvartira']?>">
                                            </div>
                                        </div>
                                        <div class="account-form__field  account-form__field--w416  account-form__field--mr0">
                                            <div class="account-form__checkboxes">

                                                <? foreach( $arResult['LIFT_ENUMS'] as $enum ){ ?>

                                                    <div class="account-form__checkbox  account-form__checkbox--mr35">
                                                        <input class="liftCheckbox" <? if( $arParams['form_data']['lift'] == $enum['VALUE']){ ?>checked<? } ?> type="radio" id="lift_<?=$enum['XML_ID']?>" name="lift" value="<?=$enum['VALUE']?>" lift_enum_id="<?=$enum['ID']?>" onchange="reloadDeliveryTemplates();">
                                                        <label for="lift_<?=$enum['XML_ID']?>"><?=$enum['VALUE']?></label>
                                                    </div>

                                                <? } ?>

                                            </div>
                                        </div>
                                    </div>

                                <? } ?>

                                <div class="account-form__field  account-form__field--w760">
                                    <span>Комментарий</span>
                                    <div class="account-form__textarea-counter">Оставшиеся символы: <span>300</span></div>
                                    <textarea name="comment" maxlength="300" placeholder="Например, можно указать другие товары, которые Вы бы хотели заказать у дилера, но которых нет в каталоге"><?=$arParams['form_data']['comment']?></textarea>
                                </div>

                            </div>

                        </div>
                    </section>

                <? // Самовывоз
                } if( $arResult['POST']['delivType'] == 'samovyvoz' ){ ?>

                    <section class="cart-form__block samovyvozBlock">

                        <h4 class="tab__title">Информация по доставке:</h4>

                        <div class="account-form__wrapperMinusMargin">

                            <div class="account-form__row">
                                <div class="cart-form__deliveryColumns">
                                    <div class="cart-form__deliveryColumn">
                                        <span class="cart-form__deliveryColumn-title">Тип доставки:</span>
                                        <span class="cart-form__deliveryColumn-text">Самовывоз</span>
                                    </div>
                                </div>
                            </div>

                            <div class="account-form__row">
                                <div class="cart-form__deliveryColumns">
                                    <div class="cart-form__deliveryColumn">
                                        <span class="cart-form__deliveryColumn-title">Населённый пункт:</span>
                                        <span class="cart-form__deliveryColumn-text"><?=$arResult['LOC']['NAME_RU']?></span>
                                    </div>
                                </div>
                            </div>

                            <div class="account-form__row">
                                <div class="cart-form__deliveryColumns">
                                    <div class="cart-form__deliveryColumn">
                                        <span class="cart-form__deliveryColumn-title">Пункт выдачи заказа:</span>
                                        <span class="cart-form__deliveryColumn-text"><?=$arResult['POST_PVZ']['NAME']?>
                                            <? if( strlen($arResult['POST_PVZ']['ADDRESS']) > 0 ){ ?>
                                                <br>Адрес: <?=$arResult['POST_PVZ']['ADDRESS']?>
                                            <? } ?>
                                            <? if( strlen($arResult['POST_PVZ']['PHONES']) > 0 ){ ?>
                                                <br>Телефоны: <?=$arResult['POST_PVZ']['PHONES']?>
                                            <? } ?>
                                            <? //if( strlen($arResult['POST_PVZ']['WORK_DAYS']) > 0 ){ ?>
                                                <!--<br>Рабочие дни: --><?//=$arResult['POST_PVZ']['WORK_DAYS']?>
                                            <?// } ?>

                            <? /*if(
                                strlen( $arResult['POST_PVZ']['PROPERTY_WORK_TIME_OT_VALUE'] ) > 0
                                &&
                                strlen( $arResult['POST_PVZ']['PROPERTY_WORK_TIME_DO_VALUE'] ) > 0
                            ){*/ ?>
                                <!--<br>Рабочее время: с <?//=$arResult['POST_PVZ']['PROPERTY_WORK_TIME_OT_VALUE']?> по <?//=$arResult['POST_PVZ']['PROPERTY_WORK_TIME_DO_VALUE']?>-->
                            <? //} ?>
							
								<?if (strlen($arResult['POST_PVZ']['~PROPERTY_REZHIM_RABOTY_VALUE']['TEXT'])>0):?>
									<br>
									<? echo htmlspecialcharsBack($arResult["POST_PVZ"]["~PROPERTY_REZHIM_RABOTY_VALUE"]["TEXT"]);?>
								<?endif;?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                <? } ?>

                <section class="cart-form__block samovyvozBlock">

                    <h4 class="tab__title">Информация по заказу:</h4>

                    <div class="account-form__wrapperMinusMargin">

                        <div class="account-form__row">
                            <div class="cart-form__deliveryColumns">
                                <div class="cart-form__deliveryColumn">
                                    <span class="cart-form__deliveryColumn-title">Вариант оплаты:</span>
                                    <span class="cart-form__deliveryColumn-text"><?=$arResult['PS']['NAME']?></span>
                                </div>
                            </div>
                        </div>

                        <div class="account-form__row">
                            <div class="cart-form__deliveryColumns">
                                <div class="cart-form__deliveryColumn">
                                    <span class="cart-form__deliveryColumn-title">Тип оплаты:</span>
                                    <span class="cart-form__deliveryColumn-text"><? if( $arResult['POST']['pay_type'] == 'pre' ){ echo 'Предоплата'; } else if( $arResult['POST']['pay_type'] == 'post' ){ echo 'Постоплата'; } ?></span>
                                </div>
                            </div>
                        </div>

                    </div>

                </section>

                <section class="cart-form__block">

                    <h4 class="tab__title">Корзина</h4>

                    <div class="cart-form__inCart">

                        <table class="cart-form__inCart-table">
                            <tbody>
                            <tr>
                                <th>Название</th>
                                <th>Количество</th>
                                <th>Цена</th>
                            </tr>

<? // Товары корзины (в наличии + хватает)
foreach( $arResult['DEALER']['basketProducts'] as $basketProduct ){
    if(
        $basketProduct['NAL'] == 'Y'
        &&
        $basketProduct['DEALER_QUANTITY'] >= $basketProduct['quantity']
    ){
        if( $arParams['IS_MOB_APP'] == 'Y' ){
            $basketProduct['el']['DETAIL_PAGE_URL'] = project\catalog::toMobileAppUrl($basketProduct['el']['DETAIL_PAGE_URL']);
        }

        ?>

        <tr>
            <td>
                <div>
                    <? if( intval($basketProduct['el']['DETAIL_PICTURE']) > 0 ){ ?>
                        <a href="<?=$basketProduct['el']['DETAIL_PAGE_URL']?>" class="cart-form__inCart-image" target="_blank">
                            <img src="<?=tools\funcs::rIMGG($basketProduct['el']['DETAIL_PICTURE'], 4, 50, 46)?>">
                        </a>
                    <? } else { ?>
                        <div style="display: block; width:50px; height:46px;"></div>
                    <? } ?>
                    <a href="<?=$basketProduct['el']['DETAIL_PAGE_URL']?>" class="cart-form__inCart-title"><?=$basketProduct['el']['NAME']?></a>
                </div>
            </td>
            <td>
                <?=$basketProduct['quantity']?>
            </td>
            <td>
                <?
                if(!$basketProduct["custom_price"] || $basketProduct["price"])
                {
                    ?>
                    <?=$basketProduct['DEALER_PRICE_FORMAT']?> <span class="rub">₽</span>
                    <?
                }
                else
                {
                    ?>
                    ПОДАРОК
                    <?
                }?>
            </td>
        </tr>

    <? }
}

// Товары корзины (в наличии, но не хватает)
$n = 0;
foreach( $arResult['DEALER']['basketProducts'] as $basketProduct ){
    if(
        $basketProduct['NAL'] == 'Y'
        &&
        $basketProduct['DEALER_QUANTITY'] < $basketProduct['quantity']
    ){
        $n++;

        if( $arParams['IS_MOB_APP'] == 'Y' ){
            $basketProduct['el']['DETAIL_PAGE_URL'] = project\catalog::toMobileAppUrl($basketProduct['el']['DETAIL_PAGE_URL']);
        }
        ?>

        <?php if( $n == 1 ){ ?>
            <tr>
                <td>
                    <p class="cart-form__order-text" style="color:#ec5151"><strong>Внимание!</strong><br>По некоторым позициям корзины заказа у дилера недостаточно либо нет товаров в наличии.</p>
                </td>
            </tr>
        <?php } ?>

        <tr>
            <td>
                <div>
                    <? if( intval($basketProduct['el']['DETAIL_PICTURE']) > 0 ){ ?>
                        <a href="<?=$basketProduct['el']['DETAIL_PAGE_URL']?>" class="cart-form__inCart-image" target="_blank">
                            <img src="<?=tools\funcs::rIMGG($basketProduct['el']['DETAIL_PICTURE'], 4, 50, 46)?>">
                        </a>
                    <? } else { ?>
                        <div style="display: block; width:50px; height:46px;"></div>
                    <? } ?>
                    <a href="<?=$basketProduct['el']['DETAIL_PAGE_URL']?>" class="cart-form__inCart-title"><?=$basketProduct['el']['NAME']?></a>
                </div>
            </td>
            <td>
                <strong><?=$basketProduct['DEALER_QUANTITY']?> (недост.)</strong>
            </td>
            <td>
                <?
                if(!$basketProduct["custom_price"] || $basketProduct["price"])
                {
                    ?>
                    <?=$basketProduct['DEALER_PRICE_FORMAT']?> <span class="rub">₽</span>
                    <?
                }
                else
                {
                    ?>
                    ПОДАРОК
                    <?
                }?>
            </td>
        </tr>

    <? }
}

// Товары корзины (нет в наличии)
foreach( $arResult['DEALER']['basketProducts'] as $basketProduct ){
    if( $basketProduct['NAL'] != 'Y' ){

        if( $arParams['IS_MOB_APP'] == 'Y' ){
            $basketProduct['el']['DETAIL_PAGE_URL'] = project\catalog::toMobileAppUrl($basketProduct['el']['DETAIL_PAGE_URL']);
        }

        $n++; ?>

        <?php if( $n == 1 ){ ?>
            <tr>
                <td>
                    <p class="cart-form__order-text" style="color:#ec5151"><strong>Внимание!</strong><br>По некоторым позициям корзины заказа у дилера недостаточно либо нет товаров в наличии.</p>
                </td>
            </tr>
        <?php } ?>

        <tr>
            <td>
                <div>
                    <? if( intval($basketProduct['el']['DETAIL_PICTURE']) > 0 ){ ?>
                        <a href="<?=$basketProduct['el']['DETAIL_PAGE_URL']?>" class="cart-form__inCart-image" target="_blank">
                            <img src="<?=tools\funcs::rIMGG($basketProduct['el']['DETAIL_PICTURE'], 4, 50, 46)?>">
                        </a>
                    <? } else { ?>
                        <div style="display: block; width:50px; height:46px;"></div>
                    <? } ?>
                    <a href="<?=$basketProduct['el']['DETAIL_PAGE_URL']?>" class="cart-form__inCart-title"><?=$basketProduct['el']['NAME']?></a>
                </div>
            </td>
            <td>
                <strong>нет</strong>
            </td>
            <td>
                <?
                if(!$basketProduct["custom_price"] || $basketProduct["price"])
                {
                    ?>
                    <? echo '-'; ?>
                    <?
                }
                else
                {
                    ?>
                    ПОДАРОК
                    <?
                }?>
            </td>
        </tr>

    <? }
} ?>

                            </tbody>
                        </table>

                        <a class="cart-form__inCart-link" onclick="$(this).parents('form').attr('action', '<?=$arParams['IS_MOB_APP']=='Y'?'/knauf_app':''?>/basket/').attr('onsubmit', 'return true').submit();" style="cursor: pointer;">Вернуться в&nbsp;корзину</a>

                    </div>

                    <div class="cart-form__inCart-totalWrapper">
                        <div>
                            <span>Стоимость товаров</span>
                            <span><?=$arResult['DEALER']['BASKET_SUM_FORMAT']?>&nbsp;<span class="rub">₽</span></span>
                        </div>

                        <? if( $arResult['POST']['delivType'] == 'delivery' ){ ?>

                            <div>
                                <span>Доставка</span>
                                <? if( !$arResult['DEALER']['DELIVERY_PRICE_FORMAT'] ){ ?>
                                    <span><?=$arResult['DEALER']['DELIVERY_PRICE']?></span>
                                <? } else { ?>
                                    <span><?=$arResult['DEALER']['DELIVERY_PRICE_FORMAT']?> <span class="rub">₽</span></span>
                                <? } ?>
                            </div>

                            <div>
                                <span>Итого</span>
                                <span><?=$arResult['DEALER']['TOTAL_SUM_FORMAT']?> <span class="rub">₽</span></span>
                            </div>

                        <? } ?>

                    </div>

                </section>

                <? if( count($arResult['under_order_goods']) > 0 ){ ?>

                    <section class="cart-form__block">

                        <div class="cart-form__inCart">

                            <table class="cart-form__inCart-table">
                                <tbody>

                                    <tr>
                                        <td colspan="2" style="padding:15px;">
                                            <h4 class="tab__title" style="margin-bottom:0;">Товары под заказ:</h4>
                                        </td>
                                    </tr>

                                    <? $cnt = 0;
                                    foreach( $arResult['under_order_goods'] as $id => $el ){

                                        if( $arParams['IS_MOB_APP'] == 'Y' ){
                                            $el['DETAIL_PAGE_URL'] = project\catalog::toMobileAppUrl($el['DETAIL_PAGE_URL']);
                                        }

                                        $cnt++; ?>

                                        <tr>
                                            <td>
                                                <div>
                                                    <? if( intval($el['DETAIL_PICTURE']) > 0 ){ ?>
                                                        <a href="<?=$el['DETAIL_PAGE_URL']?>" class="cart-form__inCart-image" target="_blank">
                                                            <img src="<?=tools\funcs::rIMGG($el['DETAIL_PICTURE'], 4, 50, 46)?>">
                                                        </a>
                                                    <? } else { ?>
                                                        <div style="display: block; width:50px; height:46px;"></div>
                                                    <? } ?>
                                                    <a href="<?=$el['DETAIL_PAGE_URL']?>" class="cart-form__inCart-title"><?=$el['NAME']?></a>
                                                </div>
                                            </td>
                                        </tr>

                                    <? } ?>

                                    <tr>
                                        <td>
                                            <p><span class="cart-form__deliveryColumn-text">Этих товаров на данный момент нет в наличии у официальных дилеров, но мы передадим информацию о вашем желании приобрести данные продукты и дилер уточнит информацию о возможности и сроках поставки при его подтверждении</span></p>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>

                        </div>
                    </section>

                <? } ?>

                <? if(
                    $arResult['POST']['delivType'] == 'delivery'
                    &&
                    !$arResult['DEALER']['DELIVERY_PRICE']
                ){ ?>

                    <section class="cart-form__block">
                        <div style="padding:15px">
                            <? if(
                                $arResult['DEALER']['UF_DELIV_INFO_TYPE'] == 'link'
                                &&
                                strlen($arResult['DEALER']['UF_DELIV_INFO_LINK']) > 0
                                &&
                                /////
                                0
                                /////
                            ){ ?>
                                <p class="cart-form__order-text"><strong>Внимание!</strong><br>C инфомацией об условиях и стоимости доставки дилера &laquo;<?=$arResult['DEALER']['UF_COMPANY_NAME']?>&raquo; Вы можете ознакомиться <a href="<?=$arResult['DEALER']['UF_DELIV_INFO_LINK']?>" target="_blank">здесь</a>.</p>
                            <? } else if(
                                $arResult['DEALER']['UF_DELIV_INFO_TYPE'] == 'file'
                                &&
                                intval($arResult['DEALER']['UF_DELIV_INFO_FILE']) > 0
                                &&
                                0
                            ){ ?>
                                <p class="cart-form__order-text"><strong>Внимание!</strong><br>C инфомацией по условиям и стоимости доставки дилера &laquo;<?=$arResult['DEALER']['UF_COMPANY_NAME']?>&raquo; Вы можете ознакомиться, скачав данный <a href="<?=\CFile::GetPath($arResult['DEALER']['UF_DELIV_INFO_FILE'])?>" target="_blank" download style="text-decoration: underline;">файл</a>.</p>
                            <? } else if(
                                is_array($arResult['DEALER']['UF_PHONES'])
                                &&
                                count($arResult['DEALER']['UF_PHONES']) > 0
                            ){ ?>
                                <p class="cart-form__order-text"><strong>Внимание!</strong><br>Инфомацию по условиям и стоимости доставки дилера &laquo;<?=$arResult['DEALER']['UF_COMPANY_NAME']?>&raquo; Вы можете уточнить по телефонам:<br><?=implode('<br>', $arResult['DEALER']['UF_PHONES'])?></p>
                            <? } else { ?>
                                <p class="cart-form__order-text"><strong>Внимание!</strong><br>Инфомацию по условиям и стоимости доставки Вы можете уточнить непосредственно у <a href="<?=$arResult['DEALER_URL']?>#showtab-tab_23111_4" target="_blank">дилера</a>.</p>
                            <? } ?>
                        </div>
                    </section>

                <? } ?>

                <section class="cart-form__block" style="border-bottom: none;">
                    <div class="account-form__field  account-form__field--w100" style="padding: 25px 15px 0 15px;">
                        <div class="account-form__checkbox">
                            <input <? if( $arParams['form_data']['agree'] == 'Y' ){ echo 'checked'; } ?> type="checkbox" id="order_agree" name="agree" value="Y">
                            <label for="order_agree">Я принимаю условия <a href="<?=$arParams['IS_MOB_APP']=='Y'?'/knauf_app':''?>/user_agreement/" target="_blank">Пользовательского соглашения</a></label>
                        </div>
                    </div>
                </section>

                <section class="cart-form__block" style="border-bottom: none;">
                    <div class="account-form__field  account-form__field--w100" style="padding: 25px 15px 0 15px;">
                        <div class="account-form__checkbox">
                            <input <? if( $arParams['form_data']['policy'] == 'Y' ){ echo 'checked'; } ?> type="checkbox" id="order_policy" name="policy" value="Y">
                            <label for="order_policy">Я согласен на обработку моей персональной информации на условиях, определенных <a href="https://www.knauf.ru/about/confidentiality/" target="_blank">Политикой конфиденциальности</a></label>
                        </div>
                    </div>
                </section>

                <section class="cart-form__block" style="border-bottom: none;">
                    <div class="account-form__field  account-form__field--w100" style="padding: 25px 15px 0 15px;">
                        <div class="account-form__checkbox">
                            <input <? if( $arParams['form_data']['subscribe'] == 'Y' ){ echo 'checked'; } ?> type="checkbox" id="order_subscribe" name="subscribe" value="Y">
                            <label for="order_subscribe">Я согласен(а) на получение маркетинговой информации по телекоммуникационным каналам (e-mail, sms, телефон)</label>
                        </div>
                    </div>
                </section>

                <?php if(
                        is_array($arResult['DEALER_WARNINGS'])
                        &&
                        count($arResult['DEALER_WARNINGS']) > 0
                ){ ?>
                    <section class="cart-form__block">
                        <div style="padding:15px">
                            <? foreach( $arResult['DEALER_WARNINGS'] as $warning ){ ?>
                                <p class="cart-form__order-text" style="color: #c6037a!important;"><?=$warning?></p>
                            <? } ?>
                        </div>
                    </section>
                <?php } ?>

                <section class="cart-form__block before_error___p">

                    <div class="cart-form__orderWrapper">

                        <a style="cursor: pointer;" class="cart-form__order-button orderButton to___process <? if( !$arResult['isFullOffer'] ){ echo 'notFull'; } ?>">Оформить</a>

                        <? if( $USER->IsAuthorized() ){ ?>

                            <p class="cart-form__order-text">Вы можете отследить статус заказа и оставить отзыв в <a href="<?=$arParams['IS_MOB_APP']=='Y'?'/knauf_app':''?>/personal/">Личном кабинете</a></p>

                        <? } ?>

                    </div>

                </section>

            <? } ?>

            <? foreach ( $arResult['BASKET_OFFERS'] as $j => $basket_offer ){ ?>
                <input type="hidden" name="BASKET_OFFERS[<?=$j?>][ID]" value="<?=$basket_offer['ID']?>">
                <input type="hidden" name="BASKET_OFFERS[<?=$j?>][QUANTITY]" value="<?=$basket_offer['QUANTITY']?>">
                <input type="hidden" name="BASKET_OFFERS[<?=$j?>][REQUIRED_QUANTITY]" value="<?=$basket_offer['REQUIRED_QUANTITY']?>">
            <? } ?>

            <? foreach ( $arResult['POST'] as $key => $value ){
                 if( !in_array( $key, [
                        //'podyezd', 'floor',
                        //'kvartira', 'lift',
                        'comment', 'BASKET_OFFERS'
                 ] ) ){ ?>
                    <input type="hidden" name="<?=$key?>" value="<?=is_array($value)?implode('|', $value):$value?>">
                <? }
            } ?>

            <input type="hidden" name="delivery_price" value="<?=$arResult['DEALER']['DELIVERY_PRICE']?>">

        </div>

    </form>

</article>



<script src="<?=SITE_TEMPLATE_PATH?>/js/sliders.js?v=<?=time()?>"></script>
<? if( $arParams['IS_AJAX'] == 'Y' ){ ?>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/aoptima/datepicker.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/default_scripts.js"></script>
<? } ?>