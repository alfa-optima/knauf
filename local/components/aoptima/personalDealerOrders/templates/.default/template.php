<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<form class="dealerActiveOrdersForm" onsubmit="return false;">
    <table class="account-form__table  account-form__table--requests  account-form__table--cartOrders">
        <caption class="account-form__table-title">Заказы из корзины</caption>

        <?
        if( count($arResult['ORDERS']['ACTIVE']) > 0 )
        {
            ?>
            <tbody>
                <tr class="first_tr">
                    <th sort_field="ID" class="sortTD to___process <?=$arResult['sort_field']=='ID'?('tdSort'.$arResult['sort_order']):'';?>" sort_order="<?=$arResult['sort_order']=='ASC'?'DESC':'ASC'?>">№ заказа</th>
                    <th sort_field="DATE_INSERT" class="sortTD to___process <?=$arResult['sort_field']=='DATE_INSERT'?('tdSort'.$arResult['sort_order']):'';?>" sort_order="<?=$arResult['sort_order']=='ASC'?'DESC':'ASC'?>">Дата</th>
                    <th class="trNotSort">Доставка</th>
                    <th class="trNotSort">Оплата</th>
                    <th class="trNotSort">Дата доставки</th>
                    <th class="trNotSort">Статус</th>
                </tr>

                <input type="hidden" name="id" value="" id="save_order_id">

                <?
                foreach( $arResult['ORDERS']['ACTIVE'] as $order_id => $order )
                {
                    // orderListItem
                    $APPLICATION->IncludeComponent(
                        "aoptima:dealerOrderListItem", "",
                        [
                            'ORDER' => $order,
                            'IS_MOB_APP' => $arParams['IS_MOB_APP']
                        ]
                    );
                } ?>
            </tbody>
            <?
        }
        else
        {
            ?>
            <tbody>
            <tr><td>
                    <p class="no___count" style="padding:30px; font-size: 16px; text-align:center;">Активных заказов нет</p>
                </td></tr>
            </tbody>
            <?
        }?>
    </table>

    <? if( count($arResult['ORDERS']['COMPLETED']) > 0 && $arParams["IS_SUB_DILER"] != "Y" ){ ?>

        <div class="dealerAccount-form__completedOrders">

            <a href="<?=PREFIX?>/personal/orders_archive/" style="color:#484846; text-decoration: underline;"><h5 class="tab__title" style="text-align: right; margin-right:30px; margin-top:24px;">Архив заказов</h5></a>

        </div>

    <? } ?>
</form>

<? if( $arParams['IS_AJAX'] == 'Y' ){
    echo '<script src="'.SITE_TEMPLATE_PATH.'/js/default_scripts.js"></script>';
} ?>