<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('sale');

use Bitrix\Main,
    Bitrix\Main\Localization\Loc as Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Main\Application,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem,
    Bitrix\Sale,
    Bitrix\Sale\Basket,
    Bitrix\Sale\Order,
    Bitrix\Sale\DiscountCouponsManager,
    Bitrix\Main\Context,
    Bitrix\Sale\Internals;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


$arResult['MAX_CNT'] = 5;

if( intval($arParams['DEALER_ID']) > 0 ){
    $arResult['DEALER_ID'] = $arParams['DEALER_ID'];
} else {
    $arResult['DEALER_ID'] = $USER->GetID();
}
$arResult['DEALER'] = tools\user::info($arResult['DEALER_ID']);

$arResult['ORDERS'] = array(
    'ACTIVE' => array(),
    'COMPLETED' => array()
);

// ID акт. заказов дилера
$arResult['ACTIVE_ORDER_IDS'] = project\order::getDealerOrders(
    $arResult['DEALER_ID'], false, array('A', 'B', 'D')
);

if( count($arResult['ACTIVE_ORDER_IDS']) > 0 ){

    $arResult['ORDER_STATUSES'] = project\order_status::getList();

    // Список вариантов доставки
    $arResult['DS_LIST'] = project\ds::getList();

    // Получаем заказы по ID
    $filter_array = array( 'ID' => $arResult['ACTIVE_ORDER_IDS'] );

    // Сортировка заказов
    $arResult['sort'] = array( 'DATE_INSERT' => 'DESC' );
    if( $_SESSION['dealerOrdersSot'] ){
        $arResult['sort'] = $_SESSION['dealerOrdersSot'];
    }
    $arResult['sort_field'] = array_keys($arResult['sort'])[0];
    $arResult['sort_order'] = $arResult['sort'][$arResult['sort_field']];

    $orders = \Bitrix\Sale\OrderTable::getList(array(
        'filter' => $filter_array,
        'select' => array( '*' ),
        'order' => $arResult['sort'],
        //'limit' => $arResult['MAX_CNT']
    ));

    while ( $order = $orders->fetch() ){

        $obOrder = \Bitrix\Sale\Order::load($order['ID']);
        $propValues = project\order::getOrderPropValues($obOrder);

        $paymentCollection = $obOrder->getPaymentCollection();
        foreach ( $paymentCollection as $payment ){
            $ps_id = $payment->getPaymentSystemId();
            $order['ps'] = array(
                'ID' => $ps_id,
                'NAME' => $payment->getPaymentSystemName()
            );
        }

        $order['DATE_INSERT'] = ConvertDateTime($obOrder->getDateInsert(), "DD.MM.YYYY", "ru");

        // Стоимость доставки
        $order['DELIVERY_SUM'] = $obOrder->getDeliveryPrice();
        $order['DELIVERY_SUM_FORMAT'] = number_format($order['DELIVERY_SUM'], project\catalog::ROUND, ",", " ");

        $order['BASKET_CNT'] = 0;   $order['BASKET_SUM'] = 0;
        $order['TOTAL_SUM'] = 0;

        // Инфо по корзине
        $basket = Sale\Order::load($order['ID'])->getBasket();
        foreach( $basket as $key => $basketItem ){

            $el = null;
            $product = null;
            $el = tools\el::info( $basketItem->getProductId() );
            if( intval($el['ID']) > 0 ){
                $product = tools\el::info( $el['PROPERTY_CML2_LINK_VALUE'] );
                $product_img = intval($product['DETAIL_PICTURE'])>0?(tools\funcs::rIMGG($product['DETAIL_PICTURE'], 4, 40, 40)):false;
            } else {
                $product_id = null;
                $basketPropertyCollection = $basketItem->getPropertyCollection();
                $values = $basketPropertyCollection->getPropertyValues();
                if( intval($values['product_id']['VALUE']) > 0 ){
                    $product_id = $values['product_id']['VALUE'];
                    $product = tools\el::info( $product_id );
                }
            }

            $bItemName = $basketItem->getField('NAME');
            preg_match("/(.+) \(D=[^\)]+\)/", $bItemName, $matches, PREG_OFFSET_CAPTURE);
            if( isset($matches[1][0]) ){   $bItemName = $matches[1][0];   }

            $order['arBasket'][$key] = array(
                'ID'                => $basketItem->getId(),
                'basketItemName'    => $bItemName,
                'el'                => $el,
                'product'           => $product,
                'product_img'       => $product_img,
                'QUANTITY'          => round($basketItem->getQuantity(), 0),
                'price'               => $basketItem->getPrice(),
                'priceFormat'         => number_format($basketItem->getPrice(), project\catalog::ROUND, ",", " "),
                'sum'               => $basketItem->getQuantity() * $basketItem->getPrice(),
                'sumFormat'         => number_format($basketItem->getQuantity() * $basketItem->getPrice(), project\catalog::ROUND, ",", " "),
            );

            $order['BASKET_SUM'] += $basketItem->getQuantity() * $basketItem->getPrice();
            $order['BASKET_CNT'] += $basketItem->getQuantity();
        }

        $order['TOTAL_SUM'] = $order['BASKET_SUM'] + $order['DELIVERY_SUM'];

        $order['BASKET_SUM_FORMAT'] = number_format($order['BASKET_SUM'], project\catalog::ROUND, ",", " ");
        $order['BASKET_CNT_STR'] = $order['BASKET_CNT'].' '.tools\funcs::pfCnt($order['BASKET_CNT'], "товар", "товара", "товаров");

        $order['TOTAL_SUM_FORMAT'] = number_format($order['TOTAL_SUM'], project\catalog::ROUND, ",", " ");

        $shipmentCollection = $obOrder->getShipmentCollection();
        foreach ( $shipmentCollection as $shipment ){
            $ds_id = $shipment->getDeliveryId();
            $order['ds'] = array(
                'ID' => $ds_id,
                'NAME' => $shipment->getDeliveryNAME()
            );
        }

        $order['delivType'] = $arResult['DS_LIST'][$order['ds']['ID']]['delivType'];
        $order['DELIVERY_ADDRESS'] = project\order::getDeliveryAddress( $propValues );

        $order['PVZ'] = project\order::getPropValue( $propValues, 'PVZ' );

        $order['LIFT'] = project\order::getPropValue( $propValues, 'LIFT' );
        $order['DELIV_DATE'] = project\order::getPropValue( $propValues, 'DELIV_DATE' );
        $order['DELIV_TIME'] = project\order::getPropValue( $propValues, 'DELIV_TIME' );
        $order['BUYER_NAME'] = project\order::getPropValue( $propValues, 'NAME' );
        $order['BUYER_LAST_NAME'] = project\order::getPropValue( $propValues, 'LAST_NAME' );
        $order['BUYER_PHONE'] = project\order::getPropValue( $propValues, 'PHONE' );
        $order['BUYER_EMAIL'] = project\order::getPropValue( $propValues, 'EMAIL' );
        $order['BUYER_LIFT'] = project\order::getPropValue( $propValues, 'LIFT' );
        $order['BUYER_COMMENT'] = project\order::getPropValue( $propValues, 'COMMENT' );
        $order['POD_ZAKAZ_GOODS'] = project\order::getPropValue( $propValues, 'POD_ZAKAZ' );
        $order['DEALER_COMMENT'] = project\order::getPropValue( $propValues, 'DEALER_COMMENT' );
        $order['FAST_ORDER'] = project\order::getPropValue( $propValues, 'FAST_ORDER' );
        if($order['FAST_ORDER'])
        {
            $order['DELIVERY_ADDRESS'] = project\order::getPropValue( $propValues, 'ADDRESS' );
        }

        $order['ORDER_DEALER_STATUSES'] = $arResult['ORDER_STATUSES'];
        $order_status = new project\order_status();
        $order['ORDER_DEALER_STATUS'] = $order_status->get( $obOrder, $propValues );

        $arResult['ORDERS']['ACTIVE'][$order['ID']] = $order;
    }

}


// ID заверш. заказов дилера
$arResult['ORDERS']['COMPLETED'] = project\order::getDealerOrders(
    $arResult['DEALER_ID'], 1, ['Z', 'X']
);















$this->IncludeComponentTemplate();