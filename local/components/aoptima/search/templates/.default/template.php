<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if( $arParams['IS_LOAD'] != 'Y' ){ ?>

    <form class="searchForm" onsubmit="return false" style="margin-bottom: 0;">

        <article class="block  block--top">
            <div class="block__wrapper">
                <h2 class="block__title  block__title--search">Поиск по каталогу</h2>
                <span class="block__text">Всего товаров: <span class="block__text--numberFound"><?=$arResult['totalCNT']?></span></span>
            </div>
        </article>

        <section class="searchBlock <? if( strlen($arParams['closeSidebar']) > 0 ){  if( $arParams['closeSidebar'] == 'Y' ){ echo 'hideMenu'; } } else if( strlen($arResult['q']) > 0 ){ echo 'hideMenu'; } ?> <? if( $arResult['catalogView'] == 'list' ){ echo 'listView'; } ?>">
            <div class="searchPanel">

                <h3 class="searchPanel__title">Найти продукты
                    <span class="searchPanel__toggle"><span class="sr-only">Скрыть/показать панель поиска</span>
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/arrow-left.png" alt="стрелка влево" class="searchPanel__toggle-image">
                    </span>
                </h3>

                <div class="searchPanel__inner">

                    <div class="searchPanel__keywords">

                        <input id="keywords" name="q" placeholder="Ключевые слова" type="text" value="<?=$arResult['q']?>" autocomplete="off">

                        <!--
                        <input id="article" name="article" value="Артикул" type="text" onfocus="this.value=''">
                        -->

                    </div>

                    <ul class="searchPanel-nav">

						<li class="searchPanel-nav__item <? if( $arResult['CURRENT_SECTION_ID'] == $section['ID'] ){ ?>searchPanel-nav__item--active<? } ?>">
                                <a style="cursor: pointer;" class="searchPanel-nav__item-link sectionButton to___process" section_id="<?=$section['ID']?>">Все продукты</span></a>
                        </li>

                        <? foreach ( $arResult['CATALOG_SECTIONS'] as $section ){ ?>

                            <li class="searchPanel-nav__item <? if( $arResult['CURRENT_SECTION_ID'] == $section['ID'] ){ ?>searchPanel-nav__item--active<? } ?>">
                                <a style="cursor: pointer;" class="searchPanel-nav__item-link sectionButton to___process" section_id="<?=$section['ID']?>"><?=$section['NAME']?></span></a>
                            </li>

                        <? } ?>

                    </ul>

                    <input type="hidden" name="section_id" value="<?=$arResult['CURRENT_SECTION_ID']?>">

                </div>
            </div>

            <div class="searchBlock__wrapper">

                <div class="view-switcher">
                    <span class="view-switcher__text">Режим просмотра</span>
                    <div class="view-switcher__buttons">
                        <a href="javascript:;" class="view-switcher__button  view-switcher__button--grid"><span class="sr-only">Сетка</span></a>
                        <a href="javascript:;" class="view-switcher__button  view-switcher__button--list"><span class="sr-only">Список</span></a>
                    </div>
                </div>

                <div class="searchBlock__content">

                    <h3 class="goods__title">Результаты поиска:</h3>

                    <? if( count($arResult['PRODUCTS']) > 0 ){
                        $GLOBALS['searchProducts']['ID'] = array_slice($arResult['PRODUCTS'], 0, $arResult['MAX_CNT']+1);
                    } else {
                        $GLOBALS['searchProducts']['ID'] = array(0);
                    }

                    if( is_array($_POST['stop_ids']) ){
                        $GLOBALS['searchProducts']['!ID'] = $_POST['stop_ids'];
                    }

                    // фильтр по СД
                    $GLOBALS['searchProducts'] = project\catalog::AddStopSDsToFilter($GLOBALS['searchProducts']);

                    $sort1 = 'PROPERTY_HAS_OFFERS';   $order1 = 'DESC';
                    $sort2 = 'SORT';   $order2 = 'ASC';

                    //////////////////////////////////
                    // Все дилеры
                    $dealer = new project\dealer();
                    $allDealers = $dealer->allList();
                    //////////////////////////////////

                    $APPLICATION->IncludeComponent("bitrix:catalog.section", "searchProducts",
                        Array(
                            'IS_MOB_APP' => IS_ESHOP?"Y":"N",
                            //////////////////////////////
                            'ALL_DEALERS' => $allDealers,
                            //////////////////////////////
                            "LOC" => $_SESSION['LOC'],
                            'IS_DEALER' => $arResult['OB_USER']->isDealer()?'Y':'N',
                            "IBLOCK_TYPE" => "content",
                            "IBLOCK_ID" => $arResult['CATALOG_IBLOCK_ID'],
                            "SECTION_USER_FIELDS" => array(),
                            "ELEMENT_SORT_FIELD" => $sort1,
                            "ELEMENT_SORT_ORDER" => $order1,
                            "ELEMENT_SORT_FIELD2" => $sort2,
                            "ELEMENT_SORT_ORDER2" => $order2,
                            "FILTER_NAME" => "searchProducts",
                            "HIDE_NOT_AVAILABLE" => "N",
                            "PAGE_ELEMENT_COUNT" => $arResult['MAX_CNT']+1,
                            "LINE_ELEMENT_COUNT" => "3",
                            "PROPERTY_CODE" => array("ARTICLE","MOD","MODS_ARTICLE","ED","VIEW","EDGE_VIEW","VIDEO","RESIST_WATER","DRYING_TIME","FLAMMABILITY_GROUP","BASE_MATERIAL","RESIST_COLD","FLOOR_COATING","TARGET","MIXTURE_BASE","PERFORATION","ROOM","POP","PREIM","PRIM","BUY_WITH","SEE_WITH","PROPERTIES","APPL_METHOD","TECH","TYPE","TYPE_OBJECT","TYPE_OF_APPLICATION","TYPE_PROFILE","TYPE_COATING","LAYER_THICKNESS","FASOVKA","TILE","COLOR","WEIGHT","HEIGHT","LENGTH","DEPTH","WIDTH","CROSS","CALC_WEIGHT","MAX_GABARIT","HAS_OFFERS",""),
                            "OFFERS_LIMIT" => "0", "OFFERS_PROPERTY_CODE" => ['LOCATION'], "TEMPLATE_THEME" => "", "PRODUCT_SUBSCRIPTION" => "N", "SHOW_DISCOUNT_PERCENT" => "N", "SHOW_OLD_PRICE" => "N", "MESS_BTN_BUY" => "Купить", "MESS_BTN_ADD_TO_BASKET" => "В корзину", "MESS_BTN_SUBSCRIBE" => "Подписаться", "MESS_BTN_DETAIL" => "Подробнее", "MESS_NOT_AVAILABLE" => "Нет в наличии", "SECTION_URL" => "", "DETAIL_URL" => "", "SECTION_ID_VARIABLE" => "SECTION_ID", "AJAX_MODE" => "N", "AJAX_OPTION_JUMP" => "N", "AJAX_OPTION_STYLE" => "Y", "AJAX_OPTION_HISTORY" => "N", "CACHE_TYPE" => "A", "CACHE_TIME" => "600", "CACHE_GROUPS" => "Y", "SET_META_KEYWORDS" => "N", "META_KEYWORDS" => "", "SET_META_DESCRIPTION" => "N", "META_DESCRIPTION" => "", "BROWSER_TITLE" => "-", "ADD_SECTIONS_CHAIN" => "N", "DISPLAY_COMPARE" => "N", "SET_TITLE" => "N", "SET_STATUS_404" => "N", "CACHE_FILTER" => "Y", "PRICE_CODE" => array('BASE'), "USE_PRICE_COUNT" => "N", "SHOW_PRICE_COUNT" => "1", "PRICE_VAT_INCLUDE" => "Y", "CONVERT_CURRENCY" => "N", "BASKET_URL" => "/personal/basket.php", "ACTION_VARIABLE" => "action", "PRODUCT_ID_VARIABLE" => "id", "USE_PRODUCT_QUANTITY" => "N", "ADD_PROPERTIES_TO_BASKET" => "Y", "PRODUCT_PROPS_VARIABLE" => "prop", "PARTIAL_PRODUCT_PROPERTIES" => "N", "PRODUCT_PROPERTIES" => "", "PAGER_TEMPLATE" => "", "DISPLAY_TOP_PAGER" => "N", "DISPLAY_BOTTOM_PAGER" => "Y", "PAGER_TITLE" => "Товары", "PAGER_SHOW_ALWAYS" => "N", "PAGER_DESC_NUMBERING" => "N", "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", "PAGER_SHOW_ALL" => "Y", "ADD_PICT_PROP" => "-", "LABEL_PROP" => "-", 'INCLUDE_SUBSECTIONS' => "Y", 'SHOW_ALL_WO_SECTION' => "Y"
                        )
                    ); ?>

                </div>

            </div>
        </section>


    </form>


<? // Подгрузка
} else if( $arParams['IS_LOAD'] == 'Y' ){

    
    $GLOBALS['searchProducts']['ID'] = count($arResult['PRODUCTS'])>0?$arResult['PRODUCTS']:array(0);
    if( is_array($_POST['stop_ids']) ){
        $GLOBALS['searchProducts']['!ID'] = $_POST['stop_ids'];
    }

    $sort1 = 'PROPERTY_HAS_OFFERS';   $order1 = 'DESC';
    $sort2 = 'SORT';   $order2 = 'ASC';

    //echo "<pre>"; print_r( $GLOBALS['searchProducts'] ); echo "</pre>";

    //////////////////////////////////
    // Все дилеры
    $dealer = new project\dealer();
    $allDealers = $dealer->allList();
    //////////////////////////////////

    $APPLICATION->IncludeComponent("bitrix:catalog.section", "searchProducts",
        Array(
            'IS_MOB_APP' => IS_ESHOP?"Y":"N",
            //////////////////////////////
            'ALL_DEALERS' => $allDealers,
            //////////////////////////////
            "LOC" => $_SESSION['LOC'],
            'IS_LOAD' => 'Y',
            'IS_DEALER' => $arResult['OB_USER']->isDealer()?'Y':'N',
            "IBLOCK_TYPE" => "content",
            "IBLOCK_ID" => $arResult['CATALOG_IBLOCK_ID'],
            "SECTION_USER_FIELDS" => array(),
            "ELEMENT_SORT_FIELD" => $sort1,
            "ELEMENT_SORT_ORDER" => $order1,
            "ELEMENT_SORT_FIELD2" => $sort2,
            "ELEMENT_SORT_ORDER2" => $order2,
            "FILTER_NAME" => "searchProducts",
            "HIDE_NOT_AVAILABLE" => "N",
            "PAGE_ELEMENT_COUNT" => $arResult['MAX_CNT']+1,
            "LINE_ELEMENT_COUNT" => "3",
            "PROPERTY_CODE" => array("ARTICLE","MOD","MODS_ARTICLE","ED","VIEW","EDGE_VIEW","VIDEO","RESIST_WATER","DRYING_TIME","FLAMMABILITY_GROUP","BASE_MATERIAL","RESIST_COLD","FLOOR_COATING","TARGET","MIXTURE_BASE","PERFORATION","ROOM","POP","PREIM","PRIM","BUY_WITH","SEE_WITH","PROPERTIES","APPL_METHOD","TECH","TYPE","TYPE_OBJECT","TYPE_OF_APPLICATION","TYPE_PROFILE","TYPE_COATING","LAYER_THICKNESS","FASOVKA","TILE","COLOR","WEIGHT","HEIGHT","LENGTH","DEPTH","WIDTH","CROSS","CALC_WEIGHT","MAX_GABARIT","HAS_OFFERS",""),
            "OFFERS_LIMIT" => "0", "OFFERS_PROPERTY_CODE" => ['LOCATION'], "TEMPLATE_THEME" => "", "PRODUCT_SUBSCRIPTION" => "N", "SHOW_DISCOUNT_PERCENT" => "N", "SHOW_OLD_PRICE" => "N", "MESS_BTN_BUY" => "Купить", "MESS_BTN_ADD_TO_BASKET" => "В корзину", "MESS_BTN_SUBSCRIBE" => "Подписаться", "MESS_BTN_DETAIL" => "Подробнее", "MESS_NOT_AVAILABLE" => "Нет в наличии", "SECTION_URL" => "", "DETAIL_URL" => "", "SECTION_ID_VARIABLE" => "SECTION_ID", "AJAX_MODE" => "N", "AJAX_OPTION_JUMP" => "N", "AJAX_OPTION_STYLE" => "Y", "AJAX_OPTION_HISTORY" => "N", "CACHE_TYPE" => "A", "CACHE_TIME" => "600", "CACHE_GROUPS" => "Y", "SET_META_KEYWORDS" => "N", "META_KEYWORDS" => "", "SET_META_DESCRIPTION" => "N", "META_DESCRIPTION" => "", "BROWSER_TITLE" => "-", "ADD_SECTIONS_CHAIN" => "N", "DISPLAY_COMPARE" => "N", "SET_TITLE" => "N", "SET_STATUS_404" => "N", "CACHE_FILTER" => "Y", "PRICE_CODE" => array('BASE'), "USE_PRICE_COUNT" => "N", "SHOW_PRICE_COUNT" => "1", "PRICE_VAT_INCLUDE" => "Y", "CONVERT_CURRENCY" => "N", "BASKET_URL" => "/personal/basket.php", "ACTION_VARIABLE" => "action", "PRODUCT_ID_VARIABLE" => "id", "USE_PRODUCT_QUANTITY" => "N", "ADD_PROPERTIES_TO_BASKET" => "Y", "PRODUCT_PROPS_VARIABLE" => "prop", "PARTIAL_PRODUCT_PROPERTIES" => "N", "PRODUCT_PROPERTIES" => "", "PAGER_TEMPLATE" => "", "DISPLAY_TOP_PAGER" => "N", "DISPLAY_BOTTOM_PAGER" => "Y", "PAGER_TITLE" => "Товары", "PAGER_SHOW_ALWAYS" => "N", "PAGER_DESC_NUMBERING" => "N", "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", "PAGER_SHOW_ALL" => "Y", "ADD_PICT_PROP" => "-", "LABEL_PROP" => "-", 'INCLUDE_SUBSECTIONS' => "Y", 'SHOW_ALL_WO_SECTION' => "Y"
        )
    );


} ?>
