<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('iblock');

$APPLICATION->SetPageProperty("title", 'Поиск по каталогу');

$arResult['CATALOG_IBLOCK_ID'] = project\catalog::catIblockID();
$arResult['OB_USER'] = new project\user();

if( $arParams['IS_AJAX'] != 'Y' && $arParams['IS_LOAD'] != 'Y' ){
    unset($_SESSION['SEARCH_PRODUCTS']);
}

$arResult['catalogView'] = $_SESSION['catalogView']?$_SESSION['catalogView']:'grid';


$arResult['PRODUCTS'] = array();
$arResult['MAX_CNT'] = 24;


// Разделы каталога 1 уровня
$arResult['CATALOG_SECTIONS'] = project\catalog::sections_1_level();

////////////////////////
$arResult['CURRENT_SECTION_ID'] = intval($arParams['form_data']['section_id'])>0?$arParams['form_data']['section_id']:false;


// Фильтр
$filter = Array(
    "IBLOCK_CODE" => project\catalog::CATALOG_IBLOCK_CODE,
    "ACTIVE" => "Y",
    "INCLUDE_SUBSECTIONS" => "Y"
);


// Фильтр по категории
if( intval($arResult['CURRENT_SECTION_ID']) > 0 ){
    $filter['SECTION_ID'] = $arResult['CURRENT_SECTION_ID'];
}


// Поиск
if( strlen($arParams['form_data']['q']) > 0 ){
    $arResult['q'] = $arParams['form_data']['q'];
} else if( strlen($_GET['q']) > 0 ){
    $arResult['q'] = $_GET['q'];
}

$arResult['q'] = strip_tags(trim($arResult['q']));
if( strlen($arResult['q']) > 0 ){
    // Поиск по элементам инфоблока
    $search_ids = project\search::iblockElementsByQ($arResult['q'], true);
    if( count($search_ids) > 0 ){
        $filter['ID'] = $search_ids;
    } else {
        $search_ids = project\search::iblockElementsByQ($arResult['q']);
        if( count($search_ids) > 0 ){
            $filter['ID'] = $search_ids;
        } else {
            $filter['ID'] = array(0);
        }
    }
}


// Поля
$fields = Array( "ID", "PROPERTY_HAS_OFFERS" );

// фильтр по СД
$filter = project\catalog::AddStopSDsToFilter($filter);

$hash_array = array(
    'filter_hash' => json_encode($filter),
    'fields_hash' => json_encode($fields),
    //'stop_ids_hash' => json_encode(is_array($_POST['stop_ids'])?$_POST['stop_ids']:array())
);
$final_hash = md5(json_encode($hash_array));

if( $_SESSION['SEARCH_PRODUCTS'][$final_hash] ){

    $arResult['PRODUCTS'] = $_SESSION['SEARCH_PRODUCTS'][$final_hash];

} else {

    // Запрос товаров каталога
    $sort1 = 'PROPERTY_HAS_OFFERS';   $order1 = 'DESC';
    $sort2 = 'SORT';   $order2 = 'ASC';
    $dbElements = \CIBlockElement::GetList(array($sort1 => $order1, $sort2 => $order2), $filter, false, false, $fields);
    while ($element = $dbElements->GetNext()) {
        $arResult['PRODUCTS'][] = $element['ID'];
    }

    $_SESSION['SEARCH_PRODUCTS'][$final_hash] = $arResult['PRODUCTS'];
}


$arResult['totalCNT'] = count($arResult['PRODUCTS']);


//$arResult['PRODUCTS'] = array_slice($arResult['PRODUCTS'], 0, $arResult['MAX_CNT']+1);



$this->IncludeComponentTemplate();