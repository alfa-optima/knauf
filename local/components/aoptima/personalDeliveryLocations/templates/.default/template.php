<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project; ?>

<div class="deliveryLocationsBlock">

    <div class="dealerAccount-form__wrapper  dealerAccount-form__wrapper--delivery">
        <h4 class="tab__title">Местоположение доставки</h4>
        <a href="javascript:;" onclick="$('.newDealerLocLink').fancybox({
        padding: 0,
        wrapCSS: 'fancybox-review'
    }) .trigger('click'); $('#modal-auto-update-region .error___p').html('');" class="dealerAccount-delivery__button">Добавить местоположение</a>
    </div>

    <section class="dealerAccount-delivery">

        <? foreach( $arResult['ITEMS'] as $key => $item ){ ?>

            <form class="deliverySettingsForm" onsubmit="return false;">

                <input type="hidden" name="item_id" value="<?=$item['ID']?>">

                <div class="dealerAccount-delivery__inner">

                    <div class="dealerAccount-delivery__header">
                        <div class="dealerAccount-delivery__header-wrapper">

                            <div class="dealerAccount-delivery__regionWrapper">
                                <span class="dealerAccount-delivery__regionName"><?=$item['LOC']['NAME_RU'].' ('.$item['LOC']['PARENT_NAME_RU'].')'?></span>
                            </div>

                            <span class="dealerAccount-delivery__link">Настроить доставку</span>

                            <span class="dealerAccount-delivery__link--del removeDeliveryLocationButton to___process" item_id="<?=$item['ID']?>">Удалить</span>

                        </div>
                    </div>

                    <div class="dealerAccount-delivery__index" style="display: none;">

                        <!-- Точка отсчёта --->
                        <div class="dealerAccount-delivery__indexBlock">
                            <div class="dealerAccount-delivery__indexBlock-wrapper">
                                <h5 class="dealerAccount-delivery__indexBlock-title">Адрес точки отсчёта</h5>
                                <div class="dealerAccount-form__row">
                                    <div class="dealerAccount-form__field  dealerAccount-form__field--w632">
                                        <input
                                            id="start_point_address_<?=$item['ID']?>"
                                            type="text"
                                            process="N"
                                            addressName=""
                                            name="start_point_address"
                                            onfocus="startPointAddressFocus($(this));"
                                            onkeyup="startPointAddressKeyUp($(this), 'start_point_address_<?=$item['ID']?>');"
                                            onfocusout="startPointAddressFocusOut($(this), '<?=$arParams['form_data']['coords']?>');"
                                            value="<?=$item['DELIV_SETTINGS']['start_point_address']?>"
                                        >
                                        <input type="hidden" name="start_point_coords" value="<?=$item['DELIV_SETTINGS']['start_point_coords']?>">
                                        <script>
                                        $(document).ready(function(){
                                            $('#start_point_address_<?=$item['ID']?>').autocomplete({
                                                source: '/ajax/searchAddress.php',
                                                minLength: 2,
                                                delay: 700,
                                                select: startPointAddressSelect,
                                                response: function(event, ui){

                                                }
                                            });
                                        })
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Настройка стоимости доставки --->
                        <div class="dealerAccount-delivery__indexBlock">
                            <div class="dealerAccount-delivery__indexBlock-wrapper">

                                <h5 class="dealerAccount-delivery__indexBlock-title">Стоимость доставки</h5>

<div class="dealerAccount-form__row">

    <!--- Метод расчета расстояния --->
    <div class="dealerAccount-form__field  dealerAccount-form__field--w100">

        <span>Метод расчета расстояния</span>

        <div class="account-form__checkboxes">

            <? $dType = false;
            $cnt = -1;
            foreach( project\delivery_price::$distanceTypes as $distanceType ){ $cnt++;
                if( $distanceType['CODE'] == $item['DELIV_SETTINGS']['distanceType_'.$item['ID']] ){
                    $dType = $distanceType['CODE'];
                } ?>

                <div class="account-form__checkbox  account-form__checkbox--w228">
                    <input class="distanceTypeInput" type="radio" <? if( $distanceType['CODE'] == $item['DELIV_SETTINGS']['distanceType_'.$item['ID']] ){ echo 'checked'; } ?> id="distanceType_<?=$item['ID']?>_<?=$distanceType['CODE']?>" name="distanceType_<?=$item['ID']?>" value="<?=$distanceType['CODE']?>">
                    <label for="distanceType_<?=$item['ID']?>_<?=$distanceType['CODE']?>"><?=$distanceType['NAME']?></label>
                </div>

            <? } ?>

        </div>

    </div>

    <!--- Метод расчета стоимости --->
    <div class="dealerAccount-form__field  dealerAccount-form__field--w100">
        <span>Метод расчета стоимости</span>
        <div class="account-form__checkboxes">

            <? $Type = false;
            $cnt = 1;
            foreach( project\delivery_price::$tariffTypes as $tariffType ){ $cnt++;
                if( $tariffType['CODE'] == $item['DELIV_SETTINGS']['tariffType_'.$item['ID']] ){
                    $tType = $tariffType['CODE'];
                } ?>

                <div class="account-form__checkbox  account-form__checkbox--w228">
                    <input class="tariffTypeInput" type="radio" <? if( $tariffType['CODE'] == $item['DELIV_SETTINGS']['tariffType_'.$item['ID']] ){ echo 'checked'; } ?> id="tariffType_<?=$item['ID']?>_<?=$tariffType['CODE']?>" name="tariffType_<?=$item['ID']?>" value="<?=$tariffType['CODE']?>">
                    <label for="tariffType_<?=$item['ID']?>_<?=$tariffType['CODE']?>"><?=$tariffType['NAME']?></label>
                </div>

            <? } ?>

        </div>
    </div>

</div>


<div class="dealerAccount-delivery__index-columns">

    <div class="dealerAccount-delivery__index-table distanceRangesBlock" <? if($item['DELIV_SETTINGS']['tariffType_'.$item['ID']] != 'distance_ranges'){ ?>style="display:none;"<? } ?>>
        <table class="dealerAccount-delivery__index-table" <? if( count($item['DELIV_SETTINGS']['distance_ranges']) == 0 ){ ?>style="display:none;"<? } ?>>
            <tbody>
                <tr>
                    <th>Расстояние "до", км</th>
                    <? foreach( $arResult['WEIGHT_RANGES'] as $weight_range ){ ?>
                        <th><?=$weight_range['NAME']?></th>
                    <? } ?>
                </tr>
                <? foreach( $item['DELIV_SETTINGS']['distance_ranges']['distances'] as $key => $distance ){ ?>
                    <tr class="key___tr" key="<?=$key?>">
                        <td>
                            <input type="text" name="distance_ranges[distances][<?=$key?>]" value="<?=$distance?>">
                        </td>
                        <? foreach( $arResult['WEIGHT_RANGES'] as $weight_range ){ ?>
                            <td>
                                <input class="price___input" type="text" name="distance_ranges[prices][<?=$key?>][<?=$weight_range['CODE']?>]" value="<?=$item['DELIV_SETTINGS']['distance_ranges']['prices'][$key][$weight_range['CODE']]?>">
                            </td>
                        <? } ?>
                    </tr>
                <? } ?>
            </tbody>
        </table>
        <span class="dealerAccount-delivery__index-addVariant">Добавить диапазон</span>
    </div>


    <div class="dealerAccount-delivery__index-table pricePerKilometerBlock" <? if($item['DELIV_SETTINGS']['tariffType_'.$item['ID']] != 'price_per_kilometer'){ ?>style="display:none;"<? } ?>>

        <table class="dealerAccount-delivery__index-table">
            <tbody>
                <tr>
                    <th></th>
                    <? foreach( $arResult['WEIGHT_RANGES'] as $weight_range ){ ?>
                        <th><?=$weight_range['NAME']?></th>
                    <? } ?>
                </tr>
                <tr>
                    <th>рублей за&nbsp;километр</th>
                    <? foreach( $arResult['WEIGHT_RANGES'] as $weight_range ){ ?>
                        <td>
                            <input class="price___input" type="text" name="prices_per_kilometer[<?=$weight_range['CODE']?>]" value="<?=$item['DELIV_SETTINGS']['prices_per_kilometer'][$weight_range['CODE']]?>">
                        </td>
                    <? } ?>
                </tr>
            </tbody>
        </table>

    </div>


    <div class="dealerAccount-delivery__index-column settingsTextBlock">
        <? if( project\delivery_price::$texts[$dType.'_'.$tType] ){ ?>
            <p class="dealerAccount-delivery__index-info "><?=project\delivery_price::$texts[$dType.'_'.$tType]?></p>
        <? } else { ?>
            <p class="dealerAccount-delivery__index-info ">Выберите методы расчета расстояния и стоимости доставки и заполните появившуюся таблицу</p>
        <? } ?>
    </div>

</div>





                            </div>
                        </div>

                        <div class="dealerAccount-delivery__indexBlock">
                            <div class="dealerAccount-delivery__indexBlock-wrapper">

                                <div class="dealerAccount-form__row">
                                    <div class="dealerAccount-form__field  dealerAccount-form__field--w100">
                                        <span>Минимальный срок доставки</span>
                                        <div class="dealerAccount-form__field--fromTo">
                                            <span>от</span>
                                            <input class="number___input" type="text" name="min_delivery_time" value="<?=$item['DELIV_SETTINGS']['min_delivery_time']?>">
                                            <span>дня</span>
                                        </div>
                                    </div>
                                    <div class="dealerAccount-form__field  dealerAccount-form__field--w100">
                                        <span>Срок доставки товаров «под заказ»</span>
                                        <div class="dealerAccount-form__field--fromTo dealerAccount-form__field--fromTo-vertical">
                                            <span>от</span>
                                            <input class="number___input" type="text" name="min_delivery_time_pod_zakaz" value="<?=$item['DELIV_SETTINGS']['min_delivery_time_pod_zakaz']?>">
                                            <span>до</span>
                                            <input class="number___input" type="text" name="max_delivery_time_pod_zakaz" value="<?=$item['DELIV_SETTINGS']['max_delivery_time_pod_zakaz']?>">
                                        </div>
                                    </div>
                                    
                                    <? //echo "<pre>"; print_r( $item['DELIV_SETTINGS'] ); echo "</pre>"; ?>

                                    <div class="dealerAccount-form__field  dealerAccount-form__field--w100">
                                        <span>Время доставки товаров:</span>
                                        <div class="dealerAccount-form__field--fromTo dealerAccount-form__field--fromTo-vertical">
<span>от</span>
<div class="styledSelect" style="width:120px; margin: 0 15px;">
    <select id="delivery_time_ot_select_<?=$item['ID']?>" class="delivery_time_ot_select" name="delivery_time_ot">
        <option <? if( $item['DELIV_SETTINGS']['delivery_time_ot'] == 'empty' || !$item['DELIV_SETTINGS']['delivery_time_ot'] ){ echo 'selected'; } ?> value="empty">Время "от"</option>
        <? for ( $h = 0; $h<=23; $h++ ){
            $hour = $h;
            if ( strlen($hour) == 1 ){ $hour = '0'.$hour; }
            $time = $hour.':00'; ?>
            <option <? if( $item['DELIV_SETTINGS']['delivery_time_ot'] == $time ){ echo 'selected'; } ?> value="<?=$time?>"><?=$time?></option>
        <? } ?>
    </select>
</div>
<span>до</span>
<div class="styledSelect" style="width:120px; margin: 0 15px;">
    <select id="delivery_time_do_select_<?=$item['ID']?>" class="delivery_time_do_select" name="delivery_time_do">
        <option <? if( $item['DELIV_SETTINGS']['delivery_time_do'] == 'empty' || !$item['DELIV_SETTINGS']['delivery_time_do'] ){ echo 'selected'; } ?> value="empty">Время "до"</option>
        <? for ( $h = 0; $h<=23; $h++ ){
            $hour = $h;
            if ( strlen($hour) == 1 ){ $hour = '0'.$hour; }
            $time = $hour.':00'; ?>
            <option <? if( $item['DELIV_SETTINGS']['delivery_time_do'] == $time ){ echo 'selected'; } ?> value="<?=$time?>"><?=$time?></option>
        <? } ?>
         <option <? if( $item['DELIV_SETTINGS']['delivery_time_do'] == '24:00' ){ echo 'selected'; } ?> value="24:00">23:59</option>
    </select>
</div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                        <div class="dealerAccount-delivery__indexBlock">
                            <div class="dealerAccount-delivery__indexBlock-wrapper">

                                <? $cnt = -1;
                                foreach( $arResult['DOP_SERVICES'] as $dop_service ){
                                    $cnt++; ?>

                                    <div class="dealerAccount-form__row  dealerAccount-form__row--fromToTable dop_service___block">
                                        <div class="dealerAccount-form__field  dealerAccount-form__field--wa">
                                            <div class="account-form__checkbox">
                                                <input type="checkbox" id="dop_setting_<?=$item['ID']?>_<?=$dop_service['CODE']?>" name="dop_services_<?=$item['ID']?>[]" value="<?=$dop_service['CODE']?>" <? if( in_array($dop_service['CODE'], $item['DELIV_SETTINGS']['dop_services']) ){ ?>checked<? } ?> onchange="dealerDopServicesCheckboxChange($(this));">
                                                <label for="dop_setting_<?=$item['ID']?>_<?=$dop_service['CODE']?>"><b><?=$dop_service['NAME']?></b></label>
                                            </div>



<div class="dop_service___settings" <? if( !in_array($dop_service['CODE'], $item['DELIV_SETTINGS']['dop_services']) ){ ?>style="display:none;"<? } ?>>

    <? // Подъем на этаж
    if( $dop_service['CODE'] == 'rise_to_the_floor' ){ ?>

        <span>Без лифта</span>
        <div class="dealerAccount-form__field--fromTo">
            <span>Минимальная стоимость подъема без лифта</span>
            <input class="price___input" type="text" name="<?=$dop_service['CODE']?>[no_lift][min_sum]" value="<?=$item['DELIV_SETTINGS'][$dop_service['CODE']]['no_lift']['min_sum']?>">
            <span>руб</span>
        </div>
        <div class="dealerAccount-form__field--fromTo">
            <span>Стоимость подъема на этаж</span>
            <input class="price___input" type="text" name="<?=$dop_service['CODE']?>[no_lift][floor_price]" value="<?=$item['DELIV_SETTINGS'][$dop_service['CODE']]['no_lift']['floor_price']?>">
            <span>руб/кг</span>
        </div>

        <span>С лифтом</span>
        <div class="dealerAccount-form__field--fromTo">
            <span>Стоимость подъема с лифтом</span>
            <input class="price___input" type="text" name="<?=$dop_service['CODE']?>[lift][price]" value="<?=$item['DELIV_SETTINGS'][$dop_service['CODE']]['lift']['price']?>">
            <span>руб/кг</span>
        </div>

        <div class="account-form__checkbox">
            <input type="checkbox" id="not_rise_large_goods_<?=$item['ID']?>_<?=$dop_service['CODE']?>" name="<?=$dop_service['CODE']?>[not_rise_large_goods]" value="Y" <? if( $item['DELIV_SETTINGS'][$dop_service['CODE']]['not_rise_large_goods'] == 'Y' ){ echo 'checked'; } ?>>
            <label for="not_rise_large_goods_<?=$item['ID']?>_<?=$dop_service['CODE']?>">Не производится подъём крупногабаритных товаров</label>
        </div>

    <? // Разгрузка товаров
    } else if( $dop_service['CODE'] == 'unloading_of_goods' ){ ?>

        <div class="dealerAccount-form__field--fromTo">
            <span>Минимальная стоимость разгрузки</span>
            <input class="price___input" type="text" name="<?=$dop_service['CODE']?>[min_sum]" value="<?=$item['DELIV_SETTINGS'][$dop_service['CODE']]['min_sum']?>">
            <span>руб</span>
        </div>

        <div class="dealerAccount-form__field--fromTo">
            <span>Стоимость разгрузки</span>
            <input class="price___input" type="text" name="<?=$dop_service['CODE']?>[price]" value="<?=$item['DELIV_SETTINGS'][$dop_service['CODE']]['price']?>">
            <span>руб/кг</span>
        </div>

<!--        <div class="account-form__checkbox">-->
<!--            <input type="checkbox" id="--><?//=$item['ID']?><!--_--><?//=$dop_service['CODE']?><!--_oversize" name="--><?//=$dop_service['CODE']?><!--[oversize]" --><?// if( $item['DELIV_SETTINGS'][$dop_service['CODE']]['oversize'] == 'Y' ){ echo 'checked'; } ?><!-- value="Y">-->
<!--            <label for="--><?//=$item['ID']?><!--_--><?//=$dop_service['CODE']?><!--_oversize">Другая стоимость для крупногабаритных товаров</label>-->
<!--        </div>-->
<!---->
<!--        <p class="dealerAccount-delivery__index-info">-->
<!--            Крупногабаритными товарами являются листовые материалы и профили длинной более 2 метров.-->
<!--        </p>-->
<!---->
<!--        <div class="dealerAccount-form__field--fromTo">-->
<!--            <span>Стоимость разгрузки крупногаб.товаров</span>-->
<!--            <input class="price___input" type="text" name="--><?//=$dop_service['CODE']?><!--[oversize_price]" value="--><?//=$item['DELIV_SETTINGS'][$dop_service['CODE']]['oversize_price']?><!--">-->
<!--            <span>руб/кг</span>-->
<!--        </div>-->

    <? // Перенос товаров
    } else if( $dop_service['CODE'] == 'transfer_of_goods' ){ ?>

        <div class="dealerAccount-form__field--fromTo">
            <span>Минимальная стоимость переноса</span>
            <input class="price___input" type="text" name="<?=$dop_service['CODE']?>[min_sum]" value="<?=$item['DELIV_SETTINGS'][$dop_service['CODE']]['min_sum']?>">
            <span>руб</span>
        </div>

        <div class="dealerAccount-form__field--fromTo">
            <span>Перемещение на 1 м</span>
            <input class="price___input" type="text" name="<?=$dop_service['CODE']?>[price]" value="<?=$item['DELIV_SETTINGS'][$dop_service['CODE']]['price']?>">
            <span>руб/кг</span>
        </div>

    <? } ?>

</div>

                                        </div>
                                    </div>

                                <? } ?>

                                <div class="dealerAccount-form__row  dealerAccount-form__row--fromToTable">
                                    <div class="dealerAccount-form__field  dealerAccount-form__field--wa">

                                        <br>
                                        <div class="dealerAccount-form__field--fromTo">
                                            <span>Бесплатная доставка от, руб.</span>
                                            <input class="price___input" type="text" name="free_delivery_basket_sum" value="<?=$item['DELIV_SETTINGS']['free_delivery_basket_sum']?>">
                                        </div>
                                        <br>

                                    </div>
                                </div>

                            </div>
                        </div>


                        <a style="cursor: pointer" class="dealerAccount-delivery__index-button dealerDeliveryPricesSave to___process">Сохранить изменения</a>

                    </div>

                </div>

            </form>

        <? } ?>

    </section>


    <?php if( 0 ){ ?>

        <div class="dealerAccount-form__wrapper  dealerAccount-form__wrapper--delivery new_delivery_location_block" style="border-top: 1px solid #c5c5c5; padding-top: 25px;">

            <form class="delivInfoForm" onsubmit="return false;">

                <h4 class="tab__title">Условия и стоимость доставки:</h4>

                <div class="account-form__wrapperMinusMargin" style="padding-bottom: 0;">
                    <div class="account-form__row  account-form__row--mb36" style="margin-bottom: 0;">
                        <div class="account-form__field  account-form__field--w760">
                            <div class="account-form__checkboxes">

                                <div class="account-form__checkbox  account-form__checkbox--w228">
                                    <input <? if( $arResult['USER']['UF_DELIV_INFO_TYPE'] == 'link' ){ echo 'checked'; } ?> type="radio" id="delivery_info_type_link" name="delivery_info_type" value="link">
                                    <label for="delivery_info_type_link">Ссылка на сайт</label>
                                </div>

                                <div class="account-form__checkbox  account-form__checkbox--w228">
                                    <input <? if( $arResult['USER']['UF_DELIV_INFO_TYPE'] == 'file' ){ echo 'checked'; } ?> type="radio" id="delivery_info_type_file" name="delivery_info_type" value="file">
                                    <label for="delivery_info_type_file">Файл</label>
                                </div>

                            </div>

                            <div class="dealerAccount-form__row delivLinkBlock" style="margin-top:20px; <? if( $arResult['USER']['UF_DELIV_INFO_TYPE'] != 'link' ){ ?>display:none;<? } ?>">
                                <div class="dealerAccount-form__field  dealerAccount-form__field--w271">
                                    <input type="text" name="deliv_info_link" value="<?=$arResult['USER']['UF_DELIV_INFO_LINK']?>">
                                </div>
                            </div>

                            <div class="dealerAccount-form__row delivFileBlock" style="margin-top:20px; <? if( $arResult['USER']['UF_DELIV_INFO_TYPE'] != 'file' ){ ?>display:none;<? } ?>">
                                <div class="dealerAccount-form__field  dealerAccount-form__field--w271" style="width: 100%;">

                                    <table class="delivFileTable">
                                        <tr>
                                            <td>
                                                <a <? if( $arResult['DELIV_INFO_FILE'] ){ ?>target="_blank" href="<?=$arResult['DELIV_INFO_FILE']['SRC']?>"<? } ?> download>
                                                    <img style="max-height: 100px" src="/local/templates/main/images/file<? if( $arResult['DELIV_INFO_FILE'] ){ ?>_ok<? } ?>.png">
                                                </a>
                                            </td>
                                            <td style="vertical-align: top;">

                                                <table>
                                                    <tr>
                                                        <td>Заголовок файла: </td>
                                                        <td>
                                                            <input type="text" name="deliv_info_file_title" <? if( $arResult['DELIV_INFO_FILE'] ){ ?>value="<?=$arResult['USER']['UF_DELIV_FILE_TITLE']?>"<? } ?>>
                                                        </td>
                                                    </tr>
                                                    <? if( $arResult['DELIV_INFO_FILE'] ){ ?>
                                                        <tr>
                                                            <td>Тип: </td>
                                                            <td><?=$arResult['DELIV_INFO_FILE']['EX']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Размер файла: </td>
                                                            <td><?=round($arResult['DELIV_INFO_FILE']['FILE_SIZE'], 1)?> <?=$arResult['DELIV_INFO_FILE']['FILE_SIZE_ED']?></td>
                                                        </tr>
                                                    <? } ?>
                                                    <tr>
                                                        <td>
                                                            <a class="delivInfoLink newDeliveryFile">Загрузить<? if( $arResult['DELIV_INFO_FILE'] ){ ?> новый<? } ?></a>
                                                        </td>
                                                        <? if( $arResult['DELIV_INFO_FILE'] ){ ?>
                                                            <td>
                                                                <a class="delivInfoLink deleteDeliveryFile">Удалить</a>
                                                            </td>
                                                        <? } ?>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>

                                    </table>

                                </div>
                            </div>

                            <p class="error___p"></p>

                            <a style="cursor: pointer; width: 110px;" class="account-form__button dealerDelivInfoSaveButton to___process">Сохранить</a>

                        </div>
                    </div>
                </div>

            </form>

        </div>

    <?php } ?>

</div>


<? foreach( project\delivery_price::$texts as $key => $text ){ ?>

    <div class="settings_text" id="<?=$key?>" style="display: none;"><?=$text?></div>

<? } ?>
