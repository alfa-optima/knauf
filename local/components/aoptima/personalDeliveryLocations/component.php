<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

global $USER;
$userID = $USER->GetID();
$currentUser = new project\user( $USER->GetID() );
$userProps = $currentUser->arUser;
if(strlen($userProps["UF_MAIN_USER"]) > 0){
	$user = new project\user( $userProps["UF_MAIN_USER"] );
	if( $user->isDealer() ){
		$userID = $user->arUser["ID"];
	}
}


$arResult['USER'] = tools\user::info($userID);

$arResult['DELIV_INFO_FILE'] = false;
if( intval($arResult['USER']['UF_DELIV_INFO_FILE']) > 0 ){
    $arResult['DELIV_INFO_FILE']['SRC'] = \CFile::GetPath($arResult['USER']['UF_DELIV_INFO_FILE']);
    $ar = explode('.', $arResult['DELIV_INFO_FILE']['SRC']);
    $arResult['DELIV_INFO_FILE']['EX'] = $ar[count($ar)-1];
    $arResult['DELIV_INFO_FILE']['INFO'] = \CFile::GetFileArray($arResult['USER']['UF_DELIV_INFO_FILE']);
    $arResult['DELIV_INFO_FILE']['FILE_SIZE'] = $arResult['DELIV_INFO_FILE']['INFO']['FILE_SIZE'];
    $arResult['DELIV_INFO_FILE']['FILE_SIZE_ED'] = 'Б';
    if( $arResult['DELIV_INFO_FILE']['FILE_SIZE'] > 1024 ){
        $arResult['DELIV_INFO_FILE']['FILE_SIZE'] = $arResult['DELIV_INFO_FILE']['FILE_SIZE']/1024;
        $arResult['DELIV_INFO_FILE']['FILE_SIZE_ED'] = 'КБ';
        if( $arResult['DELIV_INFO_FILE']['FILE_SIZE'] > 1024 ){
            $arResult['DELIV_INFO_FILE']['FILE_SIZE'] = $arResult['DELIV_INFO_FILE']['FILE_SIZE']/1024;
            $arResult['DELIV_INFO_FILE']['FILE_SIZE_ED'] = 'МБ';
        }
    }
}


// Местоположения дилера
$delivery_location = new project\delivery_location();
$arResult['ITEMS'] = $delivery_location->getList( $userID );
foreach ( $arResult['ITEMS'] as $key => $item ){
    $item['LOC'] = project\bx_location::getByID($item['UF_LOC_ID']);
    $item['DELIV_SETTINGS'] = [];
    if( strlen($item['UF_DELIV_SETTINGS']) > 0 ){
        $item['DELIV_SETTINGS'] = tools\funcs::json_to_array( $item['UF_DELIV_SETTINGS'] );
    }
    $arResult['ITEMS'][$key] =  $item;
}


$arResult['WEIGHT_RANGES'] = project\delivery_price::$weight_ranges;
$arResult['DOP_SERVICES'] = project\delivery_price::$dop_services;



$this->IncludeComponentTemplate();