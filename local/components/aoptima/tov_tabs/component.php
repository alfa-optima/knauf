<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arResult['el'] = $arParams['el'];
$arResult['tabs'] = array();

// Применение
if( strlen($arResult['el']['PROPERTY_PRIM_VALUE']['TEXT']) > 0 ){
    $tab_name = 'Применение';
    $arResult['tabs'][$tab_name] = array(
        'CODE' => strtolower(tools\funcs::translit($tab_name)),
        'VALUE' => htmlspecialchars_decode($arResult['el']['PROPERTY_PRIM_VALUE']['TEXT'])
    );
}

// Преимущества
if(
    is_array($arResult['el']['PROPERTY_PREIM_VALUE'])
    &&
    count($arResult['el']['PROPERTY_PREIM_VALUE']) > 0
){
    $tab_name = 'Преимущества';
    $arResult['tabs'][$tab_name] = array(
        'CODE' => strtolower(tools\funcs::translit($tab_name)),
        'VALUE' => $arResult['el']['PROPERTY_PREIM_VALUE']
    );
}

// Технические характеристики
if( strlen($arResult['el']['PROPERTY_TECH_VALUE']['TEXT']) > 0 ){
    $tab_name = 'Технические характеристики';
    $arResult['tabs'][$tab_name] = array(
        'CODE' => strtolower(tools\funcs::translit($tab_name)),
        'VALUE' => htmlspecialchars_decode($arResult['el']['PROPERTY_TECH_VALUE']['TEXT'])
    );
}

// Отзывы
ob_start();
    $APPLICATION->IncludeComponent(
        "aoptima:tovarReviews", "",
        array('el_id' => $arResult['el']['ID'])
    );
    $reviewsHTML = ob_get_contents();
ob_end_clean();
$tab_name = 'Отзывы';
$arResult['tabs'][$tab_name] = array(
    'CODE' => strtolower(tools\funcs::translit($tab_name)),
    'VALUE' => $reviewsHTML
);







$this->IncludeComponentTemplate();