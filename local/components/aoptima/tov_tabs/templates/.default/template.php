<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if( count($arResult['tabs']) > 0 ){ ?>

    <section class="block  block--tabs">
        <div class="block__wrapper">

            <h2 class="block__title">ПОДРОБНЕЕ</h2>

            <div class="product-tabs">

                <dl class="dropdowns dropdown">
                    <dt><a><span><?=array_keys($arResult['tabs'])[0]?></span></a></dt>
                    <dd>
                        <ul>
                            <? $cnt = 0;
                            foreach( $arResult['tabs'] as $tab_name => $tab ){ $cnt++; ?>
                                <li class='product-tab  <? if( $cnt==1 ){ ?>active<? } ?> <? if( $tab_name == 'Отзывы' ){ ?>reviewVkl<? } ?>' style="height:34px;">
                                    <a onclick="product_tab_click('<?=$tab['CODE']?>'); return true;" href="#tab_<?=$tab['CODE']?>" id="tabLink_<?=$tab['CODE']?>"><?=$tab_name?></a>
                                </li>
                            <? } ?>
                        </ul>
                    </dd>
                </dl>

                <script type="text/javascript">
                    dropDownHandler();
                </script>

                <? $cnt = 0;
                foreach( $arResult['tabs'] as $tab_name => $tab ){ $cnt++; ?>

                    <article class="tab bildtext <? if( $tab_name == 'Отзывы' ){ ?>tab--review<? } ?> <? if( $cnt==1 ){ ?>first<? } ?>" id="tab_<?=$tab['CODE']?>">
                        <section>
                            <div>
							
							
								<? if( $tab_name == 'Отзывы' ):?>
									<a style="cursor:pointer" class="product__review-button reviewButton to___process" item_id="<?=$arResult['el']['ID']?>" fullname="<?=$arResult['el']['NAME']?>">Написать отзыв</a>
								<?endif;?>
								
								
                                <h4 class="tab__title"><?=$tab_name?></h4>
                                <div class="tab__inner">

                                    <? if( $tab_name == 'Применение' ){ ?>
                                        <p><?=$tab['VALUE']?></p>
                                    <? } else if( $tab_name == 'Преимущества' ){ ?>
                                        <ul>
                                            <? foreach( $tab['VALUE'] as $item ){ ?>
                                                <li><?=$item?></li>
                                            <? } ?>
                                        </ul>
                                    <? } else if( $tab_name == 'Технические характеристики' ){ ?>
                                        <?=$tab['VALUE']?>
                                    <? } else if( $tab_name == 'Отзывы' ){ ?>
                                        <?=$tab['VALUE']?>
                                    <? } ?>

                                </div>
                            </div>
                        </section>
                    </article>

                <? } ?>

            </div>

        </div>
    </section>

<? } ?>