<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('iblock');

$arResult['KNAUF_LOC'] = $arParams['KNAUF_LOC'];

$arResult['MAIN_LOCATIONS'] = array();

$arResult['MAIN_LOCATIONS'][$arResult['KNAUF_LOC']['CITY_LOC_ID']] = array(
    'ID' => $arResult['KNAUF_LOC']['CITY_LOC_ID'],
    'NAME_RU' => $arResult['KNAUF_LOC']['CITY']
);

$arResult['LOCS_INFO'] = project\bx_location::$main_site_locations;

$arResult['BASE_DOMAIN'] = \Bitrix\Main\Config\Option::get('main', 'server_name');

$main_site_locations_info = project\bx_location::main_site_locations_info();


// TODO: временная проверка на наличие ТП (в регионе выбранного города)
//$filter = [  "IBLOCK_ID" => project\catalog::tpIblockID(),  "ACTIVE" => "Y"  ];
//foreach ( $main_site_locations_info as $key => $city_loc ){
//    $city_loc_chain = project\bx_location::getLocChain( $city_loc['ID'] );
//    foreach ( $city_loc_chain as $loc ){
//        if( $loc['PARENTS_TYPE_CODE'] == 'REGION' ){
//            $filter["PROPERTY_LOCATION"] = $loc['PARENTS_ID'];
//            $fields = [ "ID", "PROPERTY_LOCATION" ];
//            $dbElements = \CIBlockElement::GetList(
//                [ "SORT" => "ASC" ], $filter, false, [ "nTopCount" => 1 ], $fields
//            );
//            if( !$dbElements->GetNext() ){
//                unset( $main_site_locations_info[ $key ] );
//            }
//        }
//    }
//}
///////////////////////////////


foreach ( array_keys(project\bx_location::$main_site_locations) as $loc_id ){
    if( isset($main_site_locations_info[$loc_id]) ){
        $arResult['MAIN_LOCATIONS'][$loc_id] = $main_site_locations_info[$loc_id];
    }
}

$arResult['COLUMN_CNT'] = ceil(count($arResult['MAIN_LOCATIONS'])/3);











$this->IncludeComponentTemplate();