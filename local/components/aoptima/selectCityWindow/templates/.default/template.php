<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<h2 class="modal-city__title">Выберите свой город</h2>

<?php if( 0 ){ ?>
    <div>
        <p style="padding: 21px 48px 8px; margin: 0;"><i>Внимание! При смене населённого пункта ваша текущая корзина будет очищена.</i></p>
    </div>
<?php } ?>

<div class="modal-city__inner">

    <div class="account-form__field  account-form__field--w100">
        <input
            id="locCity"
            type="text"
            placeholder="Введите свой город, например 'Пермь'"
            dealer_name=""
            autocomplete="off"
            onfocus="cityFocus($(this));"
            onkeyup="cityKeyUp($(this));"
            onfocusout="cityFocusOut($(this));"
        >
        <input type="hidden" name="locCityID">
        <script>
        $(document).ready(function(){
            $('#locCity').autocomplete({
                source: '/ajax/searchOnlyCities.php',
                minLength: 2,
                delay: 700,
                select: locSelect
            })
        })
        </script>
    </div>

    <div class="modal-city__column">

        <? $cnt = 0;
        foreach( $arResult['MAIN_LOCATIONS'] as $loc_id => $loc )
        {
            $subDomain = (isset($arResult['LOCS_INFO'][$loc['ID']]['sub_domain'])?($arResult['LOCS_INFO'][$loc['ID']]['sub_domain'].'.'):'').$arResult['BASE_DOMAIN'];
            if(IS_ESHOP)
            {
                $subDomain = $arResult['BASE_DOMAIN'];
            }
            $cnt++; ?>

            <div class="modal-city__city <? if( $arResult['LOCS_INFO'][$loc_id] ){ ?>selectCityButton<? } ?> to___process" loc_id="<?=$loc['ID']?>" loc_name="<?=$loc['NAME_RU']?>" sub_domain="<?=$subDomain?>">
                <span <? if( $arResult['KNAUF_LOC']['CITY_LOC_ID'] == $loc['ID'] ){ ?>class="active"<? } ?>><?=$loc['NAME_RU']?></span>
            </div>

            <? if( $cnt%$arResult['COLUMN_CNT'] == 0 ){
                echo '</div><div class="modal-city__column">';
            } ?>

        <? } ?>

    </div>

</div>

