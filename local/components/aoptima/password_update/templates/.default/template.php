<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>


<section class="bcgBlock  bcgBlock--indexTop" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/images/blockbcg3.jpg');">
    <div class="bcgBlock__wrapper">
        <h2 class="bcgBlock__title">Восстановление пароля</h2>
    </div>
</section>


<section class="block">
    <div class="block__wrapper">

        <? if( $arResult['ERROR'] ){ ?>

            <p class="error___p"><?=$arResult['ERROR']?></p>

            <p><a href="/auth/" style="color: green; text-decoration: underline;">Авторизация</a></p>

        <? } else { ?>

            <h2 class="block__title">Новый пароль</h2>

            <form onsubmit="return false" method="post" class="form  form--restore password_update_form">
                <div class="form__wrapper">

                    <div class="form__group">
                        <label for="restore-email">Новый пароль</label>
                        <input type="password" name="PASSWORD" placeholder="Новый пароль">
                    </div>

                    <div class="form__group">
                        <label for="restore-email">Повтор пароля</label>
                        <input type="password" name="CONFIRM_PASSWORD" placeholder="Повтор пароля">
                    </div>

                    <p class="error___p"></p>

                    <input type="hidden" name="code" value="<?=$arResult['KOD']?>">

                    <div class="form__buttons">
                        <input type="button" value="Сохранить" class="form__button password_update_button to___process">
                    </div>

                </div>
            </form>

        <? } ?>

    </div>
</section>

