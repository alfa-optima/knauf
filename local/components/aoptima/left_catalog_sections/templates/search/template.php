<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<ul class="searchPanel-nav">

    <? foreach( $arResult['sections_1_level'] as $sect_1 ){ ?>

        <li class="searchPanel-nav__item  <? if( $sect_1['IS_ACTIVE'] == 'Y' ){ ?>searchPanel-nav__item--active <? if( $sect_1['subsections'] ){ ?>showSubMenu<? } ?><? } ?>">

            <a href="/search/?q=<?=$arParams['search_q']?>&section_id=<?=$sect_1['ID']?>" class="searchPanel-nav__item-link"><?=$sect_1['NAME']?><span class="searchPanel-nav__item-number">(<?=$sect_1['tovCNT']?>)</span></a>

        </li>

    <? } ?>

</ul>
