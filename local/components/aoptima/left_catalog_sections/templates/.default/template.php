<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<ul class="searchPanel-nav">

    <? foreach( $arResult['sections_1_level'] as $sect_1 ){ ?>

        <li class="searchPanel-nav__item  <? if( $sect_1['IS_ACTIVE'] == 'Y' ){ ?>searchPanel-nav__item--active <? if( $sect_1['subsections'] ){ ?>showSubMenu<? } ?><? } ?>">

            <a href="<?=$sect_1['SECTION_PAGE_URL']?>" class="searchPanel-nav__item-link"><?=$sect_1['NAME']?><span class="searchPanel-nav__item-number">(<?=$sect_1['tovCNT']?>)</span></a>

            <? if( $sect_1['subsections'] ){ ?>

                <ul class="searchPanel-nav__item-dropdown">

                    <? foreach ($sect_1['subsections'] as $subSect){ ?>

                        <li class="searchPanel-nav__item-dropdown-item <? if( $subSect['IS_ACTIVE'] == 'Y' ){ ?>menuSubsectActive<?  }?>">
                            <a href="<?=$subSect['SECTION_PAGE_URL']?>" class="searchPanel-nav__item-dropdown-link"><?=$subSect['NAME']?><span class="searchPanel-nav__item-number">(<?=$subSect['tovCNT']?>)</span></a>
                        </li>

                    <? } ?>

                </ul>

            <? } ?>

        </li>

    <? } ?>

</ul>
