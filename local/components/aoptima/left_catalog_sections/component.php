<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('iblock');

$arResult['section'] = $arParams['section'];


$arResult['sections_1_level'] = project\catalog::sections_1_level();
foreach ( $arResult['sections_1_level'] as $key => $sect1 ){

    if(
        !$arParams['search_sections']
        ||
        (
            is_array($arParams['search_sections'][$sect1['ID']])
            &&
            count($arParams['search_sections'][$sect1['ID']]) > 0
        )
    ){

        // Подсчёт элементов
        $filter = Array(
            "IBLOCK_CODE" => project\catalog::CATALOG_IBLOCK_CODE,
            "ACTIVE" => "Y",
            "SECTION_ID" => $sect1['ID'],
            "INCLUDE_SUBSECTIONS" => "Y"
        );

        if(
            is_array($arParams['search_sections'][$sect1['ID']])
            &&
            count($arParams['search_sections'][$sect1['ID']]) > 0
        ){
            $filter['ID'] = $arParams['search_sections'][$sect1['ID']];

            if ( $_GET['section_id'] == $sect1['ID'] ){
                $arResult['sections_1_level'][$key]['IS_ACTIVE'] = 'Y';
            }

        } else {

            if (tools\funcs::string_begins_with($sect1['SECTION_PAGE_URL'], tools\funcs::pureURL())){
                $arResult['sections_1_level'][$key]['IS_ACTIVE'] = 'Y';
            }

            $subSects = tools\section::sub_sects($sect1['ID']);
            foreach ($subSects as $subSect) {
                if ($subSect['DEPTH_LEVEL'] == 2){
                    if ( $subSect['SECTION_PAGE_URL'] == tools\funcs::pureURL() ){
                        $subSect['IS_ACTIVE'] = 'Y';
                    }
                    if( $arResult['sections_1_level'][$key]['IS_ACTIVE'] == 'Y' ){
                        $fieldsSUB = Array("ID");
                        $filterSUB = Array(
                            "IBLOCK_CODE" => project\catalog::CATALOG_IBLOCK_CODE,
                            "ACTIVE" => "Y",
                            "SECTION_ID" => $subSect['ID'],
                            "INCLUDE_SUBSECTIONS" => "Y"
                        );
                        if(
                            is_array($arParams['search_sections'][$sect1['ID']])
                            &&
                            count($arParams['search_sections'][$sect1['ID']]) > 0
                        ){
                            $filterSUB['ID'] = $arParams['search_sections'][$sect1['ID']];
                        }
                        $dbElementsSUB = \CIBlockElement::GetList(array("SORT" => "ASC"), $filterSUB, false, false, $fieldsSUB);
                        $subSect['tovCNT'] = $dbElementsSUB->SelectedRowsCount();
                    }


                    $arResult['sections_1_level'][$key]['subsections'][$subSect['ID']] = $subSect;
                }
            }

        }


        $fields = Array("ID");
        $dbElements = \CIBlockElement::GetList(array("SORT" => "ASC"), $filter, false, false, $fields);
        $arResult['sections_1_level'][$key]['tovCNT'] = $dbElements->SelectedRowsCount();


    } else {
        unset($arResult['sections_1_level'][$key]);
    }
}




$this->IncludeComponentTemplate();