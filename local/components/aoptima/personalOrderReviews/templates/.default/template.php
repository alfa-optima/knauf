<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if( $arParams['IS_LOAD'] != 'Y' ){ ?>

    <h4 class="tab__title">Отзывы о заказах</h4>

    <? if( count($arResult['REVIEWS']) > 0 ){ ?>

        <table class="account-form__table-reviews">
            <tbody class="dealerReviewsArea">
            <tr>
                <th>Покупатель</th>
                <th>Информация о&nbsp;заказе</th>
                <th>Отзыв</th>
            </tr>

            <? $cnt = 0;
            foreach( $arResult['REVIEWS'] as $key => $review ){ $cnt++;
                if( $cnt <= $arResult['MAX_CNT'] ){
                    $APPLICATION->IncludeComponent(
                        "aoptima:dealerReviewItem", "",
                        array( 'review' => $review )
                    );
                }
            } ?>

            </tbody>
        </table>

        <div class="more-button dealerReviewsMoreButton to___process" <? if( $cnt <= $arResult['MAX_CNT'] ){ ?>style="display:none"<? } ?>>
            <span>Показать еще</span>
        </div>

    <? } else { ?>

        <div class="dealerAccount-form__completedOrders">

            <p class="no___count" style="padding:30px; font-size: 16px; text-align:center;">Отзывов пока нет</p>

        </div>

    <? } ?>

<? } else if( $arParams['IS_LOAD'] == 'Y' ){

    $cnt = 0;
    foreach( $arResult['REVIEWS'] as $key => $review ){ $cnt++;
        if( $cnt <= $arResult['MAX_CNT'] ){
            $APPLICATION->IncludeComponent(
                "aoptima:dealerReviewItem", "",
                array( 'review' => $review )
            );
        } else {
            echo '<tr class="ost"></tr>';
        }
    }

} ?>
