<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if( $arParams['IS_LOAD'] != 'Y' ){ ?>

    <section>
        <div>
            <h4 class="tab__title">Отзывы о заказах дилера</h4>


            <? if( count($arResult['REVIEWS']) > 0 ){ ?>

                <div class="tab__inner">

                    <table>
                        <tbody class="dealerReviewsArea">

                            <tr>
                                <th>Покупатель</th>
                                <th>Отзыв</th>
                            </tr>

                            <? $cnt = 0;
                            foreach( $arResult['REVIEWS'] as $key => $review ){ $cnt++;
                                if( $cnt <= $arResult['MAX_CNT'] ){
                                    $APPLICATION->IncludeComponent(
                                        "aoptima:dealerReviewItem", "dealer_page",
                                        array( 'review' => $review )
                                    );
                                }
                            } ?>

                        </tbody>
                    </table>

                    <div class="more-button dealerReviewsMoreButton dealer_page to___process" <? if( $cnt <= $arResult['MAX_CNT'] ){ ?>style="display:none"<? } ?>>
                        <span>Показать еще</span>
                    </div>

                </div>

            <? } else { ?>

                <h4 class="tab__title" style="text-align:center;">Отзывов пока нет</h4>

            <? } ?>

        </div>
    </section>


<? } else if( $arParams['IS_LOAD'] == 'Y' ){

    $cnt = 0;
    foreach( $arResult['REVIEWS'] as $key => $review ){ $cnt++;
        if( $cnt <= $arResult['MAX_CNT'] ){
            $APPLICATION->IncludeComponent(
                "aoptima:dealerReviewItem", "dealer_page",
                array( 'review' => $review )
            );
        } else {
            echo '<tr class="ost"></tr>';
        }
    }

} ?>
