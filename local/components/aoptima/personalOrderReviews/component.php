<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('sale');

use Bitrix\Main,
Bitrix\Main\Localization\Loc as Loc,
Bitrix\Main\Loader,
Bitrix\Main\Config\Option,
Bitrix\Sale\Delivery,
Bitrix\Sale\PaySystem,
Bitrix\Sale,
Bitrix\Sale\Basket,
Bitrix\Sale\Order,
Bitrix\Sale\DiscountCouponsManager,
Bitrix\Main\Context;



$arResult['MAX_CNT'] = 10;

// Список вариантов доставки
$arResult['DS_LIST'] = project\ds::getList();


$stop_items = false;
if( is_array($_POST['stop_ids']) && count($_POST['stop_ids']) > 0 ){
    $stop_items = $_POST['stop_ids'];
}


$review_order = new project\review_order();
$arResult['REVIEWS'] = $review_order->getList(
    false,
    false,
    $arParams['DEALER_ID'],
    $arResult['MAX_CNT']+1,
    $stop_items
);

foreach ( $arResult['REVIEWS'] as $k => $review ){

    if( $arParams['IS_DEALER_PAGE'] != 'Y' ){

        $obOrder = Sale\Order::load($review['UF_ORDER']);
        $propValues = project\order::getOrderPropValues($obOrder);

        $paymentCollection = $obOrder->getPaymentCollection();
        foreach ( $paymentCollection as $payment ){
            $ps_id = $payment->getPaymentSystemId();
            $review['ORDER']['ps'] = array(
                'ID' => $ps_id,
                'NAME' => $payment->getPaymentSystemName()
            );
        }

        $review['ORDER']['DATE_INSERT'] = ConvertDateTime($obOrder->getDateInsert(), "DD.MM.YYYY", "ru");

        // Стоимость доставки
        $review['ORDER']['DELIVERY_SUM'] = 0;
        $review['ORDER']['DELIVERY_SUM_FORMAT'] = number_format($review['ORDER']['DELIVERY_SUM'], project\catalog::ROUND, ",", " ");

        $review['ORDER']['BASKET_CNT'] = 0;   $review['ORDER']['BASKET_SUM'] = 0;
        $review['ORDER']['TOTAL_SUM'] = 0;

        // Инфо по корзине
        $basket = Sale\Basket::loadItemsForOrder($obOrder);
        foreach( $basket as $key => $basketItem ){

            $el = tools\el::info( $basketItem->getProductId() );

            $product = tools\el::info( $el['PROPERTY_CML2_LINK_VALUE'] );
            $product = project\catalog::updateProductName($product);
            $product_img = intval($product['DETAIL_PICTURE'])>0?(tools\funcs::rIMGG($product['DETAIL_PICTURE'], 4, 40, 40)):false;

            $review['ORDER']['arBasket'][$key] = array(
                'ID'                => $basketItem->getId(),
                'el'                => $el,
                'product'           => $product,
                'product_img'       => $product_img,
                'QUANTITY'          => $basketItem->getQuantity(),
                'sum'               => $basketItem->getQuantity() * $basketItem->getPrice(),
                'sumFormat'         => number_format($basketItem->getQuantity() * $basketItem->getPrice(), project\catalog::ROUND, ",", " "),
            );

            $review['ORDER']['BASKET_SUM'] += $basketItem->getQuantity() * $basketItem->getPrice();
            $review['ORDER']['BASKET_CNT'] += $basketItem->getQuantity();
        }

        $review['ORDER']['TOTAL_SUM'] = $review['ORDER']['BASKET_SUM'] + $review['ORDER']['DELIVERY_SUM'];

        $review['ORDER']['BASKET_SUM_FORMAT'] = number_format($review['ORDER']['BASKET_SUM'], project\catalog::ROUND, ",", " ");
        $review['ORDER']['BASKET_CNT_STR'] = $review['ORDER']['BASKET_CNT'].' '.tools\funcs::pfCnt($review['ORDER']['BASKET_CNT'], "товар", "товара", "товаров");

        $review['ORDER']['TOTAL_SUM_FORMAT'] = number_format($review['ORDER']['TOTAL_SUM'], project\catalog::ROUND, ",", " ");

        $shipmentCollection = $obOrder->getShipmentCollection();
        foreach ( $shipmentCollection as $shipment ){
            $ds_id = $shipment->getDeliveryId();
            $review['ORDER']['ds'] = array(
                'ID' => $ds_id,
                'NAME' => $shipment->getDeliveryNAME()
            );
        }

        $review['ORDER']['delivType'] = $arResult['DS_LIST'][$review['ORDER']['ds']['ID']]['delivType'];
        $review['ORDER']['DELIVERY_ADDRESS'] = project\order::getDeliveryAddress( $propValues );

        $review['ORDER']['LIFT'] = project\order::getPropValue( $propValues, 'LIFT' );
        $review['ORDER']['DELIV_DATE'] = project\order::getPropValue( $propValues, 'DELIV_DATE' );
        $review['ORDER']['DELIV_TIME'] = project\order::getPropValue( $propValues, 'DELIV_TIME' );


        $review['ORDER']['ORDER_DEALER_STATUSES'] = $arResult['ORDER_STATUSES'];
        $order_status = new project\order_status();
        $review['ORDER']['ORDER_DEALER_STATUS'] = $order_status->get( $obOrder, $propValues );

    }

    $arResult['REVIEWS'][$k] = $review;
}















$this->IncludeComponentTemplate();