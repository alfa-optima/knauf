<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');  use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools');  use AOptima\Tools as tools;
\Bitrix\Main\Loader::includeModule('iblock');

$arResult['MESSAGE'] = false;

$user = new project\user();

if(
    !$user->isDealer()
    &&
    intval($_SESSION['LOC']['CITY_LOC_ID']) > 0
){

    $has = false;

    \Bitrix\Main\Loader::includeModule('iblock');

    $region_name = false;
    $loc_chain = project\bx_location::getLocChain($_SESSION['LOC']['CITY_LOC_ID']);
    foreach ( $loc_chain as $key => $loc_chain_item ){
        if( $loc_chain_item['PARENTS_TYPE_CODE'] == 'REGION' ){
            $region_name = $loc_chain_item['PARENTS_NAME_RU'];
            $filter = [
                "IBLOCK_CODE" => project\catalog::TP_IBLOCK_CODE,
                "ACTIVE" => "Y",
                "PROPERTY_LOCATION" => $loc_chain_item['PARENTS_ID']
            ];
            $fields = [ "ID", "PROPERTY_LOCATION" ];
            $dbElements = \CIBlockElement::GetList(
                array("SORT"=>"ASC"), $filter, false, ["nTopCount"=>1], $fields
            );
            if ($element = $dbElements->GetNext()){
                $has = true;
            }
        }
    }


    if( !$has && $region_name ){

        $arResult['MESSAGE'] = 'Это бета-версия сайта. Для региона &#171;'.$region_name.'&#187; на данный момент нет ценовых предложений дилеров';
    }

}





$this->IncludeComponentTemplate();