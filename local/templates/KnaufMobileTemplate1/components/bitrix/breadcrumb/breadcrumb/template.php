<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
include "lang/ru/template.php";

if(empty($arResult)) 
	return "";


$strReturn = '<ol class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList"><li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item"  href="/"><span itemprop="name">Главная</span></a><meta itemprop="position" content="1"></li>';
$page_title = $GLOBALS['APPLICATION']->GetTitle();

$itemSize = count($arResult)-1;

$cnt = 1;
for ($index = 0; $index <= $itemSize; $index++){ $cnt++;
	$title = /*htmlspecialcharsex(*/ $arResult[$index]["TITLE"] /*)*/;
	if ($arResult[($index+1)]["TITLE"]!=$arResult[($index)]["TITLE"]){
		if(($arResult[$index]["LINK"] <> "") && $arResult[$index+1]["TITLE"]){

			$strReturn .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item"  href=' . $arResult[$index]["LINK"] . '><span itemprop="name">' . $title . '</span></a><meta itemprop="position" content="' . $cnt . '"></li>';

		} else {

			$strReturn .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item"><span itemprop="name">' . $title . '</span></a><meta itemprop="position" content="' . $cnt . '"></li>';

		}

	}
}

$strReturn .= '</ol>';

return $strReturn; ?>
