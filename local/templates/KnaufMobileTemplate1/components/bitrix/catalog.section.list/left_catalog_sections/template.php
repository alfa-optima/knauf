<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$this->setFrameMode(true);

$level_1 = [];
foreach ( $arResult["SECTIONS"] as $sect_1 ){
    if( $sect_1['DEPTH_LEVEL'] == 1 ){
        $level_1[$sect_1['ID']] = $sect_1;
    }
}
foreach ( $arResult["SECTIONS"] as $sect_2 ){
    if(
        $sect_2['DEPTH_LEVEL'] == 2
        &&
        intval($sect_2['IBLOCK_SECTION_ID']) > 0
        &&
        $level_1[$sect_2['IBLOCK_SECTION_ID']]
    ){
        $level_1[$sect_2['IBLOCK_SECTION_ID']]['subsections'][$sect_2['ID']] = $sect_2;
    }
}

if (count($arResult["SECTIONS"]) > 0){ ?>

    <ul class="searchPanel-nav">

        <? foreach( $level_1 as $sect_1 ){ ?>

            <li class="searchPanel-nav__item  <? if( tools\funcs::string_begins_with($sect_1['SECTION_PAGE_URL'], $arParams['pureURL']) ){ ?>searchPanel-nav__item--active <? if( $sect_1['subsections'] ){ ?>showSubMenu<? } ?><? } ?>">

                <? if(
                    tools\funcs::string_begins_with($sect_1['SECTION_PAGE_URL'], $arParams['pureURL'])
                ){ ?>
                    <a href="javascript:;" class="searchPanel-nav__item-link"><?=$sect_1['NAME']?><span class="searchPanel-nav__item-number">(<?=$sect_1['ELEMENT_CNT']?>)</span></a>
                <? } else { ?>
                    <a href="<?=$sect_1['SECTION_PAGE_URL']?>" class="searchPanel-nav__item-link"><?=$sect_1['NAME']?><span class="searchPanel-nav__item-number">(<?=$sect_1['ELEMENT_CNT']?>)</span></a>
                <? } ?>


                <? if( $sect_1['subsections'] ){ ?>

                    <ul class="searchPanel-nav__item-dropdown">

                        <? foreach ($sect_1['subsections'] as $subSect){ ?>

                            <li class="searchPanel-nav__item-dropdown-item <? if( tools\funcs::string_begins_with($subSect['SECTION_PAGE_URL'], $arParams['pureURL']) ){ ?>menuSubsectActive<?  }?>">
                                <a href="<?=$subSect['SECTION_PAGE_URL']?>" class="searchPanel-nav__item-dropdown-link"><?=$subSect['NAME']?><span class="searchPanel-nav__item-number">(<?=$subSect['ELEMENT_CNT']?>)</span></a>
                            </li>

                        <? } ?>

                    </ul>

                <? } ?>

            </li>

        <? } ?>

    </ul>

<? } ?>