<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


if (count($arResult["ITEMS"]) > 0){ ?>

    <article class="block  block--top">
        <div class="block__wrapper">

            <h2 class="block__title  block__title--faq">Вопрос-ответ</h2>

            <section class="accordion">

                <? foreach($arResult["ITEMS"] as $arItem){ ?>

                    <article class="accordion__section">
                        <a href="javascript:;" class="accordion__section-title">
                            <h3><?=$arItem['NAME']?></h3>
                        </a>
                        <section class="accordion__section-content">
                            <p><?=$arItem['PREVIEW_TEXT']?></p>
                        </section>
                    </article>

                <? } ?>

            </section>

        </div>
    </article>

    <script>
    $('.accordion__section-title').click(function(e) {

        if ($(this).hasClass('active')) {

            $(this).removeClass('active');
            $(this).siblings('.accordion__section-content').slideUp(300).removeClass('open');

        } else {

            $(this).addClass('active');
            $(this).siblings('.accordion__section-content').slideDown(300).addClass('open');

            ym(55379437, 'reachGoal', 'clickQuestion', $(this).find('h3').html());
        }
    });
    </script>

<? } ?>