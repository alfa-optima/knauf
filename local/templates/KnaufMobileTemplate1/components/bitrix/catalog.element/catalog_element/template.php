<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$offers_dealers = [];
foreach ( $arResult['OFFERS'] as $offer ){
    $offers_dealers[] = $offer['PROPERTIES']['DEALER']['VALUE'];
}
$offers_dealers = array_unique($offers_dealers);

?>




<article class="block  block--top">
    <div class="block__wrapper">
        <section class="product <? if( $arResult['PROPERTIES']['ACTION']['VALUE'] ){ echo 'product--sale'; } ?>">

            <h1 class="product__title">
				<?if ($arResult['PROPERTY_79']):?>
					<?echo $arResult['PROPERTY_79'];?>
				<?else:?>
					<?=$arResult['NAME']?>
				<?endif;?>
			</h1>


            <h2 class="product__subtitle"><?=$arResult['PREVIEW_TEXT']?></h2>

            <div class="product__index">
                <div class="product__left">

                    <div class="product__review">
                        <div class="product__review-rating">
                            <div class="product__review-stars">
                                <span class="product__review-star  <? if( $arParams['VOTE_RATING'] >= 1 ){ ?>product__review-star--full<? } ?>"></span>
                                <span class="product__review-star  <? if( $arParams['VOTE_RATING'] >= 2 ){ ?>product__review-star--full<? } ?>"></span>
                                <span class="product__review-star  <? if( $arParams['VOTE_RATING'] >= 3 ){ ?>product__review-star--full<? } ?>"></span>
                                <span class="product__review-star  <? if( $arParams['VOTE_RATING'] >= 4 ){ ?>product__review-star--full<? } ?>"></span>
                                <span class="product__review-star <? if( $arParams['VOTE_RATING'] == 5 ){ ?>product__review-star--full<? } ?>"></span>
                            </div>
                            <span class="toReviewsLink">Отзывов о товаре: <?=$arParams['REVIEWS_CNT']?></span>
                        </div>

                        <? if(
                            $arParams['IS_AUTH'] == 'Y'
                            &&
                            $arParams['IS_DEALER'] == 'N'
                        ){ ?>
                            <a style="cursor:pointer" class="product__review-button reviewButton to___process" item_id="<?=$arResult['ID']?>">Написать отзыв</a>
                        <? } ?>

                    </div>

                    <div class="product__gallery  product-gallery">

                        <div class="product-gallery__image">
                            <? if( count($arResult['PICTURES']) > 0 ){ ?>
                                <a class="product-gallery__slide" href="<?=tools\funcs::rIMGG($arResult['PICTURES'][0], 4, 800, 700)?>" rel="group1">
                                    <img alt="<?=$arResult['NAME']?>" src="<?=tools\funcs::rIMGG($arResult['PICTURES'][0], 5, 433, 325)?>">
                                </a>
                                <? unset($arResult['PICTURES'][0]);
                            } else { ?>
                                <a>
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/no_image_detail.png">
                                </a>
                            <? } ?>
                        </div>

                        <? if( count($arResult['PICTURES']) > 0 ){ ?>
                            <div class="product-gallery__thumbs">
                                <div class="slider product-gallery__thumb">
                                    <? foreach ( $arResult['PICTURES'] as $photo_id ){ ?>
                                        <a class="product-gallery__slide  slider__item" href="<?=tools\funcs::rIMGG($photo_id, 4, 800, 700)?>" rel="group1">
                                            <img alt="<?=$arResult['NAME']?>" src="<?=tools\funcs::rIMGG($photo_id, 5, 433, 325)?>">
                                        </a>
                                    <? } ?>
                                </div>
                            </div>
                        <? } ?>

                    </div>
                    
                </div>

                <div class="product__right">

                    <div class="product__desc">
                        <?=$arResult['DETAIL_TEXT']?>
                    </div>

                    <a href="javascript:;" class="more">развернуть</a>

                    <div class="product__prices">
                        <? if( $arResult['PRICE_MIN'] ){ ?>
                            <span class="product__fromPrice">от <?=$arResult['PRICE_MIN']?>
                                <span class="rub">₽</span>
                            </span>
                        <? }
                        if(
                            $arResult['PRICE_MAX']
                            &&
                            $arResult['PRICE_MIN'] != $arResult['PRICE_MAX']
                        ){ ?>
                            <span class="product__toPrice">до <?=$arResult['PRICE_MAX']?>
                                <span class="rub">₽</span>
                            </span>
                        <? } ?>
						<!--<span class="tooltip__icon" title="product1">?</span>-->
						
                    </div>

                    <? // Модификации
                    if(
                        count($arParams['productMods']) > 0
                        ||
                        count($arResult['OFFERS']) > 0
                    ){ ?>

                        <form class="product__form  product-form">
                            <div class="product-form__wrapper">

                                <? // Модификации
                                if( count($arParams['productMods']) > 1 ){ ?>

                                    <div class="product-form__field  product-form__field--packing">
                                        <span>Модификации</span>
                                        <div class="product-form__select">
                                            <select name="productModSelect">
                                                <? foreach( $arParams['productMods'] as $product ){ ?>
                                                    <option <? if( $product['ID'] == $arResult['ID'] ){ echo 'selected'; } ?> value="<?=$product['URL']?>"><?=$product['MOD']?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                    </div>

                                <? } ?>

                                <? if(
                                    count($arResult['OFFERS']) > 0
                                    &&
                                    $arParams['IS_DEALER'] != 'Y'
                                ){ ?>

                                    <div class="product-form__field  product-form__field--quantity">
                                        <span>Количество</span>
                                        <input type="text" onkeyup="this.value = this.value.replace (/\D/gi, '').replace (/^0+/, '');" onblur="var cnt = $(this).val(); if( cnt.length == 0 ){ $(this).val(1); }" name="basketCnt" value="1">
                                    </div>

                                <? } ?>

                            </div>
                        </form>

                    <? } ?>

                    <? if( count($arResult['OFFERS']) == 0 ){ ?>
                        <div style="text-align:center; padding: 0 0 15px 0;">
                            <p class="no___count">Нет предложений</p>
                        </div>
                    <? } ?>

                    <div class="product__buttons">

                        <? if( $arParams['IS_DEALER'] == 'N' ){

                            if( count($arResult['OFFERS']) > 0 ){ ?>

                                <a class="product__button openQuickOrderModal to___process" style="cursor: pointer;" item_id="<?=$arResult['ID']?>" dealer_ids="<?=implode('|', $offers_dealers)?>">Быстрый заказ</a>

                                <a class="product__button basketButton basketButtonDetail to___process" style="cursor: pointer;" item_id="<?=$arResult['OFFERS'][ array_keys($arResult['OFFERS'])[0] ]['ID']?>">В корзину</a>

                            <? } else { ?>

                                <a class="product__button underOrderButton to___process" style="cursor: pointer;" item_id="<?=$arResult['ID']?>">Под заказ</a>

                            <? }

                        } else if( $arParams['IS_DEALER'] == 'Y' ){ ?>

                            <a class="product-item__cartLink addPriceOpenWindow to___process" style="cursor: pointer;" item_id="<?=$arResult['ID']?>">Добавить</a>

                        <? } ?>



                        <!--
                        <a class="product__button  product__button--bookmark" href="javascript:;">Запомнить</a>
                        -->

                    </div>

                </div>

            </div>
        </section>
    </div>
</article>

