<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
global $APPLICATION;
if (isset($templateData['TEMPLATE_THEME']))
{
	$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}
?>
<script>
    $(document).ready(function(){
        <?
        // отметим товары в корзине
        foreach($_SESSION["BASKET"] as $arBaskProd)
        {
        ?>
        var $prodBlock = $(".productItem[data-id=<?=$arBaskProd["ID"]?>]");

        $prodBlock.find(".catalog-list-inbask .listfieldCount").val(<?=$arBaskProd["QUANTITY"]?>);

        // пересчитаем количество в метрах
        var $size = $prodBlock.find('.catalog-list-inbask .slider__item-counterDimension');
        var size_one = parseFloat($size.data("size"));

        var size = size_one * <?=$arBaskProd["QUANTITY"]?>;
        $size.find('span').text(size.toFixed(2));

        $prodBlock.parents(".bx-viewport").css("height", "auto");

        $prodBlock.find(".basketButton").hide();
        $prodBlock.find(".catalog-list-inbask").show();
        <?
        }
        ?>
        <?
        \Bitrix\Main\Loader::includeModule('aoptima.project');
        use AOptima\Project as project;
        // отметим товары по акции
        $arGifts = project\action_gifts::GetList();

        foreach($arGifts as $arGift)
        {
        ?>
        $prodBlock = $(".productItem[data-id=<?=$arGift["PROD"]?>]");

        if($prodBlock.hasClass("slider__item"))
        {
            $prodBlock.addClass("slider__item--promotion");
        }
        else
        {
            $prodBlock.addClass("product-item--promotion");
        }
        <?
        }
        ?>
    });
</script>
