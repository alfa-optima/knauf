<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (count($arResult['ITEMS']) > 0){ ?>

    <section id="popular" class="sliderBlock  sliderBlock--popular">
        <div class="sliderBlock__wrapper">
            <h2 class="sliderBlock__title">Популярные товары</h2>
            <div class="slider  slider--products">

                <? foreach ($arResult['ITEMS'] as $key => $arItem){

                    // catalog_item
                    $APPLICATION->IncludeComponent(
                        "aoptima:catalog_item", "catalog_slider",
                        array(
                            "LOC" => $arParams['LOC'],
                            'arItem' => $arItem,
                            'IS_DEALER' => $arParams['IS_DEALER']
                        )
                    );

                } ?>

            </div>
        </div>
    </section>

<? } ?>