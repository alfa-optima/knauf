<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

$max_cnt = $arParams['PAGE_ELEMENT_COUNT']-1;


if ( count($arResult['ITEMS']) > 0 ){ ?>

    <section class="bcgBlock " style="background-color: white;">

        <div class="searchBlock__content">

            <div class="city_landing_products">

                <ul class="searchBlock__items catalog_load_block">

                    <? foreach ($arResult['ITEMS'] as $key => $arItem){

                        $APPLICATION->IncludeComponent(
                            "aoptima:catalog_item", "catalog",
                            array(
                                "LOC" => $arParams['LOC'],
                                'arItem' => $arItem,
                                'IS_DEALER' => $arParams['IS_DEALER'],
                                "IS_MOB_APP" => $arParams['IS_MOB_APP'],
                            )
                        );

                    } ?>

                </ul>

            </div>

        </div>

    </section>

<? } ?>