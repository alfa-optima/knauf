<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
global $APPLICATION;
if (isset($templateData['TEMPLATE_THEME']))
{
	$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}
?>
<script>
    $(document).ready(function(){
        <?
        // отметим товары в корзине
        foreach($_SESSION["BASKET"] as $arBaskProd)
        {
        ?>
        var $prodBlock = $(".productItem[data-id=<?=$arBaskProd["ID"]?>]");

        $prodBlock.find(".catalog-list-inbask .listfieldCount").val(<?=$arBaskProd["QUANTITY"]?>);

        // пересчитаем количество в метрах
        var $size = $prodBlock.find('.catalog-list-inbask .slider__item-counterDimension');
        var size_one = parseFloat($size.data("size"));

        var size = size_one * <?=$arBaskProd["QUANTITY"]?>;
        $size.find('span').text(size.toFixed(2));

        $prodBlock.parents(".bx-viewport").css("height", "auto");

        $prodBlock.find(".basketButton").hide();
        $prodBlock.find(".catalog-list-inbask").show();
        <?
        }
        ?>
    });
</script>
