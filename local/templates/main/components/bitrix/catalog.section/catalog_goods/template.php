<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

$max_cnt = $arParams['PAGE_ELEMENT_COUNT']-1;


if (count($arResult['ITEMS']) > 0){

    if( $arParams['IS_AJAX'] != 'Y' ){ ?>

        <div class="searchBlock__content">

            <ul class="searchBlock__items catalog_load_block">

    <? }

    $cnt = 0;
    foreach ($arResult['ITEMS'] as $key => $arItem){ $cnt++;
        if( $cnt <= $max_cnt ){

            // Подкатегории
            $APPLICATION->IncludeComponent(
                "aoptima:catalog_item", "catalog",
                array(
                    "LOC" => $arParams['LOC'],
                    'arItem' => $arItem,
                    'IS_DEALER' => $arParams['IS_DEALER'],
                    "IS_MOB_APP" => $arParams['IS_MOB_APP'],
                )
            );

        }
   }

    if( $arParams['IS_AJAX'] != 'Y' ){ ?>

            </ul>

            <div class="more-button catalog_load_button to___process" <? if( $cnt <= $max_cnt ){ ?>style="display:none"<? } ?>>
                <span>Показать еще</span>
            </div>

            <?php if( isset($arParams['seo_page']) && strlen($arParams['seo_page']['TEXT']) > 0 ){ ?>
                <div style="padding: 60px 0 0 0">
                    <?=html_entity_decode($arParams['seo_page']['TEXT'])?>
                </div>
            <?php } ?>

        </div>


    <? } else {

        if( $cnt > $max_cnt ){  echo '<ost></ost>';  }

    }

} else { ?>

    <div style="padding:20px">
        <p class="no___count">В данном разделе / по данному фильтру товаров нет</p>
    </div>

<? } ?>