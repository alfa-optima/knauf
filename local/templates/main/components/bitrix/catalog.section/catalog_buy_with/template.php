<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (count($arResult['ITEMS']) > 0){ ?>

    <section class="sliderBlock  sliderBlock--related">
        <div class="sliderBlock__wrapper">

            <h2 class="sliderBlock__title">Связанные товары</h2>

            <div class="slider__wrapper">
                <div class="slider  ">

                    <? foreach ($arResult['ITEMS'] as $key => $arItem){

                        // catalog_item
                        $APPLICATION->IncludeComponent(
                            "aoptima:catalog_item", "catalog_slider",
                            array(
                                "LOC" => $arParams['LOC'],
                                'arItem' => $arItem,
                                'IS_DEALER' => $arParams['IS_DEALER']
                            )
                        );

                    } ?>

                </div>
            </div>

        </div>
    </section>

<? } ?>