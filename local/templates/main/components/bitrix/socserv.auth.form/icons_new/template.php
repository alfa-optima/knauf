<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
die(); 

$classes = array(
	'VKontakte' => 'form__social-item--vk',
	'Facebook' => 'form__social-item--fb',
    'Odnoklassniki' => 'form__social-item--ok'
);

//echo "<pre>"; print_r($arParams); echo "</pre>";

/*foreach($arParams["~AUTH_SERVICES"] as $service_code => $service){
	if( !$classes[$service_code] ){
		unset($arParams["~AUTH_SERVICES"][$service_code]);
	}
}*/ ?>


<div class="form__social">
    <p>Войти с&nbsp;помощью соцсетей</p>
    <div class="form__social-items">

        <? foreach($arParams["~AUTH_SERVICES"] as $service_code => $service){ ?>

            <a href="javascript:void(0)" class="form__social-item  <?=$classes[$service_code]?>" onclick="<?=$service['ONCLICK']?>"><span class="sr-only"><?=$service['NAME']?></span></a>

        <? } ?>

    </div>
</div>


