<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$this->setFrameMode(true); 

if( $_GET['ttt'] == 'ttt' ){
    //echo "<pre>"; print_r( $arResult['ITEMS'] ); echo "</pre>";
    //echo "<pre>"; print_r( $arParams['arFilter'] ); echo "</pre>";

}

?>

<?php //echo "<pre>"; print_r( $arResult ); echo "</pre>"; ?>


<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter filter" onsubmit="return false;">

    <div class="searchPanel__keywords">
        <input class="left___q" id="keywords" name="q" type="text" value="<?=strip_tags(trim($_GET['q']))?>">
    </div>

    <? foreach($arResult["HIDDEN"] as $key => $arItem){
        if( $key != 'q' ){ ?>
            <input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
        <? }
    } ?>

    <ul class="searchPanel-nav  searchPanel-filter">

        <? foreach( $arResult['ITEMS'] as $key => $arItem ){
            
            if( !$arItem['PRICE'] ){
                
                if(
                    in_array( $arItem['PROPERTY_TYPE'], [ 'L', 'S' ] )
                    &&
                    is_array( $arItem['VALUES'] )
                    &&
                    count( $arItem['VALUES'] ) > 0
                ){

                    $open = false;
                    foreach( $arItem["VALUES"] as $val => $ar ){
                        $checked = false;
                        if( $ar["CHECKED"] ){
                            $checked = true;
                        } else if( in_array($ar['FACET_VALUE'], $arParams['arFilter']['PROP_VALUES'][ $arItem['CODE'] ]) ){
                            $checked = true;
                        }
                        if( $checked ){   $open = true;   }
                    } ?>

                    <li class="searchPanel-filter__item <? if( $open ){ echo 'showSubMenu'; } ?>">

                        <a href="javascript:;" class="searchPanel-filter__item-link"><?=$arItem['NAME']?></a>

                        <div class="searchPanel-filter__block">

                            <? foreach( $arItem["VALUES"] as $val => $ar ){

                                $checked = false;
                                if( $ar["CHECKED"] ){
                                    $checked = true;
                                } else if( in_array($ar['FACET_VALUE'], $arParams['arFilter']['PROP_VALUES'][ $arItem['CODE'] ]) ){
                                    $checked = true;
                                } ?>

                                <div class="searchPanel-filter__field">
                                    <div class="account-form__checkbox">

                                        <input
                                            class="filter___checkbox"
                                            type="checkbox"
                                            value="<?=$ar["HTML_VALUE"]?>"
                                            name="<?=$ar["CONTROL_NAME"]?>"
                                            id="<?=$ar["CONTROL_ID"]?>"
                                            <?=$checked?'checked':''; ?>
                                        />

                                        <label for="<?=$ar["CONTROL_ID"]?>"><?=$ar['VALUE']?></label>

                                    </div>
                                </div>

                            <? } ?>

                        </div>
                    </li>

                <? } else if(
                    $arItem['PROPERTY_TYPE'] == 'N'
                    &&
                    $arItem['VALUES']['MIN']['VALUE'] != $arItem['VALUES']['MAX']['VALUE']
                ){

                    $open = false;
                    if(
                        $arItem['VALUES']['MIN']['HTML_VALUE']
                        ||
                        $arItem['VALUES']['MAX']['HTML_VALUE']
                    ){   $open = true;   } ?>

                    <li class="searchPanel-filter__item <? if( $open ){ echo 'showSubMenu'; } ?>">

                        <a href="javascript:;" class="searchPanel-filter__item-link"><?=$arItem['NAME']?></a>

                        <div class="searchPanel-filter__block">
                            <div class="priceSlider filterNumberBlock">

                                <div class="priceSlider__bar" id="filter_number_slider_<?=$arItem['ID']?>"></div>

                                <div class="priceSlider__inputs">

                                    <input name="<?=$arItem['VALUES']['MIN']['CONTROL_NAME']?>" id="<?=$arItem['VALUES']['MIN']['CONTROL_ID']?>" type="text" class="filter_number_input number___input sliderValue" data-index="0" default_value="<?=$arItem['VALUES']['MIN']['VALUE']?>" value="<?=$arItem['VALUES']['MIN']['HTML_VALUE']?$arItem['VALUES']['MIN']['HTML_VALUE']:$arItem['VALUES']['MIN']['VALUE']?>" />

                                    <span>—</span>

                                    <input name="<?=$arItem['VALUES']['MAX']['CONTROL_NAME']?>" id="<?=$arItem['VALUES']['MAX']['CONTROL_ID']?>" type="text" class="filter_number_input number___input sliderValue" data-index="1" value="<?=$arItem['VALUES']['MAX']['HTML_VALUE']?$arItem['VALUES']['MAX']['HTML_VALUE']:$arItem['VALUES']['MAX']['VALUE']?>" default_value="<?=$arItem['VALUES']['MAX']['VALUE']?>"/>

                                </div>

                            </div>
                        </div>

                    </li>

                    <script>
                    $(document).ready(function(){

                        $("#filter_number_slider_<?=$arItem['ID']?>").slider({
                            min: <?=$arItem['VALUES']['MIN']['VALUE']?>,
                            max: <?=$arItem['VALUES']['MAX']['VALUE']?>,
                            step: 1,
                            values: [<?=$arItem['VALUES']['MIN']['HTML_VALUE']?$arItem['VALUES']['MIN']['HTML_VALUE']:$arItem['VALUES']['MIN']['VALUE']?>, <?=$arItem['VALUES']['MAX']['HTML_VALUE']?$arItem['VALUES']['MAX']['HTML_VALUE']:$arItem['VALUES']['MAX']['VALUE']?>],
                            slide: function(event, ui) {
                                for (var i = 0; i < ui.values.length; ++i) {
                                    $("input.filter_number_input[data-index=" + i + "]").val(ui.values[i]);
                                }
                            },
                            change: function(event, ui) {
                                for (var i = 0; i < ui.values.length; ++i) {
                                    $("input.filter_number_input[data-index=" + i + "]").val(ui.values[i]);
                                }
                                filterGo();
                            },
                        });

                    })
                    </script>

                <? }
                
            }

        } ?>

    </ul>

    <div class="resetFilterBlock" style="text-align: center; display:none;">
        <a class="product-item__cartLink resetFilterButton to___process" style="cursor: pointer; margin: 25px; ">Сбросить</a>
    </div>

</form>


<script type="text/javascript">

var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);

$(document).ready(function(){
    updateResetButton();
})

</script>
