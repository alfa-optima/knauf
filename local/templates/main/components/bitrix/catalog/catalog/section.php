<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$obFilterPage = new project\filter_page();
$obFilterPage->AddPanelButton();

$user = new project\user();

$arURI = tools\funcs::arURI( tools\funcs::pureURL() );

$smart_filter_path = '';
if( substr_count( tools\funcs::pureURL(), '/filter/' ) ){
    if(preg_match("/\/filter\/(.+)\/apply\//", tools\funcs::pureURL(),$matches)){
        $smart_filter_path = rawurldecode( $matches[1] );
    }
    $filterKey = array_search( 'filter', $arURI );
    foreach ( $arURI as $key => $urlItem ){
        if( $key >= $filterKey ){   unset($arURI[$key]);   }
    }
    $arURI[] = false;
}



// Инфо о разделе
$section = tools\section::info_by_code($arResult['VARIABLES']['SECTION_CODE'], $arParams['IBLOCK_ID']);

if( $section["DEPTH_LEVEL"] > 1 ){
    global $APPLICATION;
    $APPLICATION->SetPageProperty('SHOW_BACK_BTN', '<a href="javascript:history.back();" class="backLink">Назад</a>');
}

if( intval($section['ID']) > 0 ){

    global $APPLICATION;
    $hide_catalog_left_menu = $APPLICATION->get_cookie('hide_catalog_left_menu');

    $obFilterPage = new project\filter_page();
    $filterPage = $obFilterPage->getByUrl( tools\funcs::pureURL() );

    // Если это SEO-страница фильтра
    if( intval( $filterPage['ID'] ) > 0 ){
        $h1 = $filterPage["UF_H1"];
        $seo_title = $filterPage["UF_META_TITLE"]?$filterPage["UF_META_TITLE"]:$filterPage["UF_H1"];
        $seo_description = $filterPage["UF_META_DESCRIPTION"]?$filterPage["UF_META_DESCRIPTION"]:$filterPage["UF_H1"];
        $seo_keywords = $filterPage["UF_META_KEYWORDS"]?$filterPage["UF_META_KEYWORDS"]:$filterPage["UF_H1"];
    // Иначе
    } else {
        $h1 = $section["UF_H1"]?$section["UF_H1"]:$section["NAME"];
        $seo_title = $section["UF_TITLE"]?$section["UF_TITLE"]:$section["NAME"];
        $seo_description = $section["UF_DESCRIPTION"]?$section["UF_DESCRIPTION"]:$section["NAME"];
        $seo_keywords = $section["UF_KEYWORDS"]?$section["UF_KEYWORDS"]:$section["NAME"];
    }

    $city = project\bx_location::cityPostfix( true );
    $seo_description = $seo_description.", купить с доставкой курьером или до пункта самовывоза ".$city." в маркетплейсе «Купи КНАУФ»";

    // Крошки
    foreach ( tools\section::chain($section["ID"]) as $sect ){
        $APPLICATION->AddChainItem($sect["NAME"], $sect["SECTION_PAGE_URL"]);
    }

    // Подразделы
    $subSects = tools\section::sub_sects($section['ID']);

    $catalogView = $_SESSION['catalogView']?$_SESSION['catalogView']:'grid'; ?>


	<?$this->SetViewTarget('catalog_footer');?>
		<div style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #807367;">На нашем маркетплейсе представлен полный каталог продукции КНАУФ. Вы можете купить <?=$section['NAME']?> <?=project\bx_location::cityPostfix()?>. Положите продукцию в корзину, получите перечень предложений от официальных дилеров <?=project\bx_location::cityPostfix()?>, сделайте выбор лучшего предложения и оформите заказ.</div>
	<?$this->EndViewTarget();?>


    <article class="block  block--top">
        <div class="block__wrapper">
            <h1 class="block__title seo___h1  block__title--search"><?=$h1?></h1>
        </div>
    </article>


    <section class="searchBlock <? if( $hide_catalog_left_menu == 'Y' ){ echo 'hideMenu'; } ?> <? if( $catalogView == 'list' && count($subSects)==0 ){ echo 'listView'; } ?>">


        <div class="searchPanel">

            <h3 class="searchPanel__title">Найти продукты
                <span class="searchPanel__toggle catalogLeftMenu">
                    <span class="sr-only">Скрыть/показать панель поиска</span>
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/arrow-left.png" alt="стрелка влево" class="searchPanel__toggle-image">
                </span>
            </h3>

            <div class="searchPanel__inner">


                <? // Фильтр
                $APPLICATION->IncludeComponent(
                    "bitrix:catalog.smart.filter", "catalog_filter",
                    array(
                        "IBLOCK_TYPE" => 'catalog',
                        "IBLOCK_ID" => project\catalog::catIblockID(),
                        "SECTION_ID" => $section['ID'],
                        "FILTER_NAME" => project\catalog::FILTER_NAME,
                        "PRICE_CODE" => [ 'BASE' ],
                        "CACHE_TYPE" => 'A',
                        "CACHE_TIME" => 3600,
                        "CACHE_GROUPS" => 'Y',
                        "SAVE_IN_SESSION" => "N",
                        "FILTER_VIEW_MODE" => 'VERTICAL',
                        "XML_EXPORT" => "N",
                        "SECTION_TITLE" => "NAME",
                        "SECTION_DESCRIPTION" => "DESCRIPTION",
                        'HIDE_NOT_AVAILABLE' => 'N',
                        "TEMPLATE_THEME" => "",
                        'CONVERT_CURRENCY' => 'N',
                        'CURRENCY_ID' => '',
                        "SEF_MODE" => 'Y',
                        "SEF_RULE" => '/catalog/#SECTION_CODE_PATH#/filter/#SMART_FILTER_PATH#/apply/',
                        "SMART_FILTER_PATH" => $smart_filter_path,
                        "PAGER_PARAMS_NAME" => '',
                        "INSTANT_RELOAD" => '',
                    ),
                    $component, array('HIDE_ICONS' => 'Y')
                ); ?>

            </div>
        </div>


        <div class="searchBlock__wrapper">

            <? // Если есть подкатегории
            if( count($subSects) > 0 && 1==2 ){ ?>


                <? // Подкатегории
                $APPLICATION->IncludeComponent(
                    "aoptima:catalog_subsections", "",
                    [
                        'section' => $section,
                        'subSects' => $subSects,
                        'IS_MOB_APP' => IS_ESHOP?"Y":"N"
                    ]
                ); ?>


            <? // Если нет подкатегорий
            } else { ?>

			
                <? // Подкатегории
                $APPLICATION->IncludeComponent(
                    "aoptima:catalog_subsections", "top_catalog",
                    [
                        'section' => $section,
                        'subSects' => $subSects,
                        'IS_MOB_APP' => IS_ESHOP?"Y":"N"
                    ]
                ); ?>


                <div class="searchBlock__filter">

                    <div class="searchBlock__filter-sort">
<!--                        <span>Сортировать по</span>-->
<!--                        <select name="sort_by">-->
<!--                            <option value="price" selected="selected">цене</option>-->
<!--                            <option value="name">названию</option>-->
<!--                        </select>-->
                    </div>

                    <div class="view-switcher">
                        <span class="view-switcher__text">Режим просмотра</span>
                        <div class="view-switcher__buttons">
                            <a href="javascript:;" class="view-switcher__button  view-switcher__button--grid"><span class="sr-only">Сетка</span></a>
                            <a href="javascript:;" class="view-switcher__button  view-switcher__button--list"><span class="sr-only">Список</span></a>
                        </div>
                    </div>

                </div>



                <?

                $sort1 = 'PROPERTY_HAS_OFFERS';
                $order1 = 'DESC';

                $sort2 = 'SORT';
                $order2 = 'ASC';

                $q = strip_tags(trim($_GET['q']));
                if( strlen($q) > 0 ){
                    // Поиск по элементам инфоблока
                    $search_ids = project\search::iblockElementsByQ($q, true);
                    if( count($search_ids) > 0 ){
                        $GLOBALS[project\catalog::FILTER_NAME]['ID'] = $search_ids;
                    } else {
                        $search_ids = project\search::iblockElementsByQ($q);
                        if( count($search_ids) > 0 ){
                            $GLOBALS[project\catalog::FILTER_NAME]['ID'] = $search_ids;
                        } else {
                            $GLOBALS[project\catalog::FILTER_NAME]['ID'] = array(0);
                        }
                    }
                }

                // фильтр по СД
                $GLOBALS[project\catalog::FILTER_NAME] = project\catalog::AddStopSDsToFilter($GLOBALS[project\catalog::FILTER_NAME]);

                //////////////////////////////////
                // Все дилеры
                $dealer = new project\dealer();
                $allDealers = $dealer->allList();
                //////////////////////////////////

                $intSectionID = $APPLICATION->IncludeComponent(
                    "bitrix:catalog.section", "catalog_goods",
                    array(
                        //////////////////////////////
                        'ALL_DEALERS' => $allDealers,
                        //////////////////////////////
                        'LOC' => $_SESSION['LOC'],
                        'LOC_CHAIN' => $_SESSION['LOC_CHAIN'],
                        'IS_DEALER' => $user->isDealer()?'Y':'N',
                        "PAGE_ELEMENT_COUNT" => project\catalog::GOODS_CNT + 1,
                        "INCLUDE_SUBSECTIONS" => 'Y',
                        "SHOW_ALL_WO_SECTION" => "Y",
                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "ELEMENT_SORT_FIELD" => $sort1,
                        "ELEMENT_SORT_ORDER" => $order1,
                        "ELEMENT_SORT_FIELD2" => $sort2,
                        "ELEMENT_SORT_ORDER2" => $order1,
                        "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                        "PROPERTY_CODE_MOBILE" => $arParams["LIST_PROPERTY_CODE_MOBILE"],
                        "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
                        "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
                        "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
                        "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                        "BASKET_URL" => $arParams["BASKET_URL"],
                        "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                        "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                        "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                        "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                        "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                        "FILTER_NAME" => project\catalog::FILTER_NAME,
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => $arParams["CACHE_TIME"],
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                        "SET_TITLE" => "N",
                        "MESSAGE_404" => $arParams["~MESSAGE_404"],
                        "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                        "SHOW_404" => $arParams["SHOW_404"],
                        "FILE_404" => $arParams["FILE_404"],
                        "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
                        "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                        "PRICE_CODE" => $arParams["~PRICE_CODE"],
                        "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                        "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
                        "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                        "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
                        "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
                        "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
                        "PRODUCT_PROPERTIES" => (isset($arParams["PRODUCT_PROPERTIES"]) ? $arParams["PRODUCT_PROPERTIES"] : []),
                        "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                        "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                        "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                        "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                        "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                        "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                        "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                        "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                        "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                        "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                        "LAZY_LOAD" => $arParams["LAZY_LOAD"],
                        "MESS_BTN_LAZY_LOAD" => $arParams["~MESS_BTN_LAZY_LOAD"],
                        "LOAD_ON_SCROLL" => $arParams["LOAD_ON_SCROLL"],
                        "OFFERS_CART_PROPERTIES" => (isset($arParams["OFFERS_CART_PROPERTIES"]) ? $arParams["OFFERS_CART_PROPERTIES"] : []),
                        "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
                        "OFFERS_PROPERTY_CODE" => ['LOCATION'],
                        "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                        "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                        "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                        "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                        "OFFERS_LIMIT" => 0,
                        "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
                        "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                        "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                        "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
                        "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
                        'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                        'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                        'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                        'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
                        'LABEL_PROP' => $arParams['LABEL_PROP'],
                        'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
                        'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
                        'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                        'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
                        'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
                        'PRODUCT_ROW_VARIANTS' => $arParams['LIST_PRODUCT_ROW_VARIANTS'],
                        'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
                        'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
                        'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
                        'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
                        'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',
                        'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                        'OFFER_TREE_PROPS' => (isset($arParams['OFFER_TREE_PROPS']) ? $arParams['OFFER_TREE_PROPS'] : []),
                        'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                        'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                        'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
                        'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                        'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
                        'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
                        'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
                        'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
                        'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
                        'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
                        'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
                        'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
                        'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
                        'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
                        'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),
                        'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
                        'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
                        'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),
                        'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
                        "ADD_SECTIONS_CHAIN" => "N",
                        'ADD_TO_BASKET_ACTION' => $basketAction,
                        'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                        'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
                        'COMPARE_NAME' => $arParams['COMPARE_NAME'],
                        'USE_COMPARE_LIST' => 'Y',
                        'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
                        'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
                        'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : '')
                    ), $component
                ); ?>


                <? $APPLICATION->SetPageProperty( "title", $seo_title );
                $APPLICATION->SetPageProperty( "description", $seo_description );
                $APPLICATION->SetPageProperty( "keywords", $seo_keywords ); ?>


            <? } ?>

        </div>


        <div class="seo_text" <? if( intval( $filterPage['ID'] ) > 0 && strlen($filterPage['UF_TEXT']) > 0 ){ ?><? } else {  echo 'style="display:none;"';  } ?>>
            <?=$filterPage['UF_TEXT']?>
        </div>


    </section>


    <input type="hidden" name="section_id" value="<?=$section['ID']?>">


<? } else {

   include($_SERVER["DOCUMENT_ROOT"]."/include/404include.php");

}
