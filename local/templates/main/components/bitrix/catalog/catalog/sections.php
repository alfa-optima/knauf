<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

//////////////////////////////////
// Все дилеры
$dealer = new project\dealer();
$allDealers = $dealer->allList();
//////////////////////////////////


if(
    tools\funcs::pureURL() == '/catalog/'
    ||
    tools\funcs::pureURL() == '/knauf_app/catalog/'
){


    $catalog_iblock_id = project\catalog::catIblockID();

    $tree = project\catalog::tree(($arParams['IS_MOB_APP'] == "Y"));
    $sections_1_level = $tree['sects_1_level'];

    $user = new project\user();
    ?>

    <? include $_SERVER['DOCUMENT_ROOT']."/include/catalog_head_block.php";?>



    <? foreach( $sections_1_level as $key => $section ){
        $subSections = $tree['sects_2_level'][$section['ID']]; ?>

        <section class="sliderBlock catalogSliderBlock anchor-section parentSectionBlock" id="section_<?=$section['ID']?>" item_id="<?=$section['ID']?>">

            <? $next_key = array_keys($sections_1_level)[array_search($key, array_keys($sections_1_level))+1];
            if( $sections_1_level[$next_key] ){ ?>
                <a class="scrollDownLink" href="#section_<?=$sections_1_level[$next_key]['ID']?>">Далее</a>
            <? } ?>

            <div class="sliderBlock__wrapper">

                <h2 class="sliderBlock__title">
                    <a href="<?=$section['SECTION_PAGE_URL']?>" style="color:#484846"><?=$section['NAME']?></a>
                </h2>

                <? if( count($subSections) > 0 ){ ?>

                    <div class="sliderBlock__tabs" <? if( count($subSections)==1 ){ echo 'style="display:none;"'; } ?>>
                        <dl class="dropdowns dropdown">
                            <dt><a><span><?=$subSections[ array_keys($subSections)[0] ]['NAME']?></span></a></dt>
                            <dd>
                                <ul class="tabs">

                                    <? $cnt = 0;
                                    foreach( $subSections as $subSect ){ $cnt++; ?>

                                        <li class="catalogSectLink is___catalog_tab to___process <? if( $cnt==1 ){ ?>active<? } ?>" url="<?=$subSect['SECTION_PAGE_URL']?>" item_id="<?=$subSect['ID']?>">
                                            <a style="cursor: pointer"><?=$subSect['NAME']?></a>
                                        </li>

                                    <? } ?>

                                </ul>
                            </dd>
                        </dl>
                        <script type="text/javascript">
                            dropDownHandler();
                        </script>
                    </div>

                    <div class="sliderBlock__sliders">

                        <? $cnt = 0;
                        foreach( $subSections as $subSect ){ $cnt++;
                            if( $cnt == 1 ){ ?>

                                <div class="slider__wrapper active" id="slide_subSect_<?=$subSect['ID']?>">

                                    <? $GLOBALS['catalog_slider']['SECTION_ID'] = $subSect['ID'];
                                    $GLOBALS['catalog_slider']['INCLUDE_SUBSECTIONS'] = 'Y';

                                    // фильтр по СД
                                    $GLOBALS['catalog_slider'] = project\catalog::AddStopSDsToFilter($GLOBALS['catalog_slider']);

                                    $APPLICATION->IncludeComponent(
                                        "bitrix:catalog.section", "catalog_slider_new",
                                        Array(
                                            //////////////////////////////
                                            'ALL_DEALERS' => $allDealers,
                                            //////////////////////////////
                                            "LOC" => $_SESSION['LOC'],
                                            'SECT_ID' => $subSect['ID'],
                                            'IS_DEALER' => $user->isDealer()?'Y':'N',
                                            "IBLOCK_TYPE" => "content",
                                            "IBLOCK_ID" => $catalog_iblock_id,
                                            "SECTION_USER_FIELDS" => array(),
                                            "ELEMENT_SORT_FIELD" => "NAME",
                                            "ELEMENT_SORT_ORDER" => "ASC",
                                            "ELEMENT_SORT_FIELD2" => "NAME",
                                            "ELEMENT_SORT_ORDER2" => "ASC",
                                            "FILTER_NAME" => "catalog_slider",
                                            "HIDE_NOT_AVAILABLE" => "N",
                                            "PAGE_ELEMENT_COUNT" => "30",
                                            "LINE_ELEMENT_COUNT" => "3",
                                            "PROPERTY_CODE" => array("ARTICLE","MOD","MODS_ARTICLE","ED","VIEW","EDGE_VIEW","VIDEO","RESIST_WATER","DRYING_TIME","FLAMMABILITY_GROUP","BASE_MATERIAL","RESIST_COLD","FLOOR_COATING","TARGET","MIXTURE_BASE","PERFORATION","ROOM","POP","PREIM","PRIM","BUY_WITH","SEE_WITH","PROPERTIES","APPL_METHOD","TECH","TYPE","TYPE_OBJECT","TYPE_OF_APPLICATION","TYPE_PROFILE","TYPE_COATING","LAYER_THICKNESS","FASOVKA","TILE","COLOR","WEIGHT","HEIGHT","LENGTH","DEPTH","WIDTH","CROSS","CALC_WEIGHT","MAX_GABARIT","HAS_OFFERS",""),
                                            "OFFERS_LIMIT" => "0",
                                            "OFFERS_PROPERTY_CODE" => ['LOCATION'],
                                            "TEMPLATE_THEME" => "",
                                            "PRODUCT_SUBSCRIPTION" => "N",
                                            "SHOW_DISCOUNT_PERCENT" => "N",
                                            "SHOW_OLD_PRICE" => "N",
                                            "MESS_BTN_BUY" => "Купить",
                                            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                                            "MESS_BTN_SUBSCRIBE" => "Подписаться",
                                            "MESS_BTN_DETAIL" => "Подробнее",
                                            "MESS_NOT_AVAILABLE" => "Нет в наличии",
                                            "SECTION_URL" => "",
                                            "DETAIL_URL" => "",
                                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                                            "AJAX_MODE" => "N",
                                            "AJAX_OPTION_JUMP" => "N",
                                            "AJAX_OPTION_STYLE" => "Y",
                                            "AJAX_OPTION_HISTORY" => "N",
                                            "CACHE_TYPE" => "A",
                                            "CACHE_TIME" => "36000000",
                                            "CACHE_GROUPS" => "Y",
                                            "SET_META_KEYWORDS" => "N",
                                            "META_KEYWORDS" => "",
                                            "SET_META_DESCRIPTION" => "N",
                                            "META_DESCRIPTION" => "",
                                            "BROWSER_TITLE" => "-",
                                            "ADD_SECTIONS_CHAIN" => "N",
                                            "DISPLAY_COMPARE" => "N",
                                            "SET_TITLE" => "N",
                                            "SET_STATUS_404" => "N",
                                            "CACHE_FILTER" => "Y",
                                            "PRICE_CODE" => array('BASE'),
                                            "USE_PRICE_COUNT" => "N",
                                            "SHOW_PRICE_COUNT" => "1",
                                            "PRICE_VAT_INCLUDE" => "Y",
                                            "CONVERT_CURRENCY" => "N",
                                            "BASKET_URL" => "/personal/basket.php",
                                            "ACTION_VARIABLE" => "action",
                                            "PRODUCT_ID_VARIABLE" => "id",
                                            "USE_PRODUCT_QUANTITY" => "N",
                                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                                            "PRODUCT_PROPS_VARIABLE" => "prop",
                                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                            "PRODUCT_PROPERTIES" => "",
                                            "PAGER_TEMPLATE" => "",
                                            "DISPLAY_TOP_PAGER" => "N",
                                            "DISPLAY_BOTTOM_PAGER" => "Y",
                                            "PAGER_TITLE" => "Товары",
                                            "PAGER_SHOW_ALWAYS" => "N",
                                            "PAGER_DESC_NUMBERING" => "N",
                                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                            "PAGER_SHOW_ALL" => "Y",
                                            "ADD_PICT_PROP" => "-",
                                            "LABEL_PROP" => "-",
                                            'INCLUDE_SUBSECTIONS' => "Y",
                                            'SHOW_ALL_WO_SECTION' => "Y"
                                        )
                                    ); ?>

                                </div>

                                <script>
                                    $(document).ready(function(){
                                        $('.bx-controls').each(function() {
                                            var t = $(this);
                                            var summ = 0;
                                            $('.bx-pager-item', t).each(function() {
                                                summ += $(this).width();
                                            });
                                            $('.bx-controls-direction', t).width(summ);
                                        });

                                    })
                                </script>

                            <? }
                        } ?>

                    </div>

                <? } ?>

            </div>
        </section>

    <? } ?>


    <script src="<?=SITE_TEMPLATE_PATH?>/js/sliders.js?v=<?=time()?>"></script>


<? } else {

    include($_SERVER["DOCUMENT_ROOT"]."/include/404include.php");

} ?>