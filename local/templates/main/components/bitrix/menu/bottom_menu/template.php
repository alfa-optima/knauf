<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult)){ ?>

    <div class="main-footer__column">
        <div class="main-footer__menu-item">

            <h2 class="main-footer__menu-subtitle"><?=$arParams['TITLE']?></h2>

            <ul class="main-footer__menu-list">

                <? foreach($arResult as $arItem){
                    if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                        continue; ?>

                    <li>
                        <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                    </li>

                <? } ?>

            </ul>

        </div>
    </div>

<? } ?>