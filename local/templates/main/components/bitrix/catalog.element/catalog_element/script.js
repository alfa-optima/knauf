$(document).ready(function () {

    $("#prod_cnt").change(function () {

        var $prodSize = $("#prod_size");
        var cnt = parseFloat($(this).val());
        var size;
        var size_one = parseFloat($prodSize.data("size"));

        if(!cnt)
        {
            size = size_one;
        }
        else {
            size = cnt * size_one;
        }

        $prodSize.val(size.toFixed(2));

    });

    $("#prod_size").change(function () {

        var size = parseFloat($(this).val());
        var size_one = parseFloat($(this).data("size"));
        var cnt;

        if(!size)
        {
            cnt = 1;
            $(this).val(size_one);
        }
        else {
            cnt = Math.ceil(size / size_one);
        }

        $("#prod_cnt").val(cnt);

    });

});