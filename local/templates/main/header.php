<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Page\Asset;   $asset = Asset::getInstance();

\Bitrix\Main\Loader::includeModule('aoptima.project');    use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools');    use AOptima\Tools as tools;

global $arURI;    $arURI = tools\funcs::arURI( tools\funcs::pureURL() );
$_SESSION['LOC'] = $KNAUF_LOC;
$_SESSION['LOC_CHAIN'] = project\bx_location::getLocChain($_SESSION['LOC']['CITY_LOC_ID']);
if( $arURI[1] != 'order' ){
    //unset($_SESSION['ORDER_POST']);
    unset($_SESSION['no_auth_order']);
}

$user = new project\user();

//project\catalog::checkArticleDoubles();

//////////////////////////////////
// Все дилеры
$dealer = new project\dealer();
$allDealers = $dealer->allList();
//////////////////////////////////

?><!DOCTYPE html>
<html lang="ru">
<head>

	<!--mindbox-->
	<script>
		mindbox = window.mindbox || function() { mindbox.queue.push(arguments); };
		mindbox.queue = mindbox.queue || [];
		mindbox('create');
	</script>
	<script src="https://api.mindbox.ru/scripts/v1/tracker.js" async></script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NQBZQD7');</script>
    <!-- End Google Tag Manager -->

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="icon" href="/favicon.ico" type="image/x-icon" />

    <? if( tools\funcs::isMain() ){ ?>

        <title>Маркетплейс КНАУФ <?=project\bx_location::cityPostfix( true )?> | Маркетплейс «Купи КНАУФ»</title>

    <? } else { ?>

        <title><?$APPLICATION->ShowTitle()?> <?=project\bx_location::cityPostfix()?> | Маркетплейс «Купи КНАУФ»</title>

    <? } ?>

    <? // CSS
    $asset->addCss(SITE_TEMPLATE_PATH."/css/jquery-ui.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/style.css?v=1");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/jquery.bxslider.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/jquery.fancybox.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/jquery.fancybox-buttons.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/jquery.fancybox-thumbs.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/tooltipster.bundle.min.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/my.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/buttons.css"); ?>

    <!-- JS -->
    <script type="text/javascript">
    var user = {
        IS_AUTH: <?=$USER->IsAuthorized()?'"Y"':'"N"'?>
    };
    var cityPostfix = '<?=project\bx_location::cityPostfix()?>';
    var reloadDeliveryTemplatesProcess = 'N';
    var orderReloadProcess = 'N';
    var points_map, shop_markers = [], markerCluster, modal_map, modal_map_marker = false, coords_input;
    var isMobApp = 'N';
    </script>

    <? // JS
    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquerymin.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/json.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquery-ui.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquery.cookie.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquery.mask.min.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquery.bxslider.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquery.fancybox.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquery.fancybox-thumbs.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquery.fancybox-buttons.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/tooltipster.bundle.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/default_scripts.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/noty/packaged/jquery.noty.packaged.min.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquery.cookie.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/aoptima/personal.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/aoptima/personalDealer.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/aoptima/register.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/aoptima/shop.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/aoptima/reviews.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/aoptima/subscribe.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/aoptima/datepicker.js");

    if(
        $arURI[1] == 'dealers'
        ||
        $arURI[1] == 'selling_points'
        ||
        ( $arURI[1] == 'personal' && $USER->IsAuthorized() )
    ){
        $asset->addJs("//maps.googleapis.com/maps/api/js?key=AIzaSyBTl4HKkdHZH0Dx_aqzcll8RI1dYb9W1us&libraries=places");
        $asset->addJs("//developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js");
    } ?>

    <?$APPLICATION->ShowHead()?>

    <script>
    !function (w, d, t) {
        w.TiktokAnalyticsObject=t;var ttq=w[t]=w[t]||[];ttq.methods=["page","track","identify","instances","debug","on","off","once","ready","alias","group","enableCookie","disableCookie"],ttq.setAndDefer=function(t,e){t[e]=function(){t.push([e].concat(Array.prototype.slice.call(arguments,0)))}};for(var i=0;i<ttq.methods.length;i++)ttq.setAndDefer(ttq,ttq.methods[i]);ttq.instance=function(t){for(var e=ttq._i[t]||[],n=0;n<ttq.methods.length;n++
        )ttq.setAndDefer(e,ttq.methods[n]);return e},ttq.load=function(e,n){var i="https://analytics.tiktok.com/i18n/pixel/events.js";ttq._i=ttq._i||{},ttq._i[e]=[],ttq._i[e]._u=i,ttq._t=ttq._t||{},ttq._t[e]=+new Date,ttq._o=ttq._o||{},ttq._o[e]=n||{};n=document.createElement("script");n.type="text/javascript",n.async=!0,n.src=i+"?sdkid="+e+"&lib="+t;e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(n,e)};

        ttq.load('C6L02LS4L0UCTH07N2Q0');
        ttq.page();
    }(window, document, 'ttq');
    </script>

</head>


<body class="logged" data-app="0">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NQBZQD7" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="panel"><?$APPLICATION->ShowPanel();?></div>




<header class="main-header">

    <nav class="main-nav">

        <a href="/" class="main-nav__logo">
            <img src="<?=SITE_TEMPLATE_PATH?>/images/logo2.png" alt="Knauf">
        </a>

        <a href="https://www.knauf.ru/" class="main-nav__mainLogo mainSiteLink" onclick="ym(55379437, 'reachGoal', 'headerGoToParentKnauf'); return true;" target="_blank">
            <img src="<?=SITE_TEMPLATE_PATH?>/images/main-site-logo-grey.png">
        </a>

        <a href="javascript:;"  onclick="$('.modal-cities__link').fancybox({
        padding: 0}) .trigger('click');" class="main-nav__geo"></a>

        <a href="javascript:;" class="main-nav__menuButton-mobile">Меню</a>

        <ul class="main-nav__items">

            <li class="main-nav__item  main-nav__item--close yesMobile"><a href="javascript:;" class="main-nav__item-link"><span class="sr-only">Закрыть</span></a></li>

            <li class="main-nav__item  main-nav__item--mainLogo yesMobile"><a href="https://www.knauf.ru/" class="main-nav__item-link" target="_blank">knauf.ru</a></li>

            <li class="main-nav__item  main-nav__item--index yesMobile"><a href="/" class="main-nav__item-link">Главная</a></li>

            <? // Верхнее меню
            $APPLICATION->IncludeComponent(
                "bitrix:menu",  "top_menu", // Шаблон меню
                Array(
                    "ROOT_MENU_TYPE" => "top",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "CACHE_SELECTED_ITEMS" => "N",
                    "MENU_CACHE_GET_VARS" => array(""),
                    "MAX_LEVEL" => "2",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                )
            );?>

            <li class="main-nav__item  main-nav__item--login">

                <? if( !$USER->IsAuthorized() ){ ?>
                    <a href="/auth/" class="main-nav__item-link" >Вход</a>
                <? } else { ?>
                    <a href="/out.php?to=<?=$_SERVER['REQUEST_URI']?>" class="main-nav__item-link  main-nav__item-link--logout">Выход</a>
                <? } ?>

            </li>

        </ul>

        <a href="/personal/" class="main-nav__account <?=tools\funcs::arURI()[1]=='personal'?'main-nav__account--active':''?>"><span class="sr-only">Аккаунт</span></a>

        <? // basketSmall
        $APPLICATION->IncludeComponent(
            "aoptima:basketSmall", ""
        ); ?>

        <div class="main-nav__search">
            <a href="javascript:;"><span class="sr-only">Поиск</span></a>
            <form action="/search/" method="GET">
                <input type="text" name="q" id="search" maxlength="50" placeholder="Ключевые слова"><button type="submit">OK</button>
            </form>
        </div>

    </nav>


    <? // beta_noty_block
    $APPLICATION->IncludeComponent(
        "aoptima:beta_noty_block", ""
    ); ?>



    <? if( !tools\funcs::isMain() ){

        global $APPLICATION;
        $APPLICATION->ShowProperty('SHOW_BACK_BTN');

        // Хлебные крошки
        $APPLICATION->IncludeComponent(
            "bitrix:breadcrumb", "breadcrumb",
            Array( "START_FROM" => "0", "PATH" => "", "SITE_ID" => "s1" )
        );

    } ?>


	<? if ($APPLICATION->GetCurPage(false) !== '/order/'): ?>

			<a href="javascript:;" onclick="howItWorksOpen(); ym(55379437,'reachGoal','HowToWork');" class="howItWorks-btn"><img src="<?echo SITE_TEMPLATE_PATH?>/images/howitworks-btn.png" alt=""><span>Как работать с&nbsp;сервисом</span></a>

	<?endif;?>
</header>

<main class="hidden">

    <aside>
        <div class="anchor-menu">
            <ul class="anchor-menu__items">
                <li class="anchor-menu__item">
                    <a href="<?=\Bitrix\Main\Config\Option::get('aoptima.project', 'YOUTUBE_LINK')?>" class="anchor-menu__socialBtn  anchor-menu__socialBtn--youtube"></a>
                </li>
                <li class="anchor-menu__item">
                    <a href="<?=\Bitrix\Main\Config\Option::get('aoptima.project', 'TWITTER_LINK')?>" class="anchor-menu__socialBtn  anchor-menu__socialBtn--twitter"></a>
                </li>
                <li class="anchor-menu__item">
                    <a href="<?=\Bitrix\Main\Config\Option::get('aoptima.project', 'FACEBOOK_LINK')?>" class="anchor-menu__socialBtn  anchor-menu__socialBtn--facebook"></a>
                </li>
				<li class="anchor-menu__item">
					<a href="https://vk.com/knauf_rus" class="anchor-menu__socialBtn" style="background-position: -10px -1193px";></a>
                </li>
            </ul>
        </div>
    </aside>

    <? // Главная страница
    if( tools\funcs::isMain() ){ ?>


        <? //include $_SERVER['DOCUMENT_ROOT']."/include/main_page_head_block.php";?>

        <?
        $APPLICATION->IncludeComponent(
            "aoptima:mainBanner", "",
            Array()
        );
        ?>

		<? include $_SERVER['DOCUMENT_ROOT']."/include/main_page_welcome_block.php";?>
		
		
		<?//в центре внимания?>
			<?
			global $arrFilter;
			$arrFilter = array(
					"!PROPERTY_ATTENTION"=>false,
			   ); 
			?>
			
			<?$APPLICATION->IncludeComponent(
                "bitrix:catalog.section", "catalog_main_attention",
                Array(
                    //////////////////////////////
                    'ALL_DEALERS' => $allDealers,
                    //////////////////////////////
                    "LOC" => $_SESSION['LOC'],
                    //"IDS" => $popular_goods,
                    'IS_DEALER' => $user->isDealer()?'Y':'N',
                    "IBLOCK_TYPE" => "content",
                    "IBLOCK_ID" => project\catalog::catIblockID(),
                    "SECTION_USER_FIELDS" => array(),
                    "ELEMENT_SORT_FIELD" => 'RAND',
                    "ELEMENT_SORT_ORDER" => 'ASC',
                    "ELEMENT_SORT_FIELD2" => 'RAND',
                    "ELEMENT_SORT_ORDER2" => 'ASC',
                    "FILTER_NAME" => "arrFilter",
                    "HIDE_NOT_AVAILABLE" => "N",
                    "PAGE_ELEMENT_COUNT" => 4,
                    "LINE_ELEMENT_COUNT" => "3",
                    "PROPERTY_CODE" => array("ARTICLE","MOD","MODS_ARTICLE","ED","VIEW","EDGE_VIEW","VIDEO","RESIST_WATER","DRYING_TIME","FLAMMABILITY_GROUP","BASE_MATERIAL","RESIST_COLD","FLOOR_COATING","TARGET","MIXTURE_BASE","PERFORATION","ROOM","POP","PREIM","PRIM","BUY_WITH","SEE_WITH","PROPERTIES","APPL_METHOD","TECH","TYPE","TYPE_OBJECT","TYPE_OF_APPLICATION","TYPE_PROFILE","TYPE_COATING","LAYER_THICKNESS","FASOVKA","TILE","COLOR","WEIGHT","HEIGHT","LENGTH","DEPTH","WIDTH","CROSS","CALC_WEIGHT","MAX_GABARIT","HAS_OFFERS",""),
                    "OFFERS_LIMIT" => "0", "OFFERS_PROPERTY_CODE" => ['LOCATION'], "TEMPLATE_THEME" => "", "PRODUCT_SUBSCRIPTION" => "N", "SHOW_DISCOUNT_PERCENT" => "N", "SHOW_OLD_PRICE" => "N", "MESS_BTN_BUY" => "Купить", "MESS_BTN_ADD_TO_BASKET" => "В корзину", "MESS_BTN_SUBSCRIBE" => "Подписаться", "MESS_BTN_DETAIL" => "Подробнее", "MESS_NOT_AVAILABLE" => "Нет в наличии", "SECTION_URL" => "", "DETAIL_URL" => "", "SECTION_ID_VARIABLE" => "SECTION_ID", "AJAX_MODE" => "N", "AJAX_OPTION_JUMP" => "N", "AJAX_OPTION_STYLE" => "Y", "AJAX_OPTION_HISTORY" => "N", "CACHE_TYPE" => "A", "CACHE_TIME" => "36000000", "CACHE_GROUPS" => "Y", "SET_META_KEYWORDS" => "N", "META_KEYWORDS" => "", "SET_META_DESCRIPTION" => "N", "META_DESCRIPTION" => "", "BROWSER_TITLE" => "-", "ADD_SECTIONS_CHAIN" => "N", "DISPLAY_COMPARE" => "N", "SET_TITLE" => "N", "SET_STATUS_404" => "N", "CACHE_FILTER" => "Y", "PRICE_CODE" => array('BASE'), "USE_PRICE_COUNT" => "N", "SHOW_PRICE_COUNT" => "1", "PRICE_VAT_INCLUDE" => "Y", "CONVERT_CURRENCY" => "N", "BASKET_URL" => "/personal/basket.php", "ACTION_VARIABLE" => "action", "PRODUCT_ID_VARIABLE" => "id", "USE_PRODUCT_QUANTITY" => "N", "ADD_PROPERTIES_TO_BASKET" => "Y", "PRODUCT_PROPS_VARIABLE" => "prop", "PARTIAL_PRODUCT_PROPERTIES" => "N", "PRODUCT_PROPERTIES" => "", "PAGER_TEMPLATE" => "", "DISPLAY_TOP_PAGER" => "N", "DISPLAY_BOTTOM_PAGER" => "Y", "PAGER_TITLE" => "Товары", "PAGER_SHOW_ALWAYS" => "N", "PAGER_DESC_NUMBERING" => "N", "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", "PAGER_SHOW_ALL" => "Y", "ADD_PICT_PROP" => "-", "LABEL_PROP" => "-", 'INCLUDE_SUBSECTIONS' => "Y", 'SHOW_ALL_WO_SECTION' => "Y"
                )
            );?>


        <? // Популярные товары
        $popular_goods = project\popular_goods::getIDS();
        if( count($popular_goods) > 0 ){
            $GLOBALS['main_pop']['ID'] = $popular_goods;

            // фильтр по СД
            $GLOBALS['main_pop'] = project\catalog::AddStopSDsToFilter($GLOBALS['main_pop']);

            $APPLICATION->IncludeComponent(
                "bitrix:catalog.section", "catalog_main_pop",
                Array(
                    //////////////////////////////
                    'ALL_DEALERS' => $allDealers,
                    //////////////////////////////
                    "LOC" => $_SESSION['LOC'],
                    "IDS" => $popular_goods,
                    'IS_DEALER' => $user->isDealer()?'Y':'N',
                    "IBLOCK_TYPE" => "content",
                    "IBLOCK_ID" => project\catalog::catIblockID(),
                    "SECTION_USER_FIELDS" => array(),
                    "ELEMENT_SORT_FIELD" => 'RAND',
                    "ELEMENT_SORT_ORDER" => 'ASC',
                    "ELEMENT_SORT_FIELD2" => 'RAND',
                    "ELEMENT_SORT_ORDER2" => 'ASC',
                    "FILTER_NAME" => "main_pop",
                    "HIDE_NOT_AVAILABLE" => "N",
                    "PAGE_ELEMENT_COUNT" => count($popular_goods),
                    "LINE_ELEMENT_COUNT" => "3",
                    "PROPERTY_CODE" => array("ARTICLE","MOD","MODS_ARTICLE","ED","VIEW","EDGE_VIEW","VIDEO","RESIST_WATER","DRYING_TIME","FLAMMABILITY_GROUP","BASE_MATERIAL","RESIST_COLD","FLOOR_COATING","TARGET","MIXTURE_BASE","PERFORATION","ROOM","POP","PREIM","PRIM","BUY_WITH","SEE_WITH","PROPERTIES","APPL_METHOD","TECH","TYPE","TYPE_OBJECT","TYPE_OF_APPLICATION","TYPE_PROFILE","TYPE_COATING","LAYER_THICKNESS","FASOVKA","TILE","COLOR","WEIGHT","HEIGHT","LENGTH","DEPTH","WIDTH","CROSS","CALC_WEIGHT","MAX_GABARIT","HAS_OFFERS",""),
                    "OFFERS_LIMIT" => "0", "OFFERS_PROPERTY_CODE" => ['LOCATION'], "TEMPLATE_THEME" => "", "PRODUCT_SUBSCRIPTION" => "N", "SHOW_DISCOUNT_PERCENT" => "N", "SHOW_OLD_PRICE" => "N", "MESS_BTN_BUY" => "Купить", "MESS_BTN_ADD_TO_BASKET" => "В корзину", "MESS_BTN_SUBSCRIBE" => "Подписаться", "MESS_BTN_DETAIL" => "Подробнее", "MESS_NOT_AVAILABLE" => "Нет в наличии", "SECTION_URL" => "", "DETAIL_URL" => "", "SECTION_ID_VARIABLE" => "SECTION_ID", "AJAX_MODE" => "N", "AJAX_OPTION_JUMP" => "N", "AJAX_OPTION_STYLE" => "Y", "AJAX_OPTION_HISTORY" => "N", "CACHE_TYPE" => "A", "CACHE_TIME" => "36000000", "CACHE_GROUPS" => "Y", "SET_META_KEYWORDS" => "N", "META_KEYWORDS" => "", "SET_META_DESCRIPTION" => "N", "META_DESCRIPTION" => "", "BROWSER_TITLE" => "-", "ADD_SECTIONS_CHAIN" => "N", "DISPLAY_COMPARE" => "N", "SET_TITLE" => "N", "SET_STATUS_404" => "N", "CACHE_FILTER" => "Y", "PRICE_CODE" => array('BASE'), "USE_PRICE_COUNT" => "N", "SHOW_PRICE_COUNT" => "1", "PRICE_VAT_INCLUDE" => "Y", "CONVERT_CURRENCY" => "N", "BASKET_URL" => "/personal/basket.php", "ACTION_VARIABLE" => "action", "PRODUCT_ID_VARIABLE" => "id", "USE_PRODUCT_QUANTITY" => "N", "ADD_PROPERTIES_TO_BASKET" => "Y", "PRODUCT_PROPS_VARIABLE" => "prop", "PARTIAL_PRODUCT_PROPERTIES" => "N", "PRODUCT_PROPERTIES" => "", "PAGER_TEMPLATE" => "", "DISPLAY_TOP_PAGER" => "N", "DISPLAY_BOTTOM_PAGER" => "Y", "PAGER_TITLE" => "Товары", "PAGER_SHOW_ALWAYS" => "N", "PAGER_DESC_NUMBERING" => "N", "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", "PAGER_SHOW_ALL" => "Y", "ADD_PICT_PROP" => "-", "LABEL_PROP" => "-", 'INCLUDE_SUBSECTIONS' => "Y", 'SHOW_ALL_WO_SECTION' => "Y"
                )
            );
        } ?>


        <? // mainPageCtegoriesNew
        $APPLICATION->IncludeComponent(
            "aoptima:mainPageCtegoriesNew", ""
        ); ?>


        <? if( 1==2 ){ ?>

            <section>

                <div class="bcgBlock bcgBlock--indexMaterials" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/images/blockbcg2.jpg);">
                    <div class="bcgBlock__wrapper">
                        <h2 class="bcgBlock__title">СТРОИТЕЛЬНО-ОТДЕЛОЧНЫЕ МАТЕРИАЛЫ</h2>
                        <a href="/catalog/" class="bcgBlock__link"><span>Подробнее</span></a>
                    </div>
                </div>

                <? // Разделы
                $APPLICATION->IncludeComponent(
                    "bitrix:catalog.section.list",   "main_sections",
                    Array(
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => project\catalog::catIblockID(),
                        "COUNT_ELEMENTS" => "N",
                        "TOP_DEPTH" => 1,
                        "SECTION_FIELDS" => array(),
                        "SECTION_USER_FIELDS" => array(),
                        "VIEW_MODE" => "LIST",
                        "SHOW_PARENT_NAME" => "Y",
                        "SECTION_URL" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_GROUPS" => "Y",
                        "ADD_SECTIONS_CHAIN" => "N"
                    )
                ); ?>

            </section>

        <? } ?>





    <? } else if( project\funcs::isContentPage() ){ ?>


        <article class="block  block--top">
            <div class="block__wrapper">

                <h2 class="block__title  block__title--faq"><?$APPLICATION->ShowTitle()?></h2>



    <? } ?>
