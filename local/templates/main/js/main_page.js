$('document').ready(function() {

    var initSlider = function(sliderSelector) {

        var slider = $(sliderSelector);

        var obxSettings = {
            slideWidth: 204,
            minSlides: 1,
            maxSlides: 4,
            moveSlides: 1,
            infiniteLoop: false,
            controls: true,
            touchEnabled: false,
            slideMargin: 26
        };

        var resizeSlider = function() {

            var windowWidth = $(window).width();
            if (windowWidth < 1001 && windowWidth > 570) {
                obxSettings.minSlides = 2;
                obxSettings.maxSlides = 2;
                obxSettings.controls = true;

            } else if (windowWidth < 571) {
                obxSettings.minSlides = 2;
                obxSettings.maxSlides = 2;
                obxSettings.controls = false;
                obxSettings.slideWidth = 270;

            } else {
                obxSettings.minSlides = 4;
                obxSettings.maxSlides = 4;
                obxSettings.controls = true;
            }


        }

        resizeSlider();

        slider.bxSlider(obxSettings);

        var summ = 0;
        $('.bx-pager-item').each(function() {
            summ += 1 * $(this).width();
        });
        $('.bx-controls-direction').width(summ);

        $(window).resize(function() {
            resizeSlider();
            slider.reloadSlider();
            var summ = 0;
            $('.bx-pager-item').each(function() {
                summ += 1 * $(this).width();
            });
            $('.bx-controls-direction').width(summ);
        });
    }
    initSlider('.slider--products');
    initSlider('.slider--materials');
});