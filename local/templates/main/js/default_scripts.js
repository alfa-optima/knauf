var loadedJQModules = new Array();

// scripts

// default
var layout_direction = "ltr";


/*!
 * JavaScript Cookie v2.1.3
 * https://github.com/js-cookie/js-cookie
 *
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
 * Released under the MIT license
 */
! function(a) {
    var b = !1;
    if ("function" == typeof define && define.amd && (define(a), b = !0), "object" == typeof exports && (module.exports = a(), b = !0), !b) {
        var c = window.Cookies,
            d = window.Cookies = a();
        d.noConflict = function() { return window.Cookies = c, d }
    }
}(function() {
    function a() { for (var a = 0, b = {}; a < arguments.length; a++) { var c = arguments[a]; for (var d in c) b[d] = c[d] } return b }

    function b(c) {
        function d(b, e, f) {
            var g;
            if ("undefined" != typeof document) {
                if (arguments.length > 1) {
                    if (f = a({ path: "/" }, d.defaults, f), "number" == typeof f.expires) {
                        var h = new Date;
                        h.setMilliseconds(h.getMilliseconds() + 864e5 * f.expires), f.expires = h
                    }
                    try { g = JSON.stringify(e), /^[\{\[]/.test(g) && (e = g) } catch (a) {}
                    return e = c.write ? c.write(e, b) : encodeURIComponent(String(e)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent), b = encodeURIComponent(String(b)), b = b.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent), b = b.replace(/[\(\)]/g, escape), document.cookie = [b, "=", e, f.expires ? "; expires=" + f.expires.toUTCString() : "", f.path ? "; path=" + f.path : "", f.domain ? "; domain=" + f.domain : "", f.secure ? "; secure" : ""].join("")
                }
                b || (g = {});
                for (var i = document.cookie ? document.cookie.split("; ") : [], j = /(%[0-9A-Z]{2})+/g, k = 0; k < i.length; k++) {
                    var l = i[k].split("="),
                        m = l.slice(1).join("=");
                    '"' === m.charAt(0) && (m = m.slice(1, -1));
                    try {
                        var n = l[0].replace(j, decodeURIComponent);
                        if (m = c.read ? c.read(m, n) : c(m, n) || m.replace(j, decodeURIComponent), this.json) try { m = JSON.parse(m) } catch (a) {}
                        if (b === n) { g = m; break } b || (g[n] = m)
                    } catch (a) {}
                }
                return g
            }
        }
        return d.set = d, d.get = function(a) { return d.call(d, a) }, d.getJSON = function() { return d.apply({ json: !0 }, [].slice.call(arguments)) }, d.defaults = {}, d.remove = function(b, c) { d(b, "", a(c, { expires: -1 })) }, d.withConverter = b, d
    }
    return b(function() {})
});

// scripts
var maxSize = 1153,
    mobileSize = 1e3,
    tabSize = 570,
    headerHeight = 84,
    sUa = navigator.userAgent,
    dropDownHandlerInitialized = !1,
    targetGroups = new Array,
    breadcrumbPath = new Array,
    navigationDelayTime = 800;

$(document).ready(function() {

    if ($(window).width() < 1000) {
        $('.dropdown dd ul').css('display', 'none');
    }

    ieStartups(),
        createContextNav(),
        smoothScrollInit(),
        mobileContextNav(),
        animateContextNav(),
        dropdownInit(),
        responsiveProductTable(),
        rebuildSearchMenu(),
        contactFlyout(),
        newsletterFlyout(),
        styleForms(),
        jumpListInit(),
        getHeaderHeight(),
        fancyboxReady(),
        imageOverlayChangerReady(),
        create_fake_accordion_2(),
        iframe_popup(),
        replace_svg_to_inline(),
        fade_element(),
        special_mobil_viewport(),
        video_viewport(),
        facebook_helper(),
        resize_frame_helper(),
        "undefined" != typeof use_new_tg ? load_TargetGroupTemplate() : tgFlyoutReady(),
        printCSS(),
        countdown_teaser_function(),
    -1 != $.inArray("pointer_events_polyfill", loadedJQModules) && PointerEventsPolyfill.initialize({}),
        onPageJumps(),
        hashJumps(),
        compass_hack()

}),

    $(window).load(function() {
        0 < $(".simpleSlider").length || new_carouselLoad(), init_diyhaus_banner()
    }),

    function(t) {
        function i() { l.setAttribute("content", h), u = !0 }
        var e = navigator.userAgent;
        if (/iPhone|iPad|iPod/.test(navigator.platform) && /OS [1-5]_[0-9_]* like Mac OS X/i.test(e) && -1 < e.indexOf("AppleWebKit")) {
            var n = t.document;
            if (n.querySelector) {
                var a, o, r, s, l = n.querySelector("meta[name=viewport]"),
                    c = l && l.getAttribute("content"),
                    d = c + ",maximum-scale=1",
                    h = c + ",maximum-scale=10",
                    u = !0;
                l && (t.addEventListener("orientationchange", i, !1), t.addEventListener("devicemotion", function(e) { s = e.accelerationIncludingGravity, a = Math.abs(s.x), o = Math.abs(s.y), r = Math.abs(s.z), t.orientation && 180 !== t.orientation || !(7 < a || (6 < r && o < 8 || r < 8 && 6 < o) && 5 < a) ? u || i() : u && (l.setAttribute("content", d), u = !1) }, !1))
            }
        }
    }(this);
var isAtLeastIE11 = !(!sUa.match(/Trident/) || sUa.match(/MSIE/));
/MSIE/i.test(sUa.toLowerCase()) || isAtLeastIE11 ? ($("html").addClass("isIE"), $("html").hasClass("lt-ie10") || $("html").addClass("gt-ie9"), Function("/*@cc_on return document.documentMode===10@*/")() && $("html").addClass("ie10")) : $("html").addClass("no-ie");
var noShow = !1;

function ieStartups() { $("html").hasClass("lt-ie7") && (noShow = !0), $("html").hasClass("lt-ie10") && jQuery(window).width() > mobileSize && (columnizerInit("header.plainTextTwoCols", 2, 26), columnizerInit(".jumpList", 2, 26)) }

function columnizerInit(e, t, i) {
    -1 != $.inArray("waypoints", loadedJQModules) && $(e).each(function(e) {
        if (!$(this).is(":visible")) return !0;
        $(this).columnize({ columns: 2 }), $(".column:not(.first)", this).css("margin-left", i + "px"), width = $(this).width(), 0 == width && (width = $(this).parentsUntil(".wrapperinner").parent().width()), widthOfCol = width * (100 / t / 100) - i / 2, $(".column", this).width(widthOfCol)
    })
}
var contextnav_id_array = new Array,
    contextnav_name_array = new Array,
    full_width = $(window).width();

function createContextNav() {
    if ($("section.contextnav").each(function() { contextnav_id_array.push($(this).attr("id")), contextnav_name_array.push($(this).attr("title")) }), contextnav_id_array.reverse(), contextnav_name_array.reverse(), 1 < contextnav_id_array.length)
        for (var e = 0; e < contextnav_id_array.length; e++) builded_content = '<li id="nav' + contextnav_id_array[e] + '" class="contextnavelement">', builded_content += "<span><strong>" + contextnav_name_array[e] + "</strong></span>", builded_content += '<a href="#' + contextnav_id_array[e] + '"></a></li>', $("#contextNav > ul").prepend(builded_content);
    set_contextnav(), positionContextNavElements(),

        $(window).resize(function() {
            set_contextnav(),
                positionContextNavElements(),
                contextNav_rebuild()
        });

    $("section").removeAttr("title");

}

function scrollbar_width() {
    if (jQuery("body").height() > jQuery(window).height()) {
        var e = jQuery('<div style="width:50px;height:50px;overflow:hidden;position:absolute;top:-200px;left:-200px;"><div style="height:100px;"></div>');
        jQuery("body").append(e);
        var t = jQuery("div", e).innerWidth();
        e.css("overflow-y", "scroll");
        var i = jQuery("div", e).innerWidth();
        return jQuery(e).remove(), t - i
    }
    return 0
}

function positionContextNavElements() {
    for (var e = 0; e < contextnav_id_array.length; e++)
        if (tabSize < full_width) {
            $("#contextNav ul").show();
            var t = parseInt($("#nav" + contextnav_id_array[e] + " span").css("width"));
            $("html").hasClass("isIE"), "rtl" === layout_direction ? $("#nav" + contextnav_id_array[e] + " span").css("margin-right", "-" + t + "px") : $("#nav" + contextnav_id_array[e] + " span").css("margin-left", "-" + t + "px")
        } else $("#contextNav a").hasClass("opened") || $("#contextNav ul").hide(), "rtl" === layout_direction ? $("#nav" + contextnav_id_array[e] + " span").css("margin-right", "0") : $("#nav" + contextnav_id_array[e] + " span").css("margin-left", "0")
}

function set_contextnav() { full_width = $(window).width() + scrollbarWidth(), $("#contextNav").show(), full_width <= maxSize && tabSize < full_width ? "rtl" === layout_direction ? $("#contextNav").css("left", "24px") : $("#contextNav").css("right", "24px") : maxSize < full_width ? (einruecken = (full_width - maxSize) / 2 + 24, "rtl" === layout_direction ? $("#contextNav").css("left", einruecken + "px") : (einruecken = (full_width - maxSize) / 2 - 35, $("#contextNav").css("right", einruecken + "px"))) : 0 === contextnav_id_array.length && 0 === $("#contextNav #navLanguage").length ? $("#contextNav").hide() : "rtl" === layout_direction ? $("#contextNav").css("left", "0px") : $("#contextNav").css("right", "0px") }

function smoothScrollInit() {-1 != $.inArray("smoothscroll", loadedJQModules) && ($("#contextNav a").smoothScroll(), $(".viewportLink").smoothScroll(), $(".siteJumper a").smoothScroll(), $("a.smoothscroll").smoothScroll(), tabSize < full_width ? ($("#contextNav li").mouseenter(function() { $("#contextNav li").each(function() { $(this).hasClass("interactive") || $(this).children("span").css({ display: "none" }) }), $(this).children("span").css({ display: "inline" }) }), $("#contextNav li").mouseleave(function() { $(this).hasClass("interactive") || $(this).children("span").css({ display: "none" }) })) : $("#contextNav li").each(function() { $(this).children("span").click(function() { $("#contextNav li").each(function() { $(this).removeClass("interactive") }), $(this).parent("li").addClass("interactive"), $(this).next("a").click() }) })) }

var contextNav_rebuild = function() {
    tabSize < full_width ? (

        //$(document).unbind("click"),

        $("#contextNav li").mouseenter(function() {
            $("#contextNav li").each(function() {
                $(this).hasClass("interactive") || $(this).children("span").css({ display: "none" })
            }),
                $(this).children("span").css({ display: "inline" })
        }),
            $("#contextNav li").mouseleave(function() {
                $(this).hasClass("interactive") || $(this).children("span").css({ display: "none" })
            })
    ) : (
        $("#contextNav li").unbind("mouseenter mouseleave"),
            $("#contextNav li").each(function() {
                $(this).children("span").css({ display: "inline" }),
                    $(this).children("span").click(function() {
                        $(this).next("a").click(),
                            $(this).parent("li").addClass("interactive")
                    })
            })
    )
};

function mobileContextNav() {
    var t = $("a.goto"),
        e = $("#contextNav"),
        i = $("#contextNav ul").width();
    "rtl" === layout_direction ? ("14px" != $("header h1").css("font-size") && "14px" != $("header .brand").css("font-size") || $("#contextNav ul").css("left", -1 * i), t.click().toggle(function() { i = $("#contextNav ul").width(), $("#contextNav > ul").show(), e.animate({ left: i }, "fast"), t.addClass("opened") }, function() { e.animate({ left: 0 }, "slow"), t.removeClass("opened") })) : ("14px" != $("header h1").css("font-size") && "14px" != $("header .brand").css("font-size") || $("#contextNav ul").css("right", -1 * i), t.click().toggle(function() { i = $("#contextNav ul").width(), $("#contextNav > ul").show(), e.animate({ right: i }, "fast"), t.addClass("opened") }, function() { e.animate({ right: 0 }, "slow"), t.removeClass("opened") })), $(document).bind("click", function(e) {!$(e.target).parents().hasClass("context") && t.hasClass("opened") && t.click() })
}
var contextNav_waypoint = function(e) {};
if (-1 != $.inArray("waypoints", loadedJQModules)) var tOut, currentSite, $active, contextNav_waypoint = function(t) {
    currentSite = t;
    var i = "section.contextnav";
    $(i).waypoint(function(e) { $active = $(this), "up" === e && ($active = $active.prevAll(i)), $active.length || ($active = $active.end()), "home" !== t && ($("#contextNav ul li").removeClass(), tabSize < full_width && $("#contextNav li").each(function() { $(this).children("span").css({ display: "none" }) })), $("#nav" + $active.attr("id")).addClass("interactive"), "home" !== t ? $("#nav" + $active.attr("id")).children("span").css({ display: "inline" }).delay(2e3).queue(function() { $(this).css({ display: "none" }), $(this).dequeue() }) : $("#nav" + $active.attr("id")).children("span").css({ display: "inline" }) }, { offset: 200 });
    $("html, body").each(function() { var e = $(this).attr("scrollTop"); if ($(this).attr("scrollTop", e + 1), $(this).attr("scrollTop") == e + 1) return this.nodeName.toLowerCase(), $(this).attr("scrollTop", e), !1 })
};

function animateContextNav() {
    var e = !1;
    if (!noShow) {
        var t = 0,
            i = $("#contextNav li.contextnavelement");
        if (!(50 < $(window).scrollTop())) { a(), $(window).scroll(function() {!1 === e ? (clearInterval(n), tabSize < full_width && contextNav_waypoint(), e = !0) : "home" === currentSite && (currentSite = "", contextNav_waypoint()) }), $("#contextNav").hover(function() {!1 === e && (clearInterval(n), tabSize < full_width && contextNav_waypoint(), e = !0) }); var n = setInterval(function() { a() }, 900) }
    }

    function a() { "home" == $("body").attr("id") ? (i.filter(function(e) { return e == t % i.length }).addClass("firstRun"), (t += 1) == i.length + 1 && (clearInterval(n), tabSize < full_width && !1 === e && (currentSite = "home", $("#contextNav ul li").first().addClass("interactive"), e = !0))) : (i.removeClass("firstRun").removeClass("active").filter(function(e) { return e == t % i.length }).addClass("firstRun"), (t += 1) == i.length && (clearInterval(n), setTimeout(function() { i.removeClass("firstRun"), i.filter(":first").addClass("interactive") }, 1e3), tabSize < full_width && (contextNav_waypoint(), e = !0))) }
}

function toggleLinkList(e) {
    var t = $(e).parentsUntil(".showHideWrapper").parent();
    $(e).children("span").toggle(), t.children("section").toggle(), t.hasClass("elementsHidden") ? (t.removeClass("elementsHidden"), $(e).removeClass("active")) : (t.addClass("elementsHidden"), $(e).addClass("active"))
}

function dropDownHandler(e) {
    if (!dropDownHandlerInitialized || e)
    {
        var t = "";
        e && (t = e + " + "), jQuery(document).ready(function() {
            initOpenCloseDropdown(t), initCloseDropDown()
        }),
            dropDownHandlerInitialized = !0
    }
}

var initOpenCloseDropdown = function(e) {
        -1 === e.indexOf("+") && "" != e && (e += " + ");
        $(e + ".dropdowns dt a").click(function() {
            if ($(this).parents("dl").hasClass("inactive_select"))
                $(".dropdowns dd ul").hide();
            else {
                var e = $(this).parent().next().children();
                window.innerHeight + $(document).scrollTop() - $(this).offset().top < $(this).height() + $(e).height() + 10 ? $(e).css("top", -1 * ($(e).height() + $(this).height() + 10)) : $(e).css("top", "auto"), $(e).toggle()
            }
        });
        initDropdownListClick(e);
    },

    initDropdownListClick = function(e) {
        $(e + ".dropdowns dd ul li a").click(function() {

            if( $(this).parents('li').hasClass('is___catalog_tab') ){

                catalogTabReload( $(this).parents('li') );

            } else {

                if (!$(this).parent().hasClass("optgroup")) {
                    dropDownHandlerClick($(this));
                    var e = $(this).parents(".selectDropdown");
                    if (e) {
                        var t = e.prev("select").attr("id"),   i = $(this).html();
                        $("#" + t + " option").each(function() {
                            $(this).html() === i && $(this).parents("select").val($(this).val()).change()
                        })
                    }
                }
            }

        })
    },

    initCloseDropDown = function() { $(document).bind("click", function(e) {!$(e.target).parents().hasClass("dropdowns") && $(".dropdowns dt").is(":visible") && $(".dropdowns dd ul").hide() }) };

function dropDownHandlerClick(e) {
    $(e).parents("ul").children("li.active").removeClass("active"), $(e).parent("li").addClass("active");
    var t = e,
        i = t.html(),
        n = t.parentsUntil("dl").parent(),
        a = t.parent().hasClass("selectHint");
    $("dt a span", n).html(i).removeClass("selectHint"), a && $("dt a span", n).addClass("selectHint"), $("dt", n).is(":visible") && $("dd ul", n).hide()
}

function dropdownInit() {
    if ($(".dropdown li.account-form__tab a, .dropdown li.product-tab a, ul.switch li a, ul.tab-letters li a").click(function() {
        if (!$(this).parents(".dropdown").hasClass("normal-link")) {
            var _current = $(this),
                _content_box = $(_current.attr("href"));
            return _current.parent().parent().children().removeClass("active"), _current.parent().addClass("active"), _current.parentsUntil("section").find(".tab").hide(), _content_box.show(), _content_box.find(".mapcanvas").attr("id") && (map_canvas = _content_box.find(".mapcanvas").attr("id"), eval("my_mapOptions = my_mapOptions_" + map_canvas), eval("json_mapMarker = json_mapMarker_" + map_canvas), eval("numbered_markers = numbered_markers_" + map_canvas), eval("show_counter = show_counter_" + map_canvas), googleMap(my_mapOptions, json_mapMarker, map_canvas, numbered_markers, show_counter)), $(this).parents("ul").hasClass("tab-letters") && ($(this).parents("ul").children("li").each(function() { $(this).children("a").removeClass("active_letter") }), $(this).addClass("active_letter")), _current.parentsUntil("section").children(".switch li.text").children().hide(), $("#" + _current.attr("id") + "-Text").css("display", "block"), window.location.hash = "showtab-" + _current.attr("href").replace("#", ""), !1
        }
    }), "showtab" == window.location.hash.substring(1, 8)) {
        var _activeTab = window.location.hash.substring(12),
            _activeTabLink = $("#tabLink" + _activeTab);
        console.log(_activeTabLink);
        _activeTabLink && $(window).load(function() {
            var e = _activeTabLink.parentsUntil("section").parent().offset();
            window.scrollTo(0, e.top), _activeTabLink.click(), $(".dropdown dt").is(":visible") && dropDownHandlerClick(_activeTabLink)
        })
    }
}

function responsiveProductTable() {
    if ($("html").hasClass("no-ie") || $("html").hasClass("gt-ie9")) {
        var e = $(".productTable"),
            t = 0;
        $(window).resize(function() { 0 === t || "16px" == t && ("15px" == $("header h1").css("font-size") || "15px" == $("header .brand").css("font-size") || "14px" == $("header h1").css("font-size") || "14px" == $("header .brand").css("font-size")) ? i(!0) : "16px)" == t || "16px" != $("header h1").css("font-size") && "16px" != $("header .band").css("font-size") || i(!1), t = $(window).width() }), "15px" == $("header h1").css("font-size") || "15px" == $("header .brand").css("font-size") || "14px" == $("header h1").css("font-size") || "14px" == $("header .brand").css("font-size") ? $(window).trigger("resize") : t = 0 < $("header h1").length ? $("header h1").css("font-size") : $("header .brand").css("font-size")
    }

    function i(n) {
        e.each(function(e, t) {
            if (n) {
                var s = $(t).find("th");
                $(t).find("tbody tr").each(function(e, t) {
                    var i = $(t).find("td"),
                        n = 0;
                    for (e = 0; e < s.length; e++)
                        if ($(s[e]).height() > $(i[e]).height() && (0 < n && n--, $(i[e - n]).attr("colspan") || $(i[e - n]).css({ minHeight: $(s[e]).height() })), $(i[e]).attr("colspan")) {
                            s.length === parseInt($(i[e]).attr("colspan")) && ($(i[e]).attr("colspan", parseInt($(i[e]).attr("colspan")) - 1), $(i[e]).parent().append('<td class="colspan_helper" style="border-top: none;">&nbsp;</td>')), n = $(i[e]).attr("colspan");
                            for (var a = $(i[e]).index(), o = $(s[e]).height(), r = 1; r < $(i[e]).attr("colspan"); r++) o += $(s[a + r]).outerHeight();
                            $(i[e]).css("min-height", o)
                        }
                }), $(t).find(".scrollbarContainer").mCustomScrollbar({ horizontalScroll: !0, axis: "x", advanced: { autoExpandHorizontalScroll: !0, updateOnBrowserResize: !0, updateOnContentResize: !0 } })
            } else {
                $(t).find("tbody td").each(function(e, t) {
                    if ("0px" !== $(t).css("minHeight") && $(t).css({ minHeight: 0 }), $(t).hasClass("colspan_helper")) {
                        var i = $(t),
                            n = i.prev().attr("colspan");
                        i.prev().attr("colspan", parseInt(n) + 1), i.remove()
                    }
                });
                var i = $(".scrollbarContainer");
                $.each(i, function() { $(this).mCustomScrollbar("destroy") })
            }
        })
    }
} - 1 != $.inArray("tiptip", loadedJQModules) && $(function() { $("ul.icons img").tipTip({ defaultPosition: "top" }), $(".tooltip").tipTip({ defaultPosition: "top" }) }),
    function(o, t, i, e) {
        o.fn.doubleTapToGo = function(e) {
            return !!("ontouchstart" in t || navigator.msMaxTouchPoints || navigator.userAgent.toLowerCase().match(/windows phone os 7/i)) && (this.each("unbind" === e ? function() { o(this).off(), o(i).off("click touchstart MSPointerDown", handleTouch) } : function() {
                var a = !1;
                o(this).on("click", function(e) {
                    var t = o(this);
                    t[0] != a[0] && (e.preventDefault(), a = t)
                }), o(i).on("click touchstart MSPointerDown", function(e) {
                    for (var t = !0, i = o(e.target).parents(), n = 0; n < i.length; n++) i[n] == a[0] && (t = !1);
                    t && (a = !1)
                })
            }), this)
        }
    }(jQuery, window, document);
var nav_first_run = !0,
    nav_mobil_first_run = !0;

$(document).ready(function() { init_navigation() });
var init_navigation = function() { set_nav_vars() },
    init_nav_click_events = function() {
        $(".isDesktop #navMenu > ul li:has(ul)").doubleTapToGo(), $("#navHome").doubleTapToGo(), $("#navHome > a").on("click", function(e) { e.preventDefault(), $("#navHome").toggleClass("active") }), $(document).on("mouseover", ".isDesktop #navMenu > ul > li", function(e) {
            if ($(this).parent().children().children("ul").addClass("hard-hide"), $(this).children("ul").removeClass("hard-hide"), !1 === $(this).hasClass("gotoPage")) {
                var t = $(this).offset().left,
                    i = $(this).children("ul").width();
                if ($(document).width() < t + i) {
                    $("header.mainHead").width();
                    $(this).children("ul").css({ right: 0, left: "auto" })
                }
            }
        }), $(document).on("mouseover", ".isDesktop #navMenu > ul > li > ul.isFlyout > li", function(e) {
            if (!1 === $(this).hasClass("gotoPage")) {
                var t = $(this).offset().left,
                    i = $(this).width(),
                    n = $(this).children("ul").width();
                $(document).width() < t + n + i && $(this).children("ul").css("margin-left", -1 * (n + i))
            }
        })
    },
    init_mobil_nav_click_events = function() { $("#navHome > a").on("click", function(e) { e.preventDefault(), $("#navHome").toggleClass("active") }), $("#navHome").doubleTapToGo(), $(document).on("click", ".isMobile #navMenu > ul li:not(.close-navigation) a", function(e) { $(this).parent().toggleClass("show-next") }), $(document).on("click", ".isMobile #navMenu > a, .isMobile .close-navigation a", function(e) { e.preventDefault(), $("body").toggleClass("navOpen") }), $(".isMobile li#navMenu ul li").not(".gotoPage").children("a").on("click", function(e) { e.preventDefault() }), $(document).on("click", ".isMobile li.loggedin_navitem > a", function(e) { e.preventDefault() }), $("body").on("swipeleft", function(e) { $("#navMenu > a").click() }).on("swiperight", function(e) { $(".close-navigation a").click() }) },
    set_nav_vars = function() {
        var e;
        window.useAsMobile = !1, $("html").hasClass("isMobile") ? e = "isMobile" : $("html").hasClass("isDesktop") && (e = "isDesktop"), jQuery("html").removeClass("isMobile"), jQuery("html").addClass("isDesktop"), jQuery("html").removeClass("isIPhoneDevice"), jQuery("html").removeClass("isIpadDevice"), jQuery("html").removeClass("isIpad"), jQuery("html").addClass("isNotIpad"), jQuery("html").removeClass("isMobileDevice"), (/iphone|ipod|android|blackberry|windows\sphone|windows\sce|palm/i.test(sUa.toLowerCase()) || "15px" == $("header h1").css("font-size") || "15px" == $("header .brand").css("font-size") || "14px" == $("header h1").css("font-size") || "14px" == $("header .brand").css("font-size")) && (jQuery("html").addClass("isMobile"), jQuery("html").removeClass("isDesktop"), window.useAsMobile = !0, /iphone/i.test(sUa.toLowerCase()) && jQuery("html").addClass("isIPhoneDevice")), (/ipad/i.test(sUa.toLowerCase()) || "15px" == $("header h1").css("font-size") || "15px" == $("header .brand").css("font-size") || "14px" == $("header h1").css("font-size") || "14px" == $("header .brand").css("font-size")) && (jQuery("html").addClass("isIpad"), jQuery("html").removeClass("isNotIpad"), /ipad/i.test(sUa.toLowerCase()) && jQuery("html").addClass("isIpadDevice")), /iphone|ipad|ipod|android|blackberry|windows\sphone|windows\sce|palm/i.test(sUa.toLowerCase()) && jQuery("html").addClass("isMobileDevice"), !0 === nav_first_run && $("html").hasClass("isDesktop") && "isDesktop" !== e && init_nav_click_events(), !0 === nav_mobil_first_run && $("html").hasClass("isMobile") && "isMobile" !== e && init_mobil_nav_click_events()
    },
    rwdResizeNavigationTimer, $navWidth, $headerWidth, $searchCondition;

function resizeNavigation() { window.clearTimeout(rwdResizeNavigationTimer), rwdResizeNavigationTimer = window.setTimeout(function() { set_nav_vars() }, 250) } $(window).resize(function() { resizeNavigation() });
var $homeWidth = 51,
    $logoWidth = 95,
    $searchWidth = 189,
    mouse_is_inside = !1,
    $navRebuilded = 0,
    $MouseInForm = !1;

function rebuildSearchMenu() { mobileSize < full_width ? 0 === $navRebuilded && (rebuildSearch(), $("#navSuche").click(function() { $(this).hasClass("isOpen") ? 0 == $MouseInForm && $(this).removeClass("isOpen") : ($(this).addClass("isOpen"), $("#navSuche input").focus()) })) : full_width < tabSize ? $("#navSuche form").removeAttr("style") : ($after_search = $("#navSuche").next().attr("id"), $navRebuilded = 0, "navLogo_right" === $after_search ? 0 === $navRebuilded && (rebuildSearch(), $("#navSuche").click(function() { $(this).hasClass("isOpen") ? 0 == $MouseInForm && $(this).removeClass("isOpen") : ($(this).addClass("isOpen"), $("#navSuche input").focus()) })) : ($("#navSuche").removeClass("smallTablet"), $("#navSuche").removeClass("isOpen"))), $("#navSuche").hover(function() { mouse_is_inside = !0 }, function() { mouse_is_inside = !1 }), $("#navSuche form").hover(function() { $MouseInForm = !0 }, function() { $MouseInForm = !1 }), $("body").click(function() {!mouse_is_inside && $("#navSuche").hasClass("isOpen") && $("#navSuche").removeClass("isOpen") }) } $(window).resize(function() { mobileSize < full_width ? 0 === $navRebuilded && rebuildSearch() : ($searchCondition = $navRebuilded = 0, $("#navSuche").removeClass("isOpen")) });
var rebuildSearch = function() { $headerWidth = parseInt($(".mainHead").width()), $navWidth = parseInt($("#navMenu").width()), $fullNavWidth = $navWidth + $logoWidth + $homeWidth, $after_search = $("#navSuche").next().attr("id"), $fullNavWidth > $headerWidth - $searchWidth && 1 !== $searchCondition || "navLogo_right" === $after_search ? ($("#navSuche").addClass("smallTablet"), setTimeout(function() { 0 < $(".twtAutocompleteMindbreeze").length && $(".twtAutocompleteMindbreeze").addClass("expandableSearch") }, 1e3), $searchCondition = 1) : $headerWidth > $fullNavWidth + $searchWidth && 2 !== $searchCondition && ($("#navSuche").removeClass("smallTablet"), $("#navSuche").removeClass("isOpen"), $searchCondition = 2) },
    kontakt_flyout_open = !1;

function contactFlyout() {
    $("#jumpKontakt").click(function() {
        if (!1 === kontakt_flyout_open) {
            $(".kontakt_flyout").height();
            var e = $(this).position();
            kontakt_flyout_top = e.top - 120, $(".kontakt_flyout").css("top", "-10px"), $(".kontakt_flyout").children(".pfeil").css("top", e.top + 17), $(".kontakt_flyout").show(), $(this).addClass("active_flyout"), $(".merkzettel_flyout").hide(), kontakt_flyout_open = !0
        } else $(this).removeClass("active_flyout"), $(".kontakt_flyout").hide(), kontakt_flyout_open = !1;
        return !1
    }), $(".kontakt_flyout").mouseleave(function() { $("#jumpKontakt").removeClass("active_flyout"), $(this).hide(), kontakt_flyout_open = !1 }), $("#contextNav").mouseleave(function() { $("#jumpKontakt").removeClass("active_flyout"), $(".kontakt_flyout").hide(), kontakt_flyout_open = !1 })
}
var newsletter_flyout_open = !1,
    balanceLabelsTimer;

function newsletterFlyout() {
    $("#jumpNewsletter").click(function() {
        if (!1 === newsletter_flyout_open) {
            $(".newsletter_flyout").height();
            var e = $(this).position();
            newsletter_flyout_top = e.top - 99, $(".newsletter_flyout").css("top", "25px"), $(".newsletter_flyout").children(".pfeil").css("top", e.top - 17), $(".newsletter_flyout").show(), $(this).addClass("active_flyout"), newsletter_flyout_open = !0
        } else $(this).removeClass("active_flyout"), $(".newsletter_flyout").hide(), newsletter_flyout_open = !1;
        return !1
    }), $(".newsletter_flyout").mouseleave(function() { $("#jumpNewsletter").removeClass("active_flyout"), $(this).hide(), newsletter_flyout_open = !1 }), $("#contextNav").mouseleave(function() { $("#jumpNewsletter").removeClass("active_flyout"), $(".newsletter_flyout").hide(), newsletter_flyout_open = !1 })
}

function styleForms() { jQuery("input[type=radio]").each(function(e) { $(this).hasClass("newStyledInput") ? createNewStyledInputs(this) : createStyledInputs(this, e, "radio") }), jQuery("input[type=checkbox]").each(function(e) { $(this).hasClass("newStyledInput") ? createNewStyledInputs(this) : createStyledInputs(this, e, "checkbox") }), createStyledSelects() }

function createNewStyledInputs(e) { $(e).next("label").wrapInner('<span class="label_content"></span>').prepend('<span class="icon"></span>') }

function jQueryEscape(e) { return e.replace(/(:|\.|\[|\]|,)/g, "\\$1") }

function createStyledInputs(e, t, i) {
    var n = jQuery(e),
        a = n.attr("checked"),
        o = n.attr("name"),
        r = jQueryEscape(o),
        s = n.attr("onChange"),
        l = "",
        c = "";
    void 0 !== s && (l = 'data-style-onchange="' + s + '"'), void 0 !== n.attr("onChangeStopPropagation") && (l += 'data-style-onchange-stoppropagation="true"'), void 0 !== n.attr("locked") && (c = 'data-style-locked="true"'), n.addClass("hidden" + (a ? " checked" : "")).attr("data-style-input", o + t).attr("data-style-name", o).before('<span class="styled' + i + " " + (a ? "checked" : "") + '" ' + l + " " + c + ' data-style-name="' + o + '" data-style-input="' + o + t + '" ></span>'), jQuery("label[for=" + n.attr("id") + "]").css("cursor", "pointer").attr("data-style-input", o + t).attr("data-style-name", o), c && jQuery("label[for=" + n.attr("id") + "]").click(function() { return !1 }), jQuery("span[data-style-input=" + r + t + "]").on("click", { selector: "[data-style-input=" + r + t + "]", type: i }, toggleStyled), $.browser.msie && jQuery("label[data-style-input=" + r + t + "]").on("click", { selector: "[data-style-input=" + r + t + "]", type: i, origin: "label_click" }, toggleStyled), jQuery("input[data-style-input=" + r + t + "]").on("change", { selector: "[data-style-input=" + r + t + "]", type: i, origin: "label" }, toggleStyled).removeAttr("onChange")
}

function toggleStyled(event_, oOptions_) {
    var _$elements = jQuery(event_.data.selector),
        _$elInput = _$elements.filter("input"),
        _isChecked = _$elInput.is(":checked");
    if (!_$elements.attr("data-style-locked")) switch (event_.data.type) {
        case "radio":
            jQuery("[data-style-name=" + jQueryEscape(_$elInput.attr("name")) + "]").removeClass("checked").attr("checked", !1), _$elements.addClass("checked"), _$elInput.attr("checked", !0), "label_click" === event_.data.origin && _$elInput.change();
            break;
        case "checkbox":
            "label_click" == event_.data.origin || ("label" == event_.data.origin ? _isChecked ? (_$elements.addClass("checked"), _$elInput.attr("checked", !0)) : (_$elements.removeClass("checked"), _$elInput.attr("checked", !1)) : _isChecked ? (_$elements.removeClass("checked"), _$elInput.attr("checked", !1)) : (_$elements.addClass("checked"), _$elInput.attr("checked", !0)))
    }
    "label_click" !== event_.data.origin && (_$elements.attr("data-style-onchange") && eval(_$elements.attr("data-style-onchange")), _$elements.attr("data-style-onchange-stoppropagation") && cancelEvent(event_))
}




function createStyledSelects() {
    var a;
    // $("html").hasClass("isMobileDevice") ? $(".styledSelect").find("select:not(.isStyled)").each(function() {

    //         $(this).show(), $(this).addClass("mobile_device_select");
    //         var e = "";
    //         $(this).attr("disabled") && (e = " inactive_select"), $(this).children().each(function(e) { $(this).is("optgroup") ? (0 === e && (a = $(this).children(":first").html()), $(this).children().each(function() { $(this).attr("selected") && (a = $(this).html()) })) : (0 === e && (a = $(this).children(":first").html()), $(this).attr("selected") && (a = $(this).html())) });
    //         var t = '<dl class="dropdowns selectDropdown' + e + ' mobile_device_select_overlay"><dt><a><span>first_select</span></a></dt></dl>';
    //         t = t.replace("first_select", a), $(this).parent().append(t), $(this).addClass("isStyled"), $(this).on("change", function() {
    //             var e;
    //             $(this).children().is("optgroup") ? ($(this).children().each(function() { $(this).children().each(function() { $(this).attr("selected") && (e = $(this).text()) }) }), $(this).next(".dropdowns").find("span").html(e)) : $(this).next(".dropdowns").find("span").html($(this).children("option:selected").text())
    //         })
    //     }) :

    $(".styledSelect").find("select:not(.isStyled)").each(function() {
        var e = "",
            i = !1;
        $(this).attr("disabled") && (e = " inactive_select");
        var n = '<dl class="dropdowns selectDropdown' + e + '"><dt><a><span class="spanClass">first_select</span></a></dt><dd><ul>';
        $(this).children().each(function(e) {
            if ($(this).is("optgroup")) n += '<li class="optgroup"><a>' + $(this).attr("label") + "</a></li>", 0 === e && (a = $(this).children(":first").html()), $(this).children().each(function() {
                var e = "";
                $(this).attr("selected") && (a = $(this).html(), e = " active"), n += '<li class="optgr_child' + e + '"><a rel="' + $(this).val() + '">' + $(this).html() + "</a></li>"
            });
            else {
                var t = "";
                0 === e && (a = $(this).children(":first").html()), $(this).attr("selected") && (a = $(this).html(), t = "active"), $(this).hasClass("selectHint") && (t += " selectHint", $(this).attr("selected") && 0 === e && (i = !0)), n += '<li class="' + t + '"><a rel="' + $(this).val() + '">' + $(this).html() + "</a></li>"
            }
        }), n = (n += "</ul></dd></dl>").replace("first_select", a), i && (n = n.replace("spanClass", "selectHint")), $(this).parent().append(n), $(this).css("display", "none");

        $(this).addClass("isStyled");

        dropDownHandler();
    });
}


function balanceLabelsInit(e) { $("section div.cell", e).each(function(e, t) { $("label", this).attr("style") && $("label", this).attr("oldstyle", $("label", this).attr("style")) }), balanceLabels(e), $(window).resize(function() { window.clearTimeout(balanceLabelsTimer), balanceLabelsTimer = window.setTimeout(function() { balanceLabels(e) }, 200) }) }

function balanceLabels(e) {
    var t = $("section", e);
    $("div.cell label").each(function(e, t) { $(this).removeAttr("style"), $(this).attr("style", $(this).attr("oldstyle")) }), $.each(t, function(e, t) {
        var n, i = $("div.cell", this),
            a = new Array,
            o = 0,
            r = 0;
        $.each(i, function(e, t) {
            0 != e && n == $(this).position().top || (balanceSingleLabels(r, o, a), a = new Array, r = o = 0), a.push(this);
            var i = $("label", this).height();
            null != i && (o < i && (o = i), (0 == r || i < r) && (r = i)), n = $(this).position().top
        }), balanceSingleLabels(r, o, a)
    })
}

function balanceSingleLabels(e, i, t) { i != e && $.each(t, function(e, t) { $("label", this).height(i) }) }
var new_carouselLoad = function() {
    $("div.carouselTeaser.doubleTeaser").each(function(e, t) { new_makeCarousel(t, !1, !1) }), $("div.carouselementList").each(function(e, t) {
        var i = $(t).find(" ul ul li");
        i.unwrap().unwrap();
        var n = 3;
        i.length % 4 == 0 && tabSize < full_width && (n = 4), i.length <= 4 && (n = 2);
        for (var a = 0, o = i.length; a < o; a += n) x = i.slice(a, a + n).wrapAll("<ul/>");
        $(t).parent().parent().addClass("items" + n), $(t).find("ul.jcarousel-skin").children("ul").wrap("<li/>"), new_makeCarousel(t, !1, !1)
    }), $("div.carouselViewport").each(function(e, t) { new_setCarouselViewportWidth(t), new_makeCarousel(t, !0, "circular") }), $("div.carouselTeaser.productTeaser, div.carouselTeaser.themenTeaser, div.carouselTeaser.full-width-teaser").each(function(e, t) { new_makeCarousel(t, !1, !1) }), $("div.carouselTeaser.tribleTeaser").each(function(e, t) { new_makeCarousel(t, !1, !1) }), $("div.carouselTeaser.teaserReihe").each(function(e, t) { new_makeCarousel(t, !1, !1) }), $("ul.imageSlider").each(function(e, t) { $(t).wrap('<div class="imageSlider_carousel carouselTeaser"></div>'), new_makeCarousel($(t).parent(), !1, !1) })
};

function new_setCarouselViewportWidth(e) {
    var t = $(e).closest("section").width();
    1153 < t && (t = 1153), $(e).find(".jcarousel-skin > li").width(t + "px")
}
var new_makeCarousel = function(e, t, i) {
        window.pagination_helper = 1, window.control_helper = 1;
        var n = $("header .brand").css("font-size");
        0 < $("header h1").length && (n = $("header h1").css("font-size"));
        var a = $(e),
            o = a.closest("section");
        new_carouselPadding(a, o), $(a).find("ul.jcarousel-skin").children("li").addClass("jcarousel-item jcarousel-item-horizontal"), a.on("jcarousel:reload jcarousel:create", function() {
            n = $("header .brand").css("font-size"), 0 < $("header h1").length && (n = $("header h1").css("font-size"));
            var e = $(this),
                t = e.innerWidth(),
                i = e.jcarousel("items").length;
            $(e).children(".slider").removeClass("hidden"), $(e).hasClass("doubleTeaser") && (control_helper = 1, "14px" == n || "15px" == n ? (t /= 1, e.jcarousel("items").css("width", Math.ceil(t) + "px"), pagination_helper = 1) : "16px" == n && 2 < i ? (pagination_helper = 2, t /= 1, e.jcarousel("items").css("width", Math.ceil(t) + "px")) : (pagination_helper = 2, e.jcarousel("items").css("width", "auto"), $(e).children(".slider").addClass("hidden"))), $(e).hasClass("tribleTeaser") && ("14px" == n ? (pagination_helper = 1, control_helper = 1, e.jcarousel("items").css("width", "270px")) : "15px" == n ? (pagination_helper = 2, control_helper = 2, e.jcarousel("items").css("width", "204px")) : (pagination_helper = 3, control_helper = 3, e.jcarousel("items").css("width", "280px"), i <= 3 && ($(e).children(".slider").addClass("hidden"), $(e).addClass("noSlider")))), $(e).hasClass("carouselementList") && ("14px" == n ? (pagination_helper = 1, control_helper = 1, e.jcarousel("items").css("width", "240px")) : "15px" == n ? (pagination_helper = 2, control_helper = 2, e.jcarousel("items").css("width", "190px")) : (pagination_helper = 2, control_helper = 2, e.jcarousel("items").css("width", "190px"), i <= 2 && ($(e).children(".slider").addClass("hidden"), $(e).addClass("noSlider")))), $(e).hasClass("teaserReihe") && ("14px" == n ? (pagination_helper = 2, control_helper = 2, e.jcarousel("items").css("width", "270px")) : "15px" == n ? (pagination_helper = 2, control_helper = 2, e.jcarousel("items").css("width", "204px")) : (pagination_helper = 4, control_helper = 4, e.jcarousel("items").css("width", "204px"), i <= 4 && ($(e).children(".slider").addClass("hidden"), $(e).addClass("noSlider")))), $(e).hasClass("themenTeaser") && ("14px" == n ? (pagination_helper = 2, control_helper = 2, e.jcarousel("items").css("width", "270px")) : "15px" == n ? (pagination_helper = 2, control_helper = 2, e.jcarousel("items").css("width", "204px")) : (pagination_helper = 4, control_helper = 4, e.jcarousel("items").css("width", "204px"), i <= 4 && ($(e).children(".slider").addClass("hidden"), $(e).addClass("noSlider")))), $(e).hasClass("full-width-teaser") && ("14px" == n ? (pagination_helper = 2, control_helper = 2, e.jcarousel("items").css("width", t / 2)) : "15px" == n ? (pagination_helper = 2, control_helper = 2, e.jcarousel("items").css("width", t / 2)) : (pagination_helper = 4, control_helper = 4, $(e).hasClass("no-headline") ? e.jcarousel("items").css("width", t / i) : e.jcarousel("items").css("width", t / 4), i <= 4 && ($(e).children(".slider").addClass("hidden"), $(e).addClass("noSlider")))), $(e).hasClass("productTeaser") && ("14px" == n ? (pagination_helper = 2, control_helper = 2, e.jcarousel("items").css("width", "122px"), i <= 2 && ($(e).children(".slider").addClass("hidden"), $(e).addClass("noSlider"))) : "15px" == n ? (pagination_helper = 2, control_helper = 2, e.jcarousel("items").css("width", "204px"), i <= 2 && ($(e).children(".slider").addClass("hidden"), $(e).addClass("noSlider"))) : (pagination_helper = 4, control_helper = 4, e.jcarousel("items").css("width", "204px"), i <= 4 && ($(e).children(".slider").addClass("hidden"), $(e).addClass("noSlider")))), $(e).hasClass("carouselViewport") && (control_helper = 1, pagination_helper = 1), $(e).hasClass("imageSlider_carousel") && (0 < $(e).parents(".tab").length ? "14px" == n ? (pagination_helper = 2, control_helper = 2, e.jcarousel("items").css("width", "106px"), i <= 2 && $(e).children(".slider").addClass("hidden")) : ("15px" == n ? (pagination_helper = 4, control_helper = 4, e.jcarousel("items").css("width", "89px")) : (pagination_helper = 4, control_helper = 4, e.jcarousel("items").css("width", "91px")), i <= 4 && ($(e).children(".slider").addClass("hidden"), $(e).addClass("noSlider"))) : "14px" == n ? (pagination_helper = 2, control_helper = 2, e.jcarousel("items").css("width", "122px"), i <= 2 && $(e).children(".slider").addClass("hidden")) : ("15px" == n ? (pagination_helper = 4, control_helper = 4, e.jcarousel("items").css("width", "89px")) : (pagination_helper = 4, control_helper = 4, e.jcarousel("items").css("width", "91px")), i <= 4 && ($(e).children(".slider").addClass("hidden"), $(e).addClass("noSlider"))))
        }).on("jcarousel:createend", function() { $(this).hasClass("full-width-teaser") && ($(this).find("dd").css("height", Math.floor($(this).find("dt img").height()) - 1), $(this).find("dl").css("height", Math.floor($(this).find("dt img").height()) - 1)), $(this).find(".jcarousel-pagination").on("jcarouselpagination:active", "a", function() { $(this).addClass("active") }).on("jcarouselpagination:inactive", "a", function() { $(this).removeClass("active") }).on("click", function(e) { e.preventDefault() }).jcarouselPagination({ perPage: pagination_helper, item: function(e) { return '<a href="#' + e + '" class="sliderPage">' + e + "</a>" } }), $(this).find(".jcarousel-control-prev").on("click", function(e) { e.preventDefault(), 0 === $(this).next(".jcarousel-pagination").children(".active").prev().length && !0 === t ? $(a).jcarousel("scroll", "-=1") : $(this).next(".jcarousel-pagination").children(".active").prev().click() }), $(this).find(".jcarousel-control-next").on("click", function(e) { e.preventDefault(), 0 === $(this).prev(".jcarousel-pagination").children(".active").next().length && !0 === t ? $(a).jcarousel("scroll", "+=1") : $(this).prev(".jcarousel-pagination").children(".active").next().click() }), new_init_swipe_carousel($(this)) }).on("jcarousel:reload", function() { $(this).hasClass("carouselViewport") && new_setCarouselViewportWidth($(this)), $(this).find(".jcarousel-pagination").jcarouselPagination("reload", { perPage: pagination_helper }) }).jcarousel({ wrap: i }).jcarouselAutoscroll({ interval: 5e3, target: "+=" + control_helper, autostart: t })
    },
    new_init_swipe_carousel = function(e) { $(e).on("swipeleft", function(e) { $(this).children(".slider").children(".sliderNext").click() }).on("swiperight", function(e) { $(this).children(".slider").children(".sliderPrev").click() }) };

function new_carouselPadding(e, t) {
    if (0 < $(".simpleSlider").length);
    else if (t.hasClass("flexibleHeight") || e.hasClass("tab") || t.parent().hasClass("tab") || t.hasClass("linkList") || e.hasClass("carouselViewport") || e.hasClass("full-width-teaser")) t.hasClass("new-navigation") ? $(e).append("<div class='slider'><a href='#' class='sliderPrev jcarousel-control-prev'>prev</a><span class='sliderPagerContainer jcarousel-pagination'></span><a href='#' class='sliderNext jcarousel-control-next'>next</a></div><a href='#' class='prev-aside' onclick='javascript:click_nav(event,this);'>prev</a><a href='#' class='next-aside' onclick='javascript:click_nav(event,this);'>next</a>") : $(e).append("<div class='slider'><a href='#' class='sliderPrev jcarousel-control-prev'>prev</a><span class='sliderPagerContainer jcarousel-pagination'></span><a href='#' class='sliderNext jcarousel-control-next'>next</a></div>");
    else {
        var i = e.closest("div.wrapperinner"),
            n = i.position(),
            a = t.height() - i.height() - n.top;
        t.hasClass("new-navigation") ? $(e).append("<div class='slider'><a href='#' class='sliderPrev jcarousel-control-prev'>prev</a><span class='sliderPagerContainer jcarousel-pagination'></span><a href='#' class='sliderNext jcarousel-control-next'>next</a></div><a href='#' class='prev-aside' onclick='javascript:click_nav(event,this);'>prev</a><a href='#' class='next-aside' onclick='javascript:click_nav(event,this);'>next</a>") : $(e).append("<div class='slider'><a href='#' class='sliderPrev jcarousel-control-prev'>prev</a><span class='sliderPagerContainer jcarousel-pagination'></span><a href='#' class='sliderNext jcarousel-control-next'>next</a></div>");
        var o = e.find("div.slider"),
            r = (a - (o.height() + 10)) / 2;
        65 < r && (r = 65), o.css("padding-top", r)
    }
}
var click_nav = function(e, t) { e.preventDefault(), "prev-aside" === $(t).attr("class") ? $(t).prev().children(".sliderPrev").click() : $(t).prev().prev().children(".sliderNext").click() },
    scrollbarWidthRes;

function initCarouselViewport(e, t) {}

function scrollbarWidth() {
    var e = $('<div style="width:50px;height:50px;overflow:hidden;position:absolute;top:-200px;left:-200px;"><div style="height:100px;"></div>');
    $("body").append(e);
    var t = $("div", e).innerWidth();
    e.css("overflow-y", "scroll");
    var i = $("div", e).innerWidth();
    return $(e).remove(), scrollbarWidthRes = t - i
}

function showHideButtonInit(i, n, a) {
    $(document).ready(function() {
        var e = $(n),
            t = $(i);
        $(i).bind("click", function() { return showHideButtonHandler(t, e), !1 }), "hidden" == a ? e.hide() : t.addClass("active")
    })
}

function showHideButtonHandler(e, t) { t.is(":hidden") ? e.addClass("active") : e.removeClass("active"), t.toggle() }

function jumpListInit() { $(".jumpList a").bind("click", function() { jumpListJump($(this).attr("href")) }) }

function jumpListJump(e) { return _container = $(e).parentsUntil(".hidden"), _container.length || $(".buttonlink.showMore").click(), calculatedJump(e), !1 }

function initShowMoreButton(e, t) { $(t).bind("click", function() { return $(this).hide(), $(e).show(), !1 }) }

function calculatedJump(e) {
    var t = $(e).offset().top;
    t -= headerHeight + 5, getHeaderHeight(), $("html, body").animate({ scrollTop: t }, 500)
}
var onPageJumps = function() {
        var e = $('a[href="#cform"]');
        $.each(e, function() { $(this).on("click", function(e) { e.preventDefault(), calculatedJump($(this).attr("href")) }) })
    },
    merkzettel;

function getHeaderHeight() {
    var e = $(".mainHead").height(),
        t = $("#navBreadcrumb").height();
    return headerHeight = e + t, e
}

function sourceChanger(e, t) {
    _container = $("#" + e), _target = $("#" + t), jQuery(_container.find("a")).bind("click", { target: _target }, function(e) {
        if (_newSrc = $(this).attr("href"), $(this).hasClass("youtube")) {
            var t = (t = _newSrc.length) - 11,
                i = _newSrc.substring(t);
            _newSrc = "http://www.youtube.com/embed/" + i + "?wmode=opaque"
        }
        return e.data.target.attr("src", _newSrc), !1
    })
}

function styleSiteJumper(e) {
    if ("14px" == $("header h1").css("font-size") || "14px" == $("header .brand").css("font-size")) return !1;
    if (_container = $("#" + e).parent(), 0 < _container.find("h2.text span").length ? (_z1 = _container.find("h2.text span").width(), _z2 = _container.find("h2.text span").css("padding-right").replace("px", "")) : (_z1 = _container.find(".headline-wrapper").width(), _z2 = _container.find(".headline-wrapper").css("padding-right").replace("px", "")), _z2 = parseInt(_z2), _width = parseInt(_z1) + _z2, $("#" + e).addClass("activated"), _widthJumperHeight = $("#" + e + " span").height(), $("#" + e + " span").width(_width), _widthJumperHeight2 = $("#" + e + " span").height(), 0 == _widthJumperHeight2) return !1;
    for (_singleHeight = $("#" + e + " a").outerHeight(), _sumHeight = _singleHeight * Math.ceil($("#" + e + " a").length / 2), _z2New = _z2; _sumHeight != _widthJumperHeight2;) _width += 10, $("#" + e + " span").width(_width), _widthJumperHeight2 = $("#" + e + " span").height(), _z2New += 10;
    _z2 != _z2New && _container.find("h2.text span").css("padding-right", _z2New + "px")
}

function fancyStart(e, t, i) { var n = e.length; for (_i = 0, _pagerHtml = ""; _pagerHtml = _pagerHtml + '<a href="#" class="sliderPage" id="pager_fancy' + _i + '"></a>', _i++, _i < n;); for ($("#fancybox-pager-inner .sliderPagerContainer").html(_pagerHtml), $("#fancybox-pager-inner .sliderPagerContainer a:eq(" + t + ")").addClass("active"), _i = 0; $("#pager_fancy" + _i).bind("click", { i: _i }, function(e) { return goto = e.data.i, $.fancybox.pos(goto), !1 }), _i++, _i < n;); }

function fancyboxReady() {
    var t = { titleShow: !0, titlePosition: "over", transitionIn: "elastic", transitionOut: "elastic", overlayOpacity: .95, overlayColor: "#4c4c4c", padding: 0, margin: 0, showNavArrows: !1, onStart: fancyStart };
    $(".fancybox.basic").each(function() {
        var e = t;
        $(this).hasClass("fancyForceImage") && (e.type = "image"), $(this).fancybox(e)
    }), $("#fancybox-wrap").on("swipeleft", function(e) { $.fancybox.next() }).on("swiperight", function(e) { $.fancybox.prev() }).on("movestart", function(e) {
        (e.distX > e.distY && e.distX < -e.distY || e.distX < e.distY && e.distX > -e.distY) && e.preventDefault()
    }), imageOverlayChanger("a.plusIcon", "overlayImageWrapper", "plusIcon");
    var e = 270,
        i = 415;
    mobileSize < full_width ? (e = 894, i = 609) : tabSize < full_width && (e = 440, i = 689), $(".cadLayer").fancybox({ titleShow: !1, transitionIn: "elastic", transitionOut: "elastic", overlayOpacity: .95, overlayColor: "#4c4c4c", padding: 0, margin: 0, showNavArrows: !1, width: e, height: i, type: "iframe", showPager: !1 })
}

function imageOverlayChanger(e, n, a) {
    $(e).each(function(e, t) {
        var i = $(t).css("background-image");
        i = i.replace(/\"|\'|\)|\(|url/g, ""), $(t).wrap("<div class='" + n + "' />").removeClass(a).append("<img src='" + i + "' class='new" + a + "'>"), "infoLayerImage" == a && tiptip_open($(this).children(".new" + a), $(this).parents("dt:eq(0)").next())
    }), "infoLayerImage" == a && ($(".hotspot_closer span.close_button").live("click", function() { $(".tiptip_advanced").fadeOut(200) }), $("a").each(function() { $(this).click(function() { $(".tiptip_advanced").fadeOut(200) }) }))
}

function tiptip_open(e, t) { e.tipTip({ activation: "click", keepAlive: !0, maxWidth: "230px", edgeOffset: 6, delay: 0, autoFadeOut: !1, advancedClass: "tiptip_advanced", defaultPosition: "right", content: t.html() }) }

function imageOverlayChangerReady() { imageOverlayChanger("a.highlightImage", "overlayImageWrapper", "highlightImage"), imageOverlayChanger("div.highlightImage", "overlayImageWrapper", "highlightImage"), imageOverlayChanger("a.infoLayerImage", "overlayImageWrapper", "infoLayerImage") } $(window).resize(function() { getHeaderHeight() }), Pager = function() {}, Pager.prototype = { initialize: function(e) { var t = this; for (this.container = $("#" + e), this.pages = this.container.find("div.page"), this.length = this.pages.length, this.active = 1, this.container.find("h4 span:first").html("1"), this.container.find("h4 span:eq(1)").html(this.length), jQuery(this.container.find(".sliderNext")).bind("click", function() { return t.page("next"), !1 }), jQuery(this.container.find(".sliderPrev")).bind("click", function() { return t.page("prev"), !1 }), _i = 0, _pagerHtml = ""; _pagerHtml = _pagerHtml + '<a href="#" class="sliderPage" id="pager' + e + "-page" + _i + '"></a>', _i++, _i < this.length;); for (this.container.find(".sliderPagerContainer").html(_pagerHtml), this.container.find(".sliderPagerContainer a:first").addClass("active"), _i = 0; $("#pager" + e + "-page" + _i).bind("click", { i: _i }, function(e) { return goto = e.data.i, t.page("goto", goto), !1 }), _i++, _i < this.length;); }, page: function(e, t) { "next" == e ? this.active < this.length && this.active++ : "prev" == e ? 1 < this.active && this.active-- : "goto" == e && 0 <= t && t < this.length && (this.active = t + 1), this.setActive() }, setActive: function() { show = this.active - 1, this.container.find("div.page").hide(), this.container.find("div.page:eq(" + show + ")").show(), this.container.find(".sliderPagerContainer a").removeClass("active"), this.container.find(".sliderPagerContainer a:eq(" + show + ")").addClass("active"), this.container.find("h4 span:first").html(this.active) } };
var idizies = "",
    cookie_counter = 0,
    cookie_counter_firstrun = !1,
    _current_shown_content, switchContainer, infopaketeAngleichenTimer;

function initMerkzettel() {
    if ($.dough("merkzettel")) {
        if ("object" != typeof(merkzettel = $.dough("merkzettel"))) {
            var e = { sortiment: [], referenzen: [], seminare: [], events: [], pages: [] };
            $.dough("merkzettel", JSON.stringify(e), { expires: 365, path: "/", domain: "auto" }), merkzettel = $.dough("merkzettel")
        }
        void 0 === merkzettel.pages && (merkzettel.pages = [], $.dough("merkzettel", JSON.stringify(merkzettel), { expires: 365, path: "/", domain: "auto" })), $.each(merkzettel, function(n, e) { $.each(e, function(e, t) { "pages" !== n.toLowerCase() ? $("#" + n.toLowerCase() + "-" + t.id).html(lang_gemerkt).addClass("fav_active") : $("#" + n.toLowerCase() + "-" + t.id).html(lang_seite_gemerkt).addClass("fav_active"); var i = ""; "sortiment" == n && ("sys" == (i = t.id.substr(0, 3)) && (i = "systeme"), "pro" == i && (i = "produkte")), 1 != merkzettel_activation[n.toString()] && 1 != merkzettel_activation[i.toString()] || !1 === cookie_counter_firstrun && cookie_counter++ }) }), cookie_counter_firstrun = !0
    } else {
        e = { sortiment: [], referenzen: [], seminare: [], events: [], pages: [] };
        $.dough("merkzettel", JSON.stringify(e), { expires: 365, path: "/", domain: "auto" }), merkzettel = $.dough("merkzettel")
    }
    cookie_counter_refresh()
}

function cookiesetter(element, markerFlag) {
    var bereich = $(element).attr("id").substring(0, $(element).attr("id").indexOf("-")),
        item_id = $(element).attr("id").substring($(element).attr("id").indexOf("-") + 1, $(element).attr("id").length);
    if ($(element).html().trim() === lang_merken) {
        var ts = Math.round((new Date).getTime() / 1e3);
        eval("merkzettel." + bereich.toLowerCase() + '.push({"id": "' + item_id + '", "time": "' + ts + '"})'), $.dough("merkzettel", JSON.stringify(merkzettel), { expires: 365, path: "/", domain: "auto" }), $(element).html().trim() === lang_merken ? $(element).html(lang_gemerkt).addClass("fav_active") : $(element).html() === lang_seite_merken && $(element).html(lang_seite_gemerkt).addClass("fav_active"), cookie_counter++;
        var _area = "overview";
        2 == markerFlag && (_area = "detail"), logMarker("leaflet/" + bereich + "/" + _area + "/" + item_id)
    } else {
        var helper = eval("merkzettel." + bereich.toLowerCase());
        helper = $.grep(helper, function(e, t) { e.id === item_id && (idizies = t) }), eval("merkzettel." + bereich.toLowerCase()).splice(idizies, 1), $.dough("merkzettel", JSON.stringify(merkzettel), { expires: 365, path: "/", domain: "auto" }), $(element).html().trim() === lang_gemerkt ? $(element).html(lang_merken).removeClass("fav_active") : $(element).html().trim() === lang_seite_gemerkt && $(element).html(lang_seite_merken).removeClass("fav_active"), cookie_counter--
    }
    return cookie_counter_refresh(), !1
}

function cookie_counter_refresh() { 0 === cookie_counter ? ($("#navStern").addClass("navStern_inactive"), $("#navStern > a").html("")) : ($("#navStern").hasClass("navStern_inactive") && $("#navStern").removeClass("navStern_inactive"), $("#navStern > a").html(cookie_counter)) }

function initSetUnifiedHeight(e) { $(document).ready(function() { setUnifiedHeight(e) }), $(window).resize(function() { setUnifiedHeight(e) }) }

function setUnifiedHeight(e) {
    var t = 0;
    $("#" + e + " > div").css("height", "auto"), $("body").width() > mobileSize ? ($("#" + e + " > *").each(function() { parseInt($(this).css("height")) > parseInt(t) && (t = $(this).css("height")) }), $("#" + e + " > div").each(function() { $(this).css("height", t) })) : $("#" + e + " > div").each(function() { $(this).css("height", "auto") })
}

function switchafterselect() {
    var t = "";
    $(switchContainer + " #first_selection select").change(function() {
        "" !== t && $("#selection_" + t).hide();
        var e = $(this).val();
        $("#selection_" + e).hasClass("only_one") ? showselectedcontent(e) : ($("#selection_" + e).show(), t = $(this).val(), showselectedcontent(e), switchradiobuttons(e))
    })
}

function switchradiobuttons(e) { $(switchContainer + " #selection_" + e + " span").click(function() { showselectedcontent(e) }), $(switchContainer + " #selection_" + e + " input[type=radio]").change(function() { showselectedcontent(e) }) }

function showselectedcontent(e) {
    "" !== _current_shown_content && $(_current_shown_content).hide();
    var t = $("input[type=radio]:checked", "#selection_" + e).attr("id");
    if (_current_shown_content = "#content_" + t, $(_current_shown_content).show(), 0 < $(_current_shown_content + " div.map").length) {
        var i = $(_current_shown_content + " div.map").attr("id");
        googleMap(my_mapOptions, json_mapMarker, map_canvas, numbered_markers), google.maps.event.trigger(global_maps[i], "resize"), AutoCenter(global_maps[i])
    }
}

function set_footer_height() {
    var e = $("footer.mainFooter").outerHeight();
    $("#pageWrapperOuter").css("margin-bottom", -1 * e + "px"), $("#pageWrapperInner").css("padding-bottom", e + "px")
}

function initTableSorter(e, t, i) {
    t = void 0 === t ? "#downloadTable" : t, i = void 0 === i ? "#sort_table" : i, $.tablesorter.addParser({
        id: "Stand",
        is: function(e) { return !1 },
        format: function(e) {
            var t = e.length - 4,
                i = e.substr(t);
            t -= 1;
            var n = e.substr(0, t);
            return i + "" + (n = n.replace("Jan.", "01").replace("Feb.", "02").replace("Mär.", "03").replace("Apr.", "04").replace("Mai", "05").replace("Jun.", "06").replace("Jul.", "07").replace("Aug.", "08").replace("Sep.", "09").replace("Okt.", "10").replace("Nov.", "11").replace("Dez.", "12"))
        },
        type: "numeric"
    }), $.tablesorter.addParser({ id: "Ausgabe", is: function(e) { return !1 }, format: function(e) { return e.substr(-4) + "" + e.substr(3, 2) + e.substr(0, 2) }, type: "numeric" }), $.tablesorter.addParser({ id: "Titel", is: function(e) { return !1 }, format: function(e) { return "knauf " == e.substr(0, 6).toLowerCase() && (e = e.substr(6, e.length)), e.toLowerCase() }, type: "text" }), $(t).tablesorter(e), select_sort(t, i), $(t + " th").live("click", function() {
        this_index = $(this).index(), $.each($(i).next("dl").find("ul li"), function() { $(this).removeClass("active") });
        var e = $(i).next("dl").find("ul li").eq(this_index + 1).children("a").html();
        $(i).next("dl").find("dt a span").html(e), $(i).next("dl").find("ul li").eq(this_index + 1).addClass("active")
    })
}

function select_sort(t, e) {
    $(e).change(function() {
        var e = [
            [$(this).val(), 0]
        ];
        $(t).trigger("sorton", [e])
    })
}

function infopaketeAngleichen(e, t) {
    var i = 4;
    if (full_width < mobileSize && (i = 2), full_width < tabSize && (i = 1), t == i) return !1;
    elements = $("#" + e + " .infopaket");
    for (var n = 0; n < elements.length; n += i) {
        var a = elements.slice(n, n + i),
            o = -1;
        a.each(function(e, t) {
            $(".titleWrapper", this).height("auto");
            var i = $(".titleWrapper p", this).height();
            o = o < i ? i : o
        }), a.each(function(e, t) { $(".titleWrapper", this).height(o + "px") })
    }
    $(window).resize(function() { window.clearTimeout(infopaketeAngleichenTimer), infopaketeAngleichenTimer = window.setTimeout(function() { infopaketeAngleichen(e, i) }, 200) })
}

function infopaketePreis(e, t) { $("#" + e + " .infopaket").click(function() { infopaketePreisMachen(e, t) }), infopaketePreisMachen(e, t) }

function infopaketePreisMachen(e, t) {
    var i = 0;
    $("#" + e + " input[type=checkbox]:checked").each(function(e, t) { _localPreis = $(t).attr("title"), _localPreis && (i += parseFloat($(t).attr("title"))) }), i = (i = i.toFixed(2)).replace(".", ","), $("#" + t + " span").html(i)
}

function textareaMaxlength(e, t) {
    $("#" + e + "[maxlength]").bind("input propertychange", function() {
        var e = parseInt($(this).attr("maxlength"));
        $(this).val().length > e && $(this).val($(this).val().substr(0, e)), t && $("#" + t + " span").html(e - $(this).val().length)
    }).trigger("propertychange")
}

function showHideFormElement(e, t, i, n) { $("#" + e + " span").click(function() { showHideFormElementDo(e, t, i, n) }), $("#" + e + " input[type=radio]").change(function() { showHideFormElementDo(e, t, i, n) }), showHideFormElementDo(e, t, i, n) }

function showHideFormElementDo(e, t, i, n) { $("#" + t).is(":checked") ? ($("#" + n).show(), $("#" + n).addClass("show"), $("#" + n).removeClass("hide")) : ($("#" + n).hide(), $("#" + n).removeClass("show"), $("#" + n).addClass("hide")) }

function kapitelHandler(e, t) {
    var i = $("#" + e);
    $(".buttonlink.showHideBelow", i).attr("onclick", 'return kapitelDo("' + e + '", "' + t + '")'), "showsection" == window.location.hash.substring(1, 12) && e == window.location.hash.substring(13) && -1 == window.location.hash.substring(13).indexOf("-") && $(document).ready(function() { $(".buttonlink.showHideBelow", i).click(), calculatedJump(i) })
}

function kapitelDo(e, t) { var i, n = $("#" + e); return n.hasClass("open") ? (n.removeClass("open"), $(".buttonlink.showHideBelow", n).removeClass("active"), i = "hide") : (n.addClass("open"), $(".buttonlink.showHideBelow", n).addClass("active"), i = "show"), _levelNew = t, _levelNew--, "show" == i ? $("tr.level" + t + "[id|=" + e + "]").each(function(e, t) { $(t).show(), $(t).addClass("visible") }) : $("tr[id|=" + e + "]:not(.level" + _levelNew + ")").each(function(e, t) { $(t).hide(), $(t).removeClass("visible"), $(t).removeClass("open"), $(".buttonlink.showHideBelow", t).removeClass("active") }), !1 } - 1 != $.inArray("dough", loadedJQModules) && -1 != $.inArray("merkzettelnav", loadedJQModules) && $(document).ready(function() { initMerkzettel() }), $(window).resize(function() { set_footer_height() }), $(document).ready(function() { set_footer_height() });
var targetGroupCookieName = "tg";

function setTargetGroupCookie(e) { deleteTargetGroupCookie(), Cookies.set(targetGroupCookieName, e, { expires: 365 }) }

function getTargetGroupCookie() { return Cookies.get(targetGroupCookieName) }

function deleteTargetGroupCookie() { Cookies.remove(targetGroupCookieName, { domain: "." + location.host }), Cookies.remove(targetGroupCookieName) }

function watchTargetGroupLinks(e) {
    $("#" + e + " a").each(function(e, t) {
        var i = "#" + $(t).attr("tgvp"),
            n = $(t).attr("tgid");
        addTargetGroupSetterLinks(i, n), $(i).nextUntil("section.viewport.img.contextnav", "section").each(function(e, t) { addTargetGroupSetterLinks("#" + $(t).attr("id"), n) }), $("#contextNav a[href=" + i + "]").click(function() { setTargetGroupCookie(n) })
    })
}

function addTargetGroupSetterLinks(e, i) { $(e + " a").each(function(e, t) { $(t).hasClass("viewportLink") || $(t).hasClass("sliderNext") || $(t).hasClass("sliderPrev") || $(t).hasClass("sliderPage") || $(t).attr("onclick", "handleTargetGroupViewportLinkClick(" + i + ")") }) }

function handleTargetGroupViewportLinkClick(e) { getTargetGroupCookie() || setTargetGroupCookie(e) }

function handleTargetGroupMenu(e) { var t = !1; return (getTargetGroupCookie() || $("body#home").length) && (t = !0), setTargetGroupCookie(e), $("#navHome").removeClass("active"), $(".tgFlyout").hide(), t }
var tg_flyout_autoexpand = !1,
    tg_flyout_mobile = !0;

function tgFlyoutReady() {
    if (!$("nav#mainNav").length) return !1;
    var e = getTargetGroupCookie();
    if (e) {
        var t = $(".tgFlyout li[tgid=" + e + "]");
        if (0 == t.size()) deleteTargetGroupCookie();
        else {
            var i = $("a", t).html();
            $(t).remove(), $(".tgFlyout h3").html(i)
        }
        $("body#home").length && $(".tgFlyout #tgHomeLink").remove()
    } else $(".tgFlyout h2").html(lang_tg_addressyou), $(".tgFlyout h3").html(lang_tg_choose), $(".tgFlyout #tgHomeLink").remove(), !$("body#home").length && tg_flyout_autoexpand && (!0 === tg_flyout_mobile ? $("#navHome").addClass("active") : "15px" != $("header h1").css("font-size") && "16px" != $("header h1").css("font-size") && "15px" != $("header .brand").css("font-size") && "16px" != $("header .brand").css("font-size") || $("#navHome").addClass("active"))
}

function checkTargetGroupPage() {
    var e = getTargetGroupCookie();
    if (e) {
        var t = $(".tgFlyout li[tgid=" + e + "]");
        if (0 == t.size()) deleteTargetGroupCookie();
        else {
            var i = $("a", t).attr("href");
            3777 == e && "/" == i && (i = "/profi"), document.location.href = i
        }
    }
}

function enableLanguageSwitch() { initMobilLangChanger(), open_lang_switch(), resize_lang_switch(), $(window).resize(function() { resizeLangHandler() }) }
var resize_lang_switch = function() { "14px" == $("header h1").css("font-size") || "14px" == $("header .brand").css("font-size") ? $("#jumpLanguage + a").show() : $("#jumpLanguage + a").hide() },
    rwdResizeLangTimer, resizeLangHandler = function() { window.clearTimeout(rwdResizeLangTimer), rwdResizeLangTimer = window.setTimeout(function() { resize_lang_switch() }, 150) },
    open_lang_switch = function() { $("#navLanguage").click(function() { var e = $(this).position(); return $(".language_flyout").css("top", e.top - 33), $(".language_flyout").children(".pfeil").css("top", 40), $(".language_flyout").show(), $(this).addClass("active_flyout"), !1 }), $(".language_flyout .flyout_closer").click(function() { return $(".language_flyout").hide(), !1 }) },
    initMobilLangChanger = function() {
        var e = $(".language_flyout .info a.buttonlink").html(),
            t = $(".language_flyout .info a.buttonlink").attr("href"),
            i = $(".language_flyout .info a.buttonlink").attr("class");
        i = i.replace("buttonlink ", ""), $("#navLanguage").append('<a href="' + t + '" class="langbutton ' + i + '">' + e + "</a>");
        var n = $("#contextNav ul").width();
        "rtl" === layout_direction ? "14px" != $("header h1").css("font-size") && "14px" != $("header .brand").css("font-size") || $("#contextNav ul").css("left", -1 * n) : "14px" != $("header h1").css("font-size") && "14px" != $("header .brand").css("font-size") || $("#contextNav ul").css("right", -1 * n)
    },
    slideCount = 0,
    langSliderFunctionIsSet = !1;

function enableMultiLanguageSwitch() { initLangSelect(), initlangSlider(), open_multi_lang(), resize_multi_lang_switch(), $(window).resize(function() { resizeMultiLangHandler() }) }
var resize_multi_lang_switch = function() {
        if ("14px" == $("header h1").css("font-size") || "14px" == $("header .brand").css("font-size")) $("#jumpLanguage + div").removeClass("hidden"), $(".multilanguage_flyout").hide(), $("#contextNav ul").css("right", "-250px");
        else {
            $("#jumpLanguage + div").addClass("hidden");
            var t = 0,
                i = 0;
            $(".lang_slider ul li").each(function(e) { '<a id="lang_carousel-page' + e + '" class="sliderPage" href="#"></a>', t += parseInt($(this).width()), i = parseInt($(this).width()), slideCount++ }), 1 < slideCount && ($(".lang_slider ul").css("width", t), langSliderFunctionIsSet || (langSliderFunctionIsSet = !0, langSliderFunction(i))), $("#contextNav ul").css("right", 0)
        }
    },
    rwdResizeMultiLangTimer, resizeMultiLangHandler = function() { window.clearTimeout(rwdResizeMultiLangTimer), rwdResizeMultiLangTimer = window.setTimeout(function() { resize_multi_lang_switch() }, 50) },
    initLangSelect = function() {
        var e = $(".lang_slider a"),
            t = '<div class="styledSelect"><select id="lang_select" name="lang_select"><option>' + $("#jumpLanguage span").attr("name") + "</option>";
        $.each(e, function() {
            $(this).attr("class");
            t += '<option rel="' + $(this).attr("href") + '">' + $(this).html() + "</option>"
        }), t += "</select></div>", $("#navLanguage").append(t), $(".new_flyout").children(".info").children(".langDropdown").append(t), createStyledSelects(), $("html").hasClass("isMobileDevice") && $("#navLanguage .mobile_device_select").css("width", "200px");
        var i = $("#contextNav ul").width();
        "rtl" === layout_direction ? "14px" != $("header h1").css("font-size") && "14px" != $("header .brand").css("font-size") || $("#contextNav ul").css("left", -1 * i) : "14px" != $("header h1").css("font-size") && "14px" != $("header .brand").css("font-size") || $("#contextNav ul").css("right", -1 * i), $("#navLanguage select").live("change", function() {
            var e = $("#navLanguage select option:selected").attr("rel");
            window.location.href = e
        })
    },
    open_multi_lang = function() { $("#navLanguage #jumpLanguage").click(function() { var e = $(this).position(); return $(".multilanguage_flyout").css("top", e.top - 33), $(".multilanguage_flyout").children(".pfeil").css("top", 40), $(".multilanguage_flyout").show(), $(this).addClass("active_flyout"), !1 }), $(".multilanguage_flyout .flyout_closer").click(function() { return $(".multilanguage_flyout").hide(), !1 }) };

function initlangSlider() {
    var t = "",
        i = 0,
        n = 0;
    if ($(".lang_slider ul li").each(function(e) { t += '<a id="lang_carousel-page' + e + '" class="sliderPage" href="#"></a>', i += parseInt($(this).width()), n = parseInt($(this).width()), slideCount++ }), 1 < slideCount) {
        var e = '<div class="slider lang_slider_control" style=""><a class="sliderPrev" href="#">prev</a><span class="sliderPagerContainer">' + t + '</span><a class="sliderNext" href="#">next</a></div>';
        $(".lang_slider").after(e), $(".lang_slider ul").css("width", i), 0 !== n && (langSliderFunctionIsSet = !0, langSliderFunction(n))
    }
}

function langSliderFunction(i) {
    var n = 1;
    $(".lang_slider_control .sliderNext").click(function() { return n < slideCount && $(".lang_slider ul").animate({ left: i * n * -1 }, 1e3, function() { n++, $(".sliderPagerContainer a").removeClass("active"), $("#lang_carousel-page" + (n - 1)).addClass("active") }), !1 }), $(".lang_slider_control .sliderPrev").click(function() { return 1 < n && $(".lang_slider ul").animate({ left: parseInt($(".lang_slider ul").css("left")) + i }, 1e3, function() { n--, $(".sliderPagerContainer a").removeClass("active"), $("#lang_carousel-page" + (n - 1)).addClass("active") }), !1 }), $(".sliderPage").click(function() {
        var e = parseInt($(this).attr("id").substr(18)) + 1;
        if (n !== e) {
            if (n < e) {
                var t = e - 1;
                $(".lang_slider ul").animate({ left: i * t * -1 }, 1e3, function() { n = e, $(".sliderPagerContainer a").removeClass("active"), $("#lang_carousel-page" + (n - 1)).addClass("active") })
            }
            if (e < n) {
                t = n - e;
                $(".lang_slider ul").animate({ left: parseInt($(".lang_slider ul").css("left")) + i * t }, 1e3, function() { n = e, $(".sliderPagerContainer a").removeClass("active"), $("#lang_carousel-page" + (n - 1)).addClass("active") })
            }
        }
        return !1
    })
}

function printCSS() {
    var e = $(".kontakt_flyout .info").html();
    $("footer.mainFooter").before('<div class="contact_footer">' + e + "</div>"), $(".contact_footer").css("display", "none"), $(".print_section_only .buttonlink.print").click(function() { return $("section").each(function() { $(this).addClass("no-print") }), $(this).parents("section").removeClass("no-print"), window.print(), $("section").each(function() { $(this).removeClass("no-print") }), !1 })
}

function rwdLinkList(e) { $(document).ready(function() { _container = $("#" + e).parentsUntil("section.viewport").parent(), _container.after('<section class="linkList" id="' + e + '2">' + $("#" + e).html() + "</section>"), $("#" + e + "2").wrap('<section class="viewport txt iPhoneLinkList"><div class="wrapperinner"></div></section>') }) }

function rwdVpTextWrapper(e) { $(document).ready(function() { _container = $("#" + e).parent(), _container.next().hasClass("someBarWrapper") ? afterElement = _container.next() : afterElement = _container, afterElement.after('<section class="vpTextWrapper" id="' + e + '2">' + $("#" + e).html() + "</section>"), $("#" + e + "2").wrap('<section class="viewport txt iPhoneVpTextWrapper"><div class="wrapperinner"></div></section>') }) }

function getBreadcrumbPath(e) { var t = ""; return "[1]" == e && (tgName = targetGroups[getTargetGroupCookie()], t = void 0 !== tgName ? "TG_" + tgName + "/" : "TG_default/"), t += breadcrumbPath.join("/") }

function econdaDownload(e) {
    if (void 0 === window.emosPropertiesEvent) return !1;
    if (void 0 === window.emospro && (window.emospro = {}), -1 != e.indexOf("http")) {
        var t = e.split("/");
        e = t[t.length - 1]
    }
    window.emospro.download = e, emospro.rqtype = "hiddenpi", window.emosPropertiesEvent(window.emospro)
}

function googleEvent(e, t, i, n) { ga("send", "event", e, t, i, parseInt(n)) }

function logDownload(e) { if (-1 != $.inArray("ec", loadedTrackers)) { var t = ""; "lng" in e && (t += e.lng + "/"), "cat" in e && (t += e.cat + "/"), t += e.name, "id" in e && (t += " (" + e.id + ")"), econdaDownload(t) } - 1 != $.inArray("ga", loadedTrackers) && googleEvent("Downloads", e.cat, e.name + ("lng" in e ? "/" + e.lng : "") + ("id" in e ? " (" + e.id + ")" : ""), "id" in e ? e.id : "") }

function logMarker(e) { if (-1 != $.inArray("ec", loadedTrackers)) { if (void 0 === window.emosPropertiesEvent) return !1; "undefined" != typeof econda_marker_prefix ? emospro.marker = econda_marker_prefix + "/" + e : emospro.marker = e, emospro.rqtype = "hiddenpi", window.emosPropertiesEvent(emospro) } }

function econdaAddTG(e) {
    "undefined" != typeof use_new_tg ? tgName = targetGroups[get_new_TargetGroupCookie()] : tgName = targetGroups[getTargetGroupCookie()], void 0 !== tgName && (e.kgroupre = e.kgroupse = [
        [tgName]
    ])
}

function econdaFormatName(e) { return $("<div/>").html(e).text() }
var rwdResizePreventers = 0,
    oldSizeMarker, rwdResizeTimer, rwdResizeViewportTimer;

function checkRwdResizePreventers() {
    var e = $(".preventRwdResize");
    rwdResizePreventers += e.length;
    var t = $("section form").not("div.social_placeholder form");
    rwdResizePreventers += t.length
}

function resizeResponsiveHandler() { oldSizeMarker || (oldSizeMarker = $("header .brand").css("font-size"), 0 < $("header h1").length && (oldSizeMarker = $("header h1").css("font-size"))), window.clearTimeout(rwdResizeTimer), rwdResizeTimer = window.setTimeout(function() { resizeResponsive() }, 250) }

function resizeResponsive() {
    var e = $("header .brand").css("font-size");
    0 < $("header h1").length && (e = $("header h1").css("font-size")), e != oldSizeMarker && 0 == rwdResizePreventers && window.location.reload()
}

function cancelEvent(e) { return (e = e || window.event).stopPropagation && e.stopPropagation(), !(e.cancelBubble = !0) }

function resizeViewportStoererHandler() { window.clearTimeout(rwdResizeViewportTimer), rwdResizeViewportTimer = window.setTimeout(function() { resizeViewportStoerer(), resizeViewportImage(), resizeViewportLinklist() }, 250) } window.addEventListener && window.addEventListener("orientationchange", function() { 0 == rwdResizePreventers && window.location.reload() }, !1), $(document).ready(function() { checkRwdResizePreventers() }), $(window).resize(function() { resizeResponsiveHandler() }), $(document).ready(function() { resizeViewportStoerer(), resizeViewportImage(), resizeViewportLinklist() }), $(window).resize(function() { resizeViewportStoererHandler() });
var resizeViewportStoerer = function() {
        var e = $(".viewport_stoerer");
        $.each(e, function() {
            if ($(this).next(".vpTextWrapper").length && $(window).width() > tabSize && $(window).width() < mobileSize) {
                var e = $(".viewport h2.text").height();
                $(this).css("top", 100 + e + 12)
            } else $(this).removeAttr("style");
            var t = ($("body").width() - $(".wrapperinner").width()) / 2;
            "rtl" === layout_direction ? $(this).css("right", t) : $(this).css("left", t)
        })
    },
    resizeViewportImage = function() {
        var e = $(".vpImageWrapper");
        $(window).width();
        $.each(e, function() {
            var e = $("body").width(),
                t = $(".wrapperinner").width(),
                i = 6;
            mobileSize < full_width && (i = (e - t) / 2), "rtl" === layout_direction ? $(this).css("left", i) : $(this).css("right", i)
        })
    },
    resizeViewportLinklist = function() {
        var e = $(".linkList");
        $(window).width();
        if ($(e).hasClass("full-width-linkList")) {
            var t = $(".full-width-linkList");
            $.each(t, function() {
                var e = $("body").width(),
                    t = $(".wrapperinner").width(),
                    i = 6;
                mobileSize < full_width && (i = (e - t) / .5), "rtl" === layout_direction ? $(this).css("left", i) : $(this).css("right", i)
            })
        } else $.each(e, function() {
            var e = $("body").width(),
                t = $(".wrapperinner").width(),
                i = 6;
            mobileSize < full_width && (i = (e - t) / 2), "rtl" === layout_direction ? $(this).css("left", i) : $(this).css("right", i)
        })
    };

function getParameterByName(e) { e = e.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]"); var t = new RegExp("[\\?&]" + e + "=([^&#]*)").exec(location.search); return null === t ? "" : decodeURIComponent(t[1].replace(/\+/g, " ")) }
"function" != typeof String.prototype.trim && (String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, "") }), Date.now || (Date.now = function() { return (new Date).getTime() });

/*var cookie_message_function = function() {
    if (init_globalCookie("social_reminder", "0"), void 0 === getCookie("globalCookie").cookie_reminder)
        if ($("body").hasClass("cm_top"))
            $("body").css("margin-top", $(".cookie_message").height()), "15px" != $("header h1").css("font-size") && "16px" != $("header h1").css("font-size") && "15px" != $("header .brand").css("font-size") && "16px" != $("header .brand").css("font-size") || $("#contextNav").css("margin-top", $(".cookie_message").height()), $(".cookie_message").css("display", "block"), $(".cookie_message .box_closer, .cookie_message .cookie_message_content a").click(function() {
                edit_globalCookie("cookie_reminder", "true"), $("body, .cookie_message").removeAttr("style"), $("#contextNav").css("margin-top", 0)
            });
        else {
            setTimeout(function() { $(".cookie_message").slideDown(700) }, 1e3);
            $(".cookie_message .box_closer, .cookie_message .cookie_message_content a").click(function() {
                edit_globalCookie("cookie_reminder", "true"), $(".cookie_message").slideUp(500)
            })
        }
};

$(window).resize(function() { cookie_message_function() });*/

var getCookie = function(e) { return Cookies.getJSON(e) },
    init_globalCookie = function(entry, value) {
        if (globalCookie = Cookies.getJSON("globalCookie"), null != globalCookie) {
            globalCookie = Cookies.getJSON("globalCookie");
            var my_entry = eval("globalCookie." + entry);
            my_entry || (eval('var helper_object = {"' + entry + '" : "' + value + '" }'), $.extend(globalCookie, helper_object), Cookies.set("globalCookie", globalCookie, { expires: 365 }))
        } else Cookies.set("globalCookie", { social_reminder: "0" }, { expires: 365 }), globalCookie = Cookies.getJSON("globalCookie")
    },
    edit_globalCookie = function(entry, value) {
        var my_entry = eval("globalCookie." + entry);
        my_entry != value && (eval("globalCookie." + entry + ' = "' + value + '"'), Cookies.set("globalCookie", globalCookie, { expires: 365 }))
    },
    countdown_teaser_function = function() {
        if ("15px" == $("header h1").css("font-size") || "16px" == $("header h1").css("font-size") || "15px" == $("header .brand").css("font-size") || "16px" == $("header .brand").css("font-size")) {
            var e = $("dl.countdown, a.big_teaser_countdown, span.big_teaser_countdown");
            $.each(e, function() {
                var e, t = $(this).data("date"),
                    i = new Date(t),
                    n = new Date,
                    a = DateDiff.inDays(i, n),
                    o = DateDiff.inWeeks(i, n),
                    r = DateDiff.inMonths(i, n);
                e = a < 30 ? 1 === a ? lang_countdown_day.replace("###", a) : lang_countdown_days.replace("###", a) : o < 10 ? 1 === o ? lang_countdown_week.replace("###", o) : lang_countdown_weeks.replace("###", o) : 1 === r ? lang_countdown_month.replace("###", r) : lang_countdown_months.replace("###", r), $(this).is("dl") ? 0 === $(this).children("dt").find("a") ? $(this).children("dt").children("a").append('<span class="countdown_stoerer">' + e + "</span>") : $(this).children("dt").append('<span class="countdown_stoerer">' + e + "</span>") : $(this).append('<span class="countdown_stoerer">' + e + "</span>")
            })
        }
    },
    DateDiff = {
        inDays: function(e, t) {
            var i = t.getTime(),
                n = e.getTime();
            return Math.ceil((n - i) / 864e5)
        },
        inWeeks: function(e, t) {
            var i = t.getTime(),
                n = e.getTime();
            return Math.ceil((n - i) / 6048e5)
        },
        inMonths: function(e, t) {
            var i = t.getFullYear(),
                n = e.getFullYear(),
                a = t.getMonth();
            return e.getMonth() + 12 * n - (a + 12 * i) + 1
        }
    },
    current_site_is_diy_home = 0,
    current_site_is_home = 0,
    current_site_target = 0;
if (!0 === $("body").hasClass("dev")) var dev = !0;
else var dev = !1;
if (!0 === dev) var diy_target = [1111, 3777];
var targetGroupsName = [],
    special_tg = 0,
    not_assigned_tg = 99999,
    targets_text, targets = {},
    pro_targets = {},
    cookieTargetGroup, infoCookie, current_language, target_group = 0,
    init_click = 0,
    new_targetGroupCookieName = "new_tg",
    infoWindowAliveTime = 5e3,
    fadeDuration = 1e3,
    load_TargetGroupTemplate = function() { $.ajax({ url: targetGroupTemplateURL, dataType: "html", success: function(e) { window.target_group_template = e, init_TargetGroup() }, error: function(e, t, i) { console.error(e), console.error(t), console.error(i) } }) },
    init_TargetGroup = function() {
        var i;
        if ((targets_text = targetGroups.slice())[special_tg] = "Sonstiges", targets_text[not_assigned_tg] = "Unzugeordnet", current_site_is_home = $("body#home").length, $("body").hasClass("diy") && 0 < $("body#home").length ? (current_site_is_diy_home = 1, current_site_target = diy_target[0]) : current_site_target = $("body").hasClass("diy") ? diy_target[0] : "pro", cookieTargetGroup = get_new_TargetGroupCookie(), infoCookie = get_new_InfoCookie(), current_language = $("html").attr("lang"), buildTargets(), setTargetGroupWindowSize(), initJson(), null != cookieTargetGroup && cookieTargetGroup != special_tg && cookieTargetGroup != not_assigned_tg && (0 < $("header h1 a").length ? $("header h1 a").attr("href", targets[cookieTargetGroup].url) : $("header .brand a").attr("href", targets[cookieTargetGroup].url)), $.each(pro_targets, function(e, t) { e == cookieTargetGroup && (i = "pro") }), 1 === current_site_is_diy_home) {
            if ($(".showTargetGroupText").removeClass("noTargetGroup").addClass("diy"), cookieTargetGroup == diy_target[0]) return !0 === dev && console.log("Ich bin DIY und hab die DIY-Startseite aufgerufen -> Startseite-DIY wird ausgegeben"), !0;
            openText(), set_new_TargetGroupCookie(diy_target[0], 365), !0 === dev && console.log("User hat die DIY Startseite aufgerufen, war aber einer anderen Zielgruppe zugeordnet, somit wurde er jetzt automatisch der ZG-DIY zugeordnet, InfoLayer wurde geöffnet -> Startseite-DIY wird ausgegeben")
        } else if (1 === current_site_is_home)
            if (void 0 === cookieTargetGroup) $(".showTargetGroupText").removeClass("pro diy").addClass("noTargetGroup").click(), !0 === dev && console.log("User hat globale Startseite aufgerufen, hatte aber noch keinen Cookie, somit öffnet sich das Zielgruppen-Overlay");
            else { if (cookieTargetGroup == special_tg || cookieTargetGroup == not_assigned_tg) return $(".showTargetGroupText").removeClass("noTargetGroup").addClass("pro"), !0 === dev && console.log('Anzeigen der aktuellen Globalen Startseite, da User den Zielgruppen: "Sonstige" oder "Unzugeordnet" angehört'), !0;!0 === dev && console.log("User ist einer der 4 Zielgruppen zugeordnet -> Weiterleitung auf entsprechende COOKIE-STARTSEITE => " + targets[cookieTargetGroup].url), window.location.href = targets[cookieTargetGroup].url }
        else void 0 === cookieTargetGroup ? ($(".showTargetGroupText").removeClass("pro diy").addClass("noTargetGroup").click(), !0 === dev && console.log("User hat DeepPage oder Zielgruppen-Startseite (außer DIY) aufgerufen, hatte aber noch keinen Cookie, somit öffnet sich das Zielgruppen-Overlay")) : cookieTargetGroup == current_site_target || void 0 !== infoCookie || i == current_site_target ? ("pro" === current_site_target ? $(".showTargetGroupText").removeClass("noTargetGroup").addClass("pro") : $(".showTargetGroupText").removeClass("noTargetGroup").addClass("diy"), !0 === dev && console.log("User hat DeepPage oder Zielgruppen-Startseite (außer DIY) aufgerufen, Cookie vorhanden und Cookie-Zielgruppe = User-Ziegruppe oder InfoLayer wurde in dieser Session schon angezeigt -> einfaches Anzeigen der entsprechenden Seite")) : (openText(), "pro" === current_site_target ? $(".showTargetGroupText").removeClass("noTargetGroup").addClass("pro") : $(".showTargetGroupText").removeClass("noTargetGroup").addClass("diy"), !0 === dev && console.log("User hat DeepPage oder Zielgruppen-Startseite (außer DIY) aufgerufen, Cookie vorhanden und Cookie-Zielgruppe != User-Ziegruppe oder InfoLayer wurde in dieser Session noch nicht angezeigt -> öffnen InfoLayer + einfaches Anzeigen der entsprechenden Seite"));
        dev && ($("#user_tg").html(cookieTargetGroup), $("#site_tg").html(current_site_target), $("#site_home").html(current_site_is_home), $("#site_diy_home").html(current_site_is_diy_home), $("#cookie_info").html(infoCookie))
    },
    buildTargets = function() {
        var i = $("#navHome");
        $.each(targetGroups, function(e, t) { t && (e != diy_target[0] && e != special_target[0] && (pro_targets[e] = {}, pro_targets[e].name = t), targets[e] = {}, targets[e].name = t, targets[e].url = $(i).find('[tgid="' + e + '"]').children("a").attr("href"), 0) }), init_html()
    },
    init_html = function() {
        $("#navHome");
        var i = "",
            n = "",
            a = "";
        $.each(targets, function(e, t) { check_array(diy_target, e) ? n += '<dl class="trigger_group ' + t.name + '" data-group="' + e + '"><dt></dt></dl>' : check_array(special_target, e) ? a += '<dl class="trigger_group ' + t.name + '" data-group="' + e + '"><dt></dt></dl>' : i += '<dl class="trigger_group ' + t.name + '" data-group="' + e + '"><dt></dt></dl>' }), $("#navHome").remove(), $("#mainNav > ul").prepend('<li id="navHome" class="hasActive"><a href="#" class="showTargetGroupText" onclick="javascript:open_tg_fancy(event)"><span>Home</span><span class="targetGroupClassPro">PRO</span><span class="targetGroupClassDiy">DIY</span></a><div class="textPopup"><span class="textClose"></span></div></li>'), target_group_template = target_group_template.replace("##tg_flyout_pro_bereich_text1##", tg_flyout_pro_bereich_text1), target_group_template = target_group_template.replace("##pro_links##", i), target_group_template = target_group_template.replace("##tg_flyout_diy_bereich_text1##", tg_flyout_diy_bereich_text1), target_group_template = target_group_template.replace("##diy_links##", n), "undefined" != typeof tg_flyout_special_bereich_text1 && (target_group_template = target_group_template.replace("##tg_flyout_special_bereich_text1##", tg_flyout_special_bereich_text1)), target_group_template = target_group_template.replace("##special_links##", a);
        var e = target_group_template;
        $("body").append(e), dev && $("body").append('<div style="position: absolute; bottom: 10px; right: 10px; background: red; width: 200px; height: 100px; color: #fff; font-size: 12px; padding: 10px;">UserTarget aus Cookie: <span id="user_tg"></span><br />SiteTarget: <span id="site_tg"></span><br />Ist Startseite: <span id="site_home"></span><br />Ist DIY-Startseite: <span id="site_diy_home"></span><br />InfoLayer-Cookie: <span id="cookie_info"></span></div>')
    },
    check_array = function(e, t) {
        for (var i = 0; i < e.length; i++)
            if (e[i] == t) return !0
    },
    open_tg_fancy = function(e) { e.preventDefault(), cookieTargetGroup = get_new_TargetGroupCookie(), setActiveTG(cookieTargetGroup), $.fancybox({ type: "inline", content: "#target_group_selection", padding: 0, showNavArrows: !1, onStart: function() { $("#fancybox-content").addClass("setFancyboxWidth"), $("#fancybox-wrap").addClass("setFancyboxWidth"), $("#fancybox-close").addClass("targetGroup_closer") }, onComplete: function() { init_targetGroup_window() } }) },
    rwdResizeTimer;
$(window).resize(function() { resizeHandler() });
var resizeHandler = function() { window.clearTimeout(rwdResizeTimer), rwdResizeTimer = window.setTimeout(function() { setTargetGroupWindowSize() }, 150) },
    init_targetGroup_window = function() { 0 === init_click && ($(".trigger_group").click(function() { target_group = $(this).attr("data-group"), $("body").attr("data-targetgroup", target_group), $(".showTargetGroupText").removeClass("pro diy"), "pro" === current_site_target ? $(".showTargetGroupText").removeClass("noTargetGroup").addClass("pro") : $(".showTargetGroupText").removeClass("noTargetGroup").addClass("diy"), $.fancybox.close(), void 0 === cookieTargetGroup ? (set_new_TargetGroupCookie(target_group, 365), !0 === dev && console.log("Anzeigen der angeforderten Zielseite -> Cookie wurde auf entsprechenden geklickten Target gesetzt"), 1 === current_site_is_home && (-1 !== targets[target_group].url.indexOf("http") ? window.open(targets[target_group].url, "_blank") : window.location.href = targets[target_group].url)) : (set_new_TargetGroupCookie(target_group, 365), !0 === dev && console.log("Weiterleiten auf die Startseite der neu gewählten Zielgruppe => " + targets[target_group].url), -1 !== targets[target_group].url.indexOf("http") ? window.open(targets[target_group].url, "_blank") : window.location.href = targets[target_group].url) }), $("body").on("click", "#fancybox-close", function() { void 0 === cookieTargetGroup && (target_group = special_tg, $(".showTargetGroupText").removeClass("pro diy").addClass("noTargetGroup"), $("body").attr("data-targetgroup", target_group), set_new_TargetGroupCookie(target_group, 28), $(".showTargetGroupText").removeClass("pro diy"), "pro" === current_site_target ? $(".showTargetGroupText").removeClass("noTargetGroup").addClass("pro") : $(".showTargetGroupText").removeClass("noTargetGroup").addClass("diy"), !0 === dev && console.log("Anzeigen der angeforderten Zielseite -> Cookie wurde auf Sonstige gesetzt")) }), $("body").on("click", "#fancybox-overlay", function() { void 0 === cookieTargetGroup && (target_group = not_assigned_tg, $(".showTargetGroupText").removeClass("pro diy").addClass("noTargetGroup"), $("body").attr("data-targetgroup", target_group), set_new_TargetGroupCookie(target_group, 28), $(".showTargetGroupText").removeClass("pro diy"), "pro" === current_site_target ? $(".showTargetGroupText").removeClass("noTargetGroup").addClass("pro") : $(".showTargetGroupText").removeClass("noTargetGroup").addClass("diy"), !0 === dev && console.log("Anzeigen der angeforderten Zielseite -> Cookie wurde auf Unzugeordnet gesetzt")) }), init_click = 1) },
    openText = function() {
        infolayer_target = "pro" !== current_site_target ? "DIY" : "PRO", econda_tg_tracking("knauf.de/Zielgruppen_showInfolayer", infolayer_target + " - " + targets_text[cookieTargetGroup], "content", "knauf.de");
        var e = $(".textPopup");
        e.fadeIn(fadeDuration).delay(infoWindowAliveTime).fadeOut(fadeDuration), $(".textPopup").children(".textClose").click(function() { e.hide() }), set_new_InfoCookie()
    },
    setTargetGroupWindowSize = function() { "16px" === $("header h1").css("font-size") || "16px" === $("header .brand").css("font-size") ? $("#target_group_selection").css("width", "774px") : "15px" === $("header h1").css("font-size") || "15px" === $("header .brand").css("font-size") ? $("#target_group_selection").css("width", "435px") : $("#target_group_selection").css("width", "270px") },
    initJson = function() {
        var e = tg_flyout_address_text,
            t = tg_flyout_pro_bereich_text2,
            i = tg_flyout_diy_bereich_text2,
            n = description_pro,
            a = description_diy;
        $("#target_group_selection").prepend("<h4>" + e + " </h4>"), $(".proGroups").find(".groupName").append(t), $(".diyGroups").find(".groupName").append(i), $(".textPopup").prepend('<span class="proText">' + n + " </span>"), $(".textPopup").prepend('<span class="diyText">' + a + " </span>"), $.each($(".trigger_group"), function(e, t) { $(this).append("<dd>" + targetGroupsName[$(this).data("group")] + "</dd>") })
    },
    setActiveTG = function(i) { $(".trigger_group dt").removeClass("active"), $.each($(".trigger_group"), function(e, t) { $(this).data("group") == i && $(this).children("dt").addClass("active") }) },
    set_new_TargetGroupCookie = function(e, t) {-1 !== targets[e].url.indexOf("http") ? (econda_tg_tracking("knauf.de/Zielgruppen_setCookie", targets_text[e], "content", "knauf.de"), Cookies.set(new_targetGroupCookieName, special_tg, { expires: t })) : (void 0 !== cookieTargetGroup && econda_tg_tracking("knauf.de/Zielgruppen_changeCookie", targets_text[cookieTargetGroup] + " -> " + targets_text[e], "content", "knauf.de"), econda_tg_tracking("knauf.de/Zielgruppen_setCookie", targets_text[e], "content", "knauf.de"), Cookies.set(new_targetGroupCookieName, e, { expires: t })) },
    set_new_InfoCookie = function() { Cookies.set("info", 1) },
    get_new_TargetGroupCookie = function() { return Cookies.get(new_targetGroupCookieName) },
    get_new_InfoCookie = function() { return Cookies.get("info") },
    getHashParameter = function(e) {
        if (e) {
            var t, i, n = decodeURIComponent(window.location.hash.substring(1)).split("&");
            for (i = 0; i < n.length; i++)
                if ((t = n[i].split("="))[0].toLowerCase() === e.toLowerCase()) return void 0 === t[1] || t[1]
        }
    },
    hashJumps = function() { getHashParameter("jump") && calculatedJump("#" + getHashParameter("jump")) },
    econda_tg_tracking = function(e, t, i, n) {!0 === dev ? window.emosPropertiesEvent({ Target: ["dev." + e, t, 1, "a"], rqtype: "hiddenpi", content: i, siteid: "dev." + n }) : window.emosPropertiesEvent({ Target: [e, t, 1, "a"], rqtype: "hiddenpi", content: i, siteid: "www." + n }) },
    create_fake_accordion_2 = function() { 0 < $(".togglelist").length && ($.each($(".togglelist"), function() { $(this).wrap('<div class="slide_container" style="display: none;"></div>') }), $("h4").on("click", function() { $(this).hasClass("active") ? ($(this).removeClass("active"), $(this).next(".slide_container").slideUp("slow")) : ($(this).next(".slide_container").slideDown("slow"), $(this).addClass("active")) })) },
    iframe_popup = function() { 0 < $(".iframePopup").length && $(".iframePopup").fancybox({ width: "100%", height: "100%", autoScale: !1, transitionIn: "none", transitionOut: "none", type: "iframe" }) },
    special_mobil_viewport = function() { $("ul.ki-ts-viewport-icons") },
    replace_svg_to_inline = function() {
        $("img.svg").each(function() {
            var i = $(this),
                n = i.attr("id"),
                a = i.attr("class"),
                e = i.attr("src");
            $.get(e, function(e) {
                var t = jQuery(e).find("svg");
                void 0 !== n && (t = t.attr("id", n)), void 0 !== a && (t = t.attr("class", a + " replaced-svg")), !(t = t.removeAttr("xmlns:a")).attr("viewBox") && t.attr("height") && t.attr("width") && t.attr("viewBox", "0 0 " + t.attr("height") + " " + t.attr("width")), i.replaceWith(t)
            }, "xml")
        })
    },
    fade_element = function() {
        var n = !1;
        0 < $(".parallax").length && $(window).scroll(function() {
            var e = $(".parallax").offset().top,
                t = $(window).height(),
                i = e - t + 400;
            $(window).scrollTop() >= e - t - 200 ? $(".para-img-holder").show() : $(".para-img-holder").hide(), $(window).scrollTop() >= i && !1 === n && (n = !0, $(".content-holder h3").css("visibility", "visible").hide().fadeIn(400, function() { $(".content-holder p").css("visibility", "visible").hide().fadeIn(), $(".content-holder .buttons-holder").css("visibility", "visible").hide().delay(250).fadeIn() })), $(window).scrollTop() <= e - t && (n = !1, $(".content-holder h3").css("visibility", "hidden"), $(".content-holder p").css("visibility", "hidden"), $(".content-holder .buttons-holder").css("visibility", "hidden"))
        })
    },
    //init_scroll2top = function() { $("body").append('<div class="scroll2top-container"><a href="#" class="scroll2top"></a></div>'), scroll2top() },
    //scroll2top = function() { $(window).scroll(function() { 300 < $(this).scrollTop() ? $(".scroll2top-container").fadeIn() : $(".scroll2top-container").fadeOut() }), $(".scroll2top").click(function(e) { return e.preventDefault(), $("html, body").animate({ scrollTop: 0 }, 600), !1 }) },
    count_element = function() {
        if (mobileSize < full_width) $(".counter").mouseenter(function() { counter_function() });
        else {
            var e = $("ul.counter");
            $(window).scroll(function() {
                var t = $(window).scrollTop();
                $.each(e, function() {
                    var e = $(this).offset().top;
                    (e -= 50) <= t && counter_function()
                })
            })
        }
    },
    counter_function = function() { $(".counter-number").each(function() { $(this).prop("Counter", 0).animate({ Counter: $(this).attr("data-counter") }, { duration: 4e3, easing: "linear", step: function(e) { $(this).children("h4").text(Math.ceil(e)) } }) }) },
    init_diyhaus_banner = function() {
        (0 <= window.location.href.indexOf(".de/diy/") || 0 <= window.location.href.indexOf("test_swingimage_4wande")) && ($(".mainHead").append('<div class="swingimage-holder v1"><a href="/diy/produkte/trockenausbau/gipsplatten-finder/#open_iframe" class="swingimage swingimage-v1"></a></div>'), $("#navMenu > ul").append('<li class="mob-swingimage-holder gotoPage"><a href="/diy/produkte/trockenausbau/gipsplatten-finder/#open_iframe" class="gotoPage diy-house"><div class="swingimage"></div></a></li>'), 0 <= window.location.hash.indexOf("open_iframe") && ($("#vpHome").click(), $(".swingimage-holder a").on("click", function() { $("#vpHome").click() }))), (0 <= window.location.href.indexOf("knauf.ru") || 0 <= window.location.href.indexOf("test_swingimage_25")) && ($(".mainHead").append('<div class="swingimage-holder v1"><a class="swingimage swingimage-v1" style=""></a></div>'), $("#navMenu > ul").append('<li class="mob-swingimage-holder gotoPage"><a class="gotoPage diy-house" style=""><div class="swingimage"></div></a></li>'), $(".swingimage-holder a").css("background", "none"))
    },
    compass_hack = function() { 0 <= window.location.hash.indexOf("hideNavigation=1") && $("header").remove() },
    video_viewport = function() {
        var e = $(".viewport-yt").find(".responsive-video");
        if (0 < e.length) {
            var t = "",
                i = "";
            $("html");
            $.each(e, function() { t = $(this).find("video"), i = "14px" == $("header h1").css("font-size") || "14px" == $("header .brand").css("font-size") ? $(this).find("input.mobile-video") : $(this).find("input.desktop-video"), $(this).find("source").remove(), $.each(i, function() { t.append('<source src="' + $(this).val() + '" type="' + $(this).attr("data-type") + '">') }) })
        }
    },
    facebook_helper = function() {
        var e = window.location.host;
        $("body").hasClass("diy") && "www.knauf.de" == e && $("#navFacebook").remove()
    },

    resize_frame_helper = function() {
        if (0 < $(".iframe_script").length) {
            var e = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);
            /iPad|iPhone|iPod/.test(navigator.userAgent) && window.MSStream;
            e && ($("#safariClick").show(), document.getElementById("safariClick").click());
            var t = $(".iframe_script"),
                i = 1400;
            ("14px" == $("header h1").css("font-size") || "14px" == $("header .brand").css("font-size")) && (i *= 1.6), t.css("height", i + "px")
        }

    };