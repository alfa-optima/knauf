//Проверяем, есть ли подменю

$(".main-nav__item").each(function() {
    if ($(this).children('.main-nav__item-dropdown').length > 0) {
        $(this).addClass('main-nav__item--dropdown');
    }
});


//Проверяем, есть ли подподменю

$(".main-nav__item-dropdown-item").each(function() {
    if ($(this).children('.main-nav__item-dropdown2').length > 0) {
        $(this).addClass('main-nav__item-dropdown-item--dropright');
    }

});



$(document).on('click', '.main-nav__menuButton-mobile', function() {
    $('body').toggleClass('showMenu');
    return false;
});

$(document).on('click', '.main-nav__item--close a', function() {
    $('body').removeClass('showMenu');
});


$(document).on('click', '.main-nav__search a', function() {
    $(this).parent().toggleClass('openSearch');

});

//////////////////////////////////////////////////////
$(document).on('click', 'li.searchPanel-nav__item--active .searchPanel-nav__item-link', function() {
    $('.searchPanel-nav__item').removeClass('searchPanel-nav__item--active');
    $('.searchPanel-nav__item-dropdown-item').removeClass('searchPanel-nav__item-dropdown-item--active');
    $(this).parent('.searchPanel-nav__item').addClass('searchPanel-nav__item--active');
    $(this).parent('.searchPanel-nav__item').toggleClass('showSubMenu');
    $(this).parent('.searchPanel-nav__item').removeClass('searchPanel-nav__item--arrowUp');
});
////////////////////////////////////////////////////

$(document).on('click', '.searchPanel-filter__item-link', function(e) {
    $(this).parent('.searchPanel-filter__item').toggleClass('showSubMenu');
})

$(document).on('click', '.searchPanel-nav__item-dropdown-link', function(e) {
    $('.searchPanel-nav__item-dropdown-item').removeClass('searchPanel-nav__item-dropdown-item--active');
    $('.searchPanel-nav__item').removeClass('searchPanel-nav__item--active');
    $(this).parent('.searchPanel-nav__item-dropdown-item').addClass('searchPanel-nav__item-dropdown-item--active');
    $(this).parents('.searchPanel-nav__item').addClass('searchPanel-nav__item--arrowUp');
});

// $(document).on('click', '.searchPanel__toggle', function(e){
//     $('.searchBlock').toggleClass('hideMenu');
// });

$(document).on('click', '.cart-offer__sortLine-price', function(e) {
    $(this).toggleClass('cart-offer__sortLine-price--toLess');
    $('.cart-offer__wrapper').addClass('cart-offer__wrapper--price');
    $('.cart-offer__wrapper').removeClass('cart-offer__wrapper--quantity');
});

$(document).on('click', '.cart-offer__sortLine-quantity', function(e) {
    $(this).toggleClass('cart-offer__sortLine-quantity--toLess');
    $('.cart-offer__wrapper').addClass('cart-offer__wrapper--quantity');
    $('.cart-offer__wrapper').removeClass('cart-offer__wrapper--price');
});



/////////////////////////////////////////////
/*$(document).on('click', '.view-switcher__button--list', function() {
    $('.searchBlock').addClass('listView');
});

$(document).on('click', '.view-switcher__button--grid', function() {
    $('.searchBlock').removeClass('listView');
});*/
/////////////////////////////////////////////


$(document).on('click', '.searchPanel li', function(e) {
    var searchPanelHeight = $('.searchPanel').height();
    $('.searchBlock__content').css('min-height', searchPanelHeight + 'px');
});


$(document).on('click', '.cart-offer__table-deliveryListItem', function(e) {
    $(this).siblings('.cart-offer__table-deliveryListItem').removeClass('cart-offer__table-deliveryListItem--active');
    $(this).addClass('cart-offer__table-deliveryListItem--active');
});


//////////////////////////
/*$(document).on('click', '.account-form__table-goodsQuantity', function(e) {
    $(this).parents('tr').next('tr').toggle();
    if ($(this).parents('tr').next('tr').css('display') == "table-row") {
        $(this).parents('tr').find('td').css('border-bottom', '0');
    } else {
        $(this).parents('tr').find('td').css({ 'border-bottom': '1px solid #ccc2b8' });
    }
});*/

$(document).on('click', '.account-form__table-goodsQuantity', function(e) {
    if(!$(this).parents(".dealerUnderOrderForm").length) {
         $(this).parents('tr').next('tr').toggle();
			
        if ($(this).parents('tr').next('tr').css('display') == "table-row") {
            $(this).parents('tr').find('td').css('border-bottom', '0');
            $(this).parents('tr').find('.account-form__table-goodsQuantity--change').text('Закрыть');

            $(this).parents('.account-form__table-mainRow').removeClass('account-form__table-mainRow--cancel');
        } else {
            $(this).parents('tr').find('td').css({'border-bottom': '1px solid #ccc2b8'});
            $(this).parents('tr').find('.account-form__table-goodsQuantity--change').text('Подробнее');
            $(this).parents('.account-form__table-mainRow').addClass('account-form__table-mainRow--cancel');

        }
    }
});

/*$('.account-form__table--requests .account-form__table-mainRow input').change(function() {
    if ($(this).val() === '0') {
        $(this).addClass('warning');
    } else {
        $(this).removeClass('warning');
    }
});*/
/////////////////////////

$(document).on('click', '.account-form__table--bids .account-form__table-goodsQuantity', function(e) {
    $(this).parents('tr').eq(0).next('tr').show();
});

$(document).on('click', '.account-form__table-order-btn--hide', function(e) {
    $(this).parents('.account-form__table-orderInner').hide();
    $(this).parents('.account-form__table-orderInner').prev('tr').find('td').css({ 'border-bottom': '1px solid #ccc2b8' });
});


$(document).on('click', ".account-form__selectAll", function(e) {
    $('[name="subscribe"]').attr("checked", "checked");
});

$(document).on('click', ".account-form__cancelAll", function(e) {
    $('[name="subscribe"]').removeAttr("checked");
});


$(document).on('click', "#subscribeToggle", function(e) {
    var subscribeBlockHeight = -1 * $('.subscribe__form').innerHeight();

    if ($('.subscribe').hasClass("subscribe__hidden")) {
        $('.subscribe').css({ bottom: 0 });
    } else {
        $('.subscribe').css({ bottom: subscribeBlockHeight });

    }
    $('.subscribe').toggleClass("subscribe__hidden");


});


///////////////////////////////////////
$(document).on('click', '.account-form__moreBtn', function() {
    $(this).parent('div').siblings('.account-form__hideBlock').toggle();
    $(this).toggleClass('account-form__moreBtn--show')

    if ($(this).hasClass('account-form__moreBtn--show')) {
        $(this).text('Свернуть');
        $(this).tooltipster('disable');
    } else {
        $(this).text('Дополнительные данные');
        $(this).tooltipster('enable');
    }
});
////////////////////////////////////////


//Открываем таб отзывов и скроллим до него (страница товара)

$(document).on('click', '.product__review-rating > span', function() {
    /*$('.product-tab').removeClass('active');
    $('.product-tab--review').addClass('active');
    $('.tab').css('display', 'none');
    $('.tab--review').css('display', 'block');
    var $container = $("html,body");
    var $scrollTo = $('.product-tab--review');
    $container.animate({ scrollTop: $scrollTo.offset().top - $container.offset().top + $container.scrollTop(), scrollLeft: 0 }, 300);*/

    $('.reviewVkl a').trigger('click');
    var destination = $('.product-tabs').offset();
    var panel_height = $('#panel').length>0?$('#panel').outerHeight(true):0;
    var header_height = $('header').outerHeight(true);
    var breadcrumbs_height = $('ol.breadcrumbs').outerHeight(true);
    $('body, html').animate({scrollTop: destination.top - panel_height - header_height - breadcrumbs_height}, 400);
});

$(document).on('click', '.tab--delivery a', function() {
    $('.product-tab').removeClass('active');
    $('.product-tab--addresses').addClass('active');
    $('.tab').css('display', 'none');
    $('.tab--map').css('display', 'block');
    var $container = $("html,body");
    var $scrollTo = $('.tab--map');
    $container.animate({ scrollTop: $scrollTo.offset().top - 150 }, 300);
});

//Открываем таб адресов точек продаж и скроллим до него (страница лк дилера)

/*$(document).on('click', '.dealerAccount-form__note--addressLink', function() {
    $('.account-form__tab').removeClass('active');
    $('.account-form__tab--address').addClass('active');
    $('.tab').css('display', 'none');
    $('.tab--address').css('display', 'block');
    var $container = $("html,body");
    var $scrollTo = $('.account-form__tab--address');
    $container.animate({ scrollTop: $scrollTo.scrollTop() }, 300);
});*/




$(document).on('click', ".dealerAccount-form__field-delete", function() {
    $(this).parent('.dealerAccount-form__field').hide();
});

///////////////////////////////
$(document).on("click", ".dealerAccount-form__addTel", function() {
    var clone = $(this).prev('.dealerAccount-form__field').clone(false);
    $(this).before(clone);
    $(this).prev('.dealerAccount-form__field').find('input.phone_input').val('');
    $(this).prev('.dealerAccount-form__field').find('input.phoneItem').val('');
    //$('input[type=tel]').mask('+7 (000) 000 00 00', { placeholder: "+7 (___) ___ __ __" });
});
//////////////////////////////

$(document).on('click', ".dealerAccount-delivery__index-addTime", function() {
    $(this).before($(this).prev('.dealerAccount-form__field').clone(true));
    $(this).prev('.dealerAccount-form__field').show();
});


//В Лк дилера во вкладке доставки добавляем строчку таблицы при клике на "Добавить вариант"

$(document).on('click', ".dealerAccount-delivery__index-addVariant", function() {
    var key = 0;
    if ($(this).parents('.distanceRangesBlock').find('.key___tr').length > 0) {
        $(this).parents('.distanceRangesBlock').find('.key___tr').each(function() {
            key = Number($(this).attr('key'));
        })
        key++;
    }
    $(this).prev('table').find('tbody').append('<tr class="key___tr" key="' + key + '"><td><input class="price___input" type="text" name="distance_ranges[distances][' + key + ']"></td><td><input class="price___input" type="text" name="distance_ranges[prices][' + key + '][<=0.5]"></td><td><input class="price___input" type="text" name="distance_ranges[prices][' + key + '][<=1.5]"></td><td><input class="price___input" type="text" name="distance_ranges[prices][' + key + '][<=3.5]"></td><td><input class="price___input" type="text" name="distance_ranges[prices][' + key + '][<=5]"></td><td><input class="price___input" type="text" name="distance_ranges[prices][' + key + '][<=10]"></td><td><input class="price___input" type="text" name="distance_ranges[prices][' + key + '][>10]"></td></tr>');
    $(this).prev('table').show();
});






$(document).on('click', ".dealerAccount-delivery__index-subtitle", function() {
    $(this).toggleClass('dealerAccount-delivery__index-subtitle--active');
    $(this).next().toggle();
});

$(document).on('click', '.dealerAccount-delivery__link', function(e) {
    $(this).parents('.dealerAccount-delivery__header').next().toggle();
    $(this).parents('.dealerAccount-delivery__header').toggleClass('dealerAccount-delivery__header--active')
});


//Выставление рейтинга в модальном окне отзыва

/* 1. При ховере */
$(document).on('mouseover', '#stars li', function() {
    console.log("o1");
    var onStar = parseInt($(this).data('value'), 10); // Звезда, на которую навели

    // Ей и всем предыдущим даем класс hover
    $(this).parent().children('li.star').each(function(e) {
        if (e < onStar) {
            $(this).addClass('hover');
        } else {
            $(this).removeClass('hover');
        }
    });

}).on('mouseout', '#stars li', function() {
    $(this).parent().children('li.star').each(function(e) {
        $(this).removeClass('hover');
    });
});



/* 2. при клике */
$(document).on('click', '#stars li', function() {
    console.log("o2");
    var onStar = parseInt($(this).data('value'), 10); // Звезда, на которую кликнули
    var stars = $(this).parent().children('li.star');

    for (i = 0; i < stars.length; i++) {
        $(stars[i]).removeClass('selected');
    }

    for (i = 0; i < onStar; i++) {
        $(stars[i]).addClass('selected');
    }

    return onStar // Возвращаем рейтинг

});


//Счетчик символов в модальном окне для написания отзыва

$('#textarea').bind('input', function() {
    var counter = (300 - $(this).val().length)
    $('#counter').html(counter);
});


$('.account-form textarea').bind('input', function() {
    var counter1 = (300 - $(this).val().length)
    $(this).siblings('.account-form__textarea-counter').find('span').html(counter1);
});

$('.dealerAccount-form__block textarea').bind('input', function() {
    var counter3 = (300 - $(this).val().length)
    $(this).siblings('.account-form__textarea-counter').find('span').html(counter3);
});

$('.cart-form__block textarea').bind('input', function() {
    var counter2 = (300 - $(this).val().length)
    $(this).siblings('.account-form__textarea-counter').find('span').html(counter2);
});


$('.suggestions textarea').bind('input', function() {
    var counter4 = (300 - $(this).val().length)
    $(this).siblings('.account-form__textarea-counter').find('span').html(counter4);
});


$('.modal-orderReview__block textarea').bind('input', function() {
    var counter5 = (300 - $(this).val().length)
    $(this).siblings('.account-form__textarea-counter').find('span').html(counter5);
});


$(document).on('click', 'input#freeDelivery, input#standartDelivery, input#expressDelivery', function() {
    if ($('input#freeDelivery').is(':checked') || $('input#standartDelivery').is(':checked') || $('input#expressDelivery').is(':checked')) {
        $('.account-form__wrapperMinusMargin--lightBcg').show();
    } else {
        $('.account-form__wrapperMinusMargin--lightBcg').hide();
    }

});


/*$(document).on('click', 'input#pickup1, input#courier', function() {
    if ($('input#pickup1').is(':checked')) {
        $('.cart-form__block--courier').hide();
        $('.cart-form__block--pickup').show();
    } else {
        $('.cart-form__block--courier').show();
        $('.cart-form__block--pickup').hide();
    }

});*/




$(document).on('click', '.main-nav__item--dropdown', function() {
    if ($(window).width() < 920) {
        $(this).toggleClass('showSubMenu');
        return false;
    }
});


$(document).on('click', '.main-nav__item-dropdown-item--dropright', function() {
    if ($(window).width() < 920) {
        $(this).toggleClass('showSubMenu');
        return false;
    }
});



$(document).on('click', '.main-nav__item-dropdown-item', function() {
    if ($(window).width() < 920) {
        if (!$(this).children('.main-nav__item-dropdown2').length > 0) {
            document.location.href = $(this).children('.main-nav__item-dropdown-link').attr('href');
            return true;
        }

        return false;

    } else {
        document.location.href = $(this).children('.main-nav__item-dropdown-link').attr('href');
        return true;
    }
});


$(document).on('click', '.main-nav__item-dropdown2-item', function() {

    if ($(window).width() < 1101) {
        if (!$(this).children('.main-nav__item-dropdown3').length > 0) {
            document.location.href = $(this).children('.main-nav__item-dropdown2-link').attr('href');
            return true;
        }
        return false;
    }

});


//Переключение навигационных дотов справа при скролле до соттветствующего блока

jQuery(window).scroll(function() {
    var $sections = $('.anchor-section');
    $sections.each(function(i, el) {
        var top = $(el).offset().top - 100;
        var bottom = top + $(el).height();
        var scroll = $(window).scrollTop();
        var id = $(el).attr('id');
        if (scroll > top && scroll < bottom) {
            $('.anchor-menu__item--dot').removeClass('active');
            $('a[href="#' + id + '"]').parent('li').addClass('active');

        }
    })
});

//Плавный скролл 

$(".anchor-menu").on("click", ".anchor-menu__dot", function(event) {
    event.preventDefault();
    var id = $(this).attr('href'),
        top = $(id).offset().top - 60;
    $('body,html').animate({ scrollTop: top }, 500);
});

$(document).ready(function () {
    $("body").on("click", ".scrollDownLink", function(event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top - 50;
        $('body,html').animate({ scrollTop: top }, 500);
    });
});


jQuery(document).ready(function() {

    if ($('.dealerAccount-form__tooltip').length) {
        $('.dealerAccount-form__tooltip').tooltipster({
            animation: 'fade',
            delay: 100,
            maxWidth: 200,
            side: 'top',
            distance: 2, 
            theme: 'tooltipster-lime'
        });
    }

    if ($('.cart__goodsTable-priceText .tooltip__icon').length) {
        $('.cart__goodsTable-priceText .tooltip__icon').tooltipster({
            animation: 'fade',
            delay: 100,
            maxWidth: 240,
            side: 'top',
            distance: 2,
            theme: 'tooltipster-white',
            trigger: 'click',
            content: $('<span class="tooltip__close"></span><span class="tooltip__title">Почему цена от?</span><span class="tooltip__text">Цена на выбранные продукты может быть разной, в зависимости от выбранного дилера. Пожалуйста, заполните поля ниже, чтобы получить итоговую стоимость.</span>')
        });
    }
	
	if ($('.product__prices .tooltip__icon').length) {
        $('.product__prices .tooltip__icon').tooltipster({
            animation: 'fade',
            delay: 100,
            maxWidth: 300,
            side: 'top',
            distance: 2,
            theme: 'tooltipster-white',
            trigger: 'click',
            content: $('<span class="tooltip__close"></span><span class="tooltip__title">Почему цена от и до?</span><span class="tooltip__text">Ценовые предложения по товару могут быть указаны несколькими официальными дилерами, в этом случае в каталоге отображается диапазон цен «от и до» для заданного региона. Точную цену можно увидеть в корзине после указания дополнительных параметров по доставке и оплате.</span>')
        });
    }
	

    if ($('.table-label .tooltip__icon').length) {
        $('.table-label .tooltip__icon').tooltipster({
            animation: 'fade',
            delay: 100,
            maxWidth: 180,
            side: 'top',
            distance: 2,
            theme: 'tooltipster-white',
            trigger: ('ontouchstart' in window) ? 'click' : 'hover'
        });
    }

    if ($('.account-form__table--common .tooltip__icon').length) {
        $('.account-form__table--common .tooltip__icon').tooltipster({
            animation: 'fade',
            delay: 100,
            maxWidth: 180,
            side: 'top',
            distance: 2,
            theme: 'tooltipster-white',
            trigger: ('ontouchstart' in window) ? 'click' : 'hover'
        });
    }


    var subscribeBlockHeight = -1 * $('.subscribe__form').innerHeight() - 1;
    $('.subscribe').css({ bottom: subscribeBlockHeight });

    /*setTimeout(function() {
        $('.modal-city__link').fancybox({
            padding: 0,
        }).trigger('click');
    }, 500);*/


    var $containerWidth = $(window).width();
    if ($containerWidth >= 1200) {
        fixAnchorMenu();
    }


    var searchPanelHeight = $('.searchPanel').height();
    if (searchPanelHeight > 550) {
        $('.searchBlock__content').css('min-height', searchPanelHeight + 'px');
    }


    //var cookieMsgHeight = $('.cookie_message').outerHeight();
    //$('body').css({ 'marginTop': cookieMsgHeight + 'px' })


    $(window).on('resize', function() {
        var $containerWidth = $(window).width();
        if ($containerWidth >= 1200) {

            fixAnchorMenu();
        } else {
            $(".anchor-menu").css({ "right": "50px" });
        }

        var searchPanelHeight = $('.searchPanel').height();
        $('.searchBlock__content').css('min-height', searchPanelHeight + 'px');

        //var cookieMsgHeight = $('.cookie_message').outerHeight();
        //$('body').css({ 'marginTop': cookieMsgHeight + 'px' })
    });

    function fixAnchorMenu() {
        var windowWidth = $(window).width();
        var anchorMenuRight = (windowWidth - 1328) / 2 + 40;
        $(".anchor-menu").css({ "right": anchorMenuRight });

    }


    /*var searchPanelHeight = $('.searchPanel').height();
    $('.searchBlock__content').css('min-height', searchPanelHeight + 'px');*/


    // В корзине в таблице заказа ставим rowspan равное количеству строк

    $(".cart-offer__table").each(function() {
        var rowspan = $(this).find('tr').length;
        $(this).find('.cart-offer__table-rowspan').attr('rowspan', rowspan);
    });

    if ($('#priceSlider').length) {
        $("#priceSlider").slider({
            min: 0,
            max: 1000,
            step: 1,
            values: [0, 1000],
            slide: function(event, ui) {
                for (var i = 0; i < ui.values.length; ++i) {
                    $("input.sliderValue[data-index=" + i + "]").val(ui.values[i]);
                }
            }
        });
        // $("input.sliderValue").change(function() {
        //     var $this = $(this);
        //     $("#priceSlider").slider("values", $this.data("index"), $this.val());
        // });
    }

    if ($('.datepicker').length) {
        var curYear = (new Date()).getFullYear();
        $(".datepicker").datepicker({
            inline: true,
            showOtherMonths: true,
            yearRange: '1920:' + curYear,
            changeYear: true
        });

        jQuery(function($) {
            $.datepicker.regional['ru'] = {
                closeText: 'Закрыть',
                prevText: '&#x3c;Пред',
                nextText: 'След&#x3e;',
                currentText: 'Сегодня',
                monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                    'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
                ],
                monthNamesShort: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                    'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
                ],
                dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
                dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
                dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                weekHeader: 'Нед',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional['ru']);
        });
    }

    // Строим диаграмму отзывов на странице дилера

    if ($('.reveiws-diagram').length) {

        var reviewsQuantity = $('.reveiws-diagram__reviewsQuantity');

        reviewsQuantityNums = []
        $(reviewsQuantity).each(function() {
            var reviewsQuantityNum = parseFloat($(this).text());
            reviewsQuantityNums.push(reviewsQuantityNum);
        });
        var reviewsQuantityMax = Math.max.apply(Math, reviewsQuantityNums);


        $(reviewsQuantity).each(function() {
            reviewsQuantityNum = parseFloat($(this).text());
            var percentWidth = reviewsQuantityNum / reviewsQuantityMax * 100;
            $(this).siblings('.reveiws-diagram__maxRow').find('.reveiws-diagram__colorRow').css({ 'width': percentWidth + '%' });
        });

    }


    /*if ($('#map_canvas').length) {
        function initialize() {
            var myLatlng = new google.maps.LatLng(56.359147, 43.828451);
            var pos1 = new google.maps.LatLng(56.359147, 43.828451);
            var pos2 = new google.maps.LatLng(56.249743, 43.990306);
            var myOptions = {
                zoom: 10,
                center: myLatlng,
                scaleControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            var image = 'images/marker.png';

            var marker = new google.maps.Marker({
                position: pos1,
                map: map,
                title: 'ул. Светлоярская 43, г. Н.Новгород',
                icon: image
            });

            var marker = new google.maps.Marker({
                position: pos2,
                map: map,
                title: 'ул. Крутояровская 22, г. Н.Новгород.',
                icon: image
            });

        }

        initialize();
    }*/


    /*if ($('#map_dealers').length) {
        function initialize() {
            var myOptions = {
                zoom: 4,
                center: {
                    lat: 55.761,
                    lng: 37.609
                },
                'streetViewControl': false,
                'scaleControl': false,
                'scrollwheel': false,
                'mapTypeId': google.maps.MapTypeId.ROADMAP,
                'mapTypeControl': false,
                'zoomControl': true
            }
            var map = new google.maps.Map(document.getElementById("map_dealers"), myOptions);

            var image = 'images/marker.png';


            var markers = locations.map(function(location, i) {
                return new google.maps.Marker({
                    position: location,
                    icon: image
                });
            });


            var clusterStyles = {
                styles: [{
                    height: 43,
                    url: 'images/marker_cluster.png',
                    textColor: 'white',
                    width: 43,
                    fontFamily: 'futuralight,Arial,sans-serif',
                    fontWeight: 'normal',
                    textSize: 16
                }]
            };

            var markerCluster = new MarkerClusterer(map, markers, clusterStyles);
        }
        var locations = [
            { lat: 56.359147, lng: 43.828451 },
            { lat: 56.249743, lng: 43.990306 }
        ]


        initialize();

    }*/




    if ($('.searchPanel-nav__item').length) {

        $('.searchPanel-nav__item').each(function() {
            if ($(this).children('.searchPanel-nav__item-dropdown').length > 0) {
                $(this).addClass('searchPanel-nav__item--dropdown');
            }
        });
    }


    if ($('.product__desc').length) {
        function getLineHeight(element) {
            var temp = document.createElement(element.nodeName);
            temp.setAttribute("style", "margin:0px;padding:0px;font-family:" + element.style.fontFamily + ";font-size:" + element.style.fontSize);
            temp.innerHTML = "test";
            temp = element.parentNode.appendChild(temp);
            var ret = temp.clientHeight;
            temp.parentNode.removeChild(temp);
            return ret;
        }

        var blockHeight = $('.product__desc').outerHeight();
        console.log(blockHeight);

        var test = document.querySelector('.product__desc');
        var height = getLineHeight(test);
        var trimHeight = height * 10;

        if (blockHeight < trimHeight) {
            $('.product__right .more').hide();
            $('.product__desc').addClass('noTrim');
        } else {
            test.style.maxHeight = trimHeight + 'px';
        }


        $(document).on('click', '.product__right .more', function() {
            $('.product__desc').css('max-height', blockHeight + 'px');
            $('.product__desc').toggleClass('noTrim');

            if ($('.product__right .more').text() == 'развернуть') {
                $('.product__right .more').text('свернуть')
            } else {
                $('.product__right .more').text('развернуть');
                test.style.maxHeight = trimHeight + 'px';
            }
        });

    }

    $('.bcgBlock').each(function() {
        if ($(this).find($('.bcgBlock__title')).length == 0) {
            $(this).children('.bcgBlock__wrapper').css({
                backgroundColor: 'transparent'
            })


        }
    })

    var $containerWidth = $(window).width();
    if ($containerWidth <= 570) {

        $('.bcgBlock').each(function() {
            let bcgBlockWrapperHeight = $(this).children('.bcgBlock__wrapper').innerHeight();
            $(this).css({
                marginBottom: bcgBlockWrapperHeight
            });

            if ($(this).find($('.bcgBlock__title')).length == 0) {
                $(this).css({
                    marginBottom: 0
                })
            }
        })

    }

    $(document).on('click', '.account-form__subtabs .account-form__tab', function() {
        let clickId = $(this).attr('id');
        if (clickId != $('.account-form__subtabs .account-form__tab.active').attr('id')) {
            $('.account-form__subtabs .account-form__tab').removeClass('active');
            $(this).addClass('active');
            $('.subtab').removeClass('subtab--active');
            $('#subtab-' + clickId).addClass('subtab--active');
        }
    });

    $(window).on('resize', function() {
        if ($containerWidth <= 570) {

            $('.bcgBlock').each(function() {
                let bcgBlockWrapperHeight = $(this).children('.bcgBlock__wrapper').innerHeight();
                $(this).css({
                    marginBottom: bcgBlockWrapperHeight
                });

                if ($(this).find($('.bcgBlock__title')).length == 0) {
                    $(this).css({
                        marginBottom: 0
                    })
                }
            })
        }

    });


});



// Счетчик товаров в корзине

/*function catalogItemCounter(field) {

    var fieldCount = function(el) {

        var
            // Мин. значение
            min = el.data('min'),

            // Макс. значение
            max = el.data('max') || false,

            // Кнопка уменьшения кол-ва
            dec = el.prev('.dec'),

            // Кнопка увеличения кол-ва
            inc = el.next('.inc');

        function init(el) {
            if (!el.attr('disabled')) {
                dec.on('click', decrement);
                inc.on('click', increment);
            }

            // Уменьшим значение
            function decrement() {
                var value = parseInt(el[0].value);
                value--;

                if (value >= min) {
                    el[0].value = value;
                }
            };

            // Увеличим значение
            function increment() {
                var value = parseInt(el[0].value);

                value++;

                if (!max || value <= max) {
                    el[0].value = value++;
                }
            };

        }

        el.each(function() {
            init($(this));
        });
    };

    $(field).each(function() {
        fieldCount($(this));
    });
}

catalogItemCounter('.fieldCount');*/




// Подсказка при вводе инпутов

/*var tags = ["Москва", "Нижний Новгород", "Санкт-Петербург", "Воронеж", "Новосибирск", "Барнаул", "Волгоград", "Геленджик", "Дзержинск", "Астрахань", "Пермь"];
$('.autocomplete').on("focus", function() {
    $(this).autocomplete({
        minLength: 1,
        source: tags //Подставил пока рандомные подсказки, чисто, чтоб стилизовать было удобнее 
    });
});*/






$(document).on('click', '.dealerAccount-form .dealer_personal_data_block .tab__title', function() {
    $(this).siblings('div, table').toggle();
    $(this).toggleClass('tab__title--hide');
    $(this).parents('.dealerAccount-form__block').toggleClass('dealerAccount-form__block--hide');
});

$(document).on('click', '.cart-form__block .tab__title', function() {
    $(this).siblings('div, table').toggle();
    $(this).toggleClass('tab__title--hide');
});

$(document).on('click', '.dealer_personal_data_block .dealerAccount-form__leftCol .dealerAccount-form__block:first-child .tab__title', function() {
    $(this).parents('.dealerAccount-form__wrapper').find('.dealerAccount-form__rightCol').toggle();
});

/*$(document).on('click', '.account-form__table--dealerOrders .account-form__table-title', function() {
    $(this).parents('.account-form__table--dealerOrders').find('tr:not(:nth-child(2))').toggle();
    $(this).parents('.account-form__table--dealerOrders').find('.account-form__table-orderInner').hide();
});*/


$(document).on('click', '.deliv_dop_block_show_button', function() {
    $('.deliv_dop_block').slideToggle(300);
    setTimeout(function(){
        if( $('.deliv_dop_block:visible').length > 0 ){
            $('input[name=deliv_dop_block_show]').val('Y');
        } else {
            $('input[name=deliv_dop_block_show]').val('N');
        }
    }, 301)
});



/*$(document).on('click', '.box_closer', function() {
    $('.cookie_message').hide();
    $('body').css({ 'marginTop': 0 })

});*/

$(window).scroll(function() {
    300 < $(this).scrollTop() ? $(".scroll2top-container").fadeIn() : $(".scroll2top-container").fadeOut()
});

$(document).ready(function () {

    $(".scroll2top").click(function(e) {
        e.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, 600);
    });

});



/*$(document).on('click', '.cart-form__block--buttons input', function() {
    if ($("#register").prop("checked")) {
        $('#orderNoregBlock').hide();
        $('#orderRegBlock').show();
    } else if ($("#withoutreg").prop("checked")) {
        $('#orderNoregBlock').show();
        $('#orderRegBlock').hide();
    }
});*/




let howItWorksOpen = function() {
    $('.modal-howItWorks__link').fancybox({
        padding: 0,
        wrapCSS: 'fancybox-review',
        afterLoad: function(current, previous) {
            setTimeout(function() {
                $('.modal-howItWorks .bxslider').bxSlider({
                    infiniteLoop: false
                });
                $('.modal-howItWorks .bxslider').css('opacity', 1);

                $('.bx-controls').each(function() {
                    var t = $(this);
                    var summ = 0;
                    $('.bx-pager-item', t).each(function() {
                        summ += $(this).width();
                    });

                    $('.bx-controls-direction', t).width(summ);

                });
            }, 50);

        }
    }).trigger('click');
}

function SendLead(name, email, phone, desc, prod, comment, id_bitrix, dealer, price) {
    $.post("/ajax/lead.php", {
        lead: 1,
        metrika_client_id: yaCounter55379437.getClientID(),
        name: name,
        email: email,
        phone: phone,
        description: desc, // название формы
        product: prod, // название и количество товаров
        comment: comment, // текст обращения
        BITRIX24_LEAD_ID: id_bitrix, // id обращения
        dealer:dealer, // id дилера
        value:price // стоимость заказа
    }).done(function (id) {
        if(id)
        {
            yaCounter55379437.params({
                'wbooster': id,
                'Bitrix-id': id_bitrix,
                'product': prod, // название и количество товаров
                'dealer':dealer, // id дилера
                'value':price // стоимость заказа
            });
        }
    });
}