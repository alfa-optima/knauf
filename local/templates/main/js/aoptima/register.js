$(document).ready(function(){



    // Регистрация
    $(document).on('click', '.registerButton', function(){
        if (!is_process(this)){
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var LAST_NAME = $(this_form).find("input[name=LAST_NAME]").val();
            var NAME = $(this_form).find("input[name=NAME]").val();
            var EMAIL = $(this_form).find("input[name=EMAIL]").val();
            var PERSONAL_BIRTHDAY = $(this_form).find("input[name=PERSONAL_BIRTHDAY]").val();
            var PERSONAL_GENDER = $(this_form).find("input[name=PERSONAL_GENDER]:checked").val();
            var PASSWORD = $(this_form).find("input[name=PASSWORD]").val();
            var CONFIRM_PASSWORD = $(this_form).find("input[name=CONFIRM_PASSWORD]").val();
            var agree = $(this_form).find("input[name=agree]:checked").val();
            var error = false;
            // Проверки
            if (NAME.length == 0) {
                error = ['NAME',   'Введите, пожалуйста, Ваше имя!'];
            } else if (LAST_NAME.length == 0) {
                error = ['LAST_NAME', 'Введите, пожалуйста, Вашу фамилию!'];
            } else if (EMAIL.length == 0){
                error = ['EMAIL',   'Введите, пожалуйста, Ваш адрес электронной почты!'];
            } else if (EMAIL.length > 0 && !(/^.+@.+\..+$/.test(EMAIL))) {
                error = ['EMAIL', 'Эл. почта содержит ошибки!'];
            // } else if (PERSONAL_BIRTHDAY.length == 0){
            //     error = ['PERSONAL_BIRTHDAY',   'Укажите, пожалуйста, дату рождения!'];
            // } else if( !PERSONAL_GENDER ){
            //     error = ['PERSONAL_GENDER', 'Укажите, пожалуйста, Ваш пол!'];
            } else if (PASSWORD.length == 0) {
                error = ['PASSWORD',   'Придумайте, пожалуйста, надёжный пароль!'];
            } else if (PASSWORD.length < 8) {
                error = ['PASSWORD',   'Минимальная длина пароля - 8 символов'];
            } else if (CONFIRM_PASSWORD.length == 0) {
                error = ['CONFIRM_PASSWORD',   'Введите, пожалуйста, повтор пароля!'];
            } else if (PASSWORD != CONFIRM_PASSWORD) {
                error = ['CONFIRM_PASSWORD',   'Пароли не совпадают'];
            } else if ( agree != 'Y' ) {
                error = ['agree',   'Отметьте, пожалуйста, согласие на обработку персональных данных'];
            }
            // Если нет ошибок
            if (!error){
                process(true);
                var form_data = $(this_form).serialize();
                $.post("/ajax/register.php", {form_data:form_data}, function(data){
                    if (data.status == 'ok'){

                        $('.registerForm').replaceWith('<p class="success___p">Вы успешно зарегистрированы!</p><p class="success___p">На указанный адрес электронной почты выслана <u>ссылка для подтверждения регистрации</u></p><p class="success___p"><a href="/auth/" style="color: green; text-decoration: underline;">Авторизация</a></p>');
                        $('.registerTitleBlock').remove();

                        ym(55379437, 'reachGoal', 'sendRegForm');

                    } else if (data.status == 'error'){
                        show_error(this_form, false, data.text);
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'topRight', type: 'warning', text: "Ошибка запроса"});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                show_error(this_form, error[0], error[1]);
            }
        }
    });








})
