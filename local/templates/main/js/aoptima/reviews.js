$(document).ready(function(){




    // Отзыв о товаре (открытие окна)
    $(document).on('click', '.reviewButton', function() {
        if (!is_process(this)) {
            var item_id = $(this).attr('item_id');
			var fullname = $(this).attr('fullname');
            process(true);
            $.post("/ajax/reviewWindow.php", {item_id:item_id}, function(data){
                if (data.status == 'ok'){
                    $('.reviewForm input[name=tov_id]').val(item_id);
                    //$('.reviewForm .modal-review__subtitle').html(data.el['NAME']);
					$('.reviewForm .modal-review__subtitle').html(fullname);
                    $('.reviewForm textarea[name=review]').val('');
                    if( 'prevVote' in data && data.prevVote != 'N' ){
                        $('.prevVote span').html('Прошлая оценка: '+data.prevVote);
                    }
                    $('.modal-review__counter span#counter').html(300);
                    $.noty.closeAll();
                    $('.modal-review__link').fancybox({
                        padding: 0,
                        wrapCSS: 'fancybox-review'
                    }).trigger('click');

                } else if (data.status == 'error'){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: data.text});
                }
            }, 'json')
            .fail(function(data, status, xhr){
                var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса"});
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    })




    // Отзыв о товаре (отправка)
    $(document).on('click', '.reviewButtonSend', function() {
        if (!is_process(this)) {
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var name = $(this_form).find("input[name=name]").val();
            var review = $(this_form).find("textarea[name=review]").val();
            var error = false;
            // Проверки
            if (name.length == 0){
                error = ['name',   'Введите, пожалуйста, ваше имя!'];
            } else if (review.length == 0){
                error = ['review',   'Введите, пожалуйста, текст отзыва!'];
            }

            var vote_stars_cnt = $(this_form).find('ul#stars li.star.selected').length;

            // Если нет ошибок
            if (!error){
                process(true);
                var form_data = $(this_form).serialize();
                $.post("/ajax/reviewSend.php", {
                    form_data:form_data,
                    vote_stars_cnt:vote_stars_cnt
                }, function(data){
                    if (data.status == 'ok'){

                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'success', text: "Отзыв успешно отправлен"});

                        $.fancybox.close();

                    } else if (data.status == 'error'){
                        show_error(this_form, false, data.text);
                    }
                }, 'json')
                    .fail(function(data, status, xhr){
                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса"});
                    })
                    .always(function(data, status, xhr){
                        process(false);
                    })
                // Ошибка
            } else {
                show_error(this_form, error[0], error[1]);
            }
        }
    })




    // Подгрузка отзывов о товаре
    $(document).on('click', '.reviewsMoreButton', function(){
        if (
            !is_process(this)
            &&
            $('.reviewItem').length > 0
        ){
            var params = {};
            params['items'] = [];
            $('.reviewItem').each(function (){
                params['items'].push( $(this).attr('item_id') );
            })
            params['reviews_tov_id'] = $('input[name=reviews_tov_id]').val();

            process(true);
            $.post("/ajax/reviewsLoad.php", params, function(data){
                if (data.status == 'ok'){
                    $('.reviewsLoadTbody').append(data.html);
                    calcReviewsButton();
                }
            }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса"});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
        }
    });









})