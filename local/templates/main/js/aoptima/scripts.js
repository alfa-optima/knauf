// reloadSearchPage
function reloadSearchPage( params, searchFocus, closeSidebar ){
    if( !params ){  var params = {};  }
    params['form_data'] = $('.searchForm').serialize();
    params['closeSidebar'] = closeSidebar?'Y':'N';
    if( 'action' in params ){} else {
        params['action'] = 'reloadSearchPage';
    }
    process(true);

    params['app'] = $("body").data().app;

    $.post("/ajax/ajax.php", params, function(data){
        if (data.status == 'ok'){
            if( 'stop_ids' in params ){
                $('ul.searchBlock__items').append(data.html);
                if( $('ost').length > 0 ){
                    $('.searchMoreButton').show();
                } else {
                    $('.searchMoreButton').hide();
                }
                $('ost').remove();
            } else {
                var q = $('.searchForm input[name=q]').val();
                $('.searchForm').replaceWith(data.html);
                $('.searchForm input[name=q]').val(q);
            }
            if( searchFocus ){
                var input_el = document.querySelector(".searchForm input[name=q]");
                input_el.focus();
                input_el.selectionStart = input_el.value.length;
            }

            setSearchMinHeight();

        }
    }, 'json')
    .fail(function(data, status, xhr){
        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса"});
    })
    .always(function(data, status, xhr){
        process(false);
    })
}


function product_tab_click( tab_code ){
    if( tab_code == 'preimushchestva' ){
        ym(55379437, 'reachGoal', 'clickAdvantage');
    } else if( tab_code == 'tehnicheskie_harakteristiki' ){
        ym(55379437, 'reachGoal', 'clickSpecifications');
    } else if( tab_code == 'otzyvy' ){
        ym(55379437, 'reachGoal', 'clickReviews');
    }
}



$(document).ready(function(){

    var base_domain = $('input[name=base_domain]').val();
    var cur_domain = $('input[name=cur_domain]').val();
    var request_scheme = $('input[name=request_scheme]').val();


    calcReviewsButton();


    //$('input[type=tel]').mask('+7 (000) 000 00 00', {placeholder: "+7 (___) ___ __ __"});


    if ($('.orderDatepicker').length) {
        $(".orderDatepicker").datepicker({
            inline: true,
            showOtherMonths: true,
            minDate: 0
        });
    }
    if ($('.basketDatepicker').length) {
        $(".basketDatepicker").datepicker({
            inline: true,
            showOtherMonths: true,
            minDate: 0
        });
    }



    // $(document).on('click', '.mainSiteLink', function(e){
    //     e.preventDefault();
    //     var href = $(this).attr('href');
    //     ym(55379437, 'reachGoal', 'headerGoToParentKnauf');
    //     window.location.href = href;
    // })



    $(document).on('click', '.modalMapSelectButton', function(e){
        if( !modal_map_marker ){
            alert('Укажите точку на карте')
        } else {
            $(coords_input).val(modal_map_marker.getPosition().lat()+','+modal_map_marker.getPosition().lng());
            $.fancybox.close();
        }
    })


    // контроль формата числового инпута
    var number_input_class = 'number___input';
    prepare_number_inputs( number_input_class );
    $(document).on('keyup', '.'+number_input_class, function(e){
        check_number_inputs( $(this) );
    })

    // контроль формата ценового инпута
    var price_input_class = 'price___input';
    prepare_price_inputs( price_input_class );
    $(document).on('keyup', '.'+price_input_class, function(e){
        check_price_inputs( $(this) );
    })




    // Окно подтверждения города
    var cookie_loc_id = $.cookie('BITRIX_SM2_loc_id');
    if(
        cookie_loc_id == undefined
        ||
        cookie_loc_id.length == 0
    ){
        setTimeout(function() {
            $('.modal-city__link').fancybox({  padding: 0 }).trigger('click');
        }, 500);
    }


    // Подтверждение города
    $(document).on('click', '.city_accept_button', function(){
        var loc_id = $(this).attr('loc_id')
        $.cookie(
            'BITRIX_SM2_loc_id',
            loc_id,
            {
                expires: 30,
                path: '/',
                domain: '.'+base_domain
            }
        );
        setTimeout(function() {
            var url = document.URL;
            var arURL = url.split('#');
            window.location.href = arURL[0];
        }, 100);
    });


    // Выбор города в списке
    $(document).on('click', '.selectCityButton', function(){
        if (
            !is_process(this)
            &&
            !$(this).find('span').hasClass('active')
        ){
            var sub_domain = $(this).attr('sub_domain');
            var loc_id = $(this).attr('loc_id');

            var cur_url = document.URL;
            var arURL = cur_url.split('#');
            cur_url = arURL[0];
            var ar = cur_url.split(base_domain);
            var new_url = request_scheme+'://'+sub_domain+ar[1];

            $.cookie(
                'BITRIX_SM2_loc_id',
                loc_id,
                {
                    expires: 30,
                    path: '/',
                    domain: '.'+base_domain
                }
            );

            window.location.href = new_url;

            // Очищаем корзину при смене местоположения
            // process(true);
            // $.post("/ajax/clearBasket.php", {}, function(data){}, 'json')
            // .fail(function(data, status, xhr){})
            // .always(function(data, status, xhr){
            //     window.location.href = new_url;
            // })

        }
    });



    $(document).on('click', '.searchPanel__toggle', function(e){
        $('.searchBlock').toggleClass('hideMenu');
        if( $(this).hasClass('catalogLeftMenu') ){
            if( $(this).parents('section.searchBlock').hasClass('hideMenu') ){
                var val = 'Y';
            } else {
                var val = 'N';
            }
            $.cookie(
                'BITRIX_SM2_hide_catalog_left_menu', val,
                {
                    expires: 90,
                    path: '/',
                    domain: '.'+base_domain
                }
            );
        }
    });




    // Уведомление о куки
    var cookies_accept = $.cookie('BITRIX_SM2_cookies_accept');
    if( cookies_accept != 'Y' ){
        $('.cookie_message').show();
        var cookieMsgHeight = $('.cookie_message').outerHeight();
        //$('body').css({ 'marginTop': cookieMsgHeight + 'px' });
    }
    $(window).resize(function() {
        if( $('.cookie_message:visible').length > 0 ){
            var cookieMsgHeight = $('.cookie_message').outerHeight();
            //$('body').css({ 'marginTop': cookieMsgHeight + 'px' });
        }
    });
    // Закрытие уведомления о куках
    $(document).on('click', '.box_closer', function(){
        $('.cookie_message').hide();
        //$('body').css({ 'marginTop': 0 });
        $.cookie(
            'BITRIX_SM2_cookies_accept',
            'Y',
            {
                expires: 90,
                path: '/',
                domain: '.'+base_domain
            }
        );
    });






    // Подгрузка точек продаж
    $(document).on('click', '.moreShopsButton', function(){
        if (!is_process(this)){
            var filter_form = $('.shopsFilterForm:visible');
            var form_data = $(filter_form).serialize();
            var stop_ids = [];
            $('.shopItem').each(function(){
                var item_id = $(this).attr('item_id');
                stop_ids.push(item_id);
            })
            var postParams = {
                form_data: form_data,
                stop_ids: stop_ids
            };
            process(true);
            $.post("/ajax/shopsLoad.php", postParams, function(data){
                if (data.status == 'ok'){
                    $(filter_form).parents('article').find('.shopsLoadArea').append(data.html);
                    if( $('ost').length > 0 ){
                        $(filter_form).parents('article').find('.moreShopsButton').show();
                    } else {
                        $(filter_form).parents('article').find('.moreShopsButton').hide();
                    }
                    $('ost').remove();
                } else if (data.status == 'error'){
                    var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: data.text});
                }
            }, 'json')
            .fail(function(data, status, xhr){
                var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    })


    // Перемещение на позицию точки продаж на карте
    $(document).on('click', '.shopItem .dealer__addresses-itemTitle', function(){
        var item_id = $(this).parents('.shopItem').attr('item_id');
        points_map.setCenter(shop_points[item_id]['position']);
        points_map.setZoom(15);
        var destination = $('#map_dealers').offset();
        $('body, html').animate({scrollTop: destination.top}, 400);
    })





    $(document).on('click', '.addSuggestionButton', function(){
        if (!is_process(this)){
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var city = $(this_form).find("input[name=city]").val();
            var name = $(this_form).find("input[name=name]").val();
            var last_name = $(this_form).find("input[name=last_name]").val();
            var email = $(this_form).find("input[name=email]").val();
            var phone = $(this_form).find("input[name=phone]").val();
            var message = $(this_form).find("textarea[name=message]").val();
            var agree = $(this_form).find("input[name=agree]:checked").val();
            var agree1 = $(this_form).find("input[name=agree1]:checked").length>0;
            var agree2 = $(this_form).find("input[name=agree2]:checked").length>0;
            var theme = $(this_form).find("select[name=theme]").val();
            var error = false;
            // Проверки
            if (city.length == 0){
                error = ['city',   'Укажите, пожалуйста, Ваш населённый пункт!'];
            } else if (!theme){
                error = ['theme',   'Выберите, пожалуйста, тему обращения!'];
            } else if (name.length == 0){
                error = ['name',   'Введите, пожалуйста, Ваше имя!'];
            } else if (last_name.length == 0){
                error = ['last_name',   'Введите, пожалуйста, Вашу фамилию!'];
            } else if (email.length == 0){
                error = ['email',   'Введите, пожалуйста, Ваш адрес эл. почты!'];
            } else if (email.length > 0 && !(/^.+@.+\..+$/.test(email))){
                error = ['email',   'Эл. почта содержит ошибки!'];
            } else if (phone.length == 0){
                error = ['phone',   'Введите, пожалуйста, Ваш телефон!'];
            } else if (phone.length > 0 && !(/^ *\+? *?\d+([-\ 0-9]*|(\(\d+\)))*\d+ *$/.test(phone))){
                error = ['phone',   'В номере телефона допускаются только цифры, а также символы "+", "-", пробел и круглые скобки!'];
            } else if (message.length == 0){
                error = ['message',   'Введите, пожалуйста, текст Вашего предложения!'];
            } else if (agree != 1){
                error = ['agree',   'Отметьте, пожалуйста, согласие с политикой конфиденциальности'];
            }else if( !agree1 ){
                error = [false,   'Примите, пожалуйста, условия Пользовательского соглашения'];
            }
            /*else if( !agree2 ){
                error = [false,   'Отметьте, пожалуйста, согласие на рассылку информационных материалов'];
            }*/
            // Если нет ошибок
            if (!error){
                process(true);
                var form_data = $(this_form).serialize();
                $.post("/ajax/ajax.php", {action:"suggestion", form_data:form_data}, function(data){
                    if (data.status == 'ok'){
                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'success', text: "Ваше сообщение успешно отправлено!"})
                        $(this_form).find("input[type=text], input[type=tel], input[type=email], input[type=password], textarea").val('');
                        $(this_form).find('input[type=checkbox]').prop('checked', false);
						
						ym(55379437,'reachGoal','ContactForm');

                        SendLead(name+" "+last_name, email, phone, "Обратная связь", "", message, data.res, "", "");
						
                    } else if (data.status == 'error'){
                        show_error(this_form, false, data.error_text);
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса"});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                show_error(this_form, error[0], error[1]);
            }
        }
    });







    if( $('.catalogSliderBlock').length > 0 ){
        var firstID = $(".catalogSliderBlock").eq(0).attr('id');
        $('.bannerScrollLink').attr('href', '#'+firstID);
        $('.bannerScrollLink').show();
    }


    if( $('#popular').length > 0 ) {
        $('.mainPopScrollLink').show();
    }




    $(document).on('click', '.toReviewsLink', function(){
        $('.reviewVkl a').trigger('click');
        var destination = $('.product-tabs').offset();
        var panel_height = $('#panel').length>0?$('#panel').outerHeight(true):0;
        var header_height = $('header').outerHeight(true);
        var breadcrumbs_height = $('ol.breadcrumbs').outerHeight(true);
        $('body, html').animate({scrollTop: destination.top - panel_height - header_height - breadcrumbs_height}, 400);
    });






    // ПОИСК - выбор раздела каталога
    $(document).on('click', '.searchForm .sectionButton', function() {
        if (!is_process(this)) {
            if( $(this).parents('.searchPanel-nav__item').hasClass('searchPanel-nav__item--active') ){
                var section_id = '';
            } else {
                var section_id = $(this).attr('section_id');
            }
            //if( section_id.length > 0 ){
                $('.searchForm input[name=section_id]').val(section_id);
                reloadSearchPage();
            //}
        }
    })

    // ПОИСК - Подгрузка товаров
    $(document).on('click', '.searchMoreButton', function() {
        if (!is_process(this)) {
            var params = {stop_ids: []};
            $('.catalog___item').each(function(){
                var item_id = $(this).attr('item_id');
                params['stop_ids'].push(item_id);
            });
            reloadSearchPage(params);
        }
    });

    // ПОИСК - Ввод в строку поиска
    $(document).on('keyup', '.searchForm input[name=q]', function(e) {
        clearInterval(LKsearchInterval);
        var keyCode = e.keyCode;
        if( keyCode == 13 ){
            reloadSearchPage(false, false, true);
        } else {
            LKsearchInterval = setInterval(function(){
                clearInterval(LKsearchInterval);
                reloadSearchPage(false, true);
            }, 700);
        }
    })







});