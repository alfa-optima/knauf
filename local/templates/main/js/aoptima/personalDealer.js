var LKsearchInterval;



function dealerImportCSVStart(){
    $('#loadFileWindow .success___block').html('');
    $('#loadFileWindow .error___p').html('');
    $('#loadFileWindow .aoptima_progress_bar').css('width', '0%');
    $('#loadFileWindow .loading___status').html('0%');
    ////////////////////////////////
    $('#loadFileWindow').show();
    $('.csvFormBlock').hide();
    /*$('.loadFileWindowLink').fancybox({
        padding: 0,
    }).trigger('click');*/
    ////////////////////////////////
   dealerImportCSVImport();
}

function dealerImportYMLStart(){
    $('#loadFileWindow .success___block').html('');
    $('#loadFileWindow .errora___p').html('');
    $('#loadFileWindow .aoptima_progress_bar').css('width', '0%');
    $('#loadFileWindow .loading___status').html('0%');
    ////////////////////////////////
    $('#loadFileWindow').show();
    $('.ymlFormBlock').hide();
    /*$('.loadFileWindowLink').fancybox({
        padding: 0,
    }).trigger('click');*/
    ////////////////////////////////
    dealerImportYMLImport();
}

function dealerImportCSVImport(){

    if( $('#loadFileWindow').attr('go') == 'Y' ){

        var postParams = {};    var status_html;
        process(true);
        $.post("/ajax/dealerImportCSV.php", postParams, function(data){
            if( data == null ){

                $('#loadFileWindow .error___p').html('Ошибка запроса');
                status_html = '<a style="cursor: pointer;" onclick="loadCSVStartPosition( true );" class="goods__top-button">Попробовать снова</a>';
                $('#loadFileWindow .success___block').html(status_html);

            } else if (data.status == 'error'){

                $('#loadFileWindow .error___p').html(data.text);
                status_html = '<a style="cursor: pointer;" onclick="loadCSVStartPosition( true );" class="goods__top-button">Попробовать снова</a>';
                $('#loadFileWindow .success___block').html(status_html);

            } else {

                if( $('#loadFileWindow:visible').length > 0 ){

                    $('#loadFileWindow .aoptima_progress_bar').css('width', data.procent+'%');
                    $('#loadFileWindow .loading___status').html(data.procent+'%');

                    if (data.status == 'progress'){

                        if( $('#loadFileWindow').attr('go') == 'N' ){

                            loadCSVStartPosition();

                        } else {

                            status_html = '<p class="success___p" style="color:#3cb5ea!important">Статус: Загрузка...</p>';
                            status_html += '<p class="success___p" style="color:#3cb5ea!important">Успешно загружено записей: <strong>'+data.successLinesCnt+' из '+data.totalLinesCnt+'</strong></p>';
                            status_html += '<a style="cursor: pointer;" class="goods__top-button" onclick="$(\'#loadFileWindow\').attr(\'go\', \'N\'); $(this).css(\'background-color\', \'#cccccc\'); $(this).html(\'Остановка...\')">Остановить</a>';
                            $('#loadFileWindow .success___block').html(status_html);

                            setTimeout(function(){
                                dealerImportCSVImport();
                            }, 500);

                        }

                    } else if (data.status == 'ok'){

                        status_html = '<p class="success___p">Статус: Завершено</p>';
                        status_html += '<p class="success___p">Успешно загружено записей: <strong>'+data.successLinesCnt+' из '+data.totalLinesCnt+'</strong></p>';

                        if( 'errorLines' in data && Object.keys( data.errorLines ).length > 0 ){
                            status_html += '<p class="error___p">Не загружены записи (номера строк):<br>';
                            var n = 0;
                            for( key in data.errorLines  ){ n++;
                                var errorLine = data.errorLines[key];
                                if( n == 1 ){
                                    status_html += errorLine;
                                } else {
                                    status_html += ', '+errorLine;
                                }
                            }
                            status_html += '</p>';
                        }

                        status_html += '<a style="cursor: pointer;" onclick="loadCSVStartPosition( true );" class="goods__top-button">Загрузить ещё</a>';

                        $('#loadFileWindow .success___block').html(status_html);

                    }

                }

            }
        }, 'json')
        .fail(function(data, status, xhr){
            $('#loadFileWindow .error___p').html('Ошибка запроса');
            status_html = '<a style="cursor: pointer;" onclick="loadCSVStartPosition( true );" class="goods__top-button">Попробовать снова</a>';
            $('#loadFileWindow .success___block').html(status_html);
        })
        .always(function(data, status, xhr){
            process(false);
        })
    } else if( $('#loadFileWindow').attr('go') == 'N' ){
        loadCSVStartPosition();
    }
}

function dealerImportYMLImport(){

    if( $('#loadFileWindow').attr('go') == 'Y' ){

        var postParams = {};    var status_html;
        process(true);
        $.post("/ajax/dealerImportYML.php", postParams, function(data){
            if( data == null ){

                $('#loadFileWindow .error___p').html('Ошибка запроса');
                status_html = '<a style="cursor: pointer;" onclick="loadYMLStartPosition();" class="goods__top-button">Попробовать снова</a>';
                $('#loadFileWindow .success___block').html(status_html);

            } else if (data.status == 'error'){

                $('#loadFileWindow .error___p').html(data.text);
                status_html = '<a style="cursor: pointer;" onclick="loadYMLStartPosition();" class="goods__top-button">Попробовать снова</a>';
                $('#loadFileWindow .success___block').html(status_html);

            } else {

                if( $('#loadFileWindow:visible').length > 0 ){

                    $('#loadFileWindow .aoptima_progress_bar').css('width', data.procent+'%');
                    $('#loadFileWindow .loading___status').html(data.procent+'%');

                    if (data.status == 'progress'){

                        if( $('#loadFileWindow').attr('go') == 'N' ){

                            loadYMLStartPosition();

                        } else {

                            status_html = '<p class="success___p" style="color:#3cb5ea!important">Статус: Загрузка...</p>';
                            status_html += '<p class="success___p" style="color:#3cb5ea!important">Успешно загружено записей: <strong>'+data.successLinesCnt+' из '+data.totalLinesCnt+'</strong></p>';
                            status_html += '<a style="cursor: pointer;" class="goods__top-button" onclick="$(\'#loadFileWindow\').attr(\'go\', \'N\'); $(this).css(\'background-color\', \'#cccccc\'); $(this).html(\'Остановка...\')">Остановить</a>';
                            $('#loadFileWindow .success___block').html(status_html);

                            setTimeout(function(){
                                dealerImportYMLImport();
                            }, 500);

                        }

                    } else if (data.status == 'ok'){

                        status_html = '<p class="success___p">Статус: Завершено</p>';
                        status_html += '<p class="success___p">Успешно загружено записей: <strong>'+data.successLinesCnt+' из '+data.totalLinesCnt+'</strong></p>';

                        if( 'errorOffers' in data && Object.keys( data.errorOffers ).length > 0 ){
                            status_html += '<p class="error___p">Не загружены записи (offer id) (' + Object.keys( data.errorOffers ).length + ' шт.):<br>';
                            for( key in data.errorOffers  ){
                                var errorOfferID = data.errorOffers[key];
                                status_html += '- '+errorOfferID+';<br>';
                            }
                            status_html += '</p>';
                        }

                        status_html += '<a style="cursor: pointer;" onclick="loadYMLStartPosition( true );" class="goods__top-button">Загрузить ещё</a>';

                        $('#loadFileWindow .success___block').html(status_html);

                    }

                }

            }
        }, 'json')
        .fail(function(data, status, xhr){

            $('#loadFileWindow .error___p').html('Ошибка запроса');
            status_html = '<a style="cursor: pointer;" onclick="loadYMLStartPosition();" class="goods__top-button">Попробовать снова</a>';
            $('#loadFileWindow .success___block').html(status_html);

        })
        .always(function(data, status, xhr){
            process(false);
        })
    } else if( $('#loadFileWindow').attr('go') == 'N' ){
        loadYMLStartPosition();
    }
}

function loadCSVStartPosition( clearFormData ){
    $('#loadFileWindow').attr('go', 'Y');
    $('#loadFileWindow').hide();
    $('.csvFormBlock').show();
    if( clearFormData ){
        $('.csvFormBlock input[name=loc_id], .csvFormBlock input[name=fileRegion], .csvFormBlock input[name=csv]').val('');
        $('.csvFormBlock input[name=csv]').parents('label').find('span').eq(0).html('Выбрать файл CSV');
    }
}
function loadYMLStartPosition( clearFormData ){
    $('#loadFileWindow').attr('go', 'Y');
    $('#loadFileWindow').hide();
    $('.ymlFormBlock').show();
    if( clearFormData ){
        $('.ymlFormBlock input[name=loc_id], .ymlFormBlock input[name=fileRegion], .ymlFormBlock input[name=yml]').val('');
        $('.ymlFormBlock input[name=loc_id], .ymlFormBlock input[name=fileRegion], .ymlFormBlock input[name=yml_link]').val('');
        $('.ymlFormBlock input[name=yml]').parents('label').find('span').eq(0).html('Выбрать файл YML');
    }
}

function uploadMappingCSVResponse(d) {
    $('.dealerLoadMappingCSVForm .error___p').html('');
    eval('var obj = ' + d + ';');
    if (obj.status == 'ok'){
        var html = obj.html;
        html = html.replace("mybase64", "");
        html = Base64.decode(html);
        $('.dealerLoadMappingCSVFormBlock').replaceWith(html);
        //$('.dealerLoadMappingCSVForm .error___p').after('<p class="success___p">Файл успешно загружен!</p>');
    } else {
        $('.dealerLoadMappingCSVForm .error___p').html(obj.text);
    }
}

function uploadCSVResponse(d) {
    eval('var obj = ' + d + ';');
    if (obj.status == 'ok'){
        dealerImportCSVStart();
    } else {
        $('.loadCSVForm .error___p').html(obj.status);
    }
}
function uploadYMLResponse(d) {
    if( typeof d == 'object'){
        var obj = d;
    } else {
        eval('var obj = ' + d + ';');
    }
    if (obj.status == 'ok'){
        dealerImportYMLStart();
    } else {
        if( obj.text != undefined ){
            $('.loadYMLForm .error___p').html(obj.text);
        } else {
            $('.loadYMLForm .error___p').html(obj.status);
        }
    }
}




// reloadDealerGoods
function reloadDealerGoods( params, focus ){
    if( !params ){  var params = {};  }
    params['form_data'] = $('.dealerGoodsForm').serialize();
    if( 'action' in params ){} else {
        params['action'] = 'reloadDealerGoods';
    }
    params['app'] = $("body").data().app;

    process(true);
    $.post("/ajax/ajax.php", params, function(data){
        if (data.status == 'ok'){
            $('.dealerGoodsForm').replaceWith(data.html);
            setSearchMinHeight();
            if( focus ){
                $('input[name=goodsLKRegion]').focus();
            }
        }
    }, 'json')
    .fail(function(data, status, xhr){
        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса"});
    })
    .always(function(data, status, xhr){
        process(false);
    })
}



// reloadAddPricesPage
function reloadAddPricesPage( params, searchFocus ){
    if( !params ){  var params = {};  }
    params['form_data'] = $('.dealerAddPricesForm').serialize();
    if( 'action' in params ){} else {
        params['action'] = 'reloadAddPricesPage';
    }
    params['app'] = $("body").data().app;

    process(true);
    $.post("/ajax/ajax.php", params, function(data){
        if (data.status == 'ok'){
            if( 'stop_ids' in params ){
                $('ul.searchBlock__items').append(data.html);
                if( $('ost').length > 0 ){
                    $('.addPricesMoreButton').show();
                } else {
                    $('.addPricesMoreButton').hide();
                }
                $('ost').remove();
            } else {
                $('.dealerAddPricesForm').replaceWith(data.html);
            }
            if( searchFocus ){
                var input_el = document.querySelector(".dealerAddPricesForm input[name=q]");
                input_el.focus();
                input_el.selectionStart = input_el.value.length;
            }
        }
    }, 'json')
    .fail(function(data, status, xhr){
        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса"});
    })
    .always(function(data, status, xhr){
        process(false);
    })
}



// Загрузка фото
var fileInputName = 'persPhotoInput';
function onResponse(d) {
    eval('var obj = ' + d + ';');
    if (obj.status == 'ok'){

        process(true);
        $.post("/ajax/ajax.php", {action:"getPersPhotoBlock", fid:obj.fid}, function(data) {
            if (data.status == 'ok'){
                // вставляем фото
                $('.dealerLKPhotoBlock').replaceWith(data.html);
                // Очищаем файловый инпут
                $('input[name='+fileInputName+']').val('');
            }
        }, 'json')
        .fail(function(data, status, xhr){
            show_error( $('form.'+form_class) , 'no___field', 'Ошибка запроса');
        })
        .always(function(data, status, xhr){
            process(false);
        })
    } else {
        $('.dealerLKPhotoBlock .error___p').html(obj.status);
        // Вывод ошибки
        //var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'top', type: 'warning', text: obj.status});
    }
}

// Загрузка файла с инфо о доставке
var deliveryFileInputName = 'deliveryFileInput';
function onDelivFileResponse(d) {
    eval('var obj = ' + d + ';');
    if (obj.status == 'ok'){
        // вставляем фото
        $('.deliveryLocationsBlock').replaceWith(Base64.decode(obj.html));
        // Очищаем файловый инпут
        $('input[name='+deliveryFileInputName+']').val('');
    } else {
        $('.delivInfoForm .error___p').html(obj.status);
        // Вывод ошибки
        //var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'top', type: 'warning', text: obj.status});
    }
}

$(document).ready(function(){



    $(document).on('click', '.my___region_button', function(e){
        if (!is_process(this)){
            var loc_id = $(this).attr('loc_id');
            $('input[name=loc_id]').val(loc_id);
            reloadDealerGoods();
        }
    })




    // Загрузка фото
    $(document).on('click', '.persPhotoSelect', function(e){
        if(
            (
                $(e.target).hasClass('persPhotoSelect')
                &&
                $('.removeDealerPhotoButton').length > 0
            )
            ||
            $('.removeDealerPhotoButton').length == 0
        ){
            $('.dealerLKPhotoBlock .error___p').html('');
            $('input[name='+fileInputName+']').val('');
            $('input[name='+fileInputName+']').trigger('click');
        }
    })
    $(document).on('change', 'input[name='+fileInputName+']', function(){
        $(".persUpload___form").submit();
    })



    // Удаление фото
    $(document).on('click', '.removeDealerPhotoButton', function(){
        if (!is_process(this)){
            $.noty.closeAll();

            var n = noty({
                text        : 'Удалить фото?',
                type        : 'alert',
                dismissQueue: true,
                layout      : 'center',
                theme       : 'defaultTheme',
                buttons     : [
                    {addClass: 'btn btn-primary', text: 'Да', onClick: function ($noty) {
                            $noty.close();
                            $.post("/ajax/ajax.php", {action:"removeDealerPhoto"}, function(data){
                                if (data.status == 'ok'){
                                    // вставляем фото
                                    $('.dealerLKPhotoBlock').replaceWith(data.html);
                                    // Очищаем файловый инпут
                                    $('input[name='+fileInputName+']').val('');
                                } else if (data.status == 'error'){
                                    $('.dealerLKPhotoBlock .error___p').html(data.text);
                                }
                            }, 'json')
                            .fail(function(data, status, xhr){
                                $('.dealerLKPhotoBlock .error___p').html('Ошибка запроса');
                            })
                            .always(function(data, status, xhr){
                                process(false);
                            })
                        }},
                    {addClass: 'btn btn-danger', text: 'Нет', onClick: function ($noty) {
                            $noty.close();
                        }}
                ]
            });


        }
    })





    // ЛК ДИЛЕР - сохранение личных данных
    $(document).on('click', '.dealerDataSaveButton', function(){
        if ( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var error = false;

            var UF_COMPANY_NAME = $(this_form).find("input[name=UF_COMPANY_NAME]").val();
            var UF_TYPE_OF_ACTIVITY = $(this_form).find("select[name=UF_TYPE_OF_ACTIVITY]").val();
            var LOGIN = $(this_form).find("input[name=LOGIN]").val();
            var EMAIL = $(this_form).find("input[name=EMAIL]").val();

            var phones = [];   var phoneErrors = [];
            $(this_form).find("input.phoneItem").each(function(){
                var phone = $(this).val();
                if( phone.length > 0 ){
                    var isError = !(/^ *\+? *?\d+([-\ 0-9]*|(\(\d+\)))*\d+ *$/.test(phone));
                    if( isError ){
                        phoneErrors.push( $(this) );
                    }
                }
            })

            var UF_SITE = $(this_form).find("input[name=UF_SITE]").val();
            var UF_INN = $(this_form).find("input[name=UF_INN]").val();
            var UF_OGRN = $(this_form).find("input[name=UF_OGRN]").val();

            var UF_EMAIL_FOR_ORDERS = $(this_form).find("input[name=UF_EMAIL_FOR_ORDERS]").val();

            // Проверки
            if (
                UF_INN.length > 0
                &&
                /[^\d]/.test(UF_INN)
            ){
                error = ['UF_INN', 'ИНН может содержать только цифры'];
            } else if (
                UF_INN.length > 0
                &&
                UF_INN.length != 10
                &&
                UF_INN.length != 12
            ){
                error = ['UF_INN', 'ИНН должен состоять из 10 либо 12 цифр'];
            } else if (
                UF_OGRN.length > 0
                &&
                /[^\d]/.test(UF_OGRN)
            ){
                error = ['UF_OGRN', 'ОГРН/ОГРНИП может содержать только цифры'];
            } else if (
                UF_OGRN.length > 0
                &&
                UF_OGRN.length != 13
                &&
                UF_OGRN.length != 15
            ){
                error = ['UF_OGRN', 'ОГРН/ОГРНИП должен состоять из 13 или 15 цифр'];
            } else if (UF_COMPANY_NAME.length == 0) {
                error = ['UF_COMPANY_NAME', 'Введите, пожалуйста, название компании!'];
            //} else if (UF_TYPE_OF_ACTIVITY == 'empty') {
                //error = ['UF_TYPE_OF_ACTIVITY', 'Выберите, пожалуйста, тип деятельности!'];
            } else if( phoneErrors.length > 0 ){
                for ( key in phoneErrors ){
                    var input = phoneErrors[key];
                    $(input).addClass('is___error');
                    $(input).attr('oninput','$(this).removeClass(\'is___error\'); $(this).attr("title", "");');
                }
                error = [false, 'Не корректный формат номера телефона<br>Допускаются только цифры, а также символы "+", "-", пробел и круглые скобки!'];
            } else if ( LOGIN != undefined && LOGIN.length == 0 ){
                error = ['LOGIN',   'Введите, пожалуйста, логин!'];
            } else if (EMAIL.length == 0){
                error = ['EMAIL',   'Введите, пожалуйста, Ваш адрес эл. почты!'];
            } else if (EMAIL.length > 0 && !(/^.+@.+\..+$/.test(EMAIL))) {
                error = ['EMAIL', 'Эл. почта содержит ошибки!'];
            } else if (UF_EMAIL_FOR_ORDERS.length > 0 && !(/^.+@.+\..+$/.test(UF_EMAIL_FOR_ORDERS))) {
                error = ['UF_EMAIL_FOR_ORDERS', 'Эл. почта для заказов содержит ошибки!'];
            } else if (
                UF_SITE.length > 0
                &&
                !(/^((ftp|http|https):\/\/)?(www\.)?([A-Za-zА-Яа-я0-9]{1}[A-Za-zА-Яа-я0-9\-]*\.?)*\.{1}[A-Za-zА-Яа-я0-9-]{2,8}(\/([\w#!:.?+=&%@!\-\/])*)?/.test(UF_SITE))
            ){
                error = ['UF_SITE',   'Не корректный формат адреса сайта<br>Пример: http://site.ru/'];
            }

            if( !error ){
                var errors = {links:[], errors:{}};
                if( $('.contactPersonItem').length > 0 ){
                    $('.contactPersonItem').each(function(){
                        var item = $(this);
                        $(item).find('input').each(function(){
                            var input = $(this);
                            var val = $(input).val();
                            if( $(input).hasClass('loc_input') && val.length == 0 ){
                                errors['links'].push( $(input).prev() );
                                errors['errors']['loc'] = 'Есть контактные лица, в которых не указаны местоположения';
                            }
                            if( $(input).hasClass('name_input') && val.length == 0 ){
                                errors['links'].push($(input));
                                errors['errors']['name_input'] = 'Есть контактные лица, в которых не указаны имена';
                            }
                            if( $(input).hasClass('last_name_input') && val.length == 0 ){
                                errors['links'].push($(input));
                                errors['errors']['last_name_input'] = 'Есть контактные лица, в которых не указаны фамилии';
                            }
                            /*if( $(input).hasClass('second_name_input') && val.length == 0 ){
                                errors['links'].push($(input));
                                errors['errors']['second_name_input'] = 'Есть контактные лица, в которых не указаны отчества';
                            }*/
                            /*if( $(input).hasClass('phone_1_input') && val.length == 0 ){
                                errors['links'].push($(input));
                                errors['errors']['phone_1_input'] = 'Есть контактные лица, в которых не указан телефон';
                            }*/
                        });

                        // Проверка наличия хотя бы 1 телефона
                        var first_phone_input = $(item).find('input.phoneItem').eq(0);
                        var first_phone = $(first_phone_input).val();
                        if( first_phone!=undefined && first_phone.length == 0 ){
                            errors['links'].push($(first_phone_input));
                            errors['errors']['contact_phone'] = 'Есть контактные лица, в которых не указано ни одного телефона';
                        }

                    })
                }
                if( Object.keys( errors.links ).length ){
                    for( key in errors.links ){
                        var link = errors.links[key];
                        $(link).addClass('is___error');
                        $(link).attr('oninput','$(this).removeClass(\'is___error\'); $(this).attr("title", "");');
                    }
                }
                if( Object.keys( errors.errors ).length ){
                    var ers_str = [];
                    for( key in errors.errors ){
                        var er = errors.errors[key];
                        ers_str.push(er);
                    }
                    error = [false, ers_str.join('<br>')];
                }
            }
            // Если нет ошибок
            if (!error){
                process(true);
                var form_data = $(this_form).serialize();
                $.post("/ajax/dealerDataUpdate.php", {form_data:form_data}, function(data){
                    if (data.status == 'ok'){
                        $('.contactPersonsBlock').replaceWith(data.dealerContactPersons);
                        $('.dealer_personal_data_form .addContactPerson').show();
                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Успешное сохранение!'});
                    } else if (data.status == 'error'){
                        show_error(this_form, false, data.text);
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 3000, layout: 'topRight', type: 'warning', text: "Ошибка запроса"});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                show_error(this_form, error[0], error[1]);
            }
        }
    });
    // Добавление контактного лица
    $(document).on('click', '.dealer_personal_data_form .addContactPerson', function() {
        if (!is_process(this)) {
            var contactPersonsBlock = $(this).parents('.contactPersonsBlock');
            if( $('.contactPersonItem[item_id=new]').length == 0 ){

                $(this).hide();

                var html = '<div class="contactPersonItem" item_id="new"><div class="dealerAccount-form__row"><div class="dealerAccount-form__field  dealerAccount-form__field--w271"><span>Населённый пункт</span><input class="account-form__suggestInput" id="dealerContactPerson_new" type="text" placeholder="Начните ввод города" cityName="" autocomplete="off" onfocus="cityFocus($(this));" onkeyup="cityKeyUp($(this));" onfocusout="cityFocusOut($(this));" value=""><input class="loc_input" type="hidden" name="contact_persons[new][loc_id]"></div><span class="account-form__deleteAddress removeContactPerson to___process">Удалить запись</span></div><div class="dealerAccount-form__row"><div class="dealerAccount-form__field"><span>Имя</span><input class="name_input" type="text" name="contact_persons[new][name]"></div><div class="dealerAccount-form__field"><span>Фамилия</span><input class="last_name_input" type="text" name="contact_persons[new][last_name]"></div><div class="dealerAccount-form__field"><span>Отчество</span><input class="second_name_input" type="text" name="contact_persons[new][second_name]"></div></div><div class="dealerAccount-form__row"><div class="dealerAccount-form__field"><span>Телефон</span><input class="phoneItem" type="tel" name=contact_persons[new][phones][]"></div><div class="dealerAccount-form__addTel"><span>Добавить телефон</span></div></div></div>';

                $(this).before(html);

                //$('input[type=tel]').mask('+7 (000) 000 00 00', {placeholder: "+7 (___) ___ __ __"});

                $('#dealerContactPerson_new').autocomplete({
                    source: '/ajax/searchCity.php',
                    minLength: 2,
                    delay: 700,
                    select: citySelectShort
                })

            }
        }
    })
    // Удаление контактного лица
    $(document).on('click', '.dealer_personal_data_form .removeContactPerson', function() {
        if (!is_process(this)) {
            $(this).parents('.contactPersonItem').remove();
            var contactPersonsBlock = $(this).parents('.contactPersonsBlock');
            if( $('.contactPersonItem[item_id=new]').length == 0 ){
                $('.dealer_personal_data_form .addContactPerson').show();
            }
        }
    })







    // РЕДАКТИРОВАНИЕ ЦЕН - выбор раздела каталога
    $(document).on('click', '.dealerGoodsForm .goodsLKSection', function() {
        if (!is_process(this)) {
            if( $(this).parents('.searchPanel-nav__item').hasClass('searchPanel-nav__item--active') ){
                var section_id = '';
            } else {
                var section_id = $(this).attr('section_id');
            }
            $('.dealerGoodsForm input[name=section_id]').val(section_id);
            reloadDealerGoods()
        }
    })


    // РЕДАКТИРОВАНИЕ ЦЕН - выбор региона
    $(document).on('change', '.dealerGoodsForm select[name=loc_id]', function() {
        if (!is_process(this)) {
            reloadDealerGoods()
        }
    })


    // РЕДАКТИРОВАНИЕ ЦЕН - Подгрузка ценовых предложений в ЛК дилера
    $(document).on('click', '.moreDealerOffersButton', function() {
        if (!is_process(this)) {
            var params = {stop_ids: []};
            $('.dealerOfferItem').each(function(){
                var item_id = $(this).attr('item_id');
                params['stop_ids'].push(item_id);
            })
            reloadDealerGoods(params);
        }
    })


    // РЕДАКТИРОВАНИЕ ЦЕН - удаление
    $(document).on('click', '.deleteDealerOfferButton', function() {
        if (!is_process(this)) {
            var params = {
                delete_offer_id: $(this).attr('offer_id'),
                stop_ids: []
            };
            $('.dealerOfferItem').each(function(){
                var item_id = $(this).attr('item_id');
                params['stop_ids'].push(item_id);
            })
            var n = noty({
                text        : 'Действительно удалить это предложение?',
                type        : 'alert',
                dismissQueue: true,
                layout      : 'center',
                theme       : 'defaultTheme',
                buttons     : [
                    {addClass: 'btn btn-primary', text: 'Да', onClick: function ($noty) {
                        $noty.close();
                        reloadDealerGoods(params);
                    }},
                    {addClass: 'btn btn-danger', text: 'Нет', onClick: function ($noty) {
                        $noty.close();
                    }}
                ]
            });
        }
    })


    // РЕДАКТИРОВАНИЕ ЦЕН - отображение кнопки сохранения
    $(document).on('keyup', '.dealerOfferItem input[name=quantity], .dealerOfferItem input[name=price]', function() {
        var _this = $(this);
        //$(this).parents('.dealerOfferItem').find('.dealerPriceSave').show();
        setTimeout(() => {
            _this.parents('.dealerOfferItem').find('.dealerPriceSave').trigger('click');
        }, 1000);
		
    })
    $(document).on('change', '.dealerOfferItem input[name=pod_zakaz]', function() {
        //$(this).parents('.dealerOfferItem').find('.dealerPriceSave').show();
		$(this).parents('.dealerOfferItem').find('.dealerPriceSave').trigger('click');
    })
    // РЕДАКТИРОВАНИЕ ЦЕН - сохранение
    $(document).on('click', '.dealerPriceSave', function() {
        if (!is_process(this)) {

            $.noty.closeAll();

            var input = $(this);
            var params = {
                save_id: $(input).attr('offer_id'),
                price: $(input).parents('tr').find('input[name=price]').val(),
                quantity: $(input).parents('tr').find('input[name=quantity]').val(),
                pod_zakaz: $(input).parents('tr').find('input[name=pod_zakaz]:checked').length>0?'Y':'N',
                action: 'dealerPriceSave'
            };

            var errors = {};
            if( params.price.length == 0 ) {
                errors.price = 'Введите цену предложения';
            }
            if( params.quantity.length == 0 ){
                errors.quantity =  'Введите количество на складе';
            }

            if( Object.keys( errors ).length > 0 ){
                for( key in errors ){
                    var value = errors[key];
                    $(input).parents('tr').find('input[name='+key+']').addClass('is___error').attr('oninput', '$(this).removeClass("is___error"); $(this).attr("title", ""); $.noty.closeAll();');
                    var n = noty({closeWith: ['center'], timeout: 5000, layout: 'center', type: 'warning', text: value});
                }
            } else {
                process(true);
                $.post("/ajax/ajax.php", params, function(data){
                    if (data) {
                        if (data.status == 'ok') {

                            $(input).hide();

                        } else if (data.status == 'error') {
                            var n = noty({
                                closeWith: ['center'],
                                timeout: 5000,
                                layout: 'center',
                                type: 'warning',
                                text: data.text
                            });
                        }
                    } else {
                        var n = noty({
                            closeWith: ['center'],
                            timeout: 5000,
                            layout: 'center',
                            type: 'warning',
                            text: 'Ошибка запроса'
                        });
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['center'], timeout: 5000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            }
        }
    })










    // СОЗДАНИЕ ЦЕН - выбор раздела каталога
    $(document).on('click', '.dealerAddPricesForm .addPricesLKSection', function() {
        if (!is_process(this)) {
            if( $(this).parents('.searchPanel-nav__item').hasClass('searchPanel-nav__item--active') ){
                var section_id = '';
            } else {
                var section_id = $(this).attr('section_id');
            }
            //if( section_id.length > 0 ){
                $('.dealerAddPricesForm input[name=section_id]').val(section_id);
                reloadAddPricesPage();
            //}
        }
    })

    // СОЗДАНИЕ ЦЕН - Подгрузка товаров
    $(document).on('click', '.addPricesMoreButton', function() {
        if (!is_process(this)) {
            var params = {stop_ids: []};
            $('.catalog___item').each(function(){
                var item_id = $(this).attr('item_id');
                params['stop_ids'].push(item_id);
            })
            reloadAddPricesPage(params);
        }
    })

    // СОЗДАНИЕ ЦЕН - Ввод в строку поиска
    $(document).on('keyup', '.dealerAddPricesForm input[name=q]', function() {
        clearInterval(LKsearchInterval);
        LKsearchInterval = setInterval(function(){
            clearInterval(LKsearchInterval);
            reloadAddPricesPage(false, true);
        }, 700);
    })

    // СОЗДАНИЕ ЦЕН - открытие окна добавления цены
    $(document).on('click', '.addPriceOpenWindow', function() {
        if (!is_process(this)) {
            var postParams = {
                item_id: $(this).attr('item_id')
            };
            process(true);
            $.post("/ajax/personalAddPriceOpenWindow.php", postParams, function(data){
                if (data.status == 'ok'){
					console.log(data);
                    $('#modalAddGoods .modal-addGoods__title').html(data.el.NAME);
                    $('#modalAddGoods a.modal-addGoods__image img').attr('src', data.el.IMG);
                    $('#modalAddGoods input[name=product_id]').val(data.el.ID);
                    $('#modalAddGoods .modal-addGoods__table').html(data.items_html);

                    $('#modalAddGoods .error___p').html('');

                    $('.modalAddGoodsLink').fancybox({
                        padding: 0,
                        wrapCSS: 'fancybox-review'
                    }).trigger('click');

                } else if (data.status == 'error'){
                    var n = noty({closeWith: ['center'], timeout: 5000, layout: 'center', type: 'warning', text: data.text});
                }
            }, 'json')
            .fail(function(data, status, xhr){
                var n = noty({closeWith: ['center'], timeout: 5000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    })


    // СОЗДАНИЕ ЦЕН - доп. строка
    $(document).on('click', '.dopPriceOfferButton', function() {
        if (!is_process(this)) {
            var lastOfferRow = $('.newPriceOfferItem').last();
            var number = $(lastOfferRow).attr('number');
            number++;
            var product_id = $('#modalAddGoods input[name=product_id]').val();

            var postParams = {
                number: number,
                product_id: product_id
            };
            process(true);
            $.post("/ajax/personalNewPriceOfferItem.php", postParams, function(data){
                if (data.status == 'ok'){
                    $('#modalAddGoods .modal-addGoods__table').append(data.item_html);
                } else if (data.status == 'error'){
                    var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: data.text});
                }
            }, 'json')
            .fail(function(data, status, xhr){
                var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    })


    // СОЗДАНИЕ ЦЕН
    $(document).on('click', '.addPriceButton', function() {
        if( !is_process(this) ){
            
            var form = $(this).parents('form');
            $(form).find('.error___p').html('');

            var errors = {};

            $(form).find('.loc_input').each(function(){
                var val = $(this).val();
                if( val.length == 0 ){
                    if( 'loc' in errors ){} else {    errors['loc'] = {};    }
                    if( 'text' in errors['loc'] ){} else {
                        errors['loc']['text'] = 'Не указан регион ценового предложения';
                    }
                    if( 'links' in errors['loc'] ){} else {
                        errors['loc']['links'] = [];
                    }
                    errors['loc']['links'].push($(this).prev());
                }
            })

            $(form).find('.quantity_input').each(function(){
                var val = $(this).val();
                if( val.length == 0 ){
                    if( 'quantity' in errors ){} else {    errors['quantity'] = {};    }
                    if( 'text' in errors['quantity'] ){} else {
                        errors['quantity']['text'] = 'Не указано количество товара';
                    }
                    if( 'links' in errors['quantity'] ){} else {
                        errors['quantity']['links'] = [];
                    }
                    errors['quantity']['links'].push($(this));
                }
            })

            $(form).find('.price_input').each(function(){
                var val = $(this).val();
                if( val.length == 0 ){
                    if( 'price' in errors ){} else {    errors['price'] = {};    }
                    if( 'text' in errors['price'] ){} else {
                        errors['price']['text'] = 'Не указана цена товара';
                    }
                    if( 'links' in errors['price'] ){} else {
                        errors['price']['links'] = [];
                    }
                    errors['price']['links'].push($(this));
                }
            })

            if( Object.keys( errors ).length > 0 ){

                var str_errors = [];
                for ( key in errors ){
                    var ar = errors[key];
                    str_errors.push(ar.text);
                    for ( k in ar['links'] ) {
                        var link = ar['links'][k];
                        $(link).addClass('is___error').attr('oninput', '$(this).removeClass("is___error"); $(this).attr("title", ""); $.noty.closeAll();');
                    }
                }
                $(form).find('.error___p').html( str_errors.join('<br>') );

            } else {

                var postParams = {
                    form_data: $(this).parents('form').serialize()
                };
                process(true);
                $.post("/ajax/addDealerPrice.php", postParams, function (data) {
                    if (data.status == 'ok') {
                        var n = noty({closeWith: ['center'], timeout: 5000, layout: 'center', type: 'success', text: 'Успешное сохранение...'});
                        $('.fancybox-close').trigger('click');
                    } else if (data.status == 'error') {
                        $(form).find('.error___p').html( data.text );
                    }
                }, 'json')
                .fail(function (data, status, xhr) {
                    $(form).find('.error___p').html( 'Ошибка запроса' );
                })
                .always(function (data, status, xhr) {
                    process(false);
                })
            }
        }
    })







    // ТОЧКИ ПРОДАЖ - запрос пустой формы
    $(document).on('click', '.addDealerShopButton', function(){
        if ( !is_process(this)  ) {
            if ($('.emptyShopBlock').length == 0) {
                process(true);
                $.post("/ajax/emptyDealerShop.php", {}, function (data) {
                    if (data.status == 'ok') {
                        $('.dealerShopsArea').html(data.html);
                        var destination = $('.emptyShopBlock').offset();
                        $('body, html').animate({scrollTop: destination.top - 60}, 300);

                        $('.dealerShopsSave').show();

                    }
                }, 'json')
                .fail(function (data, status, xhr) {
                    var n = noty({
                        closeWith: ['hover'],
                        timeout: 5000,
                        layout: 'topRight',
                        type: 'warning',
                        text: "Ошибка запроса"
                    });
                })
                .always(function (data, status, xhr) {
                    process(false);
                })
            } else {
                var destination = $('.emptyShopBlock').offset();
                $('body, html').animate({scrollTop: destination.top - 60}, 300);
            }

        }
    });



    // ТОЧКИ ПРОДАЖ - удаление
    $(document).on('click', '.removeDealerShop', function(){
        $.noty.closeAll();

        if ( !is_process(this)  ) {

            var id = $(this).parents('.dealerLKShopItem').attr('item_id');

            if( id == 'new' ) {

                $('.emptyShopBlock').remove();
                $('.dealerLKShopsForm .error___p').html('');

                if( $('.dealerLKShopItem').length > 0 ){
                    $('.dealerShopsSave').show();
                } else {
                    $('.dealerShopsSave').hide();
                }

            } else {

                var n = noty({
                    text        : 'Действительно удалить точку продаж?',
                    type        : 'alert',
                    dismissQueue: true,
                    layout      : 'center',
                    theme       : 'defaultTheme',
                    buttons     : [
                        {
                            addClass: 'btn btn-primary',
                            text: 'Да',
                            onClick: function ($noty){
                                // Запрос
                                $noty.close();
                                process(true);
                                $.post("/ajax/removeDealerShop.php", {id:id}, function (data) {
                                    if (data.status == 'ok') {
                                        $('.dealerShopsArea').html(data.html);
                                        if( $('.dealerLKShopItem').length > 0 ){
                                            var destination = $('.dealerLKShopItem').eq(0).offset();
                                        } else {
                                            var destination = $('.dealerLKShopsForm').offset();
                                        }
                                        $('body, html').animate({scrollTop: destination.top - 60}, 300);

                                        if( $('.dealerLKShopItem').length > 0 ){
                                            $('.dealerShopsSave').show();
                                        } else {
                                            $('.dealerShopsSave').hide();
                                        }

                                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Успешное сохранение!'});

                                    }
                                }, 'json')
                                .fail(function (data, status, xhr) {
                                    var n = noty({
                                        closeWith: ['hover'],
                                        timeout: 5000,
                                        layout: 'topRight',
                                        type: 'warning',
                                        text: "Ошибка запроса"
                                    });
                                })
                                .always(function (data, status, xhr) {
                                    process(false);
                                })
                            }
                        },
                        {
                            addClass: 'btn btn-danger',
                            text: 'Нет',
                            onClick: function ($noty) { $noty.close(); }
                        }
                    ]
                });
            }

        }
    });





    // ТОЧКИ ПРОДАЖ - сохранение точек продаж
    $(document).on('click', '.dealerShopsSave', function(){
        $.noty.closeAll();
        if (!is_process(this)){
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find(".selectDropdown").removeClass("is___error");
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('.error___p').html('');
            // Задаём переменные
            var errors = { links:[], errors:{} };

            if( $('.dealerLKShopItem').length > 0 ){
                $(this_form).find('input[type=text], input[type=hidden]').each(function(){
                    var input = $(this);
                    var value = $(input).val();
                    if( $(input).hasClass('name_input') ){
                        if( value.length == 0 ){
                            errors['links'].push(input);
                            errors['errors']['name'] = 'Укажите, пожалуйста, название точки продаж';
                        }
                    }
                    /*if( $(input).hasClass('region_input') ){
                        if( value.length == 0 ){
                            errors['links'].push(input);
                            errors['errors']['region_input'] = 'Укажите, пожалуйста, регион';
                        }
                    }*/
                    if( $(input).hasClass('loc_input') ){
                        if( value.length == 0 ){
                            errors['links'].push(input);
                            errors['errors']['loc_input'] = 'Укажите, пожалуйста, населённый пункт';
                        }
                    }
                    if( $(input).hasClass('street_input') ){
                        if( value.length == 0 ){
                            errors['links'].push(input);
                            errors['errors']['street_input'] = 'Введите, пожалуйста, улицу точки';
                        }
                    }
                    if( $(input).hasClass('house_input') ){
                        if( value.length == 0 ){
                            errors['links'].push(input);
                            errors['errors']['house_input'] = 'Введите, пожалуйста, номер дома';
                        }
                    }

                    if( $(input).hasClass('gps_input') ){
                        if( value.length > 0 && !is_write_coords(value) ){
                            errors['links'].push(input);
                            errors['errors']['house_input'] = 'Некорректный формат координат';
                        }
                    }

                    /*if( $(input).hasClass('index_input') ){
                        if( value.length == 0 ){
                            errors['links'].push(input);
                            errors['errors']['index_input'] = 'Введите, пожалуйста, почтовый индекс';
                        }
                    }*/
                    if( $(input).hasClass('email_input') ){
                        /*if( value.length == 0 ){
                            errors['links'].push(input);
                            errors['errors']['email_input'] = 'Введите, пожалуйста, Email';
                        } else*/ if( value.length > 0 && !(/^.+@.+\..+$/.test(value)) ){
                            errors['links'].push(input);
                            errors['errors']['email_input'] = 'Эл. почта содержит ошибки';
                        }
                    }
                    /*if( $(input).hasClass('site_input') ){
                        if( value.length == 0 ){
                            errors['links'].push(input);
                            errors['errors']['site_input'] = 'Введите, пожалуйста, адрес сайта';
                        }
                    }*/
                });
                $('.dealerLKShopItem').each(function(){
                    var item = $(this);
                    // Телефоны
                    var phones_cnt = 0;
                    $(item).find('input.phone_input').each(function(){
                        var value = $(this).val();
                        if( value.length > 0 ){    phones_cnt++;    }
                    })
                    if( phones_cnt == 0 ){
                        errors['links'].push( $(item).find('input.phone_input').eq(0) );
                        errors['errors']['phone_input'] = 'В некоторых точках продаж не введено ни одного номера телефона';
                    }
                    /*if( $(item).find('select.obedTimeOt').val() == 'empty' ){
                        errors['links'].push( $(item).find('select.obedTimeOt').next() );
                        errors['errors']['obedTimeOt'] = 'В некоторых точках продаж не указано время начала обеда';
                    }
                    if( $(item).find('select.obedTimeDo').val() == 'empty' ){
                        errors['links'].push( $(item).find('select.obedTimeDo').next() );
                        errors['errors']['obedTimeDo'] = 'В некоторых точках продаж не указано время окончания обеда';
                    }*/
                })

            }

            // Если нет ошибок
            if ( Object.keys( errors.errors ).length == 0 ){

                process(true);
                var form_data = $(this_form).serialize();
                $.post("/ajax/dealerShopsSave.php", {form_data:form_data}, function(data){
                    if (data.status == 'ok'){
                        $('.dealerLKShopsForm').replaceWith(data.html);
                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Успешное сохранение!'});
                    } else if (data.status == 'error'){
                        show_error(this_form, false, data.text);
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'topRight', type: 'warning', text: "Ошибка запроса"});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {

                if( Object.keys( errors.links ).length ){
                    for( key in errors.links ){
                        var link = errors.links[key];
                        $(link).addClass('is___error');
                        if( $(link).hasClass('loc_input') ){
                            $(link).prev().addClass('is___error');
                            $(link).prev().attr('oninput','$(this).removeClass(\'is___error\');');
                        } else if( $(link).hasClass('region_input') ){
                            $(link).prev().addClass('is___error');
                            $(link).prev().attr('oninput','$(this).next().removeClass(\'is___error\');');
                        } else if( $(link).hasClass('selectDropdown') ){
                            $(link).prev().attr('onchange','$(this).next().removeClass(\'is___error\');');
                        } else {
                            $(link).attr('oninput','$(this).removeClass(\'is___error\');');
                        }
                    }
                }

                if( Object.keys( errors.errors ).length ){
                    var ers_str = [];
                    for( key in errors.errors ){
                        var er = errors.errors[key];
                        ers_str.push(er);
                    }
                    error = [false, ers_str.join('<br>')];
                    show_error(this_form, false, error);
                }

            }
        }
    });







    // Сохранение статусов заказа
    $(document).on('click', '.dealerOrderStatusesSave', function(){
        var button = $(this);

        var id = $(this).data("id");
        $("#save_order_id").val(id);

        $.noty.closeAll();
        if (!is_process(button)){
            $(button).html('Ожидание...');
            // Форма
            var this_form = $(button).parents('form');
            var postParams = {
                form_data: $(this_form).serialize()
            };
            process(true);
            $.post("/ajax/orderStatusSave.php", postParams, function(data){
                if (data.status == 'ok'){
                    $(this_form).replaceWith(data.html);
                    
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'success', text: 'Успешное сохранение!'});
                    
                } else if (data.status == 'error'){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: data.text});
                }
            }, 'json')
            .fail(function(data, status, xhr){
                var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
            })
            .always(function(data, status, xhr){
                process(false);
                $(button).html('Сохранить');
            })

        }
    });


    $(document).on('input', '.dealerActiveOrdersForm textarea', function(){
        var counter3 = (300 - $(this).val().length);
        $(this).siblings('.account-form__textarea-counter').find('span').html(counter3);
    });

    // Сортировка заказов
    $(document).on('click', '.sortTD', function(){
        $.noty.closeAll();
        if (!is_process(this)){
            var this_form = $(this).parents('form');
            var postParams = {
                sort_field: $(this).attr('sort_field'),
                sort_order: $(this).attr('sort_order'),
            };
            process(true);
            $.post("/ajax/changeDealerOrdersSort.php", postParams, function(data){
                if (data.status == 'ok'){
                    $(this_form).replaceWith(data.html);
                } else if (data.status == 'error'){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: data.text});
                }
            }, 'json')
            .fail(function(data, status, xhr){
                var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    })




    // Подгрузка архив. заказов
    $(document).on('click', '.archOrdersMoreButton', function(){
        if (!is_process(this)){
            var stop_ids = [];
            $('.archOrderItem').each(function(){
                var item_id = $(this).attr('item_id');
                stop_ids.push(item_id);
            })
            var postParams = {
                stop_ids: stop_ids,
            };
            process(true);
            $.post("/ajax/archOrdersLoad.php", postParams, function(data){
                if (data.status == 'ok'){
                    $('.archOrdersLoadArea tbody:first').append(data.html);

                    if( $('tr.ost').length > 0 ){
                        $('.archOrdersMoreButton').show();
                    } else {
                        $('.archOrdersMoreButton').hide();
                    }
                    $('tr.ost').remove();
                } else if (data.status == 'error'){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: data.text});
                }
            }, 'json')
            .fail(function(data, status, xhr){
                var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    });





    // Подгрузка отзывов о дилере
    $(document).on('click', '.dealerReviewsMoreButton', function(e){
        if (!is_process(this)){
            var stop_ids = [];
            $('.dealarReviewItem').each(function(){
                var item_id = $(this).attr('item_id');
                stop_ids.push(item_id);
            })
            var postParams = {
                stop_ids: stop_ids,
            };
            if( $(this).hasClass('dealer_page') ){
                postParams['is_dealer_page'] = 'Y';
            }
            process(true);
            $.post("/ajax/dealerReviewsLoad.php", postParams, function(data){
                if (data.status == 'ok'){
                    $('.dealerReviewsArea').append(data.html);
                    if( $('tr.ost').length > 0 ){
                        $('.dealerReviewsMoreButton').show();
                    } else {
                        $('.dealerReviewsMoreButton').hide();
                    }
                    $('tr.ost').remove();
                } else if (data.status == 'error'){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: data.text});
                }
            }, 'json')
            .fail(function(data, status, xhr){
                var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
            })
            .always(function(data, status, xhr){
                process(false);
            })


        }
    })







    // МЕСТОПОЛОЖЕНИЕ ДОСТАВКИ - добавление
    $(document).on('click', '.newDeliveryLocationButton', function(e){
        if (!is_process(this)){
            var this_form = $(this).parents('form');
            var loc_id = $(this_form).find('input[name=loc_id]').val();
            var error = false;
            if( loc_id.length == 0 ){
                error = 'Укажите населённый пункт для доставки';
            }
            if( error ){
                show_error( this_form, false, error )
            } else {
                var postParams = {
                    loc_id: loc_id
                };
                process(true);
                $.post("/ajax/addDeliveryLocation.php", postParams, function(data){
                    if (data.status == 'ok'){
                        $(this_form).find('input[name=loc_id]').val('');
                        $(this_form).find('input#deliveryLocation').val('');
                        $('.deliveryLocationsBlock').replaceWith(data.html);
                        $('.fancybox-close').trigger('click');

                        createStyledSelects();
                        var t = "";
                        initOpenCloseDropdown(t);

                    } else if (data.status == 'error'){
                        show_error( $(this_form), false, data.text )
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            }

        }
    });



    // МЕСТОПОЛОЖЕНИЕ ДОСТАВКИ - удаление
    $(document).on('click', '.removeDeliveryLocationButton', function(e){
        var button = $(this);
        if (!is_process(button)){
            var n = noty({
                text        : 'Действительно удалить это местоположение?',
                type        : 'alert',
                dismissQueue: true,
                layout      : 'center',
                theme       : 'defaultTheme',
                buttons     : [
                    {addClass: 'btn btn-primary', text: 'Да', onClick: function ($noty){
                        $noty.close();

                        var item_id = $(button).attr('item_id');
                        var postParams = { item_id: item_id };
                        process(true);
                        $.post("/ajax/removeDeliveryLocation.php", postParams, function(data){
                            if (data.status == 'ok'){
                                $('.deliveryLocationsBlock').replaceWith(data.html);
                                createStyledSelects();
                                var t = "";
                                initOpenCloseDropdown(t);
                            } else if (data.status == 'error'){
                                var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: data.text});
                            }
                        }, 'json')
                        .fail(function(data, status, xhr){
                            var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
                        })
                        .always(function(data, status, xhr){
                            process(false);
                        })

                    }},
                    {addClass: 'btn btn-danger', text: 'Нет', onClick: function ($noty){
                        $noty.close();
                    }}
                ]
            });
        }
    });



    // МЕСТОПОЛОЖЕНИЕ ДОСТАВКИ - сохранение настроек
    $(document).on('click', '.deliveryLocationSaveButton', function(e){
        if (!is_process(this)){
            var this_form = $(this).parents('form');
            var error = false;
            if( error ){
                show_error( this_form, false, error );
            } else {
                var form_data = $(this_form).serialize();
                var postParams = {
                    form_data: form_data
                };
                process(true);
                $.post("/ajax/deliveryLocationUpdate.php", postParams, function(data){
                    if (data.status == 'ok'){
                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Успешное сохранение!'});
                    } else if (data.status == 'error'){
                        show_error( $(this_form), false, data.text )
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            }

        }
    });




    // МЕСТОПОЛОЖЕНИЕ ДЛЯ АВТООБНОВЛЕНИЯ ЦЕН - добавление
    $(document).on('click', '.newPricesAutoUpdateLocationButton', function(e){
        if (!is_process(this)){
            var this_form = $(this).parents('form');
            var loc_id = $(this_form).find('input[name=loc_id]').val();
            var error = false;
            if( loc_id.length == 0 ){
                error = 'Укажите населённый пункт для автообновления цен';
            }
            if( error ){
                show_error( this_form, false, error )
            } else {
                var postParams = {
                    loc_id: loc_id
                };
                process(true);
                $.post("/ajax/addPricesAutoUpdateLocation.php", postParams, function(data){
                    if (data.status == 'ok'){
                        $(this_form).find('input[name=loc_id]').val('');
                        $(this_form).find('input#deliveryLocation').val('');
                        $('.autoUpdateLocationsBlock').replaceWith(data.html);
                        $('.fancybox-close').trigger('click');
                    } else if (data.status == 'error'){
                        show_error( $(this_form), false, data.text )
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            }

        }
    });


    // МЕСТОПОЛОЖЕНИЕ ДЛЯ ОБНОВЛЕНИЯ ЦЕН - удаление
    $(document).on('click', '.removePricesAutoUpdateLocationButton', function(e){
        var button = $(this);
        if (!is_process(button)){
            var n = noty({
                text        : 'Действительно удалить это местоположение?',
                type        : 'alert',
                dismissQueue: true,
                layout      : 'center',
                theme       : 'defaultTheme',
                buttons     : [
                    {addClass: 'btn btn-primary', text: 'Да', onClick: function ($noty){
                        $noty.close();

                        var item_id = $(button).attr('item_id');
                        var postParams = { item_id: item_id };
                        process(true);
                        $.post("/ajax/removePricesAutoUpdateLocation.php", postParams, function(data){
                            if (data.status == 'ok'){
                                $('.autoUpdateLocationsBlock').replaceWith(data.html);
                            } else if (data.status == 'error'){
                                var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: data.text});
                            }
                        }, 'json')
                        .fail(function(data, status, xhr){
                            var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
                        })
                        .always(function(data, status, xhr){
                            process(false);
                        })

                    }},
                    {addClass: 'btn btn-danger', text: 'Нет', onClick: function ($noty){
                        $noty.close();
                    }}
                ]
            });
        }
    });


    $(document).on('input', '.dealerAutoUpdatePricesInput', function(e){
        $(this).parents('div.dealerAccount-delivery__header').find('.pricesAutoUpdateLocationSaveButton').show();
    })

    // МЕСТОПОЛОЖЕНИЕ ДЛЯ АВТООБНОВЛЕНИЯ ЦЕН - сохранение настроек
    $(document).on('click', '.pricesAutoUpdateLocationSaveButton', function(e){
        if (!is_process(this)){
            var button = $(this);
            var this_form = $(this).parents('form');
            var error = false;
            if( error ){
                show_error( this_form, false, error );
            } else {
                var form_data = $(this_form).serialize();
                var item_id = $(this).attr('item_id');
                var postParams = {
                    item_id: item_id,
                    form_data: form_data,
                };
                process(true);
                $.post("/ajax/pricesAutoUpdateLocationUpdate.php", postParams, function(data){
                    if (data.status == 'ok'){
                        //var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Успешное сохранение!'});
                        $(button).hide();
                    } else if (data.status == 'error'){
                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: data.text});
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            }

        }
    });




    // ВАРИАНТЫ ОПЛАТЫ - сохранение
    $(document).on('click', '.dealerPaymentsSaveButton', function(e){
        if (!is_process(this)){
            var this_form = $(this).parents('form');
            var error = false;
            if( error ){
                show_error( this_form, false, error );
            } else {
                var form_data = $(this_form).serialize();
                var postParams = {
                    form_data: form_data
                };
                process(true);
                $.post("/ajax/dealerPaymentsSave.php", postParams, function(data){
                    if (data.status == 'ok'){
                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Успешное сохранение!'});
                    } else if (data.status == 'error'){
                        show_error( $(this_form), false, data.text )
                    }
                }, 'json')
                    .fail(function(data, status, xhr){
                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
                    })
                    .always(function(data, status, xhr){
                        process(false);
                    })
            }

        }
    });



    // Смена типа информации по доставке (ссылка/файл)
    $(document).on('change', 'input[name=delivery_info_type]', function(e){
        var type = $(this).val();
        $('.delivLinkBlock, .delivFileBlock').hide();
        if( type == 'link' ){
            $('.delivLinkBlock').show();
        } else if( type == 'file' ){
            $('.delivFileBlock').show();
        }
    })



    // ИНФО О ДОСТАВКЕ - сохранение
    $(document).on('click', '.dealerDelivInfoSaveButton', function(e){
        if (!is_process(this)){
            var this_form = $(this).parents('form');
            var form_data = $(this_form).serialize();
            var postParams = {
                form_data: form_data
            };
            process(true);
            $.post("/ajax/dealerDelivInfoSave.php", postParams, function(data){
                if (data.status == 'ok'){
                    $('.deliveryLocationsBlock').replaceWith(data.html);
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Успешное сохранение!'});
                } else if (data.status == 'error'){
                    var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: data.text});
                }
            }, 'json')
            .fail(function(data, status, xhr){
                var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    })


    // ИНФО О ДОСТАВКЕ - удаление файла
    $(document).on('click', '.deleteDeliveryFile', function(e){
        if (!is_process(this)){
            var n = noty({
                text        : 'Действительно удалить файл?',
                type        : 'alert',
                dismissQueue: true,
                layout      : 'center',
                theme       : 'defaultTheme',
                buttons     : [
                    {addClass: 'btn btn-primary', text: 'Да', onClick: function ($noty) {
                        $noty.close();

                        var postParams = {};
                        process(true);
                        $.post("/ajax/deleteDeliveryFile.php", postParams, function(data){
                            if (data.status == 'ok'){
                                $('.deliveryLocationsBlock').replaceWith(data.html);
                            } else if (data.status == 'error'){
                                var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: data.text});
                            }
                        }, 'json')
                        .fail(function(data, status, xhr){
                            var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
                        })
                        .always(function(data, status, xhr){
                            process(false);
                        })

                    }},
                    {addClass: 'btn btn-danger', text: 'Нет', onClick: function ($noty) { $noty.close(); } }
                ]
            });
        }
    });


    // ИНФО О ДОСТАВКЕ - загрузка файла
    $(document).on('click', '.newDeliveryFile', function(e){
        if (!is_process(this)){
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var file_title = $(this_form).find('input[name=deliv_info_file_title]').val();
            if( file_title.length == 0 ){
                show_error(this_form, 'deliv_info_file_title', 'Укажите заголовок файла');
            } else {
                $('input[name='+deliveryFileInputName+']').val('');
                $('input[name='+deliveryFileInputName+']').trigger('click');
                $('input[name=deliveryFileTitle]').val(file_title);
            }
        }
    })

    $(document).on('change', 'input[name='+deliveryFileInputName+']', function(){
        $(".deliveryFile___form").submit();
    })



    // ФАЙЛ СООТВЕТСТВИЯ CSV  - загрузка файла
    $(document).on('click', '.loadDealerMappingCSVButton', function(e){
        if (!is_process(this)){
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            $(this_form).find('p.success___p').remove();
            // Задаём переменные
            var csv = $(this_form).find('input[name=csv]').val();

            if( csv.length == 0 ){
                show_error(this_form, 'csv', 'Выберите, пожалуйста, файл соответствия артикулов в CSV-формате!');
            } else {
                $(this_form).submit();
            }
        }
    })

    $(document).on('change', '.dealerLoadMappingCSVForm input[name=csv]', function(){
        var val = $(this).val();
        var ar = val.split('\\');
        var file_name = ar[ ar.length-1 ];
        $(this).parents('label').find('span').eq(0).html(file_name);
        $(this).parents('form').find('p.error___p').html('');
    })

    // ФАЙЛ СООТВЕТСТВИЯ CSV  - очистка записей
    $(document).on('click', '.clearDealerMappingCSVButton', function(e){
        if (!is_process(this)){

            var n = noty({
                text        : 'Вы&nbsp;действительно хотите&nbsp;очистить загруженную&nbsp;ранее таблицу&nbsp;соответствия&nbsp;артикулов?',
                type        : 'alert',
                dismissQueue: true,
                layout      : 'center',
                theme       : 'defaultTheme',
                buttons     : [
                    {
                        addClass: 'btn btn-primary',
                        text: 'Да',
                        onClick: function ($noty){
                            // Запрос
                            $noty.close();

                            process(true);
                            $.post("/ajax/clearDealerMappingCSV.php", {}, function(data){
                                if (data.status == 'ok'){
                                    $('.dealerLoadMappingCSVFormBlock').replaceWith( data.html );
                                } else if (data.status == 'error'){
                                    show_message( data.text );
                                }
                            }, 'json')
                            .fail(function(data, status, xhr){
                                show_message( 'Ошибка запроса' );
                            })
                            .always(function(data, status, xhr){
                                process(false);
                            })

                        }
                    },
                    {
                        addClass: 'btn btn-danger',
                        text: 'Нет',
                        onClick: function ($noty) { $noty.close(); }
                    }
                ]
            });

        }
    })



    // ФАЙЛ CSV С ЦЕНАМИ - загрузка файла
    $(document).on('click', '.loadCSVButton', function(e){
        if (!is_process(this)){
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var loc_id = $(this_form).find('input[name=loc_id]').val();
            var csv = $(this_form).find('input[name=csv]').val();

            if( loc_id.length == 0 ){
                show_error(this_form, 'fileRegion', 'Укажите, пожалуйста, регион для загрузки цен!');
            } else if( csv.length == 0 ){
                show_error(this_form, 'csv', 'Выберите, пожалуйста, файл CSV с ценами!');
            } else {
                $(this_form).submit();
            }
        }
    })

    $(document).on('change', '.loadCSVForm input[name=csv]', function(){
        var val = $(this).val();
        var ar = val.split('\\');
        var file_name = ar[ ar.length-1 ];
        $(this).parents('label').find('span').eq(0).html(file_name);
    })




    // ФАЙЛ YML С ЦЕНАМИ - загрузка файла
    $(document).on('click', '.loadYMLButton', function(e){
        if (!is_process(this)){
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var loc_id = $(this_form).find('input[name=loc_id]').val();
            var yml_load_type = $(this_form).find('input[name=yml_load_type]:checked').val();
            var yml = $(this_form).find('input[name=yml]').val();
            var yml_link = $(this_form).find('input[name=yml_link]').val();

            if( loc_id.length == 0 ){
                show_error(this_form, 'fileRegion', 'Укажите, пожалуйста, регион для загрузки цен!');
            } else if( yml_load_type == 'file' && yml.length == 0 ){
                show_error(this_form, 'yml', 'Выберите, пожалуйста, файл YML с ценами!');
            } else if( yml_load_type == 'link' && yml_link.length == 0 ){
                show_error(this_form, 'yml_link', 'Укажите, пожалуйста, ссылку на файл YML с ценами!');
            } else {

                $(this_form).submit();

            }
        }
    })

    $(document).on('change', '.loadYMLForm input[name=yml]', function(){
        var val = $(this).val();
        var ar = val.split('\\');
        var file_name = ar[ ar.length-1 ];
        $(this).parents('label').find('span').eq(0).html(file_name);
    })

    $(document).on('change', 'input[name=yml_load_type]', function(e){
        var this_form = $(this).parents('form');
        $(this_form).find('p.error___p').html('');
        var type = $(this).val()
        if( type == 'link' ){
            $('#yml_type_link_block').show();
            $('#yml_type_file_block').hide();
        } else if( type == 'file' ){
            $('#yml_type_link_block').hide();
            $('#yml_type_file_block').show();
        }
    })




    // ЛК дилера - сохранение настроек местоположения доставки
    $(document).on('click', '.dealerDeliveryPricesSave', function(e){
        var button = $(this);
        if (!is_process(button)){
            var this_form = $(button).parents('form');
            var form_data = $(this_form).serialize();
            var postParams = { form_data: form_data };
            var start_point_address = $(this_form).find('input[name=start_point_address]').val();
            var start_point_coords = $(this_form).find('input[name=start_point_coords]').val();
            var delivery_time_ot = $(this_form).find('select[name=delivery_time_ot]').val();
            var delivery_time_do = $(this_form).find('select[name=delivery_time_do]').val();
            var delivery_time_ot_h = false;
            var delivery_time_do_h = false;
            if( delivery_time_ot.length > 0 && delivery_time_ot != 'empty' ){
                var ar = delivery_time_ot.split(':');
                delivery_time_ot_h = ar[0];
                var first = delivery_time_ot_h.substring(0, 1);
                if( first == '0' ){
                    delivery_time_ot_h = delivery_time_ot_h.substring(1, delivery_time_ot_h.length);
                }
                delivery_time_ot_h = Number(delivery_time_ot_h);
            }
            if( delivery_time_do.length > 0 && delivery_time_do != 'empty' ){
                var ar = delivery_time_do.split(':');
                delivery_time_do_h = ar[0];
                var first = delivery_time_do_h.substring(0, 1);
                if( first == '0' ){
                    delivery_time_do_h = delivery_time_do_h.substring(1, delivery_time_do_h.length);
                }
                delivery_time_do_h = Number(delivery_time_do_h);
            }

            if( start_point_address.length > 0 && start_point_coords.length == 0 ){
                show_error(this_form, 'start_point_coords', 'Не удалось определить координаты адреса "'+start_point_address+'"', true);
                $(this_form).find('input[name=start_point_address]').val('');
                var destination = $(this_form).find('input[name=start_point_address]').offset();
                $('body, html').animate({scrollTop: destination.top - $('header').outerHeight(true) - $('div#bx-panel').outerHeight(true) - 30}, 400);
            } else if(
                delivery_time_ot.length > 0 && delivery_time_ot != 'empty'
                &&
                delivery_time_do.length > 0 && delivery_time_do != 'empty'
                &&
                delivery_time_ot_h >= delivery_time_do_h
            ){
                show_error(this_form, 'delivery_time_do', 'Время доставки "до" должно быть больше времени "от"', true);
            } else {
                process(true);
                $.post("/ajax/dealerDeliveryPricesSave.php", postParams, function(data){
                    if (data.status == 'ok'){
                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'success', text: 'Успешное сохранение'});
                        if( data.cnt > 0 ){
                            $(button).parents('form').find('span.dealerAccount-delivery__regionNoSettings').hide();
                        } else {
                            $(button).parents('form').find('span.dealerAccount-delivery__regionNoSettings').show();
                        }
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            }
        }
    })

    $(document).on('keyup', '.deliverySettingsForm input[type=text]', function(e){
        //minus_zero( $(this) );
    })

    $(document).on('change', '.distanceTypeInput', function(e){
        setSettingsText($(this))
    })

    $(document).on('change', '.tariffTypeInput', function(e){
        if( $(this).val() == 'distance_ranges' ){
            $(this).parents('form.deliverySettingsForm').find('.distanceRangesBlock').show();
            $(this).parents('form.deliverySettingsForm').find('.pricePerKilometerBlock').hide();
        } else if( $(this).val() == 'price_per_kilometer' ){
            $(this).parents('form.deliverySettingsForm').find('.distanceRangesBlock').hide();
            $(this).parents('form.deliverySettingsForm').find('.pricePerKilometerBlock').show();
        }
        setSettingsText($(this))
    })



    // ЛК ДИЛЕР - сохранение инфы по ценам
    $(document).on('click', '.dealerPricesInfoSaveButton', function(){
        if ( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var error = false;

            var UF_PRICES_YML_LINK = $(this_form).find("input[name=UF_PRICES_YML_LINK]").val();

            // Если нет ошибок
            if (!error){
                process(true);
                var form_data = $(this_form).serialize();
                $.post("/ajax/dealerPricesInfoUpdate.php", {form_data:form_data}, function(data){
                    if (data.status == 'ok'){

                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Успешное сохранение!'});

                    } else if (data.status == 'error'){
                        show_error(this_form, false, data.text);
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 3000, layout: 'topRight', type: 'warning', text: "Ошибка запроса"});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                show_error(this_form, error[0], error[1]);
            }
        }
    });




    // $(document).on('change', '.delivery_time_ot_select', function(e){
    //     alert( $(this).val() )
    // })
    //
    // $(document).on('change', '.delivery_time_do_select', function(e){
    //     alert( $(this).val() )
    // })


})