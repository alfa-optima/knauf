$(document).ready(function(){




    // Авторизация
    $(document).on('click', '.auth___button', function(){
        if (!is_process(this)){
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var login = $(this_form).find("input[name=login]").val();
            var password = $(this_form).find("input[name=password]").val();
            var error = false;
            // Проверки
            if (login.length == 0){
                error = ['login',   'Введите, пожалуйста, Ваш логин!'];
            } else if (password.length == 0){
                error = ['password',   'Введите, пожалуйста, Ваш пароль!'];
            }
            // Если нет ошибок
            if (!error){
                process(true);
                $.post("/ajax/ajax.php", {action:"auth", login:login, password:password}, function(data){
                    if( data.status == 'ok' ){
                        ym(55379437, 'reachGoal', 'sendAuthForm');
                    }
                    if ( data.status == 'ok' || data.status == 'auth' ){
                        var curURL = document.URL;
                        curURL = curURL.replace(new RegExp(location.hash,'g'), "");
                        //window.location.href = curURL;
                        window.location.reload();
                    } else if (data.status == 'error'){
                        show_error(this_form, false, data.text);
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'topRight', type: 'warning', text: "Ошибка запроса"});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                show_error(this_form, error[0], error[1]);
            }
        }
    });




    // Восстановление пароля (ЗАПРОС)
    $(document).on('click', '.password_recovery_button', function(){
        if (!is_process(this)){
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var email = $(this_form).find("input[name=email]").val();
            var error = false;
            // Проверки
            if (email.length == 0){
                error = ['email',   'Введите, пожалуйста, Ваш адрес эл. почты!'];
            } else if (email.length > 0 && !(/^.+@.+\..+$/.test(email))){
                error = ['email',   'Эл. почта содержит ошибки'];
            }
            // Если нет ошибок
            if (!error){
                process(true);
                var form_data = $(this_form).serialize();
                $.post("/ajax/ajax.php", {action:"password_recovery", form_data:form_data}, function(data){
                    if (data.status == 'ok'){
                        $('.password_recovery_form').replaceWith('<p class="success___p">Ссылка для восстановления пароля отправлена на указанный адрес эл. почты</p>');
                    } else if (data.status == 'error'){
                        show_error(this_form, false, data.text);
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'topRight', type: 'warning', text: "Ошибка запроса"});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                show_error(this_form, error[0], error[1]);
            }
        }
    });




    // Восстановление пароля (СОХРАНЕНИЕ)
    $(document).on('click', '.password_update_button', function(){
        if (!is_process(this)){
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var PASSWORD = $(this_form).find("input[name=PASSWORD]").val();
            var CONFIRM_PASSWORD = $(this_form).find("input[name=CONFIRM_PASSWORD]").val();
            var error = false;
            // Проверки
            if (PASSWORD.length == 0) {
                error = ['PASSWORD', 'Введите, пожалуйста, новый пароль!'];
            } else if (PASSWORD.length < 8){
                error = ['PASSWORD',   'Длина пароля - минимум 8 символов'];
            } else if (CONFIRM_PASSWORD.length == 0){
                error = ['CONFIRM_PASSWORD',   'Введите, пожалуйста, повтор пароля!'];
            } else if (PASSWORD != CONFIRM_PASSWORD){
                error = ['CONFIRM_PASSWORD',   'Пароли не совпадают'];
            }
            // Если нет ошибок
            if (!error){
                process(true);
                var form_data = $(this_form).serialize();
                $.post("/ajax/ajax.php", {action:"password_update", form_data:form_data}, function(data){
                    if (data.status == 'ok'){
                        $('.password_update_form').replaceWith('<p class="success___p">Пароль успешно изменён!</p><p class="success___p"><a href="/auth/" style="color: green; text-decoration: underline;">Авторизация</a></p>');
                    } else if (data.status == 'error'){
                        show_error(this_form, false, data.text);
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'topRight', type: 'warning', text: "Ошибка запроса"});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
                // Ошибка
            } else {
                show_error(this_form, error[0], error[1]);
            }
        }
    });




    // ЛК - сохранение личных данных
    $(document).on('click', '.personalDataSaveButton', function(){
        if (!is_process(this)){
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var LAST_NAME = $(this_form).find("input[name=LAST_NAME]").val();
            var NAME = $(this_form).find("input[name=NAME]").val();
            var PERSONAL_PHONE = $(this_form).find("input[name=PERSONAL_PHONE]").val();
            var LOGIN = $(this_form).find("input[name=LOGIN]").val();
            var EMAIL = $(this_form).find("input[name=EMAIL]").val();
            var PERSONAL_GENDER = $(this_form).find("input[name=PERSONAL_GENDER]:checked").val();
            var PERSONAL_BIRTHDAY = $(this_form).find("input[name=PERSONAL_BIRTHDAY]").val();
            var error = false;
            // Проверки
            if (LAST_NAME.length == 0) {
                error = ['LAST_NAME', 'Введите, пожалуйста, Вашу фамилию!'];
            } else if (NAME.length == 0) {
                error = ['NAME',   'Введите, пожалуйста, Ваше имя!'];
            //} else if (PERSONAL_PHONE.length == 0){
                //error = ['PERSONAL_PHONE',   'Введите, пожалуйста, Ваш телефон!'];
            } else if (PERSONAL_PHONE.length > 0 && !(/^ *\+? *?\d+([-\ 0-9]*|(\(\d+\)))*\d+ *$/.test(PERSONAL_PHONE))){
                error = ['PERSONAL_PHONE',   'В номере телефона допускаются только цифры, а также символы "+", "-", пробел и круглые скобки!'];
            } else if ( LOGIN != undefined && LOGIN.length == 0 ){
                error = ['LOGIN',   'Введите, пожалуйста, логин!'];
            } else if (EMAIL != undefined && EMAIL.length == 0){
                error = ['EMAIL',   'Введите, пожалуйста, Ваш адрес эл. почты!'];
            } else if (EMAIL != undefined && EMAIL.length > 0 && !(/^.+@.+\..+$/.test(EMAIL))) {
                error = ['EMAIL', 'Эл. почта содержит ошибки!'];
            } else if( !PERSONAL_GENDER ){
                error = ['PERSONAL_GENDER', 'Укажите, пожалуйста, Ваш пол!'];
            } else if (PERSONAL_BIRTHDAY.length == 0){
                error = ['PERSONAL_BIRTHDAY',   'Укажите, пожалуйста, дату рождения!'];
            }
            // Если нет ошибок
            if (!error){
                process(true);
                var form_data = $(this_form).serialize();
                $.post("/ajax/personalDataUpdate.php", {form_data:form_data}, function(data){
                    if (data.status == 'ok'){

                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Успешное сохранение!'});

                    } else if (data.status == 'error'){
                        show_error(this_form, false, data.text);
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'topRight', type: 'warning', text: "Ошибка запроса"});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                show_error(this_form, error[0], error[1]);
            }
        }
    });



    // ЛК - сохранение пароля
    $(document).on('click', '.personalPasswordSaveButton', function(){
        if (!is_process(this)){
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var CURRENT_PASSWORD = $(this_form).find("input[name=CURRENT_PASSWORD]").val();
            var PASSWORD = $(this_form).find("input[name=PASSWORD]").val();
            var CONFIRM_PASSWORD = $(this_form).find("input[name=CONFIRM_PASSWORD]").val();
            var error = false;
            // Проверки
            if (CURRENT_PASSWORD.length == 0) {
                error = ['CURRENT_PASSWORD', 'Введите, пожалуйста, текущий пароль!'];
            } else if (PASSWORD.length == 0) {
                error = ['PASSWORD',   'Введите, пожалуйста, новый пароль!'];
            } else if (PASSWORD.length < 8) {
                error = ['PASSWORD',   'Минимальная длина пароля - 8 символов!'];
            } else if (CONFIRM_PASSWORD.length == 0) {
                error = ['CONFIRM_PASSWORD',   'Введите, пожалуйста, повтор пароля!'];
            } else if (CONFIRM_PASSWORD != PASSWORD) {
                error = ['CONFIRM_PASSWORD',   'Пароли не совпадают'];
            }
            // Если нет ошибок
            if (!error){
                process(true);
                var form_data = $(this_form).serialize();
                $.post("/ajax/personalPasswordUpdate.php", {form_data:form_data}, function(data){
                    if (data.status == 'ok'){
                        $(this_form).find('input[type=password]').val('');
                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Успешное сохранение!'});
                    } else if (data.status == 'error'){
                        show_error(this_form, false, data.text);
                    }
                }, 'json')
                    .fail(function(data, status, xhr){
                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'topRight', type: 'warning', text: "Ошибка запроса"});
                    })
                    .always(function(data, status, xhr){
                        process(false);
                    })
                // Ошибка
            } else {
                show_error(this_form, error[0], error[1]);
            }
        }
    });


    // ЛК - сохранение настроек по доставке
    $(document).on('click', '.personalDeliverySaveButton', function(){
        if (!is_process(this)){
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var ADDRESS = $(this_form).find("input[name=address]").val();
            var ADDRESS_COORDS = $(this_form).find("input[name=address_coords]").val();
            var error = false;
            // Проверки
            // if (LAST_NAME.length == 0) {
            //     error = ['LAST_NAME', 'Введите, пожалуйста, Вашу фамилию!'];
            // } else if (NAME.length == 0) {
            //     error = ['NAME',   'Введите, пожалуйста, Ваше имя!'];
            // }
            // Если нет ошибок
            if (!error){
                process(true);
                var form_data = $(this_form).serialize();
                $.post("/ajax/personalDeliveryUpdate.php", {form_data:form_data}, function(data){
                    if (data.status == 'ok'){

                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Успешное сохранение!'});

                    } else if (data.status == 'error'){
                        show_error(this_form, false, data.text);
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'topRight', type: 'warning', text: "Ошибка запроса"});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                show_error(this_form, error[0], error[1]);
            }
        }
    });


    // ЛК - сохранение шаблонов доставки
    $(document).on('click', '.deliveryTemplatesSave', function(){
        $.noty.closeAll();
        if (!is_process(this)){
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('.error___p').html('');
            // Задаём переменные
            var errors = {};
            $(this_form).find('input[type=text], input[type=date], input[type=tel]').each(function(){
                var value = $(this).val();
                var input_name = $(this).attr('name');

                var curTemplateID = $(this).parents('.deliveryTemplateItem').find('input.id_item').val();
                if( $(this).hasClass('required') ){
                    if( input_name == 'UF_TEMPLATE['+curTemplateID+']' ){
                        if( value.length == 0 ){
                            errors['UF_TEMPLATE'] = {
                                'element': this,
                                'error_text': 'Не введено название шаблона'
                            }
                        }
                    } else if( input_name == 'CITY_NAME['+curTemplateID+']' ){
                        var locIDinput = $(this).parent('div').find('input[type=hidden]');
                        var locID = $(locIDinput).val();
                        if( locID.length == 0){   $(this).val('');   }
                        if( value.length == 0 || locID.length == 0){
                            errors['UF_CITY'] = {
                                'element': this,
                                'error_text': 'Не выбран город'
                            }
                        }
                    } else if( input_name == 'UF_ADDRESS['+curTemplateID+']' ){
                        if( value.length == 0 ){
                            errors['UF_ADDRESS'] = {
                                'element': this,
                                'error_text': 'Не указан адрес'
                            }
                            $(this).parent('div').find('input[type=hidden]').val('');
                        }
                    } else if( input_name == 'UF_COORDS['+curTemplateID+']' ){
                        if( value.length == 0 ){
                            errors['UF_ADDRESS'] = {
                                'element': $(this).parent('div').find('input[type=text]'),
                                'error_text': 'Не указан адрес'
                            }
                            $(this).parent('div').find('input[type=text]').val('');
                        }
                    /* } else if( input_name == 'UF_STREET['+curTemplateID+']' ){
                        if( value.length == 0 ){
                            errors['UF_STREET'] = {
                                'element': this,
                                'error_text': 'Не введена улица'
                            }
                        }
                    } else if( input_name == 'UF_HOUSE['+curTemplateID+']' ){
                        if( value.length == 0 ){
                            errors['UF_HOUSE'] = {
                                'element': this,
                                'error_text': 'Не введён номер дома'
                            }
                        }*/
                    }
                }
            })
            if( $(this_form).find('input.liftCheckbox:checked').length == 0 ){
                errors['UF_LIFT'] = {
                    'element': false,
                    'error_text': 'В каком-то из шаблонов не указано наличие лифта'
                }
            }
            // Если нет ошибок
            if ( Object.keys( errors ).length == 0 ){
                process(true);
                var templates = $(this_form).serialize();
                $.post("/ajax/deliveryTemplatesSave.php", {templates:templates}, function(data){
                    if (data.status == 'ok'){
                        $('.personalDelivTemplatesBlock').replaceWith(data.html);
                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Успешное сохранение!'});
                    } else if (data.status == 'error'){
                        show_error(this_form, false, data.text);
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'topRight', type: 'warning', text: "Ошибка запроса"});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                var errors_text = [];
                for(key in errors){
                    error = errors[key];
                    errors_text.push(error['error_text']);
                    if( error['element'] ){
                        $(error['element']).addClass('is___error');
                        $(error['element']).attr('oninput','$(this).removeClass(\'is___error\'); $(this).attr("title", "");');
                    }
                }
                $(this_form).find('.error___p').html( errors_text.join("<br>") );
            }
        }
    });




    // ЛК - шаблоны доставки - запрос пустой формы
    $(document).on('click', '.addEmptyDeliveryTemplate', function(){
        if ( !is_process(this)  ) {
            if ($('.emptyDelivBlock').length == 0) {
                process(true);
                $.post("/ajax/emptyDeliveryTemplate.php", {}, function (data) {
                    if (data.status == 'ok') {
                        $('.formsArea').append(data.empty_template_html);
                        var destination = $('.emptyDelivBlock').offset();
                        $('body, html').animate({scrollTop: destination.top - 60}, 300);
                    }
                }, 'json')
                .fail(function (data, status, xhr) {
                    var n = noty({
                        closeWith: ['hover'],
                        timeout: 5000,
                        layout: 'topRight',
                        type: 'warning',
                        text: "Ошибка запроса"
                    });
                })
                .always(function (data, status, xhr) {
                    process(false);
                })
            } else {
                var destination = $('.emptyDelivBlock').offset();
                $('body, html').animate({scrollTop: destination.top - 60}, 300);
            }
        }
    });




    // ЛК - удаление шаблона доставки
    $(document).on('click', '.removeDeliveryTemplate', function(){
        $.noty.closeAll();

        if ( !is_process(this)  ) {
            var id = $(this).parents('.deliveryTemplateItem').find('input.id_item').val();

            if( id == 'new' ) {

                $('.emptyDelivBlock').remove();
                $('.lkAddressesForm .error___p').html('');

            } else {

                var n = noty({
                    text        : 'Действительно удалить шаблон доставки?',
                    type        : 'alert',
                    dismissQueue: true,
                    layout      : 'center',
                    theme       : 'defaultTheme',
                    buttons     : [
                        {
                            addClass: 'btn btn-primary',
                            text: 'Да',
                            onClick: function ($noty){
                                // Запрос
                                $noty.close();
                                process(true);
                                $.post("/ajax/removeDeliveryTemplate.php", {id:id}, function (data) {
                                    if (data.status == 'ok') {
                                        $('.formsArea').html(data.user_templates_html);
                                        //var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Успешное сохранение!'});
                                        var destination = $('.deliveryTemplateItem').eq(0).offset();
                                        $('body, html').animate({scrollTop: destination.top - 60}, 300);
                                    }
                                }, 'json')
                                .fail(function (data, status, xhr) {
                                    var n = noty({
                                        closeWith: ['hover'],
                                        timeout: 5000,
                                        layout: 'topRight',
                                        type: 'warning',
                                        text: "Ошибка запроса"
                                    });
                                })
                                .always(function (data, status, xhr) {
                                    process(false);
                                })
                            }
                        },
                        {
                            addClass: 'btn btn-danger',
                            text: 'Нет',
                            onClick: function ($noty) { $noty.close(); }
                        }
                    ]
                });
            }
        }
    });






    // Подгрузка истории заказов
    $(document).on('click', '.ordersListMoreButton', function(){
        if ( !is_process(this)  ) {
            var params = {stop_ids:[]};
            $('.orderListItem').each(function(){
                var item_id = $(this).attr('item_id');
                params['stop_ids'].push(item_id);
            });
            process(true);

            params['app'] = $("body").data().app;

            $.post("/ajax/ordersListLoad.php", params, function(data){
                if (data.status == 'ok'){
                    $('.personalOrderHistoryArea').append(data.html);
                    if( $('tr.ost').length > 0 ){
                        $('.ordersListMoreButton').show();
                    } else {
                        $('.ordersListMoreButton').hide();
                    }
                    $('tr.ost').remove();
                }
            }, 'json')
            .fail(function(data, status, xhr){})
            .always(function(data, status, xhr){
                process(false);
            })
        }
    });











    // ОТЗЫВ О ЗАКАЗЕ - открытие окна
    $(document).on('click', '.dealerReviewWindowButton', function(){
        var button = $(this);
        if ( !is_process(button)  ) {
            var order_id = $(button).parents('.orderListItem').attr('item_id');
            var postParams = { order_id: order_id };
            process(true);
            $.post("/ajax/personalOrderReviewWindow.php", postParams, function(data){
                if (data.status == 'ok'){

                    $('.orderReviewForm .modal-orderReview__title').html('Отзыв о заказе №'+order_id);
                    $('.orderReviewForm input[name=dealer_id]').val(data.dealer_id);
                    $('.orderReviewForm input[name=order_id]').val(order_id);
                    $('.orderReviewForm input[type=radio], .orderReviewForm input[type=checkbox]').prop('checked', false);
                    $('.orderReviewForm textarea').val('');
                    $('.orderReviewForm li.star').removeClass('selected');
                    $('.orderReviewForm li.star').removeClass('hover');

                    $('.modal-orderReview__link').fancybox({ padding: 0, wrapCSS: 'fancybox-orderReviewWindow' }) .trigger('click');

                } else if (data.status == 'error'){
                    $.noty.closeAll();
                    var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: data.text});
                }
            }, 'json')
            .fail(function(data, status, xhr){
                $.noty.closeAll();
                var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    });




    // ОТЗЫВ О ЗАКАЗЕ - отправка
    $(document).on('click', '.orderReviewButtonSend', function(){
        if ( !is_process(this)  ) {
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');

            // Задаём переменные
            var product_quality = $(this_form).find('input[name=product_quality]:checked').val();
            var shop_quality = $(this_form).find('input[name=shop_quality]:checked').val();
            var delivery_speed = $(this_form).find('input[name=delivery_speed]:checked').val();
            var review = $(this_form).find("textarea[name=review]").val();
            var dealer_vote = $(this_form).find('li.star.selected').length;
            var error = false;
            // Проверки
            if( product_quality == undefined ){
                error = [false,   'Оцените, пожалуйста, качество товара!'];
            } else if( shop_quality == undefined ){
                error = [false,   'Оцените, пожалуйста, качество работы магазина!'];
            } else if( delivery_speed == undefined ){
                error = [false,   'Оцените, пожалуйста, скорость доставки!'];
            } else if( dealer_vote == 0 ){
                error = [false,   'Оцените, пожалуйста, дилера!'];
            } else if (review.length == 0){
                error = ['review',   'Введите, пожалуйста, текст отзыва!'];
            }
            // Если нет ошибок
            if (!error){
                process(true);
                var form_data = $(this_form).serialize();
                $.post("/ajax/reviewOrderSend.php", {
                    form_data:form_data,
                    dealer_vote:dealer_vote
                }, function(data){
                    if (data.status == 'ok'){

                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'success', text: "Отзыв успешно отправлен"});

                        $.fancybox.close();

                    } else if (data.status == 'error'){
                        show_error(this_form, false, data.text);
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса"});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                show_error(this_form, error[0], error[1]);
            }
        }
    });







});