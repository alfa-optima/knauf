<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Page\Asset;   $asset = Asset::getInstance();
\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>


        <? if( !tools\funcs::isMain() ){ ?>

                </div>
            </article>

        <? } ?>

    </main>

    <?/*<footer class="main-footer">
        <div class="main-footer__wrapper">
            <div class="social-buttons">
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox_a64i"></div>
            </div>

            <? // footer_menu
            include $_SERVER['DOCUMENT_ROOT'].'/knauf_app/include/footer_menu.php'?>

            <div class="main-footer__copyright"><span>&copy;&nbsp;<?if( date('Y')==2019){echo(date('Y')); } else { echo("2019 - ".date('Y')); }?>&nbsp;ООО &laquo;КНАУФ ГИПС&raquo; </span></div>

        </div>
    </footer>*/?>



    <? if( $arURI[1] != 'personal' ){
        // subscribe_block
        $APPLICATION->IncludeComponent(
            "aoptima:subscribe_block", "modal"
        );
    } ?>


    <? // city_accept_window
    $APPLICATION->IncludeComponent(
        "aoptima:city_accept_window", "",
        array('KNAUF_LOC' => $KNAUF_LOC)
    ); ?>


    <input type="hidden" name="base_domain" value="<?=\Bitrix\Main\Config\Option::get('main', 'server_name')?>">
    <input type="hidden" name="cur_domain" value="<?=$_SERVER['SERVER_NAME']?>">
    <input type="hidden" name="request_scheme" value="https">
    <input type="hidden" name="curURL" value="<?=$_SERVER['REQUEST_URI']?>">


    <? // _site_modals
    include $_SERVER["DOCUMENT_ROOT"].'/include/_site_modals.php'; ?>

	<?//how it works
	include $_SERVER["DOCUMENT_ROOT"].'/include/howitworks.php'; ?>

    <div class="scroll2top-container">
        <a href="#" class="scroll2top"></a>
    </div>


    <? // JS
    $asset->addJs(SITE_TEMPLATE_PATH."/js/main.js");
    if( tools\funcs::isMain() ){
        $asset->addJs(SITE_TEMPLATE_PATH."/js/sliders.js?v=".time());
    }
    $asset->addJs(SITE_TEMPLATE_PATH."/js/aoptima/scripts_f.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/aoptima/scripts.js");
    //$asset->addJs("//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b218d19931edf6f"); ?>


    <!--<script type='text/javascript'>
        (function () {
            window['yandexChatWidgetCallback'] = function() {
                try {
                    window.yandexChatWidget = new Ya.ChatWidget({
                        guid: '62c64a03-de59-9026-6d8c-9f7d4dd214ca',
                        buttonText: '',
                        title: 'чат',
                        theme: 'light',
                        collapsedDesktop: 'never',
                        collapsedTouch: 'never'
                    });
                } catch(e) { }
            };
            var n = document.getElementsByTagName('script')[0],
                s = document.createElement('script');
            s.async = true;
            s.charset = 'UTF-8';
            s.src = 'https://yastatic.net/s3/chat/widget.js';
            n.parentNode.insertBefore(s, n);
        })();
    </script>-->

</body>
</html>