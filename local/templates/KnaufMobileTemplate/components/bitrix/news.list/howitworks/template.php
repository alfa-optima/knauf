<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div id='modal-howItWorks' class="modal-howItWorks">
	<div class="modal-howItWorks__wrapper">
		<a href="javascript:;" onclick="$.fancybox.close();" class="modal-review__close">Закрыть</a>
		<h2 class="modal-howItWorks__title">КАК РАБОТАТЬ С САЙТОМ</h2>
		<div class="modal-howItWorks__inner">

			<ul class="modal-howItWorks__slider bxslider">

			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				
				<li class="modal-howItWorks__slide">
					<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
					<div class="modal-howItWorks__slide-text">
						<span class="modal-howItWorks__slide-title"><?echo $arItem["NAME"]?></span>
						<p class="modal-howItWorks__slide-par"><?echo $arItem["PREVIEW_TEXT"];?></p>
					</div>
				</li>

			<?endforeach;?>

			</ul>
		</div>
	</div>
</div>
