<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if (!empty($arResult)){
     
    foreach($arResult as $arItem){

        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
            continue;

        if( $arItem['PARAMS']['FROM_IBLOCK'] ){
            continue;
        } ?>

        <li class="main-nav__item 
			<? if ($arItem["TEXT"] == "Каталог") {echo "main-nav__item--dropdown";}?>
			<? if( $arItem["SELECTED"] ){ echo 'main-nav__item--active'; } ?>
		">

            <a href="<?=$arItem["LINK"]?>" class="main-nav__item-link"><?=$arItem["TEXT"]?></a>

            <? $level_2 = array();
            foreach($arResult as $arItem2){
                if(
                    $arItem2['PARAMS']['FROM_IBLOCK']
                    &&
                    $arItem2['PARAMS']['DEPTH_LEVEL'] == 1
                    &&
                    (
                        tools\funcs::string_begins_with($arItem["LINK"], $arItem2["LINK"])
                        ||
                        (
                            $arItem2['PARAMS']['FROM_IBLOCK'] == project\catalog::catIblockID()
                            &&
                            $arItem['LINK'] == '/knauf_app/catalog/'
                        )

                    )
                ){    $level_2[] = $arItem2;    }
            }

            if( count($level_2) > 0 ){ ?>

                <ul class="main-nav__item-dropdown">
				
						<? if ($arItem["TEXT"] == "Каталог"):?>
						
							<li class="main-nav__item-dropdown-item  main-nav__item-dropdown-item--pageLink yesMobile">

								<a href="/knauf_app/catalog/" class="main-nav__item-dropdown-link"><?="Обзор страницы Каталог";?></a>
						
							</li>
						<?endif;?>	

                    <!--<li class="main-nav__item-dropdown-item  main-nav__item-dropdown--span noMobile"><span>Ваш выбор</span></li>-->

                    <? foreach ($level_2 as $level_2_sect){ ?>

                        

                            <? $level_3 = array();
                            foreach($arResult as $arItem3){
                                if(
                                    $arItem3['PARAMS']['FROM_IBLOCK']
                                    &&
                                    $arItem3['PARAMS']['DEPTH_LEVEL'] == 2
                                    &&
                                    tools\funcs::string_begins_with($level_2_sect["LINK"], $arItem3["LINK"])
                                ){    $level_3[] = $arItem3;    }
                            }

                            if( count($level_3) > 0 ){ ?>
							
							<li class="main-nav__item-dropdown-item  main-nav__item-dropdown-item--dropright">

								<a href="<?=$level_2_sect['LINK']?>" class="main-nav__item-dropdown-link"><?=$level_2_sect['TEXT']?></a>
							
                                <ul class="main-nav__item-dropdown2">

                                    <? foreach ($level_3 as $level_3_sect){ ?>

                                        <li class="main-nav__item-dropdown2-item">
                                            <a href="<?=$level_3_sect['LINK']?>" class="main-nav__item-dropdown2-link"><?=$level_3_sect['TEXT']?></a>
                                        </li>

                                    <? } ?>

                                </ul>

                            <? } else {?>
							
							<li class="main-nav__item-dropdown-item  main-nav__item-dropdown-item--pageLink">

								<a href="<?=$level_2_sect['LINK']?>" class="main-nav__item-dropdown-link"><?=$level_2_sect['TEXT']?></a>
							
							<?}?>
							
							

                        </li>

                    <? } ?>

                </ul>

            <? } ?>

        </li>

    <? }

} ?>