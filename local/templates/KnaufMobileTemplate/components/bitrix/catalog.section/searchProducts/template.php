<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$maxCnt = $arParams['PAGE_ELEMENT_COUNT']-1;

if( $arParams['IS_LOAD'] != 'Y' ){

    if (count($arResult['ITEMS']) > 0){ ?>

        <ul class="searchBlock__items">

            <? $cnt = 0;
            foreach ($arResult['ITEMS'] as $key => $arItem){ $cnt++;
                if( $cnt <= $maxCnt ){
                    // catalog_item
                    $APPLICATION->IncludeComponent(
                        "aoptima:catalog_item", "catalog",
                        array(
                            "LOC" => $arParams['LOC'],
                            'arItem' => $arItem,
                            'IS_DEALER' => $arParams['IS_DEALER'],
                            'IS_MOB_APP' => $arParams['IS_MOB_APP']
                        )
                    );
                }
            } ?>

        </ul>

        <div class="more-button searchMoreButton to___process" <?  if( $cnt <= $maxCnt ){?>style="display:none"<? } ?>>
            <span>Показать еще</span>
        </div>

    <? } else { ?>

        <hr style="border: none; color: #dedede; background-color: #dedede; height: 1px; margin-bottom: 27px;">

        <p class="lk___no_count">Товаров не найдено</p>

    <? }


} else if( $arParams['IS_LOAD'] == 'Y' ){

    $cnt = 0;
    foreach ($arResult['ITEMS'] as $key => $arItem){ $cnt++;
        if( $cnt <= $maxCnt ){
            // catalog_item
            $APPLICATION->IncludeComponent(
                "aoptima:catalog_item", "catalog",
                array(
                    "LOC" => $arParams['LOC'],
                    'arItem' => $arItem,
                    'IS_DEALER' => $arParams['IS_DEALER'],
                    'IS_MOB_APP' => $arParams['IS_MOB_APP']
                )
            );
        } else {
            echo '<ost></ost>';
        }
    }

} ?>
