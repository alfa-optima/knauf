<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (count($arResult['ITEMS']) > 0){ ?>
    <div class="slider  slider--products">
        <? foreach ($arResult['ITEMS'] as $key => $arItem){

            // catalog_item
            $APPLICATION->IncludeComponent(
                "aoptima:catalog_item", "catalog_slider",
                array(
                    "LOC" => $arParams['LOC'],
                    'arItem' => $arItem,
                    'IS_DEALER' => $arParams['IS_DEALER']
                )
            );

        } ?>
    </div>
<? } ?>