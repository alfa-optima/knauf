<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (count($arResult['ITEMS']) > 0){ ?>

    <div class="slider stop catalog_slider_<?=$arParams['SECT_ID']?>">

        <? foreach ($arResult['ITEMS'] as $key => $arItem){

            // catalog_item
            $APPLICATION->IncludeComponent(
                "aoptima:catalog_item", "catalog_slider",
                array(
                    "LOC" => $arParams['LOC'],
                    'arItem' => $arItem,
                    'IS_DEALER' => $arParams['IS_DEALER']
                )
            );

        } ?>

    </div>
	
<? } ?>