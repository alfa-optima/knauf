<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$this->setFrameMode(true);

if (count($arResult["SECTIONS"]) > 0){ ?>

    <div class="sliderBlock  sliderBlock--materials">
        <div class="sliderBlock__wrapper">
            <div class="slider  slider--materials">

                <? foreach ($arResult["SECTIONS"] as $arItem){ ?>

                    <div class="slider__item">
                        <div class="slider__item-image">
                            <a href="<?=$arItem['SECTION_PAGE_URL']?>">
                                <? if( intval($arItem['PICTURE']) > 0 ){ ?>
                                    <img src="<?=tools\funcs::rIMGG($arItem['PICTURE'], 5, 202, 151)?>" alt="<?=$arItem['NAME']?>">
                                <? } else { ?>
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/no_image.png" alt="<?=$arItem['NAME']?>">
                                <? } ?>
                            </a>
                        </div>

                        <div class="slider__item-inner">
                            <h3 class="slider__item-title"><?=$arItem['NAME']?></h3>
                            <a href="<?=$arItem['SECTION_PAGE_URL']?>" class="slider__item-link">Подробнее</a>
                        </div>

                    </div>

                <? } ?>

            </div>
        </div>
    </div>

<? } ?>