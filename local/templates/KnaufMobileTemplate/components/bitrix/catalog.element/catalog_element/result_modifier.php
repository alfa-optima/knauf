<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult = project\catalog::updateProductName($arResult);


//////////////////////////////////////////////////////
if( is_array( $arParams['ALL_DEALERS'] ) ){
    foreach ( $arParams['ALL_DEALERS'] as $dealer ){
        if( !$dealer['UF_SHOW_IN_CATALOG'] ){
            foreach ( $arResult['OFFERS'] as $k1 => $offer ){
                if( $offer['PROPERTIES']['DEALER']['VALUE'] == $dealer['ID'] ){
                    unset( $arResult['OFFERS'][$k1] );
                }
            }
        }
    }
}
//////////////////////////////////////////////////////


foreach ($arResult['OFFERS'] as $key => $offer){
    if(
        intval($arParams['LOC']['REGION_ID']) > 0
        &&
        $arParams['LOC']['REGION_ID'] != $offer['PROPERTIES']['LOCATION']['VALUE']
    ){   unset($arResult['OFFERS'][$key]);   }
}

if( is_array($arResult['OFFERS']) ){

    foreach ($arResult['OFFERS'] as $offer) {
        if( $offer['PRICES']['BASE']['ROUND_VALUE_VAT'] ){
            if(
                !$arResult['PRICE_MIN']
                ||
                (
                    $arResult['PRICE_MIN']
                    &&
                    $offer['PRICES']['BASE']['ROUND_VALUE_VAT'] <  $arResult['PRICE_MIN']
                )
            ){   $arResult['PRICE_MIN'] = $offer['PRICES']['BASE']['ROUND_VALUE_VAT'];  }
            if(
                !$arResult['PRICE_MAX']
                ||
                (
                    $arResult['PRICE_MAX']
                    &&
                    $offer['PRICES']['BASE']['ROUND_VALUE_VAT'] >  $arResult['PRICE_MAX']
                )
            ){   $arResult['PRICE_MAX'] = $offer['PRICES']['BASE']['ROUND_VALUE_VAT'];  }
        }
    }
}


$arResult['PICTURES'] = array();
if( intval($arResult['DETAIL_PICTURE']['ID']) > 0 ){
    $arResult['PICTURES'][] = $arResult['DETAIL_PICTURE']['ID'];
}
if(
    is_array($arResult['PROPERTIES']['MORE_PHOTOS']['VALUE'])
    &&
    count($arResult['PROPERTIES']['MORE_PHOTOS']['VALUE']) > 0
){
    foreach ($arResult['PROPERTIES']['MORE_PHOTOS']['VALUE'] as $photo_id){
        if( intval($photo_id) > 0 ){
            $arResult['PICTURES'][] = $photo_id;
        }
    }
}
?>