<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
die(); 

$classes = array(
	'VKontakte' => 'icon_vk',
	'Facebook' => 'icon_fb',
); 

foreach($arParams["~AUTH_SERVICES"] as $service_code => $service){
	if( !$classes[$service_code] ){
		unset($arParams["~AUTH_SERVICES"][$service_code]);
	}
} ?>


<div class="modal__socials">

	<span class="socials__title">Войти на сайт через соцсети:</span>

	<ul class="socials">

		<? foreach($arParams["~AUTH_SERVICES"] as $service_code => $service){ ?>
		
			<li class="socials__item">
				<a class="<?=$classes[$service_code]?>" href="javascript:void(0)" onclick="<?=$service['ONCLICK']?>"></a>
			</li>
			
		<? } ?>
		
	</ul>

</div>
