$(document).ready(function() {

    if ($('.product-gallery').length) {

        $(".product-gallery__slide").fancybox({
            wrapCSS: 'product-gal',

            helpers: {
                buttons: {},
                thumbs: {
                    width: 11,
                    height: 10,
                    source: function() {
                        return "";
                    }
                }
            }


        });

        $.fancybox.helpers.thumbs.onUpdate = function() {
            var thumbsWidth = $("#fancybox-thumbs ul").width();
            $(".product-gal .fancybox-prev").css({ 'marginLeft': (-1 * thumbsWidth / 2) + 'px' });
            $(".product-gal .fancybox-next").css({ 'marginRight': (-1 * thumbsWidth / 2) + 'px' });
            return false;
        };
    }

    if ($('.bcgSliderBlock__slider').length && $('.bcgSliderBlock__slider-item').length > 1) {
        $('.bcgSliderBlock__slider').bxSlider({
            auto: true,
            stopAutoOnClick: true,
            pager: true,
        });
    }

    if ($('.howItWorks').length) {
        $('.howItWorks .bxslider').bxSlider({
            infiniteLoop: false,
            mode: 'fade',
            speed: 0
        });

        $('.bx-controls').each(function() {
            var t = $(this);
            var summ = 0;
            $('.bx-pager-item', t).each(function() {
                summ += $(this).width();
            });

            $('.bx-controls-direction', t).width(summ);

        });
    }

    var config = {
        slideWidth: 204,
        minSlides: 1,
        maxSlides: 4,
        moveSlides: 4,
        infiniteLoop: false,
        oneToOneTouch: false,
        controls: true,
        touchEnabled: false,
        slideMargin: 26,
        onSliderLoad: function(){
            $(this).addClass('slider--visible');
        }
    };

    var windowWidth = $(window).width()
    if (windowWidth < 570) {
        config.minSlides = 2;
        config.slideWidth = 122;
        config.maxSlides = 2;
    } else {
        config.minSlides = 1;
        config.maxSlides = 4;
    }

    if (windowWidth < 1000) {
        config.touchEnabled = true;
    } else {
        config.touchEnabled = false;
    }


    var sliders = new Array();
    $('.slider').each(function(i, slider) {
        if ($(slider).hasClass('slider--spotlight')) {
            config.maxSlides = 2;
            config.moveSlides = 2;
            config.slideWidth = 434;
            sliders[i] = $(slider).bxSlider(config);

            if ($(slider).children('.slider__item').length < 5) {
                sliders[i].parents('.bx-wrapper').addClass('noSlider');

                if ($(slider).children('.slider__item').length >= 3) {
                    sliders[i].parents('.bx-wrapper').addClass('noSlider--moreThreeSlides');
                }
            }

        } else if ($(slider).hasClass('product-gallery__thumb')) {
            config.infiniteLoop = false;
            config.moveSlides = 2;
            sliders[i] = $(slider).bxSlider(config);
            if ($(slider).children('.slider__item').length < 5) {
                sliders[i].parents('.bx-wrapper').addClass('noSlider');

                if ($(slider).children('.slider__item').length >= 3) {
                    sliders[i].parents('.bx-wrapper').addClass('noSlider--moreThreeSlides');
                }
            }
        } else {
            config.maxSlides = 4;
            config.moveSlides = 4;
            config.slideWidth = 204;
            sliders[i] = $(slider).bxSlider(config);

            if ($(slider).children('.slider__item').length < 5) {
                sliders[i].parents('.bx-wrapper').addClass('noSlider');

                if ($(slider).children('.slider__item').length >= 3) {
                    sliders[i].parents('.bx-wrapper').addClass('noSlider--moreThreeSlides');
                }
            }
        }

    });


    // Высчитываем ширину между стрелками в зависимости от количества дотсов
    $('.bx-controls').each(function() {
        var t = $(this);
        var summ = 0;
        $('.bx-pager-item', t).each(function() {
            summ += $(this).width();
        });

        $('.bx-controls-direction', t).width(summ);

    });


    //При клике на таб

    $(document).on('click', '.tabs li', function() {

        if (!$(this).parents('.tabs').hasClass('stop')) {

            //Сначала показываем блок с нужным слайдером
            var click_id = $(this).attr('id');
            if (click_id != $('.tabs li.active').attr('id')) {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');
                $(this).parents('.sliderBlock__tabs').next('.sliderBlock__sliders').children('.slider__wrapper').removeClass('active');
                $(this).parents('.sliderBlock').next('.block--sliders').children('.block__wrapper').removeClass('active');
                $('#slide_' + click_id).addClass('active');
                $('#instruction_' + click_id).addClass('active');
            }

            //Потом реинициализируем слайдеры
            $.each(sliders, function(i, slider) {
                slider.reloadSlider(config);
                if ($(slider).children('.slider__item').length < 5) {
                    sliders[i].parents('.bx-wrapper').addClass('noSlider');

                    if ($(slider).children('.slider__item').length >= 3) {
                        sliders[i].parents('.bx-wrapper').addClass('noSlider--moreThreeSlides');
                    }
                }


            });

            //И высчитываем ширину между стрелками в зависимости от количества дотсов
            $('.bx-controls').each(function() {
                var t = $(this);
                var summ = 0;
                $('.bx-pager-item', t).each(function() {
                    summ += $(this).width();
                });

                $('.bx-controls-direction', t).width(summ);

            });

        }

    });


    $(window).resize(function() {

        var windowWidth = $(window).width()
        if (windowWidth < 570) {
            config.minSlides = 2;
            config.maxSlides = 2;
        } else {
            config.minSlides = 1;
            config.maxSlides = 4;
        }

        if (windowWidth < 1000) {
            config.touchEnabled = true;
        } else {
            config.touchEnabled = false;
        }

        //Реинициализируем слайдеры при ресайзе
        $.each(sliders, function(i, slider) {
            if ($(slider).hasClass('slider--spotlight')) {
                config.maxSlides = 2;
                config.moveSlides = 2;
                config.slideWidth = 434;
                slider.reloadSlider(config);
            } else {
                config.maxSlides = 4;
                config.moveSlides = 4;
                config.slideWidth = 204;
                slider.reloadSlider(config);
                if ($(slider).children('.slider__item').length < 5) {
                    sliders[i].parents('.bx-wrapper').addClass('noSlider');
                    if ($(slider).children('.slider__item').length >= 3) {
                        sliders[i].parents('.bx-wrapper').addClass('noSlider--moreThreeSlides');
                    }
                }
            }
        });

        //Высчитываем ширину между стрелками в зависимости от количества дотсов

        $('.bx-controls').each(function() {
            var t = $(this);
            var summ = 0;
            $('.bx-pager-item', t).each(function() {
                summ += $(this).width();
            });

            $('.bx-controls-direction', t).width(summ);

        });


    });

});


/*if ($('.cart-offer__best-items').length || $('.cart-offer__partial-items').length) {
    $(window).on("resize", function(e) {
        checkScreenSize();
    });

    checkScreenSize();

    function checkScreenSize() {

        var newWindowWidth = $(window).width();

        if ($(".cart-offer__best-items").length > 0) {
            sliderBestItems = $(".cart-offer__best-items").bxSlider({
                minSlides: 1,
                maxSlides: 1,
                infiniteLoop: false,
                moveSlides: 1,
                adaptiveHeight: true
            });
            if (newWindowWidth >= 1000) {
                sliderBestItems.destroySlider();
            }
        }

        if ($(".cart-offer__partial-items").length > 0) {
            sliderPartialItems = $(".cart-offer__partial-items").bxSlider({
                minSlides: 1,
                maxSlides: 1,
                infiniteLoop: false,
                moveSlides: 1,
                adaptiveHeight: true
            });
            if (newWindowWidth >= 1000) {
                sliderPartialItems.destroySlider();
            }
        }

    }

}*/


