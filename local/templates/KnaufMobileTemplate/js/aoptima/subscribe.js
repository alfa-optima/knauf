$(document).ready(function(){



    // Подписка на рассылку
    $(document).on('click', '.subscribeButton', function(){
        var button = $(this);
        if (!is_process(this)){
            // Форма
            var this_form = $(button).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var email = $(this_form).find("input[name=email]").val();
            var all_rub_cnt = $(this_form).find('input.rubric_checkbox').length;
            var rub_cnt = $(this_form).find('input.rubric_checkbox:checked').length;
            var error = false;
            // Проверки
            if (
                (
                    // rub_cnt > 0
                    // ||
                    user['IS_AUTH'] != 'Y'
                )
                &&
                email.length == 0
             ){
                error = ['email',   'Введите, пожалуйста, эл. почту для подписки'];
            } else if (
                email.length > 0
                &&
                !(/^.+@.+\..+$/.test(email))
            ){
                error = ['email',   'Эл. почта содержит ошибки'];
           // } else if (
           //      all_rub_cnt > 0
           //      &&
           //      rub_cnt == 0
           //      &&
           //      user['IS_AUTH'] != 'Y'
           //  ){
           //     error = [false,   'Выберите, пожалуйста, минимум одну рубрику'];
            } else if(
                $(this_form).find('input[name=subscribeYes]').length > 0
                &&
                $(this_form).find('input[name=subscribeYes]:checked').length == 0
                // &&
                // rub_cnt > 0
            ){
                error = [false,   'Отметьте, пожалуйста, согласие с условиями подписки'];
            }
            // Если нет ошибок
            if (!error){
                process(true);
                var form_data = $(this_form).serialize();
                $.post("/ajax/subscribe.php", {form_data:form_data}, function(data){
                    if (data.status == 'ok'){
                        var n = noty({closeWith: ['click'], timeout: 500000000, layout: 'center', type: 'success', text: "Успешное сохранение!"});
                        if( $(button).hasClass('isModalButton') ){
                            $('#subscribeToggle').trigger('click');
                        }
                    } else if (data.status == 'error'){
                        show_error(this_form, false, data.text);
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса"});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                show_error(this_form, error[0], error[1]);
            }
        }
    });




    $(document).on('click', ".account-form__selectAll", function() {
        $('.rubric_checkbox').attr("checked", "checked");
        var this_form = $(this).parents('form');
        $(this_form).find("input, textarea").removeClass("is___error");
        $(this_form).find('p.error___p').html('');
    });

    $(document).on('click', ".account-form__cancelAll", function() {
        $('.rubric_checkbox').removeAttr("checked");
        var this_form = $(this).parents('form');
        $(this_form).find("input, textarea").removeClass("is___error");
        $(this_form).find('p.error___p').html('');
    });




})