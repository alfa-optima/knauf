var cityKeyUpInterval;
var cityInputInterval;
var orderAddressInterval;
var addressKeyUpInterval;


function isFunction(functionToCheck)  {
	var getType = {};
	return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
}


function process(attr){
	if (attr == true){
		$('.to___process').addClass('is___process');
	} else if(attr == false){
		$('.to___process').removeClass('is___process');
	}
}
function is_process(el){
	if (el){
		return $(el).hasClass('is___process');
	} else {
		return false;
	}
}
function show_error(this_form, field_name, error, show_noty, scroll){
	$(this_form).find("p.error___p").html(error);
	$(this_form).find('.success___p').html('');
	if (this_form && field_name){
		var title = error.replace(/<[^>]+>/g, '');
		var field_type = $(this_form).find('[name='+field_name+']')[0].tagName;
		$(this_form).find(field_type+'[name='+field_name+']').addClass('is___error').attr( (field_type == 'select'?'onchange':'oninput'),'$(this).removeClass(\'is___error\'); $(this).attr("title", ""); $(this).parents("form").find("p.error___p").html("")').attr('title', title);
		if (scroll){
			if ( $(this_form).find(field_type+'[name='+field_name+']').length > 0 ){
				var destination = $(this_form).find(field_type+'[name='+field_name+']').offset();
				$('body, html').animate({scrollTop: destination.top - 30}, 400);
			}
		}
	}
	if (show_noty){
		$.noty.closeAll();
		var n = noty({closeWith: ['hover'], timeout: 6000, layout: 'center', type: 'warning', text: error});
	}
}
function show_success(this_form, message, show_noty){
	if (this_form){
		$(this_form).find("p.error___p").html('');
		$(this_form).find('.success___p').html(message);
	}
	if (show_noty){
		var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'success', text: message});
	}
}


function create_form(form_class, method, action, target_blank){
	var methodAttr = '';   
	var actionAttr = '';   
	var targetAttr = '';
	if ( method && method.length > 0 ){   methodAttr = ' method="'+method+'"';   }
	if ( action && action.length > 0 ){   actionAttr = ' action="'+action+'"';   }
	if( target_blank ){   targetAttr = ' target="_blank"';   }
	if ( $(form_class).length > 0 ){
		$('.'+form_class).html('');
	} else {
		$('body').append('<form class="'+form_class+'" '+methodAttr+' '+actionAttr+' '+targetAttr+'></form>');
	}
	return $('.'+form_class);
}

function get_pureURL(URL){
	if ( !URL ){   URL = document.URL;   }
	var arURI = URL.split("?");
	var pureURI = arURI[0];
	return pureURI;
}


function addToRequestURI(URL, pm, val){
	var arURI = URL.split("?");
	var pureURI = arURI[0];
	if (URL.indexOf("?") != -1){
		var arRequests = arURI[1].split("&");
		var arNewRequests = [];
		var cnt = -1;
		$.each(arRequests, function(key, request){
			arTMP = request.split("=");
			if (arTMP[0] != pm && request.length > 0){  cnt++;
				arNewRequests[cnt] = request;
			}
		})
		cnt++;
		arNewRequests[cnt] = pm+"="+val;
		return pureURI+"?"+arNewRequests.join("&");
	} else {
		return pureURI+"?"+pm+"="+val;
	}
}


function GET(){
	var URL = document.URL;
	var pureURL = get_pureURL();
	var GET_string = URL.replace(pureURL, '');
	var GET_string = GET_string.replace('?', '');
	var get = {};
	if( GET_string.length > 0 ){
		var params = GET_string.split('&');
		$.each(params, function(k, param){
			var ar = param.split('=');
			var key = ar[0];
			var value = ar[1];
			get[key] = value;
		})
	}
	var arCnt = Object.keys( get ).length;
	return arCnt>0?get:false;
}


function rawurldecode( str ) {  
    var histogram = {};  
    var ret = str.toString();   
    var replacer = function(search, replace, str) {  
        var tmp_arr = [];  
        tmp_arr = str.split(search);  
        return tmp_arr.join(replace);  
    };  
    histogram["'"]   = '%27';  
    histogram['(']   = '%28';  
    histogram[')']   = '%29';  
    histogram['*']   = '%2A';  
    histogram['~']   = '%7E';  
    histogram['!']   = '%21';  
    for (replace in histogram) {  
        search = histogram[replace];
        ret = replacer(search, replace, ret)
    }  
    ret = decodeURIComponent(ret);  
    return ret;  
}



// Функция number_format для форматирования чисел
// Формат: number_format(1234.56, 2, ',', ' ');
function number_format(number, decimals, dec_point, thousands_sep) {
  number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k)
        .toFixed(prec);
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
    .split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1)
      .join('0');
  }
  return s.join(dec);
}



function pfCnt(n, form1, form2, form5){ // pfCnt(productCount, "товар", "товара", "товаров")
    var n = n % 100;
    var n1 = n % 10;
    if (n > 10 && n < 20) return form5;
    if (n1 > 1 && n1 < 5) return form2;
    if (n1 == 1) return form1;
    return form5;
}





function minus_zero(this_input, not_empty_value) {
	var input_value = $(this_input).val();
	input_value = input_value.toString().replace(/[^\d;]/g, '');
	$(this_input).val(input_value);
	var dlina = input_value.length;
	if (dlina > 0){
		stop = false;
		for (var n = 1; n <= dlina; n++) {
			if (stop == false){
				if (input_value.substring((n-1),n) == 0){
					var new_str = input_value.substring(n,dlina);
					if (not_empty_value){
						$(this_input).val( (new_str.length == 0)?1:new_str );
					} else {
						$(this_input).val( (new_str.length == 0)?'':new_str );
					}
				} else {
					stop = true;
				}
			}
		}
	} else if (not_empty_value){
		$(this_input).val(1);
	}
}





var Base64 = {
   _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
   //метод для кодировки в base64 на javascript
  encode : function (input) {
    var output = "";
    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    var i = 0
    input = Base64._utf8_encode(input);
       while (i < input.length) {
       chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);
       enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;
       if( isNaN(chr2) ) {
         enc3 = enc4 = 64;
      }else if( isNaN(chr3) ){
        enc4 = 64;
      }
       output = output +
      this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
      this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
     }
    return output;
  },
   //метод для раскодировки из base64
  decode : function (input) {
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;
     input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
     while (i < input.length) {
       enc1 = this._keyStr.indexOf(input.charAt(i++));
      enc2 = this._keyStr.indexOf(input.charAt(i++));
      enc3 = this._keyStr.indexOf(input.charAt(i++));
      enc4 = this._keyStr.indexOf(input.charAt(i++));
       chr1 = (enc1 << 2) | (enc2 >> 4);
      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
      chr3 = ((enc3 & 3) << 6) | enc4;
       output = output + String.fromCharCode(chr1);
       if( enc3 != 64 ){
        output = output + String.fromCharCode(chr2);
      }
      if( enc4 != 64 ) {
        output = output + String.fromCharCode(chr3);
      }
   }
   output = Base64._utf8_decode(output);
     return output;
   },
   // метод для кодировки в utf8
  _utf8_encode : function (string) {
    string = string.replace(/\r\n/g,"\n");
    var utftext = "";
    for (var n = 0; n < string.length; n++) {
      var c = string.charCodeAt(n);
       if( c < 128 ){
        utftext += String.fromCharCode(c);
      }else if( (c > 127) && (c < 2048) ){
        utftext += String.fromCharCode((c >> 6) | 192);
        utftext += String.fromCharCode((c & 63) | 128);
      }else {
        utftext += String.fromCharCode((c >> 12) | 224);
        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
        utftext += String.fromCharCode((c & 63) | 128);
      }
     }
    return utftext;
 
  },
  //метод для раскодировки из urf8
  _utf8_decode : function (utftext) {
    var string = "";
    var i = 0;
    var c = c1 = c2 = 0;
    while( i < utftext.length ){
      c = utftext.charCodeAt(i);
       if (c < 128) {
        string += String.fromCharCode(c);
        i++;
      }else if( (c > 191) && (c < 224) ) {
        c2 = utftext.charCodeAt(i+1);
        string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
        i += 2;
      }else {
        c2 = utftext.charCodeAt(i+1);
        c3 = utftext.charCodeAt(i+2);
        string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
        i += 3;
      }
     }
     return string;
  }
}






function panel_regulation(wind_top){
	var panel_html = $('#panel').html();
	if (panel_html.length > 0){
		$('#bx-panel-expander').attr('onmouseout', 'panel_regulation();');
		$('#bx-panel-hider').attr('onmouseout', 'panel_regulation();');
		var panel_height = $('#panel').outerHeight(true);
		if (panel_height > 0){
			if (!wind_top || wind_top < 10){
				$('header').css('margin-top', panel_height+'px');
			} else {
				$('header').css('margin-top', '0');
			}
		}
	}
}



function in_array(value, array) 
{
    for(var i = 0; i < array.length; i++) 
    {
        if(array[i] == value) return true;
    }
    return false;
}



function getPaymentButton(order_id){
	$.post("/ajax/getPaymentButton.php", {order_id:order_id}, function(data){
		if( data ){
			if ( $('.hidden_pay_block').length == 0 ){
				$('body').append('<div class="hidden_pay_block" style="display:none"></div>');
			}
			$('.hidden_pay_block').html(data);
			if ( $('.hidden_pay_block form').length > 0 ){
				var form = $('.hidden_pay_block form');
				$('.pay___block').html( $(form).get(0).outerHTML );
				$('.pay___block form').html('');
				$('.pay___block form').attr('target', 'blank')
				$(form).find('input').each(function(){
					var input_html = $(this).get(0).outerHTML;
					$('.pay___block form').append(input_html);
				})
				$('.pay___block input[type=submit]').addClass('pay___button').addClass('best__pay');
				$('.pay___block input[type=submit]').val('Оплатить ('+number_format($(form).find('input[name=Sum]').val(), 0, ',', ' ')+' руб.)')
				$('.hidden_pay_block').remove();
			}
		}
	})
}





function calcReviewsButton(){
	if(
		$('.reviewItem').length > 0
		&&
		$('input[name=total_reviews_cnt]').length > 0
	){
		var total_reviews_cnt = Number($('input[name=total_reviews_cnt]').val());
		var ost_cnt = total_reviews_cnt - $('.reviewItem').length;
		$('.reviewsMoreButton span').html('Ещё осталось ('+ost_cnt+')');
		if( ost_cnt > 0 ){
			$('.reviewsMoreButton').show();
		} else {
			$('.reviewsMoreButton').hide();
		}
	}
}



// Выбор шаблона доставки в корзине
function deliveryTemplateChange( select ){
	var template_id = $(select).val();
	if( template_id == 'empty' ){
		$('.deliveryBlock input[type=text]').val('');
		$('.deliveryBlock textarea').val('');
		$('.deliveryBlock input[type=radio]').prop('checked', false);
		$('.deliveryBlock input[type=checkbox]').prop('checked', false);
		reloadOrder();
	} else {
		process(true);
		var params = { action:"deliveryTemplateInfo", template_id:template_id };
		$.post("/ajax/ajax.php", params, function(data){
			if (data.status == 'ok'){
				// Вставляем инфу в форму
				if(
					"LOC" in data.template
					&&
					$('.deliveryBlock input[name=loc_id]').length > 0
				){
					var cityName = data.template.LOC.NAME_RU+' ('+data.template.LOC.PARENT_NAME_RU+')';
					$('.deliveryBlock input[name=city]').val(cityName);
					$('.deliveryBlock input[name=loc_id]').val(data.template.LOC.ID);
				}
				$('.deliveryBlock input[name=address]').val(data.template.UF_ADDRESS);
				$('.deliveryBlock input[name=coords]').val(data.template.UF_COORDS);
				//$('.deliveryBlock input[name=street]').val(data.template.UF_STREET);
				//$('.deliveryBlock input[name=house]').val(data.template.UF_HOUSE);
				$('.deliveryBlock input[name=podyezd]').val(data.template.UF_PODYEZD);
				$('.deliveryBlock input[name=floor]').val(data.template.UF_FLOOR);
				$('.deliveryBlock input[name=kvartira]').val(data.template.UF_KVARTIRA);
				$('.deliveryBlock textarea[name=comment]').val(data.template.UF_COMMENT);
				$('.deliveryBlock input[name=lift]').prop('checked', false);
				$('.deliveryBlock input[lift_enum_id='+(data.template.UF_LIFT)+']').prop('checked', true);

				reloadOrder();
			}
		}, 'json')
		.fail(function(data, status, xhr){
			var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса"});
		})
		.always(function(data, status, xhr){
			process(false);
		})
	}
}



function reloadDeliveryTemplates(){
	if(
		$('select[name=deliveryTemplate]').length > 0
		&&
		$('select[name=deliveryTemplate] option[value=empty]:checked').length == 0
		&&
		reloadDeliveryTemplatesProcess != 'Y'
	){
		reloadDeliveryTemplatesProcess = 'Y';
		clearInterval(cityInputInterval);
		cityInputInterval = setInterval(function () {
			clearInterval(cityInputInterval);
			var postParams = {};
			process(true);
			$.post("/ajax/getDeliveryTemplates.php", postParams, function(data){
				if (data.status == 'ok'){
					$('div.styledSelect.deliveryTemplatesSelect').html(data.html);
					styleForms();
				}
			}, 'json')
			.fail(function(data, status, xhr){
				//var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
			})
			.always(function(data, status, xhr){
				process(false);
				reloadDeliveryTemplatesProcess = 'N';
			})
		}, 700);
	}
}





function addressFocus( input ){
	/*var addressName = $(input).val();
	var coordsInput = $(input).parent('div').find('input[type=hidden]');
	var coords = $(coordsInput).val();
	if( coords.length > 0 ){
		$(input).attr('addressName', addressName);
	} else {
		$(input).attr('addressName', '');
	}
	$(input).val('');*/
}
function addressKeyUp( input, id, isBasket ){
	$(input).removeClass('is___error');
	var coordsInput = $(input).parent('div').find('input[type=hidden]');
	$(input).attr('addressName', '');
	$(coordsInput).val('');
	$('#'+(id?id:'orderAddress')).autocomplete("close");
	if( !isBasket ){
		reloadDeliveryTemplates();
	} /*else {
		clearInterval(addressKeyUpInterval);
		addressKeyUpInterval = setInterval(function(){
			clearInterval(addressKeyUpInterval);
			reloadBasket();
		}, 700);
	}*/
}
$(document).on('keyup', '#orderAddress', function(e){
	var input = $(this);
	if( $(input)[0].hasAttribute('onkeyup') ){} else {
		if( e.keyCode != 38 && e.keyCode != 40 ){
			$(input).removeClass('is___error');
			var coordsInput = $(input).parent('div').find('input[type=hidden]');
			$(input).attr('addressName', '');
			$(coordsInput).val('');
			$('#orderAddress').autocomplete("close");
			reloadDeliveryTemplates();
		}
	}
})
function addressFocusOut( input, lastCoords ){
	/*if( !$(input)[0].hasAttribute('process') ){   $(input).attr('process', 'N');   }
	if( $(input).attr('process') == 'N' ) {
		var val = $(input).val();
		var coordsInput = $(input).parent('div').find('input[type=hidden]');
		var coords = $(coordsInput).val();
		var addressName = $(input).attr('addressName');
		if (val.length == 0 && addressName.length > 0 && coords.length > 0) {
			$(input).val(addressName);
		}
		$(input).attr('addressName', '');
		if ( lastCoords != coords ) {
			///////////////////////////////////////////////
			if (
				$(input)[0].hasAttribute('name')
				&&
				in_array($(input).attr('name'), ['basketAddress'])
			){
				reloadBasket();
			}
			///////////////////////////////////////////////
		}
	}*/
}
function addressSelect( event, ui ){
	var input = event.target;
	var id = $(input).attr('id');
	if (
		$(input)[0].hasAttribute('name')
		&&
		id == 'orderAddress'
	) {

		reloadOrder( { full_address: ui.item.value } );

	} else if(
		id.indexOf('delivTemplateAddress_') != -1
		||
		id.indexOf('start_point_address_') != -1
		||
		id == 'address'
		||
		id == 'persDefaultAddress'
	){
		addressGetCoords( input, ui.item.value, true );
	}
}
function addressGetCoords( input, full_address, reloadBakset ){
	$(input).attr('process', 'Y');
	var postParams = {  full_address: full_address  };
	process(true);
	$.post("/ajax/getAddressCoords.php", postParams, function(data){
	    if (data.status == 'ok'){

	    	// если это ввод адреса в корзине
			// и введённый адрес не в том городе, который выбран в качестве основного местоположения
            var id = $(input).attr('id');
            /*if(id == 'address' && data.dif_cities)
			{
                $(input).parent('div').find('input[type=hidden]').val('');
                $(input).val('');
			}
			else*/
			{
                if( 'lat' in data && 'lng' in data ){
                    $(input).parent('div').find('input[type=hidden]').val( data.lat+','+data.lng );
                } else {
                    $(input).parent('div').find('input[type=hidden]').val('');
                    $(input).val('');
                }
			}
            if( reloadBakset ){
                reloadBasket();
            }

	    } else if (data.status == 'error'){
	        var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: data.text});
	    }
	}, 'json')
	.fail(function(data, status, xhr){
	    var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
		$(input).val('');
		$(input).parent('div').find('input[type=hidden]').val('');
	})
	.always(function(data, status, xhr){
	    process(false);
		$(input).blur();
		$(input).attr('process', 'N');
	})
}



function startPointAddressFocus( input ){
	/*var addressName = $(input).val();
	var coordsInput = $(input).parent('div').find('input[type=hidden]');
	var coords = $(coordsInput).val();
	if( coords.length > 0 ){
		$(input).attr('addressName', addressName);
	} else {
		$(input).attr('addressName', '');
	}
	$(input).val('');*/
}
function startPointAddressKeyUp( input, id ){
	$(input).removeClass('is___error');
	var coordsInput = $(input).parent('div').find('input[type=hidden]');
	$(input).attr('addressName', '');
	$(coordsInput).val('');
	//$('#'+id)).autocomplete("close");
}
function startPointAddressFocusOut( input, lastCoords ){
	if( !$(input)[0].hasAttribute('process') ){   $(input).attr('process', 'N');   }
	if( $(input).attr('process') == 'N' ) {
		var val = $(input).val();
		var coordsInput = $(input).parent('div').find('input[type=hidden]');
		var coords = $(coordsInput).val();
		var addressName = $(input).attr('addressName');
		if (val.length == 0 && addressName.length > 0 && coords.length > 0) {
			$(input).val(addressName);
		}
		$(input).attr('addressName', '');
	}
}
function startPointAddressSelect( event, ui ){
	var input = event.target;
	addressGetCoords( input, ui.item.value )
}





function cityFocus( input ){
	var cityName = $(input).val();
	var locIDinput = $(input).parent('div').find('input[type=hidden]');
	var id = $(input).attr('id');
	if( id.indexOf('shopsFilterCity_') != -1  ){
		if( cityName.length > 0 ){
			$(input).val('');
			$(locIDinput).val('');
			reloadShops();
		}
	} else {
		var locID = $(locIDinput).val();
		if( locID.length > 0 ){
			$(input).attr('cityName', cityName);
		} else {
			$(input).attr('cityName', '');
		}
		$(input).val('');
	}
}
function cityKeyUp( input, isBasket ){
	$(input).removeClass('is___error');
	var locIDinput = $(input).parent('div').find('input[type=hidden]');
	$(input).attr('cityName', '');
	$(locIDinput).val('');
}
function cityFocusOut( input, lastLocID ){
	if( !$(input)[0].hasAttribute('process') ){   $(input).attr('process', 'N');   }
	if( $(input).attr('process') == 'N' ) {
		var val = $(input).val();
		var locIDinput = $(input).parent('div').find('input[type=hidden]');
		var locID = $(locIDinput).val();
		var cityName = $(input).attr('cityName');
		if (val.length == 0 && cityName.length > 0 && locID.length > 0) {
			$(input).val(cityName);
		}
		$(input).attr('cityName', '');
		if ( lastLocID != locID ) {
			///////////////////////////////////////////////
			if (
				$(input)[0].hasAttribute('name')
				&&
				in_array($(input).attr('name'), ['basketCity', 'basketSamoCity'])
			) {
				reloadBasket();
			}
			///////////////////////////////////////////////
		}
	}
}
function citySelect( event, ui ){
	var input = event.target;
	if( !$(input)[0].hasAttribute('process') ){   $(input).attr('process', 'N');   }
	if( $(input).attr('process') == 'N' ) {
		$(input).attr('process', 'Y');
		var locIDinput = $(input).parent('div').find('input[type=hidden]');
		if ($(locIDinput).length > 0) {
			clearInterval(cityInputInterval);
			cityInputInterval = setInterval(function () {
				if ($(input).val() == ui.item.value) {
					clearInterval(cityInputInterval);
					$(input).val(ui.item.label);
					$(locIDinput).val(ui.item.value);
					$(input).blur();
					$(input).attr('process', 'N');
					///////////////////////////////////////////////
					var id = $(input).attr('id');
					if (
						$(input)[0].hasAttribute('name')
						&&
						in_array($(input).attr('name'), ['basketCity', 'basketSamoCity'])
					) {

						reloadBasket();

					} else if( id.indexOf('shopsFilterCity_') != -1  ){

						reloadShops();

					}
					///////////////////////////////////////////////
				}
			}, 50);
		}
	}
}
function citySelectShort( event, ui ){
	var input = event.target;
	var locIDinput = $(input).parent('div').find('input[type=hidden]');
	if ($(locIDinput).length > 0) {
		clearInterval(cityInputInterval);
		cityInputInterval = setInterval(function () {
			if ($(input).val() == ui.item.value) {
				$(input).val(ui.item.short_name);
				$(locIDinput).val(ui.item.value);
				$(input).blur();
			}
		}, 50);
	}
}











function regionFocus( input ){
	var loc_name = $(input).val();
	var locIDinput = $(input).parent('div').find('input[type=hidden]');
	var id = $(input).attr('id');
	if( id.indexOf('shopsFilterRegion_') != -1  ){
		if( loc_name.length > 0 ){
			$(input).val('');
			$(locIDinput).val('');
			reloadShops();
		}
	} else if( $(input).attr('id') == 'goodsLKRegion' ){
		if( loc_name.length > 0 ){
			$(input).val('');
			$(locIDinput).val('');
			reloadDealerGoods( false, true );
		}
	} else {
		var locID = $(locIDinput).val();
		if( locID.length > 0 ){
			$(input).attr('loc_name', loc_name);
		} else {
			$(input).attr('loc_name', '');
		}
		$(input).val('');
	}
}
function regionKeyUp( input ){
	$(input).removeClass('is___error');
	var locIDinput = $(input).parent('div').find('input[type=hidden]');
	$(input).attr('loc_name', '');
	$(locIDinput).val('');
}
function regionFocusOut( input ){
	if( !$(input)[0].hasAttribute('process') ){
		$(input).attr('process', 'N');
	}
	if( $(input).attr('process') == 'N' ) {
		var val = $(input).val();
		var locIDinput = $(input).parent('div').find('input[type=hidden]');
		var locID = $(locIDinput).val();
		var loc_name = $(input).attr('loc_name');
		if (val.length == 0 && loc_name.length > 0 && locID.length > 0) {
			$(input).val(loc_name);
		}
		$(input).attr('loc_name', '');
	}
}
function regionSelect( event, ui ){
	var input = event.target;
	if( !$(input)[0].hasAttribute('process') ){   $(input).attr('process', 'N');   }
	if( $(input).attr('process') == 'N' ) {
		$(input).attr('process', 'Y');
		var locIDinput = $(input).parent('div').find('input[type=hidden]');
		if ($(locIDinput).length > 0) {
			clearInterval(cityInputInterval);
			cityInputInterval = setInterval(function () {
				if ($(input).val() == ui.item.value) {
					clearInterval(cityInputInterval);
					$(input).val(ui.item.label);
					$(locIDinput).val(ui.item.value);
					$(input).blur();
					$(input).attr('process', 'N');

					var id = $(input).attr('id');

					if( $(input).attr('id') == 'goodsLKRegion' ){

						reloadDealerGoods();

					} else if( id.indexOf('shopsFilterRegion_') != -1  ){

						reloadShops();

					}

				}
			}, 50);
		}
	}
}




function dealerFocus( input ){
	var dealer_name = $(input).val();
	var dealerIDinput = $(input).parent('div').find('input[type=hidden]');
	var id = $(input).attr('id');
	if( id.indexOf('shopsFilterDealer_') != -1  ){
		if( dealer_name.length > 0 ){
			$(input).val('');
			$(dealerIDinput).val('');
			reloadShops();
		}
	} else {
		var dealerID = $(dealerIDinput).val();
		if( dealerID.length > 0 ){
			$(input).attr('dealer_name', dealer_name);
		} else {
			$(input).attr('dealer_name', '');
		}
		$(input).val('');
	}
}
function dealerKeyUp( input ){
	$(input).removeClass('is___error');
	var dealerIDinput = $(input).parent('div').find('input[type=hidden]');
	$(input).attr('dealer_name', '');
	$(dealerIDinput).val('');
}
function dealerFocusOut( input ){
	var val = $(input).val();
	var dealerIDinput = $(input).parent('div').find('input[type=hidden]');
	var dealerID = $(dealerIDinput).val();
	var dealer_name = $(input).attr('dealer_name');
	if (val.length == 0 && dealer_name.length > 0 && dealerID.length > 0) {
		$(input).val(dealer_name);
	}
	$(input).attr('dealer_name', '');
}
function dealerSelect( event, ui ){
	var input = event.target;
	var dealerIDinput = $(input).parent('div').find('input[type=hidden]');
	if ($(dealerIDinput).length > 0) {
		clearInterval(cityInputInterval);
		cityInputInterval = setInterval(function () {
			if ($(input).val() == ui.item.value) {
				clearInterval(cityInputInterval);
				$(input).val(ui.item.label);
				$(dealerIDinput).val(ui.item.value);
				$(input).blur();
				var id = $(input).attr('id');
				if( id.indexOf('shopsFilterDealer_') != -1  ){
					reloadShops();
				}
			}
		}, 50);
	}
}





function reloadShops(){
	var filter_form = $('.shopsFilterForm:visible');
	var form_data = $(filter_form).serialize();
	var postParams = {
		form_data: form_data,
	};
	process(true);
	$.post("/ajax/reloadShops.php", postParams, function(data){
	    if (data.status == 'ok'){
			$(filter_form).parents('article').find('.shopsLoadArea').html(data.html);
			if( $('ost').length > 0 ){
				$(filter_form).parents('article').find('.moreShopsButton').show();
			} else {
				$(filter_form).parents('article').find('.moreShopsButton').hide();
			}
			$('ost').remove();
			if( $('.shopItem').length > 0 ){
				$('.no_count_shops').hide();
			} else {
				$('.no_count_shops').show();
			}

			removeMarkers();

			if( 'shops' in data ){

				var $mapDiv = $('#map_dealers');
				var mapDim = {
					height: $mapDiv.height(),
					width: $mapDiv.width()
				}

				shop_markers = [];
			    for( key in data.shops ){
			        var shop = data['shops'][key];
			        if( 'PROPERTY_GPS_VALUE' in shop ){
						var gps = shop.PROPERTY_GPS_VALUE;
						if( gps != null && gps.length > 0 ){
							var ar = gps.split(',');
							shop_markers.push(
								new google.maps.Marker({
									position: { lat: Number(ar[0]), lng: Number(ar[1]) },
									arShop: shop,
									icon: '/local/templates/main/images/marker.png'
								})
							);
						}
			        }
			    }

				var bounds = (shop_markers.length > 0) ? createBoundsForMarkers(shop_markers) : null;
				$.each(shop_markers, function() {   this.setMap(points_map);   });
				points_map.fitBounds(bounds);
				if( shop_markers.length == 1 ){   points_map.setZoom(15);   }

				var clusterStyles = {
					styles: [{
						height: 43,
						url: '/local/templates/main/images/marker_cluster.png',
						textColor: 'white',
						width: 43,
						fontFamily: 'futuralight,Arial,sans-serif',
						fontWeight: 'normal',
						textSize: 16
					}]
				};
				markerCluster = new MarkerClusterer(points_map, shop_markers, clusterStyles);

			}

	    } else if (data.status == 'error'){
	        var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: data.text});
	    }
	}, 'json')
	.fail(function(data, status, xhr){
	    var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
	})
	.always(function(data, status, xhr){
	    process(false);
	})
}

function removeMarkers(){
	for( key in shop_markers ){
		var shop_marker = shop_markers[key];
		shop_marker.setMap(null);
	}
	shop_markers = [];
	markerCluster.clearMarkers();
	markerCluster.repaint();
}



function openDealerModalMap( input ){
	coords_input = input;
	if( modal_map_marker ){
		modal_map_marker.setMap(null);
		modal_map_marker = false;
	}
	modal_map.setCenter({ lat: 55.75399399999374, lng: 37.62209300000001 });
	modal_map.setZoom(10);
	$('.mapModalLink').fancybox({ padding: 0, wrapCSS: 'fancybox-review' }) .trigger('click');
}



function locSelect( event, ui ){
	var input = event.target;
	var locIDinput = $(input).parent('div').find('input[type=hidden]');
	if ($(locIDinput).length > 0) {
		clearInterval(cityInputInterval);
		cityInputInterval = setInterval(function () {
			if ($(input).val() == ui.item.value) {
				clearInterval(cityInputInterval);
				$(input).val(ui.item.label);
				$(locIDinput).val(ui.item.value);
				$(input).blur();

				var loc_id = $(locIDinput).val();
				var loc_city = $(input).val();

				$curLocID = $.cookie('BITRIX_SM2_loc_id');

				var base_domain = $('input[name=base_domain]').val();
				var cur_domain = $('input[name=cur_domain]').val();

				$.cookie('BITRIX_SM2_loc_id', loc_id, {
					expires: 30, path: '/', domain: '.'+base_domain
				});
				$.cookie('BITRIX_SM2_loc_city', loc_city, {
					expires: 30, path: '/', domain: '.'+base_domain
				});

				setTimeout(function() {

					var request_scheme = $('input[name=request_scheme]').val();
					var base_domain = $('input[name=base_domain]').val();
					var cur_url = document.URL;
					var arURL = cur_url.split('#');
					cur_url = arURL[0];
					var ar = cur_url.split(base_domain);
					//var new_url = request_scheme+'://'+sub_domain+ar[1];
					if( 'sub_domain' in ui.item ){
						var new_url = request_scheme + '://' + ui.item.sub_domain + '.' + base_domain + ar[1];
					} else {
						var new_url = request_scheme + '://' + base_domain + ar[1];
					}

                    if($("body").data().app)
                    {
                        new_url = request_scheme+'://'+base_domain+ar[1];
						/*console.log(base_domain);
                        console.log(new_url);
                        return false;*/
                    }


					window.location.href = new_url;

					// if( loc_id == $curLocID ){
					// 	window.location.href = new_url;
					// } else {
					// 	process(true);
					// 	$.post("/ajax/clearBasket.php", {}, function(data){}, 'json')
					// 	.fail(function(data, status, xhr){})
					// 	.always(function(data, status, xhr){
					// 		window.location.href = new_url;
					// 	})
					// }

					// if( 'sub_domain' in ui.item ){
					// 	var url = 'http://' + ui.item.sub_domain + '.' + base_domain + '/';
					// } else {
					// 	var url = 'http://' + base_domain + '/';
					// }
					// window.location.href = url;

				}, 100);

			}
		}, 50);
	}
}





function createBoundsForMarkers(markers) {
	var bounds = new google.maps.LatLngBounds();
	$.each(markers, function(){    bounds.extend(this.getPosition());    });
	return bounds;
}

function getBoundsZoomLevel(bounds, mapDim) {
	var WORLD_DIM = { height: 256, width: 256 };
	var ZOOM_MAX = 21;
	function latRad(lat) {
		var sin = Math.sin(lat * Math.PI / 180);
		var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
		return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
	}
	function zoom(mapPx, worldPx, fraction) {
		return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
	}
	var ne = bounds.getNorthEast();
	var sw = bounds.getSouthWest();
	var latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI;
	var lngDiff = ne.lng() - sw.lng();
	var lngFraction = ((lngDiff < 0) ? (lngDiff + 1000) : lngDiff) / 1000;
	var latZoom = zoom(mapDim.height, WORLD_DIM.height, latFraction);
	var lngZoom = zoom(mapDim.width, WORLD_DIM.width, lngFraction);
	return Math.min(latZoom, lngZoom, ZOOM_MAX);
}




function setSearchMinHeight(){
	if( $('.searchPanel').length > 0 && $('.searchBlock__content').length > 0 ){
		var searchPanelHeight = $('.searchPanel').height();
		$('.searchBlock__content').css('min-height', searchPanelHeight + 'px');
	}
}



function setSettingsText( el ){
	var distanceType = $(el).parents('form.deliverySettingsForm').find('input.distanceTypeInput:checked').val();
	var tariffType = $(el).parents('form.deliverySettingsForm').find('input.tariffTypeInput:checked').val();
	if( distanceType.length > 0 && tariffType.length > 0 ){
		var id = distanceType+'_'+tariffType;
		var text = $('#'+id).html();
		$(el).parents('form.deliverySettingsForm').find('.settingsTextBlock p').html(text);
		$(el).parents('form.deliverySettingsForm').find('.settingsTextBlock').parent().show();
	} else {
		$(el).parents('form.deliverySettingsForm').find('.settingsTextBlock p').html('');
		$(el).parents('form.deliverySettingsForm').find('.settingsTextBlock').parent().hide();
	}
}


function dealerDopServicesCheckboxChange( input ){
	if( $(input).prop('checked') ){
		$(input).parents('.dop_service___block').find('.dop_service___settings').show();
	} else {
		$(input).parents('.dop_service___block').find('.dop_service___settings').hide();
	}
}




function is_wrong_number( number ){
	return (
		(/^0[0-9]+$/.test(number))
		||
		(/,+/.test(number))
		||
		(/^[^0-9]/.test(number))
		||
		(/[^0-9.]/.test(number))
		||
		(/\..*\./.test(number))
	);
}
function prepare_number_inputs( input_class ){
	if( input_class ){
		$( '.' + input_class ).each(function(){
			var value = $(this).val();
			$(this).attr('prev_value', value);
		})
	}
}
function check_number_inputs( input ){
	if( input ){
		var value = $(input).val();
		var new_value = value.replace(new RegExp(",",'g'), ".");
		if( new_value != value ){
			value = new_value;
			if( !is_wrong_number( value ) ){
				$(input).val( value );
				$(input).attr( 'prev_value', value );
			}
		}
		var prev_value = $(input).attr('prev_value');
		if( value.length > 0 ){
			if( is_wrong_number( value ) ){
				if( value.startsWith(prev_value) ){
					$(input).val( prev_value );
				} else {
					$(input).val('');
				}
			} else {
				$(input).attr( 'prev_value', value );
			}
		} else {
			$(input).attr( 'prev_value', value );
		}
	}
}



function is_wrong_price( price ){
	return (
		(/^0[0-9]+$/.test(price))
		||
		(/,+/.test(price))
		||
		(/^[^0-9]/.test(price))
		||
		(/[^0-9.]/.test(price))
		||
		(/\..*\./.test(price))
		||
		(/\.[0-9]{3}/.test(price))
	);
}
function prepare_price_inputs( input_class ){
	if( input_class ){
		$( '.' + input_class ).each(function(){
			var value = $(this).val();
			$(this).attr('prev_value', value);
		})
	}
}
function check_price_inputs( input ){
	if( input ){
		var value = $(input).val();
		var new_value = value.replace(new RegExp(",",'g'), ".");
		if( new_value != value ){
			value = new_value;
			if( !is_wrong_price( value ) ){
				$(input).val( value );
				$(input).attr( 'prev_value', value );
			}
		}
		var prev_value = $(input).attr('prev_value');
		if( value.length > 0 ){
			if( is_wrong_price( value ) ){
				if( value.startsWith(prev_value) ){
					$(input).val( prev_value );
				} else {
					$(input).val('');
				}
			} else {
				$(input).attr( 'prev_value', value );
			}
		} else {
			$(input).attr( 'prev_value', value );
		}
	}
}





function is_write_coords( coords ){
	return (/^ *[0-9]{0,1}[0-9]{0,1}[0-9]\.[0-9]+ *, *[0-9]{0,1}[0-9]{0,1}[0-9]\.[0-9]+ *$/.test(coords));
}




