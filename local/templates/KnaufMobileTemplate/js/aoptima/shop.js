var psInputInterval, filterInterval, basketInterval;


function catalogLoad( params ){
    if( !params ){ var params = {}; }
    params.section_id = $('input[name=section_id]').val();
    if( $('input[name=seo_page_code]').length > 0 ){
        params.seo_page_code = $('input[name=seo_page_code]').val();
    }
    params.url = document.URL;
    if( $('.left___q').length > 0 ){
        params.q = $('.left___q').val();
    }
    process(true);

    params['app'] = $("body").data().app;

    $.post("/ajax/catalogLoad.php", params, function(data){
        if (data.status == 'ok'){

            if( 'items' in params ){
                $('.catalog_load_block').append(data.html);
            } else {
                $('.catalog_load_block').html(data.html);

                if( 'h1' in data ){
                    $('h1.seo___h1').html(data.h1);
                }
                if( 'seo_title' in data ){
                    $('title').html(data.seo_title + " " +  cityPostfix + " | Маркетплейс «Купи КНАУФ»");
                }
                if( 'seo_description' in data ){
                    $('meta[name=description]').attr('content', data.seo_description);
                }
                if( 'seo_keywords' in data ){
                    $('meta[name=keywords]').attr('content', data.seo_keywords);
                }

                if(
                    'seo_text' in data
                    &&
                    data.seo_text.length > 0
                ){
                    $('div.seo_text').html(data.seo_text);
                    $('div.seo_text').show();
                } else {
                    $('div.seo_text').html('');
                    $('div.seo_text').hide();
                }
            }
            if( $('ost').length > 0 ){
                $('.catalog_load_button').show();
            } else {
                $('.catalog_load_button').hide();
            }
            $('ost').remove();

        } else if (data.status == 'error'){
            var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: data.text});
        }
    }, 'json')
        .fail(function(data, status, xhr){
            var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
        })
        .always(function(data, status, xhr){
            process(false);
        })
}


function reloadBasket(params){
    if( !params ){  var params = {};  }
    params['form_data'] = $('.basketForm').serialize();
    if( 'action' in params ){} else {
        params['action'] = 'reloadBasket';
    }
    params['IS_MOB_APP'] = isMobApp;
    process(true);

    $('.wait___block').show();
    $('.goods___block').hide();
    $('.warning___block').hide();

    $.post("/ajax/ajax.php", params, function(data){
        if (data.status == 'ok'){

            $('.basketArea').replaceWith(data.html);

            createStyledSelects();
            var t = "";
            initOpenCloseDropdown(t);

            var delivType = $('.basketForm input[name=ds]:checked').attr('delivType');
            if ( delivType == 'samovyvoz' ) {
                $('.deliveryBlock').hide();
                $('.samovyvozBlock').show();
            } else if ( delivType == 'delivery' ) {
                $('.deliveryBlock').show();
                $('.samovyvozBlock').hide();
            }

            var basket_cnt = 0;
            $('.basketProductTR').each(function(){
                // var qnt = Number($(this).find('input[name=quantity]').val());
                // basket_cnt += qnt;
                basket_cnt++;
            });
            $('.basketSmallBlock .main-nav__cart-quantity').html( basket_cnt );

            $('.tooltip__icon').tooltipster({
                animation: 'fade',
                delay: 100,
                maxWidth: 240,
                side: 'top',
                distance: 2,
                theme: 'tooltipster-white',
                trigger: 'click',
                content: $('<span class="tooltip__close"></span><span class="tooltip__title">Почему цена от?</span><span class="tooltip__text">Цена на выбранные продукты может быть разной, в зависимости от выбранного дилера. Пожалуйста, заполните поля ниже, чтобы получить итоговую стоимость.</span>')
            });

            if(params.cleanBasket)
            {
                var base_domain = $('input[name=base_domain]').val();
                $.cookie(
                    'BITRIX_SM2_systems_data',
                    "",
                    {
                        expires: 365,
                        path: '/',
                        domain: '.'+base_domain
                    }
                );
            }
        }
    }, 'json')
        .fail(function(data, status, xhr){
            var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса"});
        })
        .always(function(data, status, xhr){
            process(false);
        })
}


function reloadOrder( params, address_input ){
    if( orderReloadProcess != 'Y' ){
        orderReloadProcess = 'Y';
        var address_input_val = $(address_input).val();
        if( !params ){  var params = {};  }
        params['form_data'] = $('.orderForm').serialize();
        if( 'action' in params ){} else {
            params['action'] = 'reloadOrder';
        }
        params['IS_MOB_APP'] = isMobApp;
        process(true);
        $.post("/ajax/ajax.php", params, function(data){
            if (data.status == 'ok'){

                $('.orderArea').replaceWith(data.html);

                if ($('.orderDatepicker').length) {



                    var curYear = (new Date()).getFullYear();
                    $(".orderDatepicker").datepicker({
                        inline: true,
                        showOtherMonths: true,
                        minDate: 0
                    });
                    jQuery(function($) {
                        $.datepicker.regional['ru'] = {
                            closeText: 'Закрыть',
                            prevText: '&#x3c;Пред',
                            nextText: 'След&#x3e;',
                            currentText: 'Сегодня',
                            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
                            ],
                            monthNamesShort: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
                            ],
                            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
                            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
                            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                            weekHeader: 'Нед',
                            dateFormat: 'dd.mm.yy',
                            firstDay: 1,
                            isRTL: false,
                            showMonthAfterYear: false,
                            yearSuffix: ''
                        };
                        $.datepicker.setDefaults($.datepicker.regional['ru']);
                    });
                }

                styleForms();

                //$('input[type=tel]').mask('+7 (000) 000 00 00', {placeholder: "+7 (___) ___ __ __"});

                if( address_input_val ){
                    $('#orderAddress').val(address_input_val);
                    $('#orderAddress').autocomplete('enable');
                    $('#orderAddress').autocomplete('search', address_input_val);
                }

            }
        }, 'json')
            .fail(function(data, status, xhr){
                var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса"});
            })
            .always(function(data, status, xhr){
                process(false);
                orderReloadProcess = 'N';
            })
    }
}


function updateResetButton(){
    var active = false;
    $('.filter_number_input').each(function(){
        if( $(this).attr('default_value') == $(this).val() ){} else {
            active = true;
        }
    })
    if( $('.filter___checkbox:checked').length > 0 ){
        active = true;
    }
    if( active ){
        $('.resetFilterBlock').show();
    } else {
        $('.resetFilterBlock').hide();
    }
}


function filterGo(){

    var number_names = [];
    $('.filter_number_input').each(function(){
        if( $(this).attr('default_value') == $(this).val() ){
            number_names.push({ link: $(this), name: $(this).attr('name') });
            $(this).attr('name', '');
        }
    })

    /////////////////////////////
    var filter_form = $('form.smartfilter');
    var form_data = $(filter_form).serialize();
    var curURL = $('input[name=curURL]').val();
    /////////////////////////////

    if( number_names.length > 0 ){
        for( key in number_names ){
            var item = number_names[key];
            item.link.attr('name', item.name);
        }
    }

    process(true);
    $.post(curURL+'?'+form_data+'&ajax=y', {}, function(data){})
        .fail(function(data, status, xhr){})
        .always(function(data, status, xhr){
            if( 'responseText' in data ){
                var responseText = data.responseText;

                if( responseText ){

                    responseText = responseText.replace(new RegExp("\n",'g'), "");
                    responseText = responseText.replace(new RegExp("'",'g'), "\"");

                    var result = JSON.parse( responseText );
                    var new_url = result.FILTER_URL;
                    new_url = new_url.replace(new RegExp("/filter/clear/apply/",'g'), "/");
                    new_url = new_url.replace(new RegExp("/filter/apply/",'g'), "/");

                    if( $('.left___q').length > 0 ){
                        var q = $('.left___q').val();
                        if( q.length > 0 ){
                            new_url = new_url+'?q='+q;
                        }
                    }

                    if( $('input[name=seo_page_code]').length > 0 ){
                        window.location.href = new_url;
                    } else {
                        history.replaceState({}, null, new_url);
                    }

                    catalogLoad();
                }

                updateResetButton();

            }
            process(false);
        })
}


function catalogTabReload( li ){
    var link = $(li);
    var section_id = $(link).attr('item_id');
    var parent_section_id = $(link).parents('.parentSectionBlock').attr('item_id');

    process(true);

    var params = { section_id:section_id };
    params['app'] = $("body").data().app;

    $.post("/ajax/getCatalogSectionSlider.php", params, function(data){
        if (data.status == 'ok'){

            $(link).parents('.parentSectionBlock').find('.sliderBlock__sliders').html( data.html );

            $(link).parents('.parentSectionBlock').find('.catalogSectLink').removeClass('active');
            $(link).addClass('active');

            var config = {
                slideWidth: 204, minSlides: 1,
                maxSlides: 4, moveSlides: 4,
                infiniteLoop: false, controls: true,
                touchEnabled: false,  slideMargin: 26
            };
            var windowWidth = $(window).width()
            if (windowWidth < 570) {
                config.minSlides = 2;
                config.maxSlides = 2;
            } else {
                config.minSlides = 1;
                config.maxSlides = 4;
            }

            var slider = $('.catalog_slider_'+section_id).bxSlider(config);
            if ( $(slider).children('.slider__item').length < 5 ){
                slider.parents('.bx-wrapper').addClass('noSlider');
                if ($(slider).children('.slider__item').length >= 3) {
                    slider.parents('.bx-wrapper').addClass('noSlider--moreThreeSlides');
                }
            }

            $('.bx-controls').each(function() {
                var t = $(this);
                var summ = 0;
                $('.bx-pager-item', t).each(function() {
                    summ += $(this).width();
                });
                $('.bx-controls-direction', t).width(summ);
            });

            dropDownHandlerClick($(li).find('a'));

        }
    }, 'json')
        .fail(function(data, status, xhr){
            var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
        })
        .always(function(data, status, xhr){
            process(false);
        })
}



function openfilterPageWindow(){
    var url = document.URL;
    var arURL = url.split("#");
    var section_id = $('input[name=section_id]').val();
    var postParams = { url: arURL[0], section_id:section_id };
    process(true);
    $.post("/ajax/openFilterPageFormWindow.php", postParams, function(data){
        if (data.status == 'ok'){
            $('#filterPageFormWindow').html( data.html );
            $('.filterPageFormWindowLink').fancybox({padding: 0,wrapCSS: 'fancybox-review'}).trigger('click');
        } else if (data.status == 'error'){
            var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: data.text});
        }
    }, 'json')
        .fail(function(data, status, xhr){
            var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
        })
        .always(function(data, status, xhr){
            process(false);
        })

}



$(document).ready(function(){



    // Сохранение SEO-страницы фильтра
    $(document).on('click', '.saveFilterPage', function(e){
        if( !is_process(this) ){
            var button = $(this);
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input[type=text], input[type=email], input[type=password], textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var UF_H1 = $(this_form).find("input[name=UF_H1]").val();
            var UF_META_TITLE = $(this_form).find("input[name=UF_META_TITLE]").val();
            var UF_META_DESCRIPTION = $(this_form).find("input[name=UF_META_DESCRIPTION]").val();
            var UF_META_KEYWORDS = $(this_form).find("input[name=UF_META_KEYWORDS]").val();
            var UF_TEXT = $(this_form).find("textarea[name=UF_TEXT]").val();
            var error = false;

            // Проверки
            if (UF_H1.length == 0){
                error = ['UF_H1',   'Введите, пожалуйста, H1!'];
            } else if (UF_META_TITLE.length == 0){
                error = ['UF_META_TITLE',   'Введите, пожалуйста, SEO title!'];
            }
            // Если нет ошибок
            if (!error){

                var postParams = {
                    form_data: $(this_form).serialize(),
                    url: document.URL,
                    section_id: $('input[name=section_id]').val()
                };
                process(true);
                $.post("/ajax/saveFilterPage.php", postParams, function(data){
                    if (data.status == 'ok'){

                        if( 'h1' in data ){
                            $('h1.seo___h1').html(data.h1);
                        }
                        if( 'seo_title' in data ){
                            $('title').html(data.seo_title + " " +  cityPostfix + " | Маркетплейс «Купи КНАУФ»");
                        }
                        if( 'seo_description' in data ){
                            $('meta[name=description]').attr('content', data.seo_description);
                        }
                        if( 'seo_keywords' in data ){
                            $('meta[name=keywords]').attr('content', data.seo_keywords);
                        }

                        if(
                            'seo_text' in data
                            &&
                            data.seo_text.length > 0
                        ){
                            $('div.seo_text').html(data.seo_text);
                            $('div.seo_text').show();
                        } else {
                            $('div.seo_text').html('');
                            $('div.seo_text').hide();
                        }

                        $('.fancybox-close').trigger('click');

                    } else if (data.status == 'error'){
                        show_error( this_form, null, data.text );
                    }
                }, 'json')
                    .fail(function(data, status, xhr){
                        show_error( this_form, null, 'Ошибка запроса' );
                    })
                    .always(function(data, status, xhr){
                        process(false);
                    })


                // Ошибка
            } else {

                show_error(this_form, error[0], error[1]);
            }
        }
    })


    // Удаление SEO-страницы фильтра
    $(document).on('click', '.deleteFilterPage', function(e){
        if( !is_process(this) ){

            var button = $(this);

            // Форма
            var this_form = $(this).parents('form');

            var postParams = {
                form_data: $(this_form).serialize(),
                url: document.URL,
                section_id: $('input[name=section_id]').val()
            };
            process(true);
            $.post("/ajax/deleteFilterPage.php", postParams, function(data){
                if (data.status == 'ok'){

                    if( 'h1' in data ){
                        $('h1.seo___h1').html(data.h1);
                    }
                    if( 'seo_title' in data ){
                        $('title').html(data.seo_title + " " +  cityPostfix + " | Маркетплейс «Купи КНАУФ»");
                    }
                    if( 'seo_description' in data ){
                        $('meta[name=description]').attr('content', data.seo_description);
                    }
                    if( 'seo_keywords' in data ){
                        $('meta[name=keywords]').attr('content', data.seo_keywords);
                    }

                    if(
                        'seo_text' in data
                        &&
                        data.seo_text.length > 0
                    ){
                        $('div.seo_text').html(data.seo_text);
                        $('div.seo_text').show();
                    } else {
                        $('div.seo_text').html('');
                        $('div.seo_text').hide();
                    }

                    $('.fancybox-close').trigger('click');

                } else if (data.status == 'error'){
                    show_error( this_form, null, data.text );
                }
            }, 'json')
                .fail(function(data, status, xhr){
                    show_error( this_form, null, 'Ошибка запроса' );
                })
                .always(function(data, status, xhr){
                    process(false);
                })

        }
    })




    // Выбор категории в корневом разделе каталога
    $(document).on('click', '.catalogSectLink', function(){
        if (
            !is_process(this)
            &&
            !$(this).hasClass('active')
        ){     catalogTabReload( this );     }
    })



    $(document).on('change', 'select[name=productModSelect]', function(e){
        var url = $(this).val();
        window.location.href = url;
    })


    $(document).on('click', '.no_auth_order_button', function(){
        if (!is_process(this)){
            var postParams = {
                action: "no_auth_order",
            };
            process(true);
            $.post("/ajax/ajax.php", postParams, function(data){
                if (data.status == 'ok'){
                    window.location.href = document.URL;
                }
            }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 4000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
        }
    });



    // Товар в корзину
    $(document).on('click', '.basketButton', function(){
        var button = $(this);
        if (!is_process(button)){
            var tov_id = $(button).attr('item_id');
            process(true);
            var params = { action:"add_to_basket", tov_id:tov_id };
            if( $(button).hasClass('basketButtonDetail') ){
                params['quantity'] = $('input[name=basketCnt]').val();
            }
            params['IS_MOB_APP'] = isMobApp;
            $.post("/ajax/ajax.php", params, function(data){
                if (data.status == 'ok') {

                    $.noty.closeAll();
                    var n = noty({
                        closeWith: ['hover'],
                        timeout: 4000,
                        layout: 'topRight',
                        type: 'success',
                        text: "Товар добавлен в корзину!"
                    });

                    $('.basketSmallBlock').replaceWith(data.html);

                    if( $(button).hasClass('basketButtonDetail') ){
                        $('input[name=basketCnt]').val(1);
                        $('input[name=basketCnt]').change();
                    }
                    else
                    {
                        // скрыть кнопку "в корзину"
                        button.hide();
                        // показать переключатель количества
                        button.parents(".productItem").find(".catalog-list-inbask .listfieldCount").val(1);
                        // пересчитаем количество в метрах
                        var $size = button.parents(".productItem").find(".catalog-list-inbask .slider__item-counterDimension");
                        var size_one = parseFloat($size.data("size"));
                        $size.find('span').text(size_one.toFixed(2));

                        button.parents(".productItem").find(".catalog-list-inbask").show();

                        button.parents(".bx-viewport").css("height", "auto");
                    }

                    if( $(button).hasClass('basketButtonDetail') ){
                        if($("#system_complects_page").length)
                        {
                            ym(55379437, 'reachGoal', 'systemsPageAddBasketForProductPage', {URL: document.location.href});
                        }
                        else {
                            ym(55379437, 'reachGoal', 'addBasketForProductPage');
                        }
                    } else {
                        if($("#system_complects_page").length)
                        {
                            ym(55379437, 'reachGoal', 'systemsAddBusketFromCatalog', {URL: document.location.href});
                        }
                        else {
                            ym(55379437, 'reachGoal', 'addBusketFromCatalog');
                        }
                    }

                } else if (data.status == 'error'){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: data.text})
                }
            }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса"});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
        }
    });

    // Количество в списках ПЛЮС
    $(document).on('click', '.listbasketPlus', function(){
        var quantityInput = $(this).parents('.catalog-list-inbask').find('.listfieldCount');
        var quantity = Number($(quantityInput).val());
        quantity++;
        $(quantityInput).val(quantity);
        $(quantityInput).change();
    });

    // Количество в списках МИНУС
    $(document).on('click', '.listbasketMinus', function(){
        var quantityInput = $(this).parents('.catalog-list-inbask').find('.listfieldCount');
        var quantity = Number($(quantityInput).val());
        quantity--;
        if( quantity <= 0 )
        {
            quantity = 0;
            $(quantityInput).val(quantity);
            $(quantityInput).change();
        }
        else
        {
            $(quantityInput).val(quantity);
            $(quantityInput).change();
        }
    });

    // Количество в списках  - ввод значения вручную
    $(document).on('change', '.catalog-list-inbask .listfieldCount', function(){
        var quantityInput = $(this);
        var quantity = Number($(quantityInput).val());

        if( quantity <= 0 ){  quantity = 0;  }
        $(quantityInput).val(quantity);

        // пересчитаем количество в метрах
        var $size = $(this).parents('.catalog-list-inbask').find('.slider__item-counterDimension');
        var size_one = parseFloat($size.data("size"));
        var size = size_one * quantity;
        $size.find('span').text(size.toFixed(2));

        if(typeof(timeout_id) !== 'undefined')
        {
            clearTimeout(timeout_id);
        }
        timeout_id = setTimeout(
            function () {
                if (!is_process(quantityInput)){
                    var tov_id = quantityInput.attr('item_id');
                    process(true);
                    var params = {
                        action:"add_to_basket",
                        tov_id:tov_id,
                        quantity:quantity,
                        replace_quant:true // будем не добавлять переданное количество, а заменять на него
                    };
                    params['IS_MOB_APP'] = isMobApp;
                    $.post("/ajax/ajax.php", params, function(data){
                        if (data.status == 'ok') {

                            if(quantity == 0)
                            {
                                // показать кнопку "в корзину"
                                quantityInput.parents(".productItem").find(".basketButton").show();
                                // скрыть переключатель количества
                                quantityInput.parents(".productItem").find(".catalog-list-inbask").hide();
                            }
                            else {
                                $.noty.closeAll();
                                var n = noty({
                                    closeWith: ['hover'],
                                    timeout: 4000,
                                    layout: 'topRight',
                                    type: 'success',
                                    text: "Количество в корзине изменено!"
                                });
                            }
                            $('.basketSmallBlock').replaceWith(data.html);

                        } else if (data.status == 'error'){
                            var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: data.text})
                        }
                    }, 'json')
                        .fail(function(data, status, xhr){
                            var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса"});
                        })
                        .always(function(data, status, xhr){
                            process(false);
                        })
                }
            },
            1000
        );
    });


	// Количество на карточке ПЛЮС
    $(document).on('click', '.productPlus', function(){
        var quantityInput = $(this).parents('.cart__goodsTable-counter').find('input[name=basketCnt]');
        var quantity = Number($(quantityInput).val());
        quantity++;
        $(quantityInput).val(quantity);
        $(quantityInput).change();
    });

    // Количество на карточке МИНУС
    $(document).on('click', '.productMinus', function(){
        var quantityInput = $(this).parents('.cart__goodsTable-counter').find('input[name=basketCnt]');
        var quantity = Number($(quantityInput).val());
        quantity--;
        if( quantity <= 0 ){  quantity = 1;  }
        $(quantityInput).val(quantity);
        $(quantityInput).change();
    });


// КОРЗИНА ////////////////////////////////////


    // Удаление товара из корзины
    $(document).on('click', '.basketRemoveButton', function(){
        if (!is_process(this)){
            var remove_product_id = $(this).parents('.basketProductTR').data('product-id');
            var params = { remove_product_id: remove_product_id };
            reloadBasket(params);
        }
    })

    // Удаление товара под заказ из корзины
    $(document).on('click', '.basketRemoveUnderOrderButton', function(){
        if (!is_process(this)){
            var remove_product_id = $(this).parents('.basketUnderOrderProductTR').data('product-id');
            var curItems = [];
            var cookieItems = $.cookie('BITRIX_SM2_under_order');
            if( cookieItems != undefined && cookieItems.length > 0 ){
                curItems = cookieItems.split('|');
                for( key in curItems ){
                    var id = curItems[key];
                    if( id == remove_product_id ){
                        curItems.splice(key, 1);
                    }
                }
                var base_domain = $('input[name=base_domain]').val();
                $.cookie('BITRIX_SM2_under_order', curItems.join('|'), {
                    expires: 30, path: '/', domain: '.'+base_domain
                });
            }
            reloadBasket();
        }
    })

    // Пересчёт корзины
    $(document).on('click', '.basketRecalcButton', function(){
        if (!is_process(this)){
            var params = {newBasketQuantity:{}};
            $('.basketProductTR').each(function(){
                var item_id = $(this).data('product-id');
                if($(this).find('input[name=quantity]').length) {
                    var quantity = $(this).find('input[name=quantity]').val();
                    params['newBasketQuantity'][item_id] = Number(quantity);
                }
            });
            reloadBasket(params);
        }
    });

    // Очистка корзины
    $(document).on('click', '.basketCleanButton', function(){
        if (!is_process(this)){
            var params = {cleanBasket:'Y'};
            reloadBasket(params);
        }
    })


    // Количество в корзине ПЛЮС
    $(document).on('click', '.basketPlus', function(){
        var quantityInput = $(this).parents('.basketProductTR').find('input[name=quantity]');
        var quantity = Number($(quantityInput).val());
        quantity++;
        $(quantityInput).val(quantity);
        $('.basketRecalcButton').show();

        /*if (!is_process(this)){
            var params = {newBasketQuantity:{}};
            $('.basketProductTR').each(function(){
                var item_id = $(this).data('product-id');
                var quantity = $(this).find('input[name=quantity]').val();
                params['newBasketQuantity'][item_id] = Number(quantity);
            })
            reloadBasket(params);
        }*/
    })

    // Количество в корзине МИНУС
    $(document).on('click', '.basketMinus', function(){
        var quantityInput = $(this).parents('.basketProductTR').find('input[name=quantity]');
        var quantity = Number($(quantityInput).val());
        quantity--;
        if( quantity <= 0 ){  quantity = 1;  }
        $(quantityInput).val(quantity);
        $('.basketRecalcButton').show();

        /*if (!is_process(this)){
             var params = {newBasketQuantity:{}};
             $('.basketProductTR').each(function(){
                 var item_id = $(this).data('product-id');
                 var quantity = $(this).find('input[name=quantity]').val();
                 params['newBasketQuantity'][item_id] = Number(quantity);
             })
             reloadBasket(params);
         }*/
    })


    // Количество в корзине  - ввод значения вручную
    $(document).on('change', '.basketForm input[name=quantity]', function(){
        var quantityInput = $(this).parents('.basketProductTR').find('input[name=quantity]');
        var quantity = Number($(quantityInput).val());

        if( quantity <= 0 ){  quantity = 1;  }
        $(quantityInput).val(quantity);
        // $('.basketRecalcButton').show();

        if (!is_process(this)){
            var params = {newBasketQuantity:{}};
            $('.basketProductTR').each(function(){
                var item_id = $(this).data('product-id');
                if($(this).find('input[name=quantity]').length) {
                    var quantity = $(this).find('input[name=quantity]').val();
                    params['newBasketQuantity'][item_id] = Number(quantity);
                }
            });
            reloadBasket(params);
        }
    });


    $(document).on('change', '.basketForm input[name=ds], .basketForm input[name=deliveryDate], .basketForm select[name=deliveryTime], .basketForm input[name=needRazgruzka], .basketForm input[name=needPodyem], .basketForm input[name=needPerenos], .basketForm input[name=perenosDistance], .basketForm input[name=lift]', function(){
        // смена варианта доставки
        if( $(this).attr('name') == 'ds' ){
            var ds_id = $(this).val();
            // Самовывоз
            if( basketInfo['DS_LIST'][ds_id]['XML_ID'] == 'samovyvoz' ){
                ym(55379437, 'reachGoal', 'clickPickup');
                // Доставка
            } else if( basketInfo['DS_LIST'][ds_id]['XML_ID'] == 'delivery' ){
                ym(55379437, 'reachGoal', 'clickDelivery');
            }
            // смена варианта оплаты
        } else if( $(this).attr('name') == 'ps' ){
            var ps_id = $(this).val();
            // Наличные
            if( basketInfo['PS_LIST'][ps_id]['CODE'] == 'cash' ){
                ym(55379437, 'reachGoal', 'clickCash');
                // Банк. карты
            } else if( basketInfo['PS_LIST'][ps_id]['CODE'] == 'bank_card' ){
                ym(55379437, 'reachGoal', 'clickBankcard');
                // Электр. деньги
            } else if( basketInfo['PS_LIST'][ps_id]['CODE'] == 'electronic_money' ){
                ym(55379437, 'reachGoal', 'clickElectronicmoney');
            }
            // смена типа оплаты
        } else if( $(this).attr('name') == 'pay_type' ){
            // Предоплата
            if( $(this).val() == 'pre' ){
                ym(55379437, 'reachGoal', 'clickTipoplatiPred');
                // Постоплата
            } else if( $(this).val() == 'post' ){
                ym(55379437, 'reachGoal', 'clickTipoplatiPost');
            }
        }
        reloadBasket();
    });
    $(document).on('change', '.basketForm input.psInput', function(){
        clearInterval(psInputInterval);
        psInputInterval = setInterval(function(){
            clearInterval(psInputInterval);
            reloadBasket();
        }, 600);
    });
    $(document).on('change', '.basketForm input.prepsInput', function(){
        clearInterval(psInputInterval);
        psInputInterval = setInterval(function(){
            clearInterval(psInputInterval);
            reloadBasket();
        }, 600);
    });
    $(document).on('keyup', '.basketForm input[name=perenosDistance], .basketForm input[name=floor]', function(){
        var input = $(this);
        minus_zero( $(input) );
        clearInterval(basketInterval);
        basketInterval = setInterval(function(){
            clearInterval(basketInterval);
            reloadBasket();
        }, 600);
    });
    $(document).on('keyup', '.personal_delivery_form input[name=UF_FLOOR_DEFAULT]', function(){
        var input = $(this);
        minus_zero( $(input) );
    });






    // Подгрузка предложений дилеров в корзине
    $(document).on('click', '.basketMoreButton', function(e){
        var button = $(this);
        var maxCNT = Number($(button).attr('maxCNT'));
        if( $('.basketOfferItemHidden').length > 0 ){
            var cnt = 0;
            $('.basketOfferItemHidden').each(function(){ cnt++;
                if( cnt <= maxCNT ){
                    $(this).removeClass('basketOfferItemHidden');
                }
            })
        }
        if( $('.basketOfferItemHidden').length == 0 ){
            $(button).remove();
        } else {
            $(button).html('<span>Показать еще ('+$('.basketOfferItemHidden').length+')</span>');
        }
    })

    // Смена сортировки  предложений дилеров в корзине
    $(document).on('click', '.offersSortButton', function(e){
        var button = $(this);
        if (!is_process(button)){
            var sort = $(button).attr('sort');
            var order = $(button).attr('order');
            order = order=='ASC'?'DESC':'ASC';
            var postParams = {
                action: "updateBasketSortSession",
                sort:sort,
                order:order
            };
            process(true);
            $.post("/ajax/ajax.php", postParams, function(data){
                if (data.status == 'ok'){
                    reloadBasket();
                }
            }, 'json')
                .fail(function(data, status, xhr){
                    process(false);
                })
                .always(function(data, status, xhr){})
        }
    })



    // Переход к заказу
    $(document).on('click', '.toOrderButton', function(e){
        var button = $(this);
        if (!is_process(button)){

            var error = false;
            var offerBlock = $(button).parents('div.basketOfferItem');
            $(offerBlock).find('.error___td').html('');

            var dealer_id = $(offerBlock).attr('dealer_id');

            if( $(offerBlock).find('.offerPVZBlock').length > 0 ){
                var pvz = $(offerBlock).find('select[name=pvz_'+dealer_id+']').val();

                if( pvz == 'empty' ){
                    error = 'Выберите пункт выдачи для дилера';
                }
            }

            if( !error ){

                $('.basketForm input[name=dealer_id]').val(dealer_id);
                if( $(offerBlock).find('.offerPVZBlock').length > 0 ) {
                    $('.basketForm input[name=pvz]').val(pvz);
                }

                ym(55379437, 'reachGoal', 'clickCheckout');

                if( isMobApp == 'Y' ){
                    $('.basketForm').attr('action', '/knauf_app/order/');
                } else {
                    $('.basketForm').attr('action', '/order/');
                }

                $('.basketForm').attr('method', 'POST');
                $('.basketForm').attr('onsubmit', 'return true;');
                $('.basketForm').submit();

            } else {
                $(offerBlock).find('.error___td').html(error);
            }

        }
    });









    // ОФОРМЛЕНИЕ ЗАКАЗА
    $(document).on('click', '.orderButton', function(e){
        var button = $(this);
        if (!is_process(button)){
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input[type=text], input[type=email], input[type=password], textarea").removeClass("is___error");
            $(this_form).find('p.error___p').remove();
            // Задаём переменные
            var delivType = $(this_form).find("input[name=delivType]").val();

            var last_name = $(this_form).find("input[name=last_name]").val();
            var name = $(this_form).find("input[name=name]").val();
            var birthday = $(this_form).find("input[name=birthday]").val();
            var gender = $(this_form).find("input[name=gender]:checked").val();
            var phone = $(this_form).find("input[name=phone]").val();
            var email = $(this_form).find("input[name=email]").val();
            var password = $(this_form).find("input[name=password]").val();

            var coords = $(this_form).find("input[name=coords]").val();
            if( coords != undefined && coords.length == 0 ){
                $(this_form).find("input[name=address]").val('');
            }
            var address = $(this_form).find("input[name=address]").val();

            var lift_cnt = $(this_form).find("input[name=lift]:checked").length;

            var deliveryDate = $(this_form).find("input[name=deliveryDate]").val();
            var deliveryTime = $(this_form).find("select[name=deliveryTime]").val();

            //var ps_id = $(this_form).find("input[name=ps]:checked").val();

            var agree = $(this_form).find("input[name=agree]:checked").val();
            var policy = $(this_form).find("input[name=policy]:checked").val();

            var error = false;

            // Проверки
            if (last_name.length == 0){
                error = ['last_name',   'Введите, пожалуйста, Вашу фамилию!'];
            } else if (name.length == 0){
                error = ['name',   'Введите, пожалуйста, Ваше имя!'];
            } else if (phone.length == 0){
                error = ['phone',   'Введите, пожалуйста, Ваш телефон!'];
            } else if (phone.length > 0 && !(/^[0-9()+ -]{1,99}$/.test(phone))){
                error = ['phone',   'В номере телефона допускаются только цифры, а также символы "+", "-", пробел и круглые скобки!'];
            } else if (email.length == 0){
                error = ['email',   'Введите, пожалуйста, Ваш адрес эл. почты!'];
            } else if (email.length > 0 && !(/^.+@.+\..+$/.test(email))){
                error = ['email',   'Эл. почта содержит ошибки!'];
                /*} else if(
                    delivType == 'delivery'
                    &&
                    address.length == 0
                ){
                    error = ['address',   'Укажите, пожалуйста, Ваш адрес!'];*/
                /*} else if(
                    delivType == 'delivery'
                    &&
                    lift_cnt == 0
                ){
                    error = ['lift',   'Укажите, пожалуйста, наличие лифта!'];*/
                /*} else if(
                    delivType == 'delivery'
                    &&
                    deliveryDate.length == 0
                ){
                    error = ['deliveryDate', 'Укажите, пожалуйста, дату доставки!'];*/
                // } else if(
                //     delivType == 'delivery'
                //     &&
                //     deliveryTime == 'empty'
                // ){
                //     error = [false, 'Укажите, пожалуйста, время доставки!'];
                // } else if ( $(this_form).find("input[name=ps]:checked").length == 0 ) {
                //error = [false, 'Выберите, пожалуйста, вариант оплаты!'];
            } else if( agree != 'Y' ){
                error = [false, 'Отметьте, пожалуйста, согласие с пользовательским соглашением'];
            } else if( policy != 'Y' ){
                error = [false, 'Отметьте, пожалуйста, согласие с политикой конфиденциальности'];
            }
            // Если нет ошибок
            if (!error){

                // if( $(button).hasClass('notFull') ){
                //
                //     var n = noty({
                //         text        : '<strong>Внимание!<br>По некоторым позициям корзины заказа у&nbsp;дилера недостаточно либо нет товаров в наличии.<br><br>Вы действительно хотите оформить заказ?</strong>',
                //         type        : 'alert',
                //         dismissQueue: true,
                //         layout      : 'center',
                //         theme       : 'defaultTheme',
                //         buttons     : [
                //             {addClass: 'btn btn-primary', text: 'Да', onClick: function ($noty) {
                //                 $noty.close();
                //                 order_send ( this_form );
                //             }},
                //             {addClass: 'btn btn-danger', text: 'Нет', onClick: function ($noty) { $noty.close(); } }
                //         ]
                //     });
                //
                // } else {

                order_send ( this_form );

                //}

                // Ошибка
            } else {

                $(this_form).find('.before_error___p').before('<p class="error___p" ></p>');
                show_error(this_form, error[0], error[1]);
            }
        }
    });


    function order_send ( this_form ){
        process(true);
        var form_data = $(this_form).serialize();

        $.post("/ajax/ajax.php", {action:"order", form_data:form_data, app:$("body").data().app}, function(data){
            if (data) {
                if (data.status == 'ok') {

                    var base_domain = $('input[name=base_domain]').val();
                    $.cookie('BITRIX_SM2_under_order', '', {
                        expires: 30, path: '/', domain: '.'+base_domain
                    });

                    var last_name = $(this_form).find("input[name=last_name]").val();
                    var name = $(this_form).find("input[name=name]").val();
                    var phone = $(this_form).find("input[name=phone]").val();
                    var email = $(this_form).find("input[name=email]").val();
                    SendLead(name+" "+last_name, email, phone, "Заказ", data.products, "", data.order_id, data.dealer_id+"|"+data.dealer_name, data.total);

                    $(this_form).append('<input type="hidden" name="order_id" value="'+data.order_id+'">');
                    $(this_form).append('<input type="hidden" name="ps_id" value="'+data.ps_id+'">');
                    $(this_form).attr('onsubmit', 'return true');

                    $.cookie(
                        'BITRIX_SM2_systems_data',
                        "",
                        {
                            expires: 365,
                            path: '/',
                            domain: '.'+base_domain
                        }
                    );

                    $(this_form).submit();

                } else if (data.status == 'need_auth') {

                    if( $(this_form).find('.order_pass___block').length == 0 ){
                        $(this_form).find('.order_email___block').after('<div class="account-form__field order_pass___block"><span>Пароль *</span><input type="password" name="password" value="" readonly onfocus="this.removeAttribute(\'readonly\')"></div>');
                    }
                    $(this_form).find('.order_pass___block input[name=password]').val('');
                    $(this_form).find('.before_error___p').before('<p class="error___p" ></p>');
                    show_error(this_form, 'password', 'Такой адрес электронной почты уже зарегистрирован на сайте<br>Введите, пожалуйста, пароль для авторизации!');
                    process(false);

                } else if (data.status == 'wrong_password') {

                    if( $(this_form).find('.order_pass___block').length == 0 ){
                        $(this_form).find('.order_email___block').after('<div class="account-form__field order_pass___block"><span>Пароль *</span><input type="password" name="password" value=""></div>');
                    }
                    $(this_form).find('.order_pass___block input[name=password]').val('');
                    $(this_form).find('.before_error___p').before('<p class="error___p" ></p>');
                    show_error(this_form, 'password', 'Пароль не верный');
                    process(false);

                } else if (data.status == 'error') {
                    $(this_form).find('.before_error___p').before('<p class="error___p" ></p>');
                    show_error(this_form, false, data.text);
                    process(false);
                }
            }
        }, 'json')
            .fail(function(data, status, xhr){
                show_error(this_form, false, 'Ошибка запроса...');
                process(false);
            })
            .always(function(data, status, xhr){})
    }








    $(document).on('change', 'input[name=order_register], input[name=new_user]', function(e){
        reloadOrder();
    })


    // Авторизация в заказе
    $(document).on('click', '.orderAuthButton', function(e){
        var button = $(this);
        if (!is_process(button)){
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var login = $(this_form).find("input.order_login").val();
            var password = $(this_form).find("input.order_password").val();
            var error = false;
            // Проверки
            if (login.length == 0){
                error = ['login',   'Введите, пожалуйста, Ваш адрес электронной почты/логин!'];
            } else if (password.length == 0){
                error = ['password',   'Введите, пожалуйста, Ваш пароль!'];
            }
            // Если нет ошибок
            if (!error){
                process(true);
                $.post("/ajax/ajax.php", {action:"auth", login:login, password:password, is_order_auth:'Y', form_data:$(this_form).serialize()}, function(data){
                    if ( data.status == 'ok' || data.status == 'auth' ){
                        $(this_form).attr('action', '');
                        $(this_form).attr('onsubmit', '');
                        $(this_form).submit();
                    } else if (data.status == 'error'){
                        show_error(this_form, false, data.text);
                    }
                }, 'json')
                    .fail(function(data, status, xhr){
                        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'topRight', type: 'warning', text: "Ошибка запроса"});
                    })
                    .always(function(data, status, xhr){
                        process(false);
                    })
                // Ошибка
            } else {
                show_error(this_form, error[0], error[1]);
            }
        }
    })



    // Повтор заказа
    $(document).on('click', '.repeatOrderButton', function(e){
        var button = $(this);
        if (!is_process(button)){
            var order_id = $(button).attr('order_id');

            $.noty.closeAll();

            var n = noty({
                text        : 'Повторить заказ №'+order_id+'<br>точь-в-точь?',
                type        : 'alert',
                dismissQueue: true,
                layout      : 'center',
                theme       : 'defaultTheme',
                buttons     : [
                    {addClass: 'btn btn-primary', text: 'Да', onClick: function ($noty) {
                            $noty.close();

                            var postParams = {
                                action: "repeat_order",
                                order_id: order_id
                            };
                            postParams['app'] = $("body").data().app;
                            process(true);
                            $.post("/ajax/ajax.php", postParams, function(data){
                                if (data.status == 'ok'){

                                    if( isMobApp == 'Y' ){
                                        create_form( 'success_form', 'post', '/knauf_app/order_success/' );
                                    } else {
                                        create_form( 'success_form', 'post', '/order_success/' );
                                    }

                                    $('.success_form').append('<input type="hidden" name="order_id" value="'+data.order_id+'">');
                                    $('.success_form').append('<input type="hidden" name="ps_id" value="'+data.ps_id+'">');
                                    $('.success_form').attr('onsubmit', 'return true');
                                    $('.success_form').submit();

                                } else if (data.status == 'error'){
                                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: data.text});
                                }
                            }, 'json')
                                .fail(function(data, status, xhr){
                                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
                                })
                                .always(function(data, status, xhr){
                                    process(false);
                                })

                        }},
                    {addClass: 'btn btn-danger', text: 'Нет', onClick: function ($noty) { $noty.close(); } }
                ]
            });
        }
    })





    // Переключение внешнего вида каталога
    $(document).on('click', '.view-switcher__button--list', function() {
        if( !$('section.searchBlock').hasClass('listView') ) {
            $('.searchBlock').addClass('listView');

            $.post("/ajax/ajax.php", {action: "setCatalogView", catalogView: 'list'}, function (data) {}, 'json');

            ym(55379437, 'reachGoal', 'changeGrid');
        }
    });
    $(document).on('click', '.view-switcher__button--grid', function() {
        if( $('section.searchBlock').hasClass('listView') ) {
            $('.searchBlock').removeClass('listView');

            $.post("/ajax/ajax.php", {action: "setCatalogView", catalogView: 'grid'}, function (data) {}, 'json');

            ym(55379437, 'reachGoal', 'changeList');
        }
    });




    // ФИЛЬТР - чекбокс
    $(document).on('change', '.filter___checkbox', function(e){
        var input = $(this);
        /////////////////////////////
        clearInterval(filterInterval);
        filterInterval = setInterval(function(){
            clearInterval(filterInterval);
            filterGo();
        }, 500);
    })


    $(document).on('keyup', '.left___q, .filter_number_input', function(e){
        var input = $(this);
        /////////////////////////////
        clearInterval(filterInterval);
        filterInterval = setInterval(function(){
            clearInterval(filterInterval);
            filterGo();
        }, 700);
    })

    // Сброс фильтра
    $(document).on('click', '.resetFilterButton', function(e){
        if ( !is_process(this) ){
            $('.filter_number_input').each(function(){
                $(this).val( $(this).attr('default_value') );
                $(this).parents('.filterNumberBlock').find(".priceSlider__bar").slider("values", $(this).data("index"), $(this).val());
            })
            $('.filter___checkbox').prop('checked', false);
            //$('.showSubMenu').removeClass('showSubMenu');
            filterGo();
        }
    })



    // Подгрузка товаров каталога
    $(document).on('click', '.catalog_load_button', function(){
        if (
            !is_process(this)
            &&
            $('.catalog___item').length > 0
        ){
            var params = {};
            params.items = [];
            $('.catalog___item').each(function(){
                var item_id = $(this).attr('item_id');
                params.items.push(item_id);
            })
            if( $('input[name=search_results]').length > 0 ){
                var search_results = $('input[name=search_results]').val();
                params['search_results'] = search_results.split('|');
            }
            catalogLoad( params );
        }
    });







    // Отправка товара под заказ
    $(document).on('click', '.underOrderSend', function(e){
        var button = $(this);
        if (!is_process(button)){
            // Форма
            var this_form = $(this).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var fio = $(this_form).find("input[name=fio]").val();
            var phone = $(this_form).find("input[name=phone]").val();
            var email = $(this_form).find("input[name=email]").val();
            var city = $(this_form).find("input[name=city]").val();
            var number = $(this_form).find("input[name=number]").val();
            var agree = $(this_form).find("input[name=agree]:checked").length>0;
            var agree1 = $(this_form).find("input[name=agree1]:checked").length>0;
            var agree2 = $(this_form).find("input[name=agree2]:checked").length>0;
            var error = false;
            // Проверки
            if (fio.length == 0){
                error = ['fio',   'Введите, пожалуйста, Ваше ФИО'];
            } else if (email.length == 0){
                error = ['email',   'Введите, пожалуйста, Ваш адрес электронной почты!'];
            } else if (email.length > 0 && !(/^.+@.+\..+$/.test(email))){
                error = ['email',   'Эл. почта содержит ошибки!'];
            } else if (phone.length == 0){
                error = ['phone',   'Введите, пожалуйста, Ваш телефон!'];
            } else if (phone.length > 0 && !(/^[0-9()+ -]{1,99}$/.test(phone))){
                error = ['phone',   'В номере телефона допускаются только цифры, а также символы "+", "-", пробел и круглые скобки!'];
            } else if (number.length == 0){
                error = ['number',   'Введите, пожалуйста, количество товара'];
            } else if (city.length == 0){
                error = ['city',   'Введите, пожалуйста, Ваш город!'];
            } else if( !agree ){
                error = [false,   'Отметьте, пожалуйста, согласие на обработку персональных данных'];
            }
            else if( !agree1 ){
                error = [false,   'Примите, пожалуйста, условия Пользовательского соглашения'];
            }
            /*else if( !agree2 ){
                error = [false,   'Отметьте, пожалуйста, согласие на рассылку информационных материалов'];
            }*/
            // Если нет ошибок
            if( !error ){
                process(true);
                $.post("/ajax/underOrderSend.php", {form_data:$(this_form).serialize()}, function(data){
                    if ( data.status == 'ok'  ){
                        $('.fancybox-close:visible').trigger('click');
                        var n = noty({
                            closeWith: ['click'],
                            timeout: false,
                            layout: 'center',
                            theme: 'fast-order-success-popup',
                            type: 'success',
                            text: 'Спасибо, что оставили заявку на товар под заказ. В данный момент товара нет ' +
                            'в наличии на Маркетплейс ни у одного из наших партнеров в данном регионе. Мы уточним ' +
                            'возможность поставки товара под заказ и сообщим вам о результатах. Сроки доставки ' +
                            'товара под заказ могут составлять более двух недель. К сожалению, мы не можем ' +
                            'гарантировать вам поставку данного товара.'
                        });
                        $(this_form).find("input[type=text], input[type=tel], input[type=email]").val("");
                        $(this_form).find("input[type=checkbox]").prop("checked", false);

                        if($("#system_complects_page").length)
                        {
                            ym(55379437, 'reachGoal', 'systemsPageSendFormUnderOrder', {URL: document.location.href});
                        }
                        else {
                            ym(55379437, 'reachGoal', 'SendFormUnderOrder', {URL: document.location.href});
                        }

                        SendLead(fio, email, phone, "Под заказ", data.products, "", data.res, "", data.price);

                    } else if (data.status == 'error'){
                        show_error(this_form, false, data.text);
                    }
                }, 'json')
                    .fail(function(data, status, xhr){
                        show_error(this_form, false, "Ошибка запроса");
                    })
                    .always(function(data, status, xhr){
                        process(false);
                    })
                // Ошибка
            } else {
                show_error(this_form, error[0], error[1]);
            }
        }
    })



    // Товар под заказ
    $(document).on('click', '.underOrderButton', function(e){
        var button = $(this);
        var fl=0;
        if (!is_process(button)){
            var curItems = [];
            var cookieItems = $.cookie('BITRIX_SM2_under_order');
            if( cookieItems != undefined && cookieItems.length > 0 ){
                curItems = cookieItems.split('|');
            }
            var item_id = $(this).attr('item_id');
            var cnt = $(this).attr('item_cnt');
            // заказ одного товара - показываем поле количество и данные этого товара
            if(typeof cnt == "undefined")
            {
                var kratnost = $(this).data("kratnost");

                if(typeof kratnost !='undefined') {
                    kratnost = parseInt(kratnost);
                    if(kratnost>0) {
                        cnt = kratnost;
                        fl=1;
                        $(".s-p0").show();
                    }

                }
                if(fl === 0) {
                    cnt = 1;
                }
                var product_dl = $(this).data("dl");
                var product_sh = $(this).data("sh");

                product_dl =parseInt(product_dl);
                product_sh =parseInt(product_sh);


                $('.underOrderModalForm input[name=number]').parents(".account-form__row").show();
                $('.underOrderModalForm input[name=number]').val(cnt);

                var prod_name = $(this).data("name");
                var prod_url = $(this).data("url");
                var prod_pic = $(this).data("pic");



                if(typeof prod_name != "undefined"
                    && typeof prod_url != "undefined"
                    && typeof prod_pic != "undefined")
                {
                    $('.underOrderModalForm .modal-qOrder__product .modal-qOrder__product-title').text(prod_name);
                    $('.underOrderModalForm .modal-qOrder__product .modal-qOrder__product-img').attr("href", prod_url);
                    $('.underOrderModalForm .modal-qOrder__product img').attr("src", prod_pic);

                    $('.underOrderModalForm .modal-qOrder__product').show();
                }
                else
                {
                    $('.underOrderModalForm .modal-qOrder__product').hide();
                }

                var size_one = $(this).data("size_one");
                var size_name = $(this).data("size_name");
                if(typeof size_one != "undefined" && size_one
                    && typeof size_name != "undefined" && size_name)
                {
                    $('.underOrderModalForm input[name=meters]').parents(".account-form__field").find("span").replaceWith(size_name);

                    $('.underOrderModalForm input[name=meters]').val(size_one);
                    $('.underOrderModalForm input[name=meters]').data("size_one", size_one);

                    $('.underOrderModalForm input[name=meters]').parents(".account-form__field").show();
                }
                else
                {
                    $('.underOrderModalForm input[name=meters]').parents(".account-form__field").hide();
                }
            }
            // заказ нескольких товаров - скрываем количество, скрываем данные товара
            else
            {
                $('.underOrderModalForm input[name=number]').parents(".account-form__row").hide();
                $('.underOrderModalForm .modal-qOrder__product').hide();
            }

            $('.underOrderModalForm input[name=product_id]').val(item_id);
            $('.underOrderModalForm input[name=product_cnt]').val(cnt);

            $('.underOrderModalForm input[name=number]').val(cnt);

            $('.underOrderModalForm input[name=product_dl]').val(product_dl);
            $('.underOrderModalForm input[name=product_sh]').val(product_sh);
            if(fl===1) {
                var pl = (product_dl/1000)*(product_sh/1000)*cnt;

                pl = pl.toFixed(2);
                $('.underOrderModalForm input[name=meters]').attr("readonly", "readonly").val(pl);
                $('.underOrderModalForm input[name=number]').attr("readonly", "readonly");
            }

            $('.underOrderModal__link').fancybox({padding: 0,wrapCSS: 'fancybox-review'}).trigger('click');

            // if( curItems.indexOf(item_id) == -1 ){
            //     curItems.push(item_id);number
            // }
            // var base_domain = $('input[name=base_domain]').val();
            // $.cookie('BITRIX_SM2_under_order', curItems.join('|'), {
            //     expires: 30, path: '/', domain: '.'+base_domain
            // });
            // $.noty.closeAll();
            // var n = noty({
            //     closeWith: ['click'],
            //     timeout: 50000,
            //     layout: 'topRight',
            //     type: 'success',
            //     text: "Товар добавлен к заявке!"
            // });

            if($(this).hasClass('product-item__cartLink')){
                if($("#system_complects_page").length)
                {
                    ym(55379437, 'reachGoal', 'systemsPageUnderOrderCatalog', {URL: document.location.href});
                }
                else {
                    var link = window.location.origin + $(this).parent().find('.product-item__titleProduct').attr('href');
                    ym(55379437, 'reachGoal', 'UnderOrderCatalog', {URL: link});
                }
            }
            else{
                if($("#system_complects_page").length)
                {
                    ym(55379437, 'reachGoal', 'systemsPageUnderOrderProductPage', {URL: document.location.href});
                }
                else {
                    ym(55379437, 'reachGoal', 'UnderOrderProductPage', {URL: document.location.href});
                }
            }

        }
    });

    function UpdateFastOrderTotal(cnt) {
        var $fastOrderTotal = $(".fastOrderPrice");
        $fastOrderTotal.each(function () {

            var $priceWrap = $(this);
            var priceOne = parseFloat($priceWrap.data("price"));

            var priceTotal = priceOne * cnt;

            $priceWrap.text(priceTotal.toFixed(2));

            // ещё выведем предупреждения о нехватке товара у дилера
            var $dealer_wrap = $(this).parents(".modal-qOrder__dealer");
            var dealer_quantity = parseInt($dealer_wrap.data("quantity"));

            if(cnt > dealer_quantity)
            {
                $dealer_wrap.find(".quantity_warning").text("Недостаточно товара (доступно "+dealer_quantity+" из "+cnt+")").show();
            }
            else
            {
                $dealer_wrap.find(".quantity_warning").text("").hide();
            }
        });
    }

    // пересчёт количества в метры и обратно для попапов Под заказ, быстрый заказ
    $(document).on("change", '.underOrderModalForm input[name=number], .quickOrderForm input[name=number]', function () {
        var $cnt = $(this);
        var $size = $cnt.parents("form").find("input[name=meters]");
        var size_one = parseFloat($size.data("size_one"));
        var cnt = parseFloat($cnt.val());
        var size;
        if(!cnt)
        {
            size = size_one;
            $cnt.val(1);

            UpdateFastOrderTotal(1);
        }
        else {
            size = cnt * size_one;

            UpdateFastOrderTotal(cnt);
        }

        $size.val(size.toFixed(2));

    });
    $(document).on("change", '.underOrderModalForm input[name=meters], .quickOrderForm input[name=meters]', function () {
        var $size = $(this);
        var $cnt = $size.parents("form").find("input[name=number]");
        var size_one = parseFloat($size.data("size_one"));
        var size = parseFloat($size.val());
        var cnt;

        if(!size)
        {
            cnt = 1;
            $size.val(size_one);
        }
        else {
            cnt = Math.ceil(size / size_one);
        }

        $cnt.val(cnt);
        UpdateFastOrderTotal(cnt);
    });


    // Открытие модального окна Быстрый заказ
    $(document).on('click', '.openQuickOrderModal', function(e){
        var button = $(this);
        if (!is_process(button)){
            var item_id = $(this).attr('item_id');
            var dealer_ids = $(this).attr('dealer_ids');
            var postParams = {
                item_id: item_id,
                dealer_ids: dealer_ids,
                quantity:$(".product__form [name=basketCnt]").val()
            };
            process(true);

            postParams['app'] = $("body").data().app;

            $.post("/ajax/openQuickOrderModal.php", postParams, function(data){
                if (data.status == 'ok'){
                    $('.quickOrderForm').replaceWith( data.html );
                    $('#quickOrderFormLink').fancybox({
                        padding: 0,
                        wrapCSS: 'fancybox-review'
                    }).trigger('click');
                    //createStyledSelects();
                    //var t = "";
                    //initOpenCloseDropdown(t);

                    UpdateFastOrderTotal(parseInt($('.quickOrderForm [name=number]').val()));

                    if($("#system_complects_page").length)
                    {
                        ym(55379437, 'reachGoal', 'systemsPageFastOrder', {URL: document.location.href});
                    }
                    else
                    {
                        ym(55379437, 'reachGoal', 'FastOrder', {URL: document.location.href});
                    }

                } else if (data.status == 'error'){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: data.text});
                }
            }, 'json')
                .fail(function(data, status, xhr){
                    var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
                })
                .always(function(data, status, xhr){
                    process(false);
                })
        }
    });

    // Отправка Быстрого заказа
    $(document).on('click', '.quickOrderButton', function(e){
        var button = $(this);

        if (!is_process(button)){

            // Форма
            var this_form = $(this).parents('form');
            var dealer_id = button.data("id");
            if(!dealer_id)
            {
                show_error(this_form, "dealer", "Ошибка. Не удалось определить ID дилера.");
            }
            else {
                $(this_form).find("[name=dealer]").val(dealer_id);

                if (!is_process(button)) {
                    // Подготовка
                    $(this_form).find("input, textarea").removeClass("is___error");
                    $(this_form).find('p.error___p').html('');
                    // Задаём переменные
                    var dealer = $(this_form).find("[name=dealer]").val();
                    var fio = $(this_form).find("input[name=fio]").val();
                    var phone = $(this_form).find("input[name=phone]").val();
                    var email = $(this_form).find("input[name=email]").val();
                    var city = $(this_form).find("input[name=city]").val();
                    var number = $(this_form).find("input[name=number]").val();
                    var agree = $(this_form).find("input[name=agree]:checked").length > 0;
                    var agree1 = $(this_form).find("input[name=agree1]:checked").length > 0;
                    var agree2 = $(this_form).find("input[name=agree2]:checked").length > 0;
                    var error = false;
                    // Проверки
                    if (dealer == 'empty' || !dealer) {
                        error = ['dealer', 'Выберите, пожалуйста, дилера для заказа!'];
                    } else if (city.length == 0) {
                        error = ['city', 'Введите, пожалуйста, Ваш город!'];
                    } else if (fio.length == 0) {
                        error = ['fio', 'Введите, пожалуйста, Ваше ФИО'];
                    } else if (number.length == 0) {
                        error = ['number', 'Введите, пожалуйста, количество товара'];
                    } else if (email.length == 0) {
                        error = ['email', 'Введите, пожалуйста, Ваш адрес электронной почты!'];
                    } else if (email.length > 0 && !(/^.+@.+\..+$/.test(email))) {
                        error = ['email', 'Эл. почта содержит ошибки!'];
                    } else if (phone.length == 0) {
                        error = ['phone', 'Введите, пожалуйста, Ваш  телефон!'];
                    } else if (phone.length > 0 && !(/^[0-9()+ -]{1,99}$/.test(phone))) {
                        error = ['phone', 'В номере телефона допускаются только цифры, а также символы "+", "-", пробел и круглые скобки!'];
                    } else if (!agree) {
                        error = [false, 'Отметьте, пожалуйста, согласие на обработку персональных данных'];
                    } else if (!agree1) {
                        error = [false, 'Примите, пожалуйста, условия Пользовательского соглашения'];
                    }
                    /*else if( !agree2 ){
                        error = [false,   'Отметьте, пожалуйста, согласие на рассылку информационных материалов'];
                    }*/

                    // Если нет ошибок
                    if (!error) {

                        process(true);
                        
                        $.post("/ajax/quickOrder.php", {form_data: $(this_form).serialize()}, function (data) {
                            if (data.status == 'ok') {
                                $('.fancybox-close:visible').trigger('click');
                                var n = noty({
                                    closeWith: ['hover'],
                                    timeout: 5000,
                                    layout: 'center',
                                    type: 'success',
                                    text: 'Спасибо, мы свяжемся с Вами для уточнения деталей заказа!'
                                });
                                $(this_form).find("input[type=text], input[type=tel], input[type=email]").val("");
                                $(this_form).find("input[type=checkbox]").prop("checked", false);
                                $(this_form).find("select option").prop("selected", false);

                                if($("#system_complects_page").length)
                                {
                                    ym(55379437, 'reachGoal', 'systemsPageSendFormFastOrder', {URL: document.location.href});
                                }
                                else {
                                    ym(55379437, 'reachGoal', 'SendFormFastOrder', {URL: document.location.href});
                                }

                                SendLead(fio, email, phone, "Быстрый заказ", data.products, "", data.order_id, data.dealer_id+"|"+data.dealer_name, data.total);

                            } else if (data.status == 'error') {
                                show_error(this_form, false, data.text);
                            }
                        }, 'json')
                            .fail(function (data, status, xhr) {
                                show_error(this_form, false, "Ошибка запроса");
                            })
                            .always(function (data, status, xhr) {
                                process(false);
                            })
                        // Ошибка
                    } else {
                        show_error(this_form, error[0], error[1]);
                    }
                }
            }
        }
    });


});
function spminus(eto){
    var kratnost = $(eto).parents("form:first").find("input[name='product_cnt']").val();

    var product_dl = $(eto).parents("form:first").find("input[name='product_dl']").val();
    var product_sh = $(eto).parents("form:first").find("input[name='product_sh']").val();
    product_dl =parseInt(product_dl);
    product_sh =parseInt(product_sh);

    var cnt = $(eto).parents("form:first").find("input[name='number']").val();
    cnt = parseInt(cnt);
    var fl=0;
    if(typeof kratnost !='undefined') {
        kratnost = parseInt(kratnost);
        if(kratnost>0) {
            fl=1;
        }

    }

    if(fl === 0) {
        cnt = cnt-1;
        if(cnt<1) {
            cnt=1;
        }
    }else{
        cnt = cnt - kratnost;
        if(cnt<kratnost) {
            cnt=kratnost;
        }
    }
    var pl = (product_dl/1000)*(product_sh/1000)*cnt;
    pl = pl.toFixed(2);
    $(eto).parents("form:first").find("input[name='meters']").val(pl);

    $(eto).parents("form:first").find("input[name='number']").val(cnt);
    return false;
}

function spplus(eto){
    var kratnost = $(eto).parents("form:first").find("input[name='product_cnt']").val();

    var product_dl = $(eto).parents("form:first").find("input[name='product_dl']").val();
    var product_sh = $(eto).parents("form:first").find("input[name='product_sh']").val();
    product_dl =parseInt(product_dl);
    product_sh =parseInt(product_sh);

    var cnt = $(eto).parents("form:first").find("input[name='number']").val();
    cnt = parseInt(cnt);
    var fl=0;
    if(typeof kratnost !='undefined') {
        kratnost = parseInt(kratnost);
        if(kratnost>0) {
            fl=1;
        }

    }


    if(fl === 0) {
        cnt = cnt+1;
    }else{
        cnt = cnt + kratnost;
    }


    var pl = (product_dl/1000)*(product_sh/1000)*cnt;
    pl = pl.toFixed(2);

    $(eto).parents("form:first").find("input[name='meters']").val(pl);

    $(eto).parents("form:first").find("input[name='number']").val(cnt);

    return false;
}
