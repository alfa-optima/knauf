<? require $_SERVER['DOCUMENT_ROOT']."/local/php_interface/traits/trait___files.php";
use AOptima\Project as project;   use AOptima\Tools as tools;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;


//$arURL = explode("/", $_SERVER["REQUEST_URI"]);
//$stop_items = [
//    'boot.ini', 'passwd', 'win.ini',
//    'xxe_test',  'xxe.txt', 'script',
//    'onload', 'lil9j', 'pwn3d', 'ondxh'
//];
//foreach ( $stop_items as $key => $value ){
//    $stop = false;
//    if( substr_count( strtolower($_SERVER['REQUEST_URI']), strtolower($value) ) ){
//        $stop = true;
//    }
//    if(
//        !$stop
//        &&
//        $arURL[1] == 'catalog'
//        &&
//        count(
//            array_intersect(
//                array_keys($_GET),
//                [
//                    'city', 'product_id', 'agree',
//                    'number', 'email', 'loc_region',
//                    'number', 'email', 'loc_region',
//                    'request_scheme', 'loc_city', 'loc_city_id',
//                    'locCity', 'base_domain', 'section_id',
//                    'cur_domain', 'loc_country', 'curURL'
//                ]
//            )
//        ) > 0
//    ){   $stop = true;   }
//    if( $stop ){
////            $file_path = __DIR__."/ips.txt";
////            $file = fopen($file_path, "a");
////            $res = fwrite($file,  $_SERVER['REMOTE_ADDR']."\n");
////            fclose($file);
//        die();
//    }
//}



if(
    \Bitrix\Main\Loader::includeModule('aoptima.project')
    &&
    \Bitrix\Main\Loader::includeModule('aoptima.tools')
){



    //project\my_ddos_blocking::start();



    global $KNAUF_LOC;

    // Инфо о местоположении пользователя
    if($_SERVER['SERVER_NAME']=='knauf.local') {
        /* ЛОкальная заглушка */
        $KNAUF_LOC=array(
            'CITY' => "Краснодар",
            'CITY_LOC_ID' => 1132
        );
    }else {
        $KNAUF_LOC = project\bx_location::getKnaufLoc();
    }



    // определим СД по местоположению пользователя
    global $KNAUF_SD;
    $KNAUF_SD = project\bx_location::getSDForLoc($KNAUF_LOC["CITY_LOC_ID"], $KNAUF_LOC["REGION_ID"]);

    // clearOldCaches
    project\geolocation::clearOldCaches();

    if( $_REQUEST["app"] ){
        define("IS_ESHOP", true);
        define("PREFIX", "/knauf_app");
        /*define("SITE_ID", "mb");
        define("SITE_DIR", "/knauf_app/");*/
    } else {
        define("IS_ESHOP", (SITE_ID == "mb"));
        define("PREFIX", (SITE_ID == "mb")?"/knauf_app":"");
    }

}
function delete_dublicat($ms) {
    $fl_ms=0;
    if(isset($ms['ID'])) {
        if(is_array($ms['ID'])) {
            if(count($ms['ID'])>0) {
                $fl_ms=1;
            }
        }
    }
    if($fl_ms === 0) {
        return $ms;
    }else {
        $ms_tmp = Array();
        $ms_out = Array();
        $ms_out['ID'] = Array();
        foreach ($ms['ID'] as $val) {
            $fl_d=0;
            if(in_array($val, $ms_tmp)) {
                $fl_d=1;
            }
            if($fl_d === 0) {
                $ms_out['ID'][] = $val;
                $ms_tmp[]=$val;
            }
        }
        foreach($ms as $key=>$val) {
            if($key=="ID") {
            }else {
                $ms_out[$key] = $val;
            }
        }
        return $ms_out;

    }
}
function delete_dubl_plain_ms($ms) {
    $fl_ms=0;
    if(isset($ms)) {
        if(is_array($ms)) {
            if(count($ms)>0) {
                $fl_ms=1;
            }
        }
    }
    if($fl_ms === 0) {
        return $ms;
    }else {
        $ms_tmp = Array();
        $ms_out = Array();
        foreach ($ms as $val) {
            $fl_d=0;
            if(in_array($val, $ms_tmp)) {
                $fl_d=1;
            }
            if($fl_d === 0) {
                $ms_out[] = $val;
                $ms_tmp[]=$val;
            }
        }
        return $ms_out;

    }
}
function get_ibid_by_tid($tid){
    CModule::IncludeModule('iblock');
    $tovar = CIBlockElement::GetByID($tid);
    $arTovar = $tovar->Fetch();
    return $arTovar['IBLOCK_ID'];
}
function get_tovar_by_tp($tid) {
    $out=0;
    CModule::IncludeModule('iblock');
    CModule::IncludeModule('catalog');
    $mxResult = CCatalogSku::GetProductInfo($tid);
    if (is_array($mxResult)){
        $out=$mxResult['ID'];
    }
    return $out;
}

function add_filter_by_z_zakaz($in_filter, $tid,$fl_adminka){
    $out=Array();
    CModule::IncludeModule('sale');
    //$arFilter = Array("@STATUS_ID" => array("F"), 'PROPERTY_VAL_BY_CODE_STATUS'=>Array("Z"));
    $arFilter = Array('PROPERTY_VAL_BY_CODE_STATUS'=>"Z");
    $select = Array('ID', 'PROPERTY_VAL_BY_CODE_STATUS','USER_ID','PRICE');
    $rsOrder = CSaleOrder::GetList(array('ID' => 'DESC'), $arFilter, false, false,$select);
    while ($arSales = $rsOrder->Fetch()){
     $zz = $arSales['PROPERTY_VAL_BY_CODE_STATUS'];
     /* Список товаров в заказе */
     $dbBasketItems = CSaleBasket::GetList(array(), array("ORDER_ID" => $arSales['ID']), false, false, array());
     $macciw_0 = Array();
     $fl_ok_this = 0;
     while ($arItems = $dbBasketItems->Fetch()) {
         $compare_tid = $arItems['PRODUCT_ID'];
         $ib_id = get_ibid_by_tid($arItems['PRODUCT_ID']);
         if($ib_id == 3) {/* Это торг предложения */
             $compare_tid = get_tovar_by_tp($arItems['PRODUCT_ID']);
         }
         if($compare_tid == $tid) {
             $fl_ok_this = 1;
         }else {
             if($compare_tid>0) {
                 $macciw_0[] = $compare_tid;
             }
         }
     }
     if($fl_ok_this === 1) {/* Этот товар есть в заказе перечисляю остальные*/
         foreach ($macciw_0 as $val) {
                 $out[] = $val;
         }
     }

    }
    if(count($out)>0) {
        $out = delete_dubl_plain_ms($out);
    }
    /* Учитываать или нет админке*/
    $rezult_out = Array("ID"=>Array());
    if($fl_adminka === 0) {
        /* НЕ учитывать беру только массив из заказаов ничего не делаю */
        $rezult_out['ID']= $out;
    }else {
        /* Учитываю . Беру разницу массивов */
        if(isset($in_filter['ID'])) {
            if(is_array($in_filter['ID'])) {
                if(count($in_filter['ID'])>0) {
                    foreach ($in_filter['ID'] as $val) {
                        if(in_array($val, $out)) {
                            $rezult_out['ID'][] = $val;
                        }
                    }
                }
            }
        }

    }


    return $rezult_out;
}
function haAgentSendMailsOrdersNotProcessed(){
    CModule::IncludeModule('sale');
    CModule::IncludeModule('main');

    $arSelect = array('ID', 'DATE_INSERT');
    $arRuntime = array();

    $dbProperties = \Bitrix\Sale\Internals\OrderPropsTable::getList(array('select' => array('CODE'), 'filter' => ['CODE' => ['DEALER_ID', 'STATUS', 'NEW_ORDER_MAIL_NOT_PROCESSED']]));
    while ($arProperty = $dbProperties->fetch()) {
        $sCode = $arProperty['CODE'];
        $arSelect["PROPERTY_{$sCode}_VALUE"] = "PROPERTY_{$sCode}.VALUE";
        $arRuntime["PROPERTY_{$sCode}"] = new \Bitrix\Main\Entity\ReferenceField("PROPERTY_{$sCode}",
            '\Bitrix\Sale\Internals\OrderPropsValueTable', array(
                '=this.ID' => 'ref.ORDER_ID',
                '=ref.CODE' => new \Bitrix\Main\DB\SqlExpression('?', $sCode),
            ));
    }
	$phpDateTime = new DateTime();
	$dateTime = \Bitrix\Main\Type\DateTime::createFromPhp($phpDateTime);
	$dateTime->add('-2 hours')->format('d.m.Y H:i:s');
    $dbOrders = \Bitrix\Sale\Internals\OrderTable::getList(array(
        'order' => array('ID' => 'asc'),
        'filter' => array('!PROPERTY_DEALER_ID_VALUE' => '', 'PROPERTY_STATUS_VALUE' => 'A', '!PROPERTY_NEW_ORDER_MAIL_NOT_PROCESSED' => 'Y', '<DATE_INSERT' => $dateTime),
        'select' => $arSelect,
        'runtime' => $arRuntime,
    ));

    while ($arOrder = $dbOrders->fetch()) {
        
		$user = CUser::GetByID($arOrder['PROPERTY_DEALER_ID_VALUE'])->fetch();
        
		if(CEvent::Send('EVENT_SEND_ORDER_NOT', ['ORDER_ID' => $arOrder['ID'], 'EMAIL_TO' => $user['EMAIL'], 'LINK' => '?orderid='.base64_encode($arOrder['ID']).'&action=prinyat', 'CANCEL' => '?orderid='.base64_encode($arOrder['ID']).'&action=cancel'])){
            $order = Bitrix\Sale\Order::load($arOrder['ID']);
            $propertyCollection = $order->getPropertyCollection();
            $somePropValue = $propertyCollection->getItemByOrderPropertyId(33);
            $somePropValue->setValue('Y');
        }
    }
}

function haAgentSendMailsOrdersDelivery(){
    CModule::IncludeModule('sale');
    CModule::IncludeModule('main');

    $arSelect = array('ID');
    $arRuntime = array();

    $dbProperties = \Bitrix\Sale\Internals\OrderPropsTable::getList(array('select' => array('CODE'), 'filter' => ['CODE' => ['DEALER_ID', 'DELIV_DATE']]));
    while ($arProperty = $dbProperties->fetch()) {
        $sCode = $arProperty['CODE'];
        $arSelect["PROPERTY_{$sCode}_VALUE"] = "PROPERTY_{$sCode}.VALUE";
        $arRuntime["PROPERTY_{$sCode}"] = new \Bitrix\Main\Entity\ReferenceField("PROPERTY_{$sCode}",
            '\Bitrix\Sale\Internals\OrderPropsValueTable', array(
                '=this.ID' => 'ref.ORDER_ID',
                '=ref.CODE' => new \Bitrix\Main\DB\SqlExpression('?', $sCode),
            ));
    }
	$phpDateTime = new DateTime();
	$dateTime = \Bitrix\Main\Type\DateTime::createFromPhp($phpDateTime);
    $dbOrders = \Bitrix\Sale\Internals\OrderTable::getList(array(
        'order' => array('ID' => 'asc'),
        'filter' => array('PROPERTY_DELIV_DATE_VALUE' => $dateTime->format('d.m.Y')),
        'select' => $arSelect,
        'runtime' => $arRuntime,
    ));

    while ($arOrder = $dbOrders->fetch()) {
        $user = CUser::GetByID($arOrder['PROPERTY_DEALER_ID_VALUE'])->fetch();
        CEvent::Send('EVENT_SEND_DELIVERY_TODAY', ['ORDER_ID' => $arOrder['ID'], 'EMAIL_TO' => $user['EMAIL'], 'LINK' => '?orderid='.base64_encode($arOrder['ID']).'&action=vputi']);
    }
}

function haAgentSendMailsOrdersDeliveryYesterday(){
    CModule::IncludeModule('sale');
    CModule::IncludeModule('main');

    $arSelect = array('ID');
    $arRuntime = array();

    $dbProperties = \Bitrix\Sale\Internals\OrderPropsTable::getList(array('select' => array('CODE'), 'filter' => ['CODE' => ['DEALER_ID', 'DELIV_DATE']]));
    while ($arProperty = $dbProperties->fetch()) {
        $sCode = $arProperty['CODE'];
        $arSelect["PROPERTY_{$sCode}_VALUE"] = "PROPERTY_{$sCode}.VALUE";
        $arRuntime["PROPERTY_{$sCode}"] = new \Bitrix\Main\Entity\ReferenceField("PROPERTY_{$sCode}",
            '\Bitrix\Sale\Internals\OrderPropsValueTable', array(
                '=this.ID' => 'ref.ORDER_ID',
                '=ref.CODE' => new \Bitrix\Main\DB\SqlExpression('?', $sCode),
            ));
    }
    $dbOrders = \Bitrix\Sale\Internals\OrderTable::getList(array(
        'order' => array('ID' => 'asc'),
        'filter' => array('PROPERTY_DELIV_DATE_VALUE' => date('d.m.Y', strtotime('yesterday'))),
        'select' => $arSelect,
        'runtime' => $arRuntime,
    ));

    while ($arOrder = $dbOrders->fetch()) {
        $user = CUser::GetByID($arOrder['PROPERTY_DEALER_ID_VALUE'])->fetch();
        CEvent::Send('EVENT_SEND_DELIVERY_YESTERDAY', ['ORDER_ID' => $arOrder['ID'], 'EMAIL_TO' => $user['EMAIL'], 'LINK' => '?orderid='.base64_encode($arOrder['ID']).'&action=dostavlen']);
    }
}



\Bitrix\Main\EventManager::getInstance()->addEventHandler('catalog','\Bitrix\Catalog\Model\Product::OnBeforeUpdate','onBeforeProductIblockSKUUpdate');
\Bitrix\Main\EventManager::getInstance()->addEventHandler('catalog','OnBeforePriceUpdate','onBeforePriceIblockSKUUpdate');

function onBeforeProductIblockSKUUpdate(\Bitrix\Catalog\Model\Event $event){
    
    $result     =   new \Bitrix\Catalog\Model\EventResult();
    $arFields   =   $event->getParameter('fields');
    $id         =   $event->getParameter('primary')['ID'];
    $arCurFields = CCatalogProduct::GetByID($id);
    if($arCurFields['QUANTITY'] != $arFields['QUANTITY']){
        CIBlockElement::SetPropertyValuesEx($id, false, array("UPDATE_DATETIME" => new Bitrix\Main\Type\DateTime()));
    }

   return  $result;
}

function onBeforePriceIblockSKUUpdate($id, $arFields)
{
    CModule::IncludeModule('iblock');
    CIBlockElement::SetPropertyValuesEx($id, false, array("UPDATE_DATETIME" => new Bitrix\Main\Type\DateTime()));
}

\Bitrix\Main\EventManager::getInstance()->addEventHandler("sale", "OnSaleOrderBeforeSaved", "OnOrderSaveCheckStatusChange");

function OnOrderSaveCheckStatusChange($event){
    $arStatuses = [
        'A' => 'новый',
        'B' => 'принят',
        'D' => 'в пути',
        'X' => 'отменен',
        'Z' => 'выполнен'
    ];
    $order = $event->getParameter("ENTITY");
    $orderId = $order->getId();
    CModule::IncludeModule('iblock');
    CModule::IncludeModule('highloadblock');
    $hlblock = HL\HighloadBlockTable::getById(11)->fetch(); // id highload блока\
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entityClass = $entity->getDataClass();
    $propertyCollection = $order->getPropertyCollection();
    $statusProperty = $propertyCollection->getItemByOrderPropertyCode("STATUS")->getValue();
    $res = $entityClass::getList(array(
        'select' => array('UF_STATUS', 'ID'),
        'filter' => array('UF_ORDER_ID' => $orderId)
    ))->fetch();
    if(!$statusProperty)
        return true;
    if(!$res || $isNew == 1){
        $res['ID'] = $entityClass::add([
            'UF_STATUS' => $statusProperty,
            'UF_ORDER_ID' => $orderId,
            'UF_XML_ID' => $orderId
        ])->getId();
    }else{
        if($res['UF_STATUS'] == $statusProperty)
            return true;
    }
    $historyEntityType = 'ORDER';
    $historyType = 'ORDER_COMMENTED';
    \Bitrix\Sale\OrderHistory::addAction(
        $historyEntityType,
        $orderId,
        $historyType,
        $orderId,
        $order,
        ['COMMENTS' => 'Статус заказа изменен на "'.$arStatuses[$statusProperty].'" ('.$statusProperty.')']
    );
    $entityClass::update($res['ID'], [
        'UF_STATUS' => $statusProperty
    ]);
    $order->save();
}
?>