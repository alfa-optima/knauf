<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

global $USER;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
    &&
    strlen($_POST['form_data']) > 0
){

    $arFields = Array();
    parse_str($_POST["form_data"], $arFields);

    $new_subscribe_email = $arFields['email'];

    if( $USER->IsAuthorized() ){

        // Инфо о пользователе
        $user = tools\user::info($USER->GetID());
        $old_subscribe_email = $user['UF_SUBSCRIBE_EMAIL'];

        // Если отличаются
        if(
            strlen($old_subscribe_email) > 0
            &&
            $old_subscribe_email != $new_subscribe_email
        ){
            // Удаляем старую подписку, передав пустой массив рубрик
            $obSubscribe = new project\subscribe();
            $res = $obSubscribe->save( $old_subscribe_email, array() );
        }

        // Сохраняем Email подписки в пользователя
        $obUser = new \CUser;
        $userFields = Array( "UF_SUBSCRIBE_EMAIL" => $new_subscribe_email );
        if( $arFields['subscribeYes'] == 'Y' ){
            $userFields['UF_SUB_RULES_ACCEPT'] = 1;
        }
        $res = $obUser->Update($USER->GetID(), $userFields);
    }


    if( strlen($new_subscribe_email) > 0 ){
        // Сохранение статуса подписки
        $obSubscribe = new project\subscribe();
        $res = $obSubscribe->save(
            $new_subscribe_email,
            is_array($arFields['rubrics'])?$arFields['rubrics']:array()
        );
        if( $res ){
            // Ответ
            echo json_encode(Array("status" => "ok"));
            return;
        }
    } else {
        // Ответ
        echo json_encode(Array("status" => "ok"));
        return;
    }


}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка сохранения статуса подписки"));
return;