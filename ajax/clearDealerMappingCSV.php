<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$user = new project\user();
if( $USER->IsAuthorized() && $user->isDealer() ){

    $obUser = new \CUser();
    $obUser->Update( $USER->GetID(), [ 'UF_ARTICLE_MAPPING' => null ] );

    ob_start();
    	$APPLICATION->IncludeComponent("aoptima:dealer_load_mapping_file_csv", "");
    	$html = ob_get_contents();
    ob_end_clean();

    $arResult = [
        'status' => 'ok',
        'html' => $html
    ];

    echo json_encode( $arResult ); return;

} else {

    $arResult = [
        'status' => 'error',
        'text' => 'Ошибка авторизации',
    ];

    echo json_encode( $arResult ); return;

}

