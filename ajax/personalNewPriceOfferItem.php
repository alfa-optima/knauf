<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $user = new project\user();
    
    if( $USER->IsAuthorized() && $user->isDealer() ){

        $el = tools\el::info($_POST['product_id']);

        ob_start();
            $APPLICATION->IncludeComponent(
                "aoptima:newPriceOfferItem", "",
                array('NUMBER' => $_POST['number'], 'el' => $el)
            );
            $item_html = ob_get_contents();
        ob_end_clean();

        // Ответ
        echo json_encode(Array("status" => "ok", "item_html" => $item_html));
        return;

    } else {
        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;