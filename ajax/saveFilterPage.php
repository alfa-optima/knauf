<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    global $USER;
    if( $USER->IsAuthorized() ){
        $user_groups = \CUser::GetUserGroup($USER->GetID());
        if( array_intersect($user_groups, project\filter_page::$user_groups) ){

            $arFields = [];
            parse_str($_POST["form_data"], $arFields);

            $url = strip_tags(trim($_POST['url']));
            preg_match("/(\/catalog\/.*)/", $url, $matches, PREG_OFFSET_CAPTURE);
            $url = $matches[1][0];

            $section = tools\section::info($_POST['section_id']);

            $obFilterPage = new project\filter_page();

            if( intval($arFields['ID']) > 0 ){

                $res = $obFilterPage->update(
                    intval($arFields['ID']),
                    $url,
                    strip_tags(trim($arFields['UF_H1'])),
                    trim($arFields['UF_TEXT']),
                    strip_tags(trim($arFields['UF_META_TITLE'])),
                    strip_tags(trim($arFields['UF_META_DESCRIPTION'])),
                    strip_tags(trim($arFields['UF_META_KEYWORDS']))
                );

                if( !$res ){
                    // Ответ
                    echo json_encode(["status" => "error", "text" => "Ошибка редактирования SEO-страницы фильтра"]);
                    return;
                }

            } else {

                $res = $obFilterPage->add(
                    $url,
                    strip_tags(trim($arFields['UF_H1'])),
                    trim($arFields['UF_TEXT']),
                    strip_tags(trim($arFields['UF_META_TITLE'])),
                    strip_tags(trim($arFields['UF_META_DESCRIPTION'])),
                    strip_tags(trim($arFields['UF_META_KEYWORDS']))
                );

                if( !$res ){
                    // Ответ
                    echo json_encode(["status" => "error", "text" => "Ошибка создания SEO-страницы фильтра"]);
                    return;
                }
            }

            $filterPage = $obFilterPage->getByUrl( tools\funcs::pureURL( $url ) );

            // Если это SEO-страница фильтра
            if( intval( $filterPage['ID'] ) > 0 ){
                $h1 = $filterPage["UF_H1"];
                $seo_title = $filterPage["UF_META_TITLE"]?$filterPage["UF_META_TITLE"]:$filterPage["UF_H1"];
                $seo_description = $filterPage["UF_META_DESCRIPTION"]?$filterPage["UF_META_DESCRIPTION"]:$filterPage["UF_H1"];
                $seo_keywords = $filterPage["UF_META_KEYWORDS"]?$filterPage["UF_META_KEYWORDS"]:$filterPage["UF_H1"];
                $seo_text = $filterPage["UF_TEXT"];
                // Иначе
            } else {
                $h1 = $section["UF_H1"]?$section["UF_H1"]:$section["NAME"];
                $seo_title = $section["UF_TITLE"]?$section["UF_TITLE"]:$section["NAME"];
                $seo_description = $section["UF_DESCRIPTION"]?$section["UF_DESCRIPTION"]:$section["NAME"];
                $seo_keywords = $section["UF_KEYWORDS"]?$section["UF_KEYWORDS"]:$section["NAME"];
                $seo_text = '';
            }


            // Ответ
            echo json_encode([
                "status" => "ok",
                "h1" => $h1,
                "seo_title" => $seo_title,
                "seo_description" => $seo_description,
                "seo_keywords" => $seo_keywords,
                "seo_text" => $seo_text,
            ]);
            return;

        } else {

            // Ответ
            echo json_encode(["status" => "error", "text" => "Ошибка доступа"]);
            return;
        }

    } else {

        // Ответ
        echo json_encode(["status" => "error", "text" => "Ошибка авторизации"]);
        return;
    }
}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;
