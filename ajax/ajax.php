<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('sale');
\Bitrix\Main\Loader::includeModule('catalog');

use Bitrix\Main,
    Bitrix\Main\Localization\Loc as Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem,
    Bitrix\Sale,
    Bitrix\Sale\Basket,
    Bitrix\Sale\Order,
    Bitrix\Sale\DiscountCouponsManager,
    Bitrix\Main\Context;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$server_name = \Bitrix\Main\Config\Option::get('main', 'server_name');

$action = tools\funcs::param_post("action");



if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){


    if ($action == "no_auth_order"){
        $_SESSION['no_auth_order'] = 'Y';
        echo json_encode( array('status' => 'ok') );
    }






    // Загрузка фото в ЛК
    if ($action == "getPersPhotoBlock"){
		global $USER;
		$userID = $USER->GetID();
		$currentUser = new project\user( $USER->GetID() );
		$userProps = $currentUser->arUser;
		if(strlen($userProps["UF_MAIN_USER"]) > 0){
			$user = new project\user( $userProps["UF_MAIN_USER"] );
			if( $user->isDealer() ){
				$userID = $user->arUser["ID"];
			}
		}

        if ($USER->IsAuthorized()){
            $user = tools\user::info($userID);
            ob_start();
            $APPLICATION->IncludeComponent(
                "aoptima:dealerLKPhotoBlock", "",
                array('USER' => $user)
            );
            $html = ob_get_contents();
            ob_end_clean();
            // Ответ
            echo json_encode( array('status' => 'ok', 'html' => $html) );
        }
    }

    // Удаление аватара дилера
    if ($action == "removeDealerPhoto"){
        if ($USER->IsAuthorized()){

			//Добавляем проверку для подчиненных пользователей дилера
			$isSubDealer = false;
			$currentUser = new project\user();
			$userProps = $currentUser->arUser;
			if(strlen($userProps["UF_MAIN_USER"]) > 0){
				$user = new project\user( $userProps["UF_MAIN_USER"] );
				if( $user->isDealer() ){
					$isSubDealer = true;
					$idDealer = $user->arUser["ID"];
				}
			}
			else{
				$user = new project\user();
			}
            if( $user->isDealer() ){
				//Добавляем проверку для подчиненных пользователей дилера
				if(!strlen($idDealer)>0){
					$idDealer = $USER->GetID();
				}
                $obUser = new \CUser();
                $res = $obUser->Update( $idDealer, array('PERSONAL_PHOTO' => array('del' => 'Y')) );
                if( $res ){
                    ob_start();
                    $APPLICATION->IncludeComponent(
                        "aoptima:dealerLKPhotoBlock", "",
                        array('USER' => tools\user::info($idDealer))
                    );
                    $html = ob_get_contents();
                    ob_end_clean();
                    // Ответ
                    echo json_encode( array('status' => 'ok', 'html' => $html) ); return;
                } else {
                    // Ответ
                    echo json_encode( array('status' => 'error', 'text' => 'Ошибка удаления') ); return;
                }
            }
        }
        // Ответ
        echo json_encode( array('status' => 'error', 'text' => 'Ошибка авторизации') ); return;
    }




    // reloadAddPricesPage
    if ($action == "reloadAddPricesPage"){

        $form_data = Array();
        parse_str($_POST["form_data"], $form_data);

        ob_start();
        $fields = array(
            'IS_AJAX' => 'Y',
            'form_data' => $form_data
        );
        if( is_array($_POST['stop_ids']) ){
            $fields['IS_LOAD'] = 'Y';
        }
        $fields['IS_MOB_APP'] = IS_ESHOP?"Y":"N";
        $APPLICATION->IncludeComponent(
            "aoptima:personal_add_prices", "", $fields
        );
        $html = ob_get_contents();
        ob_end_clean();

        $arResult = Array("status" => "ok", "html" => $html);
        echo json_encode($arResult);
    }



    // reloadSearchPage
    if ($action == "reloadSearchPage"){

        $form_data = Array();
        parse_str($_POST["form_data"], $form_data);

        ob_start();
        $params = array(
            'IS_AJAX' => 'Y',
            'form_data' => $form_data
        );
        if( is_array($_POST['stop_ids']) ){
            $params['IS_LOAD'] = 'Y';
        }
        if( strlen($_POST['closeSidebar']) > 0 ){
            $params['closeSidebar'] = $_POST['closeSidebar'];
        }

        $APPLICATION->IncludeComponent(
            "aoptima:search", "", $params
        );
        $html = ob_get_contents();
        ob_end_clean();

        $arResult = Array("status" => "ok", "html" => $html);
        echo json_encode($arResult);
    }



    // Обновление разметки ЛК Ценовые предложения дилера
    if ($action == "reloadDealerGoods"){

        \Bitrix\Main\Loader::includeModule('iblock');
        \Bitrix\Main\Loader::includeModule('catalog');

        $form_data = Array();
        parse_str($_POST["form_data"], $form_data);

        if( intval($_POST['delete_offer_id']) > 0 ){
            \CIBlockElement::Delete($_POST['delete_offer_id']);
        }

        ob_start();
        $fields = array(
            'IS_AJAX' => 'Y',
            'form_data' => $form_data
        );
        $APPLICATION->IncludeComponent( "aoptima:personal_goods", "", $fields );
        $html = ob_get_contents();
        ob_end_clean();

        $arResult = Array("status" => "ok", "html" => $html);
        echo json_encode($arResult);
    }






    // Обновление ценового предложения дилера
    if ($action == "dealerPriceSave"){

        \Bitrix\Main\Loader::includeModule('iblock');
        \Bitrix\Main\Loader::includeModule('catalog');

        if(
            intval($_POST['save_id']) > 0
            &&
            strlen($_POST['price']) > 0
            &&
            isset($_POST['quantity'])
            &&
            strlen($_POST['pod_zakaz']) > 0
        ){

            // Инфо об элементе
            $el = tools\el::info($_POST['save_id']);

            // Сохр. "Под заказ"
            $set_prop = array(
                "POD_ZAKAZ" => $_POST['pod_zakaz']=='Y'?1:0
            );
            \CIBlockElement::SetPropertyValuesEx(
                $_POST['save_id'],
                project\catalog::getIblockID( project\catalog::TP_IBLOCK_CODE ),
                $set_prop
            );

            // Сохр. количества на складе
            $fields = array(
                'QUANTITY' => intval(strip_tags($_POST['quantity']))
            );
            $res = \CCatalogProduct::Update($_POST['save_id'], $fields);
            if( $res ){

                // Сохр. цены
                $fields = Array(
                    "PRICE" => strip_tags($_POST['price']),
                );
                CModule::IncludeModule('iblock');
                CIBlockElement::SetPropertyValuesEx($_POST['save_id'], false, array("UPDATE_DATETIME" => new Bitrix\Main\Type\DateTime()));
                $res = \CPrice::Update($el["CATALOG_PRICE_ID_".project\catalog::PRICE_ID], $fields);
                if( $res ){

                    BXClearCache(true, "/el_info/".$_POST['save_id']."/");
                    BXClearCache(true, "/tp_list/");

                    // Ответ
                    $arResult = Array("status" => "ok");
                    echo json_encode($arResult);
                    return;

                } else {
                    tools\logger::addError('Ошибка изменения цены для ТП дилера');
                }

            } else {
                tools\logger::addError('Ошибка изменения количества товара');
            }
        }

        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка сохранения"));
        return;
    }










    if ($action == "updateBasketSortSession"){
        $_SESSION['OFFERS_SORT'] = array(
            'CODE' => strip_tags($_POST['sort']),
            'ORDER' => strip_tags($_POST['order']),
        );
        // Ответ
        echo json_encode(Array("status" => "ok"));
        return;
    }



    // Инфо по шаблону доставки
    if ($action == "deliveryTemplateInfo"){
        if(
            intval(tools\funcs::param_post("template_id")) > 0
            &&
            $USER->IsAuthorized()
        ){
            $deliveryTemplate = new project\delivery_template();
            $deliveryTemplates = $deliveryTemplate->getList($USER->GetID());
            $dt = $deliveryTemplates[tools\funcs::param_post("template_id")];
            if( $dt ){
                $dt['LOC'] = false;
                if( intval($dt['UF_LOC_ID']) > 0 ){
                    $loc = project\bx_location::getByID($dt['UF_LOC_ID']);
                    if( intval($loc['ID']) > 0 ){   $dt['LOC'] = $loc;   }
                }
                // Ответ
                echo json_encode(Array("status" => "ok", "template" => $dt));
                return;
            }
        }
        echo json_encode(Array("status" => "error") );
        return;
    }



    // Переключение внешнего вида каталога
    if ($action == "setCatalogView"){
        if( strlen(tools\funcs::param_post("catalogView")) > 0 ){
            $_SESSION['catalogView'] = tools\funcs::param_post("catalogView");
            // Ответ
            echo json_encode(Array("status" => "ok"));
            return;
        }
    }




    // Авторизация
    if ($action == "auth"){

        \CModule::IncludeModule("catalog");
        \CModule::IncludeModule("sale");
        \CModule::IncludeModule("iblock");

        global $USER;
        if( !$USER->IsAuthorized() ){

            if( $_POST['is_order_auth'] == 'Y' ){
                // данные в массив
                $arFields = Array();
                parse_str($_POST["form_data"], $arFields);
                // действующая корзина
                $basket_offers = $arFields['BASKET_OFFERS'];
            }

            $login = tools\funcs::param_post("login");
            $password = strip_tags(tools\funcs::param_post("password"));

            $result = $USER->Login($login, $password, "Y");

            if (!is_array($result)){
                $USER->Authorize($USER->GetID());

                // Инфо о пользователе
                $user = tools\user::info($USER->GetID());
                if(
                    !$user['UF_REG_CONFIRMED']
                    &&
                    !$USER->IsAdmin()
                ){
                    $USER->Logout();
                    // Ответ
                    echo json_encode( Array(
                        "status" => "error",
                        "text" => "Профиль ещё не подтверждён"
                    ) );
                    return;
                }

                if( $_POST['is_order_auth'] == 'Y' ){

                    $basketInfo = project\user_basket::info();
                    $arBasket = $basketInfo['arBasket'];

                    // Очищаем корзину
                    \CSaleBasket::DeleteAll(\CSaleBasket::GetBasketUserID());
                    // Формируем новую корзину
                    $indx = 0;
                    foreach ( $basket_offers as $basket_offer ){
                        $basketItem = $arBasket[$indx];
                        if($basketItem->getField('CUSTOM_PRICE') == "Y") continue;

                        if( $basket_offer['QUANTITY'] > 0 ){

                            $product_id = project\catalog::skuProductID( $basket_offer['ID'] );

                            $basket = Sale\Basket::loadItemsForFUser(
                                Sale\Fuser::getId(),
                                Bitrix\Main\Context::getCurrent()->getSite()
                            );
                            $item = $basket->createItem('catalog', $basket_offer['ID']);
                            $item->setFields(array(
                                'QUANTITY' => $basket_offer['QUANTITY'],
                                'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                                'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
                                'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider'
                            ));
                            $basket->save();

                        }
                        $indx++;
                    }

                }

                // Ответ
                echo json_encode( Array( "status" => "ok" ) ); return;
            } else {
                // Ответ
                echo json_encode( Array( "status" => "error", "text" => "Неверный логин или пароль" ) ); return;
            }
        } else {
            // Ответ
            echo json_encode( Array( "status" => "auth", "text" => "Вы уже авторизованы" ) );
        }
    }




    // Формирование контрольной строки для восстановления пароля
    if ($action == "password_recovery"){
        // данные в массив
        $arFields = Array();
        parse_str(tools\funcs::param_post("form_data"), $arFields);
        $user_id = false;
        // Если какие-то данные определены
        if (strlen($arFields['email']) > 0){
            // проверяем EMAIL на наличие у других пользователей
            $filter = Array("=EMAIL" => $arFields["email"]);
            $users = \CUser::GetList(($by = "id"), ($order = "desc"), $filter); // выбираем пользователей
            if ($user = $users->GetNext()){
                $kod = md5(time() . rand(1, 100000));
                // Заносим код в профиль пользователя
                $obUser = new \CUser;
                $res = $obUser->Update($user['ID'], array('UF_RECOVERY_CODE' => $kod));
                if ($res){

                    $user_id = $user['ID'];

                    $link = '<a href="' . $_SERVER['REQUEST_SCHEME'] . '://' . $server_name . '/password_update/?code=' . $kod . '">' . $_SERVER['REQUEST_SCHEME'] . '://' . $server_name . '/password_update/?code=' . $kod . '</a>';

                    $content_html = '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Ссылка для восстановления пароля на сайте '.\Bitrix\Main\Config\Option::get('main', 'server_name').':</p>';
                    $content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">'.$link.'</p>';


                    $mail_fields = Array(
                        "TITLE" => 'Ссылка для восстановления пароля (сайт "'.$server_name.'")',
                        "HTML" => project\mail::header().$content_html.project\mail::footer(),
                        "EMAIL_TO" => $user["EMAIL"]
                    );
                    \CEvent::SendImmediate("MAIN", "s1", $mail_fields);

                    // Ответ
                    echo json_encode(Array("status" => "ok"));
                    return;
                }
            }
            if (!$user_id){
                // Ответ
                echo json_encode(Array("status" => "error", "text" => "Такой адрес электронной почты на сайте не зарегистрирован"));
                return;
            }
        }
    }



    // Восстановление пароля (сохранение)
    if ($action == "password_update"){
        // данные в массив
        $arFields = Array();
        parse_str(tools\funcs::param_post("form_data"), $arFields);
        if( strlen($arFields['code']) > 0 ){
            $user_id = false;
            // Ищем пользователя с таким кодом
            $filter = Array( "=UF_RECOVERY_CODE" => strip_tags($arFields['code']) );
            $rsUsers = \CUser::GetList(($by = "id"), ($order = "desc"), $filter, array('FIELDS' => array('ID'), 'SELECT' => array('UF_*')));
            if ( $arUser = $rsUsers->GetNext() ){
                $user_id = $arUser['ID'];
                // Заносим в профиль пользователя
                $fields['UF_RECOVERY_CODE'] = '';
                $fields['PASSWORD'] = strip_tags($arFields['PASSWORD']);
                $fields['CONFIRM_PASSWORD'] = strip_tags($arFields['CONFIRM_PASSWORD']);
                $user = new \CUser;
                $res = $user->Update($user_id, $fields);
                if ($res){
                    $USER->Logout();
                    // Ответ
                    echo json_encode( Array("status" => "ok") );
                } else {
                    // Ответ
                    echo json_encode(array("status" => 'error', "text" => $user->LAST_ERROR));
                }
            } else {
                // Ответ
                echo json_encode(Array("status" => "error", "text" => "Ссылка ошибочная либо устарела"));
                return;
            }
        } else {
            // Ответ
            echo json_encode(Array("status" => "error", "text" => "Ошибочная ссылка"));
            return;
        }
    }



    // Изменение пароля
    if ($action == "personal_password"){
        // данные в массив
        $arFields = Array();
        parse_str(tools\funcs::param_post("form_data"), $arFields);
        // Ищем пользователя с таким кодом
        $filter = Array("=UF_RECOVERY_CODE" => $arFields['code']);
        $rsUsers = CUser::GetList(($by = "id"), ($order = "desc"), $filter, array('FIELDS' => array('ID'), 'SELECT' => array('UF_*')));
        while ($arUser = $rsUsers->GetNext()) {
            $user_id = $arUser['ID'];
        }
        // Если какие-то данные определены
        if (intval($user_id) > 0){
            // Заносим в профиль пользователя
            $fields['UF_RECOVERY_CODE'] = '';
            $fields['PASSWORD'] = $arFields['PASSWORD'];
            $fields['CONFIRM_PASSWORD'] = $arFields['CONFIRM_PASSWORD'];
            $user = new CUser;
            $res = $user->Update($user_id, $fields);
            if ($res){
                $_SESSION['password_ok'] = 1;
                $USER->Logout();
                // Ответ
                echo json_encode(Array("status" => "ok", 'html' => $html));
            } else {
                // Ответ
                echo json_encode(array("status" => 'error', "text" => $user->LAST_ERROR));
            }
        } else {
            // Ответ
            echo json_encode(array("status" => 'error', "text" => 'Ошибочный код'));
        }
    }



    // Добавляем товар в корзину
    if ($action == "add_to_basket"){

        $siteID = $_POST['IS_MOB_APP']=='Y'?'mb':Bitrix\Main\Context::getCurrent()->getSite();

        $user = new project\user();

        if( $user->isDealer() ){

            // Ответ
            echo json_encode(["status" => "error", 'text' => 'Дилер не может оформлять заказы!']);
            return;

        } else {

            $tov_id = tools\funcs::param_post("tov_id");
            $replace_quant = tools\funcs::param_post("replace_quant");
            $quantity = tools\funcs::param_post("quantity");
            if(!$replace_quant)
            {
                $quantity = intval($quantity ? $quantity : 1);
            }
            if( intval($tov_id) > 0 ){
                $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), $siteID);
                if ($item = $basket->getExistsItem('catalog', $tov_id)){
                    if($replace_quant)
                    {
                        if($quantity)
                        {
                            if ($item->getQuantity() != $quantity)
                            {
                                $item->setField('QUANTITY', $quantity);
                            }
                        }
                        else
                        {
                            // удалить товар из корзины
                            $item->delete();
                        }
                    }
                    else
                    {
                        $item->setField('QUANTITY', $item->getQuantity() + $quantity);
                    }
                } else {
                    $item = $basket->createItem('catalog', $tov_id);
                    $item->setFields(array('QUANTITY' => $quantity, 'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(), 'LID' => $_POST['IS_MOB_APP']=='Y'?'mb':'s1', 'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',));
                }
                $basket->save();

                ob_start();
                $APPLICATION->IncludeComponent(
                    "aoptima:basketSmall", "",
                    [
                        'IS_MOB_APP' => $_POST['IS_MOB_APP']
                    ]
                );
                $html = ob_get_contents();
                ob_end_clean();

                $arResult = Array("status" => "ok", 'html' => $html);
            } else {
                $arResult = Array("status" => "error", "text" => "Ошибка добавления в корзину");
            }
        }
        // Ответ
        echo json_encode($arResult);
    }





    // Обновление корзины
    if ($action == "reloadBasket"){

        $siteID = $_POST['IS_MOB_APP']=='Y'?'mb':Bitrix\Main\Context::getCurrent()->getSite();

        $form_data = Array();
        parse_str($_POST["form_data"], $form_data);

        // Очистка корзины
        if( tools\funcs::param_post("cleanBasket") == 'Y' ){
            \Bitrix\Main\Loader::includeModule('sale');
            \CSaleBasket::DeleteAll( \CSaleBasket::GetBasketUserID() );
            unset($_SESSION['basketInfo']);
        }

        // Удаление товара из корзины
        $removeProductID = strip_tags(tools\funcs::param_post("remove_product_id"));
        if (intval($removeProductID) > 0){
            // Корзина пользователя
            $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), $siteID);
            foreach ($basket as $basketItem) {
                $result = \CCatalogSku::GetProductInfo( $basketItem->getField('PRODUCT_ID') );
                $isSKU = $result['ID'];
                if ( $isSKU ){
                    $tov_id = $result['ID'];
                } else {
                    $tov_id = $basketItem->getField('PRODUCT_ID');
                }
                if( $tov_id == $removeProductID ){
                    $basketItem->delete();
                }
            }
            $basket->save();
            unset($_SESSION['basketInfo']);
        }

        // Удаление товара из корзины
        $removeUnderOrderProductID = strip_tags(tools\funcs::param_post("remove_under_order_product_id"));
        if (intval($removeUnderOrderProductID) > 0){
            // Корзина пользователя
            $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), $siteID);
            foreach ($basket as $basketItem) {
                $result = \CCatalogSku::GetProductInfo( $basketItem->getField('PRODUCT_ID') );
                $isSKU = $result['ID'];
                if ( $isSKU ){
                    $tov_id = $result['ID'];
                } else {
                    $tov_id = $basketItem->getField('PRODUCT_ID');
                }
                if( $tov_id == $removeUnderOrderProductID ){
                    $basketItem->delete();
                }
            }
            $basket->save();
            unset($_SESSION['basketInfo']);
        }

        // Обновление количества в корзине
        if(
            is_array($_POST["newBasketQuantity"])
            &&
            count($_POST["newBasketQuantity"]) > 0
        ){
            // Корзина пользователя
            $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), $siteID);
            // Соберём инфо об элементах корзины в разбивке по основным товарам
            $basketProducts = array();
            foreach( $basket as $basketItem ){
                if($basketItem->getField('CUSTOM_PRICE') == "Y") continue;

                $el_id = $basketItem->getProductId();
                $result = \CCatalogSku::GetProductInfo( $el_id );
                $isSKU = $result['ID'];
                if ( $isSKU ){
                    $productID = $result['ID'];
                    $basketProducts[$productID][$basketItem->getId()] += $basketItem->getQuantity();
                } else {
                    $basket->getItemById($basketItemID)->delete();
                }
            }

            if( count($basketProducts) > 0 ){
                // Корректировка корзины
                foreach ($basketProducts as $productID => $basketItems){
                    $cnt = 0;
                    foreach ($basketItems as $basketItemID => $quantity){ $cnt++;
                        if( $cnt == 1 ){
                            $basket->getItemById($basketItemID)->setField('QUANTITY', $_POST["newBasketQuantity"][$productID]);
                        } else if( $cnt > 1 ){
                            $basket->getItemById($basketItemID)->delete();
                        }
                    }
                }
            }
            $basket->save();
            unset($_SESSION['basketInfo']);
        }

        ob_start();
        $fields = array(
            'IS_AJAX' => 'Y',
            'form_data' => $form_data,
            'IS_MOB_APP' => $_POST['IS_MOB_APP']
        );
        $APPLICATION->IncludeComponent( "aoptima:basket", "", $fields );
        $html = ob_get_contents();
        ob_end_clean();

        $arResult = Array("status" => "ok", "html" => $html);
        echo json_encode($arResult);
    }



    // Обновление страницы заказа
    if ($action == "reloadOrder"){

        $form_data = Array();
        parse_str($_POST["form_data"], $form_data);

        $arResult = Array( "status" => "ok"  );

        if( strlen($_POST['full_address']) > 0 ){
            $full_address = strip_tags($_POST['full_address']);
            $arResults = project\address::getSuggestions($full_address, 1);
            if(
                $arResults['suggestions'][0]['data']['geo_lat']
                &&
                $arResults['suggestions'][0]['data']['geo_lon']
            ){
                $arResult['lat'] = $arResults['suggestions'][0]['data']['geo_lat'];
                $arResult['lng'] = $arResults['suggestions'][0]['data']['geo_lon'];
                $form_data['address'] = $full_address;
                $form_data['coords'] = $arResult['lat'].','.$arResult['lng'];
            }
        }

        ob_start();
        $componentFields = array(
            'IS_AJAX' => 'Y',
            'form_data' => $form_data,
            'IS_MOB_APP' => $_POST['IS_MOB_APP']
        );
        $APPLICATION->IncludeComponent( "aoptima:order", "", $componentFields );
        $arResult['html'] = ob_get_contents();
        ob_end_clean();

        echo json_encode($arResult);

    }



    // Смена количества товара в корзине
    if ($action == "recalc_qt"){
        if (intval(tools\funcs::param_post("item_id")) > 0){
            CModule::IncludeModule("sale");
            $arFields = array("QUANTITY" => tools\funcs::param_post("quantity"));
            CSaleBasket::Update(tools\funcs::param_post("item_id"), $arFields);
            // Ответ
            $arResult = Array("status" => "ok");
            echo json_encode($arResult);
        }
    }



    // Удаляем элемент корзины
    if ($action == "unset_item"){
        if (intval(tools\funcs::param_post("item_id")) > 0){
            CModule::IncludeModule("sale");
            $res = CSaleBasket::Delete(tools\funcs::param_post("item_id"));
            if ($res){
                $arResult = Array("status" => "ok");
            } else {
                $arResult = Array("status" => "error", "text" => "Ошибка удаления");
            }
            echo json_encode($arResult);
        }
    }



    if ($action == "clear_basket"){
        CModule::IncludeModule("sale");
        CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
        // Ответ
        echo json_encode(array("status" => "ok"));
    }




    // *** ОФОРМЛЕНИЕ ЗАКАЗА***
    if ($action == "order"){

        \CModule::IncludeModule("catalog");
        \CModule::IncludeModule("sale");
        \CModule::IncludeModule("iblock");

        $rounding = project\catalog::ROUND;

        // данные в массив
        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);
        foreach ( $arFields as $key => $value ){
            if( is_string($value) ){    $arFields[$key] = strip_tags($value);    }
        }

        $siteID = Bitrix\Main\Context::getCurrent()->getSite();
        if(IS_ESHOP)
        {
            $siteID = "mb";
        }

        // Инфо по текущей корзине
        $curBasket = [];
        $basketInfo = project\user_basket::info(
            false,
            $arFields['ds'],
            false,
            1,
            $siteID
        );
        $arBasket = $basketInfo['arBasket'];
        foreach ( $arBasket as $basketItem ){
            $product_id = project\catalog::skuProductID( $basketItem->getProductId() );
            $curBasket[$product_id] += $basketItem->getQuantity();
        }

        // Пользователь по умолчанию
        $user_id = project\order::SERVICE_USER_ID;

        // Пользователь авторизован
        if( $USER->IsAuthorized() ){

            // берём ID пользователя
            $user_id = $USER->GetID();

            // Пользователь НЕ авторизован
        } else if( $arFields['order_register'] == 'Y' ){

            // проверяем EMAIL
            $checkUser = project\user::getByLogin($arFields["email"]);
            if( !$checkUser ){
                $checkUser = project\user::getByEmail($arFields["email"]);
            }
            // Если найден пользователь с таким Email
            if( intval($checkUser['ID']) > 0 ){

                if( strlen( $arFields["password"] ) > 0 ){
                    $result = $USER->Login($checkUser['LOGIN'], $arFields["password"], "Y");
                    if (!is_array($result)){
                        $user_id = $USER->GetID();
                        $USER->Authorize($user_id);
                    } else {
                        echo json_encode( Array( "status" => "wrong_password" ) );  return;
                    }
                } else {
                    // Если Email уже есть
                    echo json_encode( Array( "status" => "error", "text" => "Такой адрес электронной почты уже зарегистрирован на сайте" ) );  return;
                }

            } else {

                $password = randString(10, array( "abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "0123456789" ));

                // Регистрация пользователя
                $arUserFields = Array(
                    "LOGIN" => strip_tags($arFields["email"]),
                    "EMAIL" => strip_tags($arFields["email"]),
                    "PASSWORD" => $password,
                    "CONFIRM_PASSWORD" => $password,
                    "GROUP_ID" => Array(7),
                    "UF_REG_CONFIRMED" => 1
                );
                if( strlen($arFields["name"]) > 0 ){
                    $arUserFields['NAME'] = strip_tags($arFields["name"]);
                }
                if( strlen($arFields["last_name"]) > 0 ){
                    $arUserFields['LAST_NAME'] = strip_tags($arFields["last_name"]);
                }
                if( strlen($arFields["phone"]) > 0 ){
                    $arUserFields['PERSONAL_PHONE'] = strip_tags($arFields["phone"]);
                }
                if( strlen($arFields["birthday"]) > 0 ){
                    $arUserFields['PERSONAL_BIRTHDAY'] = strip_tags($arFields["birthday"]);
                }
                if( strlen($arFields["gender"]) > 0 ){
                    $arUserFields['PERSONAL_GENDER'] = strip_tags($arFields["gender"]);
                }
                /*$arUserFields['UF_REG_CODE'] = md5(
                    randString( 20, array( "abcdefghijklmnopqrstuvwxyz" ) ).time()
                );*/

                // Регистрация пользователя
                $user = new \CUser;
                $user_id = $user->Add($arUserFields);
                if( intval($user_id) > 0 ){

                    $USER->Authorize($user_id);

                    // Отправка рег. информации
                    tools\user::sendRegInfo($user_id, $password);

                    // Ссылка подтверждения регистрации
                    //tools\user::sendRegConfirmLink($user_id);

                }
            }
        }

        if( intval($user_id) > 0 ){

            // ID варианта оплаты
            $pss = project\ps::getList();
            foreach ( $pss as $key => $ps ){
                if( $ps['CODE'] == $arFields['ps'] ){      $ps_id = $ps['ID'];      }
            }

            // ID варианта доставки
            $ds_id = $arFields['ds'];

            if(
                is_array($arFields['BASKET_OFFERS'])
                &&
                count($arFields['BASKET_OFFERS']) > 0
                &&
                intval($ps_id) > 0
            ){

                // Удаляем старую корзину
                \CSaleBasket::DeleteAll(\CSaleBasket::GetBasketUserID());

                // Формируем новую корзину
                $basket = Sale\Basket::loadItemsForFUser(
                    Sale\Fuser::getId(),
                    Bitrix\Main\Context::getCurrent()->getSite()
                );

                // отключим добавление подарков на событии OnSaleBasketBeforeSaved, т.к. они уже учтены в $arFields['BASKET_OFFERS']
                global $noGifts;
                $noGifts = true;

                $indx = 0;
                foreach ( $arFields['BASKET_OFFERS'] as $basket_offer ){
                    if( $basket_offer['QUANTITY'] > 0 ){

                        $product_id = project\catalog::skuProductID( $basket_offer['ID'] );
                        $curBasket[$product_id] -= $basket_offer['QUANTITY'];

                        $item = $basket->createItem('catalog', $basket_offer['ID']);
                        $arFieldsCart = Array(
                            'QUANTITY' => $basket_offer['QUANTITY'],
                            'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                            'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
                            'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider'
                        );
                        $basketItem = $arBasket[$indx];
                        if($basketItem->getField('CUSTOM_PRICE') == "Y")
                        {
                            $arFieldsCart['CUSTOM_PRICE'] = "Y";
                            $arFieldsCart["PRICE"] = $basketItem->getPrice();
                            $arFieldsCart["DISCOUNT_PRICE"] = $basketItem->getDiscountPrice();
                            $arFieldsCart['BASE_PRICE'] = $basketItem->getBasePrice();
                        }

                        $item->setFields($arFieldsCart);
                    }
                    $indx++;
                }
                $basket->save();

                // Инфо по новой корзине
                $basketInfo = project\user_basket::info( false, $arFields['ds']);

                $basket_cnt = $basketInfo['basket_cnt'];
                $arBasket = $basketInfo['arBasket'];
                $basket_disc_sum = $basketInfo['basket_disc_sum'];
                $discount = $basketInfo['discount'];
                $delivery_price_is_string = preg_match("/[а-яёА-ЯЁ]+/i", $arFields['delivery_price']);
                $pay_sum = $basket_disc_sum + ($delivery_price_is_string?0:$arFields['delivery_price']);

                // СОЗДАЁМ ЗАКАЗ
                $order = Sale\Order::create('s1', $user_id);

                // Передаём корзину в заказ
                $basket = Basket::create('s1');
                foreach( $arBasket as $basketItem ){
                    $item = $basket->createItem('catalog', $basketItem->getField('PRODUCT_ID'));
                    $obIblocks = \CIBlock::GetByID($basketItem->el['IBLOCK_ID']);
                    if($iblock = $obIblocks->GetNext()){     $iblock_xml_id = $iblock['XML_ID'];     }

                    $arFieldsCart = Array(
                        'QUANTITY' => $basketItem->getField('QUANTITY'),
                        'CURRENCY' => 'RUB',
                        'LID' => 's1',
                        'PRODUCT_XML_ID' => $basketItem->el['XML_ID'],
                        'CATALOG_XML_ID' => $iblock_xml_id,
                        'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
                    );
                    if($basketItem->getField('CUSTOM_PRICE') == "Y")
                    {
                        $arFieldsCart['CUSTOM_PRICE'] = "Y";
                        $arFieldsCart["PRICE"] = $basketItem->getPrice();
                        $arFieldsCart["DISCOUNT_PRICE"] = $basketItem->getDiscountPrice();
                        $arFieldsCart['BASE_PRICE'] = $basketItem->getBasePrice();
                    }

                    $item->setFields($arFieldsCart);
                }
                $order->setBasket($basket);

                // Тип плательщика
                $order->setPersonTypeId(1);

                // Оплата
                $paymentCollection = $order->getPaymentCollection();
                $payment = $paymentCollection->createItem(Sale\PaySystem\Manager::getObjectById( $ps_id ));
                $payment->setField('SUM', $pay_sum); // Сумма к оплате
                $payment->setField('CURRENCY', 'RUB');

                // Доставка
                $shipmentCollection = $order->getShipmentCollection();
                $shipment = $shipmentCollection->createItem(Sale\Delivery\Services\Manager::getObjectById( $ds_id ));
                // ПРОИЗВОЛЬНАЯ СТОИМОСТЬ ДОСТАВКИ (ПРИ НЕОБХОДИМОСТИ)
                if ( !$delivery_price_is_string ){
                    $shipment->setField('CUSTOM_PRICE_DELIVERY', 'Y');
                    $shipment->setField('PRICE_DELIVERY', $arFields['delivery_price']);
                }
                $shipmentItemCollection = $shipment->getShipmentItemCollection();
                foreach ($basket as $basketItem){
                    $item = $shipmentItemCollection->createItem($basketItem);
                    $item->setQuantity($basketItem->getQuantity());
                }

                // Получение информации о варианте доставки
                $ds_list = project\ds::getList();
                $arDS = $ds_list[$ds_id];

                // Заполняем свойства заказа
                $propertyCollection = $order->getPropertyCollection();
                $props = array(
                    1 => $arFields["name"],
                    2 => $arFields["last_name"],
                    3 => $arFields["phone"],
                    4 => $arFields["email"],
                    32 => $arFields["where_from"]
                );

                $props[17] = $arFields["birthday"]?$arFields["birthday"]:'-';
                if( $arFields["gender"] == 'M' ){
                    $props[18] = 'мужской';
                } else if( $arFields["gender"] == 'F' ){
                    $props[18] = 'женский';
                } else {
                    $props[18] = '-';
                }

                $props[5] = $arFields["basketCity"];
                $props[29] = $arFields["loc_id"];

                if( $arDS['delivType'] == 'delivery' ){
                    $props[21] = $arFields["address"];
                    //$props[6] = $arFields["street"];
                    //$props[7] = $arFields["house"];
                    $props[8] = $arFields["podyezd"]?$arFields["podyezd"]:'-';
                    $props[9] = $arFields["floor"]?$arFields["floor"]:'-';
                    $props[10] = $arFields["kvartira"]?$arFields["kvartira"]:'-';
                    $props[11] = $arFields["lift"]?$arFields["lift"]:'-';
                    $props[12] = $arFields["comment"]?$arFields["comment"]:'-';
                    $props[13] = $arFields["deliveryDate"]?$arFields["deliveryDate"]:'-';
                    $props[14] = $arFields["deliveryTime"]?$arFields["deliveryTime"]:'-';
                } else if( $arDS['delivType'] == 'samovyvoz' ){
                    $dealer_shop = new project\dealer_shop();
                    $pvz = $dealer_shop->getByID($arFields["pvz"]);
                    $props[16] = $pvz['NAME'].' ['.$pvz['ID'].']';
                }
                $dealer = tools\user::info($arFields["dealer_id"]);
                $props[15] = htmlspecialchars_decode($dealer['UF_COMPANY_NAME']);
                $props[19] = $dealer['ID'];
                $under_order_goods = project\catalog::getUnderOrderGoods();
                if( count($under_order_goods) > 0 ){
                    $under_order_goods_names = [];
                    foreach ( $under_order_goods as $id => $el ){
                        $el = project\catalog::updateProductName($el);
                        $under_order_goods_names[] = $el['NAME'];
                    }
                    $under_order_goods_names = html_entity_decode(implode('; ', $under_order_goods_names));
                    $props[22] = $under_order_goods_names;
                }
                $pay_type = '-';
                if( $arFields['pay_type'] == 'pre' ){
                    $pay_type = 'Предоплата';
                } else if( $arFields['pay_type'] == 'post' ){
                    $pay_type = 'Постоплата';
                }
                $props[23] = $pay_type;
                $props[24] = $arFields['needRazgruzka']=='Да'?'Да':'Нет';
                $props[25] = $arFields['needPodyem']=='Да'?'Да':'Нет';
                $props[26] = $arFields['needPerenos']=='Да'?'Да':'Нет';
                if(
                    $arFields['needPerenos']=='Да'
                    &&
                    strlen($arFields['perenosDistance']) > 0
                ){     $props[27] = $arFields['perenosDistance'];     }
                // стоимость доставки по договоренности
                if($delivery_price_is_string)
                {
                    $props[30] = "Y";
                }
                foreach ( $props as $prop_id => $value ){
                    $prop = $propertyCollection->getItemByOrderPropertyId($prop_id);
                    $prop->setValue($value);
                }

                // Сохраняем новый заказ
                $result = $order->save();

                // Определяем ID нового заказа
                $order_id = $order->GetId();

                if (intval($order_id) > 0){

                    // Уменьшение остатков товара
                    foreach ( $arBasket as $basketItem ){
                        $sku_id = $basketItem->getProductId();
                        $basketItemQuantity = $basketItem->getQuantity();
                        $product = \CCatalogProduct::GetByID($sku_id);
                        $newQuantity = $product['QUANTITY'] - $basketItemQuantity;
                        if( $newQuantity < 0 ){    $newQuantity = 0;    }
                        $res = \CCatalogProduct::Update($sku_id, array( 'QUANTITY' => $newQuantity ));
                    }
                    BXClearCache(true, "/tp_list/");

                    // Установка стартового статуса заказу
                    $order_status = new project\order_status();
                    $order_status->set( $order_id, 'A' );

                    // Очищаем корзину
                    \CSaleBasket::DeleteAll(\CSaleBasket::GetBasketUserID());

                    // Пишем в свойство элементов корзины их product_id
                    $order = \Bitrix\Sale\Order::load($order_id);
                    $basket = Sale\Basket::loadItemsForOrder($order);
                    $arBasketProds = Array();
                    foreach( $basket as $key => $basketItem ){
                        $el = tools\el::info( $basketItem->getProductId() );
                        $propertyCollection = $basketItem->getPropertyCollection();
                        $propertyCollection->setProperty(array(
                            array(
                                'NAME' => 'product_id',
                                'CODE' => 'product_id',
                                'VALUE' => $el['PROPERTY_CML2_LINK_VALUE'],
                                'SORT' => 100,
                            ),
                        ));
                        $propertyCollection->save();

                        $arProdData = tools\el::info($el['PROPERTY_CML2_LINK_VALUE']);

                        $arBasketProds[] = $arProdData["ID"]
                            ."|".$arProdData["PROPERTY_ARTICLE_VALUE"]
                            ."|".$arProdData["NAME"]
                            ."|".$basketItem->getQuantity();
                    }
                    $basket->save();   $order->save();
                    $total = $order->getPrice();

                    // Возвращаем в корзину недостающее количество (при необходимости)
                    foreach ( $curBasket as $product_id => $qnt ){
                        if( $qnt > 0 ){
                            $sku_id = project\catalog::randProductSkuID( $product_id );
                            $basket = Sale\Basket::loadItemsForFUser(
                                Sale\Fuser::getId(),
                                Bitrix\Main\Context::getCurrent()->getSite()
                            );
                            $item = $basket->createItem( 'catalog', $sku_id );
                            $item->setFields(array(
                                'QUANTITY' => $qnt,
                                'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                                'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
                                'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider'
                            ));
                            $basket->save();
                        }
                    }

                    // Письмо покупателю
                    ob_start();
                    $APPLICATION->IncludeComponent("aoptima:orderMail", "client", [
                        'ORDER_ID' => $order_id
                    ]);
                    $mailHtml = ob_get_contents();
                    ob_end_clean();
                    $mail_fields = Array(
                        "TITLE" => 'Новый заказ №'.$order_id.' на сайте '.$server_name,
                        "HTML" => $mailHtml,
                        "EMAIL_TO" => $arFields['email']
                    );
                    \CEvent::Send("MAIN", "s1", $mail_fields);

                    // Письмо дилеру
                    $dealerEmailTo = isset($dealer['UF_EMAIL_FOR_ORDERS'])&&strlen($dealer['UF_EMAIL_FOR_ORDERS'])>0?$dealer['UF_EMAIL_FOR_ORDERS']:$dealer['EMAIL'];
                    ob_start();
                    $APPLICATION->IncludeComponent("aoptima:orderMail", "dealer", [
                        'ORDER_ID' => $order_id
                    ]);
                    $mailHtml = ob_get_contents();
                    ob_end_clean();
                    $mail_fields = [
                        "TITLE" => 'Новый заказ №'.$order_id.' на сайте '.$server_name,
                        "HTML" => $mailHtml,
                        "EMAIL_TO" => $dealerEmailTo
                    ];

                    $commonEmailTo = \Bitrix\Main\Config\Option::get('aoptima.project', 'COMMON_ORDERS_EMAIL_TO');
                    $commonEmailTo = trim(strtolower($commonEmailTo));
                    if( strlen($commonEmailTo) > 0 ){
                        $mail_fields["HIDDEN_COPY_EMAIL_TO"] .= $commonEmailTo.', ';
                    }

                    //адреса сбытовых дирекций
                    $rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>$dealer["ID"]),array("SELECT"=>array("UF_SBITDIR")));
                    $arUser=$rsUser->Fetch();
                    $EMAILS = array();
                    $res = CIBlockElement::GetProperty(14, $arUser["UF_SBITDIR"], "sort", "asc", array("CODE" => "EMAIL"));
                    while ($ob = $res->GetNext())
                    {
                        $EMAILS[] = $ob['VALUE'];
                    }
                    $sbitdir = implode(",", $EMAILS);
                    if( strlen($sbitdir) > 0 ){

                        $mail_fields["HIDDEN_COPY_EMAIL_TO"] .= $sbitdir;
                    }


                    \CEvent::Send("MAIN", "s1", $mail_fields);

                    // Подписка на рассылку
                    if ( $arFields['subscribe'] == 'Y' ){
                        $obSubscribe = new project\subscribe();
                        $rubrics = array_keys( $obSubscribe->rubricsList( true ) );
                        if( count($rubrics) > 0 ){
                            $subscribers = \CSubscription::GetList(
                                array("ID" => "ASC"),
                                array( "EMAIL" => $arFields['email'] )
                            );
                            if ( $subscriber = $subscribers->GetNext() ){
                                $res =  $obSubscribe->update( $subscriber['ID'], array('RUB_ID' => $rubrics) );
                            } else {
                                $res = $obSubscribe->add($arFields['email'], $rubrics);
                            }
                        }
                    }

                    // Ответ
                    echo json_encode( Array(
                        "status" => "ok",
                        "order_id" => $order_id,
                        "ps_id" => $ps_id,
                        "dealer_id" => $dealer["ID"],
                        "dealer_name" => $dealer["NAME"],
                        "products" => implode(PHP_EOL, $arBasketProds),
                        "total" => $total
                    ) );
                    return;

                } else {
                    // Ответ
                    echo json_encode( Array( "status" => "error", 'text' => $result->getErrorMessages() ) );
                    return;
                }
            } else {
                // Ответ
                echo json_encode( Array( "status" => "error", 'text' => 'Ошибка оформления заказа' ) );
                return;
            }
        } else {
            // Ответ
            echo json_encode( Array( "status" => "user_error" ) ); return;
        }
    }





    // *** ПОВТОР ЗАКАЗА ***
    if ($action == "repeat_order"){

        \CModule::IncludeModule("catalog");
        \CModule::IncludeModule("sale");
        \CModule::IncludeModule("iblock");

        $rounding = project\catalog::ROUND;

        $old_order_id = strip_tags($_POST['order_id']);

        if( intval($old_order_id) > 0 ){

            // Авторизован
            $user_id = 16;
            if( $USER->IsAuthorized() ){
                // берём ID пользователя
                $user_id = $USER->GetID();
            }

            if( intval($user_id) > 0 ){

                // Старый заказ
                $oldOrder = Sale\Order::load($old_order_id);
                $propValues = project\order::getOrderPropValues($oldOrder);

                $arFields = array();

                // Вариант оплаты
                $paymentCollection = $oldOrder->getPaymentCollection();
                foreach ($paymentCollection as $payment) {
                    $arFields['ps'] = $payment->getPaymentSystemId();
                }
                // Вариант доставки
                $shipmentCollection = $oldOrder->getShipmentCollection();
                foreach ( $shipmentCollection as $shipment ){
                    $arFields['ds'] = $shipment->getDeliveryId();
                }

                $arFields['delivery_price'] = $oldOrder->getDeliveryPrice();

                $arFields['name'] = project\order::getPropValue($propValues, 'NAME');
                $arFields['last_name'] = project\order::getPropValue($propValues, 'LAST_NAME');
                $arFields['email'] = project\order::getPropValue($propValues, 'EMAIL');
                $arFields['phone'] = project\order::getPropValue($propValues, 'PHONE');
                $arFields['birthday'] = project\order::getPropValue($propValues, 'BIRTHDAY');
                $arFields["gender"] = project\order::getPropValue($propValues, 'GENDER');
                $arFields["basketCity"] = project\order::getPropValue($propValues, 'CITY');
                $arFields["loc_id"] = project\order::getPropValue($propValues, 'LOC_ID');
                $arFields["address"] = project\order::getPropValue($propValues, 'ADDRESS');
                //$arFields["street"] = project\order::getPropValue($propValues, 'STREET');
                //$arFields["house"] = project\order::getPropValue($propValues, 'HOUSE');
                $arFields["podyezd"] = project\order::getPropValue($propValues, 'PODYEZD');
                $arFields["floor"] = project\order::getPropValue($propValues, 'FLOOR');
                $arFields["kvartira"] = project\order::getPropValue($propValues, 'KVARTIRA');
                $arFields['pay_type'] = project\order::getPropValue($propValues, 'PAY_TYPE');
                $arFields["lift"] = project\order::getPropValue($propValues, 'LIFT');
                $arFields["comment"] = project\order::getPropValue($propValues, 'COMMENT');
                $arFields["deliveryDate"] = project\order::getPropValue($propValues, 'DELIV_DATE');
                $arFields["deliveryTime"] = project\order::getPropValue($propValues, 'DELIV_TIME');
                $arFields["dealer_id"] = project\order::getPropValue($propValues, 'DEALER_ID');
                $arFields["pvz"] = project\order::getPropValue($propValues, 'PVZ');

                $arFields['needRazgruzka'] = project\order::getPropValue($propValues, 'NEED_RAZGRUZKA');
                $arFields['needPodyem'] = project\order::getPropValue($propValues, 'NEED_PODYEM');
                $arFields['needPerenos'] = project\order::getPropValue($propValues, 'NEED_PERENOS');
                $arFields['perenosDistance'] = project\order::getPropValue($propValues, 'PERENOS_DISTANCE');

                foreach ( $arFields as $key => $value ){
                    if( strlen($value) > 0 ){} else {    unset($arFields[$key]);    }
                }

                // Удаляем действующую корзину
                \CSaleBasket::DeleteAll(\CSaleBasket::GetBasketUserID());

                // Формируем новую корзину
                $oldBasket = $oldOrder->getBasket();
                foreach ( $oldBasket as $basketItem ){
                    $basket = Sale\Basket::loadItemsForFUser(
                        Sale\Fuser::getId(),
                        Bitrix\Main\Context::getCurrent()->getSite()
                    );
                    $item = $basket->createItem('catalog', $basketItem->getProductId());
                    $item->setFields(array(
                        'QUANTITY' => $basketItem->getQuantity(),
                        'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                        'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
                        'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider'
                    ));
                    $basket->save();
                }

                // Инфо по новой корзине
                $basketInfo = project\user_basket::info( false, $arFields['ds']);
                $basket_cnt = $basketInfo['basket_cnt'];
                $arBasket = $basketInfo['arBasket'];
                $basket_disc_sum = $basketInfo['basket_disc_sum'];
                $discount = $basketInfo['discount'];
                $pay_sum = $basket_disc_sum + $arFields['delivery_price'];

                // ID варианта оплаты
                $ps_id = $arFields['ps'];
                // ID варианта доставки
                $ds_id = $arFields['ds'];

                // СОЗДАЁМ ЗАКАЗ
                $order = Sale\Order::create('s1', $user_id);

                // Передаём корзину в заказ
                $basket = Basket::create('s1');
                foreach( $arBasket as $basketItem ){
                    $item = $basket->createItem('catalog', $basketItem->getField('PRODUCT_ID'));
                    $obIblocks = CIBlock::GetByID($basketItem->el['IBLOCK_ID']);
                    if($iblock = $obIblocks->GetNext()){     $iblock_xml_id = $iblock['XML_ID'];     }
                    $item->setFields(array(
                        'QUANTITY' => $basketItem->getField('QUANTITY'),
                        'CURRENCY' => 'RUB',
                        'LID' => 's1',
                        'PRODUCT_XML_ID' => $basketItem->el['XML_ID'],
                        'CATALOG_XML_ID' => $iblock_xml_id,
                        'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
                    ));
                }
                $order->setBasket($basket);

                // Тип плательщика
                $order->setPersonTypeId(1);

                // Оплата
                $paymentCollection = $order->getPaymentCollection();
                $payment = $paymentCollection->createItem(Sale\PaySystem\Manager::getObjectById( $ps_id ));
                $payment->setField('SUM', $pay_sum); // Сумма к оплате
                $payment->setField('CURRENCY', 'RUB');

                // Доставка
                $shipmentCollection = $order->getShipmentCollection();
                $shipment = $shipmentCollection->createItem(Sale\Delivery\Services\Manager::getObjectById( $ds_id ));
                // ПРОИЗВОЛЬНАЯ СТОИМОСТЬ ДОСТАВКИ (ПРИ НЕОБХОДИМОСТИ)
                if ( !is_null($arFields['delivery_price']) ){
                    $shipment->setField('CUSTOM_PRICE_DELIVERY', 'Y');
                    $shipment->setField('PRICE_DELIVERY', $arFields['delivery_price']);
                }
                $shipmentItemCollection = $shipment->getShipmentItemCollection();
                foreach ($basket as $basketItem){
                    $item = $shipmentItemCollection->createItem($basketItem);
                    $item->setQuantity($basketItem->getQuantity());
                }

                // Получение информации о варианте доставки
                $ds_list = project\ds::getList();
                $arDS = $ds_list[$ds_id];

                // Заполняем свойства заказа
                $propertyCollection = $order->getPropertyCollection();
                $props = array(
                    1 => $arFields["name"],
                    2 => $arFields["last_name"],
                    3 => $arFields["phone"],
                    4 => $arFields["email"],
                    32 => (IS_ESHOP?"APP":"SITE")
                );

                $props[17] = $arFields["birthday"]?$arFields["birthday"]:'-';
                if( $arFields["gender"] == 'M' ){
                    $props[18] = 'мужской';
                } else if( $arFields["gender"] == 'F' ){
                    $props[18] = 'женский';
                } else {
                    $props[18] = '-';
                }

                $props[5] = $arFields["basketCity"];
                $props[29] = $arFields["loc_id"];

                if( $arDS['delivType'] == 'delivery' ){
                    $props[21] = $arFields["address"];
                    //$props[6] = $arFields["street"];
                    //$props[7] = $arFields["house"];
                    $props[8] = $arFields["podyezd"]?$arFields["podyezd"]:'-';
                    $props[9] = $arFields["floor"]?$arFields["floor"]:'-';
                    $props[10] = $arFields["kvartira"]?$arFields["kvartira"]:'-';
                    $props[11] = $arFields["lift"]?$arFields["lift"]:'-';
                    $props[12] = $arFields["comment"]?$arFields["comment"]:'-';
                    $props[13] = $arFields["deliveryDate"]?$arFields["deliveryDate"]:'-';
                    $props[14] = $arFields["deliveryTime"]?$arFields["deliveryTime"]:'-';
                } else if( $arDS['delivType'] == 'samovyvoz' ){
                    $dealer_shop = new project\dealer_shop();
                    $pvz = $dealer_shop->getByID($arFields["pvz"]);
                    $props[16] = $pvz['NAME'].' ['.$pvz['ID'].']';
                }
                $dealer = tools\user::info($arFields["dealer_id"]);
                $props[15] = htmlspecialchars_decode($dealer['UF_COMPANY_NAME']);
                $props[19] = $dealer['ID'];
                $props[23] = $arFields['pay_type'];
                $props[24] = $arFields['needRazgruzka'];
                $props[25] = $arFields['needPodyem'];
                $props[26] = $arFields['needPerenos'];
                if( strlen($arFields['perenosDistance']) > 0 ){
                    $props[27] = $arFields['perenosDistance'];
                }
                foreach ( $props as $prop_id => $value ){
                    $prop = $propertyCollection->getItemByOrderPropertyId($prop_id);
                    $prop->setValue($value);
                }

                foreach ( $props as $prop_id => $value ){
                    $prop = $propertyCollection->getItemByOrderPropertyId($prop_id);
                    $prop->setValue($value);
                }

                // Сохраняем новый заказ
                $result = $order->save();

                // Определяем ID нового заказа
                $order_id = $order->GetId();

                if (intval($order_id) > 0){

                    // Уменьшение остатков товара
                    foreach ( $arBasket as $basketItem ){
                        $sku_id = $basketItem->getProductId();
                        $basketItemQuantity = $basketItem->getQuantity();
                        $product = \CCatalogProduct::GetByID($sku_id);
                        $newQuantity = $product['QUANTITY'] - $basketItemQuantity;
                        if( $newQuantity < 0 ){    $newQuantity = 0;    }
                        $res = \CCatalogProduct::Update($sku_id, array( 'QUANTITY' => $newQuantity ));
                    }
                    BXClearCache(true, "/tp_list/");

                    // Установка стартового статуса заказу
                    $order_status = new project\order_status();
                    $order_status->set( $order_id, 'A' );

                    // Очищаем корзину
                    \CSaleBasket::DeleteAll(\CSaleBasket::GetBasketUserID());

                    $userProps = '<h2>Личные данные заказчика</h2>';
                    $userProps .= '<p>Фамилия: '.$arFields["last_name"].';</p>';
                    $userProps .= '<p>Имя: '.$arFields["name"].';</p>';
                    $userProps .= '<p>Телефон: '.$arFields["phone"].';</p>';
                    $userProps .= '<p>Эл. почта: '.$arFields["email"].';</p>';
                    $userProps .= '<p>Дата рождения покупателя: '.($arFields["birthday"]?$arFields["birthday"]:'-').';</p>';

                    if( $arFields["gender"] == 'M' ){
                        $gender = 'мужской';
                    } else if( $arFields["gender"] == 'F' ){
                        $gender = 'женский';
                    } else {
                        $gender = '-';
                    }
                    $userProps .= '<p>Пол: '.$gender.';</p>';

                    $userProps .= '<h2>Информация о заказе</h2>';
                    $dealer = tools\user::info($arFields["dealer_id"]);
                    $userProps .= '<p>Дилер: '.($dealer['UF_COMPANY_NAME'].' ['.$dealer['ID'].']').';</p>';

                    $arPS = \CSalePaySystem::GetByID($ps_id);
                    $userProps .= '<p>Вариант оплаты: '.$arPS["NAME"].';</p>';
                    $userProps .= '<p>Вариант доставки: '.$arDS["NAME"].';</p>';

                    if( $arDS['delivType'] == 'delivery' ){
                        $userProps .= '<h2>Информация для доставки</h2>';
                        $userProps .= '<p>Адрес: '.$arFields["address"].';</p>';
                        //$userProps .= '<p>Город: '.$arFields["basketCity"].';</p>';
                        //$userProps .= '<p>Улица: '.$arFields["street"].';</p>';
                        $userProps .= '<p>Дом: '.$arFields["house"].';</p>';
                        $userProps .= '<p>Подъезд: '.($arFields["podyezd"]?$arFields["podyezd"]:'-').';</p>';
                        $userProps .= '<p>Этаж: '.($arFields["floor"]?$arFields["floor"]:'-').';</p>';
                        $userProps .= '<p>Кв./офис: '.($arFields["kvartira"]?$arFields["kvartira"]:'-').';</p>';
                        $userProps .= '<p>Наличие лифта: '.($arFields["lift"]?$arFields["lift"]:'-').';</p>';
                        $userProps .= '<p>Желательная дата доставки: '.($arFields["deliveryDate"]?$arFields["deliveryDate"]:'-').';</p>';
                        $userProps .= '<p>Желательное время доставки: '.($arFields["deliveryTime"]?$arFields["deliveryTime"]:'-').';</p>';
                        $userProps .= '<p>Комментарий: '.($arFields["comment"]?$arFields["comment"]:'-').';</p>';
                    } else if( $arDS['delivType'] == 'samovyvoz' ){
                        $userProps .= '<p>Пункт выдачи: '.$pvz['NAME'].' ['.$pvz['ID'].']'.';</p>';
                    }
                    // Формируем таблицу для письма
                    $arProductsTable = '<table width="100%" border="1" cellpadding="4" cellspacing="4" style="border-collapse:collapse; font-size: 13px;">';
                    $arProductsTable .= '<tr>
                        <td><b>Товар</b></td>
                        <td><b>Цена за ед., руб.</b></td>
                        <td><b>Количество</b></td>
                        <td><b>Сумма, руб.</b></td>
                    </tr>';

                    foreach ($arBasket as $basketItem){

                        // Инфо о товаре
                        $product = $basketItem->product;
                        $price = $basketItem->price;
                        $discPrice = $basketItem->discPrice;
                        $quantity = $basketItem->getField('QUANTITY');

                        $arProductsTable .= '<tr>
                            <td>'.$product['NAME'].'</td>
                            <td>'.number_format($discPrice, $rounding, ",", " ").'</td>
                            <td>'.round($quantity, 0).'</td>
                            <td>'.number_format($discPrice * $quantity, $rounding, ",", " ").'</td>
                        </tr>';
                    }
                    $arProductsTable .= '</table>';

                    $disc_sum = number_format($basket_disc_sum, $rounding, ",", " ");
                    $delivery_price = number_format($arFields['delivery_price'], $rounding, ",", " ");
                    $pay_sum = number_format($pay_sum, $rounding, ",", " ");

// Письмо покупателю
                    $content_html = '<h3 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: Arial, Helvetica, sans-serif; font-size: 22px; font-weight: normal; line-height: 26px; margin: 0; margin-bottom: 26px; padding: 0; text-align: left; word-wrap: normal;">Уважаемый(ая) '.$arFields["last_name"].' '.$arFields["name"].',</h3>';

                    $content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Вы оформили заказ №'.$order_id.'.</p>';

                    $content_html .= $userProps;

                    $content_html .= $arProductsTable;

                    $content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Стоимость доставки: '.$delivery_price.' руб.</p>';

                    $content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Итого, к оплате: '.$pay_sum.' руб.</p>';

                    $content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Благодарим Вас за покупку в онлайн-магазине<br> <a target="_blank" href="'.$_SERVER['REQUEST_SCHEME'].'://'.$server_name.'/" style="Margin: 0; color: #009fe3; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left; text-decoration: none;">'.$server_name.'</a></p>';

                    $content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Вы можете отслеживать статус заказа в <a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$server_name.'/personal/#showtab-tab_111_4" style="Margin: 0; color: #009fe3; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left; text-decoration: none;">Личном кабинете</a></p>';

                    $mail_fields = Array(
                        "TITLE" => 'Новый заказ №'.$order_id.' на сайте '.$server_name,
                        "HTML" => project\mail::header().$content_html.project\mail::footer(),
                        "EMAIL_TO" => $arFields["email"]
                    );
                    \CEvent::SendImmediate("MAIN", "s1", $mail_fields);

// Письмо дилеру
                    $content_html = '<h3 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: Arial, Helvetica, sans-serif; font-size: 22px; font-weight: normal; line-height: 26px; margin: 0; margin-bottom: 26px; padding: 0; text-align: left; word-wrap: normal;">Новый заказ №'.$order_id.'</h3>';

                    $content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">'.date('d.m.Y').' в '.date('H:i').' оформлен заказ у вашей компании '.$dealer["UF_COMPANY_NAME"].'.</p>';

                    $content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Номер нового заказа: '.$order_id.'</p>';

                    $content_html .= $userProps;

                    $content_html .= $arProductsTable;

                    $content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Стоимость доставки: '.$delivery_price.' руб.</p>';

                    $content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Итого, к оплате: '.$pay_sum.' руб.</p>';

                    $dealerEmailTo = isset($dealer['UF_EMAIL_FOR_ORDERS'])&&strlen($dealer['UF_EMAIL_FOR_ORDERS'])>0?$dealer['UF_EMAIL_FOR_ORDERS']:$dealer['EMAIL'];
                    $mail_fields = Array(
                        "TITLE" => 'Новый заказ №'.$order_id.' на сайте '.$server_name,
                        "HTML" => project\mail::header().$content_html.project\mail::footer(),
                        "EMAIL_TO" => $dealerEmailTo
                    );
                    $commonEmailTo = trim(strtolower(\Bitrix\Main\Config\Option::get('aoptima.project', 'COMMON_ORDERS_EMAIL_TO')));
                    if( strlen($commonEmailTo) > 0 ){
                        $mail_fields["COPY_EMAIL_TO"] .= $commonEmailTo;
                    }
                    \CEvent::SendImmediate("MAIN", "s1", $mail_fields);

                    // Ответ
                    echo json_encode( Array(
                        "status" => "ok",
                        "order_id" => $order_id,
                        "ps_id" => $ps_id
                    ) );
                    return;

                } else {
                    // Ответ
                    echo json_encode( Array( "status" => "error", 'text' => $result->getErrorMessages() ) );
                    return;
                }

            } else {
                // Ответ
                echo json_encode( Array( "status" => "user_error" ) ); return;
            }

        }

        // Ответ
        echo json_encode( Array( "status" => "error", 'text' => 'Ошибка оформления заказа' ) );
        return;

    }





    // Пожелания/предложения
    if ($action == "suggestion"){
        /////////////////////////////////
        $params['iblock_code'] = 'suggestions';
        $params['iblock_name'] = 'Пожелания и предложения';
        ////////////////////////////////
        $params['settings'] = array(
            'city' => 'Населённый пункт',
            'theme_name' => 'Тема обращения',
            'name' => 'Имя',
            'last_name' => 'Фамилия',
            'phone' => 'Телефон',
            'email' => 'Эл. почта',
            'message' => 'Текст сообщения',
        );
        $params['email_to'] = \Bitrix\Main\Config\Option::get('aoptima.project', 'POZH_FORM_EMAIL_TO');

        $arFields = [];
        parse_str($_POST["form_data"], $arFields);

        // если выбрана тема обращения
        if($arFields["theme"])
        {
            // получим данные темы
            $dbTheme = CIBlockElement::GetList(
                Array(
                    "SORT" => "ASC",
                    "ID" => "ASC"
                ),
                Array(
                    "IBLOCK_ID" => 18,
                    "ACTIVE" => "Y",
                    "ID" => $arFields["theme"]
                ),
                false,
                false,
                Array(
                    "ID",
                    "NAME",
                    "PROPERTY_EMAIL"
                )
            );
            if($arTheme = $dbTheme->Fetch())
            {
                $arFields["theme_name"] = $arTheme["NAME"];

                if($arTheme["PROPERTY_EMAIL_VALUE"])
                {
                    $params['email_to'] = $arTheme["PROPERTY_EMAIL_VALUE"];
                }
            }
        }

        /////////////////////////////////
        $params['mail_title'] = 'Новое сообщение из формы обратной связи (сайт "'.$server_name.'")';
        $params['mail_html'] = '<p>Новое сообщение из формы обратной связи на сайте "'.$server_name.'"</p><hr>';
        /////////////////////////////////
        // Отправка
        $res = tools\feedback::send( $params, $arFields);
        if( $res ){
            echo json_encode( array("status" => 'ok', "res" => $res) ); return;
        } else {
            echo json_encode( array("status" => 'error', 'text' => 'Ошибка отправки') ); return;
        }
    }






    // Обратная связь
    if ($action == "feedback"){
        // данные в массив
        $arFields = Array();
        parse_str(tools\funcs::param_post("form_data"), $arFields);
        $PROP[5] = $arFields["name"];
        $PROP[6] = $arFields["email"];
        $el = new CIBlockElement;
        $fields = Array("IBLOCK_ID" => 3, "PROPERTY_VALUES" => $PROP, "NAME" => 'Новое сообщение', "PREVIEW_TEXT" => $arFields["message"], "ACTIVE" => "Y");
        if ($ID = $el->Add($fields)){
            // отправка инфы на почту
            $arEventFields = Array("NAME" => $arFields["name"], "EMAIL" => $arFields["email"], "MESSAGE" => $arFields["message"],);
            CEvent::Send("FEEDBACK", "s1", $arEventFields);
            // Ответ
            echo json_encode(array("status" => 'ok'));
        }
    }



}