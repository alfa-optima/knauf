<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $user = new project\user();
    if( $USER->IsAuthorized() && $user->isDealer() ){

        if( intval($_POST['item_id']) > 0 ){

            $el = tools\el::info($_POST['item_id']);
            $el = project\catalog::updateProductName($el);

            if( intval($el['DETAIL_PICTURE']) > 0 ){
                $el['IMG'] = tools\funcs::rIMGG($el['DETAIL_PICTURE'], 5, 184, 139);
            } else {
                $el['IMG'] = SITE_TEMPLATE_PATH."/images/no_image.png";
            }

            if( intval($el['ID']) > 0 ){

                ob_start();
                	$APPLICATION->IncludeComponent(
                	    "aoptima:newPriceOfferItem", "",
                        array('NUMBER' => 1, 'el' => $el)
                    );
                	$items_html = ob_get_contents();
                ob_end_clean();

                // Ответ
                echo json_encode(
                    Array(
                        "status" => "ok",
                        "el" => $el,
                        "items_html" => $items_html
                    )
                );
                return;
            }

        }

    } else {
        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;