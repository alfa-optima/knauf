<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
    &&
    $USER->IsAuthorized()
){

    $templates = Array();
    parse_str($_POST["templates"], $templates);

    if(
        is_array($templates['ID'])
        &&
        count($templates['ID']) > 0
    ){

        $ok = true;

        foreach( $templates['ID'] as $key => $id ){

            $obTemplate = new project\delivery_template();

            $arData = array(
                'UF_TEMPLATE' => strip_tags($templates['UF_TEMPLATE'][$key]),
                //'UF_LOC_ID' => strip_tags($templates['UF_LOC_ID'][$key]),
                'UF_ADDRESS' => strip_tags($templates['UF_ADDRESS'][$key]),
                'UF_COORDS' => strip_tags($templates['UF_COORDS'][$key]),
                //'UF_STREET' => strip_tags($templates['UF_STREET'][$key]),
                //'UF_HOUSE' => strip_tags($templates['UF_HOUSE'][$key]),
                'UF_PODYEZD' => strip_tags($templates['UF_PODYEZD'][$key]),
                'UF_FLOOR' => strip_tags($templates['UF_FLOOR'][$key]),
                'UF_KVARTIRA' => strip_tags($templates['UF_KVARTIRA'][$key]),
                'UF_COMMENT' => strip_tags($templates['UF_COMMENT'][$key]),
            );

            // Лифт
            $liftEnumXMLID = strip_tags($templates['UF_LIFT'][$key]);
            if( strlen($liftEnumXMLID) > 0 ){
                $enums = CUserFieldEnum::GetList(array(), array(
                    "XML_ID" => $liftEnumXMLID
                ));
                while($enum = $enums->GetNext()){
                    $arData['UF_LIFT'] = $enum['ID'];
                }
            }


            if( $id == 'new' ){
                // Создаём новый шаблон доставки
                $arData['UF_USER'] = strip_tags($USER->GetID());
                $res = $obTemplate->add($arData);
                if( !$res ){    $ok = false;    }
            } else if( intval($id) > 0 ){
                // Редактируем шаблон доставки
                $res = $obTemplate->update($id, $arData);
                if( !$res ){    $ok = false;    }
            }

        }

        if( $ok ){

            ob_start();
                $APPLICATION->IncludeComponent(
                    "aoptima:personalDeliveryTemplates", ""
                );
                $html = ob_get_contents();
            ob_end_clean();

            // Ответ
            echo json_encode([
                "status" => "ok",
                "html" => $html
            ]);
            return;

        }

    }

}

// Ответ
echo json_encode([
    "status" => "error",
    "text" => "При сохранении произошла ошибка"
]);
return;