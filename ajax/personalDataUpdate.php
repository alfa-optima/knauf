<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    if( $USER->IsAuthorized() ){

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        $arUser = tools\user::info($USER->GetID());
        $user_groups = \CUser::GetUserGroup($USER->GetID());

        $isSocUser = $arUser['EXTERNAL_AUTH_ID']=='socservices'?'Y':'N';
        $isCallCenterUser =  in_array( project\user::CALL_CENTER_GROUP_ID, $user_groups )?'Y':'N';

        if(
            strlen($arFields["EMAIL"]) > 0
            &&
            $isSocUser != 'Y'
            &&
            $isCallCenterUser != 'Y'
        ){

            // проверяем EMAIL на логин
            $filter = Array(
                "LOGIN_EQUAL" => strip_tags($arFields["EMAIL"]),
                "!ID" => $USER->GetID(),
                "!EXTERNAL_AUTH_ID" => "socservices"
            );
            $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter);
            while ($user = $rsUsers->GetNext()){
                // Если Email уже есть
                echo json_encode( Array( "status" => "error", "text" => "Данный адрес электронной почты уже зарегистрирован на&nbsp;сайте!" ) );  return;
            }
            // проверяем EMAIL на наличие у других пользователей
            $filter = Array(
                "=EMAIL" => strip_tags($arFields["EMAIL"]),
                "!ID" => $USER->GetID(),
                "!EXTERNAL_AUTH_ID" => "socservices"
            );
            $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter);
            while ($user = $rsUsers->GetNext()){
                // Если Email уже есть
                echo json_encode( Array( "status" => "error", "text" => "Данный адрес электронной почты уже зарегистрирован на&nbsp;сайте!" ) );  return;
            }

        }

        // поля для сохранения
        $arUserFields = Array(
            "NAME" => strip_tags($arFields["NAME"]),
            "LAST_NAME" => strip_tags($arFields["LAST_NAME"]),
            "PERSONAL_PHONE" => strip_tags($arFields["PERSONAL_PHONE"]),
            "EMAIL" => strip_tags($arFields["EMAIL"]),
            "PERSONAL_BIRTHDAY" => strip_tags($arFields["PERSONAL_BIRTHDAY"]),
        );

        // логин
        if(
            strlen($arFields["LOGIN"]) > 0
            &&
            $isSocUser != 'Y'
            &&
            $isCallCenterUser != 'Y'
        ){
            $arUserFields['LOGIN'] = strip_tags($arFields["LOGIN"]);
            // проверяем логин
            $filter = Array(
                "LOGIN_EQUAL" => strip_tags($arFields["LOGIN"]),
                "!ID" => $USER->GetID()
            );
            $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter);
            while ($user = $rsUsers->GetNext()){
                // Если Логин уже есть
                echo json_encode( Array( "status" => "error", "text" => "Данный логин уже зарегистрирован на&nbsp;сайте!" ) );  return;
            }
        }
        // пол
        if( strlen($arFields["PERSONAL_GENDER"]) > 0 ){
            $arUserFields['PERSONAL_GENDER'] = strip_tags($arFields["PERSONAL_GENDER"]);
        }
        
        // Сохраняем
        $obUser = new \CUser;
        $res = $obUser->Update($USER->GetID(), $arUserFields);
        if( $res ){
            // Ответ
            echo json_encode(Array("status" => "ok"));
            return;
        } else {
            tools\logger::addError('Ошибка сохранения личных данных [user_id='.$USER->GetID().']:'.$obUser->LAST_ERROR);
        }

    } else {
        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка сохранения данных"));
return;