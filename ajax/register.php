<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    if( !$USER->IsAuthorized() ){

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        // проверяем EMAIL
        $checkUser = project\user::getByLogin($arFields["EMAIL"]);
        if( !$checkUser ){
            $checkUser = project\user::getByEmail($arFields["EMAIL"]);
        }

        if( intval( $checkUser['ID'] ) > 0 ){
            // Если Email уже есть
            echo json_encode( Array( "status" => "error", "text" => "Данный адрес электронной почты уже зарегистрирован на&nbsp;сайте!" ) );  return;
        }

        $arUserFields = Array(
            "NAME" => strip_tags($arFields["NAME"]),
            "LAST_NAME" => strip_tags($arFields["LAST_NAME"]),
            "LOGIN" => strip_tags($arFields["EMAIL"]),
            "EMAIL" => strip_tags($arFields["EMAIL"]),
            "PASSWORD" => strip_tags($arFields["PASSWORD"]),
            "CONFIRM_PASSWORD" => strip_tags($arFields["CONFIRM_PASSWORD"]),
            "PERSONAL_BIRTHDAY" => strip_tags($arFields["PERSONAL_BIRTHDAY"]),
            "GROUP_ID" => Array(7)
        );

        // пол
        if( strlen($arFields["PERSONAL_GENDER"]) > 0 ){
            $arUserFields['PERSONAL_GENDER'] = strip_tags($arFields["PERSONAL_GENDER"]);
        }

        $arUserFields['UF_REG_CODE'] = md5(
            randString( 20, array( "abcdefghijklmnopqrstuvwxyz" ) ).time()
        );

        // Создаём пользователя
        $user = new \CUser;
        $userID = $user->Add($arUserFields);
        if( intval($userID) > 0 ){

            // Подписка на рассылку
            if( $arFields["subscribe"] == 'Y' ){
                $obSubscribe = new project\subscribe();
                $currentEmailRubrics = $obSubscribe->getEmailRubrics($arFields["EMAIL"]);
                if( count($currentEmailRubrics) == 0 ){
                    $allRubrics = array_keys($obSubscribe->rubricsList());
                    $res = $obSubscribe->save(
                        strip_tags($arFields["EMAIL"]),
                        $allRubrics
                    );
                }
            }

            // Отправка рег. информации
            tools\user::sendRegInfo($userID);

            // Ссылка подтверждения регистрации
            tools\user::sendRegConfirmLink($userID);

            // Ответ
            echo json_encode(Array("status" => "ok"));
            return;

        } else {
            tools\logger::addError('Ошибка регистрации пользователя:'.$user->LAST_ERROR);
        }

    } else {
        // Ответ
        echo json_encode(Array("status" => "error", "text" => "В данный момент Вы авторизованы на сайте"));
        return;
    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка регистрации"));
return;