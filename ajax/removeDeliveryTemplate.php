<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
    &&
    intval($_POST['id']) > 0
){

    if( $USER->IsAuthorized() ){

        $obTemplate = new project\delivery_template();
        $res = $obTemplate->delete($_POST['id']);
        if( $res ){

            $delivery_template = new project\delivery_template();
            $delivery_templates = $delivery_template->getList($USER->GetID());

            ob_start();
            foreach( $delivery_templates as $template ){
                $APPLICATION->IncludeComponent(
                    "aoptima:delivery_template_item", "",
                    array('template' => $template)
                );
            }
            if( count($delivery_templates) == 0 ){
                // Пустой шаблон
                $APPLICATION->IncludeComponent("aoptima:delivery_template_item", "");
            }
            $user_templates_html = ob_get_contents();
            ob_end_clean();

            // Ответ
            echo json_encode(Array("status" => "ok", "user_templates_html" => $user_templates_html));
            return;

        }

    } else {

        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка удаления шаблона доставки"));
return;