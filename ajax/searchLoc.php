<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('sale');
use Bitrix\Sale;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $results = array();

    $term = strip_tags($_GET['term']);

    $locs = \Bitrix\Sale\Location\LocationTable::getList(array(
        'filter' => array(
            'NAME.NAME' => $term."%",
            '=NAME.LANGUAGE_ID' => 'ru',
            "TYPE.CODE" => array( 'REGION', 'SUBREGION', 'CITY' ),
            '=PARENT.NAME.LANGUAGE_ID' => 'ru',
        ),
        'select' => array(
            '*',
            'NAME_RU' => 'NAME.NAME',
            'TYPE_CODE' => 'TYPE.CODE',
            'PARENT_NAME_RU' => 'PARENT.NAME.NAME',
            'ZIP' => 'EXTERNAL.XML_ID'
        ),
        'limit' => 15
    ));
    while( $loc = $locs->fetch() ){
        $results[$loc['ID']] = array(
            'label' => $loc['NAME_RU'].' ('.$loc['PARENT_NAME_RU'].')',
            "short_name" => $loc['NAME_RU'],
            "loc_type" => $loc['TYPE_CODE'],
            "value" => $loc['ID']
        );
    }

    if( count($results) == 0 ){

        $locs = \Bitrix\Sale\Location\LocationTable::getList(array(
            'filter' => array(
                'NAME.NAME' => "%".$term."%",
                '=NAME.LANGUAGE_ID' => 'ru',
                "TYPE.CODE" => array( 'REGION', 'SUBREGION', 'CITY' ),
                '=PARENT.NAME.LANGUAGE_ID' => 'ru',
            ),
            'select' => array(
                '*',
                'NAME_RU' => 'NAME.NAME',
                'TYPE_CODE' => 'TYPE.CODE',
                'PARENT_NAME_RU' => 'PARENT.NAME.NAME',
                'ZIP' => 'EXTERNAL.XML_ID'
            ),
            'limit' => 15
        ));
        while( $loc = $locs->fetch() ){
            $results[$loc['ID']] = array(
                'label' => $loc['NAME_RU'].' ('.$loc['PARENT_NAME_RU'].')',
                "short_name" => $loc['NAME_RU'],
                "loc_type" => $loc['TYPE_CODE'],
                "value" => $loc['ID']
            );
        }

    }

    echo json_encode($results);


}



