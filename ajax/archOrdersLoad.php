<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $user = new project\user();
    if( $USER->IsAuthorized() && $user->isDealer() ){

        ob_start();
            // Архив заказов дилера
            $APPLICATION->IncludeComponent(
                "aoptima:dealer_orders_archive", "",
                array( 'IS_AJAX' => 'Y', 'IS_LOAD' => 'Y' )
            );
        	$html = ob_get_contents();
        ob_end_clean();

        // Ответ
        echo json_encode(Array("status" => "ok", "html" => $html));
        return;

    } else {

        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }
}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;