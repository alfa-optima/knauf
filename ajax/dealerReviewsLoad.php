<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $user = new project\user();
    if( $USER->IsAuthorized() && $user->isDealer() ){

        // tovarReviews
        $template = "";
        $params = array(
            'IS_LOAD' => 'Y',
            'IS_AJAX' => 'Y'
        );
        if( $_POST['is_dealer_page'] == 'Y' ){
            $template = "dealer_page";
            $params['IS_DEALER_PAGE'] = 'Y';
            $params['DEALER_ID'] = $USER->GetID();
        }
        ob_start();
            $APPLICATION->IncludeComponent(
                "aoptima:personalOrderReviews", $template, $params
            );
            $html = ob_get_contents();
        ob_end_clean();

        // Ответ
        echo json_encode(Array("status" => "ok", "html" => $html));
        return;

    } else {
        // Ответ
        echo json_encode(Array("status" => "error", "text" => 'Ошибка авторизации'));
        return;
    }

}