<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $user = new project\user();
    if( $USER->IsAuthorized() && $user->isDealer() ){

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);
        
        $server_name = \Bitrix\Main\Config\Option::get('main', 'server_name');

        // Заказы дилера
        $dealer_orders = project\order::getDealerOrders( $USER->GetID() );

        // Сохранение комментариев дилера к заказам
        if( is_array($arFields['dealer_order_comments']) ){
            foreach ( $arFields['dealer_order_comments'] as $order_id => $comment ){
                $obOrder = \Bitrix\Sale\Order::load($order_id);
                project\order::setPropValue( $obOrder, 'DEALER_COMMENT', strip_tags($comment) );
            }
        }

        // Сохранение статусов заказов
        if( is_array($arFields['statuses']) ){
            $statuses = project\order_status::getList();
            foreach ( $arFields['statuses'] as $order_id => $status_code ){
                // Если это заказ принадлежит данному дилеру
                if( in_array( $order_id, $dealer_orders) ){
                    // Получим текущий статус
                    $obOrder = \Bitrix\Sale\Order::load($order_id);
                    $propValues = project\order::getOrderPropValues($obOrder);
                    $os = new project\order_status();
                    $cur_status = $os->get( $obOrder, $propValues );

                    $user_id = $obOrder->getUserId();

                    $clientEmail = project\order::getPropValue( $propValues, 'EMAIL' );
                    $clientName = project\order::getPropValue( $propValues, 'NAME' );

                    // Если статус отличается от текущего
                    if( $cur_status != $status_code ){

                        // Сохраним изменение
                        $os->set( $order_id, $status_code );

                        // Отправим на Email сообщение покупателю
                        if( $clientEmail ){

                            $content_html = '<h3 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: Arial, Helvetica, sans-serif; font-size: 22px; font-weight: normal; line-height: 26px; margin: 0; margin-bottom: 26px; padding: 0; text-align: left; word-wrap: normal;">Уважаемый(ая) '.$clientName.',</h3>';

                            if( $status_code == 'Z' ){

                                $content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Заказ №'.$order_id.' переведён в статус  <strong>"'.ucfirst(strtolower($statuses[$status_code]['NAME'])).'"</strong>.</p>';

                                $content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Благодарим Вас за покупку в онлайн-магазине<br> <a target="_blank" href="'.$_SERVER['REQUEST_SCHEME'].'://'.$server_name.'" style="Margin: 0; color: #009fe3; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left; text-decoration: none;">'.$server_name.'</a></p>';

                                if( $user_id != project\order::SERVICE_USER_ID ){

                                    //$content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Просим Вас дать оценку качества выполнения заказа и ответить на вопросы анкеты. Это поможет нам объективно оценить работу и сделать для Вас покупки в <a target="_blank" href="'.$_SERVER['REQUEST_SCHEME'].'://'.$server_name.'" style="Margin: 0; color: #009fe3; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left; text-decoration: none;">'.$server_name.'</a> максимально комфортными и удобными.</p>';

                                    //$content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Благодарим Вас за участие!</p>';

                                    $content_html .= '<table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;"><tbody><tr style="padding: 0; text-align: left; vertical-align: top;"><th class="small-12 large-12 columns greyBlock" style="Margin: 0 auto; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 13px; padding-right: 13px; text-align: left; width: 564px;"><table style="background: #e0dad4; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;"><tbody><tr style="padding: 0; text-align: left; vertical-align: top;"><th class="center-all" style="Margin: 0; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.35; margin: 0; padding: 19px; text-align: left;">';

                                    $content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Вы можете оставить отзыв о заказе в Личном кабинете на вкладке "История заказов"</p>';

                                    $content_html .= '<table class="spacer" style="background: #e0dad4; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;"><tbody><tr style="padding: 0; text-align: left; vertical-align: top;"><td height="26px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 0px; font-weight: normal; hyphens: auto; line-height: 26px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&amp;nbsp;</td></tr></tbody></table>';

                                    $content_html .= '<table class="button" style="Margin: 0 0 16px 0; background: #e0dad4; border-collapse: collapse; border-spacing: 0; margin: 0 0 16px 0; margin-bottom: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;"><tbody><tr style="padding: 0; text-align: left; vertical-align: top;"><td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; hyphens: auto; line-height: 1.35; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">';

                                    $content_html .= '<a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$server_name.'/personal/#showtab-tab_111_4" style="Margin: 0; background: #029ee5; border: 0 solid #2199e8; border-radius: 0px; color: #ffffff; display: inline-block; font-family: Arial, Helvetica, sans-serif; font-size: 18px; font-weight: normal; height: 40px; line-height: 1; margin: 0; padding: 0; padding-top: 20px; text-align: center; text-decoration: none; width: 100%;">Личный кабинет</a>';

                                    $content_html .= '</td></tr></tbody></table></th></tr></tbody></table></th></tr></tbody></table>';

                                    $content_html .= '<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;"><tbody><tr style="padding: 0; text-align: left; vertical-align: top;"><td height="22px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 0px; font-weight: normal; hyphens: auto; line-height: 22px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&amp;nbsp;</td></tr></tbody></table>';

                                }

                            } else {

                                $content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Заказ №'.$order_id.' переведён в статус  <strong>"'.ucfirst(strtolower($statuses[$status_code]['NAME'])).'"</strong>.</p>';

                                if( $user_id != project\order::SERVICE_USER_ID ){

                                    $content_html .= '<p style="Margin: 0; Margin-bottom: 10px; color: #0a0a0a; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 16px; margin-top: 0; padding: 0; text-align: left;">Вы можете отслеживать статус заказа в <a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$server_name.'/personal/#showtab-tab_111_4" style="Margin: 0; color: #009fe3; font-family: Arial, Helvetica, sans-serif; font-weight: normal; line-height: 1.35; margin: 0; padding: 0; text-align: left; text-decoration: none;">Личном кабинете</a></p>';

                                }

                            }

                            $title = 'Изменение статуса заказа №'.$order_id.' на сайте "'.$server_name.'"';
                            if( $status_code == 'Z' ){
                                $title = 'Заказ №'.$order_id.' доставлен (сайт "'.$server_name.'")';
                            }
    
                            $mail_fields = Array(
                                "TITLE" => $title,
                                "HTML" => project\mail::header().$content_html.project\mail::footer(),
                                "EMAIL_TO" => $clientEmail
                            );
                            
                            \CEvent::SendImmediate("MAIN", "s1", $mail_fields);
                            
                        }

                    }
                }
            }
        }

        ob_start();
            // Заказы в адрес дилера
            $APPLICATION->IncludeComponent(
                "aoptima:personalDealerOrders", "",
                array('DEALER_ID' => $USER->GetID(), 'IS_AJAX' => 'Y')
            );
            $html = ob_get_contents();
        ob_end_clean();

        // Ответ
        echo json_encode(Array("status" => "ok", "html" => $html));
        return;

    } else {

        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }
}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;