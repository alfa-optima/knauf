<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $deliveryTemplates = array();
    if ($USER->IsAuthorized()){
        // Шаблоны доставки
        $deliveryTemplate = new project\delivery_template();
        $deliveryTemplates = $deliveryTemplate->getList($USER->GetID(), false, false );
    }
    
    $html = '<select id="deliveryTemplates" name="deliveryTemplate" onchange="deliveryTemplateChange( $(this) );">';
    $html .= '<option selected value="empty">--- Шаблон не выбран ---</option>';
    foreach ( $deliveryTemplates as $deliveryTemplateID => $deliveryTemplate ){
        $html .= '<option value="'.$deliveryTemplateID.'">'.$deliveryTemplate['UF_TEMPLATE'].'</option>';
    }
    $html .= '</select><script src="'.SITE_TEMPLATE_PATH.'/js/default_scripts.js"></script>';
    
	// Ответ
	echo json_encode(Array("status" => "ok", "html" => $html));
	return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;