<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
    &&
    intval($_POST['reviews_tov_id']) > 0
    &&
    is_array($_POST['items'])
    &&
    count($_POST['items']) > 0
){

    ob_start();
        // tovarReviews
        $APPLICATION->IncludeComponent(
            "aoptima:tovarReviews", "",
            array(
                'el_id' => $_POST['reviews_tov_id'],
                'stop_items' => $_POST['items'],
                'IS_AJAX' => 'Y'
            )
        );
        $html = ob_get_contents();
    ob_end_clean();

    // Ответ
    echo json_encode(Array("status" => "ok", "html" => $html));
    return;

}