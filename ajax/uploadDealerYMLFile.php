<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$user = new project\user();
if( $USER->IsAuthorized() && $user->isDealer() ){

    $arResult['ERRORS'] = array();

    $_SESSION['YML_FILE_PATH'] = false;
    $_SESSION['CHECKED_OFFERS'] = array();
    $_SESSION['ERROR_OFFERS'] = array();
    $_SESSION['SUCCESS_OFFERS_CNT'] = 0;
    $_SESSION['UPLOAD_FILE_LOC_ID'] = false;

    if( intval($_POST['loc_id']) > 0 ){

        $_SESSION['UPLOAD_FILE_LOC_ID'] = $_POST['loc_id'];

        if(
            $_POST['yml_load_type'] == 'file'
            &&
            intval($_FILES['yml']['size']) > 0
        ){

            $dealer_yml_file = new project\dealer_yml_file($_FILES, $USER->GetID());
            $uploadResult = $dealer_yml_file->upload();

            if( $uploadResult['status'] == 'ok' ){

                $_SESSION['YML_FILE_PATH'] = $uploadResult['file_path'];

                echo '<script type="text/javascript">
                window.parent.uploadYMLResponse(\'{"status": "ok"}\');
                </script>';

            } else if( $uploadResult['status'] == 'error' ){

                $arResult['ERRORS'][] = $uploadResult['text'];

                echo '<script type="text/javascript">
                window.parent.uploadYMLResponse({"status": "error", "text": "'.implode('<br>', $arResult['ERRORS']).'"});
                </script>';

            }

        } else if(
            $_POST['yml_load_type'] == 'link'
            &&
            strlen($_POST['yml_link']) > 0
        ){

            $link = trim(strip_tags($_POST['yml_link']));

            // Сделаем запрос на URL
            $client = new Client(array( 'timeout' => 10, 'verify' => false ));
            try {

                $response = $client->request( 'GET', $link );

                // код ответа 200
                if( $response->getStatusCode() == 200 ){

                    $body = $response->getBody();
                    $content = $body->getContents();

                    $cod = mb_detect_encoding($xml_content, array('utf-8', 'cp1251'));
                    if( $cod != 'UTF-8' ){
                        $content = mb_convert_encoding($content, 'UTF-8', $cod);
                    }
                    $content = html_entity_decode($content);

                    // сформируем имя и путь нового файла
                    $ar = explode('/', $link);
                    $f_name = $ar[count($ar)-1];
                    $ar = explode('.', $f_name);
                    $ex = $ar[count($ar)-1];
                    $file_name = md5( time() ).'.'.$ex;
                    $file_path = $_SERVER['DOCUMENT_ROOT'].project\dealer_yml_file::TEMP_UPLOAD_PATH.$file_name;

                    // Пробуем открыть файл для скачивания
                    $ReadFile = fopen( $link, "rb");

                    // Получилось открыть файл
                    if ( $ReadFile ){

                        // Выкачиваем файл
                        $WriteFile = fopen( $file_path, "wb" );
                        if ( $WriteFile ){
                            while( !feof($ReadFile) ){
                                fwrite($WriteFile, fread($ReadFile, 4096));
                            }
                            fclose($WriteFile);
                        }
                        fclose($ReadFile);

                    // НЕ получилось открыть файл
                    } else if( strlen($content) > 0 ){

                        // тогда запишем полученный контент в файл
                        $file = fopen($file_path, "w");
                        $res = fwrite($file,  $content);
                        fclose($file);
                    }

                    if( file_exists( $file_path ) ){
                        
                        $upload = new project\dealer_yml_file();
                        $upload->file_name = $file_name;
                        $upload->file_path = $file_path;
                        $upload->file_moved = true;

                        $check_status = $upload->checkAsFile();
                        
                        if ( $check_status == 'ok' ){

                            $_SESSION['YML_FILE_PATH'] = $file_path;

                            echo '<script type="text/javascript">
                            window.parent.uploadYMLResponse({ "status": "ok" });
                            </script>';

                        } else {

                            unlink($file_path);

                            echo '<script type="text/javascript">
                            window.parent.uploadYMLResponse({ "status": "'.$check_status.'" });
                            </script>';
                        }

                    } else {

                        echo '<script type="text/javascript">
                        window.parent.uploadYMLResponse({ "status": "Не удалось загрузить файл" });
                        </script>';
                    }

                } else {

                    echo '<script type="text/javascript">
                    window.parent.uploadYMLResponse({ "status": "Файл не доступен" });
                    </script>';
                }

            } catch(ClientException $ex){

                echo '<script type="text/javascript">
                window.parent.uploadYMLResponse({ "status": "Ошибка загрузки файла" });
                </script>';

            } catch(RequestException $ex){

                echo '<script type="text/javascript">
                window.parent.uploadYMLResponse({ "status": "Ошибка загрузки файла" });
                </script>';
            }
        }

    } else {

        echo '<script type="text/javascript">
        window.parent.uploadYMLResponse({ "status": "Не указан регион" });
        </script>';
    }

} else {

    echo '<script type="text/javascript">
    window.parent.uploadYMLResponse({ "status": "Ошибка авторизации" });
    </script>';
}

