<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');  use AOptima\Tools as tools;
\Bitrix\Main\Loader::includeModule('aoptima.project');  use AOptima\Project as project;

$results = array();

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $term = strip_tags($_GET['term']);

    $arResults = project\address::getSuggestions($term);
    
    foreach ( $arResults['suggestions'] as $key => $ar ){
        $results[] = array(
            'label' => $ar['unrestricted_value'],
            "value" => $ar['unrestricted_value']
        );
    }

}



echo json_encode($results);