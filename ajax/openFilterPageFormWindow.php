<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    global $USER;
    if( $USER->IsAuthorized() ){
        $user_groups = \CUser::GetUserGroup($USER->GetID());
        if( array_intersect($user_groups, project\filter_page::$user_groups) ){

            preg_match("/(\/catalog\/.*)/", trim(strip_tags($_POST['url'])), $matches, PREG_OFFSET_CAPTURE);
            $url = $matches[1][0];

            if( preg_match("/(\/filter\/.*\/apply\/)/", $url, $matches, PREG_OFFSET_CAPTURE) ){

                ob_start();
                    // filterPageForm
                    $APPLICATION->IncludeComponent(
                        "aoptima:filterPageFormWindow", "", []
                    );
                    $html = ob_get_contents();
                ob_end_clean();


                // Ответ
                echo json_encode([ "status" => "ok", "html" => $html ]);
                return;

            } else {

                // Ответ
                echo json_encode(["status" => "error", "text" => "Данная страница не является страницей фильтра"]);
                return;
            }

        } else {

            // Ответ
            echo json_encode(["status" => "error", "text" => "Ошибка доступа"]);
            return;
        }

    } else {

        // Ответ
        echo json_encode(["status" => "error", "text" => "Ошибка авторизации"]);
        return;
    }
}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;
