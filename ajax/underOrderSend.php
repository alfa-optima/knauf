<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $arFields = [];
    parse_str($_POST["form_data"], $arFields);

    // количество товаров
    $arProdCnts = explode(",", $arFields["product_cnt"]);
    if(count($arProdCnts) == 1)
    {
        $arProdCnts = Array($arFields["number"]);
    }

    // id товаров могут быть через запятую
    $arExpVal = explode(",", $arFields["product_id"]);
    $arProdsHtml = Array();
    $arProds = Array();
    $arBasketProds = Array();
    $total = 0;
    foreach($arExpVal as $indx => $prodID)
    {
        $product = tools\el::info($prodID);
        if( intval($product['ID']) > 0 ){
            $product = project\catalog::updateProductName($product);
            $arProdsHtml[] = $product['ID']
                .' - <a href="https://'.$_SERVER["SERVER_NAME"].$product['DETAIL_PAGE_URL'].'">'
                .$product['NAME'].'</a> ('.$arProdCnts[$indx].' шт)';

            $arProds[] = Array(
                "VALUE" => $product['ID'],
                "DESCRIPTION" => $arProdCnts[$indx]
            );

            $arBasketProds[] = $product['ID']
                ."|".$product["PROPERTY_ARTICLE_VALUE"]
                ."|".$product["NAME"]
                ."|".$arProdCnts[$indx];
        }
    }
    $arFields['product'] = implode(PHP_EOL."<br>", $arProdsHtml);

    $server_name = \Bitrix\Main\Config\Option::get('main', 'server_name');

    /////////////////////////////////
    $params['iblock_code'] = 'under_order';
    $params['iblock_name'] = 'Заявки под заказ';

    $params['email_to'] = \Bitrix\Main\Config\Option::get('aoptima.project', 'COMMON_ORDERS_EMAIL_TO');

        ////////////////////////////////
    $params['settings'] = array(
        'fio' => 'ФИО',
        'phone' => 'Телефон',
        'email' => 'Эл. почта',
        'city' => 'Город',
        'address' => 'Адрес',
        //'product_id' => 'ID товара',
        'product' => 'Товар',
		//'number' => 'Количество товара',
    );
    /////////////////////////////////
    $params['mail_title'] = 'Новая заявка на товар под заказ (сайт "'.$server_name.'")';
    $params['mail_html'] = '<p>Новая заявка на товар под заказ на сайте "'.$server_name.'"</p><hr>';
    $params['no_letter'] = true;
    /////////////////////////////////

    global $KNAUF_LOC;
    $arProps = Array(
        "CITY" => $arFields["city"],
        "LOC_CITY" => $KNAUF_LOC["CITY_LOC_ID"],
        "LOC_REGION" => $KNAUF_LOC["REGION_ID"],
        "PRODS" => $arProds,
        "FIO" => $arFields["fio"],
        "PHONE" => $arFields["phone"],
        "EMAIL" => $arFields["email"],
        "ADDRESS" => $arFields["address"],
        "DATE_CREATE_FOR_LETTERS" => ConvertTimeStamp(false, "FULL"),
    );

    $res = tools\feedback::send( $params, $arFields, $arProps );

    if( $res ){

        // оповестить дилеров о новой заявке под заказ
        project\under_order::SendMsgToDealers($res);

        project\under_order::SendMsgToClientOnCreate($res);

        // Ответ
        echo json_encode([
            "status" => "ok",
            "res" => $res,
            "products" => implode(PHP_EOL, $arBasketProds)
        ]);
        return;

    } else {

        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка отправки заявки"));
        return;

    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;
