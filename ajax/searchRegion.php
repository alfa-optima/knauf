<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

/*\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;
\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;*/

\Bitrix\Main\Loader::includeModule('sale');
use Bitrix\Sale;


$results = array();


if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $term = strip_tags($_GET['term']);

    $locs = \Bitrix\Sale\Location\LocationTable::getList(array(
        'filter' => array(
            'NAME.NAME' => $term."%",
            '=NAME.LANGUAGE_ID' => 'ru',
            "TYPE.CODE" => array('REGION'),
            '=PARENT.NAME.LANGUAGE_ID' => 'ru',
        ),
        'select' => array(
            '*',
            'NAME_RU' => 'NAME.NAME',
            'TYPE_CODE' => 'TYPE.CODE',
            'PARENT_NAME_RU' => 'PARENT.NAME.NAME',
            'ZIP' => 'EXTERNAL.XML_ID'
        ),
        'limit' => 15
    ));
    while( $loc = $locs->fetch() ){

        $results[$loc['ID']] = array(
            'label' => $loc['NAME_RU'].' ('.$loc['PARENT_NAME_RU'].')',
            "short_name" => $loc['NAME_RU'],
            "value" => $loc['ID']
        );
    }


}



echo json_encode($results);