<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$results = array();

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $term = trim(strip_tags($_GET['term']));

    if( strlen($term) > 0 ){

        $filter = Array(
            "ACTIVE" => "Y",
            "GROUPS_ID" => array(6),
            "UF_COMPANY_NAME" => "%".$term."%"
        );
        $fields = Array(
            'FIELDS' => array(
                'ID'
            ),
            //'NAV_PARAMS' => array('nTopCount' => 10),
            'SELECT' => array( 'UF_COMPANY_NAME' /*, 'UF_FULL_COMPANY_NAME'*/ )
        );
        $rsUsers = CUser::GetList(($by="id"), ($order="desc"), $filter, $fields);
        while ($arUser = $rsUsers->GetNext()){

            $results[$arUser['ID']] = array(
                'label' => htmlspecialchars_decode($arUser['UF_COMPANY_NAME']),
                "value" => $arUser['ID']
            );

        }
    }
}


echo json_encode($results);