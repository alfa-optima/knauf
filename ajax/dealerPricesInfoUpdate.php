<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/classes/general/xml.php');

//ini_set('display_errors', 'On');
//error_reporting(E_ALL);

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('iblock');

if( strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ){

    $user = new project\user();
    if( $USER->IsAuthorized() && $user->isDealer() ){

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        // поля для сохранения
        $arUserFields = [
            "UF_PRICES_YML_LINK" => strip_tags(trim($arFields["UF_PRICES_YML_LINK"])),
        ];

        // Сохраняем
        $obUser = new \CUser;
        $res = $obUser->Update($USER->GetID(), $arUserFields);
        if( $res ){

            // Ответ
            echo json_encode([ "status" => "ok" ]);
            return;

        } else {

            tools\logger::addError('Ошибка сохранения инфы по ценам [user_id='.$USER->GetID().']:'.$obUser->LAST_ERROR);
        }

    } else {

        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;