<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $arFields = [];
    parse_str($_POST["form_data"], $arFields);


    // --
    \CModule::IncludeModule("catalog");
    \CModule::IncludeModule("sale");
    \CModule::IncludeModule("iblock");

    // Пользователь по умолчанию
    $user_id = project\order::SERVICE_USER_ID;

    // Пользователь авторизован
    global $USER;
    if( $USER->IsAuthorized() )
    {

        // берём ID пользователя
        $user_id = $USER->GetID();
    }

    if( intval($user_id) > 0 ){
        // найти данные дилера $arFields["dealer"]
        $arDealer = tools\user::info($arFields["dealer"]);
        if( intval($arDealer['ID']) > 0 )
        {
            if($arFields["product_id"])
            {
                // Поиск торгового предложения для дилера
                global $KNAUF_LOC;
                $filter = [
                    "IBLOCK_CODE" => project\catalog::TP_IBLOCK_CODE,
                    "ACTIVE" => "Y",
                    "PROPERTY_LOCATION" => $KNAUF_LOC['REGION_ID'],
                    "PROPERTY_DEALER" => $arDealer['ID'],
                    "PROPERTY_CML2_LINK" => $arFields["product_id"],
                ];
                $fields = [
                    "ID", "CATALOG_GROUP_" . project\catalog::PRICE_ID, "XML_ID", "IBLOCK_ID"
                ];
                $dbOffers = \CIBlockElement::GetList(
                    ["SORT" => "ASC"], $filter,
                    false, ["nTopCount" => 1], $fields
                );

                // Если найдено
                if ($arOffer = $dbOffers->GetNext())
                {
                    // СОЗДАЁМ ЗАКАЗ
                    $order = Bitrix\Sale\Order::create('s1', $user_id);

                    // Передаём корзину в заказ
                    $basket = Bitrix\Sale\Basket::create('s1');
                    $item = $basket->createItem('catalog', $arOffer["ID"]);
                    $obIblocks = \CIBlock::GetByID($arOffer['IBLOCK_ID']);
                    if ($iblock = $obIblocks->GetNext())
                    {
                        $iblock_xml_id = $iblock['XML_ID'];
                    }
                    $orderCnt = ($arFields["number"] > $arOffer["CATALOG_QUANTITY"])?$arOffer["CATALOG_QUANTITY"]:$arFields["number"];
                    $arFieldsCart = Array(
                        'QUANTITY' => $orderCnt,
                        'CURRENCY' => 'RUB',
                        'LID' => 's1',
                        'PRODUCT_XML_ID' => $arOffer['XML_ID'],
                        'CATALOG_XML_ID' => $iblock_xml_id,
                        'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
                    );
                    $item->setFields($arFieldsCart);

                    $order->setBasket($basket);

                    // Тип плательщика
                    $order->setPersonTypeId(1);

                    // Заполняем свойства заказа
                    $propertyCollection = $order->getPropertyCollection();
                    $props = array(
                        1 => $arFields["fio"],
                        3 => $arFields["phone"],
                        4 => $arFields["email"],
                        32 => $arFields["where_from"]
                    );

                    $props[5] = $arFields["city"];
                    $props[29] = $KNAUF_LOC['CITY_LOC_ID'];
                    $props[21] = $arFields["address"];

                    $props[15] = htmlspecialchars_decode($arDealer['UF_COMPANY_NAME']);
                    $props[19] = $arDealer['ID'];

                    $props[31] = "Y"; // отметка, что быстрый заказ

                    foreach ( $props as $prop_id => $value ){
                        $prop = $propertyCollection->getItemByOrderPropertyId($prop_id);
                        $prop->setValue($value);
                    }

                    // Сохраняем новый заказ
                    $result = $order->save();

                    // Определяем ID нового заказа
                    $order_id = $order->GetId();

                    if (intval($order_id) > 0){

                        // Уменьшение остатков товара
                        \CCatalogProduct::Update($arOffer["ID"], array( 'QUANTITY' => ($arOffer["CATALOG_QUANTITY"] - $orderCnt) ));
                        BXClearCache(true, "/tp_list/");

                        // Установка стартового статуса заказу
                        $order_status = new project\order_status();
                        $order_status->set( $order_id, 'A' );

                        // Пишем в свойство элементов корзины их product_id
                        $order = \Bitrix\Sale\Order::load($order_id);
                        $basket = \Bitrix\Sale\Basket::loadItemsForOrder($order);
                        $arBasketProds = Array();
                        foreach( $basket as $key => $basketItem ){
                            $el = tools\el::info( $basketItem->getProductId() );
                            $propertyCollection = $basketItem->getPropertyCollection();
                            $propertyCollection->setProperty(array(
                                array(
                                    'NAME' => 'product_id',
                                    'CODE' => 'product_id',
                                    'VALUE' => $el['PROPERTY_CML2_LINK_VALUE'],
                                    'SORT' => 100,
                                ),
                            ));
                            $propertyCollection->save();

                            $arProdData = tools\el::info($el['PROPERTY_CML2_LINK_VALUE']);

                            $arBasketProds[] = $arProdData["ID"]
                                ."|".$arProdData["PROPERTY_ARTICLE_VALUE"]
                                ."|".$arProdData["NAME"]
                                ."|".$basketItem->getQuantity();
                        }
                        $basket->save();
                        $order->save();
                        $total = $order->getPrice();

                        $server_name = \Bitrix\Main\Config\Option::get('main', 'server_name');
                        // Письмо покупателю
                        ob_start();
                        $APPLICATION->IncludeComponent("aoptima:orderMail", "client", [
                            'ORDER_ID' => $order_id
                        ]);
                        $mailHtml = ob_get_contents();
                        ob_end_clean();
                        $mail_fields = Array(
                            "TITLE" => 'Новый заказ №'.$order_id.' на сайте '.$server_name,
                            "HTML" => $mailHtml,
                            "EMAIL_TO" => $arFields['email']
                        );
                        \CEvent::Send("MAIN", "s1", $mail_fields);

                        // Письмо дилеру
                        $dealerEmailTo = isset($arDealer['UF_EMAIL_FOR_ORDERS'])&&strlen($arDealer['UF_EMAIL_FOR_ORDERS'])>0?$arDealer['UF_EMAIL_FOR_ORDERS']:$arDealer['EMAIL'];
                        ob_start();
                        $APPLICATION->IncludeComponent("aoptima:orderMail", "dealer", [
                            'ORDER_ID' => $order_id
                        ]);
                        $mailHtml = ob_get_contents();
                        ob_end_clean();

                        $mail_fields = [
                            "TITLE" => 'Новый заказ №'.$order_id.' на сайте '.$server_name,
                            "HTML" => $mailHtml,
                            "EMAIL_TO" => $dealerEmailTo
                        ];

                        $commonEmailTo = \Bitrix\Main\Config\Option::get('aoptima.project', 'COMMON_ORDERS_EMAIL_TO');
                        $commonEmailTo = trim(strtolower($commonEmailTo));
                        if( strlen($commonEmailTo) > 0 ){
                            $mail_fields["HIDDEN_COPY_EMAIL_TO"] .= $commonEmailTo.', ';
                        }

                        //адреса сбытовых дирекций
                        $rsUser = CUser::GetList(($by="ID"), ($order="desc"), array("ID"=>$arDealer["ID"]),array("SELECT"=>array("UF_SBITDIR")));
                        $arUser=$rsUser->Fetch();
                        $EMAILS = array();
                        $res = CIBlockElement::GetProperty(14, $arUser["UF_SBITDIR"], "sort", "asc", array("CODE" => "EMAIL"));
                        while ($ob = $res->GetNext())
                        {
                            $EMAILS[] = $ob['VALUE'];
                        }
                        $sbitdir = implode(",", $EMAILS);
                        if( strlen($sbitdir) > 0 ){

                            $mail_fields["HIDDEN_COPY_EMAIL_TO"] .= $sbitdir;
                        }

                        \CEvent::Send("MAIN", "s1", $mail_fields);

                        // Ответ
                        echo json_encode( Array(
                            "status" => "ok",
                            "order_id" => $order_id,
                            "dealer_id" => $arDealer["ID"],
                            "dealer_name" => $arDealer["NAME"],
                            "products" => implode(PHP_EOL, $arBasketProds),
                            "total" => $total
                        ) );
                        return;
                    } else {
                        // Ответ
                        echo json_encode( Array( "status" => "error", 'text' => $result->getErrorMessages() ) );
                        return;
                    }
                }
                else
                {
                    // Ответ
                    echo json_encode(Array("status" => "error", "text" => "Ошибка. Не Найдено торговое предложение."));
                    return;
                }
            }
            else
            {
                // Ответ
                echo json_encode(Array("status" => "error", "text" => "Ошибка. Не передан ID товара."));
                return;
            }
        }
        else
        {
            // Ответ
            echo json_encode(Array("status" => "error", "text" => "Ошибка. Выбранный дилер не найден."));
            return;
        }
    }
    else
    {
        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }

    // --


    /*foreach ( $arFields as $key => $value ){
        if( $key == 'product_id' ){
            $product = tools\el::info($value);
            if( intval($product['ID']) > 0 ){
                $product = project\catalog::updateProductName($product);
                $arFields['product'] = '<a href="'.$product['DETAIL_PAGE_URL'].'">'.$product['NAME'].'</a>';
            }
        }
    }


    $server_name = \Bitrix\Main\Config\Option::get('main', 'server_name');

    /////////////////////////////////
    $params['iblock_code'] = 'quick_order';
    $params['iblock_name'] = 'Быстрый заказ';

    $params['email_to'] = \Bitrix\Main\Config\Option::get('aoptima.project', 'COMMON_ORDERS_EMAIL_TO');

    ////////////////////////////////
    $params['settings'] = array(
        'dealer' => 'Дилер',
        'fio' => 'ФИО',
        'phone' => 'Телефон',
        'email' => 'Эл. почта',
        'city' => 'Город',
        'product_id' => 'ID товара',
        'product' => 'Товар',
		'number' => 'Количество товара',
    );
    /////////////////////////////////
    $params['mail_title'] = 'Новый быстрый заказ на товар (сайт "'.$server_name.'")';
    $params['mail_html'] = '<p>Новый быстрый заказ на товар на сайте "'.$server_name.'"</p><hr>';
    /////////////////////////////////

    $res = tools\feedback::send( $params, $arFields );

    if( $res ){

        // Ответ
        echo json_encode([ "status" => "ok" ]);
        return;

    } else {

        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка отправки заявки"));
        return;

    }*/

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;
