<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $user = new project\user();
    if( $USER->IsAuthorized() && $user->isDealer() ){

        $fields = array(
            'UF_DELIV_INFO_FILE' => array('del' => 'Y')
        );

        $user = new \CUser;
        $res = $user->Update($USER->GetID(), $fields);

        if( $res ){

            ob_start();
                $APPLICATION->IncludeComponent(
                    "aoptima:personalDeliveryLocations", ""
                );
                $html = ob_get_contents();
            ob_end_clean();

            // Ответ
            echo json_encode(Array("status" => "ok", "html" => $html));
            return;

        }


    } else {
        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Ошибка авторизации"));
        return;
    }
}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;