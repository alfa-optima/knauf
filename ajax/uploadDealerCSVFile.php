<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$user = new project\user();
if( $USER->IsAuthorized() && $user->isDealer() ){

    $arResult['ERRORS'] = array();

    $_SESSION['CSV_FILE_PATH'] = false;
    $_SESSION['CHECKED_LINES'] = array();
    $_SESSION['ERROR_LINES'] = array();
    $_SESSION['SUCCESS_LINES_CNT'] = 0;
    $_SESSION['UPLOAD_FILE_LOC_ID'] = false;

    if(
        intval($_POST['loc_id']) > 0
        &&
        intval($_FILES['csv']['size']) > 0
    ){
        $dealer_csv_file = new project\dealer_csv_file($_FILES, $USER->GetID());
        $uploadResult = $dealer_csv_file->upload();

        if( $uploadResult['status'] == 'ok' ){

            $_SESSION['CSV_FILE_PATH'] = $uploadResult['file_path'];
            $_SESSION['UPLOAD_FILE_LOC_ID'] = $_POST['loc_id'];

            echo '<script type="text/javascript">
            window.parent.uploadCSVResponse(\'{"status": "ok"}\');
            </script>';


        } else if( $uploadResult['status'] == 'error' ){

            $_SESSION['CSV_FILE_PATH'] = false;
            $_SESSION['CHECKED_LINES'] = array();
            $_SESSION['SUCCESS_LINES_CNT'] = 0;
            $_SESSION['UPLOAD_FILE_LOC_ID'] = false;

            $arResult['ERRORS'][] = $uploadResult['text'];

            echo '<script type="text/javascript">
            window.parent.uploadCSVResponse(\'{"status": "error", "text": "'.implode('<br>', $arResult['ERRORS']).'"}\');
            </script>';

        }

    }

}

