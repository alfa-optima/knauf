<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){
    if( $USER->IsAuthorized() ){

        ob_start();
            // История заказов
            $APPLICATION->IncludeComponent(
                "aoptima:personalOrderHistoryBlock", "",
                array('USER_ID' => $USER->GetID(), 'IS_AJAX' => 'Y', 'IS_MOB_APP' => ($_REQUEST["app"])?"Y":"")
            );
            $html = ob_get_contents();
        ob_end_clean();

        // Ответ
        echo json_encode(Array("status" => "ok", "html" => $html));
        return;

    }
}