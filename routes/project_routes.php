<? require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use AOptima\Project as project;
use AOptima\Tools as tools;

if(
    \Bitrix\Main\Loader::includeModule('aoptima.project')
    &&
    \Bitrix\Main\Loader::includeModule('aoptima.tools')
){

    $arURI = tools\funcs::arURI( tools\funcs::pureURL() );

    // Проверка наличия SEO-страницы с символьным кодом $arURI[1]
    project\SeopagecategoryTable::checkTable();
    project\SeopageTable::checkTable();
    $seo_page = project\SeopageTable::getByCode( $arURI[1] );
    


    // Каталог
    if( $seo_page ){

        include( $_SERVER[ "DOCUMENT_ROOT" ] . "/catalog/seo_page.php" );

    } else {

        //include( $_SERVER[ "DOCUMENT_ROOT" ] . "/include/404include.php" );

    }

}